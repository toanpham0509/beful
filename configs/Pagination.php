<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );


define( "PER_PAGE_NUMBER" , 100 );

/*end of file Pagination.php*/
/*location: Pagination.php*/