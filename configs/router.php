<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined( 'BOOTSTRAP_PATH' ) ) exit ( "No direct script access allowed" );
/**
 * 
 * @var unknown
 */
$CONTROLLERS = array (
	"/" 								=> 					"Home",
	"dang-nhap"							=> 					"Login",
	"dang-xuat"							=> 					"LogOut",
	"don-hang"							=> 					"Order",
	"san-pham"							=> 					"Product",
	"khuyen-mai"						=> 					"Promote",
	"bao-gia"							=> 					"Price",
	"cua-hang"							=>					"Shop",
	"nhap-kho"							=> 					"ImportWarehouse",
	"chuyen-kho"						=>					"TransferWarehouse",
	"ton-kho"							=>					"Inventory",
	"xuat-kho"							=>					"ExportWarehouse",
	"tra-ve"							=>					"Refund",
	"nha-cung-cap"						=>					"Supplier",
	"bang-gia-mua"						=>					"BuyPrice",
	"khach-hang"						=> 					"Customer",
	"nhom-san-pham"						=>					"ProductCategory",
	"kho-hang"							=>					"Warehouse",
	"don-vi-san-pham"					=>					"ProductUnit",
	"dang-nhap"							=>					"Login",
	"don-hang-mua"						=>					"BuyOrder",
	"khach-hang-thanh-toan"				=>					"CustomerPay",
	"thanh-toan-nha-cung-cap"			=>					"SupplierPay",
	"tai-khoan-phai-thu-khach-hang"		=>					"HaveToReceipt",
	"tai-khoan-phai-tra-nha-cung-cap"	=>					"HaveToPay",
	"tien-mat"							=>					"Money",
	"ngan-hang"							=>					"Bank",
	"tai-khoan-ngan-hang"				=>					"BankAccount",
	"phieu-thu"							=> 					"Receipt",
	"phieu-chi"							=>					"Expense",
	"top-san-pham"						=>					"TopProduct",
	"top-khach-hang"					=>					"TopCustomer",
	"top-nhan-vien-ban-hang"			=>					"TopSalePerson",
	"top-nhap-kho"						=> 					"TopImport",
	"doanh-thu-ban-hang"				=> 					"Income",
	"the-kho"							=>					"ReportCard",
	"bang-ke-nhap-kho"					=>					"ReportImport",
	"bang-ke-xuat-kho"					=>					"ReportExport",
	"hang-ton-kho-hsd"					=>					"InventoryExpired",
	"bang-ke-nhap-xuat"					=>					"ReportImportExport"
);
$METHODS = array (
	"/"									=> 					"index",
	"them-moi"							=> 					"addNew",
	"trang"								=>					"page",
	"ton-trong-kho"						=>					"viewInWarehouse",
	"bieu-do"							=> 					"chart",
	"bieu-do-hang"						=>					"reportLine",
	"bieu-do-manh"						=>					"reportPie",
	"in-don-hang"						=> 					"printOrder",
	"chinh-sua"							=> 					"edit",
	"xoa"								=> 					"delete",
	"xac-nhan"							=>					"inspection",
	"in"								=>					"print1",
	"xem"								=>					"view"
);
/* End of file router.php */
/* Location: ./configs/router.php */