<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 * 
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
/**
 * >> cache folder path
 * 
 * @property	It is the directory where the cache file is stored
 */
define( "CACHE_FOLDER", "cache" );
/**
  * >> cache's status
  * 
  * @property	It is cache's status. true is turn on else false is turn off
  * @var		true or false
  * */
define( "CACHE", false );
/**
 * >> cache time
 * 
 * @property	It is time to cache file is valid
 * */
define( "CACHE_TIME", 1200 );

 /*end of file cache.php */
 /*location: ./configs/cache.php*/