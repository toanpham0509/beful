<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if(!defined ('BOOTSTRAP_PATH' ) ) exit("No direct script access allowed" );

/*Sale management*/
define("SALE_ORDER", null);
define("SALE_ORDER_NEW", 69);
define("SALE_ORDER_EDIT_ITEM", 70);
define("SALE_ORDER_DELETE_ITEM", 71);
define("SALE_ORDER_VIEW_ITEM", null);
define("SALE_ORDER_INSPECTION", 67);
define("SALE_ORDER_INSPECTION_AUTO", 69);
define("SALE_ORDER_PRINT", 65);
define("SALE_ORDER_EDIT_PRICE_ITEM_WHEN_NEW_ORDER", 64);
define("SALE_ORDER_APPLY_PROMOTE", 63);
define("SALE_ORDER_EXPORT_EXCEL", 59);

define("PRODUCT", null);
define("PRODUCT_VIEW_AMOUNT", 36);
define("PRODUCT_VIEW_PRICE", 36);
define("PRODUCT_NEW", 29);
define("PRODUCT_VIEW_ITEM", 28);
define("PRODUCT_EDIT_ITEM", 27);
define("PRODUCT_DELETE", 72 );

define("PROMOTE", 25);
define("PROMOTE_NEW", 26);
define("PROMOTE_VIEW_ITEM", 24);
define("PROMOTE_EDIT_ITEM", 23);

define("SALE_PRICE", null);
define("SALE_PRICE_NEW", 74);
define("SALE_PRICE_EDIT", 75);
define("SALE_PRICE_DELETE", 76);
define("SALE_PRICE_VIEW_ITEM", 77);

define("SHOP", null);
define("SHOP_NEW", 22);
define("SHOP_VIEW_ITEM", 21);
define("SHOP_EDIT_ITEM", 20);
define("SHOP_DELETE", 78);


/*Buy management*/
define("BUY_ORDER", 32);
define("BUY_ORDER_NEW", 33);
define("BUY_ORDER_PRINT", 79);
define("BUY_ORDER_EDIT", 80);
define("BUY_ORDER_DELETE", 81);
define("BUY_ORDER_INSPECTION", 82);
define("BUY_ORDER_VIEW_ITEM", 32);
define("BUY_ORDER_DEACTIVATE", 31);
define("BUY_ORDER_EXPORT_EXCEL", 30);

define("IMPORT_WAREHOUSE", 43);
define("IMPORT_WAREHOUSE_NEW", 84);
define("IMPORT_WAREHOUSE_PRINT", 85);
define("IMPORT_WAREHOUSE_VIEW", 83);
define("IMPORT_WAREHOUSE_EDIT", 86);
define("IMPORT_WAREHOUSE_DELETE", 87);
define("IMPORT_WAREHOUSE_INSPECTION", 88);

define("TRANSFER_WAREHOUSE", 41);
define("TRANSFER_WAREHOUSE_PRINT", 41);
define("TRANSFER_WAREHOUSE_NEW", 41);
define("TRANSFER_WAREHOUSE_EDIT_ITEM", 41);
define("TRANSFER_WAREHOUSE_VIEW_ITEM", 41);
define("TRANSFER_WAREHOUSE_DELETE", 41);
define("TRANSFER_WAREHOUSE_INSPECTION", 41);

define("REFUND_ORDER", 62);
define("REFUND_ORDER_NEW", 62);
define("REFUND_ORDER_EDIT", 93);
define("REFUND_ORDER_PRINT", 61);
define("REFUND_ORDER_VIEW_ITEM", 60);
define("REFUND_ORDER_DELETE", 90);
define("REFUND_ORDER_INSPECTION", 89);

define("SUPPLIER", 91);
define("SUPPLIER_NEW", 19);
define("SUPPLIER_VIEW_ITEM", 18);
define("SUPPLIER_EDIT_ITEM", 17);
define("SUPPLIER_DELETE", 92);

define("BUY_PRICE", 94);
define("BUY_PRICE_NEW", 95);
define("BUY_PRICE_EDIT", 96);
define("BUY_PRICE_VIEW_ITEM", 97);
define("BUY_PRICE_DELETE", 98);

/*Customer management*/
define("CUSTOMER", 57);
define("CUSTOMER_NEW", 58);
define("CUSTOMER_VIEW_ITEM", 56);
define("CUSTOMER_EDIT_ITEM", 55);
define("CUSTOMER_DELETE_ITEM", 111);
define("CUSTOMER_DEACTIVATE", 54);
define("CUSTOMER_VIEW_HISTORY_BUY", 53);
define("CUSTOMER_EXPORT_EXCEL", 50);
define("CUSTOMER_HISTORY_BUY_EXPORT_EXCEL", 49);

/* Warehouse management */
define("MODULE_WAREHOUSE", null);
define("WAREHOUSE", 47);
define("WAREHOUSE_NEW", 48);
define("WAREHOUSE_VIEW_ITEM", 46);
define("WAREHOUSE_EDIT_ITEM", 45);
define("WAREHOUSE_DEACTIVATE", 44);
define("WAREHOUSE_DELETE_ITEM", 100);
define("WAREHOUSE_VIEW_IMPORT_EXPORT_TRANSFER_REFUND", 101);

define("INVENTORY", 102);
define("INVENTORY_NEW", 103);
define("INVENTORY_EDIT", 104);
define("INVENTORY_DELETE", 105);
define("INVENTORY_INSPECTION", 106);

/* Account management */
define("ACCOUNT_VIEW_SALE_BUY_ORDER_REFUND", 107);
define("ACCOUNT_PAY_CUSTOMER_SUPPLIER", 108);
define("ACCOUNT_RECEIPT_EXPENSE", 109);
define("ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE", 12);
define("ACCOUNT_VIEW_PRINT_BUY_SALE_REFUND", 11);
define("ACCOUNT_BANK_MONEY", 14);

/* Report management */
define("REPORT", 110);
define("REPORT_INCOME", 9);
define("REPORT_IMPORT_EXPORT_INVENTORY", 38);
/*end of file Access.php*/
/*location: Access.php*/