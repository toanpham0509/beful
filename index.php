<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
/**
 * --------------------------
 * >> application environment
 * --------------------------
 * Values:
 * 		- development
 * 		- testing
 * 		- production
 *
 */
define ( "ENVIRONMENT", "publish" );
/**
 * -------------------------
 * >> bootstrap path
 * -------------------------
*/
define ( "BOOTSTRAP_FOLDER", "bootstraps" );
if (! is_dir ( BOOTSTRAP_FOLDER )) {
	die ( "Your system folder path does not appear to be set correctly." );
} else {
	define( "BOOTSTRAP_PATH", realpath( BOOTSTRAP_FOLDER ) . "/" );
}
/**
 * -------------------------
 * >> application path
 * -------------------------
 * */
define ( "APPLICATION_FOLDER", "application" );
if ( !is_dir ( APPLICATION_FOLDER )) {
	die ( "Your application folder path does not appear to be set correctly." );
} else {
	define( "APPLICATION_PATH", realpath( APPLICATION_FOLDER ) . "/" );
}
/**
 * ------------------------
 * >> load bootstrap file
 * ------------------------
 * */
require_once( BOOTSTRAP_FOLDER . "/Bootstrap.php" );
/* End if file index.php */
/* Location ./index.php */