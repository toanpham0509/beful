<?php
	class Warehouse_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["workplace_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "workplace_name"	,
				"table_name"	=> "workplace"
			);
			$fields["district_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "district_name"	,
				"table_name"	=> "district"
			);
			return $fields;
		}

		function get_all_warehouse(){
			$fields = array("warehouse_id","warehouse_name");
			$this->fields	= $this->filter_fields($fields);
			return $this->item_list();
		}

		function filter_by_shop($shop_id){
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".workplace_id	= '".$shop_id."'
			";
		}

		function get_warehouses_fields(){
			$fields	= array('warehouse_id','warehouse_name','is_runing','warehouse_code');
			$this->fields	= $this->filter_fields($fields);
		}

		function get_warehouse_fields(){
		//	$fields	= array('warehouse_id','warehouse_name','warehouse_address','is_runing','description');
		//	$this->fields	= $this->filter_fields($fields);
			
			$this->fields[]	= array(
				"column_name"	=> "city_id"	,
				"table_name"	=> "city"
			);
			$this->fields[]	= array(
				"column_name"	=> "city_name"	,
				"table_name"	=> "city"
			);
			$this->fields[]	= array(
				"column_name"	=> "country_id"	,
				"table_name"	=> "country"
			);
			$this->fields[]	= array(
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN city  	ON district.city_id			= city.city_id
				<br>LEFT JOIN country  	ON city.country_id			= country.country_id
			";
		}	
}		
		