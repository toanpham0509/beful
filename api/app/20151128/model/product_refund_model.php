<?php
	class Product_Refund_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/	
		function add_output_order_info(){
			$this->fields[]	=array( 
				"column_name"	=> "input_date"	,
				"table_name"	=> "input_order"
			);
			$this->fields[]	=array( 
				"column_name"	=> "input_code"	,
				"table_name"	=> "input_order"
			);
			$this->fields[]	=array( 
				"column_name"	=> "input_order_id"	,
				"table_name"	=> "input_order"
			);
			$this->fields[]	=array( 
				"column_name"	=> "order_id"	,
				"table_name"	=> "output_order"
			);
			$this->fields[]	=array( 
				"column_name"	=> "order_code"	,
				"table_name"	=> "output_order"
			);
			$this->fields[]	=array( 
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"
			);
			$this->fields[]	=array( 
				"column_name"	=> "warehouse_id"	,
				"table_name"	=> "warehouse"
			);
			$this->fields[]	=array( 
				"column_name"	=> "warehouse_name"	,
				"table_name"	=> "warehouse"
			);
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN output_order 	ON output_lines.order_id 		= output_order.order_id
				<br>LEFT JOIN input_order 	ON input_lines.input_order_id 	= input_order.input_order_id
				<br>LEFT JOIN partner 		ON output_order.partner_id		= partner.partner_id
				<br>LEFT JOIN warehouse		ON input_lines.warehouse_id		= warehouse.warehouse_id
			";
		}
		
		function add_customer_info(){
			$this->fields[]	=array( 
				"column_name"	=> "partner_id"	,
				"table_name"	=> "partner"
			);
			$this->fields[]	=array( 
				"column_name"	=> "partner_address"	,
				"table_name"	=> "partner"
			);
			$this->fields[]	=array( 
				"column_name"	=> "partner_phone_number"	,
				"table_name"	=> "partner"
			);
			$this->fields[]	=array( 
				"column_name"	=> "district_name"	,
				"table_name"	=> "district"
			);
			$this->fields[]	=array( 
				"column_name"	=> "city_name"	,
				"table_name"	=> "city"
			);
			$this->fields[]	=array( 
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN district 	ON partner.district_id 		= district.district_id
				<br>LEFT JOIN city		ON district.city_id			= city.city_id
				<br>LEFT JOIN country	ON city.country_id			= country.country_id
			";
		}
		
		function get_refundId_by_order($output_line_list){			
			$fields = array($this->item_list_model['primary_key']);
			$this->fields = $this->filter_fields($fields);			
			$this->item_list_model['filter'] .= "
				<br>AND ".$this->item_list_model["table_name"].".output_line_id IN ".$output_line_list."
			";
		}
}		
		