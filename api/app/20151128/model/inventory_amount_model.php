<?php
	class Inventory_Amount_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".inventory_date DESC ";
			$this->item_list_model["filter"]		= " WHERE 1=1 ";
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/	
		function get_fields(){
			$result[]	= array(
				"table_name" 	=> $this->item_list_model['table_name']		,
				"column_name" 	=> "inventory_date"	,
				"lable" 	=> "inventory_date"	
			);
			$result[]	= array(
				"table_name" 	=> $this->item_list_model['table_name']		,
				"column_name" 	=> "warehouse_id"	,
				"lable" 	=> "warehouse_id"	
			);
			$result[]	= array(
				"table_name" 	=> $this->item_list_model['table_name']		,
				"column_name" 	=> "input_line_id"	,
				"lable" 	=> "input_line_id"	
			);
			$result[]	= array(
				"table_name" 	=> $this->item_list_model['table_name']		,
				"column_name" 	=> "product_id"	,
				"lable" 	=> "product_id"	
			);
			$result[]	= array(
				"table_name" 	=> $this->item_list_model['table_name']		,
				"column_name" 	=> "inventory_input_line_amount"	,
				"lable" 	=> "inventory_input_line_amount"	
			);
			
			return $result;
		}
		
		
}		
		