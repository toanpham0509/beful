<?php
	class Left_Menu_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
			$this->item_list_model["not_show_list"][]	= "user_password";
			$this->item_list_model["not_show_list"][]	= "user_birthday";
			$this->item_list_model["not_show_list"][]	= "user_email";
		//	$this->item_list_model["not_show_list"][]	= "district_name";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			global $config_dir;
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/member_manager"	,
				'controller_name' 	=> "user"	,
				'text'			=> "member_manager"
			);
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/member_work_manager"	,
				'controller_name' 	=> "user"	,
				'text'			=> "member_work_manager"
			);
			
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/rule_manager"	,
				'controller_name' 	=> "rule"	,
				'text'			=> "rule_manager"
			);
			
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/position_rule_manager"	,
				'controller_name' 	=> "position_rule"	,
				'text'			=> "position_rule"
			);
			
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/promote_manager"	,
				'controller_name' 	=> "promote"	,
				'text'			=> "promote_manager"
			);
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/promote_product_manager"	,
				'controller_name' 	=> "promote_product"	,
				'text'			=> "promote_product_manager"
			);
			$this->controller_list[] = array(
				'url'			=> $config_dir["root_url"]."admin/promote_order_manager"	,
				'controller_name' 	=> "promote_order"	,
				'text'			=> "promote_order_manager"
			);
			$this->table_properties	= $this->table_properties();
		}
		
		function menu_1(){
			/*
			$controller_list[0]	= $this->controller_list[0];
			$controller_list[1]	= $this->controller_list[1];
			$controller_list[2]	= $this->controller_list[2];
			*/
			return $this->controller_list;
		}
		
		function menu_2(){
			global $config_dir;
			$path    = $config_dir["application_folder"]."controller/";			
			foreach(scandir($path) as $k=>$v) 
				if($v != "." and $v != ".." and $v != "base_controller.php"){
				//echo $k.": ".$v."<br>";
				$item_manager	 = str_replace('_controller.php','_manager',$v);
				$controller_name = str_replace('_controller.php','',$v);
				$this->controller_list[] = array(
					'url'			=> $config_dir["root_url"]."admin/".$item_manager	,
					'controller_name' 	=> $controller_name	,
					'text'			=> str_replace('_',' ',$item_manager)
				);
			}
			return $this->controller_list;
		}
		
		function get_fields(){
			return false;
		}
		
}		
		