<?php
	class Product_Transfer_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["order_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "order_code"	,
				"table_name"	=> "output_order"
			);
			$fields['warehouse_from_id']	=array( 
				"column_name"	=> "warehouse_from_name"	,
				"table_name"	=> "warehouse"
			);
			$fields["warehouse_to_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "warehouse_to_name"	,
				"table_name"	=> "tmp_table"
			);
			return $fields;
		}
		
		function fetchAll($sql,$fields,$db_info){
			$sql	= $this->replace_cat_1($sql);
			return parent::fetchAll($sql,$fields,$db_info);
		}
		
		function item_list(){
			$left_join_1	= "warehouse ON ".$this->item_list_model['table_name'].
				".warehouse_to_id = warehouse";
			$left_join_2	= "warehouse as tmp_table ON ".$this->item_list_model['table_name'].
				".warehouse_to_id = tmp_table";
			$this->item_list_model["left_join"] = str_replace($left_join_1,$left_join_2, 
				$this->item_list_model["left_join"]);
			return parent::item_list();	
		}
		
		function number_rows_pages($sql , $num_rows_pp){
			$sql	= $this->replace_cat_1($sql);
			return parent::number_rows_pages($sql , $num_rows_pp);
		}
		
		function replace_cat_1($sql){
			$sql = str_replace("warehouse.warehouse_from_name",
					"warehouse.warehouse_name as warehouse_from_name", $sql);
			$sql = str_replace("tmp_table.warehouse_to_name",
					"tmp_table.warehouse_name as warehouse_to_name", $sql);	
			$sql = str_replace("warehouse.status","tmp_table.status",$sql);			
			return $sql;		
		}
		
		function add_output_order_info(){
			$this->fields[]	=array( 
				"column_name"	=> "order_date"	,
				"table_name"	=> "output_order"
			);
			
		}
			
}		
		