<?php
	class Product_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["unit_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "unit_name"	,
				"table_name"	=> "unit"
			);
			return $fields;
		}

		function filter_warehouse($warehouse_id){			
			$this->item_list_model['filter']	.= "
				AND inventory.warehouse_id	= '".$warehouse_id."'
			";
		}
		
		function filter_product_id($product_id){
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".".
				$this->item_list_model['primary_key']
				."	= '".$product_id."'
			";
		}
		
		function get_products(){
			$this->fields[]	= array(
				'column_name'	=> 'inventory_amount'	,
				'table_name'	=> 'inventory'
			);
			$this->fields[]	= array(
				'column_name'	=> 'product_price'	,
				'table_name'	=> 'product_price'
			);
			$this->item_list_model['left_join']	.= "
				LEFT JOIN inventory ON ".$this->item_list_model['table_name'].
				".".$this->item_list_model['primary_key']."	
				= inventory.product_id
			";
			$this->item_list_model['left_join']	.="
				RIGHT JOIN product_price ON 
				".$this->item_list_model["table_name"].".
				".$this->item_list_model["primary_key"]." = product_price.product_id
			";
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
		//	return $this->item_list();
		}	
		function get_product(){
			
		}
		
		function get_all_product_fields(){
			$fields = array('product_id','product_code','product_name');
			$this->fields	= $this->filter_fields($fields);
		}
		
		function filter_price($product_id_list){			
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".product_id 
				IN (".$product_id_list.")
			";
			/*$this->item_list_model['left_join']	.="
				RIGHT JOIN price_list ON product_price.price_list_id = price_list.price_id
			";
			$this->item_list_model['filter']	.= "
				AND price_list.price_id	= '".$price_id."'
			";*/
		}
		
		function filter_inventory_date($inventory_date){
			$this->item_list_model['filter']	.= "
				AND inventory.inventory_date = '".$inventory_date."'
			";
		}
		
		function get_list_products_fields(){
			$fields = array(
				'product_id'	, 
				'product_code'	,
				'product_name'	,
				'product_thumbnail'	,
				'unit_id'	,
				'unit_name'	,
				'product_barcode'	,
			);
			$this->fields	= $this->filter_fields($fields);
		}
		
}		










		