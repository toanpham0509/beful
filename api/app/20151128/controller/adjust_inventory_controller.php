<?php
	Class Adjust_Inventory_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Adjust_Inventory_Model;
			$this->this_view	= new Adjust_Inventory_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_adjust_inventories(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_adjust_inventory(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_adjust_inventory(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_adjust_inventory(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_adjust_inventory(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
	}