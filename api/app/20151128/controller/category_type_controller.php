<?php
	Class Category_Type_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Category_Type_Model;
			$this->this_view	= new Category_Type_View;
			
		}
		
	}