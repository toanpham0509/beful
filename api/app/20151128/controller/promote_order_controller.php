<?php
	Class Promote_Order_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Promote_Order_Model;
			$this->this_view	= new Promote_Order_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_promote_orders(){
		//	$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function new_promote_order(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_promote_order(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_promote_order(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
	}