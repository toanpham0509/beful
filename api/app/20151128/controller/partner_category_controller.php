<?php
	Class Partner_Category_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Partner_Category_Model;
			$this->this_view	= new Partner_Category_View;
			
		}
		
		function get_all_customer(){
			$data	= $this->this_model->get_all_customer();
			foreach($data['item_list'] as $k=>$v){
			//	echo $k.": ".$v['partner_id'].": ".$v['partner_name']."<br>";
				$data_2[$k]['customer_id']	= $v['partner_id'];
				$data_2[$k]['customer_name']	= $v['partner_name'];
				$data_2[$k]['phone']	= $v['partner_phone_number'];
			};
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function report_sale(){
			$data	= $this->this_model->report_sale();
			foreach($data["item_list"] as $k=>$v){
				unset($v["partner_category_id"]);
				unset($v["category_id"]);
				unset($v["category_name"]);
				$output[$k]			= $this->this_model->convert_form_to_db($v,true);	
			}
			$result	= $this->this_view->return_true($output);
			return  $this->this_view->json_show($result);
		}
		
		function partner_filter(){
			$trace			=debug_backtrace();
			$model_method	= $trace[1]['function'];
			
			if(!empty($_POST['data_post'])){
				$input = $this->this_model->convert_form_to_db($_POST['data_post'],false);
				foreach($input as $k=>$v)					
					if(!empty($v)){
						if($k == 'supplier_name') $k= "partner_name";
					//	echo $k.": ".$v."<br>";
						$args["field_name"]		= $k;
						$args["field_value"]	= $v;
						$args['table_name']		= "partner";			
						$this->this_model->filter_like($args);	
				}
			}
			$data	= $this->this_model->$model_method();
			$tmp	= $data['item_list'];
			unset($data['item_list']);
			unset($data['fields']);
			unset($data['primary_key']);
			$result	= $data; 
			
			foreach($tmp as $k=>$v){
				if($model_method =="get_suppliers"){
					$v['supplier_id'] = $v['partner_id'];
					$v['supplier_name'] = $v['partner_name'];
					unset($v['partner_id']);
					unset($v['partner_name']);
				}
				$result['item_list'][$k]	= $this->this_model->convert_form_to_db($v,true);	
			}
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
			
		}
		
		function get_suppliers(){
			return $this->partner_filter();
		}
		
		function get_customers(){
			return $this->partner_filter();
		}
		
		function get_customer(){
			$this->this_model->get_partner_fields();
			$this->this_model->filter_customer();
			return $this->get_partner();
		}	
		
		function get_supplier(){
			$this->this_model->get_partner_fields();
			$this->this_model->filter_supplier();
			return $this->get_partner();
		}
		
		function get_partner(){
			$trace			= debug_backtrace();
			$model_method	= $trace[1]['function'];
			if($model_method	== 'get_customer'){
				$partner_id_field	= 'customer_id';
			}elseif($model_method	== 'get_supplier'){
				$partner_id_field	= 'supplier_id';
			}
			$data	= $this->this_model->get_partner($_POST['data_post'][$partner_id_field]);
			foreach($data['item_list'] as $k=>$v){
			//	echo $k.": ".$v[partner_id]."<br>";
					if($model_method	== 'get_supplier'){
						$v['supplier_id'] = $v['partner_id'];
						$v['supplier_name'] = $v['partner_name'];
						unset($v['partner_id']);
						unset($v['partner_name']);
					}
				$result[$k]	= $this->this_model->convert_form_to_db($v,true);	
			}
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function new_partner($update_info){
			$this->this_model->item_add_new($update_info);
		}
		
		function report_sale_line(){
			$this->this_model->filter_customer();
			$result	= $this->item_info();
			$args['controller']	= "order";
			$args['action']		= "report_sale_line";
			
			foreach($result['item_list'] as $k=>$v){
				unset($result['item_list'][$k]["partner_category_id"]);
				unset($result['item_list'][$k]["category_id"]);
				unset($result['item_list'][$k]["category_name"]);
				$_POST['data_post']['customer_id'] = $v['customer_id'];
				$order_list = json_decode($this->call_controller($args),true);
				$result['item_list'][$k]['order_list'] = $order_list['data']['item_list'];
			}
			
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
	}