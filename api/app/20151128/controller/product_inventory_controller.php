<?php
	Class product_inventory_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new product_inventory_Model;
			$this->this_view	= new product_inventory_View;
			
		}
		
		function get_products_inventory_amount(){
			if(!empty($_POST['data_post']["warehouse_id"])){
				$this->this_model->filter_warehouse_id($_POST['data_post']["warehouse_id"]);
			}
			if(!empty($_POST['data_post']["input_line_id"])){
				$this->this_model->filter_input_line_id($_POST['data_post']["input_line_id"]);
			}
			if(!empty($_POST['data_post']["inventory_date"])){
				$this->this_model->filter_inventory_date($_POST['data_post']["inventory_date"]);
			}
			$data	= $this->this_model->item_list();
			unset($data['primary_key']);
			unset($data['fields']);
			foreach($data['item_list'] as $k=>$v){
				$data['item_list'][$k] = $this->this_model->convert_form_to_db($v,true);
			}
			$result	= $this->this_view->return_true($data);
			return  $this->this_view->json_show($result);
		}
		
	}