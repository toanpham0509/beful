<?php
	
	date_default_timezone_set('Asia/Ho_Chi_Minh');	
	$application_folder	= "app/";
	
	$core_folder		= $_SERVER['DOCUMENT_ROOT']."/coderland.net/core_1.4/";
	$document_root		= $_SERVER['DOCUMENT_ROOT']."/coderland.net/ligker.net/";
	$default_library	= "default_library_2/";
	
	$base_config["db_info"]		= array(
		"hostname"		=> "localhost"	,
		"db_user"		=> "hoang"		,
		"db_pass"		=> "hoang123"	,
		"db_name" 		=> "hoang_card"	,
		"db_port" 		=> ""
	);
	
	$base_config["item_list"]["labels"]	= array(
		"last_update"		=> "cập nhật"	,
		"media_url"			=> "file_scan"	,
		"create_time"		=> 'create_time'	
	);
	
	$base_config["item_list"]["not_show_list"][1]	= "create_time";
	$base_config["item_list"]["not_show_list"][2]	= "last_update";
	$base_config["item_list"]["not_show_list"][3]	= "status";

	$base_config["root_url"]		= "/projects/card_management/" ;
	
	$base_config["front_end"]		= $application_folder."library/template/front_end_3/";
	$lang	= "en";
	
	
	