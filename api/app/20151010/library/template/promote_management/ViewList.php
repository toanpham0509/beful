{$front_end_url = "{$this_view->config_dir.root_url}{$this_view->config_dir.front_end}"}
<div class="content">
    <div class="nav-ban-hang nav-sale-off">
        <div class="line">
            <h4>Khuyến mại / Giảm giá</h4>
            <div class="search-ban-hang">
                <form method="post" action="" >
                    <img src="{$front_end_url}images/icon_search.png"/>
                    <span class="saleoff icon-search non-display">Tên chương trình Khuyến mại / Giảm giá 
                        <a href="#" id="saleoff" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="start_date icon-search non-display">Ngày bắt đầu 
                        <a href="#" id="start_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="end_date icon-search non-display">Ngày kết thúc 
                        <a href="#" id="end_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a  
					href="javascript:void();" 
					onclick="javascript:
					this.innerHTML = 'loading...';
					ajax_get('{$item_list.root_url}&controller={$item_list.cur_class}&action=item_add_new',
					'right_main');
				">
                    <input                        
						type="button" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Tạo mới"/>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <p style="float: left;">1-1 của 10</p>
                <ul class="page">
                    <li style="border-left: gray 1px solid;;font-weight: bold; border-right: gray 1px solid;"><a href="">&lt;</a></li> 
                    <li style="border-right: gray 1px solid; font-weight: bold;"><a href="">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>Tên chương trình Khuyến mại / Giảm giá 
                            <br/>
                            <a id="saleoff" href="#">
                                <img src="{$front_end_url}images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="saleoff" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày bắt đầu 
                            <br/>
                            <a id="start_date" href="#">
                                <img src="{$front_end_url}images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
            <div class="search-infor date_1 non-display">
                <input type="date" name="start_date"/>
            </div>
            </th>
            <th class="search-order">
                <label> Ngày kết thúc 
                    <br/>
                    <a id="end_date" href="#">
                        <img src="{$front_end_url}images/icon_search.png"/>
                    </a>
                </label>
                <br/>
            <div class="search-infor date_2 non-display">
                <input type="date" name="end_date"/>
            </div>
            </th>
            </tr>
            </thead>
            <tbody>
				{foreach $data.item_list key=item_list_k item=item_list_value}
				<tr>
					{foreach $item_list.fields key=field_k item=field_value}
						{if $field_value.COLUMN_KEY ne "PRI"}
						<td>{$item_list_value.{$field_value.column_name}}</td>
						{/if}
					{/foreach}
				</tr>
				{/foreach}
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/