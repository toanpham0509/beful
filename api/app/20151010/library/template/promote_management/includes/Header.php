<!DOCTYPE html {$front_end_url}public "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>
			{$this_view->translator({$page_class->page_title_1})} - 
			{$this_view->translator({$page_class->page_title_2})}
		</title>
		{$front_end_url = "{$this_view->config_dir.root_url}{$this_view->config_dir.front_end}"}
        <link rel="stylesheet" type="text/css" href="{$front_end_url}public/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="{$front_end_url}public/css/bootstrap-theme.css"/>
        <link rel="stylesheet" type="text/css" href="{$front_end_url}public/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="{$front_end_url}public/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="{$front_end_url}public/css/overight.css"/>
        
        <script type="text/javascript" src="{$front_end_url}public/js/jquery-2.1.4.min.js"></script>
		{include file="{$page_class->config_dir.application_folder}library/template/head.html"}
    </head>
    <body>
        <div class="container-header">
            <div style="margin-left: -40px; ">
                <header>
                    <ul class="header-menu">
                    	<li class="header-active">
							<a 
								href=""
								title="">
								abc
							</a>
						</li>
						<li class="">
							<a 
								href=""
								title="">
								123
							</a>
						</li>
                    </ul>
                    <div class="user_info">
                        <img src="{$front_end_url}images/user.png" class="img-circle"/>
                        <div class="user  dropdown">
                            <div class="dropdown-toggle"> 
                                User Name
                                <span class="caret"></span>
                            </div>
                            <ul class="dropdown-menu">
                                <li><a href="">Thiết Lập Tài Khoản</a></li>
                                <li><a href="">Đăng Xuất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </header>
            </div>
        </div>
		<div class="left-side left-side-warehouse-manage">
			<div class="logo">
	            <img src="{$front_end_url}images/logo.png"/>
	        </div>
		    <ul>
		    	<li class="header-active___">
					<a href="">
						Menu 1
					</a>
					
				</li>
				<li class="header-active">
					<a href="">
						Menu 2
					</a>
				</li>	
		    </ul>
		</div>
		
		<div id="right_main">
		{$position.4}
		</div>
		{include file="{$this_view->config_dir.front_end}includes/Footer.php"}