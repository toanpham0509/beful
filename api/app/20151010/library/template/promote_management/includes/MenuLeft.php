<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="left-side left-side-warehouse-manage">
    <ul>
    	<?php 
    		$menuLeftCurrent = isset( $menuLeftCurrent ) ? $menuLeftCurrent : array();
    		if( isset( $menuLefts ) ) {
    			foreach ( $menuLefts as $menuLeft ) {
    				?>
    				<li class="<?php if( $menuLeftCurrent == $menuLeft ) echo "header-active" ?>">
			        	<a href="<?php
			        		if( strlen( $menuLeft[ 'url' ] ) > 0 ) :
			        			echo System::$config->baseUrl 
					        		. $menuLeft[ 'url' ] 
					        		. System::$config->urlSuffix ;
			        		else : 
			        			echo "#";
			        		endif;
			        		?>">
			        		<?php echo $menuLeft[ 'name' ] ?>
			        	</a></li>
    				<?php
    			}
    		}
    	?>
    </ul>
</div>
<?php
/*end of file MenuLeft.php*/
/*location: MenuLeft.php*/