<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<p style="float: left;">Trang <?php echo $data[ 'page' ] ?> của <?php echo $data[ 'pages' ] ?></p>
	<ul class="page">
       	<li style="border-left: gray 1px solid;font-weight: bold; border-right: gray 1px solid;">
           	<a href="<?php 
				if( $data['page'] > 1 )
					echo $data[ 'dataUrl' ]
						. ($data['page'] - 1)
						. System::$config->urlSuffix;
            	?>">
            	&lt;
            </a>
            </li>
            <?php 
            	for( $i = 1; $i <= $data[ 'pages' ]; $i++ ) {
            	?>
            		<li style="border-left: gray 1px solid; border-right: gray 1px solid;">
		               	<a href="<?php 
		               		echo System::$config->baseUrl 
		                			. $data[ 'dataUrl' ] 
 								. $i
		               			. System::$config->urlSuffix; ?>"
		               		style="<?php if( $data['page'] == $i ) echo "color: red; font-weight: bold;"; ?>">
		               		<?php echo $i; ?>
		                	</a>
		                </li>
            			<?php
            		}
            	?>
            <li style="border-right: gray 1px solid; font-weight: bold;">
              	<a href="<?php 
				if( $data['page'] < $data['pages'] )
					echo System::$config->baseUrl
						. $data[ 'dataUrl' ] 
						. ($data['page'] + 1)
						. System::$config->urlSuffix;
               	?>">
             		&gt;
               	</a>
		</li>
	</ul>
<?php
/*end of file Pagination.php*/
/*location: Pagination.php*/