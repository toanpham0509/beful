$(document).ready(function(e) {
	$("#btnAddProductToOrder").click(function(){
		var productID = $("input#addProduct_ProductID").val();
		//var productAmount = $("input#addProduct_ProductAmount").val();
		$.ajax({
			url: baseUrl + "ajax/product/getProduct/" + productID, 
			dataType:"html", 
			type: "GET",
			data:"", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Mã sản phẩm không tồn tại." );
				} else {
					var html = "<tr>" + $( "#html_add_to_order table tr:first" ).html() + "</tr>";
					$("table.order_line tbody tr:last").before( html );
					$("table.order_line .html_product_name:last").html( data[ 'product_name' ] );
					$("table.order_line input.product_id:last").val( data[ 'product_id' ] );
					$("table.order_line .html_product_id:last").html( data[ 'product_id' ] + "(" + data[ 'product_code' ] + ")" );
					$("table.order_line .html_product_unit:last").html( data[ 'unit_name' ] );
				}
			}
		});
	});
	$("#btnAddProductToPrice").click(function(){
		var html = "<tr>" + $("#html_add_to_order table tr:first").html() + "</tr>";
		$("table.order_line tr:last").before(html);
	});
	$("#order_id").change(function(){
		var orderID = $(this).val();
		var url = baseUrl + "ajax/importOrder/getBuyOrderLineHtml/" + orderID;
		$( "table.order_line" ).html("").load( url );
	});
	$("#sale_order_id").change(function(){
		var orderID = $(this).val();
		var url = baseUrl + "ajax/importOrder/getSaleOrderLineHtml/" + orderID;
		$( "table.order_line" ).html("").load( url );
	});
	$(".don-hang-mua").show();
	$(".don-hang-ban").hide();
	$("#supplier_id").change(function(){
		var supplierID = $(this).val();
		if( supplierID == 27 ) {
			$(".don-hang-ban").show();
			$(".don-hang-mua").hide();
		} else {
			$(".don-hang-ban").hide();
			$(".don-hang-mua").show();
		}
	});
	$("select[name=customer_id]").change(function(){
		var customerID = $( this ).val();
		var url = baseUrl + "ajax/customer/getCustomer/" + customerID;
		$.ajax({
			url: url, 
			dataType: "html", 
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Không tìm kiếm được thông tin khách hàng." );
					$("input[name=address]").val( "" );
					$("input[name=district_id]").val( "" );
					$("input[name=city_id]").val( "" );
					$("input[name=country_id]").val( "" );
				} else {
					$("input[name=address]").val( data['address'] );
					$("input[name=district_id]").val( data['district_name'] );
					$("input[name=city_id]").val( data['city_name'] );
					$("input[name=country_id]").val( data['country_name'] );
				}
			}
		});
	});
	$("select[name=supplier_id]").change(function(){
		var supplierID = $( this ).val();
		var url = baseUrl + "ajax/supplier/getSupplier/" + supplierID;
		$.ajax({
			url: url, 
			dataType: "html", 
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Không tìm kiếm được thông tin nhà cung cấp." );
					$("input[name=supplier_address]").val( "" );
					$("input[name=district_id]").val( "" );
					$("input[name=city_id]").val( "" );
					$("input[name=country_id]").val( "" );
					$("input[name=supplier_phone]").val( "" );
				} else {
					$("input[name=supplier_address]").val( data['address'] );
					$("input[name=district_id]").val( data['district_name'] );
					$("input[name=city_id]").val( data['city_name'] );
					$("input[name=country_id]").val( data['country_name'] );
					$("input[name=supplier_phone]").val( data['phone'] );
				}
			}
		});
	});
});
function deleteOrderLine( object ) {
	if( confirm( 'Bạn có chắc chắn muốn xóa không?' ) ) {
		$(object).parent().parent().remove();
	} else {
		return false;
	}
}

function calTotalPriceItem( object ) {
	var parentObject = $( object ).closest( "tr" );
	var productAmount = $(parentObject).find('input[name="product_amount[]"]').val();
	var productPrice = $(parentObject).find('input[name="product_price[]"]').val();
	var orderLinePrice = productAmount * productPrice;
	$(parentObject).find('.orderLinePrice').html( dauCham(orderLinePrice) );
	calTotalPrice();
}
function calTotalPrice() {
	var total = 0;
	$(".orderLinePrice").each(function(index, element) {
        var value = $(this).text() + "";
		value = parseInt( value.replace( /\./g, '') );
		if( Number.isInteger(value) )
			total +=  value;
    });
	$('.totalPrice').text( dauCham(total) );
}
/**
* Thêm dấu chấm
*/
function dauCham( gia ) {
	var gia = gia + "";
	var stringReturn = "";
	var j = -1;
	for( var i = gia.length - 1; i >= 0 ; i-- ) {
		j++;
		if( j % 3 == 0 && j > 0 ) {
			stringReturn += "." + gia.charAt( i );
		} else {
			stringReturn += gia.charAt( i );
		}
	}
	return stringReturn.split("").reverse().join("");
}