<?php
	class Category_Relation_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["parent_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "category_name"	,
				"table_name"	=> "category"
			);
			$fields["child_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "category_name_1"	,
				"table_name"	=> "cc"
			);
			return $fields;
		}
		
		function fetchAll($sql,$fields,$db_info){
			$sql	= $this->replace_cat_1($sql);
			return parent::fetchAll($sql,$fields,$db_info);
		}
		
		function item_list(){
			$left_join_1	= "category ON category_relation.child_id = category";
			$left_join_2	= "category as cc ON category_relation.child_id = cc";
			$this->item_list_model["left_join"] = str_replace($left_join_1,$left_join_2, 
				$this->item_list_model["left_join"]);
			return parent::item_list();	
		}
		
		function number_rows_pages($sql , $num_rows_pp){
			$sql	= $this->replace_cat_1($sql);
			return parent::number_rows_pages($sql , $num_rows_pp);
		}
		
		function replace_cat_1($sql){
			$sql = str_replace("cc.category_name_1",
					"cc.category_name as category_name_1", $sql);
			return $sql;		
		}
		
		function filter_product_category(){
			$this->item_list_model["filter"]	.= "
				AND category.category_type_id = '2'
			";
		}
}		
		