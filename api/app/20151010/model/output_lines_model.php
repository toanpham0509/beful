<?php
	class Output_Lines_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/	
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["order_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "order_code"	,
				"table_name"	=> "output_order"
			);
			$fields["product_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "product_code"	,
				"table_name"	=> "product"
			);
			$fields["product_tax_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "product_tax_value"	,
				"table_name"	=> "product_tax"
			);
			
			return $fields;
		}
		function output_lines_list($order_id){
			$this->fields[]	= array(
				"column_name"	=> "unit_id"	,
				"table_name"	=> "product"
			);
			$this->fields[]	= array(
				"column_name"	=> "product_name"	,
				"table_name"	=> "product"
			);
			$this->fields[]	= array(
				"column_name"	=> "product_description"	,
				"table_name"	=> "product"
			);
			$this->fields[]	= array(
				"column_name"	=> "order_date"	,
				"table_name"	=> "output_order"
			);
			
			/*
			$this->fields[]	= array(
				"column_name"	=> "product_price"	,
				"table_name"	=> "product_price"
			);
			
			$this->fields[]	= array(
				"column_name"	=> "currency_unit_name"	,
				"table_name"	=> "currency_unit"
			);
			$this->fields[]	= array(
				"column_name"	=> "currency_unit_id"	,
				"table_name"	=> "currency_unit"
			);*/
			$this->fields[]	= array(
				"column_name"	=> "unit_name"	,
				"table_name"	=> "unit"
			);
			$this->fields[]	= array(
				"column_name"	=> "workplace_id"	,
				"table_name"	=> "workplace"
			);
			$this->fields[]	= array(
				"column_name"	=> "workplace_name"	,
				"table_name"	=> "workplace"
			);
			$this->fields[]	= array(
				"column_name"	=> "warehouse_id"	,
				"table_name"	=> "warehouse"
			);
			$this->fields[]	= array(
				"column_name"	=> "warehouse_name"	,
				"table_name"	=> "warehouse"
			);
			$this->item_list_model['left_join']	.="
				<br>LEFT JOIN unit ON product.unit_id = unit.unit_id
				<br>LEFT JOIN workplace ON warehouse.workplace_id = workplace.workplace_id
			";
			$this->item_list_model['filter']	.= "
				AND output_lines.order_id	= '".$order_id."'
			";
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function get_lineId_list_by_order($order_id){
			$fields = array($this->item_list_model["primary_key"]);
			$this->fields	= $this->filter_fields($fields);
			$this->item_list_model['filter']	.= "
				AND output_lines.order_id	= '".$order_id."'
			";
		}
}		
		