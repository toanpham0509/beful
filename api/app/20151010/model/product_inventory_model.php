<?php
	class product_inventory_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->item_list_model["filter"]		= " WHERE 1 = 1";	
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".inventory_date ASC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		
		function filter_warehouse_id($warehouse_id){
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".warehouse_id	= ".$warehouse_id."
			";
		}
		
		function filter_input_line_id($input_line_id){
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".input_line_id	= ".$input_line_id."
			";
		}

		function filter_inventory_date($inventory_date){
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".inventory_date	= ".$inventory_date."
			";
		}	
}		
		