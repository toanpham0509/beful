<?php
	Class Product_Transfer_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Transfer_Model;
			$this->this_view	= new Product_Transfer_View;
			
		}
		
		function new_transfer(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],"transfer_id");
			return $add_new_result;
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
		//	echo $field_name."<br>";
			if($field_name == 'order_date' or $field_name == 'order_code') 
				$args['table_name']	= 'output_order';
			else
				$args['table_name']	= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_equal($args);
			
			return $result;
		}
		
		function transfer_info(){
			if(!empty($_POST['data_post'])) {
				$input = $this->this_model->convert_form_to_db($_POST['data_post'],false) ;
				foreach($input as $k=>$v) if(!empty($v)){
					$this->filter_by_a_field($k,$v);
				}
			}
			$this->this_model->add_output_order_info();
			
			$data	= $this->this_model->item_list();
			$tmp	= $data['item_list'];
				unset($data['item_list']);
				unset($data['fields']);
				unset($data['primary_key']);
			$result	= $data;			
			
			foreach($tmp as $k=>$v){
				//unset($v["intended_date"]);
				$result['item_list'][$k]	= $this->this_model->convert_form_to_db($v,true) ;
			}
			return $result;
		}
		
		function get_transfers(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->transfer_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_transfer(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->transfer_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function update_transfer(){
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->update_an_item($_POST['data_post'],$primary_key);
		}
		
		function delete_transfer(){
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->delete_an_item($_POST['data_post'][$primary_key]);
		}
	}