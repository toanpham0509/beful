<?php
	Class Product_Tax_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Tax_Model;
			$this->this_view	= new Product_Tax_View;
			
		}
		
	}