<?php
	Class Product_Refund_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Refund_Model;
			$this->this_view	= new Product_Refund_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
						
			if($field_name == 'input_date' or $field_name == 'input_order_id')
				$args['table_name']	= 'input_order';
			elseif($field_name == 'order_id')
				$args['table_name']	= 'output_order';	
			elseif($field_name == 'warehouse_id')
				$args['table_name']	= 'warehouse';
			elseif($field_name == 'refund_id')
				$args['table_name']	= 'product_refund';	
			
			if($field_name == 'partner_name'){
				$args['table_name']	= 'partner';
				$this->this_model->filter_like($args);
			}else{
				$this->this_model->filter_equal($args);
			}
						
			return $result;
		}		
		
		function get_refunds(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_refund(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$this->this_model->add_output_order_info();
			$this->this_model->add_customer_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_refund(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_refund(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_refund(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_refundId_by_order($output_line_list){
			$this->this_model->get_refundId_by_order($output_line_list);
			$result = $this->this_model->item_list();
			if(count($result['item_list']) == 0) $result = "false";
			else $result	= "true";
			return $result;
		}
	}