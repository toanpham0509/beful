<?php
	Class Price_List_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Price_List_Model;
			$this->this_view	= new Price_List_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
						
			$args['table_name']	= $this->this_model->item_list_model['table_name'];
			
			if($field_name == 'price_name'){
				$this->this_model->filter_like($args);
			}else{
				$this->this_model->filter_equal($args);
			}
						
			return $result;
		}
		
		function get_prices(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_price(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_price(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_price(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_price(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
	}