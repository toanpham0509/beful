<?php
	Class Employee_Work_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Employee_Work_Model;
			$this->this_view	= new Employee_Work_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
			$args['table_name']	= $this->this_model->item_list_model['table_name'];	
			$this->this_model->filter_equal($args);			
			return $result;
		}
		
		function get_salespeople(){
			$data = $this->this_model->get_salespeople();
			foreach($data['item_list'] as $k=>$v){
			//	echo $k.": ".$v['user_id'].": ".$v['user_name']."<br>";
				$data_2[$k]['salesperson_id']	= $v['user_id'];
				$data_2[$k]['salesperson_name']	= $v['user_name'];
			};
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function get_shop_by_salesperson(){
			$data = $this->this_model->get_shop_by_salesperson($_POST['data_post']["salesperson_id"]);
			foreach($data['item_list'] as $k=>$v){
				$data_2[$k]['shop_id']	= $v['workplace_id'];	
				$data_2[$k]['shop_name']	= $v['workplace_name'];	
			}
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function get_drop_down_list($table_name){
			global $config_dir;
			
			$temp_get	= $_GET;
		//	$_GET["filter_type"] = "shop"; 
			$_GET["controller"]	= $this->get_drop_down_list_class($table_name);			
			$_GET["action"]		= "drop_down_list";
			$_GET["args"]		= $_GET["current_value"];
			include	($config_dir["core_folder"]."main.php");			
			
			$_GET		= $temp_get;
			
			return $main;
		}
		
		function member_work_list(){
			$this->this_model->sort_by_member();
			return parent::item_list();
		}
		
		function item_update($item_id){
			$check_member_work	= $this->this_model->check_member_work($_POST['item_field']);
			if($check_member_work == true) return parent::item_update($item_id);
			else return $this->this_view->not_allow();
		}
		
		function get_user_work_list(){
		//	$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_user_work(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_user_work(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_user_work(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_user_work(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
	}
	
	
	
	
	
	