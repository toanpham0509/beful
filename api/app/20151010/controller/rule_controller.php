<?php
	Class Rule_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Rule_Model;
			$this->this_view	= new Rule_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function rule_list(){
			$result = array('abc'=>123);
			return $result;
		}
		
		function get_rules(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
	}