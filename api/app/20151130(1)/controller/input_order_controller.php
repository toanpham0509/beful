<?php
	Class Input_Order_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Input_Order_Model;
			$this->this_view	= new Input_Order_View;
			
		}
		
		function convert_fields(){
			unset($this->this_model->field_list['input_order']['order_id']);
			$this->this_model->field_list['input_order']['import_id'] = 'input_order_id';
			
			unset($this->this_model->field_list['input_order']['order_date']);
			$this->this_model->field_list['input_order']['import_date'] = 'input_date';
			
			unset($this->this_model->field_list['input_order']['order_code']);
			$this->this_model->field_list['input_order']['import_code'] = 'input_code';
		}
		
		function filter_orders(){
			$this->tmp_filter = $this->this_model->item_list_model['filter'];
			unset($this->this_model->item_list_model["not_show_list"][3]);	
			$trace			= debug_backtrace();
			if($trace[1]['function'] == "get_imports")	{
				$this->convert_fields();
			}		
				
			if(!empty($_POST['data_post'])){				
				if(!empty($_POST['data_post']['order_date_from'])){
					$args["field_name"]	= 'input_date';
					$args["field_value"]= $_POST['data_post']['order_date_from'].";".
						$_POST['data_post']['order_date_to'];
					$args['table_name']	= $this->this_model->item_list_model['table_name'];	
					$this->this_model->filter_between($args);
				}
				unset($_POST['data_post']['order_date_from']) ;
				unset($_POST['data_post']['order_date_to']) ;
					
				if(!empty($_POST['data_post']['supplier_name'])){
					$args["field_name"]		= 'partner_name';
					$args["field_value"]	= $_POST['data_post']['supplier_name'];
					$args['table_name']		= 'partner';
					$this->this_model->filter_like($args);									
				}
				unset($_POST['data_post']['supplier_name']) ;
				
				$data_2 	= $this->this_model->convert_form_to_db($_POST['data_post'],false);
				foreach($data_2 as $k=>$v)
					if(!empty($v)){
					$args["field_name"]	= $k;
					$args["field_value"] = $v;
					$args['table_name']	= $this->this_model->item_list_model['table_name'];
					$this->this_model->filter_equal($args);
				}
			}
			
			if($trace[1]['function'] == "get_buy_orders"){
				$this->this_model->filter_purchase_order();
			}elseif($trace[1]['function'] == "get_imports"){
				$this->this_model->filter_imports();
			}			
			
			$data	= $this->this_model->item_list();
			$tmp	= $data['item_list'];
			unset($data['item_list']);
			unset($data['fields']);
			unset($data['primary_key']);
			$result	= $data;			
			
			foreach($tmp as $k=>$v){
				unset($v["user_id"]);
				unset($v["input_note"]);
			//	unset($v["purchase_order"]);
				$purchase_order = $v["purchase_order"];
				if($purchase_order > 0){
					$v['purchase_order_code'] = $this->get_purchase_order_code($purchase_order);	
				}else{
					$v['purchase_order_code'] = null;
				}
				if($trace[1]['function'] == "get_imports")	{
					unset($v["intended_date"]);
					unset($v["order_status"]);
				}	
				$result['item_list'][$k]	= $this->this_model->convert_form_to_db($v,true) ;
			}
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_buy_orders(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			return $this->filter_orders();
		}
		
		function get_a_order(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$trace			= debug_backtrace();
			if($trace[1]['function'] == 'get_buy_order'){
				$this->this_model->filter_purchase_order();
				$this->this_model->filter_order_id($_POST['data_post']['order_id']);
			}elseif($trace[1]['function'] == 'get_import'){
				$this->this_model->filter_import();     
				$this->this_model->filter_order_id($_POST['data_post']['import_id']);
				$this->convert_fields();
			//	$this->table_properties	= $this->this_model->table_properties();
			//	echo $this->table_properties['item_list_sql'];
			}
			
			
			
			$this->this_model->get_partner_info();
			$this->this_model->get_district_info();
			$this->this_model->get_city_info();
			$this->this_model->get_country_info();
			
			$data	= $this->this_model->item_list();
			foreach($data['item_list'] as $k=>$v){
				unset($v["user_id"]);
				unset($v["purchase_order"]);
				if($trace[1]['function'] == 'get_import'){
					unset($v["intended_date"]);	
				}	
				$data_2[$k]	= $this->this_model->convert_form_to_db($v,true) ;
			}
			
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function get_buy_order(){
			return $this->get_a_order();
		}
		
		function new_order($primary_key){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$primary_key);
			$json	= json_decode($add_new_result,true);
			$new_id	= $json["data"][$primary_key];
			$trace			= debug_backtrace();
			if ($trace[1]['function'] == "new_import"){
				$order_code_text = "PO";
			}elseif ($trace[1]['function'] == "new_buy_order"){
				$order_code_text = "RO";
			}
			$item_fields["input_code"]		= $order_code_text."-".$new_id;
			$this->this_model->item_update($new_id,$item_fields);
			return $add_new_result;
		}
			
		function new_buy_order(){
			unset($_POST['data_post']['purchase_order']);
			return $this->new_order('order_id');
		}
		
		function update_buy_order(){
			unset($_POST['data_post']['purchase_order']);
			return $this->update_an_item($_POST['data_post'],'order_id');
		}
		
		function delete_buy_order(){
			return $this->delete_an_item($_POST['data_post']['order_id']);
		}	

		function get_imports(){
			return $this->filter_orders();
		}

		function get_import(){
			return $this->get_a_order();
		}

		function update_import(){
		//	unset($_POST['data_post']['purchase_order']);
			$this->convert_fields();
			return $this->update_an_item($_POST['data_post'],'import_id');
		}	
		
		function new_import(){
			$_POST['data_post']['purchase_order'] = 0;
			$this->convert_fields();
			return $this->new_order('import_id');
		}
		
		function delete_import(){
			$this->convert_fields();
			return $this->delete_an_item($_POST['data_post']['import_id']);
		}
		
		function get_purchase_order_code($purchase_order){
			$this->this_model->item_list_model['filter'] = $this->tmp_filter;
			$purchase_info = $this->this_model->item_details($purchase_order);
			return $purchase_info['item_list'][0]['input_code'];
		}
	}
	
	