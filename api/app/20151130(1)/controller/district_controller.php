<?php
	Class District_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new District_Model;
			$this->this_view	= new District_View;
			
		}
		
		function get_all_district(){
			$data	= $this->this_model->get_all_district();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
	}