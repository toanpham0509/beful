<?php
	Class Adjust_Type_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Adjust_Type_Model;
			$this->this_view	= new Adjust_Type_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_all_adjust_type(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
	}