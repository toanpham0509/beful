<?php
	Class Inventory_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Inventory_Model;
			$this->this_view	= new Inventory_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_inventories(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_inventory(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_inventory(){
			$inventory_date = $_POST['data_post']['inventory_date'];
			$day = date('d',$inventory_date);
			$month = date('m',$inventory_date);
			$year = date('Y',$inventory_date);			
			$_POST['data_post']['inventory_date'] = mktime(0,0,0,$month,$day,$year);
			$tmp = $_POST['data_post'];
			$opening_stock = $this->get_opening_stock();
			$_POST['data_post'] = $tmp;
			$_POST['data_post']['inventory_amount'] = $opening_stock + $_POST['data_post']['amount'];
			$_POST['data_post']['opening_stock'] 	= $opening_stock; 
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_inventory(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_inventory(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_inventory_amount(){
		//	$next_day = mktime(0,0,0,10,4,2015) + 86400;
			$this->this_model->item_list_model["num_rows_per_page"] = 1;
			return $this->get_inventories();
		}
		
		function get_opening_stock(){   
			unset($_POST['data_post']['order_id']);
			unset($_POST['data_post']['amount']);
			$get_opening_stock = $this->get_inventory_amount();
			$get_opening_stock = json_decode($get_opening_stock,true);
			return $get_opening_stock['data']['item_list'][0]['inventory_amount'];
		}
		
		function get_product_price_import(){
		//	unset($this->this_model->item_list_model["not_show_list"][3]);
			$this->this_model->get_product_price_import();
			$result	= $this->item_info();
			return $result;
			
			//$result	= $this->this_view->return_true($result);
			//return  $this->this_view->json_show($result);
		}
		function get_product_info(){
			$time_start = $_POST["data_post"]["time_start"];
			unset($_POST["data_post"]["time_start"]);
			$time_end = $_POST["data_post"]["time_end"];
			unset($_POST["data_post"]["time_end"]);
			if($time_start != null and $time_end != null)
			$this->this_model->get_product_info($time_start,$time_end);
			$result	= $this->item_info();
			$opening_stock = $result["item_list"][0]['opening_stock'];
			foreach($result["item_list"] as $k=>$v){
				if($v['amount'] > 0 ) $input_amount = $input_amount + $v['amount'];
				elseif($v['amount'] < 0 ) $output_amount = $output_amount + $v['amount'];
				$inventory_amount = $v['inventory_amount'];
			}
			
			$result["item_list"] = array(0=>array(
				'opening_stock'	=> $opening_stock ,
				'input_amount'	=> $input_amount ,
				'output_amount'	=> $output_amount ,
				'inventory_amount'	=> $inventory_amount ,
			));
		//	$this->table_properties	= $this->this_model->table_properties();
		//	echo $this->table_properties["item_list_sql"];
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_inventory_id($product_id,$warehouse_id){//input: product_id,warehouse_id
			$this->this_model->item_list_model["num_rows_per_page"] = 1;
			$result	= $this->item_info();
			$result	= $result['item_list']['0']['inventory_id'];
			return $result;
		}
		function adjust_inventory(){//input: product_id,warehouse_id
			$product_id 	= $_POST["data_post"]["product_id"];
			$warehouse_id	= $_POST["data_post"]["warehouse_id"];
			$new_inventory  = $_POST["data_post"]["inventory_amount"];
			$tmp = $_POST['data_post'];
			unset($_POST['data_post']);
				$inventory_id = $this->get_inventory_id($product_id,$warehouse_id);
				$_POST['data_post']['inventory_id'] 	= $inventory_id;
				$_POST["data_post"]["inventory_amount"]	= $new_inventory;
				$result =  $this->update_inventory();
			$_POST['data_post'] = $tmp;
			return $result;
		}
	}