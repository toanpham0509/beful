<?php
	Class Opening_Stock_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Opening_Stock_Model;
			$this->this_view	= new Opening_Stock_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
				
			/*	
			if($field_name == 'input_date' or $field_name == 'input_order_id')
				$args['table_name']	= 'input_order';
			elseif($field_name == 'order_id')
				$args['table_name']	= 'output_order';	
			*/
			
			$args['table_name']	= $this->this_model->item_list_model['table_name'];
			
			if($field_name == 'partner_name'){
			//	$args['table_name']	= 'partner';
			//	$this->this_model->filter_like($args);
			}else{
				$this->this_model->filter_equal($args);
			}
						
			return $result;
		}
		
		function get_opening_stock_amount_list(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_opening_stock_amount(){
		//	if(!empty($_POST['data_post'][$this->primary_key]))
			return $this->get_opening_stock_amount_list();
		}
		
		function new_opening_stock_amount(){
			unset($_POST['data_post'][$this->primary_key]);
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_opening_stock_amount(){
			$info[$this->primary_key]		= $_POST['data_post'][$this->primary_key];
			$info['opening_stock_amount']	= $_POST['data_post']['opening_stock_amount'];
			unset($_POST['data_post']);
			return $this->update_an_item($info,$this->primary_key);
		}
		
		function delete_opening_stock_amount(){
			$result	= $this->delete_an_item($_POST['data_post'][$this->primary_key]);
			$core_model	= new Core_Model;
			$core_model->item_list_model["table_name"]	
				= $this->this_model->item_list_model["table_name"];
			$core_model->item_list_model["primary_key"]	
				= $this->this_model->item_list_model["primary_key"];	
			$core_model->item_delete($_POST['data_post'][$this->primary_key]);
			return $result;
		}
	}