<?php
	Class Output_Lines_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Output_Lines_Model;
			$this->this_view	= new Output_Lines_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function output_lines_list($order_id){
			$data	= $this->this_model->output_lines_list($order_id);
			return $data;
		}
		
		function update_lines($args){
			$input = null;
			foreach($args['order_lines'] as $k=>$v){
				$input	= $this->this_model->convert_form_to_db($v,$type);
				$input['order_id']	= $args['new_order_id'];
				if($input['order_line_id'] == null) {
					unset($input['order_line_id']);
					$result	.= $this->this_model->item_add_new($input);
				}else{
					$order_line_id	= $input['order_line_id'];
					unset($input['order_line_id']);
					$result	.= $this->this_model->item_update($order_line_id,$input);
				}
			}
			if(!is_numeric($result)) echo $result;
		//	return $result;
			return false;
		}
		
		function delete_lines($args){
			foreach($args as $k=>$v){
			//	echo $k.": ".$v['output_line_id']."<br>";
				$this->item_delete($v['output_line_id']);
			}
		}
		
		function delete_line(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_lineId_list_by_order($order_id){
			$this->this_model->get_lineId_list_by_order($order_id);
			$item_list = $this->this_model->item_list();
			$item_list	= $item_list["item_list"];
			foreach($item_list as $k=>$v){
			//	echo $k.": ".$v["output_line_id"]."<br>";
				if($result == null) $result =  $v["output_line_id"];
				else				$result .=  ", ".$v["output_line_id"];		
			}
			return $result;
		}
	}