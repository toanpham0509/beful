<?php
	Class Order_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Order_Model;
			$this->this_view	= new Order_View;
			
		}
		
		function get_orders(){
			unset($this->this_model->item_list_model["not_show_list"][3]);
			if(isset($_POST['data_post']["time_from"]) or isset($_POST['data_post']["time_to"])){
				if($_POST['data_post']["time_from"] == null) 
					$_POST['data_post']["time_from"] = 0;
				if($_POST['data_post']["time_to"] == null)
					$_POST['data_post']["time_to"]	= 999999999999;
				$_POST['data_post']["timefrom_timeto"]	= $_POST['data_post']["time_from"].";".$_POST['data_post']["time_to"];
				unset($_POST['data_post']["time_from"]);
				unset($_POST['data_post']["time_to"]);
			}
			if(isset($_POST['data_post']["order_price_from"]) or isset($_POST['data_post']["order_price_to"])){
				if($_POST['data_post']["order_price_from"] == null) 
					$_POST['data_post']["order_price_from"] = 0;
				if($_POST['data_post']["order_price_to"] == null)
					$_POST['data_post']["order_price_to"]	= 999999999999;
				$_POST['data_post']["orderpricefrom_orderpriceto"]	= $_POST['data_post']["order_price_from"].";".$_POST['data_post']["order_price_to"];
				unset($_POST['data_post']["order_price_from"]);
				unset($_POST['data_post']["order_price_to"]);
			}
			//print_r($_POST['data_post']) ;
			
			foreach($_POST['data_post'] as $k=>$v)
			if($k != "limit_number_start" and $k != "per_page_number"){				
				if($k == "phone"){
					$table_name	= "partner";
				}else{
					$table_name	= $this->this_model->get_table();
				}
				
				if($k=="order_status"){
					$k = "status_id";
				}elseif($k=="customer_id"){
					$k = "partner_id";
				}
				/*
				elseif($k=="phone"){
					$k = "partner_phone_number";
				}*/				
				
				$args["field_value"]	= $v;
				
				if($k == "timefrom_timeto"){
					$args["field_name"]		= 'order_date';
					$method_name	= "filter_between";						
				}elseif($k == "orderpricefrom_orderpriceto"){
					$args["field_name"]		= 'order_price';
					$method_name	= "filter_between";						
				}elseif($k=="phone"){
					$args["field_name"]		= 'partner_phone_number';
					$method_name	= "filter_like";	
				}else{
					$args["field_name"]		= $k;
					$method_name	= "filter_equal";	
				}
				$args['table_name']	= $table_name;
			//	$filter	.= $this->this_model->$method_name($args);
				$this->this_model->$method_name($args);
			}
			
			if(!empty($_POST['data_post']["per_page_number"])) {
				$args['num_rows_per_page'] = $_POST['data_post']["per_page_number"];
			}else{
				$args['num_rows_per_page']	= $this->this_model->item_list_model["num_rows_per_page"];	
			}
			
			if(!empty($_POST['data_post']["limit_number_start"])) {
				$args["pp"] = $_POST['data_post']["limit_number_start"];
			}else{
				$args["pp"]	= $this->this_model->item_list_model["current_page"];	
			}
			
		//	$args['filter']	= $filter;
			$data	= $this->this_model->get_orders($args);
			foreach($data['item_list'] as $k=>$v){
				$data_2[$k]['order_id']	= $v['order_id'];
				$data_2[$k]['order_code']	= $v['order_code'];
				$data_2[$k]['order_date']	= $v['order_date'];
				$data_2[$k]['customer_name']	= $v['partner_name'];
				$data_2[$k]['salesperson_name']	= $v['user_name'];
				$data_2[$k]['order_price']	= $v['order_price'];
				$data_2[$k]['order_status']	= $v['status'];
				$data_2[$k]['discount']	= $v['discount'];
				$data_2[$k]['discount_amount']	= $v['discount_amount'];
				$data_2[$k]['product_refund']	= $this->check_refund($v['order_id']);
			}
			$data_2['order_count']	= $data['num_rows_total'];
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function get_order(){
			$order_id	= $_POST['data_post']['order_id'];
			unset($this->this_model->item_details_model["do_not_show_details"]['list'][3]);
			$data	= $this->this_model->get_order($order_id);			
			if($data['item_list'] == null){
				$info['errors']	= array('order_id');
				$info['message']= array('not found order_id = '.$order_id);
				$result	= $this->this_view->return_false($info);
			}else{
				$get_output_lines_list	= $this->get_output_lines_list($order_id);		
				foreach($get_output_lines_list['item_list'] as $k=>$v){
					$output_lines_list[$k]['order_line_id']	= $v['output_line_id'];
					$output_lines_list[$k]['product_id']	= $v['product_id'];
					$output_lines_list[$k]['product_name']	= $v['product_name'];
					$output_lines_list[$k]['product_amount']	= $v['output_line_amount'];
					
					$output_lines_list[$k]['unit_id']	= $v['unit_id'];
					$output_lines_list[$k]['product_unit']	= $v['unit_name'];
					
					$output_lines_list[$k]['product_price']	= $v['product_price'];
					$output_lines_list[$k]['product_tax_id']	= $v['product_tax_id'];
					$output_lines_list[$k]['product_discount']	= $v['product_discount'];
					
					$output_lines_list[$k]['currency_unit_id']	= $v['currency_unit_id'];
					$output_lines_list[$k]['currency_unit_name']	= $v['currency_unit_name'];
					
					$output_lines_list[$k]['warehouse_id']	= $v['warehouse_id'];
					$output_lines_list[$k]['warehouse_name']	= $v['warehouse_name'];
					
					$output_lines_list[$k]['output_line_id']	= $v['output_line_id'];
					$output_lines_list[$k]['product_tax_value']	= $v['product_tax_value'];
					
				//	$output_lines_list[$k]['order_code']	= $v['order_code'];
					
				}
				$data_2[0] = array(
					'order_code'		=> $data['item_list'][0]['order_code']   ,
					
					'shop_id'			=> $data['item_list'][0]['shop_id']   ,
					'shop_name'			=> $data['item_list'][0]['workplace_name']   ,
					
					'customer_id'		=> $data['item_list'][0]['partner_id']   ,
					'customer_name'		=> $data['item_list'][0]['partner_name']   ,
					
					'address'			=> $data['item_list'][0]['partner_address']   ,
					
					'district_id'		=> $data['item_list'][0]['district_id']   ,
					'district_name'		=> $data['item_list'][0]['district_name']   ,
					
					'city_id'			=> $data['item_list'][0]['city_id']   ,
					'city_name'			=> $data['item_list'][0]['city_name']   ,
					
					'country_id'		=> $data['item_list'][0]['country_id']   ,
					'country_name'		=> $data['item_list'][0]['country_name']   ,
					
					'order_date'		=> $data['item_list'][0]['order_date']   ,
					'price_id'			=> $data['item_list'][0]['quotation_id']   ,
					
					'salesperson_id'	=> $data['item_list'][0]['salesperson_id']   ,
					'salesperson_name'	=> $data['item_list'][0]['user_name']   ,
					
					'order_status_id'	=> $data['item_list'][0]['status_id']   ,
					'order_status_name'	=> $data['item_list'][0]['status_name']   ,
					
					'order_price'		=> $data['item_list'][0]['order_price']   ,
					'order_lines'		=> $output_lines_list  ,
					'order_note'		=> $data['item_list'][0]['output_note']   ,
					'promote_id'		=> $data['item_list'][0]['promote_id']   ,
					'status'		=> $data['item_list'][0]['status']   ,
					'discount'		=> $data['item_list'][0]['discount']   ,
					'discount_amount'		=> $data['item_list'][0]['discount_amount']   ,
					'phone'		=> $data['item_list'][0]['partner_phone_number']   ,
				);
			
				if(isset($_POST['data_post']['item_edit'])){
					$data	= $this->add_drop_down_list($data);
					$data_2[0]['drop_down_lists']	= $data['item_list'][0]["drop_down_list"];
				}
					
				$result	= $this->this_view->return_true($data_2);
			}
			
		//	$data_2[]	= $get_output_lines_list['item_list'];
			
			
			/*
			$tt= $this->this_view->json_show($result);
			echo "<pre>";
			print_r(json_decode($tt));
			echo "</pre>";
			*/
			return  $this->this_view->json_show($result);
		}
		
		function get_output_lines_list($order_id){
			global $config_dir;
			
			$temp_get	= $_GET;
			
			$_GET["controller"]	= 'output_lines';			
			$_GET["action"]		= "output_lines_list";
			$_GET["args"]		= $order_id;
			include	($config_dir["core_folder"]."main.php");			
			
			$_GET		= $temp_get;
			
			return $main;
		}
		
		function get_all_price(){
			$data	= $this->this_model->get_all_price();
			foreach($data['item_list'] as $k=>$v){
				$data_2[$k]['price_id']	= $v['order_id'];
				$data_2[$k]['price_name']	= $v['order_name'];
			}
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function new_order(){
			if($_POST['data_post']['quotation_id']==null) $_POST['data_post']['quotation_id'] =0;
			$input			= $this->this_model->convert_form_to_db($_POST['data_post'],$type);
			$new_order_id	= $this->this_model->item_add_new($input);
			if(is_numeric($new_order_id)){
				$shop_code	= $this->get_shop_code($_POST['data_post']['shop_id']);
				$new_order_code	= $shop_code."-".$new_order_id;
				$order_code	= array("order_code" =>  $new_order_code);
				$this->this_model->item_update($new_order_id,$order_code);
				$args['order_lines']	= $_POST['order_lines'];
				$args['new_order_id']	= $new_order_id;
				$update_lines			= $this->update_lines($args);
				$data	= array(
					'order_id' => $new_order_id	,
					'order_code' => $new_order_code
				);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($new_order_id);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function update_lines($args){
			global $config_dir;			
			$temp_get	= $_GET;			
			$_GET["controller"]	= 'output_lines';			
			$_GET["action"]		= "update_lines";
			$_GET["args"]		= $args;
			include	($config_dir["core_folder"]."main.php");			
			$_GET		= $temp_get;			
			return $main;
		}
		
		function update_order(){
			unset($_POST['data_post']['quotation_id']);
			if($_POST['data_post']['shop_id'] == null){
				unset($_POST['data_post']['order_code']);
			}else{
				$shop_code	= $this->get_shop_code($_POST['data_post']['shop_id']);
				if($shop_code == null) {
					$info['errors']	= array('shop_id');
					$info['message']= array('shop_id not found');
					$result	= $this->this_view->return_false($info);
					return  $this->this_view->json_show($result);	
				//	unset($_POST['data_post']['order_code']);
				}				
				else
					$_POST['data_post']['order_code']	= 
					$shop_code."-".$_POST['data_post']['order_id'];
			}
			
			
			
			$input			= $this->this_model->convert_form_to_db($_POST['data_post'],$type);			
			$args['new_order_id']	= $_POST['data_post']['order_id'];
			unset($input['order_id']);
			$result	= $this->this_model->item_update($args['new_order_id'],$input);
			if($result	== "0" ){
				$args['order_lines']	= $_POST['order_lines'];
				$result	.= $this->update_lines($args);
			}
		//	echo $result."<<<";
			if($result	== "0" ){
				$data	= array('order_id' => $args['new_order_id']);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($result);
				$result	= $this->this_view->return_false($info);
			}
			return  $this->this_view->json_show($result);
			/*
			echo "<pre>";
				print_r($input);
			echo "</pre>";
			echo "<hr>";
			echo "<pre>";
				print_r($_POST['order_lines']);
			echo "</pre>";
			*/
		}
		
		function delete_order(){
			$order_id	= $_POST['data_post']['order_id'];
			$get_output_lines_list	= $this->get_output_lines_list($order_id);
			$this->delete_lines($get_output_lines_list['item_list']);
		//	$args['new_order_id']	= $order_id;
			$result	= $this->this_model->item_delete($order_id); 
			$result	= $this->this_view->return_true(null);
			return  $this->this_view->json_show($result);
		}
		
		function delete_lines($args){
			global $config_dir;			
			$temp_get	= $_GET;			
			$_GET["controller"]	= 'output_lines';			
			$_GET["action"]		= "delete_lines";
			$_GET["args"]		= $args;
			include	($config_dir["core_folder"]."main.php");			
			$_GET		= $temp_get;			
			return $main;
		}
		
		function get_prices(){
			if(!empty($_POST['data_post']["price_name"])){
				$this->this_model->filter_price_name($_POST['data_post']["price_name"]);
			}
			$data	= $this->this_model->get_prices();	
			foreach($data['item_list'] as $k=>$v){
				$data_2['item_list'][$k]['price_id']	= $v['order_id'];	
				$data_2['item_list'][$k]['price_name']	= $v['order_name'];	
				$data_2['item_list'][$k]['time_start']	= $v['time_start'];	
				$data_2['item_list'][$k]['time_end']	= $v['time_end'];	
			}
			
			$data_2['num_rows_total']	= $data['num_rows_total'];
			$data_2['num_pages']	= $data['num_pages'];
			$data_2['num_rows_show']	= $data['num_rows_show'];
			
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}	
		
		function get_price(){
			$price_id	= $_POST['data_post']["price_id"];
			$data	= $this->this_model->get_price($price_id);			
			if($data['item_list'] == null){
				$info['errors']	= array('price_id');
				$info['message']= array('not found price_id = '.$price_id);
				$result	= $this->this_view->return_false($info);
			}else{
				$get_output_lines_list	= $this->get_output_lines_list($price_id);	
				foreach($get_output_lines_list['item_list'] as $k=>$v){
					$output_lines_list[$k]['order_line_id']	= $v['output_line_id'];
					$output_lines_list[$k]['product_id']	= $v['product_id'];
					$output_lines_list[$k]['product_name']	= $v['product_name'];
					$output_lines_list[$k]['product_price']	= $v['product_price'];
					$output_lines_list[$k]['product_note']	= $v['product_description'];
				}	
				
				if($data['item_list'][0]['status_id'] == 6){
					$validity	= 1;
				}elseif($data['item_list'][0]['status_id'] == 7){
					$validity	= 0;
				}
				
				$data_2[0] = array(
					'price_code'		=> $data['item_list'][0]['order_code']   ,
					'price_name'		=> $data['item_list'][0]['order_name']   ,
					'validity'		=> $validity   ,
					'time_start'		=> $data['item_list'][0]['time_start']   ,
					'time_end'		=> $data['item_list'][0]['time_end']   ,
					
					'products'		=> $output_lines_list  ,
				);
				$result	= $this->this_view->return_true($data_2);
			}
			return  $this->this_view->json_show($result);
		}
	
		function convert_price_form_to_db(){
			$tmp	= $_POST['data_post'];
			if($tmp['validity'] == 0) $tmp['validity']	= 7;
			elseif($tmp['validity'] == 1) $tmp['validity']	= 6;
			unset($_POST['data_post']);
			if(!empty($tmp['price_id']))
				$_POST['data_post']['order_id']	= $tmp['price_id'];
			$_POST['data_post']['order_name']	= $tmp['price_name'];
			$_POST['data_post']['status_id']		= $tmp['validity'];
			$_POST['data_post']['time_start']	= $tmp['time_start'];
			$_POST['data_post']['time_end']	= $tmp['time_end'];
		}
		
		function new_price(){
			$this->convert_price_form_to_db();
			return $this->new_order();
		}
		
		function update_price(){
			$this->convert_price_form_to_db();
			return $this->update_order();
		}
		
		function delete_price(){
			$_POST['data_post']['order_id']	= $_POST['data_post']['price_id'];;
			return $this->delete_order();
		}
		
		function get_shop_code($shop_id){
			global $config_dir;
			$temp= $_GET;
				$_GET['controller']	= "workplace";
				$_GET['action']	= "get_shop_code";
				$_GET['args']	= $shop_id;
				include($config_dir['core_folder']."main.php");
			$_GET	= $temp;
			return $main;	
		}
		
		function report_sale_line(){
			$this->this_model->report_sale_line_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function check_refund($order_id){
			$args_lines['controller']	= 'output_lines';
			$args_lines['action']		= 'get_lineId_list_by_order';
			$args_lines['args']		= $order_id;			
			if($this->call_controller($args_lines) != null){
				$args_refund['controller']	= 'product_refund';
				$args_refund['action']		= 'get_refundId_by_order';
				$args_refund['args']	= "(".$this->call_controller($args_lines).")";		
				$check_refund			= $this->call_controller($args_refund);
			}else{
				$check_refund = "false";
			}
			return $check_refund;
		}
	}
	
	
	
	