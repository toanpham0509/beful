<?php
//	include($config_dir["core_folder"]."controller/image_class.php") ;
	Class Base_Controller extends Core_Controller{
		public function __construct() {		
			parent::__construct();
	//		$this->media = new image_class;
			foreach($_POST['data_post'] as $k=>$v){
			//	if($v == null) unset($_POST['data_post'][$k]);	
			}
			foreach($_POST['order_lines'] as $k=>$v){
				foreach($v as $k2=>$v2)
				if($v2 == null) unset($_POST['order_lines'][$k][$k2]);	
			}
		}
		function get_drop_down_list_class($table_name){
			return $this->this_model->get_class_from_table_name($table_name);
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
			$args['table_name']	= $this->primary_key	= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_equal($args);
			return $result;
		}
		
		function home_show(){
			return true;
		}
		
		function update_an_item($input,$primary_key){
			$item_value	= $input[$primary_key];
			unset($input[$primary_key]);
			
			$input	= $this->this_model->convert_form_to_db($input,null);
			$update_result	= $this->this_model->item_update($item_value,$input);
			if(is_numeric($update_result)){
				$data	= array($primary_key => $item_value);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($update_result);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function delete_an_item($item_id){
		//	$item_id	= $_POST['data_post']['product_id'];
			$result	= $this->this_model->item_delete($item_id); 
			$result	= $this->this_view->return_true(null);
			return  $this->this_view->json_show($result);
		}
		
		function new_an_item($input,$primary_key){
			/*echo "<pre>";
			print_r($_POST['data_post']);
			echo "</pre>";
			*/
			$input	= $this->this_model->convert_form_to_db($input,null);
			$new_item_id	= $this->this_model->item_add_new($input);
			if(is_numeric($new_item_id)){
				$data	= array($primary_key => $new_item_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($new_item_id);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		
		
		function item_info(){
			if(!empty($_POST['data_post'])) {
				$input = $this->this_model->convert_form_to_db($_POST['data_post'],false) ;
				foreach($input as $k=>$v) if(!empty($v)){
					$this->filter_by_a_field($k,$v);
				}
			}
		//	$this->this_model->add_output_order_info();
			
			$data	= $this->this_model->item_list();
			$tmp	= $data['item_list'];
				unset($data['item_list']);
				unset($data['fields']);
				unset($data['primary_key']);
			$result	= $data;			
			/*
			if(count($tmp) == 0) {
				$tmp["data"] = array(
					"num_rows_total" 	=> 0,
					"num_pages"			=> 0,
					"num_rows_show"		=> 0,
					"item_list"			=> null
				);
			}*/
			foreach($tmp as $k=>$v){
				//unset($v["intended_date"]);
				$result['item_list'][$k]	= $this->this_model->convert_form_to_db($v,true) ;
			}
			return $result;
		}
		
		function get_a_field($args){
			$get_field	= $args['get_field'];
			$city_details = $this->this_model->item_details($args['item_id']);
			return $city_details['item_list'][0][$get_field];
		}
		
		function get_dropdown_list($field_value){
			if(!empty($field_value)){
				$args["field_name"]		= $_GET['filter_by'];
				$args["field_value"]	= $field_value;
				$args['table_name']		= $this->this_model->item_list_model['table_name'];
				$this->this_model->filter_equal($args);
			}
			$data	= $this->this_model->item_list(); 
			return $this->this_view->get_dropdown_list($data['item_list'],
			$this->this_model->item_list_model['primary_key'],$_GET["field_name"]);
		}
		
		function call_controller($args){
			global $config_dir;
			$temp= $_GET;
				$_GET['controller']	= $args['controller'];
				$_GET['action']		= $args['action'];
				$_GET['args']		= $args['args'];
				include($config_dir['core_folder']."main.php");
			$_GET	= $temp;
			return $main;	
		}
		
		function inspection_order(){
			$primary_key	= $this->this_model->item_list_model['primary_key'];
			$input = array(
				$primary_key => $_POST['data_post'][$primary_key]	,
				'status' => 3 
			);
			$this->update_inventory();
			return $this->update_an_item($input,$primary_key);
		}
		
		function update_inventory(){
			$args['controller'] = 'output_lines';
			$args['action']		= 'output_lines_list';
			$args['args']		= $_POST['data_post']['order_id'];
			$line_list = $this->call_controller($args);
			
			foreach($line_list['item_list'] as $k=>$v){
				unset($_POST['data_post']);
				$_POST['data_post']['inventory_date'] = $v['order_date'];
				$_POST['data_post']['order_id'] 		= $v['order_id'];
				$_POST['data_post']['product_id'] 	= $v['product_id'];
				$_POST['data_post']['amount'] 		= -$v['output_line_amount'];
				$_POST['data_post']['warehouse_id'] 	= $v['warehouse_id'];  
				$args['controller'] = 'inventory';
				$args['action']		= 'new_inventory';
				$this->call_controller($args);
			}
		}
	}  
	
	
	
	
	
	
	