<?php
	Class Product_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Model;
			$this->this_view	= new Product_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		
		function get_products() {
			if(!empty($_POST['data_post']['warehouse_id'])){
				$this->warehouse_id = $_POST['data_post']['warehouse_id'];
			//	$this->this_model->filter_warehouse($warehouse_id);
				unset($_POST['data_post']['warehouse_id']);
			}else{
				$this->warehouse_id = null;
			}
			if(!empty($_POST['data_post']['price_id'])){
				$this->price_id = $_POST['data_post']['price_id'];
				unset($_POST['data_post']['price_id']);
				/*
				$args['controller'] = 'product_price';
				$args['action']		= 'get_product_id';
				$args['args']		= $price_id;
				$product_id_list = $this->call_controller($args);
				if($product_id_list == null) $product_id_list = 0;
				$this->this_model->filter_price($product_id_list);
				*/
			}else{
				$this->price_id = null;
			}
			if(!empty($_POST['data_post']['inventory_date'])){
				$inventory_date = $_POST['data_post']['inventory_date'];
				$this->this_model->filter_inventory_date($inventory_date);
				unset($_POST['data_post']['inventory_date']);
			}			
			
		//	$this->this_model->get_products();
			$result	= $this->item_info();
		//	echo "<pre>";
				
			foreach($result["item_list"] as $k=>$v){
				$result["item_list"][$k]['product_price'] 
					=  $this->get_product_price($v["product_id"]);
				$result["item_list"][$k]['inventory_amount'] 
					=  $this->get_inventory_amount($v["product_id"]);	
			}
				
		//	print_r($result["item_list"]);
		//	echo "</pre>";
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_products____() {
			$input			= $this->this_model->convert_form_to_db($_POST['data_post'],$type);
			if(!empty($_POST['data_post']['warehouse_id'])){
				$warehouse_id = $_POST['data_post']['warehouse_id'];
				$this->this_model->filter_warehouse($warehouse_id);
			}
			if(!empty($_POST['data_post']['price_id'])){
				$price_id = $_POST['data_post']['price_id'];
				$this->this_model->filter_price($price_id);
			}
			if(!empty($_POST['data_post']['inventory_date'])){
				$inventory_date = $_POST['data_post']['inventory_date'];
				$this->this_model->filter_inventory_date($inventory_date);
			}
			$data	= $this->this_model->get_products($input);
			unset($data['primary_key']);
			unset($data['fields']);
			$result	= $this->this_view->return_true($data);
			return  $this->this_view->json_show($result);
		}	
		
		function new_product(){
			/*echo "<pre>";
			print_r($_POST['data_post']);
			echo "</pre>";
			*/
			$input	= $this->this_model->convert_form_to_db($_POST['data_post'],null);
			$new_item_id	= $this->this_model->item_add_new($input);
			if(is_numeric($new_item_id)){
				$item_fields["product_code"]		= "P-".$new_item_id;
				$this->this_model->item_update($new_item_id,$item_fields);
				$data	= array(
					'new_product' 	=> $new_item_id	,
					'product_code'	=> 	$item_fields["product_code"]
				);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($new_item_id);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function update_product(){
			$product_id	= $_POST['data_post']['product_id'];
			unset($_POST['data_post']['product_id']);
			
			$input	= $this->this_model->convert_form_to_db($_POST['data_post'],null);
			$update_result	= $this->this_model->item_update($product_id,$input);
			if(is_numeric($update_result)){
				$data	= array('product_id' => $product_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($update_result);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function delete_product(){
			$product_id	= $_POST['data_post']['product_id'];
			$result	= $this->this_model->item_delete($product_id); 
			$result	= $this->this_view->return_true(null);
			return  $this->this_view->json_show($result);
		}
		
		function get_product(){
			$product_id	= $_POST['data_post']['product_id'];
			$this->this_model->filter_product_id($product_id);
			$data	= $this->this_model->item_list();
		//	unset($data['primary_key']);
		//	unset($data['fields']);
			foreach($data['item_list'] as $k=>$v){
				$result[$k]	= $this->this_model->convert_form_to_db($v,true); 
			}
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_all_product(){
			$this->this_model->get_all_product_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_list_products(){
			$this->this_model->get_list_products_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_product_price($product_id){
			$tmp = $_POST['data_post'];
				unset($_POST['data_post']);
				$args['controller'] = 'product_price';
				$args['action']		= 'get_price_line';
				$args['args']		= "";
				
				$_POST['data_post']['product_id'] = $product_id;
				if($this->price_id != null)
					$_POST['data_post']['price_list_id'] = $this->price_id;
				$product_price = $this->call_controller($args);
			$_POST['data_post']	= $tmp;
			
			$product_price = json_decode($product_price,true);
			return $product_price["data"][0]["product_price"];
		}
		
		function get_inventory_amount($product_id){
			$tmp = $_POST['data_post'];
				unset($_POST['data_post']);
				$args['controller'] = 'inventory';
				$args['action']		= 'get_inventory';
				$args['args']		= "";
				
				$_POST['data_post']['product_id'] = $product_id;
				if($this->warehouse_id != null)
					$_POST['data_post']['warehouse_id'] =  $this->warehouse_id;
				$product_price = $this->call_controller($args);
			$_POST['data_post']	= $tmp;
			
			$product_price = json_decode($product_price,true);
			if($product_price["data"][0]["inventory_amount"] == null)
				$product_price["data"][0]["inventory_amount"] = 0;
			return $product_price["data"][0]["inventory_amount"];
		}
	}