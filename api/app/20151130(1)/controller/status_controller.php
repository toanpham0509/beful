<?php
	Class Status_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Status_Model;
			$this->this_view	= new Status_View;
			
		}
		function get_all_order_status(){
			$data	= $this->this_model->get_all_order_status();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function get_product_status(){
			$data	= $this->this_model->get_product_status();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
	}