<?php
	Class Promote_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Promote_Model;
			$this->this_view	= new Promote_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_promotes(){
		//	$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function promote_list(){
			return $this->item_list();
		}
		
		function get_promote(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_promote(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_promote(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_promote(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
	}