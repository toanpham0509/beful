<?php
	class Employee_Work_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
			$this->item_list_model["not_show_list"][]	= "start_workdate";
			$this->item_list_model["not_show_list"][]	= "end_workdate";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
			$this->item_details_model["do_not_show_details"]["list"][] = "start_workdate";
			$this->item_details_model["do_not_show_details"]["list"][] = "end_workdate";
			
			$this->item_list_model['filter']	.="
				<br>AND (".$this->item_list_model['table_name'].".end_workdate	> '".time()."'
				<br>OR ".$this->item_list_model['table_name'].".end_workdate	= 0
				)
			";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["position_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "position_name"	,
				"table_name"	=> "position"
			);
			$fields["user_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "user_name"	,
				"table_name"	=> "user"
			);
			$fields["workplace_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "workplace_name"	,
				"table_name"	=> "workplace"
			);
			return $fields;
		}
		function get_salespeople(){
			$fields = array("user_id");
			$this->fields	= $this->filter_fields($fields);
			$this->fields[]	= array(
				"column_name"	=> "user_name"	,
				"table_name"	=> "user"	
			);
		//	echo $this->item_list_model["table_name"];
			$this->item_list_model["filter"]	.="
				AND ".$this->item_list_model["table_name"].".position_id = 5
				AND ".$this->item_list_model["table_name"].".end_workdate < '".time()."'
			" ;
		//	print_r($this->item_list_model["left_join"]);
			return $this->item_list();
		}
		function get_shop_by_salesperson($salesperson_id){
			$fields	= array('workplace_id');
			$this->fields	= $this->filter_fields($fields);
			$this->fields[]	= array(
				'column_name'	=>	'workplace_name'	,
				'table_name'	=>	'workplace'	,
			);
			$this->item_list_model['filter']	.="
				<br>AND ".$this->item_list_model['table_name'].".user_id = '".$salesperson_id."'
				<br>AND workplace.category_id	= 15
			";
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}	
		
		function check_member_work($item_field) {
				$this->item_list_model['filter'] .= "
				AND ".$this->item_list_model["table_name"].".user_id 	= '".$item_field['user_id']."'
				AND ".$this->item_list_model["table_name"].".position_id 	= '".$item_field['position_id']."'
				AND ".$this->item_list_model["table_name"].".workplace_id 	= '".$item_field['workplace_id']."'
			";
			$item_list	= $this->item_list();
		//	print_r($_POST);
			$result	= count($item_list['item_list']);
			
			if($result == 0){
				$result = true;
			}else{
				if(!empty($_POST['args'])){
					$pk= $this->item_list_model['primary_key'];
					if($_POST['args'] == $item_list['item_list'][0][$pk]){
						$result = true;
					} else{
						$result = false;
					}
				}else{
					$result = false;
				}
			}
			
			
			return $result;
		}
		
		function sort_by_member(){
			$this->item_list_model["sort"]			= " ORDER BY workplace.workplace_name ASC ,
				position.position_id ASC ,".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		}
}		
		