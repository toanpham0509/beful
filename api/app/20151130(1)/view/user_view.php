 <?php
class User_View extends Base_View{
    public function __construct(){
        parent::__construct();
		
		$this->item_list_view["table_title"] = "member_list";
		$this->form_field['district_id']["view_file"] = $this->config_dir[front_end]."district_field.html";
	}
	
	function show_module($position,$data){
		$this->smarty->assign($position,$data);
	}
	
	function login_form($status){
		//echo $this->config_dir["default_library"].'images/loading_facebook.gif';
		$this->smarty->assign('args',$status) ;
		$view_file	= $this->config_dir[default_template_folder][dir_document_root]."login_frm.html";	
		return $this->smarty->fetch($view_file);
	}
	
	function user_panel($user_info){
		$this->smarty->assign('user',$user_info);
		$view_file	= $this->config_dir[default_template_folder][dir_document_root]."user_panel.html";		
		return $this->smarty->fetch($view_file);
	}
} 





