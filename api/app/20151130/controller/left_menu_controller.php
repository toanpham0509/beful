<?php
	Class Left_Menu_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Left_Menu_Model;
			$this->this_view	= new Left_Menu_View;
		}
		
		function get_controller_list($args){
			$data	= $this->this_model->$args();
			return $this->this_view->get_controller_list($data);
		}
		
	}