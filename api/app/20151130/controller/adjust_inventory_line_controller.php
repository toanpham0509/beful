<?php
	Class Adjust_Inventory_Line_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Adjust_Inventory_Line_Model;
			$this->this_view	= new Adjust_Inventory_Line_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_adjust_inventory_lines(){
			$this->this_model->add_fields();
			$result	= $this->item_info();
			foreach($result['item_list'] as $k=>$v){				
				$result['item_list'][$k]['inventory_amount'] = $this->get_inventory_amount($v);
			}
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		/*
		function get_adjust_inventory(){
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}*/
		
		function new_adjust_inventory_line(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_adjust_inventory_line(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_adjust_inventory_line(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_inventory_amount($input){
			$tmp = $_POST;
				$_POST['data_post']['product_id'] 		= $input['product_id'];
				$_POST['data_post']['warehouse_id'] 	= $input['warehouse_id'];  
				$args['controller'] = 'inventory';
				$args['action']		= 'get_inventory_amount';
				$result= 	$this->call_controller($args);
			$_POST = $tmp;
			$result = json_decode($result,true);
			if($result[data][item_list][0][inventory_amount] == null) $result = 0;
			else
				$result = $result[data][item_list][0][inventory_amount];
			return $result;
		}
	}