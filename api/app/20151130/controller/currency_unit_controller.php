<?php
	Class Currency_Unit_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Currency_Unit_Model;
			$this->this_view	= new Currency_Unit_View;
			
		}
		
	}