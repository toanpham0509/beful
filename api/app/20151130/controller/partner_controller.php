<?php
	Class Partner_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Partner_Model;
			$this->this_view	= new Partner_View;
			
		}
		
		function update_customer(){
			return $this->update_an_item($_POST['data_post'],'customer_id');
		}
		
		function delete_customer(){
			return $this->delete_an_item($_POST['data_post']['customer_id']);
		}
		
		function add_new_partner_category($new_partner_id,$category_id){
			global $config_dir;
			$temp_get	= $_GET;
			$_GET["controller"]	= "partner_category";			
			$_GET["action"]		= "new_partner";
			$_GET["args"]		= array(
				'partner_id'	=> $new_partner_id	,
				'category_id'	=> $category_id
			);
			include	($config_dir["core_folder"]."main.php");			
			$_GET		= $temp_get;
			return $main;
		}
		
		function new_partner($primary_key){
			$trace			= debug_backtrace();
			if($trace[1]['function'] 	== 'new_customer') {
				$category_id		= 3;	
			}
			elseif($trace[1]['function'] == 'new_supplier') {
				$category_id	= 2;
				$_POST['data_post']['partner_name'] = $_POST['data_post']['supplier_name'];
				unset ($_POST['data_post']['supplier_name']);	
			}
			
			$add_new_result		= $this->new_an_item($_POST['data_post'],$primary_key);
			$json 				= json_decode($add_new_result,true);
			$new_partner_id	= $json['data'][$primary_key];
			if($new_partner_id > 0){				
				$this->add_new_partner_category($new_partner_id , $category_id);
			}
			return $add_new_result;
		}
		
		function new_customer(){
			return $this->new_partner('customer_id');
		}
		
		function new_supplier(){
			return $this->new_partner('supplier_id');
		}
		
		function update_supplier(){
			$_POST['data_post']['partner_name'] = $_POST['data_post']['supplier_name'];
			unset ($_POST['data_post']['supplier_name']);
			return $this->update_an_item($_POST['data_post'],'supplier_id');
		}
		
		function delete_supplier(){
			return $this->delete_an_item($_POST['data_post']['supplier_id']);
		}
	}