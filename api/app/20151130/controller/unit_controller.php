<?php
	Class Unit_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Unit_Model;
			$this->this_view	= new Unit_View;
			
		}
		
		function get_product_units(){
			if(!empty($_POST['data_post']['unit_name'])){
				$args["field_name"]		= 'unit_name';
				$args["field_value"] 	= $_POST['data_post']['unit_name'];
				$args['table_name']	= $this->this_model->item_list_model['table_name'];
				$this->this_model->filter_like($args);
			}			
			$data	= $this->this_model->get_product_units();
			foreach($data["item_list"] as $k=>$v){
				//unset($v["category_name"]);
				$output[$k]			= $this->this_model->convert_form_to_db($v,1);	
			}
			$result	= $this->this_view->return_true($output);
			return  $this->this_view->json_show($result);
		}
		
		function get_product_unit(){
			$args["field_name"]		= 'unit_id';
			$args["field_value"] 	= $_POST['data_post']['unit_id'];
			$args['table_name']	= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_like($args);
			return $this->get_product_units();
		}
		
		function new_product_unit(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],"unit_id");
			return $add_new_result;
		}
		
		function update_product_unit(){
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->update_an_item($_POST['data_post'],$primary_key);
		}
		
		function delete_product_unit(){
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->delete_an_item($_POST['data_post'][$primary_key]);
		}
	}