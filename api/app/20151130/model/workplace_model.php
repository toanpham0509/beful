<?php
	class Workplace_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["category_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "category_name"	,
				"table_name"	=> "category"
			);
			return $fields;
		}
		
		
		function filter_shop(){
			$this->item_list_model["filter"]	.= "
				AND ".$this->item_list_model["table_name"].".category_id	= 15 
			";
		}
		
		function get_all_shop(){
			$fields = array("workplace_id","workplace_name",'workplace_code');
			$this->fields	= $this->filter_fields($fields);
			$this->filter_shop();
			return $this->item_list();
		}

		function get_shops(){
			$this->fields[]	= array(
				'column_name'	=> 'district_name'	,
				'table_name'	=> 'district'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'city_id'	,
				'table_name'	=> 'city'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'city_name'	,
				'table_name'	=> 'city'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'country_id'	,
				'table_name'	=> 'country'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'country_name'	,
				'table_name'	=> 'country'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'user_id'	,
				'table_name'	=> 'user'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'user_name'	,
				'table_name'	=> 'user'	,
			);
			/*$this->fields[]	= array(
				'column_name'	=> 'position_id'	,
				'table_name'	=> 'employee_work'	,
			);*/
			$this->item_list_model["left_join"]	.= "
				<br>LEFT JOIN city ON district.city_id	= city.city_id
				<br>LEFT JOIN country ON city.country_id	= country.country_id
				<br>LEFT JOIN employee_work ON (".$this->item_list_model['table_name'].
				".".$this->item_list_model['primary_key']."	= employee_work.workplace_id 
					AND employee_work.position_id = '4'     
				)
				<br>LEFT JOIN user ON employee_work.user_id 		= user.user_id
			";
			$this->filter_shop();
			
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			
			return $this->item_list();
		}	
		
		function get_shop_fields(){
			/*
			$this->fields[]	= array(
				'column_name'	=> 'quittance_header'	,
				'table_name'	=> 'quittance_footer'	,
			);
			$this->fields[]	= array(
				'column_name'	=> 'quittance_header'	,
				'table_name'	=> 'quittance_footer'	,
			);
			
			echo "<pre>";
				print_r($this->fields);
			echo "</pre>";	*/
		}
		
		function get_shop_code_field(){
			$fields = array('workplace_code');
			$this->fields	= $this->filter_fields($fields);
		}
}		
		