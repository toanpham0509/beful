<?php
	class Input_Order_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["partner_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"
			);
			$fields["status_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "status_name"	,
				"table_name"	=> "status"
			);
			return $fields;
		}
		
		function filter_imports(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model['table_name'].".purchase_order IS NOT NULL
				<br>AND ".$this->item_list_model["table_name"].".status_id = 14
			";
		}
		
		function filter_import(){    
			$this->item_list_model['filter']	.= "
				<br>AND (".$this->item_list_model["table_name"].".status_id = 5 
				OR ".$this->item_list_model["table_name"].".status_id = 14)
			";
		}

		function filter_purchase_order(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model['table_name'].".purchase_order IS NULL
				<br>AND ".$this->item_list_model['table_name'].".status_id = 15
			";
		}
		
		function filter_order_id($order_id){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model['table_name'].".input_order_id = '".$order_id."'
			";
		}
		
		function get_partner_info() {
			$this->item_list_model['left_join']	.= "
				<br>
			";
			$this->fields[]	= array(
				"column_name"	=> "partner_address" ,
				"table_name"	=> "partner" ,
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_phone_number" ,
				"table_name"	=> "partner" ,
			);
		}
		
		function get_district_info(){
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN district ON partner.district_id = district.district_id
			";
			/*$this->fields[]	= array(
				"column_name"	=> "district_id" ,
				"table_name"	=> "district" ,
			);*/
			$this->fields[]	= array(
				"column_name"	=> "district_name" ,
				"table_name"	=> "district" ,
			);
		}
		function get_city_info(){
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN city ON district.city_id	= city.city_id
			";
			$this->fields[]	= array(
				"column_name"	=> "city_name" ,
				"table_name"	=> "city" ,
			);
		}
		function get_country_info(){
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN country ON city.country_id = country.country_id
			";
			$this->fields[]	= array(
				"column_name"	=> "country_name" ,
				"table_name"	=> "country" ,
			);
		}
		
		
}		
		