<?php
	class Promote_Product_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["promote_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "promote_name"	,
				"table_name"	=> "promote"
			);
			$fields['product_id']	=array( 
				"column_name"	=> "product_code"	,
				"table_name"	=> "product"
			);
			$fields["promote_product"]	=array( // country_id is column's name in city table
				"column_name"	=> "promote_product_name"	,
				"table_name"	=> "tmp_table"
			);
			$fields[""]	=array( // country_id is column's name in city table
				"column_name"	=> "product_name"	,
				"table_name"	=> "product"
			);
			return $fields;
		}
		
		function fetchAll($sql,$fields,$db_info){
			$sql	= $this->replace_cat_1($sql);
			return parent::fetchAll($sql,$fields,$db_info);
		}
		
		function item_list(){
			$left_join_1	= "product ON ".$this->item_list_model['table_name'].
				".promote_product = product";
			$left_join_2	= "product as tmp_table ON ".$this->item_list_model['table_name'].
				".promote_product = tmp_table";
			$this->item_list_model["left_join"] = str_replace($left_join_1,$left_join_2, 
				$this->item_list_model["left_join"]);
			return parent::item_list();	
		}
		
		function number_rows_pages($sql , $num_rows_pp){
			$sql	= $this->replace_cat_1($sql);
			return parent::number_rows_pages($sql , $num_rows_pp);
		}
		
		function replace_cat_1($sql){
			$sql = str_replace("product.product_code",
					"product.product_code as product_code", $sql);
			$sql = str_replace("tmp_table.promote_product_name",
					"tmp_table.product_code as promote_product_name", $sql);	
			$sql = str_replace(" product.status"," tmp_table.status",$sql);			
			return $sql;		
		}	
}		
		