<?php
	Class Base_Model extends Core_Model{
		public function __construct() {		
			parent::__construct();
			$this->item_details_model["do_not_show_details"]["list"][] = "status";
			
			if(!empty($_POST['data_post']['per_page_number'])){
				$this->item_list_model["num_rows_per_page"]	= $_POST['data_post']['per_page_number'];
				unset($_POST['data_post']['per_page_number']);
			}else{
				$this->item_list_model["num_rows_per_page"]	= 100;
			}
			
			$this->item_list_model["filter"]		= " WHERE ".$this->item_list_model["table_name"].".status != '1' ";	
			$this_class	= get_class($this);
			if(
				$_GET['action'] == 'get_order' or
				$_GET['action'] == 'get_product' or
				$_GET['action'] == 'get_import' or
				$_GET['action'] == 'get_transfer' or
				$_GET['action'] == 'get_refund' or
				$_GET['action'] == 'get_buy_order' or
				$_GET['action'] == 'get_warehouse' 
			){
			//	echo 11;
			}
			elseif(
				$this_class	==  "Product_Model" or
				$this_class	==  "Order_Model" or
				$this_class	==  "Input_Order_Model" or
				$this_class	==  "Product_Transfer_Model" or
				$this_class	==  "Product_Refund_Model"  or
				$this_class	==  "Warehouse_Model" 
			){
				$this->item_list_model["filter"]		.= " AND ".$this->item_list_model["table_name"].".status > 1 ";
			}
		
			$this->field_list['output_order']= array(
				'customer_id'		=> 'partner_id'	,
				'price_id'			=> 'quotation_id'	,
				'order_status_id'			=> 'status_id'	,
				'order_note'			=> 'output_note'	,
			);
			$this->field_list['output_lines']= array(
				'product_amount'		=> 'output_line_amount'	,
			);
			$this->field_list['product_price']= array(
			//	'product_price'		=> 'giá sản phẩm'	,
			);
			$this->field_list['category']= array(
				'product_category_id'		=> 'category_id'	,
				'product_category_name'		=> 'category_name'	,
			);
			$this->field_list['partner_category']= array(
				'customer_id'		=> 'partner_id'	,
				'customer_name'		=> 'partner_name'	,
				'customer_amount'		=> 'SUM(output_order.order_price)'	,
				'address'		=> 'partner_address'	,
				'fax'		=> 'partner_fax'	,
				'phone'		=> 'partner_phone_number'	,
				'tax_code'		=> 'partner_tax_code'	,
			);
			$this->field_list['partner']= array(
				'customer_id'		=> 'partner_id'	,
				'customer_name'		=> 'partner_name'	,
				'customer_amount'		=> 'SUM(output_order.order_price)'	,
				'address'		=> 'partner_address'	,
				'fax'		=> 'partner_fax'	,
				'phone'		=> 'partner_phone_number'	,
				'tax_code'		=> 'partner_tax_code'	,
			);
			$this->field_list['product']= array(
				'customer_id'		=> 'partner_id'	,
				'product_unit_id'	=> 'unit_id',
				'barcode'			=> 'product_barcode'
			);
			$this->field_list['workplace']= array(
				'shop_id'		=> 'workplace_id'	,
				'shop_code'		=> 'workplace_code'	,
				'shop_name'		=> 'workplace_name'	,
				'shop_address'		=> 'workplace_address'	,
				'phone'		=> 'workplace_phone'	,
			);
			$this->field_list['product_inventory']= array(
				'product_amount'		=> 'inventory_amount'	,
				'product_price_root'	=> 'input_price'
			);
			$this->field_list['input_order']= array(
				'order_id'		=> 'input_order_id'	,
				'order_code'	=> 'input_code'	,
				'supplier_id'	=> 'partner_id'	,
				'supplier_name'	=> 'partner_name'	,
				'order_date'	=> 'input_date'	,
				'order_status'	=> 'status_name'	,
				'supplier_address'	=> 'partner_address'	,
				'supplier_phone'	=> 'partner_phone_number'	,
				'order_note'	=> 'input_note'	,
			);
			$this->field_list['input_lines']= array(
				'order_line_id'		=> 'input_line_id'	,
				'product_amount'	=> 'input_line_amount' ,
				'order_id'	=> 'input_order_id',
				'product_unit_name'	=> 'unit_name'
			);
			$this->field_list['product_transfer']= array(
				'transfer_code'		=> 'order_code'	,
				'transfer_date'		=> 'order_date'	,
				
			);
			$this->field_list['product_refund']= array(
				'import_code'		=> 'input_code'	,
				'refund_date'		=> 'input_date'	,
				'import_id'		=> 'input_order_id'	,
				'customer_id'		=> 'partner_id'	,
				'customer_name'		=> 'partner_name'	,
				'customer_address'		=> 'partner_address'	,
				'customer_phone'		=> 'partner_phone_number'	,
			);
			$this->field_list['product_price']= array(
				'price_id'		=> 'price_list_id'	,
				
				
			);
		}
		
		function filter_like($args){
			$field_name		= $args["field_name"];
			$field_value	= $args["field_value"];
			$this->item_list_model['filter']	.= "
				<br> AND ".$args['table_name'].".".$field_name." LIKE '%".$field_value."%'
			";
			return false;
		}
		
		function filter_between($args){
			$field_name		= $args["field_name"];
			$between_time	= $args["field_value"];			
			$between_time	= explode(";",$between_time);
			$from_time		= $between_time[0];
			$to_time		= $between_time[1];
			
			$this->item_list_model['filter']	.= "
				<br> AND (".$args['table_name'].".".$field_name." 
				between '".$from_time."' AND '".$to_time."')
			";
			return false;
		}
		
		function filter_equal($args){
			$field_name		= $args["field_name"];
			$field_value	= $args["field_value"];
			
			$this->item_list_model['filter']	.= "
				<br>AND ".$args['table_name'].".".$field_name." = '".$field_value."'
			";
			return false;
		}
		
		function get_fields(){	
			global $config_dir;
			global $project_name;
			$table_name	= $this->item_list_model["table_name"];			
			$file_path	= $config_dir["application_folder"]."model/tables_fields/".$table_name.".xml";
			$xml_content = simplexml_load_file($file_path) or die("Error: Cannot load ".$table_name.".xml");
			$i = 0;
			foreach($xml_content->children() as $data) {
				foreach($data as $k=>$v){
					$result[$i][$k] = (string)$v;
				}
				$i++;	
			}
			return $result;
		}
		
		function create_xml_fields_file($get_fields,$file_path){			
			$xml = new DOMDocument('1.0', 'utf-8');
			$xml->formatOutput = true;
			  
			$root_element = $xml->createElement( "fields" );
			$xml->appendChild( $root_element );
		  
			foreach( $get_fields as $field ){
				$one_element = $xml->createElement( "field" );
				foreach($field as $k=>$v){
					$INFORMATION_SCHEMA = $xml->createElement( $k );
					$INFORMATION_SCHEMA->appendChild(
						$xml->createTextNode( $v )
					);
					$one_element->appendChild( $INFORMATION_SCHEMA );
				}	
				$root_element->appendChild( $one_element );
			}
		  
			$xml->save($file_path);
		}

		function check_item_exist($info){
			$field_name		= $info['field_name'];
			$field_value	= $info['field_value'];
			$table_name	= $this->item_list_model["table_name"];
			
			$tmp_item_list	= $this->item_list_model;
			$tmp_fields		= $this->fields;
			
			$this->item_list_model['filter']	.= " AND 
				".$table_name.".".$field_name."	= '".$field_value."'
			";
			
			$fields	= array($field_name);
			$this->fields	= $this->filter_fields($fields);
			$result	=	$this->item_list();
			
			$this->item_list_model	= $tmp_item_list;
			$this->fields			= $tmp_fields	;
			
			return count($result['item_list']) ;
		}
			
		function convert_form_to_db($field_array,$type){
			$table_name	= $this->item_list_model['table_name'];
			$field_list	= $this->field_list;
			foreach($field_array as $k=>$v){
				if($type == null){
					if($field_list[$table_name][$k] == null)
						$k2	= $k; 
					else
						$k2	= $field_list[$table_name][$k]; 
				}else{
					if(array_search($k,$field_list[$table_name]) == null)
						$k2	= $k; 
					else
						$k2	= array_search($k,$field_list[$table_name]);
				}
				$result[$k2]	= $v;
			}
			
			return $result;
		}
		
		function get_class_from_table_name($table_name){
			if($table_name == "output_order")
				$class_name	=	"order";
			elseif($table_name == "category")
				$class_name	=	"category";
			else
				$class_name	= $table_name;
			return $class_name;
		}
		
		function add_filter(){					
			foreach($this->fields as $k=>$v){
				$column_name	= $v["column_name"];
				if($v["COLUMN_KEY"] == "MUL" AND $v["REFERENCED_TABLE_NAME"] != null
					AND $v["REFERENCED_COLUMN_NAME"] != null
				) {
					$class_name= $this->get_class_from_table_name($v["REFERENCED_TABLE_NAME"])."_Model";
					if (!class_exists($class_name))	require_once(strtolower($class_name).".php");
					$left_join_model	= new $class_name;
					$add_filter	= str_replace('WHERE','',$left_join_model->item_list_model["filter"]);
					$add_filter	= "<br>  AND (".$add_filter." 
						<br>OR ".$v["table_name"].".".$column_name." IS NULL
					)";
					$this->item_list_model["filter"]	.= $add_filter;
				}
			}
		//	$result	= parent::get_left_join(); 
		//	return $result;
		}
		
		function item_delete($item_id){
			$item_fields	= array('status'=>1);
			return $this->item_update($item_id,$item_fields);
		}
		
		function item_list(){
			$this->add_filter();
			/*$this->table_properties	= $this->table_properties();
			echo "<div style='margin-left:300px;'>";
			echo $this->table_properties['item_list_sql'];
			echo "</div>";*/
			return parent::item_list();
		}
		function add_create_time_field(){
			unset($this->item_list_model['not_show_list'][1]);
		/*	echo "<pre>";
			print_r($this->item_list_model['not_show_list']);
			echo "</pre>"; */
		}
		
		function item_add_new($update_info){
			foreach($update_info as $k=>$v){
				if($v==null and $k != 'quotation_id') unset($update_info[$k]);
			}
			$trace=debug_backtrace();
			if($trace[2]['function'] != 'inspection_order'){
			//	unset($update_info['status'])	;
			}			
			if($update_info == null) {
				$update_info = array(
					"last_update"	=> time()
				);
			};
			return parent::item_add_new($update_info);
		}
		
		function item_update($item_id,$item_fields){
			$trace=debug_backtrace();
		//	echo $trace[2]['function'];
			if(
				$trace[2]['function'] != 'inspection_order' and
				$trace[2]['function'] != 'delete_an_item'
				){
		//		unset($item_fields['status'])	;
			}
			if($item_fields == null) {
				$item_fields = array(
					"last_update"	=> time()
				);
			};
			return $this->item_update_($item_id,$item_fields);
		}
		
		function item_update_($item_id,$item_fields){
			foreach($item_fields as $k=>$v){
				if($v != '') $v = "'".$v."'";
				else		 $v = 'NULL';	
				if($update_content == "")	$update_content	.= $k." = ".$v;
				else						$update_content	.= " , ".$k." = ".$v;				
			}
			$update_content	.= " , last_update = '".time()."'";
			$update_sql	= "UPDATE ".$this->item_list_model["table_name"]." set ".$update_content
			." WHERE ".$this->item_list_model["table_name"].".".$this->item_list_model["primary_key"]." = '".$item_id."'";
			return $this->sql_runing($update_sql,$this->db_info);
		}
	}
	
	
?>