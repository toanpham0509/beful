<?php
	class Partner_Category_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
			
		//	$this->add_filter();
			
			$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/	
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["partner_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"
			);
			$fields["category_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "category_name"	,
				"table_name"	=> "category"
			);
			return $fields;
		}
		
		function get_category_relation(){
			$this->item_list_model["left_join"]	.="
				<br>LEFT JOIN category_relation ON category.category_id = category_relation.child_id
			";
		}
		
		
		
		function get_all_customer(){
			$fields = array("partner_id");
			$this->fields	= $this->filter_fields($fields);
			$this->fields[]	= array(
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_phone_number"	,
				"table_name"	=> "partner"	
			);
			
			$this->filter_customer();
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function report_sale(){			
			$this->fields[]	= array(
				"column_name"	=> "order_price"	,
				"table_name"	=> "output_order"	
			);
			
			$this->filter_customer();
			
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN output_order ON output_order.partner_id = partner.partner_id 
			";			
			$this->item_list_model['filter']	.="
				<br>AND output_order.quotation_id IS NOT NULL 				
			";
			$this->item_list_model["sort"] = " 
				<br>GROUP BY ".
				$this->item_list_model['table_name'].
				".partner_id ".
				$this->item_list_model["sort"];
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		
		
		function fetchAll($sql,$fields,$db_info){
			$trace=debug_backtrace();
			if($trace[2]['class']	== "Partner_Category_Model"
			and $trace[2]['function'] == 'report_sale'){				
				$order_price_key	= array_search('order_price',$fields);
				$fields[$order_price_key]	= "SUM(output_order.order_price)";
				$sql = str_replace('output_order.order_price',
					'SUM(output_order.order_price)',$sql);
				$result	= parent::fetchAll($sql,$fields,$db_info);	
			}else{
				$result	= parent::fetchAll($sql,$fields,$db_info);
			}
			return $result;
		}
		
		function customer_fields(){
			$this->fields[]	= array(
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_address"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_fax"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_phone_number"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "contact_person_name"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "contact_person_phone"	,
				"table_name"	=> "partner"	
			);
			$this->fields[]	= array(
				"column_name"	=> "contact_person_email"	,
				"table_name"	=> "partner"	
			);
		}
		
		function suppliers_fields(){
			$this->customer_fields();
			$this->fields[]	= array(
				"column_name"	=> "partner_tax_code"	,
				"table_name"	=> "partner"	
			);
		}
		
		function filter_customer(){
			$this->get_category_relation();			
			$this->item_list_model["filter"]	.= "
					AND 
				(category_relation.parent_id	= 17
				OR partner_category.category_id =17
				)
			";
		}
		
		function filter_supplier(){
			$this->get_category_relation();			
			$this->item_list_model["filter"]	.= "
				AND (category_relation.parent_id	= 2
				OR partner_category.category_id		= 2)
			";
		}
		
		function get_suppliers(){
			$fields = array("partner_id");
			$this->fields	= $this->filter_fields($fields);
			$this->suppliers_fields();
			$this->filter_supplier();
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function get_customers(){
			$fields = array("partner_id");
			$this->fields	= $this->filter_fields($fields);
			$this->customer_fields();
			$this->filter_customer();
			return $this->item_list();
		}
		
		function get_supplier($supplier_id){
			return $this->get_customer($supplier_id);
		}
		
		function get_partner_fields(){
			$fields = array("partner_id");
			$this->fields	= $this->filter_fields($fields);
			$this->customer_fields();
						
			$this->fields[]	= array(
				'column_name'	=> 'district_id'	,
				'table_name'	=> 'district'
			);
			$this->fields[]	= array(
				'column_name'	=> 'district_name'	,
				'table_name'	=> 'district'
			);
			$this->fields[]	= array(
				'column_name'	=> 'city_id'	,
				'table_name'	=> 'city'
			);
			$this->fields[]	= array(
				'column_name'	=> 'city_name'	,
				'table_name'	=> 'city'
			);
			$this->fields[]	= array(
				'column_name'	=> 'country_id'	,
				'table_name'	=> 'country'
			);
			$this->fields[]	= array(
				'column_name'	=> 'country_name'	,
				'table_name'	=> 'country'
			);
			$this->fields[]	= array(
				'column_name'	=> 'partner_tax_code'	,
				'table_name'	=> 'partner'
			);
			$this->fields[]	= array(
				'column_name'	=> 'bank_number'	,
				'table_name'	=> 'partner'
			);
			$this->fields[]	= array(
				'column_name'	=> 'bank_name'	,
				'table_name'	=> 'partner'
			);
			$this->fields[]	= array(
				'column_name'	=> 'note'	,
				'table_name'	=> 'partner'
			);
		}
		
		function get_partner($partner_id){
			$args["field_name"]		= 'partner_id';
			$args["field_value"]	= $partner_id;
			$args['table_name']		= "partner";			
			$this->filter_equal($args);
			
		//	$this->get_partner_fields();
			
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN district ON partner.district_id	= district.district_id
				<br>LEFT JOIN city ON district.city_id			= city.city_id
				<br>LEFT JOIN country ON city.country_id		= country.country_id
			";
			return $this->item_list();
		}
}		
		