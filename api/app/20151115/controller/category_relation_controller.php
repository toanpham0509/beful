<?php
	Class Category_Relation_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Category_Relation_Model;
			$this->this_view	= new Category_Relation_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
						
		//	if($field_name == 'input_date' or $field_name == 'input_order_id')
		//		$args['table_name']	= 'input_order';
			$args['table_name']	= $this->this_model->item_list_model['table_name'];	
			
			if($field_name == '...'){
				
			}else{
				$this->this_model->filter_equal($args);
			}
						
			return $result;
		}
		
		function get_product_category_relation_list(){
			$this->this_model->filter_product_category();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function new_category_relation(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_category_relation(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_category_relation(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
	}