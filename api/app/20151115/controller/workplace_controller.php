<?php
	Class Workplace_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Workplace_Model;
			$this->this_view	= new Workplace_View;
			
		}
		function get_all_shop(){
			$data	= $this->this_model->get_all_shop();
			foreach($data['item_list'] as $k=>$v){
			//	echo $k.": ".$v['partner_id'].": ".$v['partner_name']."<br>";
				$data_2[$k]['shop_id']	= $v['workplace_id'];
				$data_2[$k]['shop_name']	= $v['workplace_name'];
				$data_2[$k]['shop_code']	= $v['workplace_code'];
			};
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function get_shops(){
			$args['table_name']		= $this->this_model->item_list_model['table_name'];
			if(!empty($_POST['data_post']['salesperson_id'])){
				$args["field_name"]		= 'user_code';
				$args["field_value"]	= $_POST['data_post']['salesperson_id'];
				$tmp = $args['table_name'];
					$args['table_name']		= 'user';
					$this->this_model->filter_equal($args);
				$args['table_name']	= $tmp;
			}
			if(!empty($_POST['data_post']['shop_name'])){
				$args["field_name"]		= 'workplace_name';
				$args["field_value"]	= $_POST['data_post']['shop_name'];
				$this->this_model->filter_like($args);
			}
			if(!empty($_POST['data_post']['shop_address'])){
				$args["field_name"]		= 'workplace_address';
				$args["field_value"]	= $_POST['data_post']['shop_address'];
				$this->this_model->filter_like($args);
			}
			if(!empty($_POST['data_post']['shop_manager'])){
				$args["field_name"]		= 'user_name';
				$args["field_value"]	= $_POST['data_post']['shop_manager'];
				$tmp = $args['table_name'];
				$args['table_name']		= 'user';
				$this->this_model->filter_like($args);
				$args['table_name']	= $tmp;
			}
			$data	= $this->this_model->get_shops();
			
			$data_2['item_list'] = $this->set_fields_shop($data['item_list']);
			
			$data_2['num_rows_total']	= $data['num_rows_total'];
			$data_2['num_pages']	= $data['num_pages'];
			$data_2['num_rows_show']	= $data['num_rows_show'];
			
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function set_fields_shop($item_list){
			foreach($item_list as $k=>$v){
			//	echo $k.": ".$v['partner_id'].": ".$v['partner_name']."<br>";
				$data_2[$k]['shop_id']	= $v['workplace_id'];
				$data_2[$k]['shop_name']	= $v['workplace_name'];
				$data_2[$k]['shop_code']	= $v['workplace_code'];
				$data_2[$k]['shop_address']	= $v['workplace_address'];
				$data_2[$k]['district_id']	= $v['district_id'];
				$data_2[$k]['district_name']	= $v['district_name'];
				$data_2[$k]['city_id']	= $v['city_id'];
				$data_2[$k]['city_name']	= $v['city_name'];
				$data_2[$k]['country_id']	= $v['country_id'];
				$data_2[$k]['country_name']	= $v['country_name'];
				$data_2[$k]['phone']	= $v['workplace_phone'];
				$data_2[$k]['shop_manager_id']	= $v['user_id'];
				$data_2[$k]['shop_manager_name']	= $v['user_name'];
			//	$data_2[$k]['position_id']	= $v['position_id'];
			};
			return $data_2;
		}
		
		function get_shop(){
		//	return $_POST['data_post']['shop_id'];
			$args["field_name"]		= "workplace_id";
			$args["field_value"]	= $_POST['data_post']['shop_id'];
			$args['table_name']		= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_equal($args);
			
			$data	= $this->this_model->get_shops();
			$data_2 = $this->set_fields_shop($data['item_list']);
			
			$data_2[0]['quittance_header']	= $data['item_list'][0]['quittance_header'];
			$data_2[0]['quittance_footer']	= $data['item_list'][0]['quittance_footer'];
			
			$result	= $this->this_view->return_true($data_2);
			return  $this->this_view->json_show($result);
		}
		
		function new_shop(){
			$input = $this->this_model->convert_form_to_db($_POST['data_post'],$type);
			$input['category_id']	= 15;
			$new_shop_id	= $this->this_model->item_add_new($input);
			if(is_numeric($new_shop_id)){
				$shop_code	= "CH-".$new_shop_id;
				$shop_code	= array('workplace_code'	=> $shop_code);
				$this->this_model->item_update($new_shop_id,$shop_code);
			}
			if(is_numeric($new_shop_id)){
				$data	= array('shop_id' => $new_shop_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($new_shop_id);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function update_shop(){
			$input = $this->this_model->convert_form_to_db($_POST['data_post'],$type);
			$input['category_id']	= 15;
			$shop_id	= $_POST['data_post']['shop_id'];
			unset($input['workplace_id']);
			$result	= $this->this_model->item_update($shop_id,$input);
			if($result	== "0" ){
				$data	= array('shop_id' => $shop_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($result);
				$result	= $this->this_view->return_false($info);
			}
			return  $this->this_view->json_show($result);
		}
		
		function delete_shop(){
			$shop_id	= $_POST['data_post']['shop_id'];
			$result	= $this->this_model->item_delete($shop_id); 
			$result	= $this->this_view->return_true(null);
			return  $this->this_view->json_show($result);
		}
		
		function drop_down_list($current_value){
			if($_GET["filter_type"] == "shop"){
				$args["field_name"]		= 'category_id';
				$args["field_value"]	= 15;
				$args['table_name']		= $this->this_model->item_list_model['table_name'];
				$this->this_model->filter_equal($args);
			}
			return parent::drop_down_list($current_value);
		}
		
		function get_shop_code($shop_id){
			$this->this_model->get_shop_code_field();
			$shop_details	= $this->this_model->item_details($shop_id);
			return $shop_details["item_list"][0]['workplace_code'];
		}
	}