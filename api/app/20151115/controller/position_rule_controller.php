<?php
	Class Position_Rule_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Position_Rule_Model;
			$this->this_view	= new Position_Rule_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		
		function get_position_rules(){
		//	$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
	}