<?php
	Class Input_Lines_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Input_Lines_Model;
			$this->this_view	= new Input_Lines_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function convert_fields(){
			unset($this->this_model->field_list['input_lines']['order_line_id']);
			$this->this_model->field_list['input_lines']['import_line_id'] = 'input_line_id';
		}
		
		function filter_by_a_field_2($field_name){
			$trace			= debug_backtrace();
			
			if(empty($_POST['data_post'][$field_name])){
				exit();
			}
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $_POST['data_post'][$field_name] ;
			$args['table_name']	= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_equal($args);
			$this->this_model->add_unit_info();
			$data	= $this->this_model->item_list();
			$tmp	= $data['item_list'];
				unset($data['item_list']);
				unset($data['fields']);
				unset($data['primary_key']);
			$result	= $data;			
			
			foreach($tmp as $k=>$v){
				if($trace[1]['function'] == "get_import_lines" or 
					$trace[1]['function'] == "get_import_line"){
					unset($v["intended_date"]);
				}
				$result['item_list'][$k]	= $this->this_model->convert_form_to_db($v,true) ;
			}
			return $result;
		}
		
		function get_buy_lines(){
			$result	= $this->filter_by_a_field_2('input_order_id');
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_buy_line(){
			if(empty($_POST['data_post']['input_line_id'])) exit();
			$result	= $this->filter_by_a_field_2('input_line_id');
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_input_line($primary_key){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$primary_key);
			return $add_new_result;
		}
		
		function new_buy_line(){
			return $this->new_input_line('input_line_id');
		}
		
		function update_buy_line() {
			return $this->update_an_item($_POST['data_post'],'input_line_id');
		}
		
		function delete_buy_line(){
			return $this->delete_an_item($_POST['data_post']['input_line_id']);
		}
		
		function get_import_lines(){
			$this->convert_fields();
			$result	= $this->filter_by_a_field_2('input_order_id');
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_import_line(){
			$this->convert_fields();
			if(empty($_POST['data_post']['input_line_id'])) exit();
			$result	= $this->filter_by_a_field_2('input_line_id');
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_import_line(){
			$this->convert_fields();
			return $this->new_input_line('input_line_id');
		}
		
		function update_import_line() {
			$this->convert_fields();
			return $this->update_an_item($_POST['data_post'],'input_line_id');
		}
		
		function delete_import_line(){
			$this->convert_fields();
			return $this->delete_an_item($_POST['data_post']['input_line_id']);
		}
		
		function get_product_price_import(){
			$args['controller']	= "inventory";
			$args['action']		= "get_product_price_import";
			//	$_GET['args']		= $args['args'];
			$inventory_info = $this->call_controller($args);
			$check_inventory_amount = $inventory_info["num_rows_total"];
			
			$this->this_model->get_product_price_import();
			$result	= $this->item_info();
			$result['item_list'][0]["inventory_amount"] = $inventory_info['item_list'][0]["inventory_amount"];
			if($check_inventory_amount == 0){
				$result['item_list'] = array(0=>array('product_price'=>'','inventory_amount'=>'0'));
			} else{
					
			}
		//	$this->table_properties	= $this->this_model->table_properties();
		//	echo $this->table_properties["item_list_sql"];
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
	}