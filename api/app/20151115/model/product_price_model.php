<?php
	class Product_Price_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/	
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["product_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "product_code"	,
				"table_name"	=> "product"
			);
			/*
			$fields["currency_unit_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "currency_unit_name"	,
				"table_name"	=> "currency_unit"
			);
			*/
			return $fields;
		}
		
		function get_left_join__(){
			$left_join	= parent::get_left_join();
			$left_join	.= "
				<br>LEFT JOIN product ON input_lines.product_id	= product.product_id
			";
			return $left_join;
		}
		
		function filter_price_id($price_id){
			$this->item_list_model['left_join']	.= "
				<br>LEFT JOIN output_lines ON ".$this->item_list_model['table_name'].".".$this->item_list_model['primary_key']."
						=	output_lines.product_price_id 
				<br>LEFT JOIN output_order ON output_lines.order_id = output_order.order_id		
			";
			$this->item_list_model['filter']	.= "
				<br>AND output_order.quotation_id IS NULL
				<br>AND output_order.order_id	= '".$price_id."'
			";
		//	return $price_id." = ". $price_id;
		}
		
		function filter_product_id($product_id){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model["table_name"].".product_id	= '".$product_id."'
			";
		}
		
		function filter_warehouse_id($warehouse_id){
			$this->item_list_model['filter']	.= "
				<br>AND input_lines.warehouse_id	= '".$warehouse_id."'
			";	
		}
		
		function get_current_product_prices(){
			$fields	= array('product_price_id','product_id','product_price','price_id');
			$this->fields= $this->filter_fields($fields);
			/*
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model["table_name"].".product_price_start_time	< '".time()."'
				<br>AND ".$this->item_list_model["table_name"].".product_price_end_time		> '".time()."'
			";
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();*/
		}
		function filter_price_lines(){
			$fields = array('product_price_id',
			'product_id','product_price','price_list_id','product_note');
			$this->fields	= $this->filter_fields($fields);
		}
		
		function get_product_id($price_id){
			$fields = array('product_id');
			$this->fields	= $this->filter_fields($fields);
			$this->item_list_model['filter']	.= "
				AND ".$this->item_list_model['table_name'].".price_list_id	= '".$price_id."'
			";
		}
}		
		