<?php
	class Category_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		/*
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["country_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			return $fields;
		}
		*/
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["category_type_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "category_type_name"	,
				"table_name"	=> "category_type"
			);
			return $fields;
		}

		function get_product_categories(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model['table_name'].".category_type_id = '2'
			";
			return $this->item_list();
		}

		function get_product_category_fields(){
			
			$fields = array('category_id','category_name','create_time');
			$this->fields = $this->filter_fields($fields);	
		}	
}





		
		