<?php
	class User_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
			$this->item_list_model["not_show_list"][]	= "user_password";
			$this->item_list_model["not_show_list"][]	= "user_birthday";
			$this->item_list_model["not_show_list"][]	= "user_email";
		//	$this->item_list_model["not_show_list"][]	= "district_name";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["district_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "district_name"	,
				"table_name"	=> "district"
			);
			return $fields;
		}
		
		
		
		
		function register($update_info){
			$new_user_id	= $this->item_add_new($update_info);
			return array('user_id'	=> $new_user_id);
		}
		
		function login($info){	
			$table_name	= $this->item_list_model["table_name"];
			$fields	= array('user_id','user_name','user_password');
			$this->fields	= $this->filter_fields($fields);
			$this->item_list_model['filter']	.= "
				AND ".$table_name.".".$info['field_name']." = '".$info['field_value']."'
			";
			$result	=	$this->item_list();
			return $result['item_list'];
		}
		
}		
		