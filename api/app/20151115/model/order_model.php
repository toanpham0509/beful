<?php
	class Order_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["partner_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "partner_name"	,
				"table_name"	=> "partner"
			);
			$fields["salesperson_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "user_name"	,
				"table_name"	=> "user"
			);
			$fields["status_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "status_name"	,
				"table_name"	=> "status"
			);
			$fields["shop_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "workplace_code"	,
				"table_name"	=> "workplace"
			);
			
			return $fields;
		}
	
		function get_table(){
			$get_class	= get_class($this);
			$result = str_replace('_Model','',$get_class);
			$result = "output_".$result;
			return strtolower($result);
		}
		
		function filter_orders(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model["table_name"].".quotation_id IS NOT NULL
				<br>AND ".$this->item_list_model["table_name"].".status_id = 3
			";
		}
		
		function filter_order(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model["table_name"].".quotation_id IS NOT NULL
			";
		}
		
		function filter_quotation(){
			$this->item_list_model['filter']	.= "
				<br>AND ".$this->item_list_model["table_name"].".quotation_id IS NULL
			";
		}
		
		function get_orders($args){
		//	$filter	= $args['filter'];
		//	$this->item_list_model['filter']	.= $filter;
			$this->item_list_model["num_rows_per_page"] = $args['num_rows_per_page'];
			$this->item_list_model["current_page"] = $args['pp'];
			
			$this->fields[]	= array(
				"column_name"	=> "partner_phone_number"	,
				"table_name"	=> "partner"
			);
			
			$this->filter_orders();
			
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function get_order($order_id){
		//	unset($this->item_list_model["not_show_list"][1]);
		//	unset($this->item_details_model[do_not_show_details]['list'][1]);
		//	$this->fields							= $this->get_fields();
			$fields = array("partner_id","quotation_id",'order_code','discount');
		//	$this->fields	= $this->filter_fields($fields);
			
			$this->fields[]	= array(
				"column_name"	=> "partner_address"	,
				"table_name"	=> "partner"
			);
			$this->fields[]	= array(
				"column_name"	=> "district_id"	,
				"table_name"	=> "district"
			);
			$this->fields[]	= array(
				"column_name"	=> "district_name"	,
				"table_name"	=> "district"
			);
			
			$this->fields[]	= array(
				"column_name"	=> "city_id"	,
				"table_name"	=> "city"
			);
			$this->fields[]	= array(
				"column_name"	=> "city_name"	,
				"table_name"	=> "city"
			);
			
			$this->fields[]	= array(
				"column_name"	=> "country_id"	,
				"table_name"	=> "country"
			);
			$this->fields[]	= array(
				"column_name"	=> "country_name"	,
				"table_name"	=> "country"
			);
			$this->fields[]	= array(
				"column_name"	=> "workplace_name"	,
				"table_name"	=> "workplace"
			);
			$this->fields[]	= array(
				"column_name"	=> "partner_phone_number"	,
				"table_name"	=> "partner"
			);
			/*
			$this->fields[]	= array(
				"column_name"	=> "create_time"	,
				"table_name"	=> "order"
			);*/
			
			$this->item_list_model["left_join"]	.= "
				<br>LEFT JOIN district ON district.district_id	= partner.district_id
				<br>LEFT JOIN city ON city.city_id	= district.city_id
				<br>LEFT JOIN country ON country.country_id	= city.country_id
			";
			/*
			print_r($this->item_details_model);
			$this->table_properties	= $this->table_properties();
			echo $this->table_properties['item_list_sql'];
			*/
			$this->filter_order();
			$result	= $this->item_details($order_id);
			return $result;
		}
		
		function get_all_price(){
			$fields = array('order_id','order_name');
			$this->fields	= $this->filter_fields($fields);
			$this->filter_quotation();
			
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function get_prices(){
			$this->filter_quotation();
		//	$this->table_properties	= $this->table_properties();
		//	echo $this->table_properties['item_list_sql'];
			return $this->item_list();
		}
		
		function filter_price_name($price_name){
			$args["field_name"]		= 'order_name';
			$args["field_value"]	= $price_name;
			$args['table_name']		= $this->item_list_model['table_name'];
			return $this->filter_like($args);
		}
		
		function get_price($price_id){
			$this->filter_quotation();
			$result	= $this->item_details($price_id);
			return $result;
		}
		
		function report_sale_line_fields(){
			$fields = array('order_price','order_date');
			$this->fields	= $this->filter_fields($fields);
		}
}		
		