<?php
	Class Admin_Page extends Page_Controller{
		public function __construct($cur_class) {		
			parent::__construct();			
			global $config_dir;
			if($this->cur_page[1] == "logout"){
				$this->logout();
			}
			if(empty($_SESSION['user'])){				
				$this->template_folder	=	$this->config_dir["default_template_folder"]["dir_document_root"];
				$this->template_file	=	"login.html"	;
				$_GET["controller"]= "user";
				$_GET["action"]= "login_form";
				$this->position[4]		= $this->right_main();
				$this->page_title_1	= 'Đăng nhập';
			}else{				
				if($this->cur_page[1] == null) $this->cur_page[1] = 'member_manager';
				if($this->cur_page[1] == 'member_manager'){
					$_GET["controller"]= "user";
					$_GET["action"]= "item_list";
					$this->page_title_1	= $this->cur_page[1];
				}elseif($this->cur_page[1] == 'member_work_manager'){
					$_GET["controller"]= "employee_work";
					$_GET["action"]= "member_work_list";
					$this->page_title_1	= $this->cur_page[1];
				}elseif($this->cur_page[1] == 'promote_manager'){
					$_GET["controller"]= "promote";
					$_GET["action"]= "item_list";
					$this->page_title_1	= $this->cur_page[1];
				}elseif($this->cur_page[1] == 'promote_product_manager'){
					$_GET["controller"]= "promote_product";
					$_GET["action"]= "item_list";
					$this->page_title_1	= $this->cur_page[1];
				}elseif(!empty($this->cur_page[1])){
					$_GET["controller"]= str_replace('_manager','',$this->cur_page[1]);
					$_GET["action"]= "item_list";
					$this->page_title_1	= $this->cur_page[1];
				}else{
					/*$controller_list = $this->get_controller_list();
					$_GET["controller"]= $controller_list[0]["controller_name"];
					$_GET["action"]= "home_show";
					$this->page_title_1	= 'dash board ----';*/
				}			
				$this->position[4] 		= $this->right_main();
				$user_panel				= $this->user_panel();
				$left_menu				= $this->left_menu();
				$this->position[3]		= $user_panel.$left_menu;
			//	echo $this->config_dir["front_end"];
			//	$this->page_title_1		= "pos_list_title_page0000";
			//	$this->template_folder	=	$this->config_dir["front_end"];
			//	$this->template_file	=	"page_company.html"	;
				$this->template_folder	=	$this->config_dir["default_template_folder"]["dir_document_root"];
				$this->template_file	=	"gwt.html"	;
			}
			
			
		}	
		
		function right_main(){
			global $config_dir;
			
			$this->core_folder				= $this->config_dir["core_folder"];
			include	($this->core_folder."main.php");
			$r1= $main;
			
			return $result = $r1;
		}
		function left_menu(){
			global $config_dir;
			
			$_GET["controller"]= "left_menu";
			$_GET["action"]= "get_controller_list";
			if(	
				$this->cur_page[1] == 'member_work_manager'
				or $this->cur_page[1] == 'member_manager'
				or $this->cur_page[1] == 'promote_manager'
				or $this->cur_page[1] == 'promote_product_manager'
				or $this->cur_page[1] == 'promote_order_manager'
				or $this->cur_page[1] == 'rule_manager'
				or $this->cur_page[1] == 'position_rule_manager'
			){
				$_GET['args'] = "menu_1";
			}else{
				$_GET['args'] = "menu_2";
			}
			$this->core_folder				= $this->config_dir["core_folder"];
			include	($this->core_folder."main.php");
			return  $main;
		}
		
		function user_panel(){
			global $config_dir;
			$_GET["controller"]= "user";
			$_GET["action"]= "user_panel";
			$this->core_folder				= $this->config_dir["core_folder"];
			include	($this->core_folder."main.php");
			return  $main;
		}
		
		function logout(){
			global $config_dir;
			$_GET["controller"]= "user";
			$_GET["action"]= "logout";
			$this->core_folder				= $this->config_dir["core_folder"];
			include	($this->core_folder."main.php");
			return  $main;
		}
		
}
	
	
	
	
	