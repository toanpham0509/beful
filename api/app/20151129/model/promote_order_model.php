<?php
	class Promote_Order_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["promote_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "promote_name"	,
				"table_name"	=> "promote"
			);
			$fields["promote_product_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "product_code"	,
				"table_name"	=> "product"
			);
			$fields[""]	=array( // country_id is column's name in city table
				"column_name"	=> "product_name"	,
				"table_name"	=> "product"
			);
			return $fields;
		}
			
}		
		