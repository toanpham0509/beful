<?php
	class Adjust_Inventory_Line_Model extends Base_Model{
		public function __construct(){
			parent::__construct();
			$this->fields							= $this->get_fields();
			$this->item_list_model["select"]		= $this->get_select();
			$this->item_list_model["left_join"]		= $this->get_left_join();
			$this->item_list_model["primary_key"]	= $this->get_primary_key();
		//	$this->item_list_model["not_show_list"][]	= "note";
			$this->item_list_model["sort"]			= " ORDER BY ".
				$this->item_list_model["table_name"].".".
				$this->item_list_model["primary_key"]." DESC ";
		//	$this->add_filter();
			$this->table_properties	= $this->table_properties();
		}
		
		function get_fields(){
			$fields	= parent::get_fields();			
			$fields["product_id"]	=array( // country_id is column's name in city table
				"column_name"	=> "product_name"	,
				"table_name"	=> "product"
			);
			return $fields;
		}
		
		function add_fields(){
			$this->item_list_model['left_join'] .= "
				LEFT JOIN unit ON product.unit_id = unit.unit_id
			";
			$this->fields[] = array( 
				"column_name"	=> "unit_id"	,
				"table_name"	=> "unit"
			);
			$this->fields[] = array( 
				"column_name"	=> "unit_name"	,
				"table_name"	=> "unit"
			);
			$this->fields[] = array( 
				"column_name"	=> "warehouse_id"	,
				"table_name"	=> "adjust_inventory"
			);	
		}
			
}		
		