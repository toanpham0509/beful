<?php
	Class Base_View extends Core_View{
		public function __construct() {		
			parent::__construct();
			global $config_dir;
			$this->form_field["country___"]	= array(
				"view_file"		=> $config_dir["document_root"].$config_dir["root_url"].$config_dir["front_end"]."field_country.html"	
			);
			$this->item_details_view["col_md"]		= 6;
		}
		function translator($word_id){
			$config_lang	= $this->config_lang;
			unset($_SESSION["lang"][$config_lang]);
			return parent::translator($word_id);
		}	
		function number_format($number){
		//	return number_format($number,2);
			if(empty($number))
				return "";
			return number_format($number,0);
		}
		
		function json_show($data){
			$json	= json_encode($data);
			$jsonp_callback = isset($_GET['callback']) ? $_GET['callback'] : null;
			return $jsonp_callback ? "$jsonp_callback($json)" : $json;
		}
		
		function return_false($info){
			$errors		= $info['errors'];
			$message	= $info['message'];
			return array(
				"status"	=> "0",
				"errors"	=> $errors,
				"message"	=> $message,
				"data"		=> null
			);
		}
		
		function return_true($data){
			return array(
				"status"	=> "1",
				"errors"	=> null,
				"message"	=> null,
				"data"		=> $data
			);
		}
		
		function item_already_exist($item_name){
			return array(
				'errors'	=> $item_name	,
				"message"	=> $item_name." already exist"	
			);
		}
		
		function required_input($item_name){
		//	echo $item_name;
			return array(
				'errors'	=> $item_name	,
				"message"	=> $item_name." is required"	
			);
		}
		
		function get_dropdown_list($data,$field_id,$field_name){
			foreach($data as $v){
				$result .= "<option value = '".$v[$field_id]."'>".
					$v[$field_name]."</option>";
			}
			return $result;
		}
		
		function not_allow(){
			return "<span style='font-style:italic;color:red;'>Không hợp lệ</span>";
		}
	}
?>