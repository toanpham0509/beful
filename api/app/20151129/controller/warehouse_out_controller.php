<?php
	Class Warehouse_Out_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Warehouse_Out_Model;
			$this->this_view	= new Warehouse_Out_View;
			
		}
		
	}