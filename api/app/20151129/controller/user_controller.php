<?php
	Class User_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new User_Model;
			$this->this_view	= new User_View;
			
			$this->required_input	= array(
				"user_name"	,
				"user_password"	,
			);
			$this->test_get	= false;
			if(isset($_GET['test_get'])){
				$this->test_get	= $_GET['test_get'];
			}
		}
		
		function register(){
			if(is_array($this->test_get)){
				$_POST['data_post']	= $this->test_get;
			}			
			if(isset($_POST['data_post'])){
				$required_input		= $this->required_input;
				$required_input[]	= 'first_name';
				$required_input[]	= 'last_name';
				$required_input[]	= 'user_phonenumber';
				$required_input[]	= 'user_birthday';
				$required_input[]	= 'user_address';
				$required_input[]	= 'user_email';
				$check_input = $this->check_input($required_input,$_POST['data_post']);				
				if(is_array($check_input)){
					$result		= $this->show_inputs_null($check_input);
				}else{
					$item_name				= "user_name";
					$info['field_name']		= $item_name;
					$info['field_value']	= $_POST['data_post'][$item_name];
					$check_user_name	= $this->this_model->check_item_exist($info);
					if($check_user_name == 0){
						$data	= $this->this_model->register($_POST['data_post']);	
						$result	= $this->this_view->return_true($data);
					}else{						
						$item_already_exist	= $this->this_view->item_already_exist($item_name);
						$result		= $this->this_view->return_false($item_already_exist);
					}	
				}
			}else{
				$info	= $this->this_view->required_input("input");
				$result		= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function login(){
			if(is_array($this->test_get)){
				$_POST['data_post']	= $this->test_get;
			}
			if(isset($_POST['data_post'])){
				$required_input	= $this->required_input;
				$check_input = $this->check_input($required_input,$_POST['data_post']);	
				if(is_array($check_input)){
					$result		= $this->show_inputs_null($check_input);
				}else{
					$item_name	= "user_name";
					$info['field_name']		= $item_name;
					$info['field_value']	= $_POST['data_post'][$item_name];

					$password	= $_POST['data_post']["user_password"];	
					
					$data	= $this->this_model->login($info,$password);	
					if($data	== null){
						$info['errors']		= array($item_name,"user_password");
						$info['message']	= array('Username and password do not match');	
						$result		= $this->this_view->return_false($info);
					}else{
						$result	= $this->this_view->return_true($data);
					}
				}
				
			}else{
				$info	= $this->this_view->required_input("input");
				$result		= $this->this_view->return_false($info);
			}
							
			return  $this->this_view->json_show($result);
		}
		
		function user_info(){
			$item_id	= $_POST['data_post']['user_id'];
			$data	= $this->this_model->item_details($item_id);
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function user_info_update(){
			$item_id	= $_POST['data_post']['user_id'];
			unset($_POST['data_post']['user_id']);
			unset($_POST['data_post']['user_name']);
			
		//	print_r(array_keys($_POST['data_post']));
			
			$fields	= $this->this_model->fields;
			foreach($fields as $k=>$v){
				$column_name = $v["column_name"];
				$check_field = in_array($column_name,array_keys($_POST['data_post']));
				if($check_field == true){
					$user_info_update[$column_name]	= $_POST['data_post'][$column_name];
				}
			//	echo "<br>";
			}
		//	$user_info_update	= $_POST['data_post'];
		//	print_r($user_info_update);
			
			$data	= $this->this_model->item_update($item_id,$user_info_update);
			if(is_numeric($data) and $data == 0) {
				$result	= $this->this_view->return_true("update success");
			}else{
				$info['errors']		= array("update");
				$info['message']	= array($data);	
				$result		= $this->this_view->return_false($info);
			}
			
			return  $this->this_view->json_show($result);
		}
		
		function show_inputs_null($check_input){
			foreach($check_input as $k=>$v){
				$errors[]	= $v['errors'];
				$message[]	= $v['message'];
			}
			$info	= array(
				'errors'	=> $errors	,
				'message'	=> $message	
			);
			return $this->this_view->return_false($info);
		}
		
		function check_input($required_input,$post_fields){
			$check_input = null;
			foreach($required_input as $k=>$v){
				if($post_fields[$v] == null){
					$check_input[]		= $this->this_view->required_input($v);
				}
			}
			return $check_input;
		}
		
		function item_details($item_id){			
			global $config_dir;
			$temp_get	= $_GET;
			
				$result	= $this->this_model->item_details($item_id);
				$district_id	= $result[item_list][0][district_id];
				
				$_GET["controller"]	= "district";			
				$_GET["action"]		= "get_a_field";
				$_GET["args"]		= array(
					"item_id"	=>	$district_id,
					"get_field"	=>	"city_id" 
				);
				include	($config_dir["core_folder"]."main.php");
				$city_id	= $main;
				$_GET["current_value"] = $city_id;
				$drop_down_list	= $this->get_drop_down_list('city');
				$this->this_view->show_module('city_list',$drop_down_list);
				
				$_GET["controller"]	= "city";			
				$_GET["action"]		= "get_a_field";
				$_GET["args"]		= array(
					"item_id"	=>	$city_id,
					"get_field"	=>	"country_id" 
				);
				include	($config_dir["core_folder"]."main.php");
				$country_id	= $main;
				if($country_id == null) $country_id = 1;
				$_GET["current_value"] = $country_id;
				$drop_down_list	= $this->get_drop_down_list('country');
				$this->this_view->show_module('country_list',$drop_down_list);
			
			$_GET		= $temp_get;
			return parent::item_details($item_id);
		}
		
		function login_form(){
			if(isset($_POST['data_post'])){
				$login = $this->login();
				$result = json_decode($login,true);
				if($result["status"] == 1){
					$status = array(
						'result' => 'login_success'
					);
					$_SESSION['user']['user_id']	= $result['data'][0]['user_id'];
					$_SESSION['user']['user_name']	= $result['data'][0]['user_name'];
				}else{
					$status = array(
						'result' => 'login_error'
					);
				}	
			}
			return $this->this_view->login_form($status);
		}
		
		function user_panel(){
			return $this->this_view->user_panel($_SESSION['user']);	
		}
		
		function logout(){
			unset($_SESSION['user']);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	