<?php
	Class Inventory_Amount_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Inventory_Amount_Model;
			$this->this_view	= new Inventory_Amount_View;
		//	$this->primary_key	= $this->this_model->item_list_model['primary_key'];
			$this->table_name	= $this->this_model->item_list_model['table_name'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
				
			/*	
			if($field_name == 'input_date' or $field_name == 'input_order_id')
				$args['table_name']	= 'input_order';
			*/
			$args['table_name']	= $this->table_name;	
			
			if($field_name == '...'){
			//	$args['table_name']	= 'partner';
			//	$this->this_model->filter_like($args);
			}else{
				$this->this_model->filter_equal($args);
			}
						
			return $result;
		}
		
		function get_inventory_amount_list(){
		//	$this->this_model->add_output_order_info();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);	
		}
	}