<?php
	Class City_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new City_Model;
			$this->this_view	= new City_View;
			
		}
		
		function get_all_city(){
			$data	= $this->this_model->get_all_city();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
	}