<?php
	Class Product_Price_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Price_Model;
			$this->this_view	= new Product_Price_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;						
			$args['table_name']	= $this->this_model->item_list_model['table_name'];			
			$this->this_model->filter_equal($args);						
			return $result;
		}
		
		function get_all_price(){
			$data	= $this->this_model->item_list();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function get_product_prices(){
			/*if(!empty($_POST['data_post']['price_id'])){
				$this->this_model->filter_price_id($_POST['data_post']['price_id']);
			}
			if(!empty($_POST['data_post']['product_id'])){
				$this->this_model->filter_product_id($_POST['data_post']['product_id']);
			}*/
			if(!empty($_POST['data_post']['warehouse_id'])){
				$this->this_model->filter_warehouse_id($_POST['data_post']['warehouse_id']);
			}
			$this->this_model->get_current_product_prices();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
			/*$output			= $this->this_model->convert_form_to_db($data['item_list'],1);
			unset($output[0]['product_id']);
			unset($output[0]['partner_id']);
			unset($output[0]['product_price_start_time']);
			unset($output[0]['product_price_end_time']);
			unset($output[0]['product_code']);
			$result	= $this->this_view->return_true($output[0]);
			return  $this->this_view->json_show($result);
			*/
		}
		
		function new_product_price(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_product_price(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_product_price(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////		
		function get_price_lines(){
			$this->this_model->filter_price_lines();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_price_line(){
			$this->this_model->filter_price_lines();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_price_line(){
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_price_line(){
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_price_line(){
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_product_id($price_id){
			$this->this_model->get_product_id($price_id);
			$data	= $this->this_model->item_list();
			$item_list	= $data['item_list'];
			foreach($item_list as $v){
				if($result != null) $result	 .= " , ".$v['product_id'];
				else 				$result	 .= $v['product_id'];
			}
			return $result;
		}
	}