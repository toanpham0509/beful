<?php
	Class Category_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Category_Model;
			$this->this_view	= new Category_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function get_product_categories(){
			$data	= $this->this_model->get_product_categories();
			foreach($data["item_list"] as $k=>$v){
				//unset($v["category_name"]);
				$output[$k]			= $this->this_model->convert_form_to_db($v,1);	
			}
			$result	= $this->this_view->return_true($output);
			return  $this->this_view->json_show($result);
		}
		
		function new_product_category(){
			$_POST['data_post']['category_type_id']	= 2;
			$add_new_result		= $this->new_an_item($_POST['data_post'],$this->primary_key);
			return $add_new_result;
		}
		
		function update_product_category(){
			$_POST['data_post']['category_type_id']	= 2;
			return $this->update_an_item($_POST['data_post'],$this->primary_key);
		}
		
		function delete_product_category(){
			$_POST['data_post']['category_type_id']	= 2;
			return $this->delete_an_item($_POST['data_post'][$this->primary_key]);
		}
		
		function get_product_category(){
			$this->this_model->add_create_time_field();
			$this->this_model->get_product_category_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
	}