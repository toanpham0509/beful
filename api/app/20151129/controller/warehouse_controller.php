<?php
	Class Warehouse_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Warehouse_Model;
			$this->this_view	= new Warehouse_View;
			
		}
		
		function get_all_warehouse(){
			$data	= $this->this_model->get_all_warehouse();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function get_warehouse_by_shop(){
			$this->this_model->filter_by_shop($_POST['data_post']['shop_id']);
			$data	= $this->this_model->get_all_warehouse();
			$result	= $this->this_view->return_true($data['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
			$args['table_name']	= $this->this_model->item_list_model['table_name'];
			if($field_name == "warehouse_id")
				$this->this_model->filter_equal($args);			
			elseif($field_name == "warehouse_name")
				$this->this_model->filter_like($args);	
		}
		
		function get_warehouses(){
			$this->this_model->get_warehouses_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result);
			return  $this->this_view->json_show($result);
		}
		
		function get_warehouse(){
			$this->this_model->get_warehouse_fields();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
		
		function new_warehouse(){
			if($_POST["data_post"]["is_runing"] == null)
				$_POST["data_post"]["is_runing"] = 0;
			$add_new_result		= $this->new_an_item($_POST['data_post'],"warehouse_id");
			$new_warehouse = json_decode($add_new_result,true);
			$new_warehouse_id = $new_warehouse[data]["warehouse_id"];
			if(is_numeric($new_warehouse_id) and $new_warehouse_id>0){
				$update_warehouse_code	= array('warehouse_code' => 'S-'.$new_warehouse_id);
				$this->this_model->item_update($new_warehouse_id,$update_warehouse_code);
			}			
			return $add_new_result;
		}
		
		function update_warehouse(){
			if($_POST["data_post"]["is_runing"] == null)
				$_POST["data_post"]["is_runing"] = 0;
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->update_an_item($_POST['data_post'],$primary_key);
		}
		
		function delete_warehouse(){
			$primary_key = $this->this_model->item_list_model['primary_key'];
			return $this->delete_an_item($_POST['data_post'][$primary_key]);
		}
	}