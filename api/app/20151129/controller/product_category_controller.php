<?php
	Class Product_Category_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Product_Category_Model;
			$this->this_view	= new Product_Category_View;
			$this->primary_key	= $this->this_model->item_list_model['primary_key'];
		}
		
		function filter_by_a_field($field_name,$field_value){
			$args["field_name"]		= $field_name;
			$args["field_value"] 	= $field_value ;
			$args['table_name']	= $this->primary_key	= $this->this_model->item_list_model['table_name'];
			$this->this_model->filter_equal($args);
			return $result;
		}
		
		function get_product_categories(){
			$product_id = $_POST['data_post']['product_id'];
			$this->this_model->filter_by_product_id($product_id);
			$data= $this->this_model->item_list();
			foreach($data['item_list'] as $k=>$v){
				unset($data['item_list'][$k]['product_id'])	;
				unset($data['item_list'][$k]['product_name'])	;
			}
			unset($data['primary_key']);
			unset($data['fields']);
			$result	= $this->this_view->return_true($data);
			return  $this->this_view->json_show($result);
		}
		
		function new_product_category(){
			$input	= $this->this_model->convert_form_to_db($_POST['data_post'],null);
			$new_item_id	= $this->this_model->item_add_new($input);
			if(is_numeric($new_item_id)){
				$data	= array('new_product_category' => $new_item_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($new_item_id);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function update_product_category(){
			$product_category_id	= $_POST['data_post']['product_category_id'];
			unset($_POST['data_post']['product_category_id']);
			
			$input	= $this->this_model->convert_form_to_db($_POST['data_post'],null);
			$update_result	= $this->this_model->item_update($product_category_id,$input);
			if(is_numeric($update_result)){
				$data	= array('product_category_id' => $product_category_id);
				$result	= $this->this_view->return_true($data);
			}else{
				$info['errors']	= array('one or some inputs');
				$info['message']= array($update_result);
				$result	= $this->this_view->return_false($info);
			}			
			return  $this->this_view->json_show($result);
		}
		
		function delete_product_category(){
			$product_category_id	= $_POST['data_post']['product_category_id'];
			$result	= $this->this_model->item_delete($product_category_id); 
			$result	= $this->this_view->return_true(null);
			return  $this->this_view->json_show($result);
		}
		
		function get_product_category(){
			$this->this_model->add_create_time_field();
			$this->this_model->get_product_categories();
			$result	= $this->item_info();
			$result	= $this->this_view->return_true($result['item_list']);
			return  $this->this_view->json_show($result);
		}
	}