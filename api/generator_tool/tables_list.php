<?php
	
	if($config_db["db_info"]["db_port"] == null)
				$config_db["db_info"]["db_port"] = "3306";
	$connection = mysqli_connect (
		$config_db["db_info"]["hostname"], 
		$config_db["db_info"]["db_user"], 
		$config_db["db_info"]["db_pass"] , 
		$config_db["db_info"]["db_name"]	,
		$config_db["db_info"]["db_port"]
	);
	if (!$connection) {
		die ( 'Could not connect...: ' . mysqli_error($connection) );
	} else {				
		mysqli_select_db($connection , $config_db["db_info"]["db_name"] );
		mysqli_query($connection, "SET NAMES 'UTF8'");
	}
	
	$sql = "show tables";
	$query = mysqli_query($connection,$sql);
	while($row  = mysqli_fetch_array($query)){
	//	  echo($table[0] . "<BR>");  
		$table[] = $row[0];
	}