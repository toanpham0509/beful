<?php
	Class Origin_Controller extends Base_Controller{
		public function __construct($cur_class) {		
			parent::__construct();
			$this->cur_class	= $cur_class;
			$this->this_model 	= new Origin_Model;
			$this->this_view	= new Origin_View;
			
		}
		
	}