<?php /* Smarty version Smarty-3.1.18, created on 2015-07-22 03:35:07
         compiled from "app\library\template\front_end_3\update_a_logger.html" */ ?>
<?php /*%%SmartyHeaderCode:165755ab5167a88239-86661167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9096f0a2752da1d022d98f98dd2677e6c2e781e7' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\update_a_logger.html',
      1 => 1437510895,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '165755ab5167a88239-86661167',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55ab5167b07859_58215532',
  'variables' => 
  array (
    'data' => 0,
    'this_view' => 0,
    'media_url_1' => 0,
    'media_url_2' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ab5167b07859_58215532')) {function content_55ab5167b07859_58215532($_smarty_tpl) {?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0]['media_url_1'];?>
<?php $_tmp1=ob_get_clean();?><?php if (empty($_tmp1)) {?>
	<?php $_smarty_tpl->tpl_vars['media_url_1'] = new Smarty_variable("/".((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['default_library'])."images/img_not_found.png", null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['media_url_1'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['this_view']->value->root_url).((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['application_folder']).((string)$_smarty_tpl->tpl_vars['data']->value['item_list'][0]['media_url_1']), null, 0);?>
<?php }?>

<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0]['media_url_2'];?>
<?php $_tmp2=ob_get_clean();?><?php if (empty($_tmp2)) {?>
	<?php $_smarty_tpl->tpl_vars['media_url_2'] = new Smarty_variable("/".((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['default_library'])."images/img_not_found.png", null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['media_url_2'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['this_view']->value->root_url).((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['application_folder']).((string)$_smarty_tpl->tpl_vars['data']->value['item_list'][0]['media_url_2']), null, 0);?>
<?php }?>
<div class="col-md-6">
	<!-- Custom Tabs -->
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li id="menu_tab_1" class="active">
			<a onclick="javascript:
				_('media_show_2').className ='hide_class';
				_('media_show_1').className ='show_class';
			" href="#tab_1" data-toggle="tab">
			Tổng 1</a>
			</li>
			<li id="menu_tab_2" class="">
			<a onclick="javascript:
				_('media_show_1').className ='hide_class';
				_('media_show_2').className ='show_class';				
			" href="#tab_2" data-toggle="tab">
			Tổng 2</a>
			</li>
			<li class="pull-right"><a class="text-muted" href="javascript:void();"
			onclick="javascript:
			this.innerHTML	= '...loading...';
			ajax_get('<?php echo $_smarty_tpl->tpl_vars['this_view']->value->root_url;?>
&controller=logger&action=value_update&args=<?php echo $_smarty_tpl->tpl_vars['data']->value['cur_time'];?>
'
			,'right_main');"
			>
			<?php echo date('d/m/Y',$_smarty_tpl->tpl_vars['data']->value['cur_time']);?>

			</a></li>
		</ul>
		<div style='margin:10px;'>
			<progress style="width:100%;display:none;" max="100" value="0" 
				id="upload_progress_bar"></progress>
			<div id="submit_status" ></div>	
		</div>
		
		<div class="tab-content">
			<div class="tab-pane   active" id="tab_1">
				<form onsubmit="javascript:
				_('submit_status').innerHTML = '...loading...'
				ajax_post(this.id,'submit_status','upload_progress_bar');
				" id="tong_1_form" method="post" action="javascript:void();">
				<div class="form-group">
				<label><?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_name'];?>
 - Tổng 1</label>					
				<input type="text" 
				<?php if ($_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_value']!=0) {?>
				value="<?php echo number_format($_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_value'],0,',','');?>
"
				<?php }?>
				name="item_field[logger_value]" class="form-control">
				</div>				
				<?php $_smarty_tpl->tpl_vars['channel'] = new Smarty_variable(1, null, 0);?>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_view']->value->front_end)."update_a_logger_extend.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
				
				<button class="btn btn-primary btn-sm" type="submit">Cập nhật tổng 1</button>
				</form>	
			</div><!-- /.tab-pane -->
			
			<div class="tab-pane " id="tab_2">
				<form onsubmit="javascript:
				_('submit_status').innerHTML = '...loading...'
				ajax_post(this.id,'submit_status','upload_progress_bar');
				" id="tong_2_form" method="post" action="javascript:void();">
				<div class="form-group">
				<label><?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_name'];?>
 - Tổng 2</label>					
				<input type="text" 
				<?php if ($_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_value_2']!=0) {?>
				value="<?php echo number_format($_smarty_tpl->tpl_vars['data']->value['item_list'][0]['logger_value_2'],0,',','');?>
"
				<?php }?>
				name="item_field[logger_value_2]" class="form-control">
				</div>				
				<?php $_smarty_tpl->tpl_vars['channel'] = new Smarty_variable(2, null, 0);?>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_view']->value->front_end)."update_a_logger_extend.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
				
				<button class="btn btn-primary btn-sm" type="submit">Cập nhật tổng 2</button>
				</form>	
			</div><!-- /.tab-pane -->
			
		</div><!-- /.tab-content -->
	
	</div><!-- nav-tabs-custom -->
</div>
<div class="col-md-6">
	<!-- Custom Tabs -->
	<div class="nav-tabs-custom">
	<div class="tab-content" id='show_media' style=''>
		<img id='media_show_1' src="<?php echo $_smarty_tpl->tpl_vars['media_url_1']->value;?>
" class='show_class'>
		<img id='media_show_2' src="<?php echo $_smarty_tpl->tpl_vars['media_url_2']->value;?>
" class='hide_class'>
	</div>
	</div>
</div>	




<?php }} ?>
