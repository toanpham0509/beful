<?php /* Smarty version Smarty-3.1.18, created on 2014-09-03 00:19:23
         compiled from "wp-content\Smarty316\mvc3\core\view\Aquarius_responsive_admin_panel\login_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:147535405304d9a9382-13084172%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ab18a5eacfa2c9aa7577b99875dea56d7d543d7' => 
    array (
      0 => 'wp-content\\Smarty316\\mvc3\\core\\view\\Aquarius_responsive_admin_panel\\login_frm.html',
      1 => 1409674761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '147535405304d9a9382-13084172',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5405304da22ba5_78188095',
  'variables' => 
  array (
    'this_class' => 0,
    'login_result' => 0,
    'status' => 0,
    'page_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5405304da22ba5_78188095')) {function content_5405304da22ba5_78188095($_smarty_tpl) {?> <style type="text/css">
	.login_frm_content{
		max-width:350px;
		min-width:300px;
		position:auto;
		margin:3% auto;
		max-height:250px;
	}
	.login_frm_content th{
		text-align:right;
	}
	.login_frm_content th, td{
		border:none;
	}
	.login_frm_content input{
		width:100%;
		border:1px solid #cccccc;
		border-radius:3px;
		padding:2px 5px;
		height:25px;
	}
	.login_frm_content table{
		border:none;
		margin-top:20px;
	}
	.login_status{
		font-style: italic; 
		margin: 10px 10px -10px;
		text-align: center;		
		padding: 5px 15px;
		border-radius: 5px; 
		border-color:white; 
		font-size: 9pt; 
		
		border-width: 1px; border-style: solid; 
		background: none repeat scroll 0% 0% rgb(234, 234, 234);
	}
	.login_successful{
		border-color: rgb(0, 0, 255);
		color: rgb(0, 0, 255); 
	}
	.login_error{
		border-color: #990000;
		color:red;
	}
 </style>
 <div >
	<div style="" id="loading_icon" ></div>
	<div id="result_background" class="pop_up_background" style="" >
		<div class="pop_up_content login_frm_content" style="" >
			<div class="pop_up_title">Login Form</div>
			<div style="padding:5px 25px;">
			<form method="POST" id="login_frm">
				
				<?php if ($_SESSION['user']) {?>
					<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable("logined", null, 0);?>
					Hello, <?php echo $_SESSION['user']['user_name'];?>

				<?php } else { ?>	
					<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
				<?php }?>	
				<?php if (($_POST['login'])) {?>
				<div class="login_status">	
					<?php $_smarty_tpl->tpl_vars['login_result'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->checkUserInfo(), null, 0);?>
					<?php if ($_smarty_tpl->tpl_vars['login_result']->value=='0') {?>
						<span class="login_error">Username and Password are not blank</span>
					<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='1') {?>
						<span class="login_error">No, Wrong Username</span>
						<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='2') {?>	
						<span class="login_successful">Login successful</span>
						<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable("logined", null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='3') {?>
						<span class="login_error">No, Wrong Password</span>
						<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
					<?php }?>	
				</div>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['status']->value=="logined") {?>
	<br>
	<image id="img_smb_frm"
src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_global->global_var['fld_code'];?>
/core/view/images/page-loader.gif" 
	/>	redirecting in 3s...			
	<script type="text/javascript">
	$('#img_smb_frm').load(function(){
		javascript:setInterval(function () { 			
					 $('#redirect_frm').submit();				
		}, 2000);
	});	
	</script>				
				<?php }?>
				
				
				
				<?php if ($_smarty_tpl->tpl_vars['status']->value=='') {?>
				<table>
					<tr>
						<th >User name:</th>
						<td><input type="text" name="user_name" value="<?php echo $_POST['user_name'];?>
"></td>
					</tr>
					<tr>
						<th>Password:</th>
						<td><input type="password" name="user_pass"></td>
					</tr>
					<tr>
						<th></th>
						<td style="text-align:right;">
						<button type="submit" name="login" value="1">Login</button></td>
					</tr>
				</table>
				<?php }?>
			</form>
			</div>
		</div>
	</div>
</div>
<form action="" id="redirect_frm">
<input type="hidden" name="page" value="categories">
</form>
<?php }} ?>
