<?php /* Smarty version Smarty-3.1.18, created on 2015-03-11 13:05:51
         compiled from "application\template\Aquarius_responsive_admin_panel\register_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1778854ffae3a597914-19191253%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84421bb8e19230916cbbba18129b10ae9c4e8724' => 
    array (
      0 => 'application\\template\\Aquarius_responsive_admin_panel\\register_frm.html',
      1 => 1426053948,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1778854ffae3a597914-19191253',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54ffae3a7658f8_80025949',
  'variables' => 
  array (
    'lang_file' => 0,
    'this_view' => 0,
    'register_new_membership' => 0,
    'args' => 0,
    'company_email' => 0,
    'password' => 0,
    'retype_password' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54ffae3a7658f8_80025949')) {function content_54ffae3a7658f8_80025949($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['company_email'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("company_email",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['register_new_membership'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("register_new_membership",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['password'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("password",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['retype_password'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("retype_password",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>

<div id="login-box" class="form-box">
	<div class="header"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['register_new_membership']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo ucwords($_tmp1);?>
</div>
	<form name="register_frm" method="post" action="" onsubmit="javascript:return isemail()">
		<?php if ($_smarty_tpl->tpl_vars['args']->value['result']=="register_success") {?>
			<div class="body bg-gray">
				<div class="form-group login_success">
					<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
,
					<a style="text-decoration: underline;" class="text-center" 
					href="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
login/">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("click_here_to_login",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a> 
				</div>
			</div>
			
		<?php } else { ?>
		<div class="body bg-gray">
			<?php if ($_smarty_tpl->tpl_vars['args']->value['result']!="not_show") {?>
			<div class="form-group login_error">
				<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
?	
			</div>
			<?php }?>
			
			<div class="form-group">
				<input type="text" placeholder='<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['company_email']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo ucfirst($_tmp2);?>
' 
				value="<?php echo $_POST['company_email'];?>
" class="form-control" name="company_email" >
			</div>
			<div class="form-group">
				<input type="password" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['password']->value;?>
<?php $_tmp3=ob_get_clean();?><?php echo ucfirst($_tmp3);?>
" 
				class="form-control" name="password" >
			</div>
			<div class="form-group">
				<input type="password" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['retype_password']->value;?>
<?php $_tmp4=ob_get_clean();?><?php echo ucfirst($_tmp4);?>
" class="form-control" 
				name="retype_password">
			</div>
		</div>
		<div class="footer">                    

			<button class="btn bg-olive btn-block" type="submit" onclick="">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator('sign_me_up',$_smarty_tpl->tpl_vars['lang_file']->value));?>
</button>

			<a class="text-center" href="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
login/">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("i_already_have_a_membership",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a>
		</div>
		
		<?php }?>
	</form>


</div>
<script type="text/javascript">
function isemail() {
    var str = document.forms["register_frm"]["company_email"].value;
	var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    if (str.indexOf(at) == -1) {
        alert("Email error");
        return false
    }
    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        alert("Email error");
        return false
    }
    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        alert("Email error");
        return false
    }
    if (str.indexOf(at, (lat + 1)) != -1) {
        alert("Email error");
        return false
    }
    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        alert("Email error");
        return false
    }
    if (str.indexOf(dot, (lat + 2)) == -1) {
        alert("Email error");
        return false
    }
    if (str.indexOf(" ") != -1) {
        alert("Email error");
        return false
    }
    return true
}
</script><?php }} ?>
