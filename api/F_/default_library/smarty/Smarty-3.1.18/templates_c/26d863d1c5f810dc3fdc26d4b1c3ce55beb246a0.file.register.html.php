<?php /* Smarty version Smarty-3.1.18, created on 2014-11-22 16:21:56
         compiled from "mvc4\core\view\AdminLTE-master\register.html" */ ?>
<?php /*%%SmartyHeaderCode:32557546af6cf22bb14-59211689%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26d863d1c5f810dc3fdc26d4b1c3ce55beb246a0' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\register.html',
      1 => 1416648111,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32557546af6cf22bb14-59211689',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_546af6cf3d8811_17495721',
  'variables' => 
  array (
    'this_class' => 0,
    'show_frm' => 0,
    'error_show' => 0,
    'pts' => 0,
    'k' => 0,
    'arr' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_546af6cf3d8811_17495721')) {function content_546af6cf3d8811_17495721($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['this_class']->value->result=="0") {?>
	<?php $_smarty_tpl->tpl_vars['show_frm'] = new Smarty_variable("not_show", null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['show_frm'] = new Smarty_variable('', null, 0);?>
	<?php if ($_smarty_tpl->tpl_vars['this_class']->value->result=="1") {?>
		<?php $_smarty_tpl->tpl_vars['error_show'] = new Smarty_variable("UserID is blank", null, 0);?>
	<?php } elseif ($_smarty_tpl->tpl_vars['this_class']->value->result=="2") {?>
		<?php $_smarty_tpl->tpl_vars['error_show'] = new Smarty_variable("Password is blank", null, 0);?>	
	<?php } elseif ($_smarty_tpl->tpl_vars['this_class']->value->result=="3") {?>
		<?php $_smarty_tpl->tpl_vars['error_show'] = new Smarty_variable("Retype password is blank", null, 0);?>		
	<?php } elseif ($_smarty_tpl->tpl_vars['this_class']->value->result=="4") {?>
		<?php $_smarty_tpl->tpl_vars['error_show'] = new Smarty_variable("These passwords don't match", null, 0);?>	
	<?php } elseif ($_smarty_tpl->tpl_vars['this_class']->value->result=="5") {?>
		<?php $_smarty_tpl->tpl_vars['error_show'] = new Smarty_variable("Someone already has that username", null, 0);?>		
	<?php }?>
<?php }?>
<div class="pop_up_background2">
<div id="login-box" class="form-box" style='margin-top:0px;'>
            <div class="header">Register New Membership</div>
			<?php if ($_smarty_tpl->tpl_vars['show_frm']->value=="not_show") {?>
				<div class="body bg-gray">
					<div class="form-group">	
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<b>Register Success!</b>  
					<a href="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'];?>
login/">
					click here to login</a>
				</div>
				</div></div>
			<?php } else { ?>
		   <form method="post" action="">
                <div class="body bg-gray">
					<?php if ($_smarty_tpl->tpl_vars['this_class']->value->result!='') {?>
						<div id="error_div" class="form-group" style=''>
						<div class="alert alert-danger alert-dismissable">
							<i class="fa fa-ban"></i>
							<button onclick="javascript:togg('error_div','form-group','hide_class');" 
							aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							<?php echo $_smarty_tpl->tpl_vars['error_show']->value;?>

						</div>
						
						</div>
					<?php }?>
					<div class="form-group">
                        <input type="text" placeholder="Full name" class="form-control" 
						name="real_name" value="<?php echo $_POST['real_name'];?>
">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="User ID" class="form-control" 
						name="user_name" value="<?php echo $_POST['user_name'];?>
" >
                    </div>
	<div class="form-group">
		<input type="text" placeholder="E-mail" class="form-control" 
		name="email" value="<?php echo $_POST['email'];?>
" >
	</div>
	<div class="form-group">
		<input type="text" placeholder="Phone - must be validated later with SMS" class="form-control" 
		name="phone_number" value="<?php echo $_POST['phone_number'];?>
" >
	</div>
	<div class="form-group">
		<?php $_smarty_tpl->createLocalArrayVariable('pts', null, 0);
$_smarty_tpl->tpl_vars['pts']->value[0] = "Profile type";?>
		<?php $_smarty_tpl->createLocalArrayVariable('pts', null, 0);
$_smarty_tpl->tpl_vars['pts']->value[1] = "Student";?>
		<?php $_smarty_tpl->createLocalArrayVariable('pts', null, 0);
$_smarty_tpl->tpl_vars['pts']->value[2] = "Teacher";?>
		<select class="form-control" name="pt_id" >
			<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['pts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"
			<?php if ($_smarty_tpl->tpl_vars['k']->value==((string)$_POST['pt_id'])) {?>
				selected="select"
			<?php }?>
			><?php echo $_smarty_tpl->tpl_vars['arr']->value;?>
</option>
			<?php } ?>
		</select>
	</div>	
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control" 
						name="password" >
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Retype password" class="form-control" 
						name="password2" >
                    </div>
					
                </div>
                <div class="footer">                    

                    <button class="btn bg-olive btn-block" type="submit" name="signup" value="1">
					Sign me up</button>
					
                    <a class="text-center" 
					href="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'];?>
login/">
					I already have a membership</a>
					<a href="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'];?>
" style="float:right;">
					Back Home</a>
                </div>
            </form>
			<?php }?>
          
        </div>
</div>		<?php }} ?>
