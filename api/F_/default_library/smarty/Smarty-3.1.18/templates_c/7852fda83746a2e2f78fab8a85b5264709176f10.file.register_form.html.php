<?php /* Smarty version Smarty-3.1.18, created on 2015-04-10 14:40:06
         compiled from "app\library\template\front_end_toan\register_form.html" */ ?>
<?php /*%%SmartyHeaderCode:158435524d9da5ae389-04447699%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7852fda83746a2e2f78fab8a85b5264709176f10' => 
    array (
      0 => 'app\\library\\template\\front_end_toan\\register_form.html',
      1 => 1428651556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '158435524d9da5ae389-04447699',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5524d9da616482_84303156',
  'variables' => 
  array (
    'register_result' => 0,
    'fields' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5524d9da616482_84303156')) {function content_5524d9da616482_84303156($_smarty_tpl) {?><form action="" method="post">
<div class="row">
	<?php if ($_smarty_tpl->tpl_vars['register_result']->value!='') {?>
		<div><?php echo $_smarty_tpl->tpl_vars['register_result']->value;?>
</div>
	<?php }?>	
	<?php if ($_smarty_tpl->tpl_vars['register_result']->value!="register_success") {?>
		
	
	<div class="col-lg-6 col-md-6" style="border-right: 1px soid #000">
    	<h4 class="text-success">Tài khoản đăng nhập</h4>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="user_name" class="label">
                	<span class="text-danger">*</span>
                	Tên đăng nhập
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="text" name="frm_field[company_email]" id="user_name" 
				value="<?php echo $_POST['frm_field']['company_email'];?>
" class="form-control" placeholder="Tên đăng nhập..." />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="user_password" class="label">
                	<span class="text-danger">*</span>
                	Mật khẩu
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="password" name="frm_field[password]" id="user_password" class="form-control" />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="user_repassword" class="label">
                	<span class="text-danger">*</span>
                	Nhập lại mật khẩu
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="password" name="frm_field[retype_password]" id="user_repassword" class="form-control" />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<br />
            	<input type="submit" name="submit" class="btn btn-danger" value="Đăng ký" />
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
    	<h4 class="text-success">Thông tin đơn vị</h4>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="company_name" class="label">
                	Tên công ty / Đơn vị
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="text" name="company_name" id="company_name" class="form-control" placeholder="Tên công ty / Đơn vị..." />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="service_type" class="label">
                	Loại hình dịch vụ
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	
				<?php echo $_smarty_tpl->tpl_vars['fields']->value['activity_list'];?>

				
            	<input type="text" name="service_type_text" id="service_type" class="form-control" 
                	placeholder="Loại hình dịch vụ..." />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="country" class="label">
                	Quốc gia
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<?php echo $_smarty_tpl->tpl_vars['fields']->value['country_list'];?>

            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="city" class="label">
                	Thành phố / Tỉnh
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8" id="column_city_id">
            	<?php echo $_smarty_tpl->tpl_vars['fields']->value['city_list'];?>

            </div>
        </div>
        <!--
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="contact_name" class="label">
                	Tên người liên hệ
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="text" name="contact_name" id="contact_name" class="form-control" 
                	placeholder="Tên người liên hệ..." />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="contact_phone" class="label">
                	Điện thoại liên hệ
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="text" name="contact_phone" id="contact_phone" class="form-control" 
                	placeholder="Điện thoại liên hệ..." />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3 col-sm-3">
            	<label for="contact_email" class="label">
                	Thư điện tử
            	</label>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8">
            	<input type="email" name="contact_email" id="contact_email" class="form-control" 
                	placeholder="Thư điện tử..." />
            </div>
        </div>
        -->
    </div>
	<?php }?>
</div>
</form>
<?php }} ?>
