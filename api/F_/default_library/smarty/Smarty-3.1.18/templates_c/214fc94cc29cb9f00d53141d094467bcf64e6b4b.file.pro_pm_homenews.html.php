<?php /* Smarty version Smarty-3.1.18, created on 2014-09-09 00:08:00
         compiled from "mvc4\view\supermarket\pro_pm_homenews.html" */ ?>
<?php /*%%SmartyHeaderCode:4247540dd460129d77-49239190%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '214fc94cc29cb9f00d53141d094467bcf64e6b4b' => 
    array (
      0 => 'mvc4\\view\\supermarket\\pro_pm_homenews.html',
      1 => 1410192246,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4247540dd460129d77-49239190',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'root_url' => 0,
    'template_fld' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_540dd4602500b7_48390130',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540dd4602500b7_48390130')) {function content_540dd4602500b7_48390130($_smarty_tpl) {?><div class="pro-pm-homenews">
    <div class="pro-pm-list">
    <ul class="ls pro-pm-list-menu">
        <li>
            <h1>
                <a class="" rel="0" href="javascript:;" title="Siêu thị điện máy MediaMart - Thế giới điện máy MediaMart - Khuyến mại sốc">
                    Sản phẩm khuyến mại</a></h1>
        </li>
        <li><h2><a href="javascript:;" rel="1" title="Sản phẩm bán chạy">Sản phẩm bán chạy</a></h2></li>
        <li><h2><a href="javascript:;" rel="2" title="Sản phẩm mới" class="active">Sản phẩm mới</a></h2></li>
        <li><a href="javascript:;" rel="0" title="Xem tất cả sản phâm khuyến mại - sản phẩm bán chạy - sản phẩm mới">
        </a></li>
    </ul>
    <div class="cl">
    </div>
    <div class="pro-pm-list-box">
            <ul class="ls pro-pm-item">
                    <li>
                        <p class="image">
                            <a href="/tivi-sharp/tv-led-sharp-lc-60le650d2-60-inch-full-hd-100hz.htm" title="TV LED SHARP LC-60LE650D2 60 inch Full HD 100Hz" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/thumb_17515_13621_tv-led-sharp-lc-60le650d2-60-inch-full-hd-100hz.jpg" title="TV LED SHARP LC-60LE650D2 60 inch Full HD 100Hz" alt="TV LED SHARP LC-60LE650D2 60 inch Full HD 100Hz">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/tivi-sharp/tv-led-sharp-lc-60le650d2-60-inch-full-hd-100hz.htm" title="TV LED SHARP LC-60LE650D2 60 inch Full HD 100Hz" target="_blank">
                                TV LED SHARP LC-60LE650D2 60 inch Full HD 100Hz
                            </a>
                        </h3>
                        <p class="price1">
                            28.990.000 đ
                        </p>
                        <p class="price2">
                            35.000.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/tivi-sharp/tv-led-sharp-lc-32le153m-32-inch-hd-ready-50hz.htm" title="TV LED SHARP LC-32LE153M 32 inch, HD Ready, 50Hz" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/13458_type2.jpg" title="TV LED SHARP LC-32LE153M 32 inch, HD Ready, 50Hz" alt="TV LED SHARP LC-32LE153M 32 inch, HD Ready, 50Hz">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/tivi-sharp/tv-led-sharp-lc-32le153m-32-inch-hd-ready-50hz.htm" title="TV LED SHARP LC-32LE153M 32 inch, HD Ready, 50Hz" target="_blank">
                                TV LED SHARP LC-32LE153M 32 inch, HD Ready, 50Hz
                            </a>
                        </h3>
                        <p class="price1">
                            4.690.000 đ
                        </p>
                        <p class="price2">
                            5.990.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/smartphones-samsung/dien-thoai-samsung-galaxy-trend-plus-s7580-white.htm" title=" Điện thoại Samsung Galaxy Trend Plus S7580 White" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/thumb_8e1dadf1-f705-4215-97ed-250a51912bec_samsung-smartphones-galaxy-trend-plus-s7580-4-loi-kep-12ghz-5mp-ram-768mb-s7580-dien-thoai-samsung-galaxy-trend-plus-s7580-white.jpg" title=" Điện thoại Samsung Galaxy Trend Plus S7580 White" alt=" Điện thoại Samsung Galaxy Trend Plus S7580 White">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/smartphones-samsung/dien-thoai-samsung-galaxy-trend-plus-s7580-white.htm" title=" Điện thoại Samsung Galaxy Trend Plus S7580 White" target="_blank">
                                 Điện thoại Samsung Galaxy Trend Plus S7580 White
                            </a>
                        </h3>
                        <p class="price1">
                            2.788.000 đ
                        </p>
                        <p class="price2">
                            3.090.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/laptop-dell/laptop-dell-ins-n3421oak14v1403204.htm" title="Laptop Dell INS N3421(OAK14V1403204)" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/10072_type2.jpg" title="Laptop Dell INS N3421(OAK14V1403204)" alt="Laptop Dell INS N3421(OAK14V1403204)">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/laptop-dell/laptop-dell-ins-n3421oak14v1403204.htm" title="Laptop Dell INS N3421(OAK14V1403204)" target="_blank">
                                Laptop Dell INS N3421(OAK14V1403204)
                            </a>
                        </h3>
                        <p class="price1">
                            8.990.000 đ
                        </p>
                        <p class="price2">
                            10.490.000 đ
                        </p>
                    </li>
            </ul>
            <ul class="ls pro-pm-item">
                    <li>
                        <p class="image">
                            <a href="/tivi-samsung/tivi-led-samsung-ua-46f5000.htm" title="TV LED SAMSUNG UA-46F5000 46 inches Full HD CMR 100Hz" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/thumb_e4df53ae-70da-4f22-a080-b07cdb931584_samsung-tv-tv-led-46-inch-full-hd-46f5000-tv-led-samsung-ua-46f5000-46-inches-full-hd-cmr-100hz.jpg" title="TV LED SAMSUNG UA-46F5000 46 inches Full HD CMR 100Hz" alt="TV LED SAMSUNG UA-46F5000 46 inches Full HD CMR 100Hz">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/tivi-samsung/tivi-led-samsung-ua-46f5000.htm" title="TV LED SAMSUNG UA-46F5000 46 inches Full HD CMR 100Hz" target="_blank">
                                TV LED SAMSUNG UA-46F5000 46 inches Full HD CMR 100Hz
                            </a>
                        </h3>
                        <p class="price1">
                            11.990.000 đ
                        </p>
                        <p class="price2">
                            16.900.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/tu-lanh-sanyo/tu-lanh-2-canh-sanyo-sr-125pn.htm" title="TỦ LẠNH 2 CÁNH SANYO SR-125PN/SS" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/834_type3.jpg" title="TỦ LẠNH 2 CÁNH SANYO SR-125PN/SS" alt="TỦ LẠNH 2 CÁNH SANYO SR-125PN/SS">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/tu-lanh-sanyo/tu-lanh-2-canh-sanyo-sr-125pn.htm" title="TỦ LẠNH 2 CÁNH SANYO SR-125PN/SS" target="_blank">
                                TỦ LẠNH 2 CÁNH SANYO SR-125PN/SS
                            </a>
                        </h3>
                        <p class="price1">
                            3.890.000 đ
                        </p>
                        <p class="price2">
                            4.467.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/dieu-hoa-nhiet-do-electrolux/dieu-hoa-1-chieu-electrolux-9000btu-esm09crf.htm" title="Điều hòa 1 chiều Electrolux 9000BTU ESM09CRF" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/13223_type3.jpg" title="Điều hòa 1 chiều Electrolux 9000BTU ESM09CRF" alt="Điều hòa 1 chiều Electrolux 9000BTU ESM09CRF">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/dieu-hoa-nhiet-do-electrolux/dieu-hoa-1-chieu-electrolux-9000btu-esm09crf.htm" title="Điều hòa 1 chiều Electrolux 9000BTU ESM09CRF" target="_blank">
                                Điều hòa 1 chiều Electrolux 9000BTU ESM09CRF
                            </a>
                        </h3>
                        <p class="price1">
                            5.990.000 đ
                        </p>
                        <p class="price2">
                            7.990.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/laptop-asus/laptop-asus-x551ca-sx125d-black.htm" title="Laptop Asus X551CA-SX125D Black" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/13173_type3.jpg" title="Laptop Asus X551CA-SX125D Black" alt="Laptop Asus X551CA-SX125D Black">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/laptop-asus/laptop-asus-x551ca-sx125d-black.htm" title="Laptop Asus X551CA-SX125D Black" target="_blank">
                                Laptop Asus X551CA-SX125D Black
                            </a>
                        </h3>
                        <p class="price1">
                            5.990.000 đ
                        </p>
                        <p class="price2">
                            6.490.000 đ
                        </p>
                    </li>
            </ul>
            <ul class="ls pro-pm-item pro-pm-item-active">
                    <li>
                        <p class="image">
                            <a href="/tivi-sharp/tv-led-sharp-lc-39le155m-39-inches-hd-ready.htm" title="TV LED SHARP LC-39LE155M 39 inch, Full HD" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/thumb_647f9b39-0926-4d92-a578-9744f19f7c1c_sharp-tv-tv-led-39-inch-hd-ready-39le155m-tv-led-sharp-lc-39le155m-39-inches-hd-ready.jpg" title="TV LED SHARP LC-39LE155M 39 inch, Full HD" alt="TV LED SHARP LC-39LE155M 39 inch, Full HD">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/tivi-sharp/tv-led-sharp-lc-39le155m-39-inches-hd-ready.htm" title="TV LED SHARP LC-39LE155M 39 inch, Full HD" target="_blank">
                                TV LED SHARP LC-39LE155M 39 inch, Full HD
                            </a>
                        </h3>
                        <p class="price1">
                            6.690.000 đ
                        </p>
                        <p class="price2">
                            8.990.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/smartphones-sony/dien-thoai-sony-xperia-c3.htm" title="Điện thoại Sony Xperia C3 Dual - D2502 White" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/13838_type4.jpg" title="Điện thoại Sony Xperia C3 Dual - D2502 White" alt="Điện thoại Sony Xperia C3 Dual - D2502 White">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/smartphones-sony/dien-thoai-sony-xperia-c3.htm" title="Điện thoại Sony Xperia C3 Dual - D2502 White" target="_blank">
                                Điện thoại Sony Xperia C3 Dual - D2502 White
                            </a>
                        </h3>
                        <p class="price1">
                            6.988.000 đ
                        </p>
                        <p class="price2">
                            7.290.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/smartphones-samsung/dien-thoai-samsung-galaxy-alpha.htm" title="Điện thoại Samsung Galaxy Alpha G850F" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/14052_type4.jpg" title="Điện thoại Samsung Galaxy Alpha G850F" alt="Điện thoại Samsung Galaxy Alpha G850F">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/smartphones-samsung/dien-thoai-samsung-galaxy-alpha.htm" title="Điện thoại Samsung Galaxy Alpha G850F" target="_blank">
                                Điện thoại Samsung Galaxy Alpha G850F
                            </a>
                        </h3>
                        <p class="price1">
                            13.988.000 đ
                        </p>
                        <p class="price2">
                            14.288.000 đ
                        </p>
                    </li>
                    <li>
                        <p class="image">
                            <a href="/laptop-dell/laptop-dell-ins-n3542a-p40f001-ti34500.htm" title="Laptop Dell INS N3542A P40F001-TI34500" target="_blank">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/13483_type4.jpg" title="Laptop Dell INS N3542A P40F001-TI34500" alt="Laptop Dell INS N3542A P40F001-TI34500">
                            </a>
                        </p>
                        <h3 class="name">
                            <a href="/laptop-dell/laptop-dell-ins-n3542a-p40f001-ti34500.htm" title="Laptop Dell INS N3542A P40F001-TI34500" target="_blank">
                                Laptop Dell INS N3542A P40F001-TI34500
                            </a>
                        </h3>
                        <p class="price1">
                            9.990.000 đ
                        </p>
                        <p class="price2">
                            10.390.000 đ
                        </p>
                    </li>
            </ul>
        <div class="cl">
        </div>
    </div>
</div>



    <div class="newshome">
    <ul class="ls newshome-title">
        <li><a href="/tin-tuc/san-pham-cong-nghe/" title="Tin công nghệ" target="_blank">
            Tin công nghệ</a> </li>
        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
            <a itemprop="url" href="/tin-tuc" title="Tin khuyến mại" target="_blank">
                <span itemprop="title">Tin khuyến mại</span>
            </a>
        </li>
        <li><a class="active" href="javascript:;" title="TVC-VIDEO">TVC-VIDEO</a> </li>
    </ul>
        <ul class="ls newshome-item">
        <li><a class="vtc-video" title="TVC-VIDEO" href="javascript:fActionYouTube('.vtc-video')" rel="http://www.youtube.com/v/GSRhRGFoeQo" rel1="GSRhRGFoeQo">
            <img width="304" height="240" alt="Video" src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/9cdb4909-6853-4802-9ab2-fe221b29eb33_tvc---website.jpg">
        </a></li>
    </ul>

</div>

    <div class="cl">
    </div>
</div>
<?php }} ?>
