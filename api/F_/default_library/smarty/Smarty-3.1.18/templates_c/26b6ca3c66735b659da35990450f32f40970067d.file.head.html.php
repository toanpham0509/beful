<?php /* Smarty version Smarty-3.1.18, created on 2014-11-22 15:19:36
         compiled from "mvc4\view\Aquarius_responsive_admin_panel\head.html" */ ?>
<?php /*%%SmartyHeaderCode:22500546418e4283366-52100019%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26b6ca3c66735b659da35990450f32f40970067d' => 
    array (
      0 => 'mvc4\\view\\Aquarius_responsive_admin_panel\\head.html',
      1 => 1416644374,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22500546418e4283366-52100019',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_546418e42852d5_90617922',
  'variables' => 
  array (
    'root_url' => 0,
    'fld_code' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_546418e42852d5_90617922')) {function content_546418e42852d5_90617922($_smarty_tpl) {?><style type='text/css'>
	.starRating{
  display        : inline-block;
  position       : relative;
  height         : 24px;
  background     : url('<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
core/view/images/stars.png') repeat-x 0 0;
  vertical-align : bottom;
}

.starRating div{
  float    : left;
  position : relative;
  height   : 24px;
}

.starRating input{
  position : relative;
  z-index  : 1;
  width    : 24px;
  height   : 24px;
  margin   : 0;
  padding  : 0;
  opacity  : 0;
}

.starRating label{
  position : absolute;
  top      : 0;
  left     : 0;
  width    : 100%;
  height   : 24px;
}

.starRating span{
  display : none;
}

.starRating       input:checked       + label,
.starRating:hover input:checked:hover + label{
  background : url('<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
core/view/images/stars.png') repeat-x 0 -24px;
}

.starRating:hover input:checked + label{
  background : transparent;
}

.starRating:hover input:hover + label{
  background : url('<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
core/view/images/stars.png') repeat-x 0 -48px;
}

	
/* end rating code */

	.form-box{
		margin-top:35px;
	}
	#loading_icon_full{
		position: fixed;
	}
	.pop_up_content {
		max-width:1500px;
	}
	.my_btn{
		background-color:#3c8dbc;
		border-radius:2px;
		box-shadow:
		0px 2px 2px 0px rgba(0, 0, 0, 0.5),
		0px 2px 2px 0px rgba(255, 255, 255, 0.5) inset; 
		padding: 10px 20px;
		color:white;
		text-decoration: none;
	}
	.my_btn:hover{
		color:white;
	}
	.my_btn:focus{
		color:white;
	}
	.my_btn:active{
		background-color:#367fa9;
		color:#ccc;
		box-shadow:
		0px 2px 2px 0px rgba(0, 0, 0, 0.5) inset,
		0px 2px 2px 0px rgba(255, 255, 255, 0.5);
	}
	
	.skin-blue .sidebar > .sidebar-menu > li.active > a {
		background: #3d9970;
		color:#ffffff;
		margin:0px 0 auto auto;
	}
	.skin-blue .sidebar > .sidebar-menu > li > a:hover {
		background: #f9f9f9;
		color:#3d9970;
		margin:0px 0 auto auto;
	}
	.textarea_adfrm{
		height:200px;
		max-height:300px;
	}
	.sidebar .sidebar-menu  {
		margin:auto auto 15px auto;
		border: 1px solid #dddddd;
	}
	.show_sm1{
		border: 1px solid #CCCCCC;
    border-radius: 3px;
    color: #333333;
    margin: 0 1px;
    padding: 1px 5px;
	}
	.col_select_multiple{
		
	}
</style><?php }} ?>
