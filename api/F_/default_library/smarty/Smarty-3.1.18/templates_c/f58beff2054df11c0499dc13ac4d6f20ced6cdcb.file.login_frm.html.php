<?php /* Smarty version Smarty-3.1.18, created on 2015-02-16 16:50:08
         compiled from "base\view\AdminLTE-master\login_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1246254cc6207bb4db8-28851059%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f58beff2054df11c0499dc13ac4d6f20ced6cdcb' => 
    array (
      0 => 'base\\view\\AdminLTE-master\\login_frm.html',
      1 => 1423732500,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1246254cc6207bb4db8-28851059',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54cc6207c2d3c9_29067506',
  'variables' => 
  array (
    'lang_file' => 0,
    'this_view' => 0,
    'sign_in' => 0,
    'args' => 0,
    'user_name' => 0,
    'user_pass' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cc6207c2d3c9_29067506')) {function content_54cc6207c2d3c9_29067506($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['user_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("user_name",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['sign_in'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("sign_in",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['user_pass'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("user_pass",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<div id="login-box" class="form-box">

	<div class="header"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['sign_in']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo ucwords($_tmp1);?>
</div>
	<form method="post" action="">
		<?php if ($_smarty_tpl->tpl_vars['args']->value['result']=="login_success") {?>
			<div class="body bg-gray">
				<div class="form-group login_success">
					<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>

<image style="height:20px;" src="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['fld_code'];?>
<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['fld_core'];?>
view/images/loading_facebook.gif"/>					
				</div>
			</div>
			<meta http-equiv="refresh" content="3; url=<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
" />
		<?php } else { ?>
		<div class="body bg-gray">
			<?php if ($_smarty_tpl->tpl_vars['args']->value['result']!="not_show") {?>
			<div class="form-group login_error">
				<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
?	
			</div>
			<?php }?>
			
			<div class="form-group">
				<input type="text" placeholder='<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo ucfirst($_tmp2);?>
' class="form-control" 
					value="<?php echo $_POST['user_name'];?>
" name="user_name" >
			</div>
			<div class="form-group">
				<input type="password" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['user_pass']->value;?>
<?php $_tmp3=ob_get_clean();?><?php echo ucfirst($_tmp3);?>
" 
				class="form-control" name="user_pass" >
			</div>          
			<div class="form-group" style="display:none;">
				<input type="checkbox" name="remember_me"> Remember me
			</div>
		</div>
		<div class="footer">                                                               
			<button class="btn bg-olive btn-block" type="submit">
			<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("sign_me_in",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</button>  
			
			<p><a href="#"><?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("forgot_password",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a></p>
			
			<a class="text-center" href="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
register/">
			<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("register_a_new_membership",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a>
		</div>
		<?php }?>
	</form>


</div><?php }} ?>
