<?php /* Smarty version Smarty-3.1.18, created on 2014-10-08 10:49:03
         compiled from "mvc4\core\view\AdminLTE-master\login_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:225154088c33d95480-69990483%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'edde09f1d31bebda04843c3e1016f9e4bd52cf62' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\login_frm.html',
      1 => 1412736367,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '225154088c33d95480-69990483',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54088c34374ad8_87372062',
  'variables' => 
  array (
    'page_class' => 0,
    'template_fld' => 0,
    'this_class' => 0,
    'url' => 0,
    'args' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54088c34374ad8_87372062')) {function content_54088c34374ad8_87372062($_smarty_tpl) {?> <style type="text/css">
	.login_frm_content{
		max-width:350px;
		min-width:300px;
		position:auto;
		margin:3% auto;
		max-height:250px;
	}
	.login_frm_content th{
		text-align:right;
		min-width:100px;
	}
	.login_frm_content th, td{
		padding:5px 10px;
		border:none;
	}
	.login_frm_content input{
		width:100%;
		border:1px solid #cccccc;
		border-radius:3px;
		padding:2px 5px;
		height:25px;
	}
	.login_frm_content table{
		border:none;
		margin-top:5px;
	}
	.login_status{
		font-style: italic; 
		margin: 10px 10px -10px;
		text-align: center;		
		padding: 5px 15px;
		border-radius: 5px; 
		border-color:white; 
		font-size: 9pt; 
		
		border-width: 1px; border-style: solid; 
		background: none repeat scroll 0% 0% rgb(234, 234, 234);
	}
	.login_successful{
		border-color: rgb(0, 0, 255);
		color: rgb(0, 0, 255); 
	}
	.login_error{
		border-color: #990000;
		color:red;
	}
 </style>
<?php if ($_SESSION['user']) {?>
		<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable("logined", null, 0);?>
		Hello, <?php echo $_SESSION['user']['user_name'];?>

	<?php } else { ?>	
		<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
	<?php }?>
 <?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['page_class']->value->cur_page->page_info['template_fld'], null, 0);?>
 <div >
	<div style="" id="loading_icon" ></div>
	<div id="result_background" class="pop_up_background" style="" >
		<div class="pop_up_content login_frm_content" style="" >
			<div class="pop_up_title">Login Form</div>
			
			<?php if ($_GET['member_login']=="1") {?>
				<div style="padding:25px 25px 10px 25px;">
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_fld']->value)."login_frm1.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
					
				</div>
			<?php } else { ?>
				<div style="padding:25px 25px 10px 25px;">
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_fld']->value)."login_frm1.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

					<!-- login_frm2.html-->
				</div>
			<?php }?>
			<?php if ($_SESSION['user']) {?>
				<div style="width:100%;text-align:center;">
				<?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['redirect_to']['home'], null, 0);?>
				<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['url']->value)."||3000||redirecting in 3s...", null, 0);?>
				<?php echo $_smarty_tpl->tpl_vars['this_class']->value->redirect_to($_smarty_tpl->tpl_vars['args']->value);?>

				</div>
			<?php }?>
			</div>
		</div>
	</div>
</div>


<?php }} ?>
