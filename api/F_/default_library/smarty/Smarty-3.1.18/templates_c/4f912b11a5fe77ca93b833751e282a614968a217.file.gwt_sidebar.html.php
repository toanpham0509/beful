<?php /* Smarty version Smarty-3.1.18, created on 2015-02-26 09:13:10
         compiled from "base\view\AdminLTE-master\gwt_sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:1651054cb0f04094ec4-56600087%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f912b11a5fe77ca93b833751e282a614968a217' => 
    array (
      0 => 'base\\view\\AdminLTE-master\\gwt_sidebar.html',
      1 => 1424916772,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1651054cb0f04094ec4-56600087',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54cb0f040e24c1_04380586',
  'variables' => 
  array (
    'page_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cb0f040e24c1_04380586')) {function content_54cb0f040e24c1_04380586($_smarty_tpl) {?>
<!-- Sidebar user panel -->


<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="active">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_page->config_page['root_url'];?>
">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Members</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_company/"><i class="fa fa-angle-double-right">
			</i>admin_company</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_client/"><i class="fa fa-angle-double-right">
			</i>admin_user</a></li>
		</ul>
	</li>
	<li class="">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_pos/">
			<i class="fa fa-edit"></i> <span>POS</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Member Card</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_member_card_category/"><i class="fa fa-angle-double-right">
			</i>admin_member_card_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_member_card/"><i class="fa fa-angle-double-right">
			</i>admin_member_card</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Stamp Card</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_category/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_card/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_card</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_history/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_history</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Coupon</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_coupon_category/"><i class="fa fa-angle-double-right">
			</i>admin_coupon_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_coupon/"><i class="fa fa-angle-double-right">
			</i>admin_coupon</a></li>
		</ul>
	</li>
	<li class="">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_news/">
			<i class="fa fa-edit"></i> <span>News</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Other</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_activity/"><i class="fa fa-angle-double-right">
			</i>admin_activity</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_civility/"><i class="fa fa-angle-double-right">
			</i>admin_civility</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_country/"><i class="fa fa-angle-double-right">
			</i>admin_country</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_city/"><i class="fa fa-angle-double-right">
			</i>admin_city</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_status/"><i class="fa fa-angle-double-right">
			</i>admin_status</a></li>
			
		</ul>
	</li>

</ul><?php }} ?>
