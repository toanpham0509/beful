<?php /* Smarty version Smarty-3.1.18, created on 2014-11-16 08:28:34
         compiled from "mvc4\view\ts\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:1607854662e3a4a94c8-92575552%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb3b905bffebc3be4b4b70d01e0bae7d63f3949d' => 
    array (
      0 => 'mvc4\\view\\ts\\item_details.html',
      1 => 1416101309,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1607854662e3a4a94c8-92575552',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54662e3ac53793_14513008',
  'variables' => 
  array (
    'this_class' => 0,
    'cur_arg' => 0,
    'cur_temp' => 0,
    'fields' => 0,
    'f_v' => 0,
    'field_id' => 0,
    'item_details' => 0,
    'f_id' => 0,
    'cur_field_id' => 0,
    'ff' => 0,
    'fv' => 0,
    'cur_fid' => 0,
    'parent_id' => 0,
    'parent_field' => 0,
    'cur_fv' => 0,
    'cur_vshow' => 0,
    'parent_value' => 0,
    'root_url' => 0,
    'img_url' => 0,
    'item_show' => 0,
    'cur_class' => 0,
    'show_accept_btn' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54662e3ac53793_14513008')) {function content_54662e3ac53793_14513008($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['item_details'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_details, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_arg'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->cur_arg, null, 0);?>
<?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>


<?php $_smarty_tpl->tpl_vars['cur_temp'] = new Smarty_variable(explode('=',$_smarty_tpl->tpl_vars['cur_arg']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_item'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_temp']->value[1], null, 0);?>

<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_details']['extend_tmp_fld'], null, 0);?>
<!--
<div style="float:left;width:100%;margin-top:0px;">
	<div style="float:left;width:100%;padding:5px 10px;min-height:300px;background:#fbfbfb;">
		
		<table>
		<?php  $_smarty_tpl->tpl_vars['f_v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f_v']->_loop = false;
 $_smarty_tpl->tpl_vars['f_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f_v']->key => $_smarty_tpl->tpl_vars['f_v']->value) {
$_smarty_tpl->tpl_vars['f_v']->_loop = true;
 $_smarty_tpl->tpl_vars['f_id']->value = $_smarty_tpl->tpl_vars['f_v']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]!="hidden") {?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="select_multiple") {?>
					<?php $_smarty_tpl->tpl_vars['cur_field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['field_id']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value), null, 0);?>
					<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = $_smarty_tpl->tpl_vars['this_class']->value->show_select_multiple(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value));?>	
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable('', null, 0);?>
					<?php  $_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fv']->_loop = false;
 $_smarty_tpl->tpl_vars['ff'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fv']->key => $_smarty_tpl->tpl_vars['fv']->value) {
$_smarty_tpl->tpl_vars['fv']->_loop = true;
 $_smarty_tpl->tpl_vars['ff']->value = $_smarty_tpl->tpl_vars['fv']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['f_v']->value[5]==((string)$_smarty_tpl->tpl_vars['ff']->value)) {?>
							<?php $_smarty_tpl->tpl_vars['parent_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['f_v']->value[5]-1, null, 0);?>
						<?php }?>
						<?php if ((($_smarty_tpl->tpl_vars['f_v']->value[4]==$_smarty_tpl->tpl_vars['fv']->value[4])&&($_smarty_tpl->tpl_vars['fv']->value[3]=="id_field"))) {?>
							<?php $_smarty_tpl->tpl_vars['cur_fid'] = new Smarty_variable($_smarty_tpl->tpl_vars['fv']->value[0], null, 0);?>
							<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['cur_fid']->value], null, 0);?>
							
						<?php }?>
					<?php } ?>	
					<?php $_smarty_tpl->tpl_vars['parent_field'] = new Smarty_variable($_smarty_tpl->tpl_vars['fields']->value[$_smarty_tpl->tpl_vars['parent_id']->value][0], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['parent_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['parent_field']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['cur_vshow'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>	
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['cur_fv']->value)."�".((string)$_smarty_tpl->tpl_vars['f_id']->value)."�".((string)$_smarty_tpl->tpl_vars['cur_vshow']->value)."�".((string)$_smarty_tpl->tpl_vars['parent_value']->value), null, 0);?>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="file") {?>
					<?php $_smarty_tpl->tpl_vars['img_url'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['item_details']->value[0]['media_url'])."/".((string)$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]), null, 0);?>
	<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = "<image style='width:100px;' src='".((string)$_smarty_tpl->tpl_vars['img_url']->value)."'>";?>				
					
					<?php $_smarty_tpl->createLocalArrayVariable('f_v', null, 0);
$_smarty_tpl->tpl_vars['f_v']->value[2] = 'file_upload_frm';?>
				<?php }?>	
				<tr>
					<th style="vertical-align:top;text-align:right;padding:5px 0;"><?php echo $_smarty_tpl->tpl_vars['f_v']->value[1];?>
</th>
					<td style="padding:5px 10px;" id="id_<?php echo $_smarty_tpl->tpl_vars['f_v']->value[0];?>
">
					
					<?php if ($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]) {?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("---", null, 0);?>
					<?php }?>

	<?php ob_start();?><?php echo $_SESSION['user']['user_name'];?>
<?php $_tmp1=ob_get_clean();?><?php if (($_smarty_tpl->tpl_vars['f_v']->value[0]=="user_name")&&($_smarty_tpl->tpl_vars['item_show']->value==$_tmp1)) {?>
		<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable('me', null, 0);?>
		<?php $_smarty_tpl->tpl_vars['show_accept_btn'] = new Smarty_variable('not_show', null, 0);?>	
	<?php } elseif ($_smarty_tpl->tpl_vars['f_v']->value[0]=="teacher_id") {?>
		<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_class']->value->get_status($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id']), null, 0);?>	
		<?php if ($_smarty_tpl->tpl_vars['item_show']->value!="OPEN") {?>
			<?php $_smarty_tpl->tpl_vars['show_accept_btn'] = new Smarty_variable('not_show', null, 0);?>	
		<?php }?>
	<?php }?>				
	<a href="javascript:void()" >				
		<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>
 
	</a>
	</td>
				</tr>
				
			<?php }?>
		<?php } ?>
		
		</table>
	<?php if ($_smarty_tpl->tpl_vars['show_accept_btn']->value!='not_show'&&($_SESSION['user']['team_id']=="0")) {?>	
		<style type='text/css'>
	
	</style>
	<div id="mbtn" style="margin:10px 50px">	
		<a  href="javascript:void();" class="my_btn"
		onclick="javascript:confirm_accept();"
		>Accept Task</a>
	</div>	
	<?php }?>
	</div>

</div>
<script type="text/javascript">

	function confirm_accept(){
		var txt;
		var r = confirm("Will you accept this taks?");
		if (r == true) {
			ajax_get('actions_list[task][0]' ,'accept_task' 
			,'args[task][0]=<?php echo $_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'];?>
' ,'id_status_name');
			document.getElementById("mbtn").innerHTML = "";
		} else {
			txt = "You pressed Cancel!";
		}
		
	}
</script>




















<?php }} ?>
