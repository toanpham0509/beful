<?php /* Smarty version Smarty-3.1.18, created on 2015-05-22 10:41:44
         compiled from "app\library\template\front_end_2\pos_info.html" */ ?>
<?php /*%%SmartyHeaderCode:20283555b08ffc59884-01824256%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a12af4a147d7da1ff1fdeab6526be9c549c5e2e6' => 
    array (
      0 => 'app\\library\\template\\front_end_2\\pos_info.html',
      1 => 1432266102,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20283555b08ffc59884-01824256',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_555b08ffd57e01_15756532',
  'variables' => 
  array (
    'this_view' => 0,
    'page_class' => 0,
    'pos_info' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_555b08ffd57e01_15756532')) {function content_555b08ffd57e01_15756532($_smarty_tpl) {?><div class="col-lg-10 col-md-10 border-left col-item">
    <div class="box-header">
        <a href="<<?php ?>?php echo BASE_URL ?<?php ?>>"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("home");?>
</a> > 
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->front_end;?>
pos"> <?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("branch_management");?>
</a> >
		<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("branch_information");?>
: <?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['pos_name'];?>

	</div>
    <br />
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
	                    <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("pos_name");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['pos_name'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("country");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['country_name'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                	<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
				<div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("city");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['city_name'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                	<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("address");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['address'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>       	
            </div>                       
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span> Zipcode
                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['zipcode'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                	<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
				<div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("opening_hour");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['opening_hour'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                	<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("closing_hour");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['closing_hour'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                	<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>            
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("email");?>

                    </label>
                </div>
                <div class="col-lg-7 col-md-7">
                    <label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['email'];?>
</label>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
			    </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <label class="label">
			            <span class="red">*</span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("phone");?>

                    </label>
                </div>
				<div class="col-lg-7 col-md-7">
					<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['phone'];?>
</label>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<label class="label">
						<span class="red">*</span> <?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("activity_name");?>

					</label>
				</div>
				<div class="col-lg-7 col-md-7">
					<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['activity_name'];?>
</label>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="badge" data-toggle="tooltip" data-placement="top" title="Tooltip">?</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<br />
					<a href="<?php echo $_smarty_tpl->tpl_vars['pos_info']->value['root_url'];?>
pos_edit&pos_id=<?php echo $_smarty_tpl->tpl_vars['pos_info']->value['pos_id'];?>
" title="Chỉnh sửa" style="float: left;">
						<button type="button" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("edit");?>
</button>
					</a>
					<a href="" title="Xóa" onClick="return confirm('Bạn có chắc chắn không?')" class="float-left">
						<button type="button" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete");?>
</button>
					</a>
					<a href="javascript:void();" onclick='javascript:
	ajax_get2("&controller=pos&action=pos_edit&args=<?php echo $_smarty_tpl->tpl_vars['pos_info']->value['pos_id'];?>
",
	"right_main"); 			
	this.innerHTML = "...loading..."; 			
					'>test</a>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
			<div class="map">
				<div id="googleMap" style="width:100%; height:200px;margin-top:10px;margin:auto;margin-top:10px;"></div>
				<br />
				<div style="text-align:center"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("map");?>
</div>
			</div>
			<br />
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="qrcode">
						<img src="http://mycouper.com/api/qrcode/images/kool-soft-192c3425f0a3a4cca70e96149b5f3ff9-145x145-255-255-255-0-0-0.jpg" />
						<br /><br />
						<div style="text-align:center">QR code</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
					<img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->front_end;?>
images/credit-card.jpg" />
				</div>
			</div>
			<br />
			<div style="text-align: left">
				<a href="" title="Tạo mới" class="float-left">
					<span class="icon-add-new"></span>
				</a>
				<a href="" title="Tải về" class="float-left">
					<span class="icon-download"></span>
				</a>
				<a href="" title="In" class="float-left">
					<span class="icon-printer"></span>
				</a>
			</div>
		</div>
    </div>
</div><?php }} ?>
