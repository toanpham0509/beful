<?php /* Smarty version Smarty-3.1.18, created on 2015-05-04 00:26:29
         compiled from "C:\xampp\htdocs\hawacom.vn\apps\default_library\AdminLTE-master\item_form.html" */ ?>
<?php /*%%SmartyHeaderCode:1545955465a45319194-49360191%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f5f170489b9ee19fd3f8c2b8f8dd5e80a8527fc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hawacom.vn\\apps\\default_library\\AdminLTE-master\\item_form.html',
      1 => 1430671729,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1545955465a45319194-49360191',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item_details' => 0,
    'this_view' => 0,
    'delete_confirm' => 0,
    'submit' => 0,
    'frm_right_content' => 0,
    'arr' => 0,
    'attach_frm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55465a4541ece9_68645247',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55465a4541ece9_68645247')) {function content_55465a4541ece9_68645247($_smarty_tpl) {?><div class="edit_form" style="float:left;width:100%;">
<div id="popup_window">
	<div style="" class="row">
	<div class="col-md-12">
		<!-- Primary box -->
		<div class="box box-solid box-primary" style="background: none repeat scroll 0 0 #eef;
		float: left;width: 100%;min-height:400px;
		">
			<div class="box-header">
				<h3 class="box-title">
				<?php if ($_smarty_tpl->tpl_vars['item_details']->value['item_id']) {?>
					<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("item_edit_title");?>

				<?php } else { ?>
					<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("add_new_frm_title");?>

				<?php }?>	
				</h3>
				<div class="box-tools pull-right">
					<button data-widget="collapse" class="hide_class btn btn-primary btn-sm"><i class="fa fa-minus"></i></button>
					<a href="" class="btn btn-primary btn-sm" style='color:white;'><i class="fa fa-times"></i></a>
				</div>
			</div>
			<div id="submit_response" class="box-body" style="display: block;
    ">
		<div class="col-md-6">
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("fields");?>
</h3>
</div>
		<div class="box-body" id="cur_item_details"><?php echo $_smarty_tpl->tpl_vars['item_details']->value['form_content'];?>
</div>
		<div class="box-footer" style="background:none;">
			<?php if ($_smarty_tpl->tpl_vars['item_details']->value['item_id']) {?>
				<?php $_smarty_tpl->tpl_vars['delete_confirm'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("delete_confirm"), null, 0);?>
				<a onclick="javascript:	
					var result = confirm('<?php echo $_smarty_tpl->tpl_vars['delete_confirm']->value;?>
');
					if (result) {
						gwt_result
						('gwt_html','<?php echo $_smarty_tpl->tpl_vars['item_details']->value['root_url'];?>
&controller=<?php echo $_smarty_tpl->tpl_vars['item_details']->value['cur_class'];?>
&action=item_del&args=<?php echo $_smarty_tpl->tpl_vars['item_details']->value['item_id'];?>
'
						,'','right_main','');
						this.innerHTML = '..deleting...';
					}
				" style="background-color: #f56954;
    color: white;
    margin-right: 15px;
    padding: 12px 25px;" class="my_btn" href="javascript:void();"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete");?>
</a>
				<?php $_smarty_tpl->tpl_vars['submit'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("update_submit"), null, 0);?>
			<?php } else { ?>	
				<?php $_smarty_tpl->tpl_vars['submit'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("add_new_submit"), null, 0);?>
			<?php }?>
			<input type="hidden" name="add_new_btn">
			<input onclick="" type="submit" style="border:none;" value="<?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
" class="my_btn">
		</div>
	</div></div>	
	<div class="col-md-6 " id="frm_right_content">
	<?php if ($_smarty_tpl->tpl_vars['frm_right_content']->value) {?>
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['frm_right_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
?>
			<?php echo $_smarty_tpl->tpl_vars['arr']->value;?>

		<?php } ?>
	<?php }?>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?>
	<div class="col-md-6 " id="attach_frm">
		
		<div class="box-primary box">
			
			<div class="box-header">
				<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("attach_file");?>
</h3>
				<div style="float:right;margin-right: 5px;">
<!--			<a  href="javascript:void();" onclick="javascript:togg('attach_frm','popup_background','hide_class')">x</a> -->
		</div>
			</div>
			<div class="box-body"><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
</div>
		</div>
	</div>	
	<?php }?>
	
	</div>
	
	</div>
	</div>
	</div>
</div>
</div>

<?php }} ?>
