<?php /* Smarty version Smarty-3.1.18, created on 2015-03-10 14:21:46
         compiled from "core\view\AdminLTE-master\gwt_table.html" */ ?>
<?php /*%%SmartyHeaderCode:621854f6d61380d5e8-55904981%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd84be1019bf12bd13a68e7296ea7c51eb1cfbbb1' => 
    array (
      0 => 'core\\view\\AdminLTE-master\\gwt_table.html',
      1 => 1425972104,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '621854f6d61380d5e8-55904981',
  'function' => 
  array (
    'field_list' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54f6d6139adf56_87338411',
  'variables' => 
  array (
    'edit_button' => 0,
    'tl_arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'primary_key' => 0,
    'lang_file' => 0,
    'this_view' => 0,
    'delete_button' => 0,
    'fields' => 0,
    'field_arr' => 0,
    'sort' => 0,
    'sort_show' => 0,
    'config_fields' => 0,
    'this_class' => 0,
    'config_table_title' => 0,
    'add_new_button' => 0,
    'item_pages' => 0,
    'item_list' => 0,
    'tbl_title' => 0,
    'config_primary_key' => 0,
  ),
  'has_nocache_code' => 0,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54f6d6139adf56_87338411')) {function content_54f6d6139adf56_87338411($_smarty_tpl) {?><?php if (!function_exists('smarty_template_function_field_list')) {
    function smarty_template_function_field_list($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['field_list']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
	<?php if ($_smarty_tpl->tpl_vars['edit_button']->value==true) {?>
		<?php if ($_smarty_tpl->tpl_vars['tl_arr']->value) {?>
		<td class='details_col'><a href="javascript:void();" onclick="javascript:
		this.innerHTML = 'loading...';
		gwt_result
		('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_update_frm&args=<?php echo $_smarty_tpl->tpl_vars['tl_arr']->value[$_smarty_tpl->tpl_vars['primary_key']->value];?>
'
			,'right_main','right_main');"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("edit_button",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>

			</a></td>
		<?php } else { ?>
			<th class='details_col'><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("details",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</th>
		<?php }?>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['delete_button']->value==true) {?>
		<?php if ($_smarty_tpl->tpl_vars['tl_arr']->value) {?>
			<td>
				<a href= "javascript:void();" class='' style="color:red;" onclick="javascript:	
					var result = confirm('<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['lang_file']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator('delete_confirm',$_tmp1);?>
');
					if (result) {
						gwt_result
						('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_del&args=<?php echo $_smarty_tpl->tpl_vars['tl_arr']->value[$_smarty_tpl->tpl_vars['primary_key']->value];?>
'
						,'','right_main','')
					}
				"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['lang_file']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator('delete_button',$_tmp2);?>
</a>
			</td>
		<?php } else { ?>
			<th class='details_col'><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</th>
		<?php }?>
	<?php }?>
	
	<?php  $_smarty_tpl->tpl_vars['field_arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_arr']->_loop = false;
 $_smarty_tpl->tpl_vars['field_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_arr']->key => $_smarty_tpl->tpl_vars['field_arr']->value) {
$_smarty_tpl->tpl_vars['field_arr']->_loop = true;
 $_smarty_tpl->tpl_vars['field_key']->value = $_smarty_tpl->tpl_vars['field_arr']->key;
?>		
		<?php if ($_smarty_tpl->tpl_vars['field_arr']->value['field_label']!=''&&$_smarty_tpl->tpl_vars['field_arr']->value['field_type']!="not_show_list") {?>
			<?php if ($_smarty_tpl->tpl_vars['tl_arr']->value) {?>
				<td><?php echo $_smarty_tpl->tpl_vars['tl_arr']->value[$_smarty_tpl->tpl_vars['field_arr']->value['tbl_col']];?>
</td>				
			<?php } else { ?>
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['field_arr']->value['tbl_col'];?>
<?php $_tmp3=ob_get_clean();?><?php if ($_GET['order_by']==$_tmp3) {?>
					<?php if ($_GET['sort']=="ASC") {?>
						<?php $_smarty_tpl->tpl_vars['sort'] = new Smarty_variable("DESC", null, 0);?>
						<?php $_smarty_tpl->tpl_vars['sort_show'] = new Smarty_variable("DESC", null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['sort'] = new Smarty_variable("ASC", null, 0);?>
						<?php $_smarty_tpl->tpl_vars['sort_show'] = new Smarty_variable("ASC", null, 0);?>
					<?php }?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['sort'] = new Smarty_variable("ASC", null, 0);?>
					<?php $_smarty_tpl->tpl_vars['sort_show'] = new Smarty_variable('', null, 0);?>
				<?php }?>
				<th><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['field_arr']->value['field_label']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
<br>
				<a href="javascript:void()" onclick="javascript:this.innerHTML = 'loading...';
				gwt_result
		('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_list&order_by=<?php echo $_smarty_tpl->tpl_vars['field_arr']->value['tbl_col'];?>
&sort=<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
','right_main','right_main');">sort <?php echo $_smarty_tpl->tpl_vars['sort_show']->value;?>
</a>
				</th>				
			<?php }?>
		<?php }?>	
	<?php } ?>
		
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

<?php ob_start();?><?php smarty_template_function_field_list($_smarty_tpl,array('fields'=>$_smarty_tpl->tpl_vars['config_fields']->value));?>
<?php $_tmp4=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['tbl_title'] = new Smarty_variable($_tmp4, null, 0);?>

<div class="row" id="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->pos;?>
_<?php echo $_smarty_tpl->tpl_vars['this_class']->value->pos_id;?>
" style="">
<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['config_table_title']->value;?>
</h3>
		</div><!-- /.box-header -->
		<div class="box-body table-responsive">
		<div style="float:left;width:100%;">
		<div style="float:left;">
		<?php if ($_smarty_tpl->tpl_vars['add_new_button']->value==true) {?>
		<a href="javascript:void();" onclick="javascript:
		this.innerHTML = 'loading...';
		gwt_result
		('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_add_new','right_main','right_main');		
		" class="btn" style="border:none;box-shadow:none;">
		<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("add_new_button",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>

		</a>
		<?php }?>
		
		</div>
		<div style="float:right;margin-bottom:0px;"><?php echo $_smarty_tpl->tpl_vars['item_pages']->value;?>
</div>
		</div><br>
		<?php if ($_smarty_tpl->tpl_vars['item_list']->value) {?>			
			<table id="" style="" class="table table-bordered table-hover">
				<thead><tr><?php echo $_smarty_tpl->tpl_vars['tbl_title']->value;?>
</tr></thead>
				<tfoot><tr><?php echo $_smarty_tpl->tpl_vars['tbl_title']->value;?>
</tr></tfoot>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['tl_arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tl_arr']->_loop = false;
 $_smarty_tpl->tpl_vars['tl_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tl_arr']->key => $_smarty_tpl->tpl_vars['tl_arr']->value) {
$_smarty_tpl->tpl_vars['tl_arr']->_loop = true;
 $_smarty_tpl->tpl_vars['tl_k']->value = $_smarty_tpl->tpl_vars['tl_arr']->key;
?>
				<tr>
					<?php smarty_template_function_field_list($_smarty_tpl,array('fields'=>$_smarty_tpl->tpl_vars['config_fields']->value,'tl_arr'=>$_smarty_tpl->tpl_vars['tl_arr']->value,'primary_key'=>$_smarty_tpl->tpl_vars['config_primary_key']->value));?>

				</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php } else { ?>
			
		<?php }?>
		<script type="text/javascript">
		
	</script>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
</div>


<?php }} ?>
