<?php /* Smarty version Smarty-3.1.18, created on 2015-01-07 21:24:12
         compiled from "gwt\application\view\Aquarius_responsive_admin_panel\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:1093354ad3ec6c0cdb1-10334962%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a3025b33f743f70846df816aef1428e29e04ae31' => 
    array (
      0 => 'gwt\\application\\view\\Aquarius_responsive_admin_panel\\item_details.html',
      1 => 1420640624,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1093354ad3ec6c0cdb1-10334962',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54ad3ec6c11783_20041344',
  'variables' => 
  array (
    'updated_status' => 0,
    'before_fields' => 0,
    'frm_content' => 0,
    'arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'item_id' => 0,
    'after_fields' => 0,
    'attach_frm' => 0,
    'this_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54ad3ec6c11783_20041344')) {function content_54ad3ec6c11783_20041344($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['updated_status']->value==false) {?>
<div class='gwt-Popup-Title'>POPUP WINDOW
	<div style='float:right;margin:-2px'><a href="" onclick="" style="color:white;">[x]</a></div>
</div>
<div class="popup_content" id="popup_window">
<?php }?>
	<div style="float:left;width:100%;margin-top:10px;margin-left: 0;" class="row">
	<div class="col-md-6" >
		<div class="box box-primary" >
			<div class="box-header">
				<h3 class="box-title">Details</h3>
			</div>
		<div class="box-body" id="cur_item_details">
		<?php echo $_smarty_tpl->tpl_vars['before_fields']->value;?>

		<table>	
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['frm_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
		<tr class="form-group">
			<th class='details_titlte'><label ><?php echo $_smarty_tpl->tpl_vars['arr']->value['field_label'];?>
</label></th>
			<td>		
				<a href="javascript:void();" onclick="javascript:gwt_result
			('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_edit_frm&args=<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_id'];?>
'
				,'popup_window','item_edit_frm','edit_frm_item')">
				<?php if ($_smarty_tpl->tpl_vars['arr']->value['cur_value']) {?>
					<?php echo $_smarty_tpl->tpl_vars['arr']->value['cur_value'];?>
 <-- click here
				<?php } else { ?>
					--- <-- click here
				<?php }?>
				</a>
			</td>
		</tr>
		<?php } ?>		
		</table>
		<?php echo $_smarty_tpl->tpl_vars['after_fields']->value;?>

		<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
<?php }?>
		</div>
		<div class="box-footer" style="text-align:center;padding: 25px;">
			<a href="javascript:void();" class='my_btn' style="background:red;"
			onclick="javascript:gwt_result
		('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=del_item&args=<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
','','popup_window','')"
			>DELETE THIS ITEM</a>
		</div>	
		</div>
	</div>
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit field</h3>
			</div>
			<div class="box-body">
				<div class="form-group" id="item_edit_frm" ></div>
			</div>
		</div>
	</div>
	
</div>
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->updated==false) {?>
</div>
<?php }?>
<?php }} ?>
