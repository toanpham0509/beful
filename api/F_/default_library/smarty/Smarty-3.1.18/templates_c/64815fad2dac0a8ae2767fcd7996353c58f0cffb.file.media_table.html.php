<?php /* Smarty version Smarty-3.1.18, created on 2014-09-25 23:24:08
         compiled from "mvc4\core\view\AdminLTE-master\media_table.html" */ ?>
<?php /*%%SmartyHeaderCode:3169554090eb6f400a0-97911759%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64815fad2dac0a8ae2767fcd7996353c58f0cffb' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\media_table.html',
      1 => 1411658645,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3169554090eb6f400a0-97911759',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54090eb745a524_42914973',
  'variables' => 
  array (
    'this_class' => 0,
    'template_fld' => 0,
    'gwt_pn' => 0,
    'item_list' => 0,
    'arri' => 0,
    'fileName' => 0,
    'file_exp' => 0,
    'file_ext' => 0,
    'root_url' => 0,
    'thumb_mf' => 0,
    'fld_code' => 0,
    'tm' => 0,
    'thumbnail' => 0,
    'i' => 0,
    'j' => 0,
    'fields' => 0,
    'arrj' => 0,
    'arr_field' => 0,
    'select_list' => 0,
    'cur_class' => 0,
    'primary_field' => 0,
    'cur_field' => 0,
    'k' => 0,
    'i2' => 0,
    'cur_table' => 0,
    'field_id' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54090eb745a524_42914973')) {function content_54090eb745a524_42914973($_smarty_tpl) {?><section class="content" id="fff1">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
	<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_list']['extend_tmp_fld'], null, 0);?>
	<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['item_list'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_list, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
<?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>

	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_fld']->value)."table_head.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
					
	<table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
		<tbody id="<?php echo $_smarty_tpl->tpl_vars['gwt_pn']->value;?>
">
			
			<?php  $_smarty_tpl->tpl_vars['arri'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arri']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arri']->key => $_smarty_tpl->tpl_vars['arri']->value) {
$_smarty_tpl->tpl_vars['arri']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['arri']->key;
?>
				<tr>
				<td style="width: 100px;">
				<?php $_smarty_tpl->tpl_vars['fileName'] = new Smarty_variable($_smarty_tpl->tpl_vars['arri']->value['media_file'], null, 0);?>
				<?php $_smarty_tpl->tpl_vars['thumb_mf'] = new Smarty_variable("thumb_".((string)$_smarty_tpl->tpl_vars['fileName']->value), null, 0);?>
				<!--
				<?php $_smarty_tpl->tpl_vars['file_exp'] = new Smarty_variable(explode(".",$_smarty_tpl->tpl_vars['fileName']->value), null, 0);?>
				<?php $_smarty_tpl->tpl_vars['file_ext'] = new Smarty_variable(strtolower(end($_smarty_tpl->tpl_vars['file_exp']->value)), null, 0);?>
				<?php if (($_smarty_tpl->tpl_vars['file_ext']->value=="jpg")||($_smarty_tpl->tpl_vars['file_ext']->value=="jpeg")||($_smarty_tpl->tpl_vars['file_ext']->value=="png")) {?>
					
					<?php $_smarty_tpl->tpl_vars['thumbnail'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['arri']->value['media_url'])."/".((string)$_smarty_tpl->tpl_vars['thumb_mf']->value), null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['thumbnail'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['fld_code']->value)."core/images/no_img.1.jpg", null, 0);?>
				<?php }?>
				--->
				<?php $_smarty_tpl->tpl_vars['tm'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['arri']->value['media_url'])."/".((string)$_smarty_tpl->tpl_vars['thumb_mf']->value), null, 0);?>
				<?php if (file_exists($_smarty_tpl->tpl_vars['tm']->value)) {?>
					<?php $_smarty_tpl->tpl_vars['thumbnail'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['arri']->value['media_url'])."/".((string)$_smarty_tpl->tpl_vars['thumb_mf']->value), null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['thumbnail'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['fld_code']->value)."core/view/images/no_img.1.jpg", null, 0);?>
				<?php }?>
				<img style="width:100px;" src="<?php echo $_smarty_tpl->tpl_vars['thumbnail']->value;?>
" />
				</td>
				<td id="<?php echo $_smarty_tpl->tpl_vars['gwt_pn']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
">
				<div style="border:none;float:left;">
				<?php  $_smarty_tpl->tpl_vars['arrj'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arrj']->_loop = false;
 $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arrj']->key => $_smarty_tpl->tpl_vars['arrj']->value) {
$_smarty_tpl->tpl_vars['arrj']->_loop = true;
 $_smarty_tpl->tpl_vars['j']->value = $_smarty_tpl->tpl_vars['arrj']->key;
?>					
					<?php if ($_smarty_tpl->tpl_vars['arrj']->value[2]!="hidden") {?>
					<div>					
					<span style="font-weight:bold;"><?php echo $_smarty_tpl->tpl_vars['arrj']->value[1];?>
:
					
					
					
					
					</span>
					<span> 					
					<?php if ($_smarty_tpl->tpl_vars['arrj']->value[2]=="select_list") {?>
						<?php  $_smarty_tpl->tpl_vars['arr_field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr_field']->_loop = false;
 $_smarty_tpl->tpl_vars['k_field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr_field']->key => $_smarty_tpl->tpl_vars['arr_field']->value) {
$_smarty_tpl->tpl_vars['arr_field']->_loop = true;
 $_smarty_tpl->tpl_vars['k_field']->value = $_smarty_tpl->tpl_vars['arr_field']->key;
?>
							<?php if (($_smarty_tpl->tpl_vars['arr_field']->value[4]==$_smarty_tpl->tpl_vars['arrj']->value[4])&&($_smarty_tpl->tpl_vars['arr_field']->value[3]=="id_field")) {?>
								<?php $_smarty_tpl->tpl_vars['field_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr_field']->value[0], null, 0);?>
								<?php $_smarty_tpl->tpl_vars['select_list'] = new Smarty_variable("
									&	args[select_list][tbl_name]=".((string)$_smarty_tpl->tpl_vars['arrj']->value[4])."
									&	args[select_list][field_id]=".((string)$_smarty_tpl->tpl_vars['arr_field']->value[0])."
									&	args[select_list][cur_value]=".((string)$_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arr_field']->value[0]])."
									&	args[select_list][field_show]=".((string)$_smarty_tpl->tpl_vars['arrj']->value[0])."
									
								", null, 0);?>
							<?php }?>
							
						<?php } ?>
						
						<?php $_smarty_tpl->tpl_vars['cur_field'] = new Smarty_variable($_smarty_tpl->tpl_vars['select_list']->value, null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['cur_field'] = new Smarty_variable("&	args[cur_text_value]	=".((string)$_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arrj']->value[0]]), null, 0);?>
					<?php }?>
					
					<?php if ($_smarty_tpl->tpl_vars['arrj']->value[2]=="file") {?>
						
						
						<a href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['arri']->value['media_url'];?>
/<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arrj']->value[0]];?>
"
						target="_blank" title="view/download file"
						>
						<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arrj']->value[0]];?>
</a>
					<?php } else { ?>
					<a title="Edit" href='javascript:Action01("edit_frm
					; class=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&args[primary_field]	=<?php echo $_smarty_tpl->tpl_vars['primary_field']->value;?>

						&	args[row_id]		=<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>

						&	args[edit_field]	=<?php echo $_smarty_tpl->tpl_vars['arrj']->value[0];?>

						
						&	args[input_type]	=<?php echo $_smarty_tpl->tpl_vars['arrj']->value[2];?>

						&	args[field_title]	=<?php echo $_smarty_tpl->tpl_vars['arrj']->value[1];?>

						<?php echo $_smarty_tpl->tpl_vars['cur_field']->value;?>
					
					;2;update_item						
					");'>
						...
						</a>
						<form method="post" id="field_<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>
" 
	onsubmit="document.getElementById('result_background').style.display	= 'block';"					
						>
			<span style='display:none;'><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[0];?>
- <?php echo $_smarty_tpl->tpl_vars['i2']->value[1];?>
- <?php echo $_smarty_tpl->tpl_vars['i2']->value[2];?>
- <?php echo $_smarty_tpl->tpl_vars['i2']->value[3];?>
- <?php echo $_smarty_tpl->tpl_vars['i2']->value[4];?>
</span>
			<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="textbox">
			<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" 
			value="media_<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>
">
			<a title="Edit" id ="link_<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>
" 
			href="javascript:asf_v3('field_<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>
','edit_frm','loading_icon','fast');"
				onclick =""
				
			><?php if ($_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arrj']->value[0]]!='') {?>
							<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['arrj']->value[0]];?>

						<?php } else { ?>
							(No text)
						<?php }?>
			</a> 
			</form>
					<?php }?>
				</span>	
				</div>
				
				<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['arrj']->value[3]=="primary_field") {?>
						<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['arrj']->value[0];?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['primary_field'] = new Smarty_variable($_tmp1, null, 0);?>
					<?php }?>
				<?php } ?>
				</div>
				<div style="float:right;margin-left:-50px;">
	<a href="javascript:ajax_get('actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]'
,'item_details'
,'args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]=<?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
.<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['arri']->value[$_smarty_tpl->tpl_vars['primary_field']->value];?>
&del_item=1'
,'show_result');"
onclick ="document.getElementById('result_background').style.display	= 'block';"
>
	<img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
core/view/images/b_drop.png" />
</a>
					</div>
				</td>
				
				</tr>
			<?php } ?>
			                                
		</tbody>
	</table>
	<div class="clear"></div>
	<table style="" id="k11">
	
	</table>




<div class="row" style="display:none1;">
	<div class="col-xs-6">
	<div class="dataTables_info" id="example1_info">Showing 1 to 10 of 57 entries</div></div>
	<div class="col-xs-6"><div class="dataTables_paginate paging_bootstrap">
	<ul class="pagination"><li class="prev disabled"><a href="#">← Previous</a></li>
	<li class="active"><a href="#">1</a></li><li><a href="#">2</a></li>
	<li><a href="#">3</a>
	</li>
	<li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next">
	<a href="#">Next → </a></li></ul></div></div></div>
	</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>

</section>
<style type="text/css">
	.table table td{
		border:none;
	}
</style>

<?php }} ?>
