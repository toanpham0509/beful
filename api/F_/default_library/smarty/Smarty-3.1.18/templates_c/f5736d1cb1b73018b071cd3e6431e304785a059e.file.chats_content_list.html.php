<?php /* Smarty version Smarty-3.1.18, created on 2014-09-22 21:50:29
         compiled from "mvc4\core\view\AdminLTE-master\chats_content_list.html" */ ?>
<?php /*%%SmartyHeaderCode:60375419b9ac580f06-22435434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5736d1cb1b73018b071cd3e6431e304785a059e' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\chats_content_list.html',
      1 => 1411393827,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '60375419b9ac580f06-22435434',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5419b9ac7c8995_15807838',
  'variables' => 
  array (
    'ctk' => 0,
    'cta' => 0,
    'this_class' => 0,
    'arr1' => 0,
    'k1' => 0,
    'from_chat' => 0,
    'cc_content' => 0,
    'title_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5419b9ac7c8995_15807838')) {function content_5419b9ac7c8995_15807838($_smarty_tpl) {?><div class="hide_class">
	<?php  $_smarty_tpl->tpl_vars['cta'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cta']->_loop = false;
 $_smarty_tpl->tpl_vars['ctk'] = new Smarty_Variable;
 $_from = $_SESSION['user']['windows_chat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cta']->key => $_smarty_tpl->tpl_vars['cta']->value) {
$_smarty_tpl->tpl_vars['cta']->_loop = true;
 $_smarty_tpl->tpl_vars['ctk']->value = $_smarty_tpl->tpl_vars['cta']->key;
?>
	<div id="chat_content_source_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
">	
		
		<?php  $_smarty_tpl->tpl_vars['arr1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr1']->_loop = false;
 $_smarty_tpl->tpl_vars['k1'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['this_class']->value->chats_content[$_smarty_tpl->tpl_vars['cta']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr1']->key => $_smarty_tpl->tpl_vars['arr1']->value) {
$_smarty_tpl->tpl_vars['arr1']->_loop = true;
 $_smarty_tpl->tpl_vars['k1']->value = $_smarty_tpl->tpl_vars['arr1']->key;
?>
			<div ><?php if ($_smarty_tpl->tpl_vars['arr1']->value['cc_from']==((string)$_SESSION['user']['user_id'])) {?>
				<?php $_smarty_tpl->tpl_vars['from_chat'] = new Smarty_variable("me", null, 0);?>
				<?php echo $_smarty_tpl->tpl_vars['this_class']->value->lastest_chat_row($_smarty_tpl->tpl_vars['cta']->value,$_smarty_tpl->tpl_vars['k1']->value);?>

			<?php } else { ?>
				<?php $_smarty_tpl->tpl_vars['from_chat'] = new Smarty_variable("<span style='color:blue;'>
				".((string)$_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['chat_to_uname'])."</span>", null, 0);?>
			<?php }?>
			
			<span class='chat_name_title'><?php echo $_smarty_tpl->tpl_vars['from_chat']->value;?>
: </span>
			<?php $_smarty_tpl->tpl_vars['cc_content'] = new Smarty_variable(str_replace('<br>','',$_smarty_tpl->tpl_vars['arr1']->value['cc_content']), null, 0);?>
			<?php $_smarty_tpl->tpl_vars['cc_content'] = new Smarty_variable(str_replace('\n','',$_smarty_tpl->tpl_vars['cc_content']->value), null, 0);?>
			<?php if ($_smarty_tpl->tpl_vars['from_chat']->value!="me") {?>
			<span style="font-style:italic;color:blue;"><?php echo $_smarty_tpl->tpl_vars['cc_content']->value;?>
</span>
			<?php } else { ?>
			<?php echo $_smarty_tpl->tpl_vars['cc_content']->value;?>

			<?php }?>
			
			</div>
			
		<?php } ?>
		<span style="display:none;">
		<?php echo $_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['lastest_row'];?>
 <br> <?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
:<?php echo $_smarty_tpl->tpl_vars['cta']->value;?>
:<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
</span>
	</div>	
	
	<span id="chat_source_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
_1">
		<input type="hidden" name="args[user][0]" value="<?php echo $_smarty_tpl->tpl_vars['cta']->value;?>
">
		
		<?php if ($_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['partner_typing_status'][1]=='2') {?>
			<?php echo $_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['chat_to_uname'];?>
 is typing ...
		<?php } else { ?>
			...
		<?php }?>
	</span>	
	
	<div id="chat_source_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
_0">
	
		
			<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo $_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['lastest_row'];?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp1>$_tmp2) {?>
				<?php $_smarty_tpl->tpl_vars['title_class'] = new Smarty_variable("chat_title_wait", null, 0);?>				
			<?php } else { ?>
				<?php $_smarty_tpl->tpl_vars['title_class'] = new Smarty_variable("chat_title", null, 0);?>
			<?php }?>
		<span style="display:none;" id="ct_id_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['title_class']->value;?>
</span>	
		<span style="display:none;" id="ct_ctu_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
" ><?php echo $_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['chat_to_uname'];?>
</span>
	
	<div style="float:left;width:100%;" class="<?php echo $_smarty_tpl->tpl_vars['title_class']->value;?>
">	
		<a  href="#" class="chat_mini_icon"
		onclick="javascript:
			togg('chat_frm_area_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
','chat_form','hide_class');
			togg('chat_content_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
','chat_content','hide_class');
			togg('waiting_status_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
','show_class','hide_class');
			"
		style="color:white;">
		<?php echo $_SESSION['user']['chat_to'][$_smarty_tpl->tpl_vars['cta']->value]['chat_to_uname'];?>

		
		<div style="float:right;width:12%;text-align:right;">--</div>
		
		</a>
		<a 
onclick="
	javascript:
		document.getElementById('chat_ajax_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
').style.display	= 'none';
		setTimeout(function(){
		document.getElementById('chat_ajax_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
').style.display	= '';
	}, 4031);
"
	href="javascript:ajax_get(
				'actions_list[user][0]'
			,	'chat_window_close'
			,	'args[user][0]=<?php echo $_smarty_tpl->tpl_vars['cta']->value;?>
;<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
'
			,	'ttt'
		);" class="chat_close_icon" style='color:white;'>x</a>
	</div>	
	</div>	
	
	<span id="chat_source_<?php echo $_smarty_tpl->tpl_vars['ctk']->value;?>
_2">
	<input type="hidden" name="args[user][0]" value="<?php echo $_smarty_tpl->tpl_vars['cta']->value;?>
">
	</span>
	<?php } ?>
	
	
	
	
	
	</div><?php }} ?>
