<?php /* Smarty version Smarty-3.1.18, created on 2014-11-17 23:45:00
         compiled from "mvc4\view\ts\task_list.html" */ ?>
<?php /*%%SmartyHeaderCode:245495465cc48e619e7-85037485%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c99e6645adb81bb4f1fdc28cc1203aa4c6dd5979' => 
    array (
      0 => 'mvc4\\view\\ts\\task_list.html',
      1 => 1416242698,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '245495465cc48e619e7-85037485',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5465cc4900e2d4_99569235',
  'variables' => 
  array (
    'this_class' => 0,
    'fields' => 0,
    'i' => 0,
    'item_list' => 0,
    'i2' => 0,
    'root_url' => 0,
    'arr' => 0,
    'k22' => 0,
    'field_id' => 0,
    'cur_table' => 0,
    'k' => 0,
    'cur_page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5465cc4900e2d4_99569235')) {function content_5465cc4900e2d4_99569235($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_list']['extend_tmp_fld'], null, 0);?>
	<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['item_list'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_list, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['front_end'])."task_list_head.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
								
		<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_rows>'0') {?>
<style type="text/css">
	th{
		color:#888888;
	}
</style>		
		<table style="margin-top:10px;" class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
			<thead>
				<tr role="row">
<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
<?php if (($_smarty_tpl->tpl_vars['i']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i']->value[3]!='not_show_list'))) {?>
<th class="sorting_asc" role="columnheader" 
				tabindex="0" aria-controls="example1" rowspan="1" colspan="1" 
				style="width: auto;" aria-sort="ascending" aria-label="Rendering 
				engine: activate to sort column descending"><?php echo $_smarty_tpl->tpl_vars['i']->value[1];?>
</th>
<?php }?>
<?php } ?>
<th class='min_width'>Details</th>
				
</tr>
			</thead>
			
			<tfoot>
				<tr>
				<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
<?php if (($_smarty_tpl->tpl_vars['i']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i']->value[3]!='not_show_list'))) {?>
				<th rowspan="1" colspan="1"><?php echo $_smarty_tpl->tpl_vars['i']->value[1];?>
</th>
<?php }?>
<?php } ?>	
<th class='min_width'>Details</th>
				
			</tfoot>
		<tbody role="alert" aria-live="polite" aria-relevant="all">
		
		
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
<tr class="even">
<?php  $_smarty_tpl->tpl_vars['i2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i2']->_loop = false;
 $_smarty_tpl->tpl_vars['k22'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i2']->key => $_smarty_tpl->tpl_vars['i2']->value) {
$_smarty_tpl->tpl_vars['i2']->_loop = true;
 $_smarty_tpl->tpl_vars['k22']->value = $_smarty_tpl->tpl_vars['i2']->key;
?>
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]=="file") {?>
		<td style="width:100px;">
		<img style="width:100px;"src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['arr']->value['media_url'];?>
/thumb_<?php echo $_smarty_tpl->tpl_vars['arr']->value['media_file'];?>
" />
		</td>
		
	<?php } elseif (($_smarty_tpl->tpl_vars['i2']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i2']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i2']->value[3]!='not_show_list'))) {?>
	<td class="col_<?php echo $_smarty_tpl->tpl_vars['i2']->value[2];?>
"> 
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]=="select_multiple") {?>
		<?php echo $_smarty_tpl->tpl_vars['this_class']->value->show_select_multiple(((string)$_smarty_tpl->tpl_vars['k22']->value).";".((string)$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value]));?>

		<span style='display:None;'>
			>><?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
<<
		<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
- <?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
 -	<?php echo $_smarty_tpl->tpl_vars['i2']->value[4];?>
 ->><?php echo $_smarty_tpl->tpl_vars['i2']->value[5];?>
 ?? <?php echo $_smarty_tpl->tpl_vars['i2']->value[6];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[7];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[8];?>

		</span>
		
	
	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]]) {?>
		<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]];?>

	<?php } else { ?>
		---
	<?php }?>	
	</td>
	<?php }?>
<?php } ?>
<td class="td_center">
<form method="post" id="frm_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" class="tbl_frm">
<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_details">
<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
.<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
">
<?php $_smarty_tpl->tpl_vars['cur_page'] = new Smarty_variable(str_replace('/','',$_GET['page']), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['cur_page']->value=="my_messages") {?>
<?php if ($_smarty_tpl->tpl_vars['arr']->value['user_name']!=$_SESSION['user']['user_name']) {?>
<a href="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'];?>
task_messages/&tb_id=<?php echo $_smarty_tpl->tpl_vars['arr']->value['tb_id'];?>

	&task_title=<?php echo $_smarty_tpl->tpl_vars['arr']->value['task_title'];?>
&chat_to_name=<?php echo $_smarty_tpl->tpl_vars['arr']->value['user_name'];?>

	" 
	onclick="">Read/reply</a> 
<?php } else { ?>(me)	
<?php }?>	
<?php } else { ?>
<a href="javascript:asf_v3('frm_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
','show_result','loading_icon','fast');"
onclick ="document.getElementById('result_background').style.display	= 'block';"
>view</a>
<?php }?>
</form>
</td>

</tr>
<?php } ?>
	
	
	</tbody></table>
	<div class="row" style="display:none;">
	<div class="col-xs-6">
	<div class="dataTables_info" id="example1_info">Showing 1 to 10 of 57 entries</div></div>
	<div class="col-xs-6"><div class="dataTables_paginate paging_bootstrap">
	<ul class="pagination"><li class="prev disabled"><a href="#">← Previous</a></li>
	<li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a>
	</li>
	<li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next">
	<a href="#">Next → </a></li></ul></div></div></div>
	<?php }?>
	</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>


<?php }} ?>
