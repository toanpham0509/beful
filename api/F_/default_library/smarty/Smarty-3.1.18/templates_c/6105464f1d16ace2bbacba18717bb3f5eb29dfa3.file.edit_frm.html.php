<?php /* Smarty version Smarty-3.1.18, created on 2014-07-28 16:34:26
         compiled from "core\html\edit_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1744253cf8fdd3a40b4-20915978%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6105464f1d16ace2bbacba18717bb3f5eb29dfa3' => 
    array (
      0 => 'core\\html\\edit_frm.html',
      1 => 1406536460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1744253cf8fdd3a40b4-20915978',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_53cf8fdd46db75_32091222',
  'variables' => 
  array (
    'args' => 0,
    'table_name' => 0,
    'field_title' => 0,
    'field_id' => 0,
    'select_list' => 0,
    'arr' => 0,
    'cur_value' => 0,
    'field_show' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53cf8fdd46db75_32091222')) {function content_53cf8fdd46db75_32091222($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['field_title'] = new Smarty_variable($_smarty_tpl->tpl_vars['args']->value['field_title'], null, 0);?>

<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['args']->value['select_list']['field_id'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['field_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['args']->value['select_list']['field_show'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['args']->value['select_list']['cur_value'], null, 0);?>

<div class="content" style="margin:0px;padding:0px;
	background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
    bottom: 0;
    display: block;
    left: 0;
    opacity: 1;
    outline: 0 none;
    padding: 0;
    position: fixed;
    right: 0;
    top: 0;
    transition: opacity 0.3s linear 0s, top 0.3s ease-out 0s;
    width: auto;
    z-index: 1050;
">	
	<div class="workplace" style="padding:10px 100px;width:500px;margin:50px auto;">
	<div class="row-fluid pop_up" id="login_form">
	<div style="
		padding:0px;
		margin:5px auto;
		width:auto;
		float:none;
		" 
		class="span6 pop_up_content">

	<div class="head">
		<div class="isw-ok"></div>
		<h1>Edit item</h1>
		<div class="clear"></div>
	</div>
<div style=" " class="block-fluid"> 
	
	<div id="reg_frm_content">	
	
<div class="row-form">
	<input type="hidden" name="table_name" value="<?php echo $_smarty_tpl->tpl_vars['table_name']->value;?>
">
	<input type="hidden" name="row_id" value="<?php echo $_smarty_tpl->tpl_vars['args']->value['row_id'];?>
">
	
	<input type="hidden" name="primary_field" value="<?php echo $_smarty_tpl->tpl_vars['args']->value['primary_field'];?>
">
	<input type="hidden" name="field_title" value="<?php echo $_smarty_tpl->tpl_vars['field_title']->value;?>
">
	
	<div style="width:100%;">
	<div style="" class="span3"><?php echo $_smarty_tpl->tpl_vars['field_title']->value;?>
</div>
	<div class="span9">        
	<?php if ($_smarty_tpl->tpl_vars['args']->value['input_type']=="select_list") {?>
		<input type="hidden" name="field_id" value="<?php echo $_smarty_tpl->tpl_vars['args']->value['select_list']['field_id'];?>
">
		<select name="<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
">
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['kk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['select_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['kk']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>			
			<option value="<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
"
				<?php if ($_smarty_tpl->tpl_vars['cur_value']->value==$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value]) {?>
					selected = "select"
				<?php }?>
			><?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_show']->value];?>
</option>	
		<?php } ?>
		</select>
	<?php } elseif ($_smarty_tpl->tpl_vars['args']->value['input_type']=="textbox") {?>
		<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['args']->value['cur_text_value'];?>
" name="<?php echo $_smarty_tpl->tpl_vars['args']->value['edit_field'];?>
">
		<input type="hidden" name="field_id" value="<?php echo $_smarty_tpl->tpl_vars['args']->value['edit_field'];?>
">
	<?php }?>
	<div style="display:none;">
	<hr>
	<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['args']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
		<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
: <?php echo $_smarty_tpl->tpl_vars['arr']->value;?>
<br>
	<?php } ?>
	</div>
	</div>
	</div>	
	
		
	<div class="clear"></div>
	
	
	
	
	
</div>
<div class="row-form">
	<div class="footer" style="text-align:right;">
		<button type="submit" class="btn"  onclick="javascript:
			Action03([
					action_call('Action02','1;action_default','0')
				,	action_call('Action01','action_default;0;0;action_default','1000')
				
			]);
		">Submit</button>
		<span style="" id="add_result">
			<button id="close_btn" type="button" class="btn btn-danger link_bcPopupList"
			onclick="Action02('0')">Close</button>
		</span>
		
	</div>
	

	
</div>
 
		
	</div>	
</div>	
</div></div></div></div>

	



<?php }} ?>
