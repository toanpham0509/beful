<?php /* Smarty version Smarty-3.1.18, created on 2015-01-06 20:08:59
         compiled from "gwt\core_v2\view\AdminLTE-master\gwt_sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:9202549a9cfacaf3e8-31220644%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd46b945cdc8ab422357f49849e5f026cd3ff3b2' => 
    array (
      0 => 'gwt\\core_v2\\view\\AdminLTE-master\\gwt_sidebar.html',
      1 => 1420549738,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9202549a9cfacaf3e8-31220644',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_549a9cfacd0ae2_07441092',
  'variables' => 
  array (
    'page_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_549a9cfacd0ae2_07441092')) {function content_549a9cfacd0ae2_07441092($_smarty_tpl) {?>
<!-- Sidebar user panel -->
<div class="user-panel">
	<div class="pull-left image">
		<img src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->template_fld;?>
img/avatar3.png" class="img-circle" alt="User Image" />
	</div>
	<div class="pull-left info">
		<p>Hello, Admin </p>

		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
		<input type="text" name="q" class="form-control" placeholder="Search..."/>
		<span class="input-group-btn">
			<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
		</span>
	</div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="active">
		<a href="index.html">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Nguyên liệu, đồ uống</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nldu/"><i class="fa fa-angle-double-right">
			</i>Danh sách, tồn kho, chi tiết xuất nhập</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nhapkho/"><i class="fa fa-angle-double-right">
			</i>Nhập hàng</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_xuatkho/"><i class="fa fa-angle-double-right">
			</i>Xuất hàng</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nldu_dvt/"><i class="fa fa-angle-double-right">
			</i>Đơn vị tính</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Món ăn</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nhomthucdon/"><i class="fa fa-angle-double-right">
			</i>Nhóm thực đơn</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_monan/"><i class="fa fa-angle-double-right">
			</i>Thực đơn</a></li>
			
		</ul>
	</li>

</ul><?php }} ?>
