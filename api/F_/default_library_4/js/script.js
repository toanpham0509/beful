	function _(el){
		return document.getElementById(el);
	}
	function togg(div_id,class_1,class_2) {
		var whichpost = _(div_id);
		if (whichpost.className==class_2) { whichpost.className=class_1; } 
		else { whichpost.className=class_2; }
	}
	function ajax_get(url,show_position){
		if (window.XMLHttpRequest){ // code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}else{ // code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200){				
				_(show_position).innerHTML=xmlhttp.responseText;
				
			}
		}
		xmlhttp.open("GET",url,true);
		xmlhttp.send();
	}
	
	

	function get_ajax_url(form_id){
		var url = "";
		var tmp = "";
		var elements = _(form_id).elements;
		for (i=0; i<elements.length; i++){
			if(escapeHtml(elements[i].value) != ""){
				if(url === ""){
					tmp = "?";
				}
				else{
					tmp="&";
				}
				url = url + tmp + elements[i].name + "=" + escapeHtml(elements[i].value);
			}
		}
		return url;
	}

	function progressHandler(event,target,upload_progress_bar){
		_(upload_progress_bar).style.display	= 'block';
		//_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
		var percent = (event.loaded / event.total) * 100;
		_(upload_progress_bar).value = Math.round(percent);
		_(target).innerHTML = Math.round(percent)+"% uploaded... please wait";
	}
	function completeHandler(event,target,upload_progress_bar){
		_(target).innerHTML = event.target.responseText;
		_(upload_progress_bar).value = 0;
		_(upload_progress_bar).style.display	= 'none';
	}
	function errorHandler(event,target,upload_progress_bar){
		_(target).innerHTML = "Upload Failed";
	}
	function abortHandler(event,target,upload_progress_bar){
		_(target).innerHTML = "Upload Aborted";
	}
	function escapeHtml(str) {
		if (typeof(str) == "string") {
			str = str.replace(/&/g, "%26"); /* must do &amp; first */
			str = str.replace(/'/g, "%27");
		}
		return str;
	}
	function ajax_post(form_id, target , upload_progress_bar,file_id){
		var ajax = new XMLHttpRequest();
		var formdata = new FormData();
		var elements = _(form_id).elements;
		for (i=0; i<elements.length; i++){
			if(elements[i].value != ""){
				formdata.append(elements[i].name, elements[i].value);
				if(elements[i].files){
					formdata.append(elements[i].name, elements[i].files[0]);
				}
			}
		}
		
		if(file_id){
			var file_upload = _(file_id);
			var file = file_upload.files[0];
			formdata.append(file_upload.name, file);
		}
		
		ajax.upload.addEventListener("progress", function (event){
			progressHandler(event,target,upload_progress_bar);
		}, false);
		ajax.addEventListener("load", function(event){
			completeHandler(event,target,upload_progress_bar);
		}, false);
		ajax.addEventListener("error", function(event){
			errorHandler(event,target,upload_progress_bar);
		}, false);
		ajax.addEventListener("abort", function(event){
			abortHandler(event,target,upload_progress_bar);
		}, false);
		
		ajax.open("POST", "");
		ajax.send(formdata);
	}
	
	function get_item_list____(class_name,div_status,div_show) {
		var elements = document.getElementsByClassName(class_name);
		var i = 0;
		t	= setInterval(function(){
			if (elements[i] != undefined){			
				$("#" + elements[i].id).mouseover(); 
				
				_(div_status).className	= "alert alert-warning alert-dismissable";
				_(div_status).innerHTML= "<i class='fa fa-warning'></i>loading..."+i+"s"; 
			}else{
			//	document.getElementById(div_status).innerHTML= "all code segment finished running: "+i; 
				_(div_status).className	= 'hide_class';
				clearInterval(t);
				_(div_show).className	= "show_class";
			} //end interval, stops run
			i++; //segment of code finished running, next...
		},1000);	
	}
	
	function bootstrap_datepicker(element){
    //       $("#"+datepicker222).datepicker();
     //   $("#"+datepicker222).datepicker("show");
	//		alert(element.id);
		$(element).datepicker({
			todayBtn: "linked",
			autoclose: true,
			todayHighlight: true,
			format: 'dd/mm/yyyy'
		});
		$(element).datepicker("show");
	}

