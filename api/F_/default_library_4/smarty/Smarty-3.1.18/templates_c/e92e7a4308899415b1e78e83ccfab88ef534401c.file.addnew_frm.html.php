<?php /* Smarty version Smarty-3.1.18, created on 2014-11-18 23:18:07
         compiled from "mvc4\core\view\AdminLTE-master\addnew_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1066154087fb12c2181-16715633%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e92e7a4308899415b1e78e83ccfab88ef534401c' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\addnew_frm.html',
      1 => 1416327483,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1066154087fb12c2181-16715633',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54087fb155eb14_00797506',
  'variables' => 
  array (
    'this_class' => 0,
    'cur_tbl' => 0,
    'fields' => 0,
    'arr' => 0,
    'k' => 0,
    'arr2' => 0,
    'select_tbl' => 0,
    'cur_arg' => 0,
    'method' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54087fb155eb14_00797506')) {function content_54087fb155eb14_00797506($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['fields'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_tbl'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>

<form method="POST" id="add_new_frm" 
onsubmit="document.getElementById('result_background').style.display	= 'none';">
<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="addnew_item">
<input type="hidden" name="cur_tbl" value="<?php echo $_smarty_tpl->tpl_vars['cur_tbl']->value;?>
">
<table style="float:left;width:100%;margin-top:10px;">

<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
	<?php if (($_smarty_tpl->tpl_vars['arr']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['arr']->value[2]!='show_only')) {?>
	<tr>
	<th style='text-align:right;border:none;width:150px;vertical-align:top;
	padding:10px 0 0 0;
	'><?php echo $_smarty_tpl->tpl_vars['arr']->value[1];?>
</th>
	<td style="border:none;padding:5px 5px;">
		<div id="an_field_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"  >
	
	<div style="padding: 0px 10px;" id="an_field_content_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
	<?php if ($_smarty_tpl->tpl_vars['arr']->value[2]=='select_list') {?>
		<?php $_smarty_tpl->tpl_vars['select_tbl'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[4], null, 0);?>
		<?php $_smarty_tpl->tpl_vars['cur_field_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[0], null, 0);?>
		<?php  $_smarty_tpl->tpl_vars['arr2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr2']->_loop = false;
 $_smarty_tpl->tpl_vars['k2'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr2']->key => $_smarty_tpl->tpl_vars['arr2']->value) {
$_smarty_tpl->tpl_vars['arr2']->_loop = true;
 $_smarty_tpl->tpl_vars['k2']->value = $_smarty_tpl->tpl_vars['arr2']->key;
?>
			<?php if (($_smarty_tpl->tpl_vars['arr2']->value[4]==$_smarty_tpl->tpl_vars['select_tbl']->value)&&($_smarty_tpl->tpl_vars['arr2']->value[3]=='id_field')) {?>
				<?php $_smarty_tpl->tpl_vars['cur_field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr2']->value[0], null, 0);?>
			<?php }?>	
		<?php } ?>
		<?php $_smarty_tpl->tpl_vars['cur_arg'] = new Smarty_variable("0»".((string)$_smarty_tpl->tpl_vars['k']->value)."»", null, 0);?>
		<?php echo $_smarty_tpl->tpl_vars['this_class']->value->select_list($_smarty_tpl->tpl_vars['cur_arg']->value);?>

	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[2]=='textarea') {?>
		<textarea name="cur_field[<?php echo $_smarty_tpl->tpl_vars['arr']->value[0];?>
]" style="max-width:500px;width:100%;" class="textarea_adfrm form-control"></textarea>
	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[2]=='textbox') {?>
		<input class="form-control" type="text" name="cur_field[<?php echo $_smarty_tpl->tpl_vars['arr']->value[0];?>
]" value="" 
		style="width:100%;max-width:500px;">
	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[2]=="file") {?>	
		<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_class']->value->class_info['addnew_frm']['extend_tmp_fld'])."upload_frm.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	<?php } else { ?>
		<?php $_smarty_tpl->tpl_vars['method'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[2], null, 0);?>		
		<?php $_tmp1=$_smarty_tpl->tpl_vars['method']->value;?><?php echo $_smarty_tpl->tpl_vars['this_class']->value->$_tmp1($_smarty_tpl->tpl_vars['k']->value);?>

		
	<?php }?>
	</div>
	</div>
	</td>
	
	
	
	</tr>
	<?php }?>
<?php } ?>

</table>
</form>
<div style="
	 float: left;
    padding: 10px 165px;
    text-align: left;
    width: 100%;
"><button 
onclick="javascript:asf_v3('add_new_frm','fff1','loading_icon','fast');">Submit</button>
</div>
<script type="text/javascript">
	document.getElementById('add_new_frm').onkeydown = function(e){
	   if(e.keyCode == 13){
			asf_v3('add_new_frm','fff1','loading_icon','fast');
	   }
	};
</script><?php }} ?>
