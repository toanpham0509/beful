<?php /* Smarty version Smarty-3.1.18, created on 2015-01-26 16:23:38
         compiled from "gwt\core_v2\view\AdminLTE-master\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:29376549be4ad0a7310-51599799%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7f482e01d833462e167fa32cc85bd64b9e32e0b' => 
    array (
      0 => 'gwt\\core_v2\\view\\AdminLTE-master\\item_details.html',
      1 => 1422264213,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29376549be4ad0a7310-51599799',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_549be4ad1f9656_50880610',
  'variables' => 
  array (
    'updated_status' => 0,
    'before_fields' => 0,
    'frm_content' => 0,
    'arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'item_id' => 0,
    'after_fields' => 0,
    'attach_frm' => 0,
    'this_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_549be4ad1f9656_50880610')) {function content_549be4ad1f9656_50880610($_smarty_tpl) {?><meta charset="UTF-8">
<link href="/01/ligker/gwt/core_v2/view/css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/01/ligker/gwt/core_v2/view/js/script.js"></script>
<script src="/01/ligker/gwt/core_v2/view/gwt/gwt_tool/war/gwt_tool/gwt_tool.nocache.js"></script>
<div id="popup_div"></div>




<?php if ($_smarty_tpl->tpl_vars['updated_status']->value==false) {?>
<div class='gwt-Popup-Title'>POPUP WINDOW
	<div style='float:right;margin:-2px'><a href="" onclick="" style="color:white;">[x]</a></div>
</div>
<div class="popup_content" id="popup_window">
<?php }?>
	<div style="float:left;width:100%;margin-top:10px;margin-left: 0;" class="row">
	<div class="col-md-6" >
		<div class="box box-primary" >
			<div class="box-header">
				<h3 class="box-title">Details</h3>
			</div>
		<div class="box-body" id="cur_item_details">
		<?php echo $_smarty_tpl->tpl_vars['before_fields']->value;?>

		<table>	
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['frm_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
		<tr class="form-group">
			<th class='details_titlte'><label ><?php echo $_smarty_tpl->tpl_vars['arr']->value['field_label'];?>
</label></th>
			<td>		
				<a href="javascript:void();" onclick="javascript:gwt_result
			('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_edit&args=<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_id'];?>
;<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['frm_type'];?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_name'];?>
'
				,'popup_window','item_edit_frm','edit_frm_item')">
				<?php if ($_smarty_tpl->tpl_vars['arr']->value['cur_value']) {?>
					<?php echo $_smarty_tpl->tpl_vars['arr']->value['cur_value'];?>
 <-- click here
				<?php } else { ?>
					--- <-- click here
				<?php }?>
				</a>
			</td>
		</tr>
		<?php } ?>		
		</table>
		<?php echo $_smarty_tpl->tpl_vars['after_fields']->value;?>

		<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
<?php }?>
		</div>
		<div class="box-footer" style="text-align:center;padding: 25px;">
			<a href="javascript:void();" class='my_btn' style="background:red;"
			onclick="javascript:gwt_result
		('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=del_item&args=<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
','','popup_window','')"
			>DELETE THIS ITEM</a>
		</div>	
		</div>
	</div>
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit field</h3>
			</div>
			<div class="box-body">
				<div class="form-group" id="item_edit_frm" ></div>
			</div>
		</div>
	</div>
	
</div>
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->updated==false) {?>
</div>
<?php }?>
<?php }} ?>
