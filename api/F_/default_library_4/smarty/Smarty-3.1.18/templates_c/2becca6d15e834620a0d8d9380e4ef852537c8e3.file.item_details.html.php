<?php /* Smarty version Smarty-3.1.18, created on 2014-11-21 20:58:35
         compiled from "mvc4\core\view\AdminLTE-master\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:438454087e52f29ec0-07963223%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2becca6d15e834620a0d8d9380e4ef852537c8e3' => 
    array (
      0 => 'mvc4\\core\\view\\AdminLTE-master\\item_details.html',
      1 => 1416578313,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '438454087e52f29ec0-07963223',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54087e53472726_77869756',
  'variables' => 
  array (
    'this_class' => 0,
    'cur_arg' => 0,
    'cur_temp' => 0,
    'fields' => 0,
    'f_v' => 0,
    'field_id' => 0,
    'item_details' => 0,
    'f_id' => 0,
    'cur_field_id' => 0,
    'ff' => 0,
    'fv' => 0,
    'cur_fid' => 0,
    'parent_id' => 0,
    'parent_field' => 0,
    'cur_fv' => 0,
    'cur_vshow' => 0,
    'parent_value' => 0,
    'root_url' => 0,
    'img_url' => 0,
    'item_show' => 0,
    'args' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54087e53472726_77869756')) {function content_54087e53472726_77869756($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['item_details'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_details, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_arg'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->cur_arg, null, 0);?>
<?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>


<?php $_smarty_tpl->tpl_vars['cur_temp'] = new Smarty_variable(explode('=',$_smarty_tpl->tpl_vars['cur_arg']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_item'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_temp']->value[1], null, 0);?>

<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_details']['extend_tmp_fld'], null, 0);?>

<div class="row" style="float:left;width:100%;margin-top:10px;margin-left: 0;">
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Details</h3>
			</div>
			<div class="box-body">
		<table>
		<?php  $_smarty_tpl->tpl_vars['f_v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f_v']->_loop = false;
 $_smarty_tpl->tpl_vars['f_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f_v']->key => $_smarty_tpl->tpl_vars['f_v']->value) {
$_smarty_tpl->tpl_vars['f_v']->_loop = true;
 $_smarty_tpl->tpl_vars['f_id']->value = $_smarty_tpl->tpl_vars['f_v']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]!="hidden") {?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="select_multiple") {?>
					<?php $_smarty_tpl->tpl_vars['cur_field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['field_id']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value), null, 0);?>
					<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = $_smarty_tpl->tpl_vars['this_class']->value->show_select_multiple(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value));?>	
				<?php } elseif ($_smarty_tpl->tpl_vars['f_v']->value[2]=="change_pass") {?>
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['cur_field_id']->value), null, 0);?>
					<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = "Change";?> 
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable('', null, 0);?>
					<?php  $_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fv']->_loop = false;
 $_smarty_tpl->tpl_vars['ff'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fv']->key => $_smarty_tpl->tpl_vars['fv']->value) {
$_smarty_tpl->tpl_vars['fv']->_loop = true;
 $_smarty_tpl->tpl_vars['ff']->value = $_smarty_tpl->tpl_vars['fv']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['f_v']->value[5]==((string)$_smarty_tpl->tpl_vars['ff']->value)) {?>
							<?php $_smarty_tpl->tpl_vars['parent_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['f_v']->value[5]-1, null, 0);?>
						<?php }?>
						<?php if ((($_smarty_tpl->tpl_vars['f_v']->value[4]==$_smarty_tpl->tpl_vars['fv']->value[4])&&($_smarty_tpl->tpl_vars['fv']->value[3]=="id_field"))) {?>
							<?php $_smarty_tpl->tpl_vars['cur_fid'] = new Smarty_variable($_smarty_tpl->tpl_vars['fv']->value[0], null, 0);?>
							<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['cur_fid']->value], null, 0);?>
							
						<?php }?>
					<?php } ?>	
					<?php $_smarty_tpl->tpl_vars['parent_field'] = new Smarty_variable($_smarty_tpl->tpl_vars['fields']->value[$_smarty_tpl->tpl_vars['parent_id']->value][0], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['parent_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['parent_field']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['cur_vshow'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>	
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['cur_fv']->value)."»".((string)$_smarty_tpl->tpl_vars['f_id']->value)."»".((string)$_smarty_tpl->tpl_vars['cur_vshow']->value)."»".((string)$_smarty_tpl->tpl_vars['parent_value']->value), null, 0);?>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="file") {?>
					<?php $_smarty_tpl->tpl_vars['img_url'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['item_details']->value[0]['media_url'])."/".((string)$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]), null, 0);?>
	<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = "<image style='width:100px;' src='".((string)$_smarty_tpl->tpl_vars['img_url']->value)."'>";?>				
					
					<?php $_smarty_tpl->createLocalArrayVariable('f_v', null, 0);
$_smarty_tpl->tpl_vars['f_v']->value[2] = 'file_upload_frm';?>
				<?php }?>	
				<tr>
					<th style="vertical-align:top;text-align:right;padding:5px 0;"><?php echo $_smarty_tpl->tpl_vars['f_v']->value[1];?>
</th>
					<td style="padding:5px 10px;">
					
					<?php if ($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]) {?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("---", null, 0);?>
					<?php }?>
	<?php if ($_GET['del_item']=='1') {?>
			<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>

		<?php } else { ?>				
	<a href="javascript:asf_v3('field_<?php echo $_smarty_tpl->tpl_vars['f_id']->value;?>
','edit_frm','loading_icon','fast');">				
					<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>
 
	</a>
	<form method="POST" id="field_<?php echo $_smarty_tpl->tpl_vars['f_id']->value;?>
">
		<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]"
		value= "<?php echo $_smarty_tpl->tpl_vars['f_v']->value[2];?>
">
	<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" 
		value="<?php echo $_smarty_tpl->tpl_vars['args']->value;?>
">
	</form>
		<?php }?>
					</td>
				</tr>
				
			<?php }?>
		<?php } ?>
		</table>
		</div>
		
	</div>
	</div>
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit</h3>
			</div>
			<div class="box-body">
		<?php if ($_GET['del_item']=='1') {?>
			<form method="POST" id="del_frm">
	You are deleting this item...
	<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_delete">			
	<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->cur_arg;?>
">
	</form>
	<button style="color:red;" id="del_btn"
	onclick="javascript:asf_v3('del_frm','show_result','loading_icon','fast');">Delete</button>
	</div>
		<?php } else { ?>
		<form method="POST" id="update_frm">
		<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_update">			
		<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->cur_arg;?>
">
		<div id="edit_frm" ></div>
		</form>
		
		</div>
	<div class="box-footer"><button class="my_btn" style="display:none;border:none;" id="smb_btn"
		onclick="javascript:asf_v3('update_frm','show_result','loading_icon','fast');">
		Submit</button>
		<script type="text/javascript">
			document.getElementById('update_frm').onkeydown = function(e){
			   if(e.keyCode == 13){
					asf_v3('update_frm','show_result','loading_icon','fast');
			   }
			};
		</script></div>
		<?php }?>
	
	</div>
	</div>
</div>




















<?php }} ?>
