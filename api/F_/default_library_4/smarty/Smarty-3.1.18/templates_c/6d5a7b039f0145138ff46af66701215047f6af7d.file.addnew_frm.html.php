<?php /* Smarty version Smarty-3.1.18, created on 2014-09-01 22:19:20
         compiled from "wp-content\Smarty316\mvc3\core\view\Aquarius_responsive_admin_panel\addnew_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1242454043b4b228d42-35140918%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d5a7b039f0145138ff46af66701215047f6af7d' => 
    array (
      0 => 'wp-content\\Smarty316\\mvc3\\core\\view\\Aquarius_responsive_admin_panel\\addnew_frm.html',
      1 => 1409581152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1242454043b4b228d42-35140918',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54043b4b3b0a42_71547804',
  'variables' => 
  array (
    'this_class' => 0,
    'fields' => 0,
    'arr' => 0,
    'cur_tbl' => 0,
    'arr2' => 0,
    'select_tbl' => 0,
    'cur_field_name' => 0,
    'cur_field_id' => 0,
    'cur_arg' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54043b4b3b0a42_71547804')) {function content_54043b4b3b0a42_71547804($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['fields'], null, 0);?>
<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
	<?php if ($_smarty_tpl->tpl_vars['arr']->value[3]=='primary_field') {?>
		<?php $_smarty_tpl->tpl_vars['cur_tbl'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[4], null, 0);?>
	<?php }?>
<?php } ?>

<form method="POST" id="add_new_frm" 
onsubmit="document.getElementById('result_background').style.display	= 'none';">
<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="addnew_item">
<input type="hidden" name="cur_tbl" value="<?php echo $_smarty_tpl->tpl_vars['cur_tbl']->value;?>
">
<table>

<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
	<?php if ($_smarty_tpl->tpl_vars['arr']->value[2]!='hidden') {?>
	<tr>
	<th style='text-align:right;border:none;'><?php echo $_smarty_tpl->tpl_vars['arr']->value[1];?>
</th>
	<td style="border:none;">
	<?php if ($_smarty_tpl->tpl_vars['arr']->value[2]=='select_list') {?>
		<?php $_smarty_tpl->tpl_vars['select_tbl'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[4], null, 0);?>
		<?php $_smarty_tpl->tpl_vars['cur_field_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr']->value[0], null, 0);?>
		<?php  $_smarty_tpl->tpl_vars['arr2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr2']->_loop = false;
 $_smarty_tpl->tpl_vars['k2'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr2']->key => $_smarty_tpl->tpl_vars['arr2']->value) {
$_smarty_tpl->tpl_vars['arr2']->_loop = true;
 $_smarty_tpl->tpl_vars['k2']->value = $_smarty_tpl->tpl_vars['arr2']->key;
?>
			<?php if (($_smarty_tpl->tpl_vars['arr2']->value[4]==$_smarty_tpl->tpl_vars['select_tbl']->value)&&($_smarty_tpl->tpl_vars['arr2']->value[3]=='id_field')) {?>
				<?php $_smarty_tpl->tpl_vars['cur_field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['arr2']->value[0], null, 0);?>
			<?php }?>	
		<?php } ?>
		<?php $_smarty_tpl->tpl_vars['cur_arg'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['cur_tbl']->value)."&".((string)$_smarty_tpl->tpl_vars['select_tbl']->value)."&".((string)$_smarty_tpl->tpl_vars['cur_field_name']->value)."&".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value), null, 0);?>
		<?php echo $_smarty_tpl->tpl_vars['this_class']->value->select_list($_smarty_tpl->tpl_vars['cur_arg']->value);?>

	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[2]=='textbox') {?>
		<input type="text" name="cur_field[<?php echo $_smarty_tpl->tpl_vars['arr']->value[0];?>
]" value="">
	<?php }?>
	</td>
	</tr>
	<?php }?>
<?php } ?>
<tr><td style="border:none;"></td>
<td style="border:none;">
<button onmouseup=""
onclick="javascript:asf_v3('add_new_frm','fff1','loading_icon','fast');">Submit</button>
</td>
</table>
</form>

<script type="text/javascript">
	document.getElementById('add_new_frm').onkeydown = function(e){
	   if(e.keyCode == 13){
			asf_v3('add_new_frm','fff1','loading_icon','fast');
	   }
	};
</script><?php }} ?>
