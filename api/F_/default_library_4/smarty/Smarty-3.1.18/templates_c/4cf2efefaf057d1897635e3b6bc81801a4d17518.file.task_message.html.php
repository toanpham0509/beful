<?php /* Smarty version Smarty-3.1.18, created on 2014-11-17 21:49:21
         compiled from "mvc4\view\ts\task_message.html" */ ?>
<?php /*%%SmartyHeaderCode:51954694eea5f02c6-65757987%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4cf2efefaf057d1897635e3b6bc81801a4d17518' => 
    array (
      0 => 'mvc4\\view\\ts\\task_message.html',
      1 => 1416235760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '51954694eea5f02c6-65757987',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54694eea9f9cb1_49002497',
  'variables' => 
  array (
    'this_class' => 0,
    'fields' => 0,
    'i' => 0,
    'item_list' => 0,
    'i2' => 0,
    'root_url' => 0,
    'arr' => 0,
    'k22' => 0,
    'field_id' => 0,
    'cur_table' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54694eea9f9cb1_49002497')) {function content_54694eea9f9cb1_49002497($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_list']['extend_tmp_fld'], null, 0);?>
	<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['item_list'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_list, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['front_end'])."task_list_head.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
								
		<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_rows>'0') {?>
<style type="text/css">
	th{
		color:#888888;
	}
</style>		
		<table style="margin-top:10px;" class="table table-bordered table-striped dataTable" id="example1" aria-describedby="example1_info">
			<thead>
				<tr role="row">
<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
<?php if (($_smarty_tpl->tpl_vars['i']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i']->value[3]!='not_show_list'))) {?>
<th class="sorting_asc" role="columnheader" 
				tabindex="0" aria-controls="example1" rowspan="1" colspan="1" 
				style="width: auto;" aria-sort="ascending" aria-label="Rendering 
				engine: activate to sort column descending"><?php echo $_smarty_tpl->tpl_vars['i']->value[1];?>
</th>
<?php }?>
<?php } ?>

				
</tr>
			</thead>
			
			<tfoot>
				<tr>
				<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
<?php if (($_smarty_tpl->tpl_vars['i']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i']->value[3]!='not_show_list'))) {?>
				<th rowspan="1" colspan="1"><?php echo $_smarty_tpl->tpl_vars['i']->value[1];?>
</th>
<?php }?>
<?php } ?>	

				
			</tfoot>
		<tbody role="alert" aria-live="polite" aria-relevant="all">
		
		
		<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
<tr class="even">
<?php  $_smarty_tpl->tpl_vars['i2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i2']->_loop = false;
 $_smarty_tpl->tpl_vars['k22'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i2']->key => $_smarty_tpl->tpl_vars['i2']->value) {
$_smarty_tpl->tpl_vars['i2']->_loop = true;
 $_smarty_tpl->tpl_vars['k22']->value = $_smarty_tpl->tpl_vars['i2']->key;
?>
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]=="file") {?>
		<td style="width:100px;">
		<img style="width:100px;"src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['arr']->value['media_url'];?>
/thumb_<?php echo $_smarty_tpl->tpl_vars['arr']->value['media_file'];?>
" />
		</td>
		
	<?php } elseif (($_smarty_tpl->tpl_vars['i2']->value[1]=='ID')||(($_smarty_tpl->tpl_vars['i2']->value[2]!='hidden')&&($_smarty_tpl->tpl_vars['i2']->value[3]!='not_show_list'))) {?>
	<td class="col_<?php echo $_smarty_tpl->tpl_vars['i2']->value[2];?>
"> 
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]=="select_multiple") {?>
		<?php echo $_smarty_tpl->tpl_vars['this_class']->value->show_select_multiple(((string)$_smarty_tpl->tpl_vars['k22']->value).";".((string)$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value]));?>

		<span style='display:None;'>
			>><?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
<<
		<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
- <?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
 -	<?php echo $_smarty_tpl->tpl_vars['i2']->value[4];?>
 ->><?php echo $_smarty_tpl->tpl_vars['i2']->value[5];?>
 ?? <?php echo $_smarty_tpl->tpl_vars['i2']->value[6];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[7];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[8];?>

		</span>
		
	
	<?php } elseif ($_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]]) {?>
		<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]];?>

	<?php } else { ?>
		---
	<?php }?>	
	</td>
	<?php }?>
<?php } ?>


</tr>
<?php } ?>
	
	
	</tbody></table>
	
	
	<?php } else { ?>
	
	
	<?php }?>
	<div class="row" style="margin:0px;padding:5px;">
		New message:		
				<form method="POST">
		<textarea style="width:100%" name="tm_message"></textarea>
		<div style="text-align:right;">
		<button class='my_btn' type="submit" style="border:0px;margin:5px 0;">Send</button>
		</div>
		</form>
	</div>
	</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		

	
	
	
	<?php }} ?>
