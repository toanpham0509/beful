<?php /* Smarty version Smarty-3.1.18, created on 2015-09-21 15:35:16
         compiled from "app\library\template\front_end_3\position_form.html" */ ?>
<?php /*%%SmartyHeaderCode:3174655ffb8a3bab904-33908927%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9641eb49f1573e981107bd2acb48de106a3aadbe' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\position_form.html',
      1 => 1442824514,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3174655ffb8a3bab904-33908927',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55ffb8a3bad907_47587139',
  'variables' => 
  array (
    'item_list' => 0,
    'field_value' => 0,
    'data' => 0,
    'cur_value' => 0,
    'field_k' => 0,
    'this_view' => 0,
    'field_name' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ffb8a3bad907_47587139')) {function content_55ffb8a3bad907_47587139($_smarty_tpl) {?><div class="col-md-12">
<div class="col-md-6">
		<?php  $_smarty_tpl->tpl_vars['field_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_value']->_loop = false;
 $_smarty_tpl->tpl_vars['field_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_value']->key => $_smarty_tpl->tpl_vars['field_value']->value) {
$_smarty_tpl->tpl_vars['field_value']->_loop = true;
 $_smarty_tpl->tpl_vars['field_k']->value = $_smarty_tpl->tpl_vars['field_value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['field_value']->value['COLUMN_KEY']!="PRI") {?>				
				<?php $_smarty_tpl->tpl_vars['cur_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['data']->value['item_list'][0][$_smarty_tpl->tpl_vars['field_value']->value['column_name']], null, 0);?>
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['cur_value']->value;?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['cur_value'] = new Smarty_variable(str_replace("<br>","\n",$_tmp1), null, 0);?>
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['cur_value']->value;?>
<?php $_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['cur_value'] = new Smarty_variable(str_replace("<br/>","\n",$_tmp2), null, 0);?>
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['cur_value']->value;?>
<?php $_tmp3=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['cur_value'] = new Smarty_variable(str_replace("<br />","\n",$_tmp3), null, 0);?>
				<?php $_smarty_tpl->tpl_vars['field_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['data']->value['fields'][$_smarty_tpl->tpl_vars['field_k']->value]['column_name'], null, 0);?>
				<?php if ($_smarty_tpl->tpl_vars['field_value']->value['view_file']) {?>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['field_value']->value['view_file']), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

				<?php } else { ?>
					<div class="form-group">
	<label><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['field_value']->value['label']));?>
</label>
	
		<?php if ($_smarty_tpl->tpl_vars['item_list']->value['fields'][$_smarty_tpl->tpl_vars['field_k']->value]['REFERENCED_TABLE_NAME']) {?>						
		<select id="field_<?php echo $_smarty_tpl->tpl_vars['field_name']->value;?>
" class='form-control' 
			name='item_field[<?php echo $_smarty_tpl->tpl_vars['field_name']->value;?>
]' onchange='javascript:'>
			<?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0]['drop_down_list'][$_smarty_tpl->tpl_vars['data']->value['fields'][$_smarty_tpl->tpl_vars['field_k']->value]['column_name']];?>

		</select>
		<span ></span>
		<?php } else { ?>							
			<input id="field_<?php echo $_smarty_tpl->tpl_vars['field_name']->value;?>
" class='form-control' type='text' name='item_field[<?php echo $_smarty_tpl->tpl_vars['field_value']->value['column_name'];?>
]'
			value="<?php echo $_smarty_tpl->tpl_vars['cur_value']->value;?>
">
		<?php }?>
						
					</div>
				<?php }?>
			<?php }?>
		<?php } ?>
</div>
<div class="col-md-6">
	<div class="form-group" >
	<label>Phân quyền</label>
	<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 3+1 - (1) : 1-(3)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
	<div class="checkbox" style='margin-left:20px;margin-top: 0;'>
		<label class="">
			<div class="" style="">
			<input type="checkbox" style="">
			</div>
			Checkbox 1
		</label>
	</div>
	<?php }} ?>
	</div>
</div>
</div>
		<div style='display:none;'>
		<input type="text" name="action" value="item_update">
		<input type="text" name="args" value="<?php echo $_GET['args'];?>
">
		<input type="text" name='controller' value="<?php echo $_smarty_tpl->tpl_vars['item_list']->value['cur_class'];?>
">
		</div>
	
		<button  type='submit' class="btn btn-primary btn-sm">SUBMIT</button>
	<?php }} ?>
