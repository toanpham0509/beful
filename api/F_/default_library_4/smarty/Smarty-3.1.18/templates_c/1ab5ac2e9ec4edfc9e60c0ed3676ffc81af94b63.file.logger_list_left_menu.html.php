<?php /* Smarty version Smarty-3.1.18, created on 2015-07-22 17:03:29
         compiled from "app\library\template\front_end_3\logger_list_left_menu.html" */ ?>
<?php /*%%SmartyHeaderCode:1238455ab3d4f78d211-38069442%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ab5ac2e9ec4edfc9e60c0ed3676ffc81af94b63' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\logger_list_left_menu.html',
      1 => 1437559405,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1238455ab3d4f78d211-38069442',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55ab3d4f7fbbb4_94407983',
  'variables' => 
  array (
    'data' => 0,
    'this_view' => 0,
    'i' => 0,
    'arr' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ab3d4f7fbbb4_94407983')) {function content_55ab3d4f7fbbb4_94407983($_smarty_tpl) {?><?php $_smarty_tpl->createLocalArrayVariable('data', null, 0);
$_smarty_tpl->tpl_vars['data']->value['cur_month'] = date('m',$_smarty_tpl->tpl_vars['data']->value['cur_time']);?>
<?php $_smarty_tpl->createLocalArrayVariable('data', null, 0);
$_smarty_tpl->tpl_vars['data']->value['cur_year'] = date('Y',$_smarty_tpl->tpl_vars['data']->value['cur_time']);?>
<ul class="sidebar-menu">
	<!--
	logger_id,logger_name,sub_cat_id
	-->
	
	<li class="active">
		<a href="javascript:void();">
			<i class="fa fa-dashboard"></i> <span>Danh sách chức năng</span>
		</a>
	</li>
	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-th"></i>
			<span>Tháng <?php echo $_smarty_tpl->tpl_vars['data']->value['cur_month'];?>

			năm <?php echo $_smarty_tpl->tpl_vars['data']->value['cur_year'];?>

			</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li >
			<form method='get' action='<?php echo $_smarty_tpl->tpl_vars['this_view']->value->root_url;?>
index.php' class='box' 
				style='padding: 0px; border-radius: 0px; border: 0px none;'
			>
			<div class="box-body">
				<table>
					<tr class="form-group">
						<td><label for="">Tháng</label></td>
						<td>
							<select class='form-control' name='month'>
							<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 12+1 - (1) : 1-(12)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
							<option 
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['cur_month'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_smarty_tpl->tpl_vars['i']->value==$_tmp1) {?>
								selected="selected"
							<?php }?>
							><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
							<?php }} ?>
							</select>
						</td>
					</tr>
					<tr class="form-group">
						<td><label for="exampleInputEmail1">Năm</label></td>
						<td>
							<select class='form-control' name='year'>
							<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2050+1 - (2014) : 2014-(2050)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2014, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
							<option
								<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['cur_year'];?>
<?php $_tmp2=ob_get_clean();?><?php if ($_smarty_tpl->tpl_vars['i']->value==$_tmp2) {?>
									selected="selected"
								<?php }?>
							><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
							<?php }} ?>
							</select>
						</td>
					</tr>
				</table>
					
			<input type='hidden' name='page' value='admin'>		
		</div><!-- /.box-body -->
		<div class="box-footer">
			<button class="btn btn-primary" type="submit" onclick="javascript:
			this.innerHTML = '...loading...';
			">Thiết lập thời gian</button>
		</div>
		</form>
			</li>
		</ul>
	</li>
	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-th"></i>
			<span>Chốt chỉ số</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value['item_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
				<li><a href="javascript:void();" onclick="javascript:
				default_cols();
				_('right_main').innerHTML = '...loading...';
				ajax_get('<?php echo $_smarty_tpl->tpl_vars['this_view']->value->root_url;?>
&controller=logger&action=update_value&args=<?php echo $_smarty_tpl->tpl_vars['arr']->value['logger_id'];?>
&cur_time=<?php echo $_smarty_tpl->tpl_vars['data']->value['cur_time'];?>
' 	
				,'right_main'); 
				">
				<i class="fa fa-angle-double-right"></i> 
				<?php echo $_smarty_tpl->tpl_vars['arr']->value['logger_name'];?>
</a>
				</li>
			<?php } ?>
			
		</ul>
	</li>
	
	<!--li>
		<a href="#">
			<i class="fa fa-th"></i> <span>Thành viên</span> 
			<small class="badge pull-right bg-green">đang xd</small>
		</a>
	</li--->
</ul><?php }} ?>
