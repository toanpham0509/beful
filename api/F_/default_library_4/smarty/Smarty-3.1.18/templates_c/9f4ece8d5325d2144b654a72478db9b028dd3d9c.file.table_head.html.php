<?php /* Smarty version Smarty-3.1.18, created on 2014-10-02 21:51:58
         compiled from "mvc4\view\Aquarius_responsive_admin_panel\table_head.html" */ ?>
<?php /*%%SmartyHeaderCode:24603542d58176ef0c3-57877576%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f4ece8d5325d2144b654a72478db9b028dd3d9c' => 
    array (
      0 => 'mvc4\\view\\Aquarius_responsive_admin_panel\\table_head.html',
      1 => 1412257917,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '24603542d58176ef0c3-57877576',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_542d58176f3879_02178355',
  'variables' => 
  array (
    'this_class' => 0,
    'num_rows_show' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_542d58176f3879_02178355')) {function content_542d58176f3879_02178355($_smarty_tpl) {?>
<div >
	<div style="" id="loading_icon" ></div>
	<div id="result_background" class="pop_up_background" style="display:none;" >
		<div class="pop_up_content" >
			<div class="pop_up_title">Form
				<div class="pop_up_close_btn" ><a href="">[x]</a></div>
			</div>
			<div id="show_result"></div>
		</div>
	</div>
</div>
<div class="box-header" style="">
	<h3 class="box-title" style=""><?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['item_list']['title'];?>

	
	
	</h3>
</div><!-- /.box-header -->
<div class="box-body table-responsive">
	<div role="grid" class="dataTables_wrapper form-inline" id="example1_wrapper">
	
	<div class="row" style="display:none;"><div class="col-xs-6"><div id="example1_length" class="dataTables_length">
	</div></div><div class="col-xs-6"><div class="dataTables_filter" id="example1_filter">
	<label>Search: <input type="text" aria-controls="example1"></label>
	</div></div></div>

	<div style="float:left;width:100%;padding:0 2px 1px 3px;">
<form method="post" id="frm_addnew" class="tbl_frm" style="float:left;padding: 0 10px 0 0;"
	onsubmit="document.getElementById('result_background').style.display	= 'block';">
	<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="addnew_frm">
	<a onclick ="javascript:asf_v3('frm_addnew','show_result','loading_icon','fast');"
	href="javascript:void();">Add new</a>
</form>

<form method="post" id="import_window" class="tbl_frm" style="float:left;width:100px;"
	onsubmit="document.getElementById('result_background').style.display	= 'block';">
	<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="import_frm">
	<a onclick ="javascript:asf_v3('import_window','show_result','loading_icon','fast');"
		href="javascript:void();">Import</a>
</form>

<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_rows>'0') {?>
<form method="POST" style="float:right;margin-bottom:0px;" >

<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_rows_show<$_smarty_tpl->tpl_vars['this_class']->value->num_rows) {?>	
<?php $_smarty_tpl->tpl_vars['num_rows_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->num_rows_show, null, 0);?>
<?php } else { ?>	
<?php $_smarty_tpl->tpl_vars['num_rows_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->num_rows, null, 0);?>	
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_rows_show) {?>
<div style="">Showing <?php echo $_smarty_tpl->tpl_vars['num_rows_show']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['this_class']->value->num_rows;?>
 entries 
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->num_pages!='1') {?>
, page <select style="
border: 1px solid #CCCCCC;
border-radius: 0;
height: auto;
margin: 0 0 2px;
padding: 0;
width: 45px;
" name="new_page" onchange="this.form.submit();">
<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['this_class']->value->num_pages+1 - (1) : 1-($_smarty_tpl->tpl_vars['this_class']->value->num_pages)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
<option
<?php if ($_GET['pp']==$_smarty_tpl->tpl_vars['i']->value) {?>
selected = "select"
<?php }?>
><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
<?php }} ?>
</select> 
<?php }?>
</div>
<?php }?>
</form>
<?php } else { ?>
	not found
<?php }?>
</div>	<br><?php }} ?>
