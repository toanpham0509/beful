<?php /* Smarty version Smarty-3.1.18, created on 2015-02-03 21:05:37
         compiled from "base\view\AdminLTE-master\register_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:3087654d03d5b0f03f6-31465306%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd60dd6176bbce04fd9fabf8bdd9f2dd69f02dae6' => 
    array (
      0 => 'base\\view\\AdminLTE-master\\register_frm.html',
      1 => 1422972336,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3087654d03d5b0f03f6-31465306',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54d03d5b2f4aa8_61572712',
  'variables' => 
  array (
    'lang_file' => 0,
    'this_view' => 0,
    'register_new_membership' => 0,
    'args' => 0,
    'full_name' => 0,
    'user_name' => 0,
    'user_pass' => 0,
    'retype_password' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54d03d5b2f4aa8_61572712')) {function content_54d03d5b2f4aa8_61572712($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['user_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("user_name",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['full_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("full_name",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['register_new_membership'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("register_new_membership",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['user_pass'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("user_pass",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>
<?php $_smarty_tpl->tpl_vars['retype_password'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_view']->value->translator("retype_password",((string)$_smarty_tpl->tpl_vars['lang_file']->value)), null, 0);?>

<div id="login-box" class="form-box">
	<div class="header"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['register_new_membership']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo ucwords($_tmp1);?>
</div>
	<form method="post" action="">
		<?php if ($_smarty_tpl->tpl_vars['args']->value['result']=="register_success") {?>
			<div class="body bg-gray">
				<div class="form-group login_success">
					<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
,
					<a style="text-decoration: underline;" class="text-center" 
					href="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
login/">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("click_here_to_login",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a> 
				</div>
			</div>
			
		<?php } else { ?>
		<div class="body bg-gray">
			<?php if ($_smarty_tpl->tpl_vars['args']->value['result']!="not_show") {?>
			<div class="form-group login_error">
				<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['args']->value['result']),((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
?	
			</div>
			<?php }?>
			<div class="form-group">
				<input type="text" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['full_name']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo ucfirst($_tmp2);?>
" 
				value="<?php echo $_POST['full_name'];?>
" class="form-control" name="full_name">
			</div>
			<div class="form-group">
				<input type="text" placeholder='<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['user_name']->value;?>
<?php $_tmp3=ob_get_clean();?><?php echo ucfirst($_tmp3);?>
' 
				value="<?php echo $_POST['user_name'];?>
" class="form-control" name="user_name" >
			</div>
			<div class="form-group">
				<input type="password" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['user_pass']->value;?>
<?php $_tmp4=ob_get_clean();?><?php echo ucfirst($_tmp4);?>
" 
				class="form-control" name="user_pass" >
			</div>
			<div class="form-group">
				<input type="password" placeholder="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['retype_password']->value;?>
<?php $_tmp5=ob_get_clean();?><?php echo ucfirst($_tmp5);?>
" class="form-control" 
				name="retype_password">
			</div>
		</div>
		<div class="footer">                    

			<button class="btn bg-olive btn-block" type="submit">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator('sign_me_up',$_smarty_tpl->tpl_vars['lang_file']->value));?>
</button>

			<a class="text-center" href="<?php echo $_smarty_tpl->tpl_vars['args']->value['config']['root_url'];?>
login/">
	<?php echo ucfirst($_smarty_tpl->tpl_vars['this_view']->value->translator("i_already_have_a_membership",$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a>
		</div>
		
		<?php }?>
	</form>


</div><?php }} ?>
