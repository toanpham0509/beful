<?php /* Smarty version Smarty-3.1.18, created on 2015-05-19 17:02:42
         compiled from "app\library\template\front_end\pos_info.html" */ ?>
<?php /*%%SmartyHeaderCode:2973155540f499256a2-38129516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '01858dcaa9b19a0c6151a2d9bc620faddb66c596' => 
    array (
      0 => 'app\\library\\template\\front_end\\pos_info.html',
      1 => 1432029757,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2973155540f499256a2-38129516',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55540f499a31d1_17564559',
  'variables' => 
  array (
    'pos_info' => 0,
    'k' => 0,
    'arr' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55540f499a31d1_17564559')) {function content_55540f499a31d1_17564559($_smarty_tpl) {?><div class="col-lg-10 col-md-10 border-left col-item">
<div class="box-header">
	Thông tin chi nhánh: <?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['pos_name'];?>

</div>
<br />

<div class="row">
	<div class="col-lg-8 col-md-8">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span>Tên chi nhánh
				</label>
			</div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['pos_name'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Quốc gia
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['country_name'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Tỉnh/ thành phố
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['city_name'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Địa chỉ
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['address'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Zipcode
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['zipcode'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Giờ mở cửa
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['opening_hour'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Giờ đóng của
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['closing_hour'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Email:
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['company_email'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Số điện thoại
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['phone'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label class="label">
					<span class="red">*</span> Loại hình hoạt động
				</label>
			 </div>
			<div class="col-lg-8 col-md-8">
				<label class="view-info"><?php echo $_smarty_tpl->tpl_vars['pos_info']->value['data']['activity_name'];?>
</label>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
			</div>
			<div class="col-lg-8 col-md-8">
				<br />
				<a href="<?php echo $_smarty_tpl->tpl_vars['pos_info']->value['root_url'];?>
pos_edit&pos_id=<?php echo $_smarty_tpl->tpl_vars['pos_info']->value['pos_id'];?>
" title="Chỉnh sửa" style="float: left;">
					<button type="button" class="btn btn-primary">Chỉnh sửa</button>
				</a>
				<a href="" title="Xóa" onClick="return confirm('Bạn có chắc chắn không?')" class="float-left">
					<button type="button" class="btn btn-primary">Xóa</button>
				</a>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4">
		<div class="map">
			<div id="googleMap" style="width:100%; height:200px;margin-top:10px;margin:auto;margin-top:10px;"></div>
			<br />
			<div style="text-align:center">Bản đồ</div>
		</div>
		<br />
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<div class="qrcode">
					<img src="http://mycouper.com/api/qrcode/images/kool-soft-192c3425f0a3a4cca70e96149b5f3ff9-145x145-255-255-255-0-0-0.jpg" />
					<br /><br />
					<div style="text-align:center">QR code</div>
				</div>
				<br />
				<div style="text-align:center">
					<a href="" title="Tạo mới" class="float-left">
						<span class="icon-add-new"></span>
					</a>
					<a href="" title="Tải về" class="float-left">
						<span class="icon-download"></span>
					</a>
					<a href="" title="In" class="float-left">
						<span class="icon-printer"></span>
					</a>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
				<img class="img-responsive" src="<<?php ?>?php echo BASE_URL ?<?php ?>>images/credit-card.jpg" />
			</div>
		</div>
	</div>
</div>
</div>
<!--<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['pos_info']->value['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
	<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
:<?php echo $_smarty_tpl->tpl_vars['arr']->value;?>
 <br />
<?php } ?>--><?php }} ?>
