<?php /* Smarty version Smarty-3.1.18, created on 2015-09-21 18:37:16
         compiled from "app\library\template\front_end_3\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:3017555fbcc77df4844-78314328%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '73fd35a11688b190c77e632ebef19d45183bd448' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\item_details.html',
      1 => 1442835419,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3017555fbcc77df4844-78314328',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55fbcc78058cf8_94793298',
  'variables' => 
  array (
    'item_list' => 0,
    'this_view' => 0,
    'field_value' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55fbcc78058cf8_94793298')) {function content_55fbcc78058cf8_94793298($_smarty_tpl) {?><progress id="upload_progress_bar" value="0" max="100" 
	style="width:300px;display:none;"></progress>
	<div id="submit_status"></div>
	
	<form action="javascript:void();" method="post" id="items_form"
	onsubmit="javascript:
		_('submit_status').innerHTML = '...loading...'
		ajax_post(this.id,'submit_status','upload_progress_bar');
	">
<div class="col-md-12">
	<!-- Custom Tabs -->
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active" id="menu_tab_1"><a data-toggle="tab" href="#tab_1"
				onclick="javascript:
					_('tab_1').innerHTML = 'loading...';
		ajax_get('<?php echo $_smarty_tpl->tpl_vars['item_list']->value['root_url'];?>
&controller=<?php echo $_smarty_tpl->tpl_vars['item_list']->value['cur_class'];?>
&action=item_details&args=<?php echo $_GET['args'];?>
'
			,'right_main');
					
				"
			>
			<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("details");?>
</a></li>
			<li id="menu_tab_2">
				<a data-toggle="tab" href="#tab_2" onclick='javascript:'>
				<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("edit_button");?>

				</a>
			</li>
			<li><a data-toggle="tab" href="#tab_3">
			<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete_button");?>

			</a></li>
			<li><a   href="" onclick="javascript:this.innerHTML = 'loading...'">
			<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("item_list");?>

			</a></li>
			
		</ul>
		<div class="tab-content">
			<div id="tab_1" class="tab-pane   active">
			<dl class="dl-horizontal">
			
			<?php  $_smarty_tpl->tpl_vars['field_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_value']->_loop = false;
 $_smarty_tpl->tpl_vars['field_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_value']->key => $_smarty_tpl->tpl_vars['field_value']->value) {
$_smarty_tpl->tpl_vars['field_value']->_loop = true;
 $_smarty_tpl->tpl_vars['field_k']->value = $_smarty_tpl->tpl_vars['field_value']->key;
?>
				<?php if ($_smarty_tpl->tpl_vars['field_value']->value['COLUMN_KEY']!="PRI") {?>
				<dt><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['field_value']->value['label']));?>
</dt>
				<dd>
					<?php if ($_smarty_tpl->tpl_vars['data']->value['item_list'][0][$_smarty_tpl->tpl_vars['field_value']->value['column_name']]) {?>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['item_list'][0][$_smarty_tpl->tpl_vars['field_value']->value['column_name']];?>

					<?php } else { ?>
						<span style='color:#ddd;'>---</span>
					<?php }?>
				</dd>
				<?php }?>
			<?php } ?>
			</dl>
			</div><!-- /.tab-pane -->
			<div id="tab_2" class="tab-pane " >
				<div id='get_item_list_status'></div>
				<div id="tab_2_content" class="">
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['front_end'])."promote_form.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

				</div>
			</div><!-- /.tab-pane -->
			
			<div id="tab_3" class="tab-pane">			
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-ban"></i>
				<div id="delete_status">
				<b>Alert!</b> <span style='margin-right:10px;'><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete_confirm");?>
</span>
				
				
				<a href='javascript:void();' onclick='javascript:
				ajax_get("<?php echo $_smarty_tpl->tpl_vars['item_list']->value['root_url'];?>
&controller=<?php echo $_smarty_tpl->tpl_vars['item_list']->value['cur_class'];?>
&action=item_delete&args=<?php echo $_GET['args'];?>
",
								"tab_3");
				_("delete_status").innerHTML	= "deleting...";
				_("menu_tab_1").style.display	= "none";	
				_("menu_tab_2").style.display	= "none";	
				'><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("yes");?>
</a>
				</div>
			</div>
			</div><!-- /.tab-pane -->
			<div id="tab_4" class="tab-pane " >
				...loading...
			</div><!-- /.tab-pane -->
		</div><!-- /.tab-content -->
	</div><!-- nav-tabs-custom -->
</div>
</form>
<?php }} ?>
