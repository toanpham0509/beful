<?php /* Smarty version Smarty-3.1.18, created on 2015-06-23 15:36:22
         compiled from "app\library\template\front_end_3\set_month.html" */ ?>
<?php /*%%SmartyHeaderCode:29290558909d125a8e6-54245060%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d31542b5ec8e9cb8fcc9877bc7a4c7c05797282' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\set_month.html',
      1 => 1435048544,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29290558909d125a8e6-54245060',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_558909d12aaec2_34675180',
  'variables' => 
  array (
    'i' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558909d12aaec2_34675180')) {function content_558909d12aaec2_34675180($_smarty_tpl) {?><div class="box box-primary" style='border-top-color:#ddd;'>
	<div class="box-header">
		<h3 class="box-title">Thiết lập thời gian</h3>
	</div><!-- /.box-header -->
	<!-- form start -->
	<form  action="javascript:void();" method="POST" id="set_month_form" 
		onsubmit="javascript:ajax_post(this.id,'right_main','upload_progress_bar'); 
		_('tab_3').innerHTML = '...loading...';
		">
		<div class="box-body">
			<div class="form-group">
				<label for="exampleInputEmail1">Tháng</label>
				<select class='form-control' name='set_month'>
				<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 12+1 - (1) : 1-(12)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
				<option 
				<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['cur_month'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_smarty_tpl->tpl_vars['i']->value==$_tmp1) {?>
					selected="selected"
				<?php }?>
				><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
				<?php }} ?>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Năm</label>
				<select class='form-control' name='set_year'>
				<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 2050+1 - (2014) : 2014-(2050)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2014, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
				<option
					<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['cur_year'];?>
<?php $_tmp2=ob_get_clean();?><?php if ($_smarty_tpl->tpl_vars['i']->value==$_tmp2) {?>
						selected="selected"
					<?php }?>
				><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
				<?php }} ?>
				</select>
			</div>
			
		</div><!-- /.box-body -->

		<div class="box-footer">
			<button class="btn btn-primary" type="submit">Thiết lập</button>
		</div>
		<div style="display:none;">
			<input type="text" value="set_time" name="action" >
			
			<input type="text" value="logger" name="controller">
		</div>
	</form>
</div>


<?php }} ?>
