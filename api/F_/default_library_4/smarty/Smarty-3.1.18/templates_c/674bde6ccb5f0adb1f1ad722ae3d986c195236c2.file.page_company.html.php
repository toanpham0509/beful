<?php /* Smarty version Smarty-3.1.18, created on 2015-05-24 22:24:58
         compiled from "app\library\template\front_end_3\page_company.html" */ ?>
<?php /*%%SmartyHeaderCode:1687155617025ab32f3-19067083%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '674bde6ccb5f0adb1f1ad722ae3d986c195236c2' => 
    array (
      0 => 'app\\library\\template\\front_end_3\\page_company.html',
      1 => 1432481097,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1687155617025ab32f3-19067083',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_55617025c85e63_27927130',
  'variables' => 
  array (
    'page_class' => 0,
    'this_view' => 0,
    'position' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55617025c85e63_27927130')) {function content_55617025c85e63_27927130($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>
        	<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['page_class']->value->page_title_1));?>
	|
			<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator(((string)$_smarty_tpl->tpl_vars['page_class']->value->page_title_2));?>

        </title>
        <link rel="stylesheet" media="screen" href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
css/template.min.css" />
        <link rel="stylesheet" media="screen" href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
css/template-theme.min.css" />
        <link rel="stylesheet" media="screen" href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
css/style.css" />
        <script language="javascript" type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
javascript/jquery-2.1.0.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
javascript/template.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
javascript/style.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
        <script>
			function initialize( $lat, $long ) {
				var myCenter = new google.maps.LatLng(21.00828,105.802027);
				var mapProp = {
				 center:myCenter,
				zoom:18,
				mapTypeId:google.maps.MapTypeId.ROADMAP
				};
	
				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
	
				var marker=new google.maps.Marker({
				 position:myCenter,
				 animation:google.maps.Animation.BOUNCE
				});
	
				marker.setMap(map);
	
				var infowindow = new google.maps.InfoWindow({
					content:"Company location!"
				  });
		
				infowindow.open(map,marker);
			}
			google.maps.event.addDomListener(window, 'load', initialize );
		</script>
		<script language="javascript" type="text/javascript" 
		 src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['default_library'];?>
js/script.js"></script>
	</head>		
	<body style="overflow-x: hidden">
		<div class="header navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['root_url'];?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_dir['front_end'];?>
images/icon.png" /></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("home");?>
</a></li>
						<li><a href="user_info"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("information");?>
</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['position']->value[3]=="pos") {?> 
								class="active"
							<?php }?>
						>
							<a href="pos"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("branch");?>
</a>
						</li>
						<li class=" dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<span><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("card_management");?>
</span>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="member-card.php" title="Thẻ thành viên"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("member_card");?>
</a>
								</li>
								<li>
									<a href="stamp-card.php" title="Thẻ tích điểm">Thẻ tích điểm</a>
								</li>
								<li>
									<a href="cheat-manager.php" title="Quản lý gian lận">Quản lý gian lận</a>
								</li>
							</ul>
						</li>
						<li><a href="coupon.php">Phiếu</a></li>
						<li><a href="report.php"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("report");?>
</a></li>
						<li><a href="">VI/EN</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"> User name <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="user-info.php"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("information");?>
</a></li>
								<li><a href="login.php"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("log_out");?>
</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.nav-collapse -->
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="row">
						<?php echo $_smarty_tpl->tpl_vars['position']->value[4];?>

						<div id="right_main"><?php echo $_smarty_tpl->tpl_vars['position']->value[5];?>
</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid footer">
			<div class="row">
				<div class="col-lg-12" style="margin: 0px 15px;">
					© 2015 myCouper.com  Mọi bản quyền thuộc về myCouper
				</div>	
			</div>
		</div>
	</body>
</html><?php }} ?>
