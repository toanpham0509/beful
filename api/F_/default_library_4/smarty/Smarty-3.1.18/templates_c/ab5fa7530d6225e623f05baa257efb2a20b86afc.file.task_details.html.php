<?php /* Smarty version Smarty-3.1.18, created on 2014-11-24 21:04:11
         compiled from "mvc4\view\ts\task_details.html" */ ?>
<?php /*%%SmartyHeaderCode:11435467fdd2be2db5-46261317%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ab5fa7530d6225e623f05baa257efb2a20b86afc' => 
    array (
      0 => 'mvc4\\view\\ts\\task_details.html',
      1 => 1416837847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11435467fdd2be2db5-46261317',
  'function' => 
  array (
    'menu' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
      ),
      'compiled' => '',
    ),
    'stars_arr' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5467fdd3069819_14332150',
  'variables' => 
  array (
    'this_class' => 0,
    'cur_arg' => 0,
    'cur_temp' => 0,
    'fields' => 0,
    'f_v' => 0,
    'field_id' => 0,
    'item_details' => 0,
    'f_id' => 0,
    'cur_field_id' => 0,
    'ff' => 0,
    'fv' => 0,
    'cur_fid' => 0,
    'parent_id' => 0,
    'parent_field' => 0,
    'cur_fv' => 0,
    'cur_vshow' => 0,
    'parent_value' => 0,
    'root_url' => 0,
    'img_url' => 0,
    'item_show' => 0,
    'cur_class' => 0,
    'get_status' => 0,
    'level' => 0,
    'i' => 0,
    'stars' => 0,
    'rating_show' => 0,
    'args' => 0,
    'task_status' => 0,
    'check_bid' => 0,
    'show_accept_btn' => 0,
    'cur_task' => 0,
  ),
  'has_nocache_code' => 0,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5467fdd3069819_14332150')) {function content_5467fdd3069819_14332150($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['item_details'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_details, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_arg'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->cur_arg, null, 0);?>
<?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>


<?php $_smarty_tpl->tpl_vars['cur_temp'] = new Smarty_variable(explode('=',$_smarty_tpl->tpl_vars['cur_arg']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_item'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_temp']->value[1], null, 0);?>

<?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_details']['extend_tmp_fld'], null, 0);?>
<div id="loading_icon_full"></div>
<div  class="row" style="float:left;width:100%;margin-top:10px;margin-left: 0;">
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Details</h3>
			</div>
			<div class="box-body">
	<table>
		<?php  $_smarty_tpl->tpl_vars['f_v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f_v']->_loop = false;
 $_smarty_tpl->tpl_vars['f_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f_v']->key => $_smarty_tpl->tpl_vars['f_v']->value) {
$_smarty_tpl->tpl_vars['f_v']->_loop = true;
 $_smarty_tpl->tpl_vars['f_id']->value = $_smarty_tpl->tpl_vars['f_v']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]!="hidden") {?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="select_multiple") {?>
					<?php $_smarty_tpl->tpl_vars['cur_field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['field_id']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value), null, 0);?>
					<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = $_smarty_tpl->tpl_vars['this_class']->value->show_select_multiple(((string)$_smarty_tpl->tpl_vars['f_id']->value).";".((string)$_smarty_tpl->tpl_vars['cur_field_id']->value));?>	
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable('', null, 0);?>
					<?php  $_smarty_tpl->tpl_vars['fv'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fv']->_loop = false;
 $_smarty_tpl->tpl_vars['ff'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fv']->key => $_smarty_tpl->tpl_vars['fv']->value) {
$_smarty_tpl->tpl_vars['fv']->_loop = true;
 $_smarty_tpl->tpl_vars['ff']->value = $_smarty_tpl->tpl_vars['fv']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['f_v']->value[5]==((string)$_smarty_tpl->tpl_vars['ff']->value)) {?>
							<?php $_smarty_tpl->tpl_vars['parent_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['f_v']->value[5]-1, null, 0);?>
						<?php }?>
						<?php if ((($_smarty_tpl->tpl_vars['f_v']->value[4]==$_smarty_tpl->tpl_vars['fv']->value[4])&&($_smarty_tpl->tpl_vars['fv']->value[3]=="id_field"))) {?>
							<?php $_smarty_tpl->tpl_vars['cur_fid'] = new Smarty_variable($_smarty_tpl->tpl_vars['fv']->value[0], null, 0);?>
							<?php $_smarty_tpl->tpl_vars['cur_fv'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['cur_fid']->value], null, 0);?>
							
						<?php }?>
					<?php } ?>	
					<?php $_smarty_tpl->tpl_vars['parent_field'] = new Smarty_variable($_smarty_tpl->tpl_vars['fields']->value[$_smarty_tpl->tpl_vars['parent_id']->value][0], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['parent_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['parent_field']->value], null, 0);?>
					<?php $_smarty_tpl->tpl_vars['cur_vshow'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>	
					<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['cur_fv']->value)."»".((string)$_smarty_tpl->tpl_vars['f_id']->value)."»".((string)$_smarty_tpl->tpl_vars['cur_vshow']->value)."»".((string)$_smarty_tpl->tpl_vars['parent_value']->value), null, 0);?>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['f_v']->value[2]=="file") {?>
					<?php $_smarty_tpl->tpl_vars['img_url'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['root_url']->value).((string)$_smarty_tpl->tpl_vars['item_details']->value[0]['media_url'])."/".((string)$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]), null, 0);?>
	<?php $_smarty_tpl->createLocalArrayVariable('item_details', null, 0);
$_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]] = "<image style='width:100px;' src='".((string)$_smarty_tpl->tpl_vars['img_url']->value)."'>";?>				
					
					<?php $_smarty_tpl->createLocalArrayVariable('f_v', null, 0);
$_smarty_tpl->tpl_vars['f_v']->value[2] = 'file_upload_frm';?>
				<?php }?>	
				<tr>
					<th style="vertical-align:top;text-align:right;padding:5px 0;"><?php echo $_smarty_tpl->tpl_vars['f_v']->value[1];?>
</th>
					<td style="padding:5px 10px;" id="id_<?php echo $_smarty_tpl->tpl_vars['f_v']->value[0];?>
">
					
					<?php if ($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]]) {?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_details']->value[0][$_smarty_tpl->tpl_vars['f_v']->value[0]], null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("---", null, 0);?>
					<?php }?>
	<?php ob_start();?><?php echo $_SESSION['user']['user_name'];?>
<?php $_tmp1=ob_get_clean();?><?php if (($_smarty_tpl->tpl_vars['f_v']->value[0]=="user_name")&&($_smarty_tpl->tpl_vars['item_show']->value==$_tmp1)) {?>
		<?php $_smarty_tpl->tpl_vars['cur_task'] = new Smarty_variable("me", null, 0);?>
		<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable('me', null, 0);?>
		<?php $_smarty_tpl->tpl_vars['show_accept_btn'] = new Smarty_variable('not_show', null, 0);?>	
	<?php } elseif ($_smarty_tpl->tpl_vars['f_v']->value[0]=="teacher_id") {?>
		<?php $_smarty_tpl->tpl_vars['rating_show'] = new Smarty_variable('', null, 0);?>
		
		<?php $_smarty_tpl->tpl_vars['get_status'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_class']->value->get_status($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id']), null, 0);?>
		<?php $_smarty_tpl->tpl_vars['get_status'] = new Smarty_variable(explode(';',$_smarty_tpl->tpl_vars['get_status']->value), null, 0);?>
		
		<?php if ($_smarty_tpl->tpl_vars['get_status']->value[0]=="task_opening") {?>
			<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("OPEN", null, 0);?>
		<?php } elseif ($_smarty_tpl->tpl_vars['get_status']->value[0]=="task_completed") {?>
			<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("<span style='color:#3D9970;font-weight:bold;'>
			COMPLETED by ".((string)$_smarty_tpl->tpl_vars['get_status']->value[1])."</span>", null, 0);?>
		<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars['item_show'] = new Smarty_variable("IN PROGRESS: ".((string)$_smarty_tpl->tpl_vars['get_status']->value[1]), null, 0);?>
			<?php $_smarty_tpl->tpl_vars['rating_show'] = new Smarty_variable("show_rs", null, 0);?>
		<?php }?>	
		<?php if ($_smarty_tpl->tpl_vars['item_show']->value!="OPEN") {?>
			<?php $_smarty_tpl->tpl_vars['show_accept_btn'] = new Smarty_variable('not_show', null, 0);?>	
			<?php $_smarty_tpl->tpl_vars['task_status'] = new Smarty_variable("awarded", null, 0);?>
		<?php }?>
	<?php }?>	

	<?php if (!function_exists('smarty_template_function_stars_arr')) {
    function smarty_template_function_stars_arr($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['stars_arr']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>         
		 <input type="radio" name="rating_value" value="<?php echo $_smarty_tpl->tpl_vars['level']->value;?>
">
		  <label ><span><?php echo $_smarty_tpl->tpl_vars['level']->value;?>
</span></label>
	<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

	<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 9+1 - (1) : 1-(9)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
		<?php if ($_smarty_tpl->tpl_vars['i']->value=="1") {?>
			<?php ob_start();?><?php smarty_template_function_stars_arr($_smarty_tpl,array('level'=>((string)$_smarty_tpl->tpl_vars['i']->value)));?>
<?php $_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['stars'] = new Smarty_variable($_tmp2, null, 0);?>
		<?php } else { ?>	
			<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
<?php $_tmp3=ob_get_clean();?><?php ob_start();?><?php smarty_template_function_stars_arr($_smarty_tpl,array('level'=>$_tmp3));?>
<?php $_tmp4=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['stars'] = new Smarty_variable("<div>".((string)$_smarty_tpl->tpl_vars['stars']->value)."</div>".$_tmp4, null, 0);?>
		<?php }?>
	<?php }} ?>

	
	<?php if ($_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_task_page']!="my_tasks") {?>
			<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>

	<?php } elseif ($_smarty_tpl->tpl_vars['rating_show']->value=="show_rs") {?>	
		<div id='task_status_show'>
		<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>
<br>
		
		<a href="javascript:void();" id='complete_link' onclick="javascript:togg('complete_frm'
			,'hide_class','show_class');
			">
		Complete Task
		</a>
		<div id="complete_frm" class='hide_class'>
		
		

		<form method="POST" id="task_rating_frm" style="margin-bottom: 20px;">
		<input type="hidden" name="actions_list[task][0]" value= "rating_for_teacher">
		<input type="hidden" name="args[task][0]" value="<?php echo $_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'];?>
">
		 Rating: 
		<div class="starRating"><?php echo $_smarty_tpl->tpl_vars['stars']->value;?>
</div> <br>or 
		<input type="radio" name="rating_value" value="0" checked="check" 
			style="vertical-align: top;"> 0 Star
		</form>
		
		<a href="javascript:void();" class="my_btn" 
		onclick="javascript:asf_v3('task_rating_frm','task_status_show','loading_icon','fast');			
			">Submit</a>
		</div>
		</div>
		
	<?php } else { ?>				
	<a href="javascript:asf_v3('field_<?php echo $_smarty_tpl->tpl_vars['f_id']->value;?>
','edit_frm','loading_icon','fast');">				
					<?php echo $_smarty_tpl->tpl_vars['item_show']->value;?>

	</a>
	<form method="POST" id="field_<?php echo $_smarty_tpl->tpl_vars['f_id']->value;?>
">
		<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]"
		value= "<?php echo $_smarty_tpl->tpl_vars['f_v']->value[2];?>
">
	<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" 
		value="<?php echo $_smarty_tpl->tpl_vars['args']->value;?>
">
	</form>
		<?php }?>
			
					</td>
				</tr>
				
			<?php }?>
		<?php } ?>
		<?php if ($_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_task_page']=="completed_work") {?>
			<tr><th style="vertical-align:top;text-align:right;padding:5px 0;">
				Rating Student</th>
			<td style="padding:5px 10px;">
			
		<div id='tfs_show'>	
		<?php if ($_smarty_tpl->tpl_vars['get_status']->value[2]!='') {?>
			<?php echo $_smarty_tpl->tpl_vars['get_status']->value[2];?>

		<?php } else { ?>
			not yet
		<?php }?>
		</div>	
		<form method="POST" id="rfs" style="margin-bottom: 20px;">
		<input type="hidden" name="controller" value= "task">
		<input type="hidden" name="action" value= "rating_for_student">
		<input type="hidden" name="args" value="<?php echo $_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'];?>
">
		 Rating: 
		<div class="starRating"><?php echo $_smarty_tpl->tpl_vars['stars']->value;?>
</div> <br>or 
		<input type="radio" name="rating_value" value="0" checked="check" 
			style="vertical-align: top;"> 0 Star
		</form>
		
		<a href="javascript:void();" class="my_btn" 
		onclick="javascript:asf_v3('rfs','tfs_show','loading_icon','fast');			
			">Rating</a>
				
				
			
			</td>
			</tr>
		<?php }?>
		</table>
		
	</div>
		
	</div>
	</div>
	<div class="col-md-6" >
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Bid</h3>
			</div>
			<div class="box-body">
	<?php $_smarty_tpl->tpl_vars['check_bid'] = new Smarty_variable($_smarty_tpl->tpl_vars['cur_class']->value->check_bid($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id']), null, 0);?>
		<?php if ($_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_task_page']=="my_tasks") {?>
			<form method="POST" id="update_frm">
			<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_update">			
			<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->cur_arg;?>
">
			<input type="hidden" name="not_refresh_list" value="task">
			<div id="edit_frm" ></div>
			</form>
			<button style="display:none;" class="my_btn" id="smb_btn"
			onclick="javascript:asf_v3('update_frm','show_result','loading_icon','fast');">
			Submit</button>
			<script type="text/javascript">
				document.getElementById('update_frm').onkeydown = function(e){
				   if(e.keyCode == 13){
						asf_v3('update_frm','show_result','loading_icon','fast');
				   }
				};
			</script>
			<div><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['task_status']->value;?>
<?php $_tmp5=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['cur_class']->value->task_bids_list($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'],"yes",$_tmp5);?>
</div>
		<?php } elseif ($_GET['del_item']=='1') {?>
			<form method="POST" id="del_frm">
	You are deleting this item...
	<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_delete">			
	<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->cur_arg;?>
">
	</form>
	<button style="color:red;" id="del_btn"
	onclick="javascript:asf_v3('del_frm','show_result','loading_icon','fast');">Delete</button>
		<?php } elseif ($_smarty_tpl->tpl_vars['check_bid']->value!="0") {?>
			You bid this Task
			<hr>
			<div><?php echo $_smarty_tpl->tpl_vars['cur_class']->value->task_bids_list($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id']);?>
</div>
		<?php } elseif ($_smarty_tpl->tpl_vars['show_accept_btn']->value!='not_show'&&($_SESSION['user']['team_id']=="0")) {?>	
			<div id="mbtn" style="margin:10px 5px">	
				<form id="bid_frm" method="POST">
	<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="bid_task">			
	<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'];?>
">
				<textarea placeholder="Message" class="form-control" 
				style="width:100%;height:200px;" name="bid_message"></textarea>
				</form>
				<div style="margin: 20px 0;text-align: right;">
					<a  href="javascript:void();" class="my_btn"
					onclick="javascript:confirm_bid();">Bid Task</a>
				</div>
				<hr>
			<div><?php echo $_smarty_tpl->tpl_vars['cur_class']->value->task_bids_list($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id']);?>
</div>
			</div>
			<script type="text/javascript">
				function confirm_bid(){
					var txt;
					var r = confirm("Will you accept this taks?");
					if (r == true) {
						asf_v3('bid_frm','mbtn','loading_icon','fast');
					//	document.getElementById("mbtn").innerHTML = "";
					} else {
						txt = "You pressed Cancel!";
					}
					
				}
			</script>
		<?php } elseif ($_smarty_tpl->tpl_vars['cur_task']->value=="me") {?>	
			<div><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['task_status']->value;?>
<?php $_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['cur_class']->value->task_bids_list($_smarty_tpl->tpl_vars['item_details']->value[0]['task_id'],"yes",$_tmp6);?>
</div>
		<?php }?>
	</div>
	</div>
	</div>
	
</div>





















<?php }} ?>
