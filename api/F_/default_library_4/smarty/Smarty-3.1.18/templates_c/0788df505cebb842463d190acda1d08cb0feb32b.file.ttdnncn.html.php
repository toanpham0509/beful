<?php /* Smarty version Smarty-3.1.18, created on 2014-08-04 18:00:51
         compiled from "mvc3\class\dvc\ttdnncn.html" */ ?>
<?php /*%%SmartyHeaderCode:1943053df58fc749902-93138384%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0788df505cebb842463d190acda1d08cb0feb32b' => 
    array (
      0 => 'mvc3\\class\\dvc\\ttdnncn.html',
      1 => 1407146432,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1943053df58fc749902-93138384',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_53df58fc8459e6_14097409',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53df58fc8459e6_14097409')) {function content_53df58fc8459e6_14097409($_smarty_tpl) {?><?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['title'] = "Thỏa thuận đấu nối nguồn cấp nước";?>
<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][1] = "
			- Bước 1: Khách hàng nộp hồ sơ và nhận phiếu hẹn tại Phòng Hành chính – Quản trị.<br> 
			- Bước 2: Khách hàng nhận kết quả theo giấy hẹn tại Phòng Hành chính - Quản trị.
		";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][2] = "
			Trực tiếp tại Phòng Hành chính - Quản trị, Công ty Nước sạch Hà Nội: 
			Số 44 Đường Yên Phụ, Quận Ba Đình, Hà Nội
		";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][3] = "
			a) Thành phần hồ sơ bao gồm: 
- Công văn đề nghị thỏa thuận về đấu nối nguồn cấp nước (có mẫu)<br>
- Quyết định thành lập doanh nghiệp hoặc Đăng ký doanh nghiệp nếu là
 doanh nghiệp/quyết định thành lập cơ quan.<br>
- Bản vẽ xác định vị trí khu đất dự án/công trình (bản sao có dấu xác nhận của cơ 
quan xin thỏa thuận).<br>
- Bản vẽ quy hoạch tổng thể mặt bằng tỷ lệ 1/500 
(bản sao có dấu xác nhận của cơ quan xin thỏa thuận).<br>
- Bảng tính toán (dự kiến) tổng nhu cầu dùng nước của dự án/công trình. <br>
- Văn bản pháp lý liên quan dự án/công trình: Quyết định của cấp thẩm quyền
 hoặc người có thẩm quyền cho phép đầu tư dự án/công trình (bản sao chứng thực).<br>
b) Số lượng hồ sơ: 01 (bộ)

		";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][4] = "07 ngày làm việc kể từ ngày nhận đủ hồ sơ hợp lệ";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][5] = "Cơ quan - doanh nghiệp (Đối với các dự án đầu tư xây dựng 
		khu nhà ở đô thị, khu tái định cư, khu nhà ở công nhân, khu - cụm công nghiệp, thương mại và dịch vụ)";?>
	
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][6] = "Đơn vị có thẩm quyền quyết định theo quy định: Công ty Nước sạch Hà Nội ";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][7] = "Văn bản thỏa thuận về đấu nối nguồn cấp nước";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][8] = "Không";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][9] = "Công văn đề nghị thỏa thuận về đấu nối nguồn cấp nước";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][10] = "Khách hàng nộp hồ sơ phải có giấy giới thiệu của cơ quan/doanh nghiệp xin thỏa thuận và phối hợp cùng đơn vị cấp nước kiểm tra, khảo sát, xác định vị trí dự án/công trình tại hiện trường làm cơ sở để trả lời đấu nối cấp nước. ";?>
		<?php $_smarty_tpl->createLocalArrayVariable('dvc', null, 0);
$_smarty_tpl->tpl_vars['dvc']->value[1]['sub_dv'][1]['content'][11] = "- Nghị định số 117/2007/NĐ-CP ngày 11/07/2007 của Chính Phủ về Sản xuất, cung cấp và tiêu thụ nước sạch.<br>
- Quyết định số 69/2013/QĐ-UBND ngày 30/12/2013 của UBND thành phố Hà Nội về Ban hành quy định về sản xuất, cung cấp, sử dụng nước sạch và bảo vệ công trình cấp nước trên địa bàn thành phố Hà Nội
";?><?php }} ?>
