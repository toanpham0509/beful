<?php /* Smarty version Smarty-3.1.18, created on 2015-05-15 17:24:38
         compiled from "app\library\template\front_end\login.html" */ ?>
<?php /*%%SmartyHeaderCode:15204555303c4120792-33908142%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c143011dc783302388a4607a5e14dc099c9baf0' => 
    array (
      0 => 'app\\library\\template\\front_end\\login.html',
      1 => 1431685314,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15204555303c4120792-33908142',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_555303c41a76c7_51094943',
  'variables' => 
  array (
    'check_register' => 0,
    'this_view' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_555303c41a76c7_51094943')) {function content_555303c41a76c7_51094943($_smarty_tpl) {?><head>
	<style>
		.form_login{
			margin:auto;
			float:none;
		}
		.col-md-3{
			width:35%;
		}
		.col-lg-8{
			width:56.667%;
		}
	</style>
</head>
<br /><br />
<div class="row">
	<div class="col-lg-3 col-md-3">
    </div>
    <div class="col-lg-6 col-md-6 form_login">
	<h4 class="text-success"><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['check_register']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator($_tmp1);?>
</h4>
    <form action="" method="post">
    	<div class="row">
        	<div class="col-lg-3 col-md-3">
            	<label for="user_name" class="label">
	            	<span class="text-danger">*</span>
	                <?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("user_name");?>
:
                </label>
            </div>
            <div class="col-lg-8 col-md-8">
            	<input type="text" id="user_name" name="user_name" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("user_name");?>
" />
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3">
            	<label for="user_password" class="label">
	            	<span class="text-danger">*</span>
	                <?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("user_password");?>
:
                </label>
            </div>
            <div class="col-lg-8 col-md-8">
            	<input type="password" id="user_password" name="user_password" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("user_password");?>
"/>
            </div>
        </div>
        <div class="row">
        	<div class="col-lg-3 col-md-3">
            </div>
            <div class="col-lg-8">
            	<br />
            	<button type="submit" name="login" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("login");?>
</button>
                <br /><br />
                <a href="<<?php ?>?php echo BASE_URL ?<?php ?>>register.php" title="Đăng ký"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("register");?>
</a> | <a href="" title=""  data-toggle="modal" data-target="#passsword"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("forgot_password");?>
</a>
                <!-- Modal -->
                <div class="modal fade" id="passsword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("forgot_password");?>
</h4>
                      </div>
                      <div class="modal-body">
                      	<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("send_new_password");?>

                        	<div class="row">
                                <div class="col-lg-3">
                                    <label for="user_email" class="label">
                                        <span class="text-danger">*</span>
                                        Email:
                                    </label>
                                </div>
                                <div class="col-lg-8">
                                    <input type="text" id="user_email" name="user_email" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("your_email");?>
 "/>
                                </div>	
							</div>
                      </div>
                      <div class="modal-footer">
                      		<button type="button" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("send");?>
</button>
                        	<button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("close");?>
</button>
                      </div>
                    </div>
                  </div>
                </div>
                
            </div>
        </div>
    </form>
    </div>
    <div class="col-lg-3 col-md-3">
    </div>
</div><?php }} ?>
