<?php /* Smarty version Smarty-3.1.18, created on 2015-09-30 06:34:32
         compiled from "app\library\template\promote_management\ViewList.php" */ ?>
<?php /*%%SmartyHeaderCode:449560b18b40ec301-10359487%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ceed7f768cd80ec0c48441a732a92ac6c362e327' => 
    array (
      0 => 'app\\library\\template\\promote_management\\ViewList.php',
      1 => 1443569671,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '449560b18b40ec301-10359487',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_560b18b4179ed8_63805242',
  'variables' => 
  array (
    'this_view' => 0,
    'front_end_url' => 0,
    'item_list' => 0,
    'data' => 0,
    'field_value' => 0,
    'item_list_value' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_560b18b4179ed8_63805242')) {function content_560b18b4179ed8_63805242($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['front_end_url'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['root_url']).((string)$_smarty_tpl->tpl_vars['this_view']->value->config_dir['front_end']), null, 0);?>
<div class="content">
    <div class="nav-ban-hang nav-sale-off">
        <div class="line">
            <h4>Khuyến mại / Giảm giá</h4>
            <div class="search-ban-hang">
                <form method="post" action="" >
                    <img src="<?php echo $_smarty_tpl->tpl_vars['front_end_url']->value;?>
images/icon_search.png"/>
                    <span class="saleoff icon-search non-display">Tên chương trình Khuyến mại / Giảm giá 
                        <a href="#" id="saleoff" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="start_date icon-search non-display">Ngày bắt đầu 
                        <a href="#" id="start_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="end_date icon-search non-display">Ngày kết thúc 
                        <a href="#" id="end_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a  
					href="javascript:void();" 
					onclick="javascript:
					this.innerHTML = 'loading...';
					ajax_get('<?php echo $_smarty_tpl->tpl_vars['item_list']->value['root_url'];?>
&controller=<?php echo $_smarty_tpl->tpl_vars['item_list']->value['cur_class'];?>
&action=item_add_new',
					'right_main');
				">
                    <input                        
						type="button" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Tạo mới"/>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <p style="float: left;">1-1 của 10</p>
                <ul class="page">
                    <li style="border-left: gray 1px solid;;font-weight: bold; border-right: gray 1px solid;"><a href="">&lt;</a></li> 
                    <li style="border-right: gray 1px solid; font-weight: bold;"><a href="">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>Tên chương trình Khuyến mại / Giảm giá 
                            <br/>
                            <a id="saleoff" href="#">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['front_end_url']->value;?>
images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="saleoff" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày bắt đầu 
                            <br/>
                            <a id="start_date" href="#">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['front_end_url']->value;?>
images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
            <div class="search-infor date_1 non-display">
                <input type="date" name="start_date"/>
            </div>
            </th>
            <th class="search-order">
                <label> Ngày kết thúc 
                    <br/>
                    <a id="end_date" href="#">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['front_end_url']->value;?>
images/icon_search.png"/>
                    </a>
                </label>
                <br/>
            <div class="search-infor date_2 non-display">
                <input type="date" name="end_date"/>
            </div>
            </th>
            </tr>
            </thead>
            <tbody>
				<?php  $_smarty_tpl->tpl_vars['item_list_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_list_value']->_loop = false;
 $_smarty_tpl->tpl_vars['item_list_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value['item_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_list_value']->key => $_smarty_tpl->tpl_vars['item_list_value']->value) {
$_smarty_tpl->tpl_vars['item_list_value']->_loop = true;
 $_smarty_tpl->tpl_vars['item_list_k']->value = $_smarty_tpl->tpl_vars['item_list_value']->key;
?>
				<tr>
					<?php  $_smarty_tpl->tpl_vars['field_value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_value']->_loop = false;
 $_smarty_tpl->tpl_vars['field_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_value']->key => $_smarty_tpl->tpl_vars['field_value']->value) {
$_smarty_tpl->tpl_vars['field_value']->_loop = true;
 $_smarty_tpl->tpl_vars['field_k']->value = $_smarty_tpl->tpl_vars['field_value']->key;
?>
						<?php if ($_smarty_tpl->tpl_vars['field_value']->value['COLUMN_KEY']!="PRI") {?>
						<td><?php echo $_smarty_tpl->tpl_vars['item_list_value']->value[$_smarty_tpl->tpl_vars['field_value']->value['column_name']];?>
</td>
						<?php }?>
					<?php } ?>
				</tr>
				<?php } ?>
            </tbody>
        </table> 
    </div>
</div>
<<?php ?>?php
/*end of file ViewList.php*/
/*location: ViewList.php*/<?php }} ?>
