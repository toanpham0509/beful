<?php /* Smarty version Smarty-3.1.18, created on 2014-12-14 22:58:52
         compiled from "gwt\view\woothemes-FlexSlider\thumbnail-controlnav.html" */ ?>
<?php /*%%SmartyHeaderCode:3829548db005f1c365-52721642%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'caf67bb7e197f107356d71d0d836b96e9396d3fd' => 
    array (
      0 => 'gwt\\view\\woothemes-FlexSlider\\thumbnail-controlnav.html',
      1 => 1418572727,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3829548db005f1c365-52721642',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_548db006074385_22217782',
  'variables' => 
  array (
    'this_class' => 0,
    'k' => 0,
    'arr' => 0,
    'root_url' => 0,
    'fld_code' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_548db006074385_22217782')) {function content_548db006074385_22217782($_smarty_tpl) {?><ul class="tc_listnews">
<li class="clearfix">
<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['this_class']->value->this_model->config; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
	<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
:<?php echo $_smarty_tpl->tpl_vars['arr']->value;?>
<br>
<?php } ?>
<?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->this_model->config['root_url'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->this_model->config['fld_code'], null, 0);?>

	<!-- Modernizr -->
  <script src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/modernizr.js"></script>


<div class="loading">

  <div id="container" class="cf">
    <header role="navigation">
      <a class="logo" href="http://www.woothemes.com" title="WooThemes">
        <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/images/logo.png" alt="WooThemes" />
	  </a>
      <h1>FlexSlider 2</h1>
      <h2>The best responsive slider. Period.</h2>
      <a class="button green" href="https://github.com/woothemes/FlexSlider/zipball/master">Download Flexslider</a>
      <h3 class="nav-header">Other Examples</h3>
      <nav>
        <ul>
          <li><a href="index.html">Basic Slider</a></li>
          <li class="active"><a href="thumbnail-controlnav.html">Slider w/thumbnail controlNav pattern</a></li>
          <li><a href="thumbnail-slider.html">Slider w/thumbnail slider</a></li>
          <li><a href="basic-carousel.html">Basic Carousel</a></li>
          <li><a href="carousel-min-max.html">Carousel with min and max ranges</a></li>
          <li><a href="dynamic-carousel-min-max.html">Carousel with dynamic min/max ranges</a></li>
          <li><a href="video.html">Video & the api (vimeo)</a></li>
          <li><a href="video-wistia.html">Video & the api (wistia)</a></li>
        </ul>
      </nav>
    </header>

	<div id="main" role="main">
      <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            <li data-thumb="images/kitchen_adventurer_cheesecake_brownie.jpg">
  	    	    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/images/kitchen_adventurer_cheesecake_brownie.jpg" />
  	    		</li>
  	    		<li data-thumb="images/kitchen_adventurer_lemon.jpg">
  	    	    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/images/kitchen_adventurer_lemon.jpg" />
  	    		</li>
  	    		<li data-thumb="images/kitchen_adventurer_donut.jpg">
  	    	    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/images/kitchen_adventurer_donut.jpg" />
  	    		</li>
  	    		<li data-thumb="images/kitchen_adventurer_caramel.jpg">
  	    	    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/images/kitchen_adventurer_caramel.jpg" />
  	    		</li>
          </ul>
        </div>
      </section>
      <aside>
        <div class="cf">
          <h3>Thumbnail ControlNav Pattern</h3>
          <ul class="toggle cf">
            <li class="js"><a href="#view-js">JS</a></li>
            <li class="html"><a href="#view-html">HTML</a></li>
          </ul>
        </div>
        
       
      </aside>
    </div>

  </div>

  <!-- jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/libs/jquery-1.7.min.js">\x3C/script>')</script>

  <!-- FlexSlider -->
  <script defer src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>


  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/jquery.easing.js"></script>
  <script src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
view/woothemes-FlexSlider/js/demo.js"></script>

</div>
</li>
</ul>
<?php }} ?>
