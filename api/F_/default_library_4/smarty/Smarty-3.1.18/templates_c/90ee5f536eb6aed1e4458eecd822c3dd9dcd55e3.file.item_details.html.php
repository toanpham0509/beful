<?php /* Smarty version Smarty-3.1.18, created on 2015-02-12 11:21:06
         compiled from "base/view/AdminLTE-master/item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:183138545454dc8ca2a11cc7-94051931%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90ee5f536eb6aed1e4458eecd822c3dd9dcd55e3' => 
    array (
      0 => 'base/view/AdminLTE-master/item_details.html',
      1 => 1423782900,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183138545454dc8ca2a11cc7-94051931',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'updated_status' => 0,
    'lang_file' => 0,
    'this_view' => 0,
    'before_fields' => 0,
    'frm_content' => 0,
    'arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'item_id' => 0,
    'after_fields' => 0,
    'attach_frm' => 0,
    'this_class' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54dc8ca2b6c193_61405581',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54dc8ca2b6c193_61405581')) {function content_54dc8ca2b6c193_61405581($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['updated_status']->value==false) {?>
<div class="edit_form" style="float:left;width:100%;">
<div id="popup_window">
<?php }?>
	<div style="" class="row">
	<div class="col-md-12">
		<!-- Primary box -->
		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("details",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
				<div class="box-tools pull-right">
					<button data-widget="collapse" class="hide_class btn btn-primary btn-sm"><i class="fa fa-minus"></i></button>
					<a href="" class="btn btn-primary btn-sm" style='color:white;'><i class="fa fa-times"></i></a>
				</div>
			</div>
			<div class="box-body" style="display: block;background: none repeat scroll 0 0 #eef;
    float: left;width: 100%;min-height:400px;">
				<div class="col-md-6" >
					<div class="box box-primary" >
						<div class="box-header">
							<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("fields",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
						</div>
					<div class="box-body" id="cur_item_details">
					<div class="hide_class" id="item_edit_popup" style="margin-bottom:0px;">
						<div class="popup_content" >
						<div class="popup_header">
							<?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("update_frm",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>

							<div class="popup_close_btn ">
							<a href="javascript:void();" style="color:white;" onclick="javascript:
							document.getElementById('item_edit_popup').className = 'hide_class';
							">x</a></div>
						</div><br>
						<div id="item_edit_frm"></div>
						</div>
					</div>
					<?php echo $_smarty_tpl->tpl_vars['before_fields']->value;?>

					<table>	
					<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['frm_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
					<tr class="form-group">
						<th class='details_titlte'><label ><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['arr']->value['field_label'];?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['lang_file']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator($_tmp1,$_tmp2);?>
</label></th>
						<td style="max-width: 70px;">		
							<a href="javascript:void();" onclick="javascript:
							document.getElementById('item_edit_popup').className = 'popup_background';
							gwt_result
						('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_edit&args=<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_id'];?>
;<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['frm_type'];?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_name'];?>
'
							,'popup_window','item_edit_frm','edit_frm_item')">
							<?php if ($_smarty_tpl->tpl_vars['arr']->value['cur_value']) {?>
								<?php echo $_smarty_tpl->tpl_vars['arr']->value['cur_value'];?>
 
							<?php } else { ?>
								--- 
							<?php }?>
							</a>
						</td>
					</tr>
					<?php } ?>		
					</table>
					<?php echo $_smarty_tpl->tpl_vars['after_fields']->value;?>

					
					</div>
					<div class="box-footer" style="text-align:center;padding: 25px;">
						<a href="javascript:void();" class='my_btn' style="background:red;"
						onclick="javascript:gwt_result
					('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=del_item&args=<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
','','popup_window','')"
						><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("delete_button",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a>
						<a href="" class='my_btn' style="color:white;"
						onclick="javascript:document.getElementById('head_form').innerHTML = '...backing...';" 	
							><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("back_link",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</a>
					</div>	
					</div>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?>
				<div class="col-md-6" >
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("attach_file",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
						</div>
						<div class="box-body"><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
</div>
					</div>
				</div>	
				<?php }?>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
	
	
	
	
	
</div>
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->updated==false) {?>
</div></div>
<?php }?>
<?php }} ?>
