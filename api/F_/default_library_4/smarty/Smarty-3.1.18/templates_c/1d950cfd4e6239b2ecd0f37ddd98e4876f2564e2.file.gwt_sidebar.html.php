<?php /* Smarty version Smarty-3.1.18, created on 2015-01-27 20:11:09
         compiled from "gwt\base\view\AdminLTE-master\gwt_sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:1508254c78e6de88050-58288391%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d950cfd4e6239b2ecd0f37ddd98e4876f2564e2' => 
    array (
      0 => 'gwt\\base\\view\\AdminLTE-master\\gwt_sidebar.html',
      1 => 1420549738,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1508254c78e6de88050-58288391',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_class' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54c78e6dedf4b5_08605364',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c78e6dedf4b5_08605364')) {function content_54c78e6dedf4b5_08605364($_smarty_tpl) {?>
<!-- Sidebar user panel -->
<div class="user-panel">
	<div class="pull-left image">
		<img src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->template_fld;?>
img/avatar3.png" class="img-circle" alt="User Image" />
	</div>
	<div class="pull-left info">
		<p>Hello, Admin </p>

		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
		<input type="text" name="q" class="form-control" placeholder="Search..."/>
		<span class="input-group-btn">
			<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
		</span>
	</div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="active">
		<a href="index.html">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Nguyên liệu, đồ uống</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nldu/"><i class="fa fa-angle-double-right">
			</i>Danh sách, tồn kho, chi tiết xuất nhập</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nhapkho/"><i class="fa fa-angle-double-right">
			</i>Nhập hàng</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_xuatkho/"><i class="fa fa-angle-double-right">
			</i>Xuất hàng</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nldu_dvt/"><i class="fa fa-angle-double-right">
			</i>Đơn vị tính</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Món ăn</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_nhomthucdon/"><i class="fa fa-angle-double-right">
			</i>Nhóm thực đơn</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_monan/"><i class="fa fa-angle-double-right">
			</i>Thực đơn</a></li>
			
		</ul>
	</li>

</ul><?php }} ?>
