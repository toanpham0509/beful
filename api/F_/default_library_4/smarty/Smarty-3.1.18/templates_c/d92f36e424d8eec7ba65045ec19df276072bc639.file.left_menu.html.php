<?php /* Smarty version Smarty-3.1.18, created on 2015-03-04 16:53:09
         compiled from "core\view\AdminLTE-master\left_menu.html" */ ?>
<?php /*%%SmartyHeaderCode:1670654f6d605707928-91451516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd92f36e424d8eec7ba65045ec19df276072bc639' => 
    array (
      0 => 'core\\view\\AdminLTE-master\\left_menu.html',
      1 => 1424967619,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1670654f6d605707928-91451516',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'menu_config' => 0,
    'menu_list' => 0,
    'cur_class' => 0,
    'display' => 0,
    'arr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54f6d605867ca2_61595218',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54f6d605867ca2_61595218')) {function content_54f6d605867ca2_61595218($_smarty_tpl) {?><!-- sidebar menu: : style can be found in sidebar.less -->

<ul class="sidebar-menu">
	<li class="">
		<a href="<?php echo $_smarty_tpl->tpl_vars['menu_config']->value['root_url'];?>
">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<?php  $_smarty_tpl->tpl_vars['menu_list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menu_list']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu_config']->value['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menu_list']->key => $_smarty_tpl->tpl_vars['menu_list']->value) {
$_smarty_tpl->tpl_vars['menu_list']->_loop = true;
?>		
		<?php if ($_smarty_tpl->tpl_vars['menu_list']->value['sub_menu']) {?>
		
		<?php if ($_smarty_tpl->tpl_vars['menu_list']->value['check_active']=='1') {?>
			<?php $_smarty_tpl->tpl_vars['cur_class'] = new Smarty_variable("active", null, 0);?>
			<?php $_smarty_tpl->tpl_vars['display'] = new Smarty_variable("block;", null, 0);?>
		<?php } else { ?>	
			<?php $_smarty_tpl->tpl_vars['cur_class'] = new Smarty_variable('', null, 0);?>
			<?php $_smarty_tpl->tpl_vars['display'] = new Smarty_variable("none;", null, 0);?>
		<?php }?>
		
		<li class="treeview">
			<a href="<?php echo $_smarty_tpl->tpl_vars['menu_list']->value['menu']['menu_url'];?>
">
				<i class="fa fa-edit"></i> <span><?php echo $_smarty_tpl->tpl_vars['menu_list']->value['menu']['menu_label'];?>
</span>
				<i class="fa fa-angle-left pull-right"></i>
			</a>			
			<ul class="treeview-menu <?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
" style="display:<?php echo $_smarty_tpl->tpl_vars['display']->value;?>
">
				<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu_list']->value['sub_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
?>
					<li class="<?php echo $_smarty_tpl->tpl_vars['arr']->value['sub_menu_class'];?>
"><a href="<?php echo $_smarty_tpl->tpl_vars['arr']->value['sub_menu_url'];?>
">
					<i class="fa fa-angle-double-right"></i><?php echo $_smarty_tpl->tpl_vars['arr']->value['sub_menu_label'];?>
</a></li>
				<?php } ?>				
			</ul>
		</li>
		<?php } else { ?>
			<li class="<?php echo $_smarty_tpl->tpl_vars['menu_list']->value['menu']['menu_class'];?>
">
				<a href="<?php echo $_smarty_tpl->tpl_vars['menu_list']->value['menu']['menu_url'];?>
">
					<i class="fa fa-edit"></i> <span><?php echo $_smarty_tpl->tpl_vars['menu_list']->value['menu']['menu_label'];?>
</span>
				</a>
			</li>
		<?php }?>
	<?php } ?>
	

</ul><?php }} ?>
