<?php /* Smarty version Smarty-3.1.18, created on 2015-02-12 11:17:48
         compiled from "base/view/AdminLTE-master/gwt_sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:143897322954dc8bdccdcd80-29836940%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a094221a2f14a12c707fabb9aca25cd477efb49' => 
    array (
      0 => 'base/view/AdminLTE-master/gwt_sidebar.html',
      1 => 1423782900,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '143897322954dc8bdccdcd80-29836940',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_class' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54dc8bdcd3fab5_02276743',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54dc8bdcd3fab5_02276743')) {function content_54dc8bdcd3fab5_02276743($_smarty_tpl) {?>
<!-- Sidebar user panel -->
<div class="user-panel">
	<div class="pull-left image">
		<img src="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
<?php echo $_smarty_tpl->tpl_vars['page_class']->value->template_fld;?>
img/avatar3.png" class="img-circle" alt="User Image" />
	</div>
	<div class="pull-left info">
		<p>Hello, Admin </p>

		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		<a href="&action=logout">logout</a>
	</div>
</div>
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
		<input type="text" name="q" class="form-control" placeholder="Search..."/>
		<span class="input-group-btn">
			<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
		</span>
	</div>
</form>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="active">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->config_page->config_page['root_url'];?>
">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Members</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_company/"><i class="fa fa-angle-double-right">
			</i>admin_company</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_client/"><i class="fa fa-angle-double-right">
			</i>admin_user</a></li>
		</ul>
	</li>
	<li class="">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_pos/">
			<i class="fa fa-edit"></i> <span>POS</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>	
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Member Card</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_member_card_category/"><i class="fa fa-angle-double-right">
			</i>admin_member_card_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_member_card/"><i class="fa fa-angle-double-right">
			</i>admin_member_card</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Stamp Card</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_category/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_card/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_card</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_stamp_history/"><i class="fa fa-angle-double-right">
			</i>admin_stamp_history</a></li>
		</ul>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Coupon</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_coupon_category/"><i class="fa fa-angle-double-right">
			</i>admin_coupon_category</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_coupon/"><i class="fa fa-angle-double-right">
			</i>admin_coupon</a></li>
		</ul>
	</li>
	<li class="">
		<a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_news/">
			<i class="fa fa-edit"></i> <span>News</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
	</li>
	<li class="treeview">
		<a href="javascript:void();">
			<i class="fa fa-edit"></i> <span>Other</span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_activity/"><i class="fa fa-angle-double-right">
			</i>admin_activity</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_civility/"><i class="fa fa-angle-double-right">
			</i>admin_civility</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_country/"><i class="fa fa-angle-double-right">
			</i>admin_country</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_city/"><i class="fa fa-angle-double-right">
			</i>admin_city</a></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['page_class']->value->root_url;?>
admin_status/"><i class="fa fa-angle-double-right">
			</i>admin_status</a></li>
			
		</ul>
	</li>

</ul><?php }} ?>
