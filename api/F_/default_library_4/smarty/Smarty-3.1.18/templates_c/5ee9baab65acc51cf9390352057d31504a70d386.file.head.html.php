<?php /* Smarty version Smarty-3.1.18, created on 2014-06-21 21:29:48
         compiled from "system\templates\Aquarius_responsive_admin_panel\head.html" */ ?>
<?php /*%%SmartyHeaderCode:3115453a588cc7b5cd0-02992231%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ee9baab65acc51cf9390352057d31504a70d386' => 
    array (
      0 => 'system\\templates\\Aquarius_responsive_admin_panel\\head.html',
      1 => 1403357268,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3115453a588cc7b5cd0-02992231',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'template_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_53a588cc81d825_59995109',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a588cc81d825_59995109')) {function content_53a588cc81d825_59995109($_smarty_tpl) {?><head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	
    <title>Tables - Aquarius - responsive admin panel</title>

    <link rel="icon" type="image/ico" href="<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
favicon.ico"/>
    
    <link href="<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
css/stylesheets.css" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' type='text/css' href='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
css/fullcalendar.print.css' media='print' />
    
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'></script>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/jquery/jquery.mousewheel.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/charts/jquery.flot.resize.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/sparklines/jquery.sparkline.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/fullcalendar/fullcalendar.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/select2/select2.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/uniform/uniform.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/maskedinput/jquery.maskedinput-1.3.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/validation/languages/jquery.validationEngine-en.js' charset='utf-8'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/validation/jquery.validationEngine.js' charset='utf-8'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/animatedprogressbar/animated_progressbar.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/qtip/jquery.qtip-1.0.0-rc3.min.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/cleditor/jquery.cleditor.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/dataTables/jquery.dataTables.min.js'></script>    
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins/fancybox/jquery.fancybox.pack.js'></script>
    
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/cookies.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/actions.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/charts.js'></script>
    <script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['template_url']->value;?>
js/plugins.js'></script>
    
</head><?php }} ?>
