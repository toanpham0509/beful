<?php /* Smarty version Smarty-3.1.18, created on 2014-09-06 00:47:37
         compiled from "mvc4\core\view\sb-admin-2\sb_simple_tables.html" */ ?>
<?php /*%%SmartyHeaderCode:94025409de9ddf9869-75728413%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '50593d930e2590c0e7f0e0b22cb6d83ade5adb7a' => 
    array (
      0 => 'mvc4\\core\\view\\sb-admin-2\\sb_simple_tables.html',
      1 => 1409935656,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94025409de9ddf9869-75728413',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5409de9e054585_78252725',
  'variables' => 
  array (
    'this_class' => 0,
    'template_fld' => 0,
    'fields' => 0,
    'i' => 0,
    'item_list' => 0,
    'rr' => 0,
    'i2' => 0,
    'arr' => 0,
    'kk' => 0,
    'arr9' => 0,
    'field_id' => 0,
    'cur_table' => 0,
    'select_multiple_sql' => 0,
    'select_multiple_fields' => 0,
    'multiple_values' => 0,
    'arrm' => 0,
    'tmp_value' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5409de9e054585_78252725')) {function content_5409de9e054585_78252725($_smarty_tpl) {?><div class="row" style="display:none;height:20px;">
	<div class="col-lg-12">
		<h1 class="page-header">Tables</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row" style="height:15px;">
	<div class="col-lg-12">
		
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" style="margin-top:0px;">
	<div class="col-lg-12">
		<div class="panel panel-default">
		   
		   <?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['item_list']['extend_tmp_fld'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['field_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['primary_key'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['cur_table'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['table_name'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['item_list'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->item_list, null, 0);?>
<?php $_smarty_tpl->tpl_vars['fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->fields, null, 0);?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_fld']->value)."z_table_head.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="panel-body">
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
				<tr>
				<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
<?php if ($_smarty_tpl->tpl_vars['i']->value[2]!='hidden') {?>
					<th><?php echo $_smarty_tpl->tpl_vars['i']->value[1];?>
</th>
<?php }?>
<?php } ?> <th class='min_width'>Details</th>
<th class='min_width'>Del</th>	                                           
				</tr>
			</thead>
			<tbody>
				<?php $_smarty_tpl->tpl_vars['rr'] = new Smarty_variable('0', null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
				<span style="display:none;"><?php echo $_smarty_tpl->tpl_vars['rr']->value++;?>
</span>
				<tr class="
				<?php if ($_smarty_tpl->tpl_vars['rr']->value%2=='0') {?>
				odd
				<?php } else { ?>
				even
				<?php }?>
				gradeX">
					
<?php  $_smarty_tpl->tpl_vars['i2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i2']->key => $_smarty_tpl->tpl_vars['i2']->value) {
$_smarty_tpl->tpl_vars['i2']->_loop = true;
?>
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]!='hidden') {?>
	<td> 
	<?php if ($_smarty_tpl->tpl_vars['i2']->value[2]=="select_multiple") {?>
	<span style="display:None;">		
	<?php  $_smarty_tpl->tpl_vars['arr9'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr9']->_loop = false;
 $_smarty_tpl->tpl_vars['kk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['arr']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr9']->key => $_smarty_tpl->tpl_vars['arr9']->value) {
$_smarty_tpl->tpl_vars['arr9']->_loop = true;
 $_smarty_tpl->tpl_vars['kk']->value = $_smarty_tpl->tpl_vars['arr9']->key;
?>
			<?php echo $_smarty_tpl->tpl_vars['kk']->value;?>
:<?php echo $_smarty_tpl->tpl_vars['arr9']->value;?>
<br>
		<?php } ?>
		>><?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
<<
	<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
- <?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
 -	<?php echo $_smarty_tpl->tpl_vars['i2']->value[4];?>
 -<?php echo $_smarty_tpl->tpl_vars['i2']->value[5];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[6];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[7];?>
 - <?php echo $_smarty_tpl->tpl_vars['i2']->value[8];?>

	</span>
<?php $_smarty_tpl->tpl_vars['select_multiple_sql'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['this_class']->value->class_info['select_multiple'][$_smarty_tpl->tpl_vars['i2']->value[5]]['db_sql']).((string)$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value]), null, 0);?>			
<?php $_smarty_tpl->tpl_vars['select_multiple_fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['select_multiple'][$_smarty_tpl->tpl_vars['i2']->value[5]]['fields'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['multiple_values'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->basic_model->fetchAll($_smarty_tpl->tpl_vars['select_multiple_sql']->value,$_smarty_tpl->tpl_vars['select_multiple_fields']->value), null, 0);?>				
<?php $_smarty_tpl->createLocalArrayVariable('arr', null, 0);
$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]] = '';?>
<?php  $_smarty_tpl->tpl_vars['arrm'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arrm']->_loop = false;
 $_smarty_tpl->tpl_vars['km'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['multiple_values']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arrm']->key => $_smarty_tpl->tpl_vars['arrm']->value) {
$_smarty_tpl->tpl_vars['arrm']->_loop = true;
 $_smarty_tpl->tpl_vars['km']->value = $_smarty_tpl->tpl_vars['arrm']->key;
?>
<?php $_smarty_tpl->tpl_vars['tmp_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['arrm']->value[$_smarty_tpl->tpl_vars['select_multiple_fields']->value[1][0]], null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]]=='') {?>
<?php $_smarty_tpl->createLocalArrayVariable('arr', null, 0);
$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]] = $_smarty_tpl->tpl_vars['tmp_value']->value;?>
<?php } else { ?>
<?php $_smarty_tpl->createLocalArrayVariable('arr', null, 0);
$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]] = ((string)$_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]]).", ".((string)$_smarty_tpl->tpl_vars['tmp_value']->value);?>
<?php }?>	
<?php } ?>
		
		
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]]) {?>
		<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['i2']->value[0]];?>

	<?php } else { ?>
		---
	<?php }?>	
	</td>
	<?php }?>
<?php } ?>
<td class="td_center">
<form method="post" id="frm_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" class="tbl_frm">
<input type="hidden" name="actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="item_details">
<input type="hidden" name="args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]" value="<?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
.<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
">
<a href="javascript:asf_v3('frm_<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
','show_result','loading_icon','fast');"
onclick ="document.getElementById('result_background').style.display	= 'block';"
>view</a>
</form>
</td>
<td class="td_center">
<a href="javascript:ajax_get('actions_list[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]'
,'item_details'
,'args[<?php echo $_smarty_tpl->tpl_vars['this_class']->value->class_info['cur_class'];?>
][0]=<?php echo $_smarty_tpl->tpl_vars['cur_table']->value;?>
.<?php echo $_smarty_tpl->tpl_vars['field_id']->value;?>
=<?php echo $_smarty_tpl->tpl_vars['arr']->value[$_smarty_tpl->tpl_vars['field_id']->value];?>
&del_item=1'
,'show_result');"
onclick ="document.getElementById('result_background').style.display	= 'block';"
>
del</a></td>

				</tr>
<?php } ?>				
			</tbody>
		</table>
	</div>
	<!-- /.table-responsive -->
	<div class="well" style="display:none;">
		<h4>DataTables Usage Information</h4>
		<p>DataTables is a very flexible, advanced tables plugin for jQuery. In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3. We have also customized the table headings to use Font Awesome icons in place of images. For complete documentation on DataTables, visit their website at <a target="_blank" href="https://datatables.net/">https://datatables.net/</a>.</p>
		<a class="btn btn-default btn-lg btn-block" target="_blank" href="https://datatables.net/">View DataTables Documentation</a>
	</div>
</div>
<!-- /.panel-body -->
</div>
<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php }} ?>
