<?php /* Smarty version Smarty-3.1.18, created on 2015-07-07 09:54:38
         compiled from "generator_tools\library\template\base_html\file_editer.html" */ ?>
<?php /*%%SmartyHeaderCode:248335598edde8a2580-13835505%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34acbaca5e7add68da0d750dd9c01c1fb6fc0020' => 
    array (
      0 => 'generator_tools\\library\\template\\base_html\\file_editer.html',
      1 => 1436237656,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '248335598edde8a2580-13835505',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5598edde91c4c0_25552362',
  'variables' => 
  array (
    'data' => 0,
    'arr' => 0,
    'k' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5598edde91c4c0_25552362')) {function content_5598edde91c4c0_25552362($_smarty_tpl) {?><div class="row">
	<div class="col-md-12">
		<!-- Custom Tabs -->
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab_1">Edit file content</a></li>
				
				<li class="dropdown pull-right">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">
						methods<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value['method_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
						<li role="presentation">
						<a draggable="true" ondragstart='return OnDragStart (event,"<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
")' 
						class="drag_item" title='<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
'><?php echo $_smarty_tpl->tpl_vars['arr']->value['name'];?>
</a>
						</li>
						<?php } ?>
						<li class="divider" role="presentation"></li>
						<li role="presentation">
						<a draggable="true" ondragstart="return OnDragStart (event,'new_function')" 
						class="drag_item"
						style=""
						>new method</a>
						</li>
					</ul>
				</li>
				<li class="dropdown pull-right">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">
						properties<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value['item_list_model']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
						<li role="presentation">
						<a draggable="true" ondragstart='return OnDragStart (event,"<?php echo $_smarty_tpl->tpl_vars['arr']->value['name'];?>
")' 
						class="drag_item" title='<?php echo $_smarty_tpl->tpl_vars['arr']->value['value'];?>
'><?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</a>
						</li>
						<?php } ?>
						
					</ul>
				</li>

			</ul>
			<div class="tab-content">
				<div id="tab_1" class="tab-pane active">
					<b>Content:</b>
					<form onsubmit="javascript:ajax_post(this.id,'right_main','upload_progress_bar'); 
					_('right_main').innerHTML = '...loading...';
					" id="update_file_content" method="POST" action="javascript:void();">
						<input type='hidden' name='controller' value='<?php echo $_GET['controller'];?>
'>
						<input type='hidden' name='action' value='<?php echo $_GET['action'];?>
'>
						<input type='hidden' name='args' value='<?php echo $_GET['args'];?>
'>
						<textarea id="file_area" class='form-control' name='file_content' 
						style='width:100%;height:350px;'><?php echo $_smarty_tpl->tpl_vars['data']->value['file_content'];?>
</textarea>
						<br>
						<button type='submit' class='btn btn-primary'>SAVE</button>
					</form>
				</div><!-- /.tab-pane -->
				
			</div><!-- /.tab-content -->
		</div><!-- nav-tabs-custom -->
	</div><!-- /.col -->

   
</div>




<?php }} ?>
