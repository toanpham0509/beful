<?php /* Smarty version Smarty-3.1.18, created on 2015-01-28 20:51:21
         compiled from "gwt\base\view\AdminLTE-master\gwt_table.html" */ ?>
<?php /*%%SmartyHeaderCode:114454c78e6e098ca5-72980107%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6cf199732d08c3732b9c9d298c484e4f46c9ebc' => 
    array (
      0 => 'gwt\\base\\view\\AdminLTE-master\\gwt_table.html',
      1 => 1422453080,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114454c78e6e098ca5-72980107',
  'function' => 
  array (
    'field_list' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54c78e6e1d73d6_04525837',
  'variables' => 
  array (
    'fields' => 0,
    'field_arr' => 0,
    'tl_arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'primary_key' => 0,
    'view' => 0,
    'details' => 0,
    'config_fields' => 0,
    'this_class' => 0,
    'config_table_title' => 0,
    'add_new_btn' => 0,
    'item_pages' => 0,
    'item_list' => 0,
    'tbl_title' => 0,
    'config_primary_key' => 0,
  ),
  'has_nocache_code' => 0,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c78e6e1d73d6_04525837')) {function content_54c78e6e1d73d6_04525837($_smarty_tpl) {?><?php if (!function_exists('smarty_template_function_field_list')) {
    function smarty_template_function_field_list($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['field_list']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
	<?php  $_smarty_tpl->tpl_vars['field_arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_arr']->_loop = false;
 $_smarty_tpl->tpl_vars['field_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_arr']->key => $_smarty_tpl->tpl_vars['field_arr']->value) {
$_smarty_tpl->tpl_vars['field_arr']->_loop = true;
 $_smarty_tpl->tpl_vars['field_key']->value = $_smarty_tpl->tpl_vars['field_arr']->key;
?>		
		<?php if ($_smarty_tpl->tpl_vars['field_arr']->value['field_label']!=''&&$_smarty_tpl->tpl_vars['field_arr']->value['field_type']!="not_show_list") {?>
			<?php if ($_smarty_tpl->tpl_vars['tl_arr']->value) {?>
				<td><?php echo $_smarty_tpl->tpl_vars['tl_arr']->value[$_smarty_tpl->tpl_vars['field_arr']->value['tbl_col']];?>
</td>				
			<?php } else { ?>
				<th><?php echo $_smarty_tpl->tpl_vars['field_arr']->value['field_label'];?>
</th>				
			<?php }?>
		<?php }?>	
	<?php } ?>
	<?php if ($_smarty_tpl->tpl_vars['tl_arr']->value) {?>
	<td class='details_col'><a href="javascript:void();" onclick="javascript:
	gwt_result
	('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_details&args=<?php echo $_smarty_tpl->tpl_vars['tl_arr']->value[$_smarty_tpl->tpl_vars['primary_key']->value];?>
'
		,'','right_main');"><?php echo $_smarty_tpl->tpl_vars['view']->value;?>
</a></td>
	<?php } else { ?>
		<th class='details_col'><?php echo $_smarty_tpl->tpl_vars['details']->value;?>
</th>
	<?php }?>	
<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

<?php ob_start();?><?php smarty_template_function_field_list($_smarty_tpl,array('fields'=>$_smarty_tpl->tpl_vars['config_fields']->value));?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['tbl_title'] = new Smarty_variable($_tmp1, null, 0);?>

<div class="row" id="<?php echo $_smarty_tpl->tpl_vars['this_class']->value->pos;?>
_<?php echo $_smarty_tpl->tpl_vars['this_class']->value->pos_id;?>
">
<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['config_table_title']->value;?>
</h3>
		</div><!-- /.box-header -->
		<div class="box-body table-responsive">
		<div style="float:left;width:100%;">
		<div style="float:left;">
		<a href="javascript:void();" onclick="javascript:
		gwt_result
		('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_add_new','right_main','right_main');		
		" class="btn" style="border:none;box-shadow:none;"><?php echo $_smarty_tpl->tpl_vars['add_new_btn']->value;?>
</a>
		
		</div>
		<div style="float:right;margin-bottom:0px;"><?php echo $_smarty_tpl->tpl_vars['item_pages']->value;?>
</div>
		</div><br>
		<?php if ($_smarty_tpl->tpl_vars['item_list']->value) {?>			
			<table id="" style="" class="table table-bordered table-hover">
				<thead><tr><?php echo $_smarty_tpl->tpl_vars['tbl_title']->value;?>
</tr></thead>
				<tfoot><tr><?php echo $_smarty_tpl->tpl_vars['tbl_title']->value;?>
</tr></tfoot>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['tl_arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tl_arr']->_loop = false;
 $_smarty_tpl->tpl_vars['tl_k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tl_arr']->key => $_smarty_tpl->tpl_vars['tl_arr']->value) {
$_smarty_tpl->tpl_vars['tl_arr']->_loop = true;
 $_smarty_tpl->tpl_vars['tl_k']->value = $_smarty_tpl->tpl_vars['tl_arr']->key;
?>
				<tr>
					<?php smarty_template_function_field_list($_smarty_tpl,array('fields'=>$_smarty_tpl->tpl_vars['config_fields']->value,'tl_arr'=>$_smarty_tpl->tpl_vars['tl_arr']->value,'primary_key'=>$_smarty_tpl->tpl_vars['config_primary_key']->value));?>

				</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php } else { ?>
			
		<?php }?>
		<script type="text/javascript">
		
	</script>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
</div>


<?php }} ?>
