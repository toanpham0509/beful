<?php /* Smarty version Smarty-3.1.18, created on 2014-11-12 15:19:56
         compiled from "mvc4\view\Aquarius_responsive_admin_panel\login_frm.html" */ ?>
<?php /*%%SmartyHeaderCode:1733054408862777929-04091930%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '04ec3387d1f797603af42bcc03d489bd493a8895' => 
    array (
      0 => 'mvc4\\view\\Aquarius_responsive_admin_panel\\login_frm.html',
      1 => 1415780395,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1733054408862777929-04091930',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_5440886277c3c4_51566300',
  'variables' => 
  array (
    'this_class' => 0,
    'page_class' => 0,
    'root_url' => 0,
    'template_fld' => 0,
    'login_result' => 0,
    'status' => 0,
    'fld_code' => 0,
    'url' => 0,
    'args' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5440886277c3c4_51566300')) {function content_5440886277c3c4_51566300($_smarty_tpl) {?> <style type="text/css">
	.login_frm_content{
		max-width:350px;
		min-width:300px;
		position:auto;
		margin:3% auto;
		max-height:250px;
	}
	.login_frm_content th{
		text-align:right;
		min-width:100px;
	}
	.login_frm_content th, td{
		padding:5px 10px;
		border:none;
	}
	.login_frm_content input{
		width:100%;
		border:1px solid #cccccc;
		border-radius:3px;
		padding:2px 5px;
		height:25px;
	}
	.login_frm_content table{
		border:none;
		margin-top:5px;
	}
	.login_status{
		font-style: italic; 
		margin: 10px 10px -10px;
		text-align: center;		
		padding: 5px 15px;
		border-radius: 5px; 
		border-color:white; 
		font-size: 9pt; 
		
		border-width: 1px; border-style: solid; 
		background: none repeat scroll 0% 0% rgb(234, 234, 234);
	}
	.login_successful{
		border-color: rgb(0, 0, 255);
		color: rgb(0, 0, 255); 
	}
	.login_error{
		border-color: #990000;
		color:red;
	}
 </style>
 <?php $_smarty_tpl->tpl_vars['root_url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['root_url'], null, 0);?>
	<?php $_smarty_tpl->tpl_vars['fld_code'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['fld_code'], null, 0);?>
<?php if ($_SESSION['user']) {?>
		<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable("logined", null, 0);?>
		Hello, <?php echo $_SESSION['user']['user_name'];?>

	<?php } else { ?>	
		<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
	<?php }?>
 <?php $_smarty_tpl->tpl_vars['template_fld'] = new Smarty_variable($_smarty_tpl->tpl_vars['page_class']->value->cur_page->page_info['template_fld'], null, 0);?>
 <div >
	<div style="" id="loading_icon" ></div>
	<div id="result_background" class="pop_up_background" style="" >
		<div class="pop_up_content login_frm_content" style="" >
			<div class="pop_up_title"><span id="popup_title">Sign in</span>
				<div class="pop_up_close_btn" >
				<a href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
" style='color:white;'>[x]</a></div>
			</div>
			<?php echo $_SESSION['user']['session_time']['year'];?>

			<?php if ($_GET['guest_login']=="1") {?>
				<div style="padding:25px 25px 10px 25px;">
					<form method="POST" id="login_frm2" style="">
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['template_fld']->value)."login_frm2.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

					</form>
				</div>
			<?php } else { ?>
				<div style="padding:25px 25px 10px 25px;">
					<div style="width:100%;text-align:center;">Member login</div>
					<form method="POST" id="login_frm">
					<?php if (($_POST['login'])) {?>
	<div class="login_status">	
		<?php $_smarty_tpl->tpl_vars['login_result'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->checkUserInfo(), null, 0);?>
		<?php if ($_smarty_tpl->tpl_vars['login_result']->value=='0') {?>
			<span class="login_error">Username and Password are not blank</span>
		<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='1') {?>
			<span class="login_error">No, Wrong Username</span>
			<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
		<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='2') {?>	
			<span class="login_successful">Login successful</span>
			<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable("logined", null, 0);?>
		<?php } elseif ($_smarty_tpl->tpl_vars['login_result']->value=='3') {?>
			<span class="login_error">No, Wrong Password</span>
			<?php $_smarty_tpl->tpl_vars['status'] = new Smarty_variable('', null, 0);?>
		<?php }?>	
	</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['status']->value=='') {?>
	<table>
		<tr>
			<th >User name</th>
			<td><input type="text" name="user_name" value="<?php echo $_POST['user_name'];?>
"></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" name="user_pass"></td>
		</tr>
		<?php $_smarty_tpl->tpl_vars['cur_time'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->config_global->global_var['cur_time'], null, 0);?>
		
		
		<tr>
			<td style="text-align:center;" colspan="2">
			<a href="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
" style="
			background: none repeat scroll 0 0 #ffffff;
    border-color: #dddddd;
    border-radius: 3px;
    border-style: solid;
    border-width: 0 2px 2px 0;
    margin-right: 20px;
    padding: 6px 10px;
			">
	<img style="width:20px;margin-top: -3px;" src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['fld_code']->value;?>
core/view/images/arrow.jpg" 
	onload="javascript:document.getElementById('ct3').innerHTML = '';">	Back home	
			</a>
			<button class="btn btn-primary btn-sm"
			type="submit" name="login" value="1">Sign me in</button></td>
		</tr>
	</table>
	
	
	
<?php }?>

					</form>
					<!-- login_frm2.html-->
				</div>
			<?php }?>
			<?php if ($_SESSION['user']) {?>
				<div style="width:100%;text-align:center;">
				<?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['this_class']->value->class_info['redirect_to']['home'], null, 0);?>
				<?php $_smarty_tpl->tpl_vars['args'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['url']->value)."||3000||redirecting in 3s...", null, 0);?>
				<?php echo $_smarty_tpl->tpl_vars['this_class']->value->redirect_to($_smarty_tpl->tpl_vars['args']->value);?>

				</div>
			<?php }?>
			</div>
		</div>
	</div>
</div>


<?php }} ?>
