<?php /* Smarty version Smarty-3.1.18, created on 2015-01-29 14:37:42
         compiled from "main\base\view\AdminLTE-master\item_details.html" */ ?>
<?php /*%%SmartyHeaderCode:623154c8ee4fb9b546-68554283%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3a3b6c17bba5ff867f7d6b5322af91a1dd29c2fe' => 
    array (
      0 => 'main\\base\\view\\AdminLTE-master\\item_details.html',
      1 => 1422516539,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '623154c8ee4fb9b546-68554283',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54c8ee4fc5d2c5_91474846',
  'variables' => 
  array (
    'updated_status' => 0,
    'details' => 0,
    'before_fields' => 0,
    'frm_content' => 0,
    'arr' => 0,
    'root_url' => 0,
    'cur_class' => 0,
    'item_id' => 0,
    'after_fields' => 0,
    'delete_button' => 0,
    'back_link' => 0,
    'attach_frm' => 0,
    'this_class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54c8ee4fc5d2c5_91474846')) {function content_54c8ee4fc5d2c5_91474846($_smarty_tpl) {?><meta charset="UTF-8">
<link href="/01/ligker/main/base/view/css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/01/ligker/main/base/view/js/script.js"></script>
<script src="/01/ligker/main/base/view/gwt/gwt_tool/war/gwt_tool/gwt_tool.nocache.js"></script>
<div id="popup_div"></div>




<?php if ($_smarty_tpl->tpl_vars['updated_status']->value==false) {?>
<div class="edit_form" style="float:left;width:100%;">
<div id="popup_window">
<?php }?>
	<div style="" class="row">
	<div class="col-md-12">
		<!-- Primary box -->
		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">Primary Solid Box</h3>
				<div class="box-tools pull-right">
					<button data-widget="collapse" class="hide_class btn btn-primary btn-sm"><i class="fa fa-minus"></i></button>
					<a href=""><button data-widget="remove" class="btn btn-primary btn-sm"><i class="fa fa-times"></i></button></a>
				</div>
			</div>
			<div class="box-body" style="display: block;background: none repeat scroll 0 0 #eef;
    float: left;width: 100%;min-height:400px;">
				<div class="col-md-6" >
					<div class="box box-primary" >
						<div class="box-header">
							<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['details']->value;?>
</h3>
						</div>
					<div class="box-body" id="cur_item_details">
					<div class="box box-solid" style="margin-bottom:0px;">
						<div class="hide_class" id="item_edit_frm"></div>
					</div>
					<?php echo $_smarty_tpl->tpl_vars['before_fields']->value;?>

					<table>	
					<?php  $_smarty_tpl->tpl_vars['arr'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['arr']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['frm_content']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['arr']->key => $_smarty_tpl->tpl_vars['arr']->value) {
$_smarty_tpl->tpl_vars['arr']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['arr']->key;
?>
					<tr class="form-group">
						<th class='details_titlte'><label ><?php echo $_smarty_tpl->tpl_vars['arr']->value['field_label'];?>
</label></th>
						<td>		
							<a href="javascript:void();" onclick="javascript:
							document.getElementById('item_edit_frm').className = 'box-body';
							gwt_result
						('gwt_frm','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=item_edit&args=<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_id'];?>
;<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['frm_type'];?>
;<?php echo $_smarty_tpl->tpl_vars['arr']->value['field_name'];?>
'
							,'popup_window','item_edit_frm','edit_frm_item')">
							<?php if ($_smarty_tpl->tpl_vars['arr']->value['cur_value']) {?>
								<?php echo $_smarty_tpl->tpl_vars['arr']->value['cur_value'];?>
 <-- click here
							<?php } else { ?>
								--- <-- click here
							<?php }?>
							</a>
						</td>
					</tr>
					<?php } ?>		
					</table>
					<?php echo $_smarty_tpl->tpl_vars['after_fields']->value;?>

					
					</div>
					<div class="box-footer" style="text-align:center;padding: 25px;">
						<a href="javascript:void();" class='my_btn' style="background:red;"
						onclick="javascript:gwt_result
					('gwt_html','<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
&controller=<?php echo $_smarty_tpl->tpl_vars['cur_class']->value;?>
&action=del_item&args=<?php echo $_smarty_tpl->tpl_vars['item_id']->value;?>
','','popup_window','')"
						><?php echo $_smarty_tpl->tpl_vars['delete_button']->value;?>
</a>
						<a href="" class='my_btn' style="color:white;"
						onclick="javascript:document.getElementById('head_form').innerHTML = '...backing...';" 	
							><?php echo $_smarty_tpl->tpl_vars['back_link']->value;?>
</a>
					</div>	
					</div>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?>
				<div class="col-md-6" >
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Attach files</h3>
						</div>
						<div class="box-body"><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
</div>
					</div>
				</div>	
				<?php }?>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div>
	
	
	
	
	
	
</div>
<?php if ($_smarty_tpl->tpl_vars['this_class']->value->updated==false) {?>
</div></div>
<?php }?>
<?php }} ?>
