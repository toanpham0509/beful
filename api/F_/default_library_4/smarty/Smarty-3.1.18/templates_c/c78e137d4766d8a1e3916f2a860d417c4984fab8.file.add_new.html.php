<?php /* Smarty version Smarty-3.1.18, created on 2015-03-02 12:34:28
         compiled from "base\view\AdminLTE-master\add_new.html" */ ?>
<?php /*%%SmartyHeaderCode:1707454cb0fa78994f5-38575627%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c78e137d4766d8a1e3916f2a860d417c4984fab8' => 
    array (
      0 => 'base\\view\\AdminLTE-master\\add_new.html',
      1 => 1425274464,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1707454cb0fa78994f5-38575627',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_54cb0fa78aa815_17276121',
  'variables' => 
  array (
    'lang_file' => 0,
    'this_view' => 0,
    'frm_content' => 0,
    'attach_frm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54cb0fa78aa815_17276121')) {function content_54cb0fa78aa815_17276121($_smarty_tpl) {?><div class="edit_form" style="float:left;width:100%;">
<div id="popup_window">
	<div style="" class="row">
	<div class="col-md-12">
		<!-- Primary box -->
		<div class="box box-solid box-primary" style="background: none repeat scroll 0 0 #eef;
		float: left;width: 100%;min-height:400px;
		">
			<div class="box-header">
				<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("add_new_frm_title",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
				<div class="box-tools pull-right">
					<button data-widget="collapse" class="hide_class btn btn-primary btn-sm"><i class="fa fa-minus"></i></button>
					<a href="" class="btn btn-primary btn-sm" style='color:white;'><i class="fa fa-times"></i></a>
				</div>
			</div>
			<div id="submit_response" class="box-body" style="display: block;
    ">
		<div class="col-md-6">
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("fields",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
</div>
		<div class="box-body" id="cur_item_details"><?php echo $_smarty_tpl->tpl_vars['frm_content']->value;?>
</div>
		<div class="box-footer" style="background:none;">
			<input type="hidden" name="add_new_btn">
			<input onclick="" type="submit" style="border:none;" value="Submit" class="my_btn">
		</div>
	</div></div>	
	
	<?php if ($_smarty_tpl->tpl_vars['attach_frm']->value) {?>
	<div class="col-md-6 " id="attach_frm">
		
		<div class="box-primary popup_content">
			
			<div class="box-header">
				<h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['this_view']->value->translator("attach_file",((string)$_smarty_tpl->tpl_vars['lang_file']->value));?>
</h3>
				<div style="float:right;margin-right: 5px;">
			<a  href="javascript:void();" onclick="javascript:
			togg('attach_frm','popup_background','hide_class')
			">x</a>
		</div>
			</div>
			<div class="box-body"><?php echo $_smarty_tpl->tpl_vars['attach_frm']->value;?>
</div>
		</div>
	</div>	
	<?php }?>

	
	</div>
	
	</div>
	</div>
	</div>
</div>
</div>

<?php }} ?>
