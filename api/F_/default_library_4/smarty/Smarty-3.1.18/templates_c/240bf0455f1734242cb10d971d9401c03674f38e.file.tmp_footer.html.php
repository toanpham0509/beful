<?php /* Smarty version Smarty-3.1.18, created on 2014-09-09 00:11:26
         compiled from "mvc4\view\supermarket\tmp_footer.html" */ ?>
<?php /*%%SmartyHeaderCode:13538540dd52e52d6c3-14149331%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '240bf0455f1734242cb10d971d9401c03674f38e' => 
    array (
      0 => 'mvc4\\view\\supermarket\\tmp_footer.html',
      1 => 1410192684,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13538540dd52e52d6c3-14149331',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'root_url' => 0,
    'template_fld' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_540dd52e67c523_20335539',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540dd52e67c523_20335539')) {function content_540dd52e67c523_20335539($_smarty_tpl) {?><div class="footer">
    <div class="f-top">
        <div class="f-top-1">
            <h4>
                Cam kết</h4>
            <ul class="ls">
                <li>Sản phẩm, hàng hóa chính hãng đa dạng phong phú. </li>
                <li>Luôn luôn giá rẻ &amp; khuyến mại không ngừng. </li>
                <li>Dịch vụ chăm sóc khách hàng tốt nhất. </li>
            </ul>
        </div>
        <div class="f-top-2">
            <h4>
                Hỗ trợ khách hàng</h4>
            <ul class="ls">
                <li><a target="_blank" title="Hướng dẫn mua online" href="http://mediamart.vn/tin-tuc/ho-tro-mua-hang/huong-dan-mua-hang-online.htm" rel="nofollow">Hướng dẫn mua online</a></li>
                <li><a target="_blank" title="Hướng dẫn mua trả góp" href="http://mediamart.vn/tin-tuc/tin-khuyen-mai/mua-hang-tra-gop-lai-suat-0-chi-co-tai-media-mart.htm" rel="nofollow">Hướng dẫn mua trả góp</a></li>
                <li><a target="_blank" title="Bảo hành, đổi trả" href="http://mediamart.vn/tin-tuc/chinh-sach-chung/chinh-sach-doi-tra-hang.htm" rel="nofollow">Bảo hành, đổi trả</a></li>
                <li><a target="_blank" title="Quy đinh, chính sách" href="http://mediamart.vn/tin-tuc/ho-tro-chinh-sach-dich-vu/" rel="nofollow">Quy đinh, chính sách</a></li>
                <li><a target="_blank" title="Thành viên vip" href="http://mediamart.vn/tin-tuc/tin-khuyen-mai/chinh-sach-uu-dai-cho-hoi-vien-cua-media-mart.htm" rel="nofollow">Chính sách hội viên</a></li>
                <li><a target="_blank" title="Chính sách giao hàng" href="http://mediamart.vn/tin-tuc/chinh-sach-chung/chinh-sach-giao-nhan.htm" rel="nofollow">Chính sách giao hàng</a></li>
            </ul>
        </div>
        <div class="f-top-3">
            <h4>
                Thông tin công ty</h4>
            <ul class="ls">
                <li><a target="_blank" title="Giới thiệu công ty" href="http://mediamart.vn/tin-tuc/thong-tin-chung/gioi-thieu-cong-ty.htm">
                    Giới thiệu công ty</a></li>
                <li><a target="_blank" title="Tuyển dụng" href="http://mediamart.vn/tin-tuc/tin-tuyen-dung-mediamart/">
                    Tuyển dụng</a></li>
            </ul>
            <div class="cl">
            </div>
            <div class="social">
                <a class="google" href="https://plus.google.com/110250954373042001082" rel="publisher" target="_blank"></a><a class="facebook" target="_blank" title="Facebook MediaMart" href="https://www.facebook.com/DienmayMediaMart"></a>
            </div>
            <div class="cl">
            </div>
        </div>
        <div class="f-top-4">
            <h4>
                Tổng đài trợ giúp</h4>
            <p>
            </p>
        </div>
        <div class="cl">
        </div>
    </div>
    <div class="f-mid">
        <div class="f-mid-1">
            <h4>
                MediaMart Hai Bà Trưng
            </h4>
            <p>
                29F Hai Bà Trưng - Hoàn Kiếm Hà Nội
            </p>
            <p>
                Điện thoại: 043 9366 366
            </p>
            <a href="http://mediamart.vn/Address?mapid=1" target="_blank" title="Bản đồ đường đi tới MediaMart Hai Bà Trưng" rel="nofollow">Bản đồ đường đi</a>
        </div>
        <div class="f-mid-2">
            <h4>
                MediaMart Trường Chinh
            </h4>
            <p>
                72 Trường Chinh – Hà nội (Cạnh cầu vượt Ngã Tư Vọng)
            </p>
            <p>
                Điện thoại: 04 6285 9666
            </p>
            <a href="http://mediamart.vn/Address?mapid=2" target="_blank" title="Bản đồ đường đi tới MediaMart Trường Chinh" rel="nofollow">Bản đồ đường đi</a>
        </div>
        <div class="f-mid-3">
            <h4>
                MediaMart Thanh Xuân
            </h4>
            <p>
                Tòa nhà Sông Đà - KM10 Nguyễn Trãi - Thanh Xuân (Cạnh Đại học Kiến trúc)
            </p>
            <p>
                Điện thoại: 04 6251 8866
            </p>
            <a href="http://mediamart.vn/Address?mapid=3" target="_blank" title="Bản đồ đường đi tới MediaMart Thanh Xuân" rel="nofollow">Bản đồ đường đi</a>
        </div>
        <div class="f-mid-4">
            <h4>
                MediaMart Long Biên
            </h4>
            <p>
                Số 3 Nguyễn Văn Linh – Long Biên (Cạnh BigC Long Biên)
            </p>
            <p>
                Điện thoại: 043 7753 888
            </p>
            <a href="http://mediamart.vn/Address?mapid=4" target="_blank" title="Bản đồ đường đi tới MediaMart Long Biên" rel="nofollow">Bản đồ đường đi</a>
        </div>
        <div class="f-mid-5">
            <h4>
                MediaMart Mỹ Đình
            </h4>
            <p>
                Tòa nhà B.I.G TOWER, Số 18 Phạm Hùng (Cách cầu vượt Mai Dịch 100m)
            </p>
            <p>
                Điện thoại: 04 3795 5656
            </p>
            <a href="http://mediamart.vn/Address?mapid=5" target="_blank" title="Bản đồ đường đi tới MediaMart Mỹ Đình" rel="nofollow">Bản đồ đường đi</a>
        </div>
        <div class="cl">
        </div>
        <div class="f-mid-1">
            <div class="f-mid-2-1">
                <h4>
                    MediaMart Hải Phòng
                </h4>
                <p>
                    10 Lê Hồng Phong - Ngô Quyền
                </p>
                <p>
                    Điện thoại: 0313 72 6666
                </p>
                <a href="http://mediamart.vn/Address?mapid=7" target="_blank" title="Bản đồ đường đi tới MediaMart Hải Phòng" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-2">
            <div class="f-mid-2-2">
                <h4>
                    MediaMart Nguyễn Chí Thanh
                </h4>
                <p>
                    18 Nguyễn Chí Thanh - Ba Đình
                </p>
                <p>
                    Điện thoại: 04 3724 7788
                </p>
                <a href="http://mediamart.vn/Address?mapid=6" target="_blank" title="Bản đồ đường đi tới MediaMart Nguyễn Chí Thanh" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-3">
            <div class="f-mid-2-3">
                <h4>
                    MediaMart Thái Nguyên
                </h4>
                <p>
                    Số 3 đường Bắc Cạn
                </p>
                <p>
                    Điện thoại: 0280 3757 686
                </p>
                <a href="http://mediamart.vn/Address?mapid=8" target="_blank" title="Bản đồ đường đi tới MediaMart Thái Nguyên" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-4">
            <div class="f-mid-2-4">
                <h4>
                    MediaMart Bắc Ninh
                </h4>
                <p>
                    Số 37 Đường Lý Thái Tổ
                </p>
                <p>
                    Điện thoại: 0241 3828868
                </p>
                <a href="http://mediamart.vn/Address?mapid=9" target="_blank" title="Bản đồ đường đi tới MediaMart Bắc Ninh" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-5">
            <div class="f-mid-2-5">
                <h4>
                    MediaMart HEAD OFFICE
                </h4>
                <p>
                    29F Hai Bà Trưng - Hoàn Kiếm
                </p>
                <p>
                    Điện thoại: 04 6262 8888
                </p>
                <a href="http://mediamart.vn/Address?mapid=1" target="_blank" title="Bản đồ đường đi tới MediaMart HEAD OFFICE" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="cl">
        </div>
        <div class="f-mid-1">
            <div class="f-mid-3-1">
                <h4>
                    MEDIAMART HƯNG YÊN
                </h4>
                <p>
                    Số 374 Nguyễn Văn Linh - Phường An Tảo – TP. Hưng Yên - Tỉnh Hưng Yên
                </p>
                <p>
                    Điện thoại: 0321 361 6666
                </p>
                <a href="http://mediamart.vn/Address?mapid=12" target="_blank" title="Bản đồ đường đi tới MediaMart Hưng Yên" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-2">
            <div class="f-mid-3-2">
                <h4>
                    MEDIAMART PHẠM VĂN ĐỒNG
                </h4>
                <p>
                    Số 166 Đường Phạm Văn Đồng, Xuân Đỉnh, Quận Từ Liêm, TP Hà Nội
                </p>
                <p>
                    Điện thoại: 04.3757 0570
                </p>
                <a href="http://mediamart.vn/Address?mapid=11" target="_blank" title="Bản đồ đường đi tới MediaMart Phạm Văn Đồng" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-3">
            <div class="f-mid-3-3">
                <h4>
                    MEDIAMART HỒ TÙNG MẬU
                </h4>
                <p>
                    111 Hồ Tùng Mậu, Quận Cầu Giấy, TP Hà Nội
                </p>
                <p>
                    Điện thoại: 04 3763 2222
                </p>
                <a href="http://mediamart.vn/Address?mapid=15" target="_blank" title="Bản đồ đường đi tới MediaMart Hồ Tùng Mậu" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-4">
            <div class="f-mid-3-4">
                <h4>
                    MEDIAMART VĨNH YÊN
                </h4>
                <p>
                    Số 635 Mê Linh - TP Vĩnh Yên(Cạnh bến xe Vĩnh Yên)
                </p>
                <p>
                    Điện thoại: 0211 353 8888
                </p>
                <a href="http://mediamart.vn/Address?mapid=14" target="_blank" title="Bản đồ đường đi tới MediaMart Vĩnh Phúc" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="f-mid-5">
            <div class="f-mid-3-5">
                <h4>
                    MEDIAMART THANH HÓA
                </h4>
                <p>
                    Số 301 Trần Phú - Phường Ba Đình - TP Thanh Hóa - Tỉnh Thanh Hóa
                </p>
                <p>
                    Điện thoại: 0373 68 6666
                </p>
                <a href="http://mediamart.vn/Address?mapid=13" target="_blank" title="Bản đồ đường đi tới MediaMart Thanh Hóa" rel="nofollow">Bản đồ đường đi</a>
            </div>
        </div>
        <div class="cl">
        </div>
    </div>
    <div class="f-bot">
        <p>
            @ 2007 Công ty Cổ phần MEDIAMART Việt Nam - ĐCĐK: 29F Hai Bà Trưng. GPĐKKD số:
            0102516308 do Sở KHĐT Tp.Hà Nội cấp ngày 15/11/2007.
        </p>
        <p>
            Email: hotro@mediamart.com.vn. Điện thoại: 04 6262 8888. Fax: 043 933 0766
        </p>
        <p>
            <a href="http://mediamart.vn/" rel="nofollow">mediamart.vn</a> / <a href="http://thegioidienmay.com" rel="nofollow">thegioidienmay.com</a>
        </p>
    </div>
    <div class="cl">
    </div>
</div>
  
    <div id="highstreet" class="highstreet" style="height: 0px;">
        <div class="highstreet-inner">
            <p class="btn-highstreet">
                <a href="javascript:;" rel="false"><span></span></a>
            </p>
            <div class="cl">
            </div>
            <div class="highsteet-gallery">
                <div class="highsteet-gallery-inner">
                    <ul class="ls banner-getcategory">
                            <li><a href="http://mediamart.vn/khuyen-mai-tuan" title="Khai trương Giá sốc Cơn lốc quà tặng" target="_blank">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/756dad21-51b2-4450-ac1e-0500074f3399_khai-truong-gia-soc-con-loc-qua-tang.jpg" alt="Khai trương Giá sốc Cơn lốc quà tặng">
                            </a></li>
                            <li><a href="http://mediamart.vn/tin-tuc/tin-khuyen-mai/uu-dai-chua-tung-co-cho-chu-the-bankplus-mastercard-tai-media-mart.htm" title="Cơ hội trúng 58 iPhone 5S, 09 Smart TV 3D 55 inch" target="_blank">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/746d57be-4a13-4885-8696-f50212d944ed_co-hoi-trung-58-iphone-5s-09-smart-tv-3d-55-inch.jpg" alt="Cơ hội trúng 58 iPhone 5S, 09 Smart TV 3D 55 inch">
                            </a></li>
                            <li><a href="http://mediamart.vn/khuyen-mai-tuan" title="Sở hữu Laptop HP chỉ từ 1.3 triệu đồng" target="_blank">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/2fc22dc4-de5c-4b13-a2d8-7f115d84500e_so-huu-laptop-hp-chi-tu-13-trieu-dong.jpg" alt="Sở hữu Laptop HP chỉ từ 1.3 triệu đồng">
                            </a></li>
                            <li><a href="http://tragop.mediamart.vn/" title="Ưu đãi Trả góp 0% lãi suất" target="_blank">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/0899a8cb-8b19-4c51-8bee-7bfe881b53d8_uu-dai-tra-gop-0-lai-suat.jpg" alt="Ưu đãi Trả góp 0% lãi suất">
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="cl">
    </div>

    <div class="g-ts">
            <div class="g-ts-r">
                <a href="http://mediamart.vn/tin-tuc/tin-khuyen-mai/1000-laptop-gia-soc-giam-tiep-5-tang-them-voucher-1-trieu.htm" target="_blank">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/b533e944-0df4-44c1-a9df-cf6947106736_laptop-giam-gia.jpg" alt="">
                </a>
            </div>
            <div class="g-ts-l">
                <a href="http://mediamart.vn/tin-tuc/tin-khuyen-mai/uu-dai-chua-tung-co-cho-chu-the-bankplus-mastercard-tai-media-mart.htm" target="_blank">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/9dc0fbf8-55f1-462d-8ec7-b69edbfc9d92_right-mob.jpg" alt="">
                </a>
            </div>
    </div>

<div class="cl">
</div>
<script type="text/javascript"> window._sbzq || function (e) { e._sbzq = []; var t = e._sbzq; t.push(["_setAccount", 6988]); var n = e.location.protocol == "https:" ? "https:" : "http:"; var r = document.createElement("script"); r.type = "text/javascript"; r.async = true; r.src = n + "//static.subiz.com/public/js/loader.js"; var i = document.getElementsByTagName("script")[0]; i.parentNode.insertBefore(r, i) } (window);</script>

    <div class="cl">
    </div>
            <div class="box-promotion">
                
                    <div class="box-promotion-item" style="left: 381.5px; top: 187.5px; width: 500px; height: 300px;">
                        <div class="box-banner">
                            <a href="/khuyen-mai-tuan" target="_blank" title="Khuyến mại tuần">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['root_url']->value;?>
<?php echo $_smarty_tpl->tpl_vars['template_fld']->value;?>
/image/8f921add-3bf7-4bed-8c9b-62ec0f183aa2_khuyen-mai-tuan.jpg" alt="Khuyến mại tuần">
                            </a>
                        </div>
                        <a class="box-promotion-close" href="javascript:;" title="Đóng lại"></a>
                    </div>
                
            </div>
    <div class="cl">
    </div>
    
    
<script src="http://lib.mediamart.vn/Scripts/Plugin/LazyLoad/lazyload.js" type="text/javascript"></script>
<script src="http://lib.mediamart.vn/Scripts/Core/Web/lib.js" type="text/javascript"></script>
<!-- Add UI -->
<link href="http://lib.mediamart.vn/Content/themes/ui/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="http://lib.mediamart.vn/Scripts/jquery-ui.min.js" type="text/javascript"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="http://lib.mediamart.vn/Scripts/Plugin/FancyBox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
<script type="text/javascript" src="http://lib.mediamart.vn/Scripts/Plugin/FancyBox/jquery.fancybox.pack.js?v=2.1.5"></script>
<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="http://lib.mediamart.vn/Scripts/Plugin/FancyBox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen">
<script type="text/javascript" src="http://lib.mediamart.vn/Scripts/Plugin/FancyBox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("img.lazy").lazyload({ threshold: 1500 });

        $('.fancybox').fancybox();

        $(".fancybox-thumb").fancybox({
            prevEffect: 'none',
            nextEffect: 'none',
            helpers: {
                title: {
                    type: 'outside'
                },
                thumbs: {
                    width: 76,
                    height: 61
                }
            }
        });
    });
</script>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-42719228-1', 'mediamart.vn');
    ga('send', 'pageview');

</script>







<div class="sbzoff" style="display: block; margin-top: 0px; margin-right: 0px;
 margin-bottom: 0px; padding: 0px; border: 0px; overflow: hidden; position: fixed; 
 z-index: 16000001; bottom: 0px; width: 241px; height: 33px; right: 10px;
 background-color: rgba(0, 0, 0, 0);">
 <iframe id="sbzoff_frame" frameborder="0" style="background-color: transparent; 
 vertical-align: text-bottom; position: relative; width: 100%; height: 100%; 
 margin: 0px; overflow: hidden;" src="javascript:false"></iframe></div>
 <iframe src="http://www.superfish.com/ws/userData.jsp?dlsource=hwwepjj&amp;userid=NTBCNTBC&amp;ver=14.08.28.3" 
 style="position: absolute; top: -100px; left: -100px; z-index: -10; border: none; visibility: hidden; width: 1px; height: 1px;">
 </iframe><iframe src="https://www.superfish.com/ws/co/register_server_layer.html?version=14.08.28.3" 
 style="position: absolute; width: 1px; height: 1px; left: -100px; top: -100px; visibility: hidden;">
 </iframe><iframe style="position: absolute; width: 1px; height: 1px; top: 0px; left: 0px; visibility: hidden;"></iframe><?php }} ?>
