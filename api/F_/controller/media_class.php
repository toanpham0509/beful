<?php		
	Class media_class{
		public function __construct() {
			
		}
		
		function upload_file($k,$media_url = null,$upload_method=null){					
			$fileName 	= $_FILES["file"]["name"][$k]; // The file name 
			$fileTmpLoc = $_FILES["file"]["tmp_name"][$k]; // File in the PHP tmp folder 
			$fileType = $_FILES["file"]["type"][$k]; // The type of file it is 
			$fileSize = $_FILES["file"]["size"][$k]; // File size in bytes 
			$fileErrorMsg = $_FILES["file"]["error"][$k]; // 0 for false... and 1 for true 
			global $config_dir;
			if (!$fileTmpLoc) { // if file not chosen 
				//$result = "ERROR: Please browse for a file before clicking the upload button.";
			}
			else{
				$fileName = $this->get_file_name($fileName,$k);
				if($media_url==null){
					$media_folder	= $this->create_folder();
					$path_file		= $config_dir["application_folder"].$media_folder;				
					$media_url		= $path_file.$fileName;
				}
				$fileType = explode('/',$fileType);
				if($upload_method != null){
					$upload_method($fileTmpLoc, $media_url );
				}elseif($fileType[0] == 'image'){
					include($config_dir["core_folder"]."controller/image_class.php") ;
					$this->image = new image_class;
					
					$this->image->load($fileTmpLoc);
					$this->image->resizeToWidth(300);
					$this->image->save($media_url);
					
					/* if want to create a thumb image
					$thumb_name = 	"thumb_".$fileName;
					$this->image->resizeToWidth(200);
					$this->image->save($path_file.$thumb_name);
					*/
				}else{
					move_uploaded_file($fileTmpLoc, $media_url );	
				}
			}
			return	$media_folder.$fileName;
		}
		
		function file_ext($fileName){
			$file_exp = explode('.',$fileName);
			$file_ext	= strtolower(end($file_exp));
			return $file_ext;
		}
		
		function get_file_name($fileName,$k){
			$file_ext	= $this->file_ext($fileName);
			$file_name 	= str_replace(".$file_ext","",$fileName);
		//	$file_name	= $file_name."(".time().")";
			if($k > 0){
				$fileName	= time()."(".$k.").".$file_ext;
			}else{
				$fileName	= time().".".$file_ext;
			}
			
			return $fileName;
		}
		
		function create_folder(){
			global $config_dir;
		//	return $config_dir["application_folder"];
			$fld_year = date('Y',time());		
			if(is_dir($config_dir["application_folder"]."uploads/".$fld_year)==""){
				mkdir($config_dir["application_folder"]."uploads/".$fld_year);
				chmod($config_dir["application_folder"]."uploads/".$fld_year,0777);
			}
			$fld_month = date('m',time());
				if(is_dir($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month)==""){
					mkdir($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month);
					chmod($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month,0777);
				}
			$fld_day = date('d',time());
				if(is_dir($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month."/".$fld_day)==""){			
					mkdir($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month."/".$fld_day);
					chmod($config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month."/".$fld_day,0777);
				}
			return "uploads/".$fld_year."/".$fld_month."/".$fld_day."/";	
		//	return $config_dir["application_folder"]."uploads/".$fld_year."/".$fld_month."/".$fld_day."/";	
		//	return  $fld_year."/".$fld_month."/".$fld_day;
		}
	}