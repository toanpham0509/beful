<?php
	Class Core_Page_Controller {
		public function __construct() {		
			global $config_dir;
			global $config_get_page;
			$this->cur_page 		= 	$config_get_page ;
			$this->config_dir	= $config_dir;
			$this->default_library = "/".$config_dir["default_library"];
			$this->default_template_folder["dir"] = $config_dir["default_library"]."AdminLTE-master/";
			$this->default_template_folder["dir_document_root"]
				= $config_dir["document_root"].$this->default_template_folder["dir"];
			
			$this->template_folder	=	$this->default_template_folder["dir_document_root"];
			$this->template_file	=	"gwt.html"	;
			$this->page_title_1		=	"page_title_1"		;	
			$this->page_title_2		=	"page_title_2"		;
			$this->page_title_3		=	"Dashboard"	;
			$this->page_title_4		=	"Admin"		;		
		}
		function display_page(){
			global $smarty;
			$smarty->assign('page_class',$this);
			$smarty->assign('position',$this->position);
			//echo $this->template_folder.$this->template_file;
			$result = $smarty->fetch($this->template_folder.$this->template_file);
			return $result;
		}
		function page_not_found(){
			return "this page not found ";
		}		
	}