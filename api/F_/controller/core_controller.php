<?php
	Class Core_Controller{
		public function __construct() {
			$this->item_list	= array(
				"view_method"	=> "item_list"	,
				"model_method"	=> "item_list"
			);
			$this->item_details	=array(
				"view_method"	=> "item_details"	,
				"model_method"	=> "item_details"
			);
			$this->item_add_new["view_method"]= "item_add_new";
			
			$this->item_update	=array(
				"view_method"	=> array(
					"update_successful"	=> "update_successful"	,
					"add_new_successful"	=> "add_new_successful"
				)	,
				"model_method"	=> array(
					"update"	=> "item_update"	,
					"add_new"	=> "item_add_new"
				)
			);
			$this->item_delete	=array(
				"view_method"	=> "item_delete_successful"	,
				"model_method"	=> "item_delete"
			);
		}
		
		function item_list($args){
			$item_methods	= $this->item_list;						
			$data	= $this->this_model->$item_methods["model_method"]($args);
			return $this->this_view->$item_methods["view_method"]($data);
		}
		
		function item_add_new(){
			$this->item_details["view_method"]	= $this->item_add_new["view_method"];
			return $this->item_details();
		}
		
		function item_details($item_id){
			$item_methods	= $this->item_details;	
			$data	= $this->this_model->$item_methods["model_method"]($item_id);
			$data	= $this->add_drop_down_list($data);
			return $this->this_view->$item_methods["view_method"]($data);
		}
		
		function item_update($item_id){
			$item_methods	= $this->item_update;
			if($item_id){
				$data	= $this->this_model->$item_methods["model_method"]["update"]($item_id,$_POST["item_field"]);
				if($data == "0") 
					return $this->this_view->$item_methods["view_method"]["update_successful"]();
				else
					return $data;
			}else{
				$data	= $this->this_model->$item_methods["model_method"]["add_new"]($_POST["item_field"]);
				if(is_numeric($data)){
					return $this->this_view->$item_methods["view_method"]["add_new_successful"]();
				}else{
					return $data;
				}
			}
		}
		
		function item_delete($item_id){
			$item_methods	= $this->item_delete;
			$data	= $this->this_model->$item_methods["model_method"]($item_id);
			if($data == "0"){
				return $this->this_view->$item_methods["view_method"]();
			}else{
				return $data;
			}
		}
		
		function add_drop_down_list($data){			
			foreach($data["fields"] as $k=>$v) {
				if($v["REFERENCED_TABLE_NAME"]){
					$column_name	= $v["column_name"];
					$table_name	= $v["REFERENCED_TABLE_NAME"];
					if($data["item_list"][0][$column_name])
						$_GET["current_value"] = $data["item_list"][0][$column_name];
					else
						$_GET["current_value"]	= null;	
					$data["item_list"][0]["drop_down_list"][$column_name]
					= $this->get_drop_down_list($table_name);
				}
			}
			return $data;
		}
		
		function get_drop_down_list($table_name){
			global $config_dir;
			
			$temp_get	= $_GET;
			
			$_GET["controller"]	= $this->get_drop_down_list_class($table_name);			
			$_GET["action"]		= "drop_down_list";
			$_GET["args"]		= $_GET["current_value"];
			include	($config_dir["core_folder"]."main.php");			
			
			$_GET		= $temp_get;
			
			return $main;
		}
		
		function get_drop_down_list_class($table_name){
			$class_name	= $table_name;
			return $class_name;
		}
		
		function drop_down_list($current_value){
			$data	= $this->this_model->drop_down_list($current_value);
			$data["current_value"]	= $current_value;
			$data["field_name"]		= $_GET["field_name"];
			return $this->this_view->drop_down_list($data);
		}
		
		

	}
