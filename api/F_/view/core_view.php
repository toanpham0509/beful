<?php
	Class Core_View{
		public function __construct() {		
			global $smarty;
			$this->smarty= $smarty;
			$this->smarty->assign('this_view',$this);
			global $config_lang;
			$this->config_lang	= $config_lang; 
			global $config_dir;
			$this->config_dir	= $config_dir;
			$this->item_list_view	=  array(
					"view_file"		=> $config_dir["default_template_folder"]["dir_document_root"]."table.html"			,
					"column_titles" => $config_dir["default_template_folder"]["dir_document_root"]."column_titles.html"			,
					"item_pages"	=> $config_dir["default_template_folder"]["dir_document_root"]."gwt_table_pages.html"	,
					
					"table_title"	=> "core_view_table_title"	,
					"add_new_button"	=> true	,
					"edit_button"		=> true	,
					"delete_button"		=> true ,
					"not_show_list"		=> array('')	
			);
			$this->item_details_view	= array(
				"view_file"		=>	$config_dir["default_template_folder"]["dir_document_root"]."item_details.html"
			);
			$this->item_add_new_view	= array(
				"view_file"		=>	$config_dir["default_template_folder"]["dir_document_root"]."item_add_new.html"
			);
			$this->drop_down_list		= array(
				"view_file"		=> $config_dir["default_template_folder"]["dir_document_root"]."drop_down_list.html"
			);
			$this->notice["update_successful"]["view_file"]	= $config_dir["default_template_folder"]["dir_document_root"]."notice_update_successful.html";
			$this->notice["add_new_successful"]["view_file"]	= $config_dir["default_template_folder"]["dir_document_root"]."notice_update_successful.html";
		//	$this->front_end	= $application_folder."library/template/front_end_test/";
		//	$this->back_end		= $application_folder."library/template/Aquarius_responsive_admin_panel/";
			$this->core_folder	= $config_dir["core_folder"];			
			$this->default_library	= $config_dir["default_library"];	
			$this->default_template_folder	= $config_dir["default_template_folder"];
			//$this->lang	= $lang;
			$this->root_url		= $config_dir["root_url"];
			$this->front_end	= $config_dir["front_end"];
		}
		
		function item_list($info){
			$data	= $info;
			foreach($data["item_list"] as $k=>$v){
				$data["item_list"][$k]["create_time"] = $this->date_time_style($data["item_list"][$k]["create_time"]);
				$data["item_list"][$k]["last_update"] = $this->date_time_style($data["item_list"][$k]["last_update"]);
			}
			$data["table_title"]	= $this->item_list_view["table_title"];
			$this->smarty->assign('data',$data);
			$item_list				= $this->item_list_view;
			$item_list["fields"]	= $info["fields"]	;
			foreach($item_list["fields"] as $k=>$v) {
				$column_name	= $v["column_name"];
				if(isset($item_list["fields"][$column_name])){
					$item_list["fields"][$k]["column_name"]	= $item_list["fields"][$column_name]["column_name"];
					unset($item_list["fields"][$column_name]);
				}
			}
			$item_list["primary_key"]	= $info["primary_key"]	;
			$item_list["root_url"]	= $this->root_url;
			$item_list["cur_class"]	= $_GET["controller"];
			$this->smarty->assign('item_list',$item_list);
			$column_titles	= $this->smarty->fetch($item_list["column_titles"]);
			$this->smarty->assign('column_titles',$column_titles);
			
			return $this->smarty->fetch($this->item_list_view["view_file"]);
		}
		
		function item_add_new($info){
			$this->item_details_view["view_file"]	= $this->item_add_new_view["view_file"];
			return $this->item_details($info);
		}
		
		function item_details($info){
			$this->item_list_view["view_file"] = $this->item_details_view["view_file"];
			foreach($info["fields"] as $k=>$v) {
				$column_name	= $v["column_name"];
				if($this->form_field[$column_name]["view_file"] != null){
					$info["fields"][$k]["view_file"]	= $this->form_field[$column_name]["view_file"];
				}
			}
			return $this->item_list($info);
		}
		
		function translator($word_id){
			$lang	= $this->config_lang;
			if($_GET["translator_unset"] == $lang){
				unset($_SESSION["lang"][$lang]);
			}
			if(!isset($_SESSION["lang"][$lang])){
				global $config_dir;			
				$lang_url = $config_dir["document_root"].$config_dir["default_library"]."lang/".$lang.".xml";
				$this->get_lang($lang,$lang_url);
				
				$lang_url = $config_dir["application_folder"]."library/lang/".$lang.".xml";
				$this->get_lang($lang,$lang_url);
			}
			if($_SESSION["lang"][$lang][$word_id] == null){
				$result	= $word_id;
			}else{
				$result	= $_SESSION["lang"][$lang][$word_id];
			}
			return $result;
		}
		
		function date_time_style($unix_timestamp){
			return date('Y-m-d H:i:s',$unix_timestamp);
		}
		
		function get_lang($lang,$lang_url){
			$xml = simplexml_load_file($lang_url) or die("Error: Cannot create object");
			foreach($xml->children() as $data) {
				$id		= (string)$data->id;
				$value	= (string)$data->value;
				$_SESSION["lang"][$lang][$id]	= $value;
			}
		}
		
		function drop_down_list($info){
			$field_0	= $info["fields"][0]["column_name"];
			$field_1	= $info["fields"][1]["column_name"];
			foreach($info["item_list"] as $k=>$v) {
				$info["drop_down_list"][$k]	= array(
					"value"		=>	$v[$field_0]	,
					"show"		=>	$v[$field_1]	
				);
			}
			$this->smarty->assign('info',$info);
			return $this->smarty->fetch($this->drop_down_list["view_file"]);
		}
		
		function add_new_successful(){
			$this->smarty->assign('update_result','add_new_successful');
			return $this->smarty->fetch($this->notice["add_new_successful"]["view_file"]);
		}
		
		function update_successful(){
			$this->smarty->assign('update_result','update_successful');
			return $this->smarty->fetch($this->notice["update_successful"]["view_file"]);
		}
		
		function item_delete_successful(){
			$this->smarty->assign('update_result','delete_successful');
			return $this->smarty->fetch($this->notice["update_successful"]["view_file"]);
		}
	}
	