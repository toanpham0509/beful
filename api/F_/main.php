<?php
ob_start();
//	session_save_path("url//////////tmp");		
	$debug_mode = false;
	if ($debug_mode) error_reporting(E_ALL);
	else error_reporting(E_ALL&~E_NOTICE&~E_WARNING);
	session_start();	
	//include($config_dir["document_root"]."test2.php");
	
	if (!class_exists('Smarty'))
		include($config_dir["document_root"].$config_dir["default_library"]."smarty/include_smarty.php");
	
	require_once ("model/core_model.php");
	require_once ("view/core_view.php");
	require_once ("controller/core_controller.php");
	
	if (!class_exists('base_model'))
		include ($config_dir["application_folder"]."model/base_model.php");
	if (!class_exists('base_view'))	
		include ($config_dir["application_folder"]."view/base_view.php");
	if (!class_exists('base_controller'))
		include ($config_dir["application_folder"]."controller/base_controller.php");
	
	if(isset($_FILES["file1"]["name"]) and $_FILES["file1"]["name"] !=null){
		$_GET["controller"] = "media";
		$_GET["action"]		= "ajax_upload";
	}
	if(	
		(isset($_GET["controller"]) and isset($_GET["action"]))	or
		(isset($_POST["controller"]) and isset($_POST["action"]))	
	){
		foreach ($_POST as $k=>$v)			$_GET[$k] = $v;
		$controller_call	= $_GET["controller"];
		$action_call		= $_GET["action"];		
		if(isset($_GET["args"]))
			$args = $_GET["args"];
		else
			$args = "";
		if (!class_exists($controller_call."_model"))	
			include ($config_dir["application_folder"]."model/".$controller_call."_model.php");
		if (!class_exists($controller_call."_view"))
			include ($config_dir["application_folder"]."view/".$controller_call."_view.php");
		if (!class_exists($controller_call."_controller"))
			include ($config_dir["application_folder"]."controller/".$controller_call."_controller.php");
		$cur_controller	=	$controller_call."_controller";
	}else{
		include ($config_dir["application_folder"]."page/base_page.php");
	//	$base_controller	= new Base_Controller;
		$get_page	= $config_get_page;
		$controller_call	= $get_page[0];
		include ($config_dir["application_folder"]."page/".$controller_call."_page.php");
		$action_call		= "display_page";
		$cur_controller	=	$controller_call."_page";
	}
	
	if (!class_exists($cur_controller)){
		$page_controller	= new Page_Controller;
		$main	= $page_controller->page_not_found();
	}else{
		$cur_class		= 	new $cur_controller($controller_call);		
	//	require_once("cache.php");
		$main	= $cur_class->$action_call($args);
	}
	
//	echo $main;