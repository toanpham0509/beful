<?php
	class Core_Model{
		function __construct() {		
			global $config_db;
			$this->config_db	= $config_db;
			$this->db_info		= $this->config_db["db_info"];
			
			$this->item_list_model	= array(
				"labels"			=> $this->config_db["item_list"]["labels"]	,
				"not_show_list"		=> $this->config_db["item_list"]["not_show_list"]	,
				"table_name"		=> $this->get_table()	,
				"filter"			=> " WHERE 1=1 "	,
			//	"sort"				=> " ORDER BY create_time DESC "	,
				"num_rows_per_page" => 10		,
				"current_page"		=> $this->config_db["current_page"]	
			);
			$this->item_details_model	= array(
				"do_not_show_details"			=> array(
					"list"		=> array('','create_time','last_update')
				)
			);
		//	$this->get_fields	= $this->get_fields();
		//	$this->table_properties	= $this->table_properties();
		}
		protected function connection($db_info){	
			if($db_info["db_port"] == null)
				$db_info["db_port"] = "3306";
				$this->connection = mysqli_connect (
				$db_info["hostname"], 
				$db_info["db_user"], 
				$db_info["db_pass"] , 
				$db_info["db_name"]	,
				$db_info["db_port"]
			);
			if (!$this->connection) {
				$this->flagConnect = false;
				die ( 'Could not connect...: ' . mysqli_error($this->connection) );
			} else {				
				mysqli_select_db($this->connection , $db_info["db_name"] );
				mysqli_query($this->connection, "SET NAMES 'UTF8'");
				$this->flagConnect = true;
			}
		}
		function num_rows($num_items_query,$db_info){
			$this->connection($db_info);	
			if ($this->flagConnect == true) {
				$num_items_query	= str_replace('<br>',' ',$num_items_query);
				$num_items_sql = mysqli_query($this->connection,$num_items_query);
				if($this->result = mysqli_num_rows($num_items_sql)){
					
				}else{
					$this->result = mysqli_error($this->connection);
				}
				$this->__destruct();
				return  $this->result;				
			}			
		}
		protected function sql_runing($sql,$db_info){
			$this->connection($db_info);
			if ($this->flagConnect == true) {
				if(mysqli_query($this->connection,$sql))
					$respon=  mysqli_insert_id($this->connection) ;
				else
					$respon=  mysqli_error($this->connection);
				$this->result= $respon;
				$this->__destruct();
				return $this->result;				
			}
		}
		
		protected function fetchAll($sql,$fields,$db_info){
			$this->connection($db_info);
			$sql = str_replace('<br>',' ',$sql);
			if ($this->flagConnect == true) {
				$query = mysqli_query($this->connection,$sql);
				$row_id=0;
				while($row  = mysqli_fetch_assoc($query)){
					foreach($fields as $field){
						if(isset($row[$field])){
							$result_text = str_replace("\n","<br>",$row[$field]);
							$result_text = stripslashes($result_text);
							$items_list[$row_id][$field]= $result_text;
						}	
						else
							$items_list[$row_id][$field]= "";
					}
					$row_id++;
				}
				$this->result = $items_list;
				$this->__destruct();
				return $this->result;
			}
		}
		
		function get_cols($tbl,$db_info){
			$this->connection($db_info);
			$result = mysqli_query($this->connection,"SHOW COLUMNS FROM ".$tbl);
			if (!$result) {
				echo 'Could not run query: ' . mysqli_error($this->connection);
				exit;
			}
			$fields = array();
			if (mysqli_num_rows($result) > 0) {
				while ($row = mysqli_fetch_assoc($result)) {
					$col[] = array(
							'col_key' => $row["Key"]
						,	'col_field' => $row["Field"]
						)
					;
				}
				$this->__destruct();
				return $col;
			}
		}
		
		function __destruct() {
			if ($this->flagConnect == true) {
				mysqli_free_result($this->result);
				mysqli_close ( $this->connection );
			}
		}
		
		function item_list(){
			foreach($this->fields as $k=>$v) {
				if(array_search($v["column_name"],$this->item_list_model["not_show_list"])) {
					unset($this->fields[$k]);						
				}else{
					$this->item_list_model["fields"][]	= $v["column_name"];
				}
				
			}
			$this->item_list_model["select"]		= $this->get_select();
			$this->table_properties	= $this->table_properties();
			$sql	= $this->table_properties["item_list_sql"];
			$number_rows_pages	= $this->number_rows_pages(
				$sql	,
				$this->item_list_model["num_rows_per_page"]				
			);
			$limit_from = ($this->item_list_model["current_page"] - 1)*$this->item_list_model["num_rows_per_page"];
			$sql = $sql." LIMIT ".$limit_from.",".$this->item_list_model["num_rows_per_page"];
			$data["item_list"]		= $this->fetchAll($sql,$this->item_list_model["fields"],$this->db_info);	
			$data["num_rows_total"]	= $number_rows_pages["num_rows_total"];
			$data["num_pages"]		= $number_rows_pages["num_pages"];
			$data["num_rows_show"]	= count($data["item_list"]);
			$data["primary_key"]	= $this->table_properties["primary_key"];
			$data["fields"]	= $this->fields;
			return $data;
		}
		
		function item_details($item_id){
			$this->item_list_model["not_show_list"]	
				= $this->item_details_model["do_not_show_details"]["list"];
			$this->item_list_model["filter"] = $this->item_list_model["filter"]." AND ".
				$this->item_list_model["table_name"]
				.".".$this->item_list_model["primary_key"]
				." = '".$item_id."'" ;
			$this->table_properties	= $this->table_properties();
			return $this->item_list();
		}
		
		function drop_down_list($current_value){
		$this->item_list_model["num_rows_per_page"] = 1000;
		/*
		foreach($this->fields as $k=>$v){
			if($k>1) unset($this->fields[$k]);
		}
		*/
		/*
		$this->fields	= null;
		$this->fields[0]	= array(
			"COLUMN_KEY"	=> "PRI"		,
			"table_name"	=> "company"	,
			"column_name"	=> "company_id"	,
			"COLUMN_COMMENT"=> ""			, 	
			"constraint_schema"	=> "hoang_card"	,
			"CONSTRAINT_NAME"	=> "PRIMARY"	,
			"REFERENCED_TABLE_NAME"	=> ""		,
			"REFERENCED_COLUMN_NAME"	=>""	,
			"label"						=> "company_id"
		);
		
		$this->fields[1]	= array(
			"COLUMN_KEY"	=> "UNI"	,
			"table_name"	=> "company"	,
			"column_name"	=> "company_email"	,
			"COLUMN_COMMENT"=>	""				,
			"constraint_schema"	=> "hoang_card"	,
			"CONSTRAINT_NAME"	=> "company_email"	,
			"REFERENCED_TABLE_NAME"		=> ""		,
			"REFERENCED_COLUMN_NAME"	=> ""		,
			"label"						=> "company_email"
		);
		*/
		
		/*
		echo "<div style='margin-left:300px;'>";
		foreach($this->fields as $k=>$v) echo $k.": ".$v["column_name"]."<br />";		
		echo "</div>";
		*/
		return $this->item_list();
	//	return $current_value;
		
	}
		
		function get_table(){
			$get_class	= get_class($this);
			$result = str_replace('_Model','',$get_class);
			return strtolower($result);
		}
		
		
		
		function filter_fields($fields){
			$array_count_values	= array_count_values($fields);
			foreach($this->fields as $k=>$v) {				
				$column_name	= $v["column_name"];
				if($array_count_values[$column_name] == 1){
					$result[$k]	= $v;
				}
			}
			return $result;
		}
		
		function get_select(){						
			$select = null;
			foreach($this->fields as $k=>$v){
				$column_name	= $v["column_name"];
				if($select == null)	
					$select	= $v["table_name"].".".$column_name;
				else				
					$select.= " ,<br>".$v["table_name"].".".$column_name;
			}
			return $select;
		}
		
		function get_left_join(){
			$table_name	= $this->item_list_model["table_name"];
			$fields = $this->fields;
			$left_join = null;
			foreach($fields as $k=>$v){
				$column_name	= $v["column_name"];
				
				if($v["COLUMN_KEY"] == "MUL" 
					AND $v["REFERENCED_TABLE_NAME"] != null
					AND $v["REFERENCED_COLUMN_NAME"] != null
				) {
					$left_join	.= 
					"<br>LEFT JOIN ".
					$v["REFERENCED_TABLE_NAME"].
					" ON ".$v["table_name"].".".
					$column_name." = ".
					$v["REFERENCED_TABLE_NAME"].".".
					$v["REFERENCED_COLUMN_NAME"]."";
				}
			}
			return $left_join;
		}
		
		function get_primary_key(){
			$fields = $this->fields;
			foreach($fields as $k=>$v){
				if($v["COLUMN_KEY"] == "PRI") 			return  $v["column_name"];
			//	if($v["CONSTRAINT_NAME"] == "PRIMARY")	return  $v["column_name"];
			}
		}
		
		function table_properties(){			
			$table_name	= $this->item_list_model["table_name"];
		//	unset($_SESSION["table_properties"][$table_name]);
			/*
			if(isset($_GET["table_properties_unset"])){
				unset($_SESSION["table_properties"][$table_name]);
			}
			if(isset($_SESSION["table_properties"][$table_name])){
				return $_SESSION["table_properties"][$table_name];
			}*/
		//	$left_join = $this->get_left_join();
			/*
				echo "<div style='margin-left:300px;'>";
					foreach($this->fields as $k=>$v) echo $k.": ".$v["column_name"]."<br>";
					echo "<hr />";
					echo $this->item_list_model["select"];
				echo "</div>";
			*/
			
			$table_properties["primary_key"]	= $this->item_list_model["primary_key"];
			$table_properties["fields"]	= $this->fields;
			$table_properties["item_list_sql"]	= "SELECT ".$this->item_list_model["select"].
				"<br>FROM (
				".$this->item_list_model["table_name"]." ".$this->item_list_model["left_join"]				
				."
				)
				<br>".$this->item_list_model["filter"]." <br>
				".$this->item_list_model["sort"]."";
		//	$_SESSION["table_properties"][$table_name]	= $table_properties;
			return $table_properties;
		}
		
		function number_rows_pages($item_list_sql , $num_rows_pp){
			$number_rows_pages["num_rows_total"]	= 
				$this->num_rows($item_list_sql	,	$this->db_info);					
			$page_int= (int)($number_rows_pages["num_rows_total"]/$num_rows_pp); 
			$residual = ($number_rows_pages["num_rows_total"]%$num_rows_pp); 
			if($residual == 0)
				$number_rows_pages["num_pages"] = $page_int;
			else
				$number_rows_pages["num_pages"] = $page_int + 1;
			return $number_rows_pages;
		}

		function item_update($item_id,$item_fields){
			foreach($item_fields as $k=>$v){
				if($update_content == "")	$update_content	.= $k." = '".$v."'";
				else						$update_content	.= " , ".$k." = '".$v."'";				
			}
			$update_content	.= " , last_update = '".time()."'";
			$update_sql	= "UPDATE ".$this->item_list_model["table_name"]." set ".$update_content
			." WHERE ".$this->item_list_model["table_name"].".".$this->item_list_model["primary_key"]." = '".$item_id."'";
			return $this->sql_runing($update_sql,$this->db_info);
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////		
		function item_delete($item_id){
			$delete_sql	= "delete from ".$this->item_list_model["table_name"]
				." WHERE ".$this->item_list_model["table_name"]
				.".".$this->item_list_model["primary_key"]." = '".$item_id."'";
			return $this->sql_runing($delete_sql,$this->db_info);
		}
				
		function item_add_new($update_info){
			foreach($update_info as $k=>$v){ 
				if($an_fields == ""){
					$an_fields = $k;
					$an_values = "'".$v."'";
				}	
				elseif($k <> $this->item_list_model["primary_key"]){	
					$an_fields = $an_fields." , ".$k;
					$an_values = $an_values." , '".$v."'";	
				}	
			}
			$an_fields = " (".$an_fields." , create_time)";
			$an_values = " (".$an_values." , '".time()."')";
			$addnew_sql = "insert into ".$this->item_list_model["table_name"]
				.	$an_fields
				.	" values "
				.	$an_values
			;
			return $this->sql_runing($addnew_sql,$this->db_info);
		}
		
		
/////////////////////////////////////////////////////////////////////////////////////////////////		
		
		
		
		
	}
?>






