<?php
	date_default_timezone_set('Asia/Ho_Chi_Minh');	
	$project_name	= basename(dirname(__FILE__));
	
	if(isset($_GET['version']) && $_GET['version'] == 'tmp'){
		$config_dir["application_folder"]	= "app/20151130(1)/";
	}else{
		$config_dir["application_folder"]	= "app/20151130(1)/";
	}
	$config_dir["core_folder"]		= $_SERVER['DOCUMENT_ROOT']."/beful/api/F_/";
	$config_dir["document_root"]		= $_SERVER['DOCUMENT_ROOT']."/";
	
	$config_dir["default_library"]	= "beful/api/F_/default_library_4/";
	$config_dir["default_template_folder"]["dir"]	= $config_dir["default_library"]."AdminLTE-master/";
	$config_dir["default_template_folder"]["dir_document_root"]
		= $config_dir["document_root"].$config_dir["default_template_folder"]["dir"];
	
	$config_dir["root_url"]		= "/beful/api/";
	$config_dir["front_end"]		= $config_dir["application_folder"]."library/template/front_end_3/";	
	$config_lang	= "vi";
	$config_db["db_info"]		= array(
		"hostname"		=> "localhost"	,
		"db_user"		=> "root"		,
		"db_pass"		=> "",
		"db_name" 		=> "beful"	,
		"db_port" 		=> ""
	);
	
	if(!empty($_POST['data_post']['limit_number_start'])){
		$_GET["pp"]	= $_POST['data_post']['limit_number_start'];
		unset($_POST['data_post']['limit_number_start']);
	}
	
	if(!isset($_GET["pp"]) or $_GET["pp"] == 0)
		$config_db["current_page"] = 1;
	else
		$config_db["current_page"] = $_GET["pp"];
	$config_db["item_list"]["labels"]	= array(
		"user_phonenumber"			=> "phone"	,
	//	"country_id"			=> "country"	,
	//	"district_id"			=> "district"	,
		"create_time"		=> 'create_time'	
	);
	$config_db["item_list"]["not_show_list"][1]	= "create_time";
	$config_db["item_list"]["not_show_list"][2]	= "last_update";
	$config_db["item_list"]["not_show_list"][3]	= "status";
	
	if(isset($_GET["page"])){
		$config_get_page= $_GET["page"];	
	}
	else{
		$config_get_page= "home";
	}
	$config_get_page = explode('/',$config_get_page);
	
	///////////////////////////////////////////////////////////////////////////////////////
	
//	$config_default_day	= 16;
//	$config_dir["upload_form"]		= $config_dir["front_end"]."upload_form.html";
//	include($config_dir["application_folder"]."library/config.php");
	