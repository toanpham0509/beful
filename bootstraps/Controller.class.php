<?php
namespace	BKFW\Bootstraps;
use 		BKFW\Bootstraps\Load;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
/**
 * >> Get Instance
 * @param
 * @return		Object
 *
 */
function &getControllerInstance() {
	return Controller::getInstance();
}
/**
 * >> Class Controller
 * 
 */
class Controller {
	/**
	 * A instance of class Load
	 * @var		Load
	 * @access	public
	 */
	public $load;
	/**
	 * A instance of class Lang
	 * @var		Lang
	 * @access	Public
	 */
	public $lang;
	/**
	 * A instance of class Router
	 * @var		Router
	 * @access	Public
	 */
	public $router;
	/**
	 * Istance of this
	 */
	private static $instance;
	/**
	 * 
	 * @var unknown
	 */
	public $page;
	/**
	 * Constructor
	 */
 	public function __construct() {
 		global $system;
 		self::$instance =& $this;
 		$this->load = System::$load;
 		$this->lang = System::$lang;
 		$this->router = System::$router;
 		$this->page = array(
 				"pageID" => "",
 				"pageTitle" => "",
 				"data"	=> array( "urlPrint" => "" )
 		);
	}
	/**
	 * Get instance
	 * @access		public
	 * @return		object
	 */
	public static function &getInstance() {
		return self::$instance;
	}
}

/*end of file Controller.class.php */
/*location: */