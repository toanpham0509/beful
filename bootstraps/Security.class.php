<?php
namespace BKFW\Bootstraps;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Security {
	public function __construct() {
	}
	
	public function validateControllerName( $stringInput ){
		if( ctype_alpha( $stringInput[0] ) ) {
			$stringInput[ 0 ] = strtoupper( $stringInput[0] );
		}
		$stringLength = strlen( $stringInput );
		for( $i = 1; $i < $stringLength; $i++ ) {
			if( ( $stringInput[ $i ] == "-" || $stringInput[ $i ] == "_" ) && isset( $stringInput[ $i + 1 ] ) && ctype_alpha( $stringInput[ $i + 1 ] ) ) {
				$stringInput[ $i ] = strtoupper( $stringInput[ $i + 1 ] );
				$stringInput[ $i + 1 ] = "";
				$i += 2;
			}	
		}
		return $stringInput;
	}
}
/*end of file Security.class.php */
/*location: bootstraps/Security.class.php*/