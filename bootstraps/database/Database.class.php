<?php
namespace	BKFW\Bootstraps\Database;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
abstract class Database {
	/**
	 * Param connect
	 * 
	 * @access	protected
	 * @param	unknow
	 */
	protected $cn;
	/**
	 * Host type
	 * 
	 * @access	protected
	 * @param	string
	 */
	protected $hostType;
	/**
	 * Database server
	 * 
	 * @var		string
	 * @access	protected
	 */
	protected 	$host;
	/**
	 * Post name
	 * 
	 * @var 	int
	 * @access	protected
	 */
	protected 	$port;
	/**
	 * Database's name
	 * 
	 * @var		string
	 * @access	protected
	 */
	protected 	$databaseName;
	/**
	 * 
	 * @var		string
	 */
	protected 	$userName;
	/**
	 * 
	 * @var		string
	 */
	protected 	$userPasswork;
	/**
	 *
	 * @var		array
	 */
	protected $from;
	/**
	 * @var 	array
	 */
	protected $join;
	/**
	 * @access	public
	 * @var		array
	 */
	protected $select;
	/**
	 * 
	 * @var		array
	 */
	protected $where;
	/**
	 * 
	 * @var		array
	 */
	protected $orWhere;
	/**
	 * @access	protected
	 * @var		string
	 */
	protected $limit;
	/**
	 * @access	protected
	 * @var		string
	 */
	protected $groupBy;
	/**
	 * @access	protected
	 * @var		string
	 */
	protected $having;
	/**
	 * @access	protected
	 * @var		boolean
	 */
	protected $selectDistinct;
	/**
	 * @access	protected
	 * @var		string
	 */
	protected $order;
	/**
	 * 
	 * @var		string
	 */
	protected $query;
	/**
	 * @var		array | boolean
	 * @access	protected
	 */
	protected $result;
	/**
	 * @var		array | boolean
	 * @access	protected
	 */
	protected $insertedID;
	/**
	 * @var		array
	 * @access	protected
	 */
	protected $setItem;
	/**
	 * Connect to database
	 * @access		public
	 * @return		boolean
	 */
	public abstract function connect();
	/**
	 * Disconnect to database
	 * @access		public
	 * @return		boolean
	 */
	public abstract function disconnect();
	/**
	 * Select column in database table
	 * 
	 * @param		array	|	string		$dataSelect
	 * @access		public
	 * @return		void
	 */
	public abstract function select( $dataSelect );
	/**
	 * Select max value in a column of databse table
	 * @param		string		$columnName
	 * @access		public
	 * @return		void
	 */
	public abstract function selectMax( $columnName, $asName = null );
	/**
	 * Select min value in a column of database table
	 * @param		string		$columnName
	 * @access		public
	 * @return		void
	 */
	public abstract function selectMin( $columnName, $asName = null );
	/**
	 * Select sum of all record of database table column name
	 * 
	 * @param		string		$columnName
	 * @return		void
	 * @access		public
	 */
	public abstract function selectSum( $columnName, $asName = null );
	/**
	 * Select average of all record of database table column name
	 *
	 * @param		string		$columnName
	 * @return		void
	 * @access		public
	 */
	public abstract function selectAvg( $columnName, $asName = null );
	/**
	 * Select count of all record of database table column name
	 *
	 * @param		string		$columnName
	 * @return		void
	 * @access		public
	 */
	public abstract function selectCount( $columnName, $asName = null );
	/**
	 * Select table name in database
	 * 
	 * @param		string		$tableName
	 * @access		public
	 * @return		void
	 */
	public abstract function from( $tableName, $asTableName = NULL );
	/**
	 * Join table
	 * 
	 * @param		string		$tableName
	 * @param		string		$clause
	 * @param		string		$option
	 * @access		public
	 * @return		void
	 */
	public abstract function join( $tableName, $clause, $option = NULL );
	/**
	 * Where
	 * 
	 * @param 		string 	|	array		$key
	 * @param 		string 					$value
	 * @access		public
	 * @return		void
	 */
	public abstract function where( $key, $value = NULL, $operator = NULL );
	/**
	 * Or where
	 * 
	 * @param		string | array		$key
	 * @param		string				$value
	 * @access		public
	 * @return		void
	 */
	public abstract function orWhere( $key, $value = NULL, $operator = NULL );
	/**
	 * Where in
	 * 
	 * @param		string			$key
	 * @param		array			$dataIn
	 * @accesd		public
	 */
	public abstract function whereIn( $key, $dataIn );
	/**
	 * Or where in
	 *
	 * @param		string			$key
	 * @param		array			$dataIn
	 * @accesd		public
	 */
	public abstract function orWhereIn( $key, $dataIn );
	/**
	 * Where not in
	 *
	 * @param		string			$key
	 * @param		array			$dataIn
	 * @accesd		public
	 */
	public abstract function whereNotIn( $key, $dataIn );
	/**
	 * Or where not in
	 *
	 * @param		string			$key
	 * @param		array			$dataIn
	 * @accesd		public
	 */
	public abstract function orWhereNotIn( $key, $dataIn );
	/**
	 * Like
	 * 
	 * @param		string			$key
	 * @param		string			$value
	 * @param		string			$option
	 * @access		public
	 * @return		void
	 */
	public abstract function like( $key, $value, $option = NULL );
	/**
	 * Or like
	 *
	 * @param		string			$key
	 * @param		string			$value
	 * @param		string			$option
	 * @access		public
	 * @return		void
	 */
	public abstract function orLike( $key, $value, $option = NULL );
	/**
	 * Not like
	 *
	 * @param		string			$key
	 * @param		string			$value
	 * @param		string			$option
	 * @access		public
	 * @return		void
	 */
	public abstract function notLike( $key, $value, $option = NULL );
	/**
	 * Or not like
	 *
	 * @param		string			$key
	 * @param		string			$value
	 * @param		string			$option(LEFT, RIGHT, BOTH <=> NULL)
	 * @access		public
	 * @return		void
	 */
	public abstract function orNotLike( $key, $value, $option = NULL );
	/**
	 * Group by
	 *
	 * @param		string | array	$key
	 * @access		public
	 * @return		void
	 */
	public abstract function groupBy( $data );
	/**
	 * Select distinct record
	 */
	public abstract function distinct();
	/**
	 * Having
	 * 
	 * @param		string		$key
	 * @param		string		$value
	 * @param		string		$operator (<, =, >, <=, >=, !=)
	 * @access		public
	 * @return		void
	 */
	public abstract function having( $key, $value, $operator = NULL );
	/**
	 * 
	 * Order by
	 * 
	 * @param		string		$columnName
	 * @param		string		$option
	 * @return		void
	 */
	public abstract function order( $columnName, $option = NULL );
	/**
	 * Limit 
	 * 
	 * @param		int			$limit
	 * @param		int			$start
	 * @return		void
	 */
	public abstract function limit( $limit, $start = 0 );
	/**
	 * Run customs query
	 * 
	 * @param		string		$queryString
	 * @access		public
	 * @return		void
	 */
	public abstract function query( $queryString = NULL );
	/**
	 * 
	 * @param 		string		$tableName
	 * @access		public
	 * @return		array
	 */
	public abstract function get( $tableName = NULL );
	/**
	 * Get result of query
	 * 
	 * @return		array | boolean
	 * @access		public
	 */
	public abstract function getResult();
	/**
	 * Number row of rusult
	 * @access		public
	 * @return		void
	 */
	public abstract function countResult();
	/**
	 * Set item for insert and update query
	 * 
	 * @param		string | array		$key
	 * @param 		string 				$value
	 */
	public abstract function setItem( $key, $value = NULL );
	/**
	 * Insert
	 * @param 		string		$tableName
	 * @param		array		$dataInsert
	 * @return		boolean
	 */
	public abstract function insert( $tableName, $dataInsert = NULL );
	/**
	 * Get inserted id
	 * @return		int
	 * @access		public
	 */
	public abstract function insertedID();
	/**
	 * Update 
	 * @param		string		$tableName
	 * @param		array		$dataUpdate
	 * @access		public
	 * @return 		boolean
	 */
	public abstract function update( $tableName, $dataUpdate = NULL );
	/**
	 * Delete
	 * @param		string		$tableName
	 * @param		array		$dataCondition
	 */
	public abstract function delete( $tableName, $dataCondition = NULL );
	/**
	 * Empty table
	 * @param		string		$tableName
	 * @return		boolean
	 */
	public abstract function emptyTable( $tableName );
	/**
	 * Create query string
	 * @return		boolean
	 */
	public abstract function createQueryStringSelect();
	/**
	 * Get query string
	 * @return		boolean
	 */
	public abstract function getQueryString();
	/**
	 * Get connect
	 * @return		boolean
	 */
	public abstract function getConnect();
}
/*end of file Database.class.php*/
/*location: ./bootstraps/database/Database.class.php*/