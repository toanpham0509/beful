<?php
namespace 	BKFW\Bootstraps;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
/**
 * Class Load
 * 
 * @package			bootstraps
 * @link			http://bk-fw.bk-webs.com/user_guide/load.html
 * @property		include a file, a package
 */
class Load {
	/**
	 * List of path to load controller from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathControllers = array();
	/**
	 * List of path to load models from
	 * 
	 * @var 		array
	 * @access		private
	 */
	private $basePathModels = array();
	/**
	 * List of path to load views from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathViews = array();
	/**
	 * List of path to load helpers from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathHelpers = array();
	/**
	 * List of path to load libraries from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathLibraries = array();
	/**
	 * All class was included
	 * 
	 * @access		private
	 * @var			array
	 */
	private $loadedClasses;
	/**
	 * construct
	 */
	public function __construct() {
		$this->basePathHelpers = array(
			APPLICATION_FOLDER . "/helpers/",
			BOOTSTRAP_FOLDER . "/helpers/"
		);
		$this->basePathLibraries = array(
			APPLICATION_FOLDER . "/libraries/",
			BOOTSTRAP_FOLDER . "/libraries/"
		);
		$this->basePathControllers = array(
			APPLICATION_FOLDER . "/controllers/"
		);
		$this->basePathModels = array( APPLICATION_FOLDER . "/models/" );
		$this->basePathViews = array( APPLICATION_FOLDER . "/views/" );
		$this->loadedClasses = array();
		
		//register autoload function
// 		spl_autoload_register( array( $this, "model" ) );
// 		spl_autoload_register( array( $this, "helper" ) );
// 		spl_autoload_register( array( $this, "library" ) );
		
	}
	/**
	 * get models's base path
	 *
	 * @access			public
	 * @return 			array
	 */
	public function getBasePathModels() {
		return $this->basePathModels;
	}
	/**
	 * get views's base path
	 *
	 * @access			public
	 * @return			array
	 */
	public function getBasePathViews() {
		return $this->basePathViews;
	}
	/**
	 *
	 */
	public function getBasePathHelpers() {
		return $this->basePathHelpers;
	}
	/**
	 * get libraries's base path
	 *
	 * @access			public
	 * @return 			array
	 */
	public function getBasePathLibraries() {
		return $this->basePathLibraries;
	}
	/**
	 * load class
	 * 
	 * @param		string		class name
	 * @param		string		object name
	 * @param		array		parameter
	 * @access		public
	 * @return		void
	 */
	public function loadClass( $className, $objectName = NULL ) {
		$inst =& getControllerInstance();
		$this->loadedClasses[] = $className;
		$objectName = ( $objectName !== NULL ) ? $objectName : $className;
		$objectName[0] = strtolower( $objectName[0] );
		$inst->{$objectName} = new $className();
	}
	/**
	 * isLoaded class
	 * 
	 * @access		public
	 * @param		string		Class name
	 * @return		boolean		Return true if that class was included, else return false
	 */
	public function isLoaded( $className ) {
		return isset( $this->loadedClasses[ $className ] ) ? true : false;
	}
	/**
	 * Load view
	 * 
	 * @param		String
	 * @access		public
	 * @return		void
	 * @property	include filePath if it is exist.
	 * */
	public function view( $viewName, $fwdata = array() ) {
		if( !empty( $fwdata ) ) {
			foreach ( $fwdata as $key => $value ) {
				${$key} = $value;
			}
		}
		unset( $fwdata );
		foreach( $this->basePathViews as $item ) {
			$pathHaveExt = ( System::$config->viewFileExtension == NULL ) ? 
							( $item . $viewName . ".php" ) : 
							$item . $viewName . ".{System::$config->viewFileExtension}.php";
			$pathHaventExt = $item . $viewName;
			if( file_exists( $pathHaveExt ) ) {
				include_once( $pathHaveExt );
			} elseif( file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
			}
		}
	}
	/**
	 * Load model
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 * @property
	 */
	public function model( $modelName, $className = NULL, $objectName = NULL ) {
		if( $className == NULL ) $className = $modelName;

		foreach( $this->basePathModels as $item ) {
		 	$pathHaveExt = $item . $modelName . "." . System::$config->subclassPrefix . ".php";
		 	$pathHaventExt = $item . $modelName;
			if( file_exists( $pathHaveExt ) ) {
				include_once( $pathHaveExt );
			} elseif( file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
			}
		}
		$className = System::$config->namespaceAppModel . "\\" . $className;
		if( class_exists( $className ) ) {
			$objectName = ( $objectName == NULL ) ? $modelName : $objectName;
			$this->loadClass( $className, $objectName );
		}
	}
	/**
	 * Load controller
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 * @property
	 */
	public function controller( $controllerName, $className = NULL, $objectName = NULL ) {
		if( $className == NULL ) $className = $controllerName;

		foreach( $this->basePathControllers as $item ) {
			$pathHaveExt = $item . $controllerName . "." . System::$config->subclassPrefix . ".php";
			$pathHaventExt = $item . $controllerName;
			if( file_exists( $pathHaveExt ) ) {
				include_once( $pathHaveExt );
			} elseif( file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
			}
		}
		$className = System::$config->namespaceAppController . "\\" . $className;
		if( class_exists( $className ) ) {
			$objectName = ( $objectName == NULL ) ? $controllerName : $objectName;
			$this->loadClass( $className, $objectName );
		}
	}
	/**
	 * Load helper
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 */
	public function helper( $filePath, $className = NULL, $objectName = NULL ) {
		foreach( $this->basePathHelpers as $item ) {
			if( $className == NULL ) {
				$class = $filePath;
				$xClass = $class;
			}
			$pathHaveExt = $item . $filePath . ".{System::$config->subclassPrefix}.php";
			$pathHaventExt = $item . $filePath;
			if( file_exists( $pathHaveExt ) ) {
				include_once( $pathHaveExt );
			} elseif( file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
			}
			$class = System::$config->namespaceSystemHelper . "\\" . $class;
			if( class_exists( $class ) ) {
				$objectName = ( $objectName == NULL ) ? $xClass : $objectName;
				$this->loadClass( $class, $objectName );
			}
		}
	}
	/**
	 * library
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 */
	public function library( $filePath, $className = NULL, $objectName = NULL ) {
		$libClass = $filePath;
		if( $className == NULL ) $className = $filePath;
		foreach( $this->basePathLibraries as $item ) {
			$pathHaveExt = $item . $filePath . "." . System::$config->subclassPrefix . ".php";
			$pathHaventExt = $item . $filePath;
			if( file_exists( $pathHaveExt ) ) {
				include_once( $pathHaveExt );
				$className = System::$config->namespaceSystemLibarary . "\\" . $className;
				if( class_exists( $className ) ) {
					$objectName = ( $objectName == NULL ) ? $filePath : $objectName;
					$this->loadClass( $className, $objectName );
				}
			} elseif( is_file( $pathHaventExt ) && file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
				$className = System::$config->namespaceSystemLibarary . "\\" . $className;
				if( class_exists( $className ) ) {
					$objectName = ( $objectName == NULL ) ? $className : $objectName;
					$this->loadClass( $className, $objectName );
				}
			}
		}
	}
	/**
	 * load a file
	 * 
	 * @param		String
	 * @access		public
	 * @return 		void
	 */
	public function file( $filePath ) {
		if( file_exists( $filePath ) ) {
			include ( $filePath );
		} elseif( file_exists( $filePath . ".php" ) ) {
			include ( $filePath . ".php" );
		}
	}
	/**
	 * include a package
	 * 
	 * @param		String
	 * @access		public
	 * @return 		void
	 * @property	include all file in a package if it is exists
	 */
	public function package( $packagePath ) {
		if( is_dir( $packagePath ) ) {
			$files = scandir( $packagePath );
			foreach ( $files as $file ) {
				$filePath = $packagePath . "/" . $file;
				if( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/*end of file Load.class.php*/
/*location: ./boostraps/Load.class.php*/