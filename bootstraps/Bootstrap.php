<?php
ob_start();
use 		BKFW\Bootstraps\Cache;
use 		BKFW\Bootstraps\System;
use 		BKFW\Bootstraps\Security;
use 		BKFW\Bootstraps\Error;
use 		BKFW\Bootstraps\Config;
use 		BKFW\Bootstraps\Lang;
use 		BKFW\Bootstraps\Router;
use 		BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined( 'BOOTSTRAP_PATH' ) ) exit("No direct script access allowed");
//report error
if( ENVIRONMENT == "publish" ) {
	//disable error message
	ini_set( "error_reporting", 0 );
}
/**
 * Load functions
 */
require_once ( 'Functions.php' );
/**
 * >> autoload class
 */
function __autoload( $className ) {
	$className = explode( '\\', $className );
	$className = end( $className );
	$filePath = BOOTSTRAP_FOLDER . "/" . $className . ".class.php";
	if (is_file ( $filePath )) {
		require_once ($filePath);
	}
}
/**
 * >> Security
 * */
System::setSecurity( new Security() );
/**
 * >> include all file congfig in folder configs
 * */
$folderConfig = "configs";
if( is_dir( $folderConfig ) ) {
	$files = scandir ( $folderConfig );
	foreach ( $files as $file ) {
		$filePath = $folderConfig . "/" . $file;
		if ( is_file ( $filePath ) ) {
			include_once ( $filePath );
		}
	}
} else {
	$error = new Error( "404", "404", "Config folder ($folderConfig) not found" );
	$error->showError();
	exit();
}
System::setConfig( new Config() );
/**
 * >> Language
 * */
System::setLang( new Lang() );
/**
 * >> autoload package. This has been defined in file autoload.php in folder config 
 * */
if( isset ( $autoload [ 'packages' ] ) ) {
	foreach ( $autoload [ 'packages' ] as $item ) {
		if ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * >> autoload libraries. This has been defined in file autoload.php in folder config
 * */
if ( isset ( $autoload [ 'libraries' ] ) ) {
	foreach ( $autoload [ 'libraries' ] as $item ) {
		$item = "libraries/" . $item;
		$filePath = $item;
		if ( is_file ( $filePath ) ) {
			include_once ( $filePath );
		} elseif ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * >> autoload helpers. This has been defined in file autoload.php in folder config
 * 
 * */
$helperFolder = "helpers";
if ( is_dir( $helperFolder ) ) {
	$files = scandir( $helperFolder );
	foreach ( $files as $file ) {
		$filePath = $helperFolder . "/" . $file;
		if ( is_file( $filePath ) ) {
			include_once ( $filePath );
		}
	}
}
if ( isset( $autoload [ 'helpers' ] ) ) {
	foreach ( $autoload [ 'helpers' ] as $item ) {
		$item = APPLICATION_FOLDER . "/helpers/" . $item ;
		if ( is_file( $item ) ) {
			include_once ( $item );
		} elseif ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * >> autoload language. This has been defined in file autoload.php in folder config
 * 
 * Autoload language will processed in class Lang( file Lang.class.php )	
 * */

/**
 * >> autoload model. This has been defined in file autoload.php in folder config
 * */
System::$load->file( BOOTSTRAP_FOLDER . "/Model.class.php" );
if ( isset( $autoload['models'] ) ) {
	foreach ( $autoload [ 'models' ] as $item ) {
		$item = APPLICATION_FOLDER . "/models/" . $item;
		if ( is_file( $item ) ) {
			include_once ( $item );
		} elseif ( is_dir( $item ) ) {
			$files = scandir( $item );
			foreach ( $files as $file ) {
				include_once ( $item . "/" . $file );
			}
		}
	}
}
//set default time zone
date_default_timezone_set( System::$config->timeZone );
// create a object from Class Cache
System::setCache( new Cache() );
//start cache
System::$cache->start();
//create a object Router in object $system
System::setRouter( new Router() );
//load controler, method, method's parameter
System::$router->load();
//router
System::$router->router();
//get output buffer content then write to file cache
System::$cache->push();
System::$cache->finish();
/* End of file bootstrap.php */
/* Location: ./bootstraps/Bootstrap.php */