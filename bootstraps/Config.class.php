<?php
namespace BKFW\Bootstraps;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Config {
	/**
	 * >> autoload 
	 * 
	 * @var 	ArrayObject
	 * @access 	public		
	 */
	public $autoload;
	/**
	 * @var 	string
	 * @access	public
	 */
	public $baseUrl;
	/**
	 * Allowed file type to upload
	 * 
	 * @var		string
	 * @access	public
	 */
	public $allowedFileType;
	/**
	 * >> cache's status
	 * 
	 * @var 	boolean
	 * @access	public
	 */
	public $cache;
	/**
	 * >> cache folder
	 * 
	 * @var		string
	 * @access	public
	 */
	public $cacheFolder;
	/**
	 * @var 	int
	 * @access	public
	 */
	public $cacheTime;
	/**
	 * 
	 * @var		string
	 * @access	public
	 */
	public $host;
	/**
	 * @var		string : mysqli, sqlsever, nosql, sqlite, or oraclesql
	 * @access	public
	 */
	public $databaseType;
	/**
	 * @var		string
	 * @access	public
	 */
	public $databaseName;
	/**
	 * @var 	string
	 * @access	public
	 */
	public $userName;
	/**
	 * @var 	string
	 * @access	public
	 */
	public $userPassword;
	/**
	 * @var 	boolean
	 * @property	port status
	 * @access	public
	 */
	public $port;
	/**
	 * @var 	boolean
	 * @property	email status
	 * @access	public
	 */
	public $mail;
	/**
	 * @var 	string 		email
	 * @access	public
	 */
	public $mailFrom;
	/**
	 * @var		string		email
	 * @access	public
	 */
	public $mailFromName;
	/**
	 * @var		string
	 * @access	public 
	 */
	public $mailReply;
	/**
	 * @var 	string
	 * @access	public
	 */
	public $defaultControler;
	/**
	 * @var		string
	 * @access	public
	 */
	public $urlSuffix;
	/**
	 * @var		string
	 * @access	public
	 */
	public $charset;
	/**
	 * @var		string
	 * @access	public
	 */
	public $subclassPrefix;
	/**
	 * @var		string
	 * @access	public
	 */
	public $viewFileExtension;
	/**
	 * @var		string
	 * @access	public
	 */
	public $cacheFileExtension;
	/**
	 * @var		string
	 * @access	public
	 */
	public $permittedURIChar;
	/**
	 * @var		string
	 * @access	public 
	 */
	public $timeZone;
	/**
	 * @var		string
	 * @access	public 
	 */
	public $dateFormat;
	/**
	 * @var		string
	 * @access	public
	 */
	public $encryptionKey;
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceAppController;
	
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceAppModel;
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceSystemLibarary;
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceAppLibarary;
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceSystemHelper;
	/**
	 * @var		string
	 * @access	public
	 */
	public $namespaceAppHelper;
	/**
	 * Construct
	 */
	public function __construct() {
		//autoload
		global $autoload;
		$this->autoload = $autoload;
		//base url
		$this->baseUrl = defined( "BASE_URL" ) ? BASE_URL : NULL;
		//allowed file type
		$this->allowedFileType = defined( "ALLOWED_FILE_TYPE_UPLOAD" ) ? ALLOWED_FILE_TYPE_UPLOAD : NULL;
		//cache
		$this->cache = defined( "CACHE" ) ? CACHE : false;
		//cache folder
		$this->cacheFolder = defined( "CACHE_FOLDER" ) ? CACHE_FOLDER : "cache";
		//cache time
		$this->cacheTime = defined( "CACHE_TIME" ) ? CACHE_TIME : 3600 ;
		//db host
		$this->host = defined( "HOST" ) ? HOST : 3600 ;
		//database type
		$this->databaseType = defined( "DATABASE_TYPE" ) ? DATABASE_TYPE : "mysql";
		//database type
		$this->port = defined( "PORT" ) ? PORT : null;
		//database name
		$this->databaseName = defined( "DATABASE_NAME" ) ? DATABASE_NAME : NULL;
		//user name
		$this->userName = defined( "USER_NAME" ) ? USER_NAME : NULL;
		//user password
		$this->userPassword = defined( "USER_PASSWORD" ) ? USER_PASSWORD : NULL;
		//mail
		$this->mail = defined( "MAIL" ) ? MAIL : NULL;
		//mail from
		$this->mailFrom = defined( "FROM_MAIL" ) ? FROM_MAIL : NULL;
		//mail from name
		$this->mailFromName = defined( "FROM_NAME" ) ? FROM_NAME : NULL;
		//mail reply
		$this->mailReply = defined( "MAIL_REPLY" ) ? MAIL_REPLY : NULL;
		//default controller
		$this->defaultControler = defined( "DEFAULT_CONTROLER" ) ? DEFAULT_CONTROLER : NULL;
		//url suffix
		$this->urlSuffix = defined( "URL_SUFFIX" ) ? URL_SUFFIX : NULL;
		//charset
		$this->charset = defined( "CHARSET" ) ? CHARSET : "utf-8";
		//subclass prefix
		$this->subclassPrefix = defined( "SUBCLASS_PREFIX" ) ? SUBCLASS_PREFIX : NULL;
		//extension of view file
		$this->viewFileExtension = defined( "VIEW_FILE_EXT" ) ? VIEW_FILE_EXT : NULL;
		//extension of cache file
		$this->cacheFileExtension = defined( "CACHE_FILE_EXT" ) ? CACHE_FILE_EXT : NULL; 
		//permitted url char
		$this->permittedURIChar = defined( "PERMITTED_URI_CHAR" ) ? PERMITTED_URI_CHAR : NULL;
		//time zone
		$this->timeZone = defined( "TIME_ZONE" ) ? TIME_ZONE : "Asia/Ho_Chi_Minh";
		//date format
		$this->dateFormat = defined( "DATE_FORMAT" ) ? DATE_FORMAT : "Y-m-d H-i-s";
		//encryption key
		$this->encryptionKey = defined( "ENCRYPTION__KEY" ) ? ENCRYPTION__KEY : "V#$(**D#SFB#%#^";
		//namespace controller
		$this->namespaceAppController = defined( "NAMESPACE_APPLICATON_CONTROLLER" ) ? NAMESPACE_APPLICATON_CONTROLLER : "App\\Controllers";
		//namespacemodel
		$this->namespaceAppModel = defined( "NAMESPACE_APPLICATION_MODEL" ) ? NAMESPACE_APPLICATION_MODEL : "App\\Models";
		//namespaceAppLibrary
		$this->namespaceAppLibarary = defined( "NAMESPACE_LIBRARY" ) ? NAMESPACE_APPLICATION_LIBRARY : "App\\Libraries";
		//namespaceSystemLibrary
		$this->namespaceSystemLibarary = defined( "NAMESPACE_SYSTEM_LIBRARY" ) ? NAMESPACE_SYSTEM_LIBRARY : "BKFW\\Libraries";
		//namespace system helper
		$this->namespaceSystemHelper = defined( "NAMESPACE_SYSTEM_HELPER" ) ? NAMESPACE_SYSTEM_HELPER : "BKFW\\Helpers";
		//namespace applicatio helper
		$this->namespaceAppHelper = defined( "NAMESPACE_APPLICATION_HELPER" ) ? NAMESPACE_APPLICATION_HELPER : "App\Helpers";
	}
	/**
	 * >> set item config
	 * 
	 * @param		string		$key
	 * @param		unknow		$value
	 * @access		public
	 * @return		void
	 */
	public function setItem( $key, $value ) {
		$this->key = $value;
	}
}
/*end of file Config.class.php*/
/*location: ./bootstraps/Config.class.php*/