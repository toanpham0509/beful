<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
/**
 *
 * @return string
 */
function baseURL( $path = NULL ) {
	return System::$config->baseUrl . $path;
}
/**
 * 
 */
function getPrefixURL() {
	global $system;

}
function isHome() {
	$path = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	if( $path == BASE_URL ) {
		return true;
	} else {
		return false;
	}
}
function theExcerpt( $theContent, $charlength = 200 ) {
	$excerpt = strip_tags($theContent);
	$charlength++;
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$return = mb_substr( $subex, 0, $excut );
		} else {
			$return = $subex;
		}
		$return .= '...';
	} else {
		$return = $excerpt;
	}
	return $return;
}
/*end of file Functions.php*/
/*location: Functions.php*/