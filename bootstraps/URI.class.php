<?php
namespace	BKFW\Bootstraps;
use 		BKFW\Bootstraps\Error		as		NSError;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class URI {
	/**
	 * Current uri string
	 *
	 * @var 		string
	 * @access		private
	 */
	private $uri;
	/**
	 * @var			ArrayObject
	 * @access		private
	 */
	private $segment;
	/**
	 * >> constuctor
	 */
	public function __construct() {
	}
	/**
	 * @return		string
	 * @access		public
	 */
	public function getURI() {
		global $system;
		return str_replace( System::$config->urlSuffix , "", URI::filterURI( URI::detectURI() ) );
	}
	/**
	 * Detects the URI
	 *
	 * This function will detect the URI automatically and fix the query string
	 * if necessary.
	 *
	 * @access		private
	 * @return		string
	 */
	private function detectURI() {
		if ( !isset($_SERVER['REQUEST_URI']) OR !isset($_SERVER['SCRIPT_NAME'])){
			return '';
		}
	
		$uri = $_SERVER['PHP_SELF'];
		if( strpos($uri, $_SERVER['SCRIPT_NAME']) === 0 ) {
			$uri = substr($uri, strlen($_SERVER['SCRIPT_NAME']));
		}
		elseif (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0) {
			$uri = substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
		}
	
		// This section ensures that even on servers that require the URI to be in the query string (Nginx) a correct
		// URI is found, and also fixes the QUERY_STRING server var and $_GET array.
		if (strncmp($uri, '?/', 2) === 0) {
			$uri = substr($uri, 2);
		}
		$parts = preg_split('#\?#i', $uri, 2);
		$uri = $parts[0];
		if (isset($parts[1])) {
			$_SERVER['QUERY_STRING'] = $parts[1];
			parse_str($_SERVER['QUERY_STRING'], $_GET);
		} else {
			$_SERVER['QUERY_STRING'] = '';
			$_GET = array();
		}
	
		if ($uri == '/' || empty($uri)) {
			return '/';
		}
	
		$uri = parse_url($uri, PHP_URL_PATH);
		$uri = str_replace( " ", "", $uri );
		
		
		// Do some final cleaning of the URI and return it
		return $uri = str_replace( array( '//', '../' ), '/', trim( $uri, '/' ) );
	}
	/**
	 * Filter segments for malicious characters
	 *
	 * @access	private
	 * @param	string
	 * @return	string
	 */
	private function filterURI( $str ) {
		global $system;
		if ( $str != '' && System::$config->permittedURIChar != '' ) {
			// preg_quote() in PHP 5.3 escapes -, so the str_replace() and addition of - to preg_quote() is to maintain backwards
			// compatibility as many are unaware of how characters in the permitted_uri_chars will be parsed as a regex pattern
			if ( ! preg_match("|^[".str_replace(array('\\-', '\-'), '-', preg_quote( System::$config->permittedURIChar , '-'))."]+$|i", $str))
			{
				$error = new Error( "404", $system->lang->getString( "danger" ), $system->lang->getString( "the_uri_you_submitted_has_disallowed_characters" ), $system->lang->getString( "Danger" ) );
				$error->showError();
				die();
			}
		}
		// Convert programatic characters to entities
		$bad	= array('$',		'(',		')',		'%28',		'%29');
		$good	= array('&#36;',	'&#40;',	'&#41;',	'&#40;',	'&#41;');
	
		return str_replace($bad, $good, $str);
	}
}
/*end of file URI.class.php*/
/*location: URI.class.php*/