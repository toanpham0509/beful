<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 * @author    	BK-Webs
 * @author uri	http://bk-webs.com
 * @copyright	Copyright (c) 2015, BK-Webs, Inc.
 * @license   	http://bk-fw.bk-webs.com/license/
 * @link    	http://bk-fw.bk-webs.com
 * @version		Version 1.0
 * @filesource	http://bk-fw.bk-webs.com/download/
 */