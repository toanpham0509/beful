<?php
namespace	BKFW\Bootstraps;
use 		BKFW\Bootstraps		as 		BKFW;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
System::$load = new Load();
class System {
	/**
	 * @var 	string
	 * @access	private
	 */
	private static $langCode;
	/**
	 * @var 	Lang
	 * @access	public static
	 */
	public static $lang;
	/**
	 * @var 	Security
	 * @access	public static
	 */
	public static $security;
	/**
	 * @var 	Config
	 * @access	public static
	 */
	public static $config;
	/**
	 * @var 	Cache
	 * @access	public static
	 */
	public static $cache;
	/**
	 * @var 	Load
	 * @access	public static
	 */
	public static $load;
	/**
	 * @var		Router
	 * @access	public static
	 */	
	public static $router;
	/**
	 * >> constructor
	 */
	public function __construct() {
	}
	/**
	 * >> set langcode
	 * 
	 * @param	string		$langCode
	 * @access	public static
	 */
	public static function setLangCode( $langCode ) {
		System::$langCode = $langCode;
		System::$lang = new NSLang\Lang( $this->langCode );
	}
	/**
	 * >> get lang code
	 * 
	 * @return	string	Lang code
	 * @access	public static
	 */
	public static function getLangCode() {
		return $this->langCode;
	}
	/**
	 * >> set lang
	 *  
	 * @param	Lang	$lang
	 * @access	public static
	 */
	public static function setLang( $lang ) {
		System::$langCode = $lang->getLangCode();
		System::$lang = $lang;
	}
	/**
	 * >> get Lang
	 * 
	 * @return		Lang
	 * @access	 	public static
	 */
	public static function getLang() {
		return System::$lang;
	}
	/**
	 * >> set security
	 * 
	 * @param	Security	$security
	 * @access	public static
	 */
	public static function setSecurity( $security ){
	 	 System::$security = $security;
	}
	/**
	 * >> set config
	 * 
	 * @param	Config		$config
	 * @access	public static
	 */
	public static function setConfig( $config ) {
		System::$config = $config;
	}
	/**
	 * >> set cache
	 * 
	 * @param	Cache	$cache
	 * @access	public static
	 */
	public static function setCache( $cache ) {
		System::$cache = $cache;
	}
	/**
	 * >> get cache
	 * 
	 * @return		Cache
	 * @access	 	public static
	 */
	public static function getCache() {
		return System::$cache;
	}
	/**
	 * >> set router
	 * 
	 * @param		Router 		$router
	 * @access		public static
	 */
	public static function setRouter( $router ) {
		System::$router = $router;
	}
	/**
	 * >> get router
	 * 
	 * @return		Router
	 * @access		public static
	 */
	public static function getRouter() {
		return System::$router;
	}
}
/*end of file System.class.php*/
/*location: ./bootstraps/System.class.php*/