$(document).ready(function(){
	objectResult = $(".tp-search-box .result");
	objectCloseSearch = $(".closeSearch");
	var objectInputSearch = $('.tp-search-box .input-search');
	var objectResultItem = $(".tp-search-box .item-result");
	$( objectResult ).hide();
	//$( objectResultItem ).hide();
	$( objectInputSearch ).focus(function(){
		$(this).parent().find( objectResult ).show();
		$(".mask").show();
	});
	$( objectInputSearch ).blur(function(){
		//$( objectResult ).hide();
	});
	$( objectInputSearch ).keyup(function(){
		var lengthTmp = $(this).val().length;

		/*
		if(lengthTmp == 1) {
			console.log("Search server");
		} else {
			console.log("Search local");
		}
		*/
		console.log("Search server");

		var inputName = $(".item-result").attr("data-input-name");

		var url;
		var html  = '<a class="closeSearch">Đóng</a>';
		if(inputName == "customer_id") {
			url = baseUrl + "ajax/customer/searchCustomer/?customer_name=" + $(this).val();
			$.ajax({
				url: url,
				dataType: "html",
				type: "GET",
				data: "",
				success: function( data ){
					data = $.parseJSON( data );
					if(data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							html += '<div ' +
									'class="item-result" ' +
									'data-name="' + convertVietnamese(data[i]['customer_name']) + ' (' +
									data[i]['customer_id']
									+ ')" ' +
									'data-id="' + data[i]['customer_id'] + '" ' +
									'data-display="' +
									data[i]['customer_name']
									+ ' (' + data[i]['customer_id'] + ')" ' +
									'data-input-name="customer_id">' +
									data[i]['customer_name'] + ' (' +
									data[i]['customer_id']
									+ ')' + ' - ' + data[i]['phone'] + ' </div>';
						}
						$("input[name=customer_id]").parent().find(".result").html(html);
						//$(".result").html(html);
					} else {
					}
				}
			});
		} else if(inputName == "supplier_id") {
			url = baseUrl + "ajax/supplier/searchSupplier/?supplier_name=" + $(this).val();
			$.ajax({
				url: url,
				dataType: "html",
				type: "GET",
				data: "",
				success: function( data ){
					data = $.parseJSON( data );
					if(data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							html += '<div ' +
									'class="item-result" ' +
									'data-name="' + convertVietnamese(data[i]['supplier_name']) + ' (' +
									data[i]['customer_id']
									+ ')" ' +
									'data-id="' + data[i]['supplier_id'] + '" ' +
									'data-display="' +
									data[i]['supplier_name']
									+ ' (' + data[i]['supplier_id'] + ')" ' +
									'data-input-name="supplier_id">' +
									data[i]['supplier_name'] + ' (' +
									data[i]['supplier_id']
									+ ')' + ' - ' + data[i]['phone'] + ' </div>';
						}
						$("input[name=supplier_id]").parent().find(".result").html(html);
						//$(".result").html(html);
					} else {
					}
				}
			});
		}

		tpSearch( $(this), $(this).parent().find(objectResultItem) );
	});
	$(".mask").click(function(){
		$( objectResult ).hide();
		$(".mask").hide();
	});
	$(objectCloseSearch).click(function(){
		$( objectResult ).hide();
		$(".mask").hide();
		//if($("input[name=customer_id]").val() > 0 || $("input[name=supplier_id]").val()) {
		//} else {
		//	$(".input-search").val("");
		//}
	});
	$(document).on("click", ".closeSearch", function(e){
		$( objectResult ).hide();
		$(".mask").hide();
	});
	$(document).on("click", ".item-result", function(e){
		$(".mask").hide();
		var id = $(this).attr('data-id');
		var value = $(this).attr('data-name');
		var display  = $(this).attr('data-display');
		var inputName = $(this).attr('data-input-name');
		//console.log( "ID:" + id );
		//console.log( "Name:" + value );
		$(this).parent().parent().find( objectInputSearch ).val( display );
		$( objectResult ).hide();
		$( "input[name=" + inputName + "]" ).val( id );

		//get address, phone...
		var customerID = id;
		var url = $('input[name=base_url]').val() + customerID;
		if( inputName != "addProduct_ProductID" )
			$.ajax({
				url: url,
				dataType: "html",
				type: "GET",
				data: "",
				success: function( data ){
					data = $.parseJSON( data );
					if( data == null ) {
						/* alert( "Không tìm kiếm được thông tin khách hàng." ); */
						$("input[name=address]").val( "" );
						$("input[name=district_id]").val( "" );
						$("input[name=city_id]").val( "" );
						$("input[name=country_id]").val( "" );
						$("input[name=tax_code]").val("");
						$("input[name=fax]").val("");
					} else {
						if( data['phone'].length > 0 )
							$("input[name=phone]").val( data['phone'] );
						$("input[name=address]").val( data['address'] );
						$("input[name=district_id]").val( data['district_name'] );
						$("input[name=city_id]").val( data['city_name'] );
						$("input[name=country_id]").val( data['country_name'] );
						$("input[name=tax_code]").val( data['tax_code'] );
						$("input[name=fax]").val( data['fax'] );
					}
				}
			});

		//add order to pay
		var value = $("#pay").attr("data-value");
		if(value == 1) {
			if(inputName == "customer_id") {
				var url = baseUrl + "ajax/saleOrder/getOrderHtmlByCustomerId/" + customerID;
			} else {
				var url = baseUrl + "ajax/buyOrder/getOrderHtmlBySupplierId/" + customerID;
			}
			$("table.tbl-order-detail tbody").load(url, function() {
				calculatorCustomerPay() // file add order to pay
			});
		}

		//lựa chọn nhà cung cấp => thay đổi select đơn giá mua hàng
		if(inputName == "supplier_id") {
			var value = $("select[name=buy_price_id]").val();
			if(value != undefined) {
				$("select[name=buy_price_id]").load(baseUrl + "ajax/buyPrice/getBuyPricesBySupplierId/" + customerID);
			}
		}
	});
});
/**
 *
 * @param objectInputSearch
 * @param objectResultItem
 */
function tpSearch( objectInputSearch, objectResultItem ) {
	var searchKey = $(objectInputSearch).val();
	searchKey = convertVietnamese( searchKey );
	$( objectResultItem ).each(function(index, element) {
		$(this).hide();
		if( isInString( $(this).attr('data-name'), searchKey ) == true )
			$(this).show();
	});
}
function isInString( str, value ) {
	var str = str;
	var index = str.indexOf( value );
	//console.log( str + ":" + value + ":" + index );
	if( index >= 0 ) {
		return true;
	}
	return false;
}
/**
 * Convert vn to en
 *
 * @param str
 * @returns
 */
function convertVietnamese(str) {
    str= str.toLowerCase();
    str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
    str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
    str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
    str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
    str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
    str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
    str= str.replace(/đ/g,"d");
    str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|$|_/g,"-");
    str= str.replace(/-+-/g,"-");
    str= str.replace(/^\-+|\-+$/g,"");
    return str;
}