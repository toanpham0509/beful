$(document).ready(function() {
    $("form").submit(function(){
        //validate input number
        var min;
        var max;
        var value;
        var isValid = true;
        $("input[type=number]").each(function() {
            min = $(this).attr("min");
            max = $(this).attr("max");
            value = $(this).val();
            if(value < min || value > max) {
                $(this).css({"border": "1px solid #F00"});
                isValid = false;
            } else {
                $(this).css({"border": "1px solid #000"});
            }
        });
        if(isValid == false) {
            alert("Thông tin các trường bôi đỏ không hợp lệ!");
            return false;
        }
    });
});