$(document).ready(function(e) {
	$("#btnAddProductToOrder").click(function() {
		var productID = $("input#addProduct_ProductID").val();
		var priceID = $("select[name=price_id]").val();
		//var productAmount = $("input#addProduct_ProductAmount").val();
		$.ajax({
			url: baseUrl + "ajax/product/getProduct/" + productID + "/" + priceID, 
			dataType:"html", 
			type: "GET",
			data:"", 
			success: function( data ){
				console.log(data);
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Mã sản phẩm không tồn tại." );
				} else {
					var html = "<tr>" + $( "#html_add_to_order table tr:first" ).html() + "</tr>";
					$("table.order_line tbody tr:last").before( html );
					$("table.order_line .html_product_name:last").html( data[ 'product_name' ] );
					$("table.order_line input.product_id:last").val( data[ 'product_id' ] );
					$('table.order_line input[name="product_price[]"]:last').val( data[ 'product_price' ] );
					$("table.order_line .html_product_id:last").html( data[ 'product_id' ] + "(" + data[ 'product_code' ] + ")" );
					$("table.order_line .html_product_unit:last").html( data[ 'unit_name' ] );
					$('table.order_line input[name="product_amount[]"]:last').val(0);
					
					if($("#transfer").val() == 1) {
						console.log("Transfer");
						var url = baseUrl + "ajax/product/getProductPriceInWarehouse/" 
							+ productID 
							+ "/" + $("select[name=warehouse_from_id]").val();
						//console.log(url);
						$.ajax({
							url: url, 
							dataType:"html", 
							type: "GET",
							data:"", 
							success: function( data ){
								data = $.parseJSON( data );
//								alert(data);
								$('table.order_line input[name="product_price[]"]:last').val( parseInt(data['product_price']) );
								$('table.order_line input[name="product_amount[]"]:last').after("Tồn kho:" + parseInt(data['inventory_amount']));
//								$('table.order_line input[name="product_amount[]"]:last').attr("max", parseInt(data['inventory_amount']));
							}
						});
					} else if($("#adjustInventory").val() == 1) {
						//Điều chỉnh tồn kho
						console.log("AdjustInventory");
						var url = baseUrl + "ajax/product/getProductPriceInWarehouse/" 
							+ productID 
							+ "/" + $("select[name=warehouse_id]").val();
						//console.log(url);
						$.ajax({
							url: url, 
							dataType:"html", 
							type: "GET",
							data:"", 
							success: function( data ){
								data = $.parseJSON( data );
								console.log(data);
								$('table.order_line .product_inventory_amount:last').html(parseInt(data['inventory_amount']));
								$('table.order_line input[name="product_price[]"]:last').val(data['product_price']);
//								$('table.order_line input[name="product_amount[]"]:last').attr("max", parseInt(data['inventory_amount']));
							}
						});
					} else if($("select[name=buy_price_id]").val() > 0) {
						//get product price
						console.log("Select buy price in buy order page");
						var url = baseUrl + "ajax/product/getProductBuyPrice/" 
							+ productID 
							+ "/" + $("select[name=buy_price_id]").val();
						//console.log(url);
						$.ajax({
							url: url, 
							dataType:"html", 
							type: "GET",
							data:"", 
							success: function( data ){
								data = $.parseJSON( data );
								$('table.order_line input[name="product_price[]"]:last').val(parseInt(data['product_price']));
							}
						});
					}
					$('.product_detail').css("display", "block");
				}
			}
		});
	});
	$("#btnAddProductToPrice").click(function(){
		var html = "<tr>" + $("#html_add_to_order table tr:first").html() + "</tr>";
		$("table.order_line tr:last").before(html);
	});
	$("#order_id").change(function(){
		var orderID = $(this).val();
		var url = baseUrl + "ajax/importOrder/getBuyOrderLineHtml/" + orderID;
		$( "table.order_line" ).html("").load( url );
		
		var url = baseUrl + "ajax/supplier/getSupplierByBuyOrderId/" + orderID;
		$.ajax({
			url: url,
			dataType: "html", 
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				insertSupplierInfomation(data);
			}
		});
	});
	$("#sale_order_id").change(function(){
		var orderID = $(this).val();
		if( orderID > 0 ) {
			var url = baseUrl + "ajax/importOrder/getSaleOrderLineHtmlNotPrice/" + orderID;
			$( "table.order_line" ).html("").load( url );
			
			var url = baseUrl + "ajax/customer/getCustomerByOrderId/" + orderID;
			$.ajax({
				url: url,
				dataType: "html", 
				type: "GET",
				data: "", 
				success: function( data ){
					data = $.parseJSON( data );
					insertCustomerInfomation(data);
				}
			});
		} else {
			$( "table.order_line tbody" ).html("");
			
		}
	});
	//cho phần mua hàng - chuyển kho, nhập kho
	$(".don-hang-mua").show();
	$(".don-hang-ban").hide();
	$("#supplier_id").change(function(){
		var supplierID = $(this).val();
		if( supplierID == 27 ) {
			$(".don-hang-ban").show();
			$(".don-hang-mua").hide();
		} else {
			$(".don-hang-ban").hide();
			$(".don-hang-mua").show();
		}
	});
	
	//Lựa chọn khách hàng => hiển thị thông tin khách hàng
	$("select[name=customer_id]").change(function(){
		var customerID = $( this ).val();
		var url = baseUrl + "ajax/customer/getCustomer/" + customerID;
		$.ajax({
			url: url, 
			dataType: "html", 
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Không tìm kiếm được thông tin khách hàng." );
					$("input[name=address]").val( "" );
					$("input[name=district_id]").val( "" );
					$("input[name=city_id]").val( "" );
					$("input[name=country_id]").val( "" );
				} else {
					$("input[name=address]").val( data['address'] );
					$("input[name=district_id]").val( data['district_name'] );
					$("input[name=city_id]").val( data['city_name'] );
					$("input[name=country_id]").val( data['country_name'] );
				}
			}
		});
	});
	
	//lựa chọn nhà cung cấp => hiển thị thông tin nhà cung cấp.
	$("select[name=supplier_id]").change(function(){
		var supplierID = $( this ).val();
		var url = baseUrl + "ajax/supplier/getSupplier/" + supplierID;
		$.ajax({
			url: url, 
			dataType: "html",
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					alert( "Không tìm kiếm được thông tin nhà cung cấp." );
					$("input[name=supplier_address]").val( "" );
					$("input[name=district_id]").val( "" );
					$("input[name=city_id]").val( "" );
					$("input[name=country_id]").val( "" );
					$("input[name=supplier_phone]").val( "" );
				} else {
					$("input[name=supplier_address]").val( data['address'] );
					$("input[name=district_id]").val( data['district_name'] );
					$("input[name=city_id]").val( data['city_name'] );
					$("input[name=country_id]").val( data['country_name'] );
					$("input[name=supplier_phone]").val( data['phone'] );
				}
			}
		});
	});
	
	//lựa chọn báo giá trong phần bán hàng => đơn hàng thì enable nút thêm sản phẩm
	$(".main_new_order select[name=price_id]").change(function(e){
		var value = $(this).val();
		if(value > 0 ) 
			$('#add_product').attr("data-price", "true");
		else
			$('#add_product').attr("data-price", "false");
	});
	
    //Lựa chọn báo giá trong phần đơn đặt hàng => enable nut thêm sản phẩm.
    $(".main_new_order select[name=buy_price_id]").change(function(e){
		var value = $(this).val();
		if(value > 0 ) 
			$('#add_product').attr("data-buy-price", "true");
		else
			$('#add_product').attr("data-buy-price", "false");
	});
    
	//Sử lý sự kiện người dùng khi click vào nút thêm sản phẩm ở phần đơn hàng bán + đơn đặt hàng
    $('#add_product').click(function () {
		if($(this).attr("data-price") != undefined && $(this).attr("data-price") == "false" ) {
			alert("Bạn chưa chọn báo giá! Hãy chọn báo giá trước khi thêm sản phẩm.");
			$('.product_detail').css("display", "none");
			return false;
		} else {
	        $('.product_detail').css("display", "block");
		}
		
		if($(this).attr("data-buy-price") != undefined && $(this).attr("data-buy-price") == "false" ) {
			alert("Bạn chưa chọn bảng giá! Hãy chọn báo giá trước khi thêm sản phẩm.");
			$('.product_detail').css("display", "none");
			return false;
		} else {
	        $('.product_detail').css("display", "block");
		}
    });
    
    //Khuyến mại cho phần bán hàng => đơn hàng
    $('select[name=promote_id]').change(function() {
		processPromoteSaleOrder($(this));
	});
    
	//thay đổi kho hàng trong phần quản lý điều chỉnh tồn kho
    $("select[name=warehouse_id]").change(function(e) {
    	if($("#adjustInventory").val() == 1) {
    		$('input[name="product_id[]"]').each(function(e){
    			var productId = $(this).val();
    			var object = $(this);
    			if(productId > 0) {
    				var url = baseUrl + "ajax/product/getProductPriceInWarehouse/" 
						+ productId 
						+ "/" + $("select[name=warehouse_id]").val();
					//console.log(url);
					$.ajax({
						url: url, 
						dataType:"html", 
						type: "GET",
						data:"", 
						success: function( data ){
//							alert($(object).val());
							console.info(data);
							data = $.parseJSON( data );
//							$('input[name=').closest( "tr" ).find('.product_inventory_amount').html(parseInt(data['inventory_amount']));
							$(object).closest( "tr" ).find('.product_inventory_amount').html(parseInt(data['inventory_amount']));
//							$(object).closest( "tr" ).find('input[name="product_amount[]"]').attr("max", parseInt(data['inventory_amount']));
						}
					});
    			}
    		});
    	}
    });
    //số dư đầu kỳ, cuối kỳ trong phần Kế toán => tk tiền mặt, tk ngân hàng.
    $("input.startSession").on('change', function() {
    	processSession();
    });
    $("input.startSession").keyup(function() {
    	processSession();
    });

	//cofirm submit
	$( "form" ).submit(function() {
		if(confirm( "Bạn có chắc chắn muốn lưu?" )) {
		} else {
			event.preventDefault();
		}
	});
});
/**
 * Hàm sử lý số dư đầu kỳ, cuối kỳ
 */
function processSession() {
	var startSession = $(".startSession").val();
	var inSession = $(".totalPrice").text();
	inSession = parseInt( inSession.replace( /\./g, '') );
	startSession = parseInt(startSession);
	var endSession = startSession + inSession;
	if(!Number.isInteger(endSession))
		endSession = 0;
		$("input.endSession").val(endSession);
}
/**
 * Hàm sử lý trong phần đơn hàng bán. Khi người dùng chưa chọn or thay đổi không chọn khuyến mại nữa.
 */
function notPromote() {
	console.log('Chưa được');
	$(".promote").html("");
	$("input[name=order_discount]").val(0);
	$('.totalDiscount').text(0);
	$(".finalTotal").text( $('.totalPrice').text() );
}
/**
 * Xóa 1 item
 * 
 * @param object
 * @returns {Boolean}
 */
function deleteOrderLine( object ) {
	if( confirm( 'Bạn có chắc chắn muốn xóa không?' ) ) {
		$(object).parent().parent().remove();
		calTotalPrice();
	} else {
		return false;
	}
}
/**
 * Cập nhật lại thông tin tổng tiền của từng item trong đơn hàng 
 * 
 * @param object
 */
var countCallPromoteProcess = 1;
var rowObjectChange = null;
function calTotalPriceItem( object ) {
	var parentObject = $( object ).closest( "tr" );
	rowObjectChange = parentObject;
//	alert(rowObjectChange.html());
	var productAmount = $(parentObject).find('input[name="product_amount[]"]').val();
	var productPrice = $(parentObject).find('input[name="product_price[]"]').val();
	var productDiscount = $(parentObject).find('input[name="product_discount[]"]').val();
	if( isNaN(parseInt(productDiscount)) ) {
		var orderLinePrice = productAmount * productPrice;
	} else {
		var orderLinePrice = parseInt( productAmount * productPrice * (100 - productDiscount ) / 100 );
		
		var a = productAmount * productPrice * productDiscount / 100;
		$(parentObject).find('.orderLineDiscount').html( "% =" + dauCham(a));
	}
	console.log( "Oparator order line:" + productAmount + " - " + productPrice + " -" + productDiscount );
	$(parentObject).find('.orderLinePrice').html( dauCham(orderLinePrice) );
	calTotalPrice();
	if($(object).attr("name") == "product_discount[]") {
		return;
	} else {
		console.log($(object).attr("name"));
	}
	//cập nhật lại thông tin khuyến mại
	if(countCallPromoteProcess == 1) {
		processPromoteSaleOrder($('select[name=promote_id]'));
	} else {
		countCallPromoteProcess = 1;
	}
}
/**
 * Cập nhật lại thông tin tổng tiền của cả đơn hàng.
 */
function calTotalPrice() {
	var total = 0;
	$(".orderLinePrice").each(function(index, element) {
        var value = $(this).text() + "";
		value = parseInt( value.replace( /\./g, '') );
		if( Number.isInteger(value) )
			total +=  value;
    });
	$('.totalPrice').text( dauCham(total) );
	
	$('.totalDiscount').text(dauCham(total * $("input[name=order_discount]").val() / 100));
	var discount = $('.totalDiscount').text();
	discount = parseInt( discount.replace( /\./g, '') );
	discount = parseInt(discount);

	var finalTotal = 0;
	if( !Number.isInteger(discount) )
		discount = 0;

	finalTotal =  total - discount;

	var discountAmount = parseInt($('input[name=discount_amount]').val());
	if( Number.isInteger(discountAmount) )
		finalTotal =  finalTotal - discountAmount;
	else
		finalTotal =  finalTotal;

	$(".finalTotal").text( dauCham(finalTotal) );
}
/**
 * Tính tổng giá trị phiếu thu, chi
 */
function calTotalPricePay() {
	var total = 0;
	var va = 0;
	$('input[name="money[]"]').each(function(index, element) {
		va = parseInt($(this).val());
		if( Number.isInteger(va) ) {
			total += va;
		}
    });
	$(".totalPricePay").text(dauCham(total));
}
/**
 * Sử lý các thông tin khuyến mại cho đơn hàng bán.
 * 
 * @param object
 */
function processPromoteSaleOrder(object) {
	console.log("Load promote for order!");
	var promoteId = $(object).val();
	if( promoteId == 0 ) {
		notPromote();
		return;
	}
	var url = baseUrl + "ajax/promote/getPromoteProducts/" + promoteId;
	$.ajax({
		url: url, 
		dataType: "html", 
		type: "GET",
		data: "", 
		success: function( data ){
			console.log(data);
			data = $.parseJSON( data );
			if( data == null ) {
			} else {
				var size = data.length;
				var is = 0;
				for( var i = 0; i < size; i++) {
					console.log( data[i] );
					$('input[name="product_id[]"]').each(function(index, element) {
                        var productId = $(this).val();
						var amount = $(this).closest( "tr" ).find('input[name="product_amount[]"]').val();
						if( productId == data[i]['product_id'] && amount >= data[i]['min_amount'] ) {
							if(rowObjectChange == null) {
								$(this).closest( "tr" ).find('input[name="product_discount[]"]').val( data[i]['discount'] );
							} else {
//								alert(rowObjectChange.html());
//								alert($(this).closest( "tr" ).html());
								if($(this).closest( "tr" ).is($(rowObjectChange)) ) {
									$(this).closest( "tr" ).find('input[name="product_discount[]"]').val( data[i]['discount'] );
								}
							}
							//calTotalPriceItem( $(this).closest( "tr" ).find('input[name="product_discount[]"]') );
						}
                    });
				}
				rowObjectChange = null;
			}
		}
	});
	var url = baseUrl + "ajax/promote/getPromoteOrders/" + promoteId;
	$.ajax({
		url: url, 
		dataType: "html", 
		type: "GET",
		data: "", 
		success: function( data ){
			data = $.parseJSON( data );
			var saleTotal = $(".totalPrice").text();
			saleTotal = saleTotal.replace(/\./g,""); 
			saleTotal = parseInt(saleTotal);
			if( data == null ) {
				notPromote();
			} else {
				var size = data.length;
				var is = 0;
				for( var i = 0; i < size; i++) {
					console.log( data[i] );
					if( 
						( saleTotal >= data[i]['order_price_from'] && data[i]['order_price_to'] == "" )  ||
						( saleTotal <= data[i]['order_price_to'] && data[i]['order_price_from'] == "" )  ||
						( saleTotal <= data[i]['order_price_to'] && saleTotal >= data[i]['order_price_from'] )
					) {
						var discount = data[i]['discount'];
						discount = parseInt(discount);
						if( isNaN(discount) ) {
							discount = 0;
						}
						$("input[name=order_discount]").val(discount);
						$('.totalDiscount').text( dauCham( saleTotal * discount / 100 ) );
						saleTotal = saleTotal * (100 - discount) / 100;
						$(".finalTotal").text( dauCham(saleTotal));
						var html = "Chiết khấu: " + discount + " %";
						//html += "<br />Được khuyến mại sản phẩm: <a target='_blank' href='"
						//	+ baseUrl 
						//	+ "ban-hang/san-pham/chinh-sua/"
						//	+ data[i]['promote_product_id'] +"'><b>" + data[i]['product_name'] + "</b></a>";
						console.log( html );
						$(".promote").html(html);
						is = 1;
						break;
					}
				}
				if( is == 0 ) {
					notPromote();
					return;
				}
			}
		}
	});
}
/**
* Thêm dấu chấm
*/
function dauCham( gia ) {
	var gia = gia + "";
	var stringReturn = "";
	var j = -1;
	for( var i = gia.length - 1; i >= 0 ; i-- ) {
		j++;
		if( j % 3 == 0 && j > 0 ) {
			stringReturn += "." + gia.charAt( i );
		} else {
			stringReturn += gia.charAt( i );
		}
	}
	return stringReturn.split("").reverse().join("");
}
/**
 * Chèn thông tin khách hàng
 * 
 * @param data
 */
function insertCustomerInfomation( data ) {
	$("input[name=customer_name]").val( data['customer_name'] );
	$("input[name=address]").val( data['address'] );
	$("input[name=district_id]").val( data['district_name'] );
	$("input[name=city_id]").val( data['city_name'] );
	$("input[name=country_id]").val( data['country_name'] );
	$("input[name=phone]").val( data['phone'] );
	
}
function insertSupplierInfomation(data) {
	$("input[name=supplier_name]").val( data['supplier_name'] );
	$("input[name=address]").val( data['address'] );
	$("input[name=district_id]").val( data['district_name'] );
	$("input[name=city_id]").val( data['city_name'] );
	$("input[name=country_id]").val( data['country_name'] );
	$("input[name=phone]").val( data['phone'] );
	$("input[name=supplier_id]").val( data['supplier_id'] );
	$("input[name=tax_code]").val( data['tax_code'] );
	$("input[name=fax]").val( data['fax'] );
}
function _equals(obj1, obj2) {
    return JSON.stringify(obj1)
        === JSON.stringify($.extend(true, {}, obj1, obj2));
}