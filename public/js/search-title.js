/* global baseUrl */

$(document).ready(function () {
    objSearch = $(".search-order input");
    $(objSearch).keyup(function (event) {
//        searchValue = $(this).val();
//        searchName = $(this).attr("name");
        var valOrderId = $(document.getElementsByName('order_id')).val();
        var valDateFrom = $(document.getElementsByName('date-from')).val();
        var valDateTo = $(document.getElementsByName('date-to')).val();
        var valCustomerName = $(document.getElementsByName('customer_name')).val();
        var valSaleEmployee = $(document.getElementsByName('sale_employee')).val();
        var valMoney = $(document.getElementsByName('money_number')).val();
        var valOrderStatus = $(document.getElementsByName('order_status')).val();
        var valProductId = $(document.getElementsByName('product_id')).val();
        var valProductName = $(document.getElementsByName('product_name')).val();
        var valProductCode = $(document.getElementsByName('product_qrcode')).val();
        var valUnit = $(document.getElementsByName('product_unit')).val();
        var valSaleoff = $(document.getElementsByName('saleoff')).val();
        var valStartDate = $(document.getElementsByName('start_date')).val();
        var valEndDate = $(document.getElementsByName('end_date')).val();
        var valPriceName = $(document.getElementsByName('price_name')).val();
        var valShopCode = $(document.getElementsByName('shop_code')).val();
        var valShopName = $(document.getElementsByName('shop_name')).val();
        var valShopAdd = $(document.getElementsByName('shop_address')).val();
        var valShopPhone = $(document.getElementsByName('shop_telephone')).val();
        var valWarehouse = $(document.getElementsByName('warehouse')).val();
        var valWarehouseManagement = $(document.getElementsByName('warehouse_management')).val();
        var valOrderNumberBuy = $(document.getElementsByName('order_number_buy')).val();
        var valProviders = $(document.getElementsByName('providers')).val();
        var valOrderdate = $(document.getElementsByName('order_date')).val();
        var valEstimateDateBuy = $(document.getElementsByName('estimate_date_buy')).val();
        var valCostnonTax = $(document.getElementsByName('cost_non_tax')).val();
        var valTotalPrice = $(document.getElementsByName('total_price')).val();
        var valStatus = $(document.getElementsByName('status')).val();
        var valImportID = $(document.getElementsByName('import_id')).val();
        var valTranferID = $(document.getElementsByName('tranfer_id')).val();
        var valWarehouseFrom = $(document.getElementsByName('warehouse_from')).val();
        var valWarehouseTo = $(document.getElementsByName('warehouse_to')).val();
        var valProvidersName = $(document.getElementsByName('providers_name')).val();
        var valProvidersAdd = $(document.getElementsByName('providers_address')).val();
        var valProviderPhone = $(document.getElementsByName('providers_phone')).val();
        var valCodeTax = $(document.getElementsByName('providers_code_tax')).val();
        var valProvidersContact = $(document.getElementsByName('providers_contact')).val();
        var valProvidersEmail = $(document.getElementsByName('providers_email')).val();
        var valProvidersTelephone = $(document.getElementsByName('providers_telephone')).val();
        var valPriceID = $(document.getElementsByName('price_id')).val();
        title = $(".nav-ban-hang .line h4").html();
//        alert(valOrderId + "\n" + title);
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $('.nav-right').css("display", "none");
            /*Bán Hàng*/
            if (title == 'Đơn bán hàng') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllOrder";
                var data = {
                    order_id: valOrderId,
                    date_from: valDateFrom,
                    date_to: valDateTo,
                    customer_name: valCustomerName,
                    sale_employee: valSaleEmployee,
                    money_number: valMoney,
                    order_status: valOrderStatus
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';

//                $.ajax({
//                    url: url,
//                    type: "get",
//                    data: {
//                        order_id: valOrderId
//                    },
//                    success: function (response) {
//                        $('table.tbl-main ').append(response);
//                    }
//                });

                $.post(url, data, success, dataType);
            }

            if (title == 'Sản phẩm') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllProduct";
                var data = {
                    product_id: valProductId,
                    product_name: valProductName,
                    product_qrcode: valProductCode,
                    product_unit: valUnit
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);

            }

            if (title == 'Khuyến mại / Giảm giá') {

            }

            if (title == 'Báo giá') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllPriceName";
                var data = {
                    price_name: valPriceName,
                    start_date: valStartDate,
                    end_date: valEndDate
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);

            }
            if (title == 'Hệ thống cửa hàng') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllShop";
                var data = {
                    shop_code: valShopCode,
                    shop_name: valShopName,
                    shop_address: valShopAdd,
                    shop_phone: valShopPhone,
                    warehouse: valWarehouse,
                    management: valWarehouseManagement
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            /*Mua Hàng*/
            if (title == 'Đơn đặt hàng') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllInputOrder";
                var data = {
                    order_number_buy: valOrderNumberBuy,
                    providers: valProviders,
                    order_date: valOrderdate,
                    estimate_date_buy: valEstimateDateBuy,
                    cost_non_tax: valCostnonTax,
                    total_price: valTotalPrice,
                    status: valStatus
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            if (title == 'Nhập kho') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllImportWarehouse";
                var data = {
                    date_from: valDateFrom,
                    date_to: valDateTo,
                    import_code: valImportID,
                    order_number_buy: valOrderNumberBuy,
                    providers: valProviders,
                    cost_non_tax: valCostnonTax,
                    total_price: valTotalPrice
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            if (title == 'Chuyển kho') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllTranfer";
                var data = {
                    date_from: valDateFrom,
                    date_to: valDateTo,
                    tranfer_id: valTranferID,
                    warehouse_from: valWarehouseFrom,
                    warehouse_to: valWarehouseTo
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            if (title == 'Hàng trả về') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllRefund";
                var data = {
                    date_from: valDateFrom,
                    date_to: valDateTo,
                    import_code: valImportID,
                    customer_name: valCustomerName,
                    order_number_buy: valOrderNumberBuy,
                    warehouse: valWarehouse
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            if (title == 'Nhà cung cấp') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllSupplier";
                var data = {
                    providers_name: valProvidersName,
                    providers_address: valProvidersAdd,
                    providers_phone: valProviderPhone,
                    providers_code_tax: valCodeTax,
                    providers_contact: valProvidersContact,
                    providers_email: valProvidersEmail,
                    providers_telephone: valProvidersTelephone
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            if (title == 'Bảng giá mua') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllBuyPrice";
                var data = {
                    price_name: valPriceName,
                    providers: valProviders,
                    start_date: valStartDate,
                    end_date: valEndDate
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
            /*Khách Hàng*/
            if (title == 'Khách hàng') {
                $('table.tbl-main tbody').css("display", "none");
                var url = baseUrl + "ajax/searchOrder/getAllCustomer";
                var data = {
                    customer_name: valCustomerName,
                    providers_address: valProvidersAdd,
                    providers_phone: valProviderPhone,
                    price_name: valPriceID,
                    providers_contact: valProvidersContact,
                    providers_telephone: valProvidersTelephone,
                    providers_email: valProvidersEmail
                };
                var success = function (response) {
                    $('table.tbl-main ').append(response);
                }
                var dataType = 'text';
                $.post(url, data, success, dataType);
            }
        }
        if (keycode == '8') {
            $('table.tbl-main tbody').removeAttr("style");
            $('tr#result_ajax').css("display", "none");
            $('tbody#result_ajax').css("display", "none");
            $('.nav-right').css("display", "block");
        }
    });
});
