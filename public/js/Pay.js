var payMoney = 0;
var showError = false;
function calculatorCustomerPay() {
	$('input[name="haveToPay[]"').each(function(index) {
		$(this).closest("tr").find('input[name="amount_paid[]"]').val(0);
		$(this).closest("tr").find('.text-money-owed').text(dauCham($(this).closest("tr").find('input[name="haveToPay[]"]').val()));
	});
	showError = true;
	if(payMoney == parseInt($("input[name=pay_money]").val())) {
		showError = false;
	}
	payMoney = parseInt($("input[name=pay_money]").val());
	if(payMoney > 0) {
		var x = 0;
		$('input[name="haveToPay[]"').each(function(index) {
			x += parseInt($(this).val());
		});
		if(x < payMoney && showError) {
			alert("Số tiền thanh toán phải nhỏ hơn hoặc bằng số tiền cần thanh toán.");
			$("input[name=pay_money]").css("border", "2px solid #F00");
			return false;
		} else {
			$("input[name=pay_money]").css("border", "1px solid #000");
		}
		$('input[name="haveToPay[]"').each(function(index) {
			haveToPay = parseInt($(this).val());
			if(payMoney > 0) {
				if(payMoney > haveToPay) {
					payMoney = payMoney - haveToPay;
					$(this).closest("tr").find('input[name="amount_paid[]"]').val(haveToPay);
					$(this).closest("tr").find('.text-money-owed').text("0");
				} else {
					$(this).closest("tr").find('input[name="amount_paid[]"]').val(payMoney);
					$(this).closest("tr").find('.text-money-owed').text(dauCham(haveToPay - payMoney));
					return false;
				}
			}
		});
	} else {
		$('input[name="haveToPay[]"').each(function(index) {
			var haveToPay = $(this).val();
			$(this).closest("tr").find('input[name="amount_paid[]"]').val("0");
			$(this).closest("tr").find('.text-money-owed').text(dauCham(haveToPay));
		});
	}
}

/**
 *
 * @returns {boolean}
 */
function checkPayMoney() {
	payMoney = parseInt($("input[name=pay_money]").val());
	if(payMoney > 0) {
		var x = 0;
		$('input[name="haveToPay[]"').each(function(index) {
			x += parseInt($(this).val());
		});
		if(x < payMoney) {
			alert("Số tiền thanh toán phải nhỏ hơn hoặc bằng số tiền cần thanh toán.");
			$("input[name=pay_money]").trigger('blur');
			return false;
		}
	}
}

/**
 *
 */
$(document).ready(function(e) {
	$("input[name=pay_money]").keyup(function() {
		calculatorCustomerPay();
	});
	$("input[name=pay_money]").change(function() {
		calculatorCustomerPay();
	});
	//add item to pay : phiếu thu, phiếu chi
	$("#addItemToPay").click(function(){
		var html = "<tr>" + $( "#htmlAddItemToPay table tr:first" ).html() + "</tr>";
		$("table.order_line tr:last").before(html);
	});
});
/*
function searchPartner(object) {
	alert("Hihi");
	objectResult = $(object).find(".result");
	alert(objectResult);
	objectCloseSearch = $(".closeSearch");
	var objectInputSearch = $('.tp-search-box .input-search');
	var objectResultItem = $(".tp-search-box .item-result");
	$( objectResult ).hide();
	//$( objectResultItem ).hide();
	$( objectInputSearch ).focus(function(){
		$(this).parent().find( objectResult ).show();
	});
	$( objectInputSearch ).blur(function(){
		//$( objectResult ).hide();
	});
	$( objectInputSearch ).keyup(function(){
		tpSearch( $(this), $(this).parent().find(objectResultItem) );
	});
	$(objectCloseSearch).click(function(){
		$( objectResult ).hide();
	});
	$( objectResultItem ).click(function() {
		var id = $(this).attr('data-id');
		var value = $(this).attr('data-name');
		var display  = $(this).attr('data-display');
		var inputName = $(this).attr('data-input-name');
		console.log( "ID:" + id );
		console.log( "Name:" + value );
		$(this).parent().parent().find( objectInputSearch ).val( display );
		$( objectResult ).hide();
		$( 'input[name="' + inputName + '"]' ).val( id );
		
		//get address, phone...
		var customerID = id;
		var url = $('input[name=base_url]').val() + customerID;
		if( inputName != "addProduct_ProductID" )
		$.ajax({
			url: url, 
			dataType: "html",
			type: "GET",
			data: "", 
			success: function( data ){
				data = $.parseJSON( data );
				if( data == null ) {
					// alert( "Không tìm kiếm được thông tin khách hàng." );
					$("input[name=address]").val( "" );
					$("input[name=district_id]").val( "" );
					$("input[name=city_id]").val( "" );
					$("input[name=country_id]").val( "" );
					$("input[name=tax_code]").val("");
					$("input[name=fax]").val("");
				} else {
					if( data['phone'].length > 0 )
						$("input[name=phone]").val( data['phone'] );
					$("input[name=address]").val( data['address'] );
					$("input[name=district_id]").val( data['district_name'] );
					$("input[name=city_id]").val( data['city_name'] );
					$("input[name=country_id]").val( data['country_name'] );
					$("input[name=tax_code]").val( data['tax_code'] );
					$("input[name=fax]").val( data['fax'] );
				}
			}
		});

		//add order to pay
		var value = $("#pay").attr("data-value");
		if(value == 1) {
			if(inputName == "customer_id") {
				var url = baseUrl + "ajax/saleOrder/getOrderHtmlByCustomerId/" + customerID;
			} else {
				var url = baseUrl + "ajax/buyOrder/getOrderHtmlBySupplierId/" + customerID;
			}
			$("table.tbl-order-detail tbody").load(url);
			calculatorCustomerPay() // file add order to pay
		}
	});
}
*/