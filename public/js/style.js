$(document).ready(function () {
    $('a.option-window').click(function () {
    	//lấy giá trị thuộc tính href - chính là phần tử "#option-box"
        var optionBox = $(this).attr('href');
        //cho hiện hộp đăng nhập trong 500ms
        $(optionBox).fadeIn(500);
        // thêm phần tử id="over" vào sau body
        $('<div id="over"></div>').appendTo('.main_order');
        $('.nav-left').css("display", "none");
        $('a.index').removeClass("list-item");
        $('a.option-window').addClass("option-active");
        $('#over').fadeIn(500);
        return false;
    });
    // khi click đóng hộp thoại
    $(document).on('click', "a.exit", function () {
        $('#over, .option').fadeOut(500, function () {
            $('#over').remove();
        });
        $('.nav-left').css("display", "block");
        $('a.index').addClass("list-item");
        $('a.option-window').removeClass("option-active");
        return false;
    });
    //click de tat search
    $(document).click(function (e)
    {
        // Đối tượng container chứa popup
        var container = $(".search-infor");

        // Nếu click bên ngoài đối tượng container thì ẩn nó đi
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $('.search-detail').addClass('non-display');
            $('.search_date').addClass('non-display');
            $('.date_1').addClass('non-display');
            $('.date_2').addClass('non-display');
        }
    });

    //lấy giá trị search
    $('.search-order a').click(function () {
        $('.search-detail').addClass('non-display');
        $('.search_date').addClass('non-display');
        $('.date_1').addClass('non-display');
        $('.date_2').addClass('non-display');
        var searchId = $(this).attr('id');
        var searchIcon = document.getElementsByClassName('' + searchId + '');
        var inputName = document.getElementsByName('' + searchId + '');
        if (searchId == 'date') {
            $('.search_date').removeClass('non-display');
        }
        if (searchId == 'order_date') {
            $('.date_1').removeClass('non-display');
        }
        if (searchId == 'estimate_date') {
            $('.date_2').removeClass('non-display');
        }
        if (searchId == 'start_date') {
            $('.date_1').removeClass('non-display');
        }
        if (searchId == 'end_date') {
            $('.date_2').removeClass('non-display');
        }
        $(inputName).removeClass('non-display');
        $(inputName).focus();
        $(searchIcon).removeClass('non-display');
        $(searchIcon).addClass('display-in-line');
        return false;
    });
//    cách xóa các search
    $('.icon-search a').click(function () {
        var searchId = $(this).attr('id');
        var searchIcon = document.getElementsByClassName('' + searchId + '');
        $(searchIcon).removeClass('display-in-line');
        $(searchIcon).addClass('non-display');
        return false;
    });

    //an hien grid va list
    $('#gird').click(function () {
        $('.product_grid').css("display", "block");
        $('.product_list').css("display", "none");
        $('#gird').addClass("list-item");
        $('#list').removeClass("list-item");
    });
    $('#list').click(function () {
        $('.product_list').css("display", "block");
        $('.product_grid').css("display", "none");
        $('#list').addClass("list-item");
        $('#gird').removeClass("list-item");
    });
    
//    $("body").load("http://www.lize.vn/api-get-news/?limit=5&page_=2");
});
 