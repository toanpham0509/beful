// JavaScript Document
$(document).ready(function(e) {
    $("select[name=buy_price_id]").change(function(e) {
        var priceId = $(this).val();
		$.ajax({
			url: baseUrl + "ajax/product/getJsonProductsInABuyPrice/" + priceId, 
			dataType:"html", 
			success: function(data){
				var productId;
				$(".add_product .input-search").val("");
				$(".add_product input[name=addProduct_ProductID]").val("");
				$(".add_product .tp-search-box .result .item-result").hide().each(function(index, element) {
                    productId = $(this).attr("data-id");
					if(data.indexOf( " " + productId + " ") >= 0)  {
						console.info(productId);
						$(this).show();
					}
                });
			}
		});
    });
});