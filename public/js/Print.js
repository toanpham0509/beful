$(document).ready(function(){
	$('.btnPrint').printPage({
		message:"Tài liệu đang được khởi tạo..."
	});

    $(".btnGetReportNewWindow").click(function(){
		object = $(this);
		var url = $(object).attr('href');
		var index = url.indexOf("?");
		if(index > 0)
			url = url.substring(0, index);
		var i = 0;
		$("input").each(function(index, element) {
			if( i == 0 )
				url += "?" + $(this).attr('name') + "=" + $(this).val();
			else
            	url += "&" + $(this).attr('name') + "=" + $(this).val();
				
			i++;
        });
		$("select").each(function(index, element) {
			if( i == 0 )
				url += "?" + $(this).attr('name') + "=" + $(this).val();
			else
            	url += "&" + $(this).attr('name') + "=" + $(this).val();
				
			i++;
        });
		$(object).attr('href', url);
	});
});