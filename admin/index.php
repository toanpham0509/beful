<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
/**
 * -------------------------
 * Core path
 * -------------------------
 */
define ( "CORE_FOLDER", "core" );
if (! is_dir ( CORE_FOLDER )) {
	die ( "Your system folder path does not appear to be set correctly." );
} else {
	define( "CORE_PATH", realpath( CORE_FOLDER ) . "/" );
}
/**
 * -------------------------
 * Application path
 * -------------------------
 * */
define ( "APP_FOLDER", "app" );
if ( !is_dir ( APP_FOLDER )) {
	die ( "Your application folder path does not appear to be set correctly." );
} else {
	define( "APP_PATH", realpath( APP_FOLDER ) . "/" );
}
/**
 * ------------------------
 * >> load bootstrap file
 * ------------------------
 * */
require_once( CORE_FOLDER . "/Bootstrap.php" );
/* End if file index.php */
/* Location ./index.php */