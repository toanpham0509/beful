<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * 
 * @var unknown
 */
$CONTROLLERS = array (
	"/" 								=> 					"Home",
	"phan-quyen-theo-chuc-vu"			=>					"RuleByPosition",
	"dang-nhap"							=>					"LogIn"
);
$METHODS = array (
	"/"									=> 					"index",
	"xem"								=>					"index"
);
/* End of file router.php */
/* Location: ./configs/router.php */