<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * AUTO-LOADER
 * 
 * This file specifies which systems should be loaded by default.
 *
 * In order to keep the framework as light-weight as possible only the
 * absolute minimal resources are loaded by default. For example,
 * the database is not connected to automatically since no assumption
 * is made regarding whether you intend to use it.  This file lets
 * you globally define which systems you would like loaded with every
 * request.
 *
 * Instructions
 *
 * These are the things you can load automatically:
 *
 * 1. Packages
 * 2. Libraries
 * 3. Helper files
 * 4. Language files
 * 5. Models
 *
*/
/**
 * >> auto load packages
 * 
 * @prototype		$autoload[ 'packages' ] = array ( 'package1's path ', 'package2's path', 'packages's path' )
 */
$autoload[ 'packages' ] = array (
);
/**
 * >> autoload libraries
 * 
 * @description		
 * @prototype		$autoload[ 'libraries' ] = array ( "css", "js", "mail" )
 * @notice 			This folder is located in the libraries folder
 * */
$autoload[ 'libraries' ] = array (
);
/**
 * >> autoload helper file
 * 
 * @property		$autoload[ 'helper' ] = array('url', 'file');
 * */
$autoload[ 'helpers' ] = array (
);
/**
 * >> autoload language file
 * 
 *  @property		$autoload[ 'languages' ]		
 * */
$autoload[ 'languages' ] =  array (
);
/**
 * >> autoload models
 * 
 * @property		$autoload[ 'models' ] = array();
 * */
$autoload[ 'models' ] = array (
);
/* End of file autoload.php */
/* Location: ./configs/autoload.php */