<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/*
 * Session Variables
 *
 * 'sess_cookie_name'		= the name you want for the cookie
 * 'sess_expiration'			= the number of SECONDS you want the session to last.
 * by default sessions last 7200 seconds (two hours). Set to zero for no expiration.
 * 'sess_expire_on_close'	= Whether to cause the session to expire automatically
 * when the browser window is closed
 * 'sess_encrypt_cookie'		= Whether to encrypt the cookie
 * 'sess_use_database'		= Whether to save the session data to a database
 * 'sess_table_name'			= The name of the session database table
 * 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
 * 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
 * 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
 * |
 */
define( "COOKIE_NAME", "bk_fw_session" );
define( "EXPIRATION", 7200 );
// define( "", "");
// define( "", "");
// define( "", "");
// define( "", "");
// define( "", "");
// $config ['sess_cookie_name'] = 'ci_session';
// $config ['sess_expiration'] = 7200;
// $config ['sess_expire_on_close'] = FALSE;
// $config ['sess_encrypt_cookie'] = FALSE;
// $config ['sess_use_database'] = FALSE;
// $config ['sess_table_name'] = 'ci_sessions';
// $config ['sess_match_ip'] = FALSE;
// $config ['sess_match_useragent'] = TRUE;
// $config ['sess_time_to_update'] = 300;

/*
 * |--------------------------------------------------------------------------
 * Cookie Related Variables
 * |--------------------------------------------------------------------------
 * |
 * 'cookie_prefix' = Set a prefix if you need to avoid collisions
 * 'cookie_domain' = Set to .your-domain.com for site-wide cookies
 * 'cookie_path' = Typically will be a forward slash
 * 'cookie_secure' = Cookies will only be set if a secure HTTPS connection exists.
 * |
 */
// $config ['cookie_prefix'] = "";
// $config ['cookie_domain'] = "";
// $config ['cookie_path'] = "/";
// $config ['cookie_secure'] = FALSE;

/*
 * |--------------------------------------------------------------------------
 * Global XSS Filtering
 * |--------------------------------------------------------------------------
 * |
 * Determines whether the XSS filter is always active when GET, POST or
 * COOKIE data is encountered
 * |
 */
// $config ['global_xss_filtering'] = FALSE;

/*
 * |--------------------------------------------------------------------------
 * Cross Site Request Forgery
 * |--------------------------------------------------------------------------
 * Enables a CSRF cookie token to be set. When set to TRUE, token will be
 * checked on a submitted form. If you are accepting user data, it is strongly
 * recommended CSRF protection be enabled.
 * |
 * 'csrf_token_name' = The token name
 * 'csrf_cookie_name' = The cookie name
 * 'csrf_expire' = The number in seconds the token should expire.
 */
// $config ['csrf_protection'] = FALSE;
// $config ['csrf_token_name'] = 'csrf_test_name';
// $config ['csrf_cookie_name'] = 'csrf_cookie_name';
// $config ['csrf_expire'] = 7200;
/*end of file session.php*/
/*location: ./configs/session.php*/