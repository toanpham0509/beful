<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );

//memcached
$vFastConfigGlobal["memcached"] =  array(
    array('127.0.0.1'=>'11211')
);

//Cache file

/**
 * >> cache folder path
 * 
 * @property	It is the directory where the cache file is stored
 */
define( "CACHE_FOLDER", "cache" );
/**
  * >> cache's status
  * 
  * @property	It is cache's status. true is turn on else false is turn off
  * @var		true or false
  * */
define( "CACHE_STATUS", false );
/**
 * >> cache time
 * 
 * @property	It is time to cache file is valid
 * */
define( "CACHE_TIME", 1200 );

 /*end of file cache.php */
 /*location: ./configs/cache.php*/