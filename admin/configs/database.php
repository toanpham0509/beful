<?php
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * 
 * @var unknown
 */
define( "DATABASE_TYPE", "mysql");
/**
 * 
 * @var unknown
 */
// define( "HOST", "ligker.net");
define( "HOST", "localhost");
/**
 * 
 * @var unknown
 */
// define( "DATABASE_NAME", "hoang_beful_2015");
define( "DATABASE_NAME", "beful");
/**
 * 
 * @var unknown
 */
// define( "USER_NAME", "hoang");
define( "USER_NAME", "root");
/**
 * 
 * @var unknown
 */
// define( "USER_PASSWORD", "hoang123");
define( "USER_PASSWORD", "");
/**
 * 
 * @var unknown
 */
// define("PORT", 1982);
define("PORT", null);
/*end of file database.php*/
/*location: ./bootstraps/database.php*/