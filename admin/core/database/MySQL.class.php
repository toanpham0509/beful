<?php
namespace	vFast\Core\Database;
use 		vFast\Core\VFast;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
class MySql extends Database {
	private $r;
	/**
	 * Construct
	 */
	public function __construct() {
		global $system;
		
		$this->cn = NULL;
		$this->from = NULL;
		$this->select = array();
		$this->having = NULL;
		$this->databaseName = NULL;
		$this->host = NULL;
		$this->hostType = NULL;
		$this->port = null;
		$this->limit = NULL;
		$this->port = 0;
		$this->result = array();
		$this->select = array();
		$this->userName = NULL;
		$this->userPasswork = NULL;
		$this->where = array();
		$this->orWhere = array();
		$this->groupBy = NULL;
		$this->order = NULL;
		$this->selectDistinct = FALSE;
		$this->query = NULL;
		$this->insertedID = 0;
		$this->setItem = array();
		$this->r = NULL;
		
		$this->host = VFast::$config->getHost();
		$this->databaseName = VFast::$config->getDatabaseName();
		$this->userName = VFast::$config->getUserName();
		$this->userPasswork = VFast::$config->getUserPassword();
		$this->port = VFast::$config->getPost();
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::connect()
	 */
	public function connect() {
		//Connect to db
		$this->cn = mysqli_connect( $this->host, $this->userName, $this->userPasswork, $this->databaseName, $this->port ) 
				or die ( "Can not connect to database!" );
		mysqli_set_charset( $this->cn, 'utf8' );
		//
		if( !$this->cn ) {
			$error = new Error("database");
			$error->setPageTitle( System::$lang->getString( "database_error" ) );
			$error->setHeaderMessage( System::$lang->getString( "database_error" ) );
			$error->setErrorMessage( System::$lang->getString( "error_when_connect_do_database" ) . " : " . mysqli_error( $this->cn ) );
			$error->showError();
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::disconnect()
	 */
	public function disconnect() {
		mysqli_close( $this->cn );
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::select()
	 */
	public function select( $dataSelect ){
		if( is_array( $dataSelect ) && !empty( $dataSelect ) ) {
			$arrayKey = array_keys( $dataSelect );
			$size = sizeof( $arrayKey );
			for( $i = 0; $i < $size; $i++ ) {
				if( filter_var( $arrayKey[ $i ], FILTER_VALIDATE_INT ) || $arrayKey[ $i ] == "0" ) {
					// data array item is index
					$this->select[] = " {$dataSelect[ $i ]}";
				} else {
					// data array item is key value
					$this->select[] = " {$arrayKey[ $i ]} AS {$dataSelect[ $arrayKey[ $i ] ]}";
				}
			}
		} else {
			$dataSelect = preg_split ( "/,/", $dataSelect );
			$this->select = array_merge( $this->select, $dataSelect );
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::selectMax()
	 */
	public function selectMax( $columnName, $asName = NULL ) {
		if( $asName == NULL )
			$this->select[] = " MAX($columnName) ";
		else 
			$this->select[] = " MAX($columnName) as $asName ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::selectMin()
	 */
	public function selectMin( $columnName, $asName = NULL ) {
		if( $asName == NULL )
			$this->select[] = " MIN($columnName) ";
		else
			$this->select[] = " MIN($columnName) as $asName ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::selectAvg()
	 */
	public function selectAvg( $columnName, $asName = NULL ) {
		if( $asName == NULL )
			$this->select[] = " AVG($columnName) ";
		else
			$this->select[] = " AVG($columnName) as $asName ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::selectSum()
	 */
	public function selectSum( $columnName, $asName = NULL ) {
		if( $asName == NULL )
			$this->select[] = " SUM($columnName) ";
		else
			$this->select[] = " SUM($columnName) as $asName ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::selectCount()
	 */
	public function selectCount( $columnName, $asName = NULL ) {
		if( $asName == NULL )
			$this->select[] = " COUNT($columnName) ";
		else
			$this->select[] = " COUNT($columnName) as $asName ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::from()
	 */
	public function from( $tableName, $asTableName = NULL ) {
		if( $asTableName == NULL ) {
			$this->from[] = " $tableName";
		} else { 
			$this->from[] = " $tableName AS $asTableName ";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::join()
	 */
	public function join( $tableName, $clause, $option = NULL ) {
		$option = strtoupper( $option );
		if( 	$option !== "RIGHT" || 
				$option !== "LEFT" ||
				$option !== "OUTER" ||
				$option !== "INNER" ||
				$option !== "LEFT OUTER" ||
				$option !== "RIGHT OUTER" 
				) {
			$option = NULL;
		}
		$this->join[] = " $option JOIN $tableName ON $clause";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::where()
	 */
	public function where( $key, $value = NULL, $operator = NULL ) {
		if( $value !== NULL ) {
			if( $operator == NULL || ( $operator !== NULL  && 
				$operator !== "<"  && 
				$operator !== "<=" &&
				$operator !== ">"  &&
				$operator !== ">=" &&
				$operator !== "="  &&
				$operator !== "!="
				)
			) {
				$operator = "=";
			}
			$this->where[] = " $key $operator \"$value\" ";
		} elseif( is_array( $key ) && !empty( $key ) ) {
			$arrayKey = array_keys( $key );
			$size = sizeof( $arrayKey );
			for( $i = 0; $i < $size; $i++ ) {
				if( filter_var( $arrayKey[ $i ], FILTER_VALIDATE_INT ) || $arrayKey[ $i ] == "0" ) {
					// data array item is index
					$this->where[] = " {$key[ $i ]} ";
				} else {
					// data array item is key value
					$this->where[] = " {$arrayKey[ $i ]} = '{$key[ $arrayKey[ $i ] ]}' ";
				}
			}
		} else {
			$this->where[] = $key;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orWhere()
	 */
	public function orWhere( $key, $value = NULL, $operator = NULL ) {
	if( $value !== NULL ) {
			if( $operator == NULL || ( $operator !== NULL  && 
				$operator !== "<"  && 
				$operator !== "<=" &&
				$operator !== ">"  &&
				$operator !== ">=" &&
				$operator !== "="  &&
				$operator !== "!="
				)
			) {
				$operator = "=";
			}
			$this->orWhere[] = " $key $operator $value ";
		} elseif( is_array( $key ) && !empty( $key ) ) {
			$arrayKey = array_keys( $key );
			$size = sizeof( $arrayKey );
			for( $i = 0; $i < $size; $i++ ) {
				if( filter_var( $arrayKey[ $i ], FILTER_VALIDATE_INT ) || $arrayKey[ $i ] == "0" ) {
					// data array item is index
					$this->orWhere[] = " {$key[ $i ]}";
				} else {
					// data array item is key value
					$this->orWhere[] = " {$arrayKey[ $i ]} = \"{$key[ $arrayKey[ $i ] ]}\"";
				}
			}
		} else {
			$this->orWhere[] = $key;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::whereIn()
	 */
	public function whereIn( $key, $dataIn ) {
		if( is_array( $dataIn ) ) {
			$in = implode( " ,", $dataIn);
		 	$this->where[] = " $key IN( $in ) ";	
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orWhereIn()
	 */
	public function orWhereIn( $key, $dataIn ) {
		if( is_array( $dataIn ) ) {
			$in = implode( " ,", $dataIn);
			$this->orWhere[] = " $key IN( $in ) ";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::whereNotIn()
	 */
	public function whereNotIn($key, $dataIn ) {
		if( is_array( $dataIn ) ) {
			$in = implode( " ,", $dataIn);
			$this->where[] = " $key NOT IN( $in ) ";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orWhereNotIn()
	 */
	public function orWhereNotIn( $key, $dataIn ) {
		if( is_array( $dataIn ) ) {
			$in = implode( " ,", $dataIn);
			$this->orWhere[] = " $key NOT IN( $in ) ";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::like()
	 */
	public function like( $key, $value, $option = NULL ) {
		if( $option !== NULL ) {
			$option = strtolower( $option );
			switch ( $option ) {
				case "both": {
					$this->where[] = " $key LIKE \"%$value%\"";
					break;
				}
				case "after": {
					$this->where[] = " $key LIKE \"$value%\"";
					break;
				}
				case "before": {
					$this->where[] = " $key LIKE \"%$value\"";
					break;
				}
				default: {
					$this->where[] = " $key LIKE \"$value\"";
				}
			}
		} else {
			$this->where[] = " $key LIKE \"%$value%\"";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orLike()
	 */
	public function orLike( $key, $value, $option = NULL ) {
		if( $option !== NULL ) {
			$option = strtolower( $option );
			switch ( $option ) {
				case "both": {
					$this->orWhere[] = " $key LIKE \"%$value%\"";
					break;
				}
				case "after": {
					$this->orWhere[] = " $key LIKE \"$value%\"";
					break;
				}
				case "before": {
					$this->orWhere[] = " $key LIKE \"%$value\"";
					break;
				}
				default: {
					$this->orWhere[] = " $key LIKE \"$value\"";
				}
			}
		} else {
			$this->orWhere[] = " $key LIKE \"%$value%\"";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::notLike()
	 */
	public function notLike( $key, $value, $option = NULL ) {
		if( $option !== NULL ) {
			$option = strtolower( $option );
			switch ( $option ) {
				case "both": {
					$this->where[] = " $key NOT LIKE \"%$value%\"";
					break;
				}
				case "after": {
					$this->where[] = " $key NOT LIKE \"$value%\"";
					break;
				}
				case "before": {
					$this->where[] = " $key NOT LIKE \"%$value\"";
					break;
				}
				default: {
					$this->where[] = " $key NOT LIKE \"$value\"";
				}
			}
		} else {
			$this->where[] = " $key NOT LIKE \"%$value%\"";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orNotLike()
	 */
	public function orNotLike( $key, $value, $option = NULL ) {
		if( $option !== NULL ) {
			$option = strtolower( $option );
			switch ( $option ) {
				case "both": {
					$this->orWhere[] = " $key NOT LIKE \"%$value%\"";
					break;
				}
				case "after": {
					$this->orWhere[] = " $key NOT LIKE \"$value%\"";
					break;
				}
				case "before": {
					$this->orWhere[] = " $key NOT LIKE \"%$value\"";
					break;
				}
				default: {
					$this->orWhere[] = " $key NOT LIKE \"$value\"";
				}
			}
		} else {
			$this->orWhere[] = " $key NOT LIKE \"%$value%\"";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::groupBy()
	 */
	public function groupBy( $data ) {
		if( is_array( $data ) && !empty( $data ) ) {
			$this->groupBy = implode( " ,", $data );
		} elseif( !is_array( $data ) ) {
			$this->groupBy = $data;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::distinct()
	 */
	public function distinct() {
		$this->selectDistinct = TRUE;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::having()
	 */
	public function having( $key, $value, $operator = NULL) {
		if( $value !== NULL ) {
			if( $operator == NULL || ( $operator !== NULL  && 
				$operator !== "<"  && 
				$operator !== "<=" &&
				$operator !== ">"  &&
				$operator !== ">=" &&
				$operator !== "="  &&
				$operator !== "!="
				)
			) {
				$operator = "=";
			}
			$this->having[] = " $key $operator $value ";
		} elseif( is_array( $key ) && !empty( $key ) ) {
			$arrayKey = array_keys( $key );
			$size = sizeof( $arrayKey );
			for( $i = 0; $i < $size; $i++ ) {
				if( filter_var( $arrayKey[ $i ], FILTER_VALIDATE_INT ) || $arrayKey[ $i ] == "0" ) {
					// data array item is index
					$this->having[] = " {$key[ $i ]}";
				} else {
					// data array item is key value
					$this->having[] = " {$arrayKey[ $i ]} = {$key[ $arrayKey[ $i ] ]}";
				}
			}
		} else {
			$this->having[] = $key;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::orderBy()
	 */
	public function order( $columnName, $option = NULL ) {
		$option = ( $option == NULL ) ? "ASC" : "DESC";
		$this->order = " ORDER BY $columnName $option ";
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::limit()
	 */
	public function limit( $limit, $start = 0 ) {
		if( filter_var( $limit, FILTER_VALIDATE_INT ) ) {
			if( filter_var( $start, FILTER_VALIDATE_INT ) ) {
				$this->limit = " LIMIT $start, $limit ";
			} else {
				$this->limit = " LIMIT $limit ";
			}
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::query()
	 */
	public function query( $queryString = NULL ) {
		if( $queryString !== NULL ) {
			$this->query = $queryString;
		}
		$r = mysqli_query( $this->cn, $this->query ) or die( mysqli_error( $this->cn ) );
		
		$this->result = array();
		if( mysqli_num_rows( $r ) > 0 ) {
			while( ( $this->result[] = mysqli_fetch_array( $r, MYSQLI_ASSOC ) ) !== NULL ) {
			}
			unset( $this->result[ sizeof( $this->result ) - 1 ] );
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::getResult()
	 */
	public function getResult() {
		return $this->result;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::get()
	 */
	public function get( $tableName = NULL ) {
		if( $tableName !== NULL  ){
			$this->from[] = $tableName;
		}
		//create query string
		$this->createQueryStringSelect();
		//execute
		$this->query();
		$this->select = array();
		$this->where = array();
		$this->from = array();
		$this->groupBy = "";
		return $this->result;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::countResult()
	 */
	public function countResult() {
		return sizeof( $this->result );
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::setItem()
	 */
	public function setItem( $key, $value = NULL ) {
		if( is_array( $key ) && !empty( $key ) ) {
			$arrayKey = array_keys( $key );
			$size = sizeof( $arrayKey );
			for( $i = 0; $i < $size; $i++ ) {
				if( filter_var( $arrayKey[ $i ], FILTER_VALIDATE_INT ) || $arrayKey[ $i ] == "0" ) {
				} else {
					// data array item is key value
					$this->setItem[ "{$arrayKey[ $i ]}" ] = "\"{$key[ $arrayKey[ $i ] ]}\"";
				}
			}
		} elseif( !is_array( $key ) && $value !== NULL ) {
			$this->setItem[ "$key" ] = "\"" . $value . "\"";
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::insert()
	 */
	public function insert( $tableName, $dataInsert = NULL ) {
		if( $dataInsert !== NULL && is_array( $dataInsert ) ) {
			$this->setItem( $dataInsert );
		}
		// build query excute
		$columns = "(" . implode( ",", array_keys( $this->setItem ) ) . ")";
		$values  = "(" . implode( ",", array_values( $this->setItem ) ) . ")";
		$this->query = "INSERT INTO $tableName $columns VALUES $values";
		mysqli_query( $this->cn, $this->query );
		$this->insertedID = $this->insertedID();
		$this->setItem = array();
		return $this->insertedID;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::insertedID()
	 */
	public function insertedID() {
		return mysqli_insert_id( $this->cn );
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::update()
	 */
	public function update( $tableName, $dataUpdate = NULL ) {
		if( $dataUpdate !== NULL && is_array( $dataUpdate ) ) {
			$this->setItem( $dataUpdate );
		}
		$str = "";
		$i = 0;
		foreach( $this->setItem AS $key => $value ) {
			if($i == 0 )
				$str = " $key = $value ";
			else 
				$str .= ", $key = $value ";
			$i++;
		}
		$this->query = "UPDATE $tableName SET $str " . $this->buildStringWhere() . $this->limit;
		try {
			mysqli_query( $this->cn, $this->query );
			$this->setItem = array();
			$this->where = array();
			$this->from = array();
			
			return true;
		} catch( Exception $e ) {
			return false;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::delete()
	 */
	public function delete( $tableName, $dataCondition = NULL ) {
		if( $dataCondition !== NULL ) $this->where( $dataCondition );
		$this->query = "DELETE FROM $tableName " . $this->buildStringWhere() . $this->limit;
		try {
			mysqli_query( $this->cn, $this->query );
			$this->setItem = array();
			$this->where = array();
			$this->from = array();
			return  true;
		} catch( Exception $e ) {
			return false;
		}
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::emptyTable()
	 */
	public function emptyTable( $tableName ) {
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::createQueryString()
	 */
	public function createQueryStringSelect() {
		if( $this->selectDistinct == true ) {
			$this->query = "SELECT DISTINCT ";
		} else {
			$this->query = "SELECT ";
		}
		//select
		$size = sizeof( $this->select );
		if( $size == 0 ) {
			$this->query .= " * ";
		} else {
			$size --;
			for( $i = 0; $i < $size; $i++ ) {
				$this->query .= " " . $this->select[$i] . ", " ;
			}
			$this->query .= $this->select[ $i ];
		}
		//from
		$size = sizeof( $this->from );
		if( $size == 0 ) {
			$this->query = "";
			return FALSE;
		} else {
			$size --;
			$this->query .= " FROM ";
			for( $i = 0; $i < $size; $i++ ) {
				$this->query .= " " . $this->from[$i] . ", " ;
			}
			$this->query .= $this->from[ $i ];
		}
		//join
		$size = sizeof( $this->join );
		if( $size == 0 ) {
		} else {
			$size --;
			for( $i = 0; $i < $size; $i++ ) {
				$this->query .= " " . $this->join[$i] . ", " ;
			}
			$this->query .= $this->join[ $i ];
		}
		//where
		$this->query .= $this->buildStringWhere();
		//Group by
		if( strlen( $this->groupBy ) )
			$this->query .= " GROUP BY " . $this->groupBy;
		//Order by
		if( strlen( $this->order ) ) {
			$this->query .= $this->order;
		}
		//Limit
		if( strlen( $this->limit ) ) {
			$this->query .= $this->limit;
		}
		//Having
		if( !empty( $this->having ) ) {
			$this->query .= " HAVING ";
			$size = sizeof( $this->having );
			$size --;
			for( $i = 0; $i < $size; $i++ ) {
				$this->query .=  $this->having[$i] . ", ";
			}
			$this->query .= $this->having[$i];
		}
		return  true;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::getQueryString()
	 */
	public function getQueryString() {
		return $this->query;
	}
	/**
	 * (non-PHPdoc)
	 * @see \BKFW\Bootstraps\Database\Database::getConnect()
	 */
	public function getConnect() {
		return $this->cn;
	}
	/**
	 * 
	 */
	private function buildStringWhere() {
		//where
		$str = " WHERE ";
		if( empty( $this->where ) ) {
			if( empty( $this->orWhere ) ) {
				$str .= " 1";
			} else {
				$str .= $this->orWhere[0];
				$size = sizeof( $this->where );
				for ( $i = 1; $i < $size; $i++ ) {
					$str .= " OR " . $this->orWhere[ $i ];
				}
			}
		} else {
			$str .= $this->where[0];
			$size = sizeof( $this->where );
			for ( $i = 1; $i < $size; $i++ ) {
				$str .= " AND " . $this->where[ $i ];
			}
			// or where
			$size = sizeof( $this->orWhere );
			for ( $i = 0; $i < $size; $i++ ) {
				$str .= " OR " . $this->orWhere[ $i ];
			}
		}
		return $str;
	}
}
/*end of file MySQL.class.php*/
/*location: ./bootstraps/database/MySQL.class.php*/