<?php
namespace 		vFast\Core\Helpers;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
class Validate {
	/**
	 * 
	 * @param unknown $input
	 * @return string
	 */
	public function validate( $input ) {
		return strip_tags( $input );
	}

	/**
	 * @param $input
	 * @return bool
	 */
	public function isValid( $input ) {
		$input = strip_tags( $input );
		if( strlen( $input ) > 0 ) return true;
		return false;
	}
	/**
	 * 
	 * @param unknown $input
	 * @return mixed
	 */
	public function isValidEmail( $input ) {
		return filter_var( $input, FILTER_VALIDATE_EMAIL );
	}
	/**
	 * 
	 * @param unknown $input
	 * @return number
	 */
	public function isValidPhone( $input ) {
		return preg_match("/^[0-9]{2,3}[0-9]{4}[0-9]{4}$/", $input);
	}
}
/*end of file Validate.class.php*/
/*location: Validate.class.php*/