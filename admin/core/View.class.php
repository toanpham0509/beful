<?php
namespace          vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class View
 * @package     vFast\Core
 */
class View {

    /**
     * @var View
     */
    static private $instance = null;

    /**
     * @var     array
     */
    private $data;

    /**
     * @var     String
     * @access  public
     */
    private $title;

    /**
     * @var     String
     * @access  private
     */
    private $layoutName;

    /**
     * @var     String
     * @access  private
     */
    private $layoutPath;

    /**
     * @var     String
     * @access  private
     */
    private $viewPath;

    /**
     * View constructor.
     */
    private function __construct() {
        $this->setLayoutName("default");
    }

    /**
     * @return View
     */
    public static function getInstance() {
        if(View::$instance == null) {
            View::$instance = new View();
        }
        return View::$instance;
    }

    /**
     *
     */
    public function generateView() {
        $this->setViewPath(
            VFast::$config->getControllerClass() . "/"
            . VFast::$config->getControllerMethod()
        );

        if(VFast::$load->isFileExists($this->getLayoutPath())) {
            include_once($this->getLayoutPath());
            VFast::$load->newLoadedFile($this->getLayoutPath());
        }
    }

    /**
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData ($data) {
        $this->data = $data;
    }

    /**
     * @param       $data
     * @param       null        $value
     */
    public function setDataItem ($data, $value = null) {
        if($value != null && is_string($data)) {
            $this->data[$data] = $value;
        }
    }

    /**
     * @param $key
     * @return null
     */
    public function getDataItem($key) {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }
    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title = null) {
        $this->title = $title;
    }

    /**
     * @return String
     */
    public function getLayoutName() {
        return $this->layoutName;
    }

    /**
     * @param String $LayoutName
     */
    public function setLayoutName($layoutName = null) {
        $this->layoutName = $layoutName;

        $this->setLayoutPath(
            VFast::$config->getTemplatePath()
            . "layout/" . $this->getLayoutName() . ".php"
        );
    }

    /**
     * @return String
     */
    public function getLayoutPath() {
        return $this->layoutPath;
    }

    /**
     * @param String $layoutPath
     */
    private function setLayoutPath($layoutPath) {
        $this->layoutPath = $layoutPath;
    }

    /**
     * @return String
     */
    public function getViewPath() {
        return $this->viewPath;
    }

    /**
     * @param String $viewPath
     */
    public function setViewPath($viewPath) {
        $this->viewPath = $viewPath;
    }

    /**
     * Load view
     */
    public function loadView() {
        $this->loadComponent($this->getViewPath());
    }

    /**
     * @param       $componentPath
     */
    public function loadComponent($componentPath) {
        $componentPath = VFast::$config->getTemplatePath()
            . "components/" . $componentPath . ".php";
        if(VFast::$load->isFileExists($componentPath)) {
            include_once ($componentPath);
            VFast::$load->newLoadedFile($componentPath);
        }
    }
}
/*end of file View.class.php*/
/*location: View.class.php*/