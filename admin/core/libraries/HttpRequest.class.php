<?php
namespace 		vFast\Core\Libraries;
use				vFast\Core\Libraries\HttpRequest\Requests;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 * 
 */
if (! defined ( 'CORE_PATH' )) exit ( "No direct script access allowed" );
$filePathRe = dirname(__FILE__) . "/HttpRequest/Requests.class.php";
if( file_exists( $filePathRe ) ) require_once ( $filePathRe );
/**
 * Class HttpRequest
 * @package vFast\Core\Libraries
 */
class HttpRequest {
	/**
	 * POST method
	 *
	 * @var string
	 */
	const POST = 'POST';
	
	/**
	 * PUT method
	 *
	 * @var string
	 */
	const PUT = 'PUT';
	
	/**
	 * GET method
	 *
	 * @var string
	 */
	const GET = 'GET';
	
	/**
	 * HEAD method
	 *
	 * @var string
	 */
	const HEAD = 'HEAD';
	
	/**
	 * DELETE method
	 *
	 * @var string
	 */
	const DELETE = 'DELETE';
	
	/**
	 * PATCH method
	 *
	 * @link http://tools.ietf.org/html/rfc5789
	 * @var string
	 */
	const PATCH = 'PATCH';
	
	/**
	 * Current version of Requests
	 *
	 * @var string
	 */
	const VERSION = '1.6';
	/**
	 * 
	 * @var unknown
	 */
	private $requestMethod;
	/**
	 * 
	 * @var unknown
	 */
	private $server;
	/**
	 * 
	 * @var unknown
	 */
	private $data;
	/**
	 * 
	 */
	private $dataGet;
	/**
	 * 
	 * @var unknown
	 */
	private $response;
	/**
	 * 
	 */
	public function __construct(){
		Requests::register_autoloader();
		$this->data = array();
		$this->dataGet = null;
		$this->requestMethod = self::POST;
		$this->response = "";
		$this->server  = "";
	}
	/**
	 * 
	 * @param unknown $requestMethod
	 */
	public function setMethod( $requestMethod ) {
		$requestMethod = strtoupper( $requestMethod );
		if( $requestMethod == HttpRequest::POST ) {
			$this->requestMethod = HttpRequest::POST;
		} elseif( $requestMethod == HttpRequest::GET ) {
			$this->requestMethod = HttpRequest::GET;
		}
	}
	/**
	 * Get medthod
	 * 
	 * @return \BKFW\Libraries\unknown
	 */
	public function getMethod() {
		return $this->requestMethod;
	}
	/**
	 * 
	 * @param unknown $server
	 */
	public function setServer( $server ) {
		$this->server = $server;
	}

	/**
	 * @return string|unknown
	 */
	public function getServer() {
		return $this->server;
	}

	/**
	 * 
	 * @param unknown $data
	 * @param string $value
	 */
	public function setData( $data, $value = null ) {
		if( $value == null ) {
			if( is_array( $data ) ) {
				$this->data = array_merge( $this->data, $data );
			}
		} else {
			if( !is_array( $data ) ) {
				$this->data[ $data ] = $value;
			}
		}
	}
	/**
	 * 
	 * @param string $data
	 */
	public function send( $data = null ) {
		$this->setData( $data );
		$response = "";
		if( $this->requestMethod == HttpRequest::POST ) $response = $this->post();
		if( $this->requestMethod == HttpRequest::GET ) $response = $this->get();
		$this->data = array();
		return $response;
	}
	/**
	 * 
	 * @param string $data
	 * @return \BKFW\Libraries\unknown
	 */
	public function post( $data = null ) {
		$this->setData( $data );
		try {
			$this->response = Requests::post( $this->server, array(), $this->data );
			return $this->response->body;
		} catch (\Exception $e) {
			echo "Can not connect to internet! Error: " . $e->getMessage();
			exit();
		}
	}
	/**
	 * 
	 * @param string $data
	 * @return string
	 */
	public function get( $data = null ) {
		$this->setData( $data );
		$this->genDataToGetRequest();
		if(strpos($this->server, "?") > 0) {
			$url = $this->server . "&" . $this->dataGet;
		} else {
			$url = $this->server . "?" . $this->dataGet;
		}
		try {
			$this->response = Requests::get( $url, array('Accept' => 'application/json'));
			return $this->response->body;
		} catch (\Exception $e) {
			echo "Can not connect to internet! Error: " . $e->getMessage();
			exit();
		}
	}
	/**
	 * 
	 * @return \BKFW\Libraries\unknown
	 */
	public function getResponse() {
		return $this->response;
	}
	private function genDataToGetRequest( $data = null ) {
		$this->setData( $data );
		$size = sizeof( $this->data );
		if( $size > 0 ) {
			foreach ( $this->data as $k => $v ) {
				if( is_array( $v ) ) continue;
				$this->dataGet .= $k . "=" . $v . "&";
			}
			if( strlen( $this->dataGet ) ) {
				$this->dataGet = substr( $this->dataGet, 0, strlen( $this->dataGet ) - 1);
			}
			$this->dataGet = str_replace( " ", "%20", $this->dataGet );
		}
	}

	/**
	 * @return unknown
	 */
	public function getRequestMethod() {
		return $this->requestMethod;
	}

	/**
	 * @param unknown $requestMethod
	 */
	public function setRequestMethod($requestMethod) {
		$this->requestMethod = $requestMethod;
	}

	/**
	 * @return mixed
	 */
	public function getDataGet() {
		return $this->dataGet;
	}

	/**
	 * @param mixed $dataGet
	 */
	public function setDataGet($dataGet) {
		$this->dataGet = $dataGet;
	}
}
/*end of file HttpRequest.class.php*/
/*location: HttpRequest.class.php*/