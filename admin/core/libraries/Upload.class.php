<?php
namespace		vFast\Core\Libraries;
use 			BKFW\Bootstraps\System;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 * 
 */
if (! defined ( 'CORE_PATH' )) exit ( "No direct script access allowed" );
class Upload {
	/**
	 * @var		int
	 * @access	private
	 */
	private $maxSize;
	/**
	 * @var		int
	 * @access	private
	 */
	private $maxFileName;
	/**
	 * @var		string
	 * @access	private
	 */
	private $allowedTypes;
	/**
	 * @var		string
	 * @access	private
	 */
	private $fileTemp;
	/**
	 * @var		string
	 * @access	private
	 */
	private $filePath;
	/**
	 * @var		string
	 * @access	private
	 */
	private $fileName;
	/**
	 * @var		string
	 * @access	private
	 */
	private $fileType;
	/**
	 * @var		string
	 * @access	private
	 */
	private $fileSize;
	/**
	 * @var		string
	 * @access	private
	 */
	private $fileExtention;
	/**
	 * @var		string
	 * @access	private
	 */
	private $encryptName;
	/**
	 * @var		string
	 * @access	private
	 */
	private $uploadFolder;
	/**
	 * @var		array
	 * @access	private
	 */
	private $error;
	/**
	 * 
	 * @param array $configs
	 */
	public function __construct( $configs = array() ) {
		$this->maxSize = 0;
		$this->maxFileName = 0;
		$this->allowedTypes = null;
		$this->fileTemp = null;
		$this->filePath = null;
		$this->fileName = null;
		$this->fileType = null;
		$this->fileSize = 0;
		$this->fileExtention = null;
		$this->encryptName = false;
		$this->uploadFolder = null;
		$this->error = array();
		$this->config( $configs );
	}
	/**
	 * 
	 * @param 	array 	$configs
	 */
	public function config( $configs ) {
		$this->maxSize = isset( $configs[ 'maxSize' ] ) ? $configs[ 'maxSize' ] : 0;
		$this->maxFileName = isset( $configs[ 'maxFileName' ] ) ? $configs[ 'maxFileName' ] : 0;
		$this->allowedTypes = isset( $configs[ 'allowedTypes' ] ) ? $configs[ 'allowedTypes' ] : System::$config->allowedFileType;
		$this->fileTemp = isset( $configs[ 'fileTemp' ] ) ? $configs[ 'fileTemp' ] : null;
		$this->filePath = isset( $configs[ 'filePath' ] ) ? $configs[ 'filePath' ] : null;
		$this->fileName = isset( $configs[ 'fileName' ] ) ? $configs[ 'fileName' ] : null;
		$this->fileType = isset( $configs[ 'fileType' ] ) ? $configs[ 'fileType' ] : null;
		$this->fileSize = isset( $configs[ 'fileSize' ] ) ? $configs[ 'fileSize' ] : 0;
		$this->fileExtention = isset( $configs[ 'fileExtention' ] ) ? $configs[ 'fileExtention' ] : null;
		$this->encryptName = isset( $configs[ 'encryptName' ] ) ? $configs[ 'encryptName' ] : false;
		$this->uploadFolder = isset( $configs[ 'uploadFolder' ] ) ? $configs[ 'uploadFolder' ] : null;
	}
	/**
	 * 
	 * @param 	string 	$field
	 * @return 	boolean
	 */
	public function doUpload( $fieldName = "file" ) {
		global $_FILES;
		if( $this->isUpload( $fieldName ) ) {
			//get file info
			$this->fileTemp = $_FILES[ $fieldName ][ 'tmp_name' ];
			$this->fileSize = $_FILES[ $fieldName ][ 'size' ];
			$this->fileName = $this->getFileNameFromPath( $_FILES[$fieldName]['name'] );
			$this->fileExtention = $this->getExtension( $_FILES[ $fieldName ][ 'name' ] );
			$this->fileType = $this->fileExtention;
			if( $this->isAllowedFileType( $this->fileType ) ) {
				if( $this->isAllowedFilesize( $this->fileSize ) ) {
					if( $this->isAllowedFileName( $this->fileName ) ) {
						//create path
						$genPath = $this->generateFolderUpload();
						$this->fileName  = $this->encryptName( $this->fileName );
						$this->filePath = $genPath[ 'path' ] . $this->fileName . "." . $this->fileExtention;
						$pathToUpload = $genPath[ 'realPath' ] . $this->fileName . "." . $this->fileExtention;
						//upload
						if( move_uploaded_file( $this->fileTemp, $pathToUpload ) ) {
							return true;
						} else {
							return false;
						}
					} else {
						$this->error[ "file_name" ] = "file_name";
					}
				} else {
					$this->error[ "file_size" ] = "file_size";
				}
			} else {
				$this->error[ "file_type" ] = "file_type";
			}
		} else {
			$this->error[ "file_path" ] = "file_path";
		}
		if( empty( $this->error ) ) return true;
		else
			return false;
	}
	/**
	 * Is file uploaad
	 * 
	 * @param unknown $fieldName
	 * @return boolean
	 */
	public function isUpload( $fieldName ) {
		global $_FILES;
		if( isset( $_FILES[ $fieldName ] ) && strlen( $_FILES[ $fieldName ][ 'tmp_name' ] ) ) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Is allowed file size
	 * 
	 * @param unknown $fileSize
	 * @return boolean
	 */
	public function isAllowedFilesize( $fileSize ) {
		if( $this->maxSize == 0 ) return true;
		if( $fileSize <= $this->maxSize ) {
			return true;
		}
		return false;
	}
	/**
	 * 
	 * @param unknown $fileName
	 */
	public function isAllowedFileName( $fileName ) {
		if( $this->maxFileName == 0 ) return true;
		if( $this->maxFileName < strlen( $fileName ) ) return false;
		return true;
	}
	/**
	 * 
	 * @param unknown $fieldName
	 * @return boolean
	 */
	public function isAllowedFileType( $fileType ) {
		$index = strpos( $this->allowedTypes, $fileType );
		if( filter_var( $index, FILTER_VALIDATE_INT ) ) return true;
		return false;
	}
	/**
	 * Extract the file extension
	 *
	 * @param	string
	 * @return	string
	 */
	private function getExtension( $filePath ) {
		$x = explode('.', $filePath);
		return $x[ sizeof( $x ) - 1 ];
	}
	/**
	 * 
	 * @param unknown $filePath
	 * @return Ambigous <>
	 */
	public function getFileNameFromPath( $filePath ) {
		$x = explode('.', $filePath );
		return $x[ 0 ];
	}
	/**
	 * Generate folder upload
	 */
	public function generateFolderUpload() {
		$path = APPLICATION_FOLDER . "/upload/" . date( "Y" ) . "/" . date( "m" ) . "/";
		if( !is_dir( $path ) ) {
			mkdir( $path, 0777, true);
		}
		return array( "path" => $path, "realPath" => $path );
	}
	/**
	 * 
	 * @param unknown $fileName
	 * @return string
	 */
	public function encryptName( $fileName ) {
		$rand = new Random();
		if( $this->encryptName == true ) {
			//genetate random string
			$fileName = $rand->randomString( 20 ) . "-" . time();
		} else {
			$fileName = $fileName . "-" . time();
		}
		return $fileName;
	}
	/**
	 * 
	 * @param unknown $maxSize
	 */
	public function setMazSize( $maxSize ) {
		if( is_int( $maxSize ) )
			$this->maxSize = $maxSize;
	}
	/**
	 * 
	 * @return number
	 */
	public function getMaxSize() {
		return $this->maxSize;
	}
	/**
	 * 
	 * @param unknown $maxFileName
	 */
	public function setMaxFileName( $maxFileName ) {
		$this->maxFileName = $maxFileName;
	}
	/**
	 * 
	 * @return number
	 */
	public function getMaxFileName() {
		return $this->maxFileName;
	}
	/**
	 * 
	 * @param unknown $input
	 */
	public function setAllowedTypes( $input ) {
		$this->allowedTypes = $input;
	}
	/**
	 * 
	 * @return array()
	 */
	public function getAllowedTypes(){
		return $this->allowedTypes;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFileTemp() {
		return $this->fileTemp;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFilePath() {
		return $this->filePath;
	}
	/**
	 * 
	 * @param unknown $fileName
	 */
	public function setFileName( $fileName ) {
		$this->fileName = $fileName;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFileName() {
		return $this->fileName;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFileSize() {
		return $this->fileSize;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFileType() {
		return $this->fileType;
	}
	/**
	 * 
	 * @return string
	 */
	public function getFileExtension() {
		return $this->fileExtention;
	}
	/**
	 * 
	 * @param unknown $encryptName
	 */
	public function setEncryptName( $encryptName ) {
		if( is_bool( $encryptName ) )
			$this->encryptName = $encryptName;
	}
	/**
	 * 
	 * @return string
	 */
	public function getEncryptName() {
		return $this->encryptName;
	}
	/**
	 * 
	 * @param 	string	 $folderPath
	 */
	public function setUploadFolder( $folderPath ) {
		$this->uploadFolder = $folderPath;
	}
	/**
	 * 
	 * @return string
	 */
	public function getUploadFolder() {
		return $this->uploadFolder;
	}
	/**
	 * 
	 * @return array
	 */
	public function getError(){
		return $this->error;
	}
}
class Random {
	public function random5Str5Int() {
		$random = new Random();
		return $random->randomString( 5 ) . rand( 12345, 99999);
	}
	public function randomString( $length ) {
		$string = array();
		for( $i = 0; $i < $length; $i++ ) {
			$ran = rand( 65, 90 );
			$string[$i] = chr( $ran );
		}
		return implode( "", $string );
	}
	public function random( $length ) {
		return substr( md5( rand( 10000, 200000) ) , 0, $length );
	}
}
/*end of file Upload.class.php*/
/*location: Upload.class.php*/