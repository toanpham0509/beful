<?php
namespace 		vFast\Core\Libraries;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 * 
 */
if (! defined ( 'CORE_PATH' )) exit ( "No direct script access allowed" );
class Excel {
	public static function autoloader($class) {
		// Check that the class starts with "Requests"
		$class = str_replace( System::$config->namespaceSystemLibarary . "\\", '', $class);
		if ( strpos( $class, 'Requests' ) !== 0) {
			return;
		}
		$file = str_replace('_', '/', $class);
		if (file_exists(dirname(__FILE__) . '/' . $file . '.php')) {
			require_once(dirname(__FILE__) . '/' . $file . '.php');
		}
	}
	public function __construct() {
		
	}
}
/*end of file Excel.class.php*/
/*location: Excel.class.php*/