<?php
use 		vFast\Core\AppEnviroment;
use 		vFast\Core\VFast;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined( 'CORE_PATH' ) ) exit("No direct script access allowed");

/**
 * Autoload class
 */
function __autoload( $className ) {
	$className = explode( '\\', $className );
	$className = end( $className );
	$autoLoads = array(
		CORE_FOLDER,
		CORE_FOLDER . "/libraries",
		APP_FOLDER . "/controllers",
		APP_FOLDER . "/models",
		APP_FOLDER . ""
	);
	foreach($autoLoads as $item) {
		$filePath = $item . "/" . $className . ".class.php";
		if (is_file ( $filePath )) {
			include_once ($filePath);
		}
	}
}
/**
 * --------------------------
 * Application environment
 * --------------------------
 * Values:
 * 		- development
 * 		- testing
 * 		- production
 *
*/

$vFast = VFast::getInstance();
//set enviroment
$vFast->setErrorReport(AppEnviroment::$PRODUCTION);
//setup error report
$vFast->setErrorReport();
/**
 * >> Security
 * */
$vFast->setSecurity();
/**
 * >> include all file congfig in folder configs
 * */
$folderConfig = "configs";
if( is_dir( $folderConfig ) ) {
	$files = scandir ( $folderConfig );
	foreach ( $files as $file ) {
		$filePath = $folderConfig . "/" . $file;
		if ( is_file ( $filePath ) ) {
			include_once ( $filePath );
		}
	}
} else {
	$error = new Error( "404", "404", "Config folder ($folderConfig) not found" );
	$error->showError();
	exit();
}
$vFast->setConfig();
/**
 * Set load object for vFast object
 */
$vFast->setLoad();
/**
 * >> Language
 * */
$vFast->setLang();
/**
 * >> autoload package. This has been defined in file autoload.php in folder config 
 * */
if( isset ( $autoload [ 'packages' ] ) ) {
	foreach ( $autoload [ 'packages' ] as $item ) {
		if ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * >> autoload libraries. This has been defined in file autoload.php in folder config
 * */
if ( isset ( $autoload [ 'libraries' ] ) ) {
	foreach ( $autoload [ 'libraries' ] as $item ) {
		$item = "libraries/" . $item;
		$filePath = $item;
		if ( is_file ( $filePath ) ) {
			include_once ( $filePath );
		} elseif ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * autoload helpers. This has been defined in file autoload.php in folder config
 * 
 * */
$helperFolder = "helpers";
if ( is_dir( $helperFolder ) ) {
	$files = scandir( $helperFolder );
	foreach ( $files as $file ) {
		$filePath = $helperFolder . "/" . $file;
		if ( is_file( $filePath ) ) {
			include_once ( $filePath );
		}
	}
}
if ( isset( $autoload [ 'helpers' ] ) ) {
	foreach ( $autoload [ 'helpers' ] as $item ) {
		$item = APPLICATION_FOLDER . "/helpers/" . $item ;
		if ( is_file( $item ) ) {
			include_once ( $item );
		} elseif ( is_dir ( $item ) ) {
			$files = scandir ( $item );
			foreach ( $files as $file ) {
				$filePath = $item . "/" . $file;
				if ( is_file ( $filePath ) ) {
					include_once ( $filePath );
				}
			}
		}
	}
}
/**
 * >> autoload language. This has been defined in file autoload.php in folder config
 * 
 * Autoload language will processed in class Lang( file Lang.class.php )	
 * */

/**
 * >> autoload model. This has been defined in file autoload.php in folder config
 * */
VFast::$load->file( CORE_FOLDER . "/Model.class.php" );
if ( isset( $autoload['models'] ) ) {
	foreach ( $autoload [ 'models' ] as $item ) {
		$item = APPLICATION_FOLDER . "/models/" . $item;
		if ( is_file( $item ) ) {
			include_once ( $item );
		} elseif ( is_dir( $item ) ) {
			$files = scandir( $item );
			foreach ( $files as $file ) {
				include_once ( $item . "/" . $file );
			}
		}
	}
}
//set default time zone
date_default_timezone_set( VFast::$config->getTimeZone() );
// create a object from Class Cache
$vFast->setCache();
//start cache
VFast::$cache->start();
//create a object Router in object $system
$vFast->setRouter();
//load controler, method, method's parameter
VFast::$router->load();
//router
VFast::$router->router();
//get output buffer content then write to file cache
VFast::$cache->push();
VFast::$cache->finish();
/* End of file bootstrap.php */
/* Location: ./bootstraps/Bootstrap.php */