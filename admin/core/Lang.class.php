<?php
namespace 		vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 * 
 */
if (! defined ( 'CORE_PATH' )) exit ( "No direct script access allowed" );

/**
 * Class Lang
 * @package vFast\Core
 */
class Lang {
	/**
	 * @var Lang
	 */
	static private $instance = null;
	/**
	 * @var 	string
	 * @access	private
	 */
	private $langCode;

	/**
	 * @var		ArrayObject
	 * @access	private
	 */
	private $data;

	/**
	 * @return Lang
	 */
	public static function getInstance() {
		if(Lang::$instance == null) {
			Lang::$instance = new Lang();
		}
		return Lang::$instance;
	}

	/**
	 * @param 	String $langCodeuageFolderName
	 * @access	public        	
	 */
	private function __construct() {
		ob_start();
		$this->data = array ();
		if( isset( $_GET[ 'lang' ] ) && strlen( $_GET[ 'lang' ] ) == 2 ) {
			$this->langCode = $_GET[ 'lang' ];
		} elseif( isset( $_COOKIE[ 'lang' ] ) && strlen( $_COOKIE[ 'lang' ] ) == 2  ) {
			$this->langCode = $_COOKIE[ 'lang' ];
		} elseif(  defined( "LANGUAGE" ) && strlen( LANGUAGE ) ) {
			$this->langCode = LANGUAGE;
		}
		$folderPaths = array( "languages/" . $this->langCode, APP_FOLDER . "/languages/" . $this->langCode );
		foreach ( $folderPaths as $folderPath ) {
			if ( is_dir( $folderPath ) ) {
				$files = scandir( $folderPath );
				foreach ( $files as $file ) {
					$filePath = $folderPath . "/" . $file;
					if ( pathinfo( $filePath, PATHINFO_EXTENSION ) == "xml" ) {
						$this->data = array_merge( $this->data, Lang::convertXMLToArray ( $filePath ) );
					}
				}
			}
		}
		if( isset ( $autoload[ 'languages' ] ) && !empty( $autoload[ 'languages' ] ) ) {
			foreach( $autoload[ 'languages' ] as $item ) {
				$folderPath = "application/languages/" . $item;
				if (is_dir ( $folderPath )) {
					$files = scandir ( $folderPath );
					foreach ( $files as $file ) {
						$filePath = $folderPath . "/" . $file;
						if (pathinfo ( $filePath, PATHINFO_EXTENSION ) == "xml") {
							$this->data = array_merge ( $this->data, Lang::convertXMLToArray( $filePath ) );
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param 	String		$newLang
	 */
	public function changeLanguageByCookie( $newLang, $url = NULL ) {
		global $system;
		ob_start();
		setcookie( "lang", $newLang, time() + 36000000 , "/", "", 0, 0);
		if( $url == NULL ) {
			$system->router->router( baseURL() );
		} else {
			$system->router->router( $url );
		}
	}
	/**
	 * >> get langcode
	 */
	public function getLangCode() {
		return $this->langCode;
	}
	/**
	 *
	 * @param		String	$stringID        	
	 * @return		String
	 * @access		public
	 * @property	Get string langguage with references string input
	 */
	public function getString($stringID) {
		if(isset ( $this->data["$stringID"] )) {
			return $this->data["$stringID"];
		} else {
			return $stringID;
		}
	}
	/**
	 * >> getAllData
	 *
	 * @return	multitype: array
	 * @access	public
	 */
	public function getAllData() {
		return $this->data;
	}
	/**
	 * >> convertXMLToArray
	 *
	 * @param String $filePath
	 * @return array
	 * @access	private
	 */
	private function convertXMLToArray($filePath) {
		$data = array ();
		try {
			$xml = simplexml_load_file ( $filePath );
			foreach ( $xml->children () as $item ) {
				$data[ "$item->id" ] = ( string ) $item->value;
			}
			return $data;
		} catch( Exception $e ) {
			return NULL;
		}
	}
}
/*end of file Lang.class.php*/
/*location: ./bootstraps/Lang.class.php*/