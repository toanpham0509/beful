<?php
namespace 		vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
class Error {
 	const ERROR_GENERAL = "general";
	const ERROR_404 = "404";
	const ERROR_PHP = "php";
	const ERROR_DATABASE = "database";
	private $errorType = "general";
	private $errorHeader;
	private $errorMessage;
	private $pageTitle;
	/**
	 * >> construct
	 * */
	public function __construct( $errorType, $header = "", $message = "", $title = "404" ){

		if( VFast::$config->getAppEnviroment() == "PUBLISH"
				|| VFast::$config->getAppEnviroment() == "publish" ) {
			$errorType = "general";
		}
		$this->errorType = $errorType;
		$this->errorHeader = $header;
		$this->errorMessage = $message;
		$this->pageTitle = $title;
	}
	/**
	 * >> show error page
	 * */
	public function showError(){
		$errorHeader = $this->errorHeader;
		$errorMessage = $this->errorMessage;
		$pageTitle = $this->pageTitle;
		switch ( $this->errorType ) {
			case "404": {
				include_once ( VFast::$config->getAppFolder() . '/errors/404.error.php' );
				break;
			}
			case "php": {
				include_once ( VFast::$config->getAppFolder() . '/errors/code.error.php' );
				break;
			}
			case "database": {
				include_once ( VFast::$config->getAppFolder() . '/errors/database.error.php' );
				break;
			}
			default: {
				include_once ( VFast::$config->getAppFolder() . '/errors/general.error.php' );
			}
		}
	}
	/**
	 * Setter
	 * @param		string		$headerMessage
	 */
	public function setHeaderMessage( $headerMessage ) {
		$this->errorHeader = $headerMessage;
	}
	/**
	 * Setter
	 * @param		string		$headerMessage
	 */
	public function setErrorMessage( $errorMessage ) {
		$this->errorMessage = $errorMessage;
	}
	/**
	 * Setter
	 * @param		string		$headerMessage
	 */
	public function setPageTitle( $pageTitle ) {
		$this->pageTitle = $pageTitle;
	}
	
	/**
	 * >> getter: getHeader
	 * 
	 * @return		string
	 * */
	public function getHeader() {
		return $this->errorHeader;
	}
	/**
	 * >> getter: getMessage
	 * 
	 * @return		string
	 * */
	public function getMessage() {
		return $this->errorMessage;
	}
	/**
	 * >> getter: getPageTitle
	 * @return		string
	 */
	public function getPageTitle() {
		return $this->pageTitle;
	}
}

/*end of file Error.class.php */
/*location: bootstraps/Error.class.php*/