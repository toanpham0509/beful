<?php
namespace 	vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * Class Load
 * 
 * @package			bootstraps
 * @link			http://bk-fw.bk-webs.com/user_guide/load.html
 * @property		include a file, a package
 */
class Load {

	/**
	 * The instance of Load class
	 * 
	 * @var 		Load
	 */
	private static $instance;

	/**
	 * @var			array
	 * @access		private
	 */
	private static $loadedFiles;

	/**
	 * @var			array
	 * @access		private
	 */
	private static $loadedClass;

	/**
	 * @var			array
	 * @access		private
	 */
	private static $loadedObjects;

	/**
	 * List of path to load controller from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathControllers = array();

	/**
	 * List of path to load models from
	 * 
	 * @var 		array
	 * @access		private
	 */
	private $basePathModels = array();

	/**
	 * List of path to load views from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathViews = array();

	/**
	 * List of path to load helpers from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathHelpers = array();

	/**
	 * List of path to load libraries from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathLibraries = array();

	/**
	 * Load constructor.
	 */
	private function __construct() {
		Load::$loadedClass = array();
		Load::$loadedFiles = array();
		Load::$loadedObjects = array();

		$this->basePathHelpers = array(
			VFast::$config->getAppFolder() . "/helpers/",
			VFast::$config->getCoreFolder() . "/helpers/"
		);

		$this->basePathLibraries = array(
			VFast::$config->getAppFolder() . "/libraries/",
			VFast::$config->getCoreFolder() . "/libraries/"
		);

		$this->basePathControllers = array(
			VFast::$config->getAppFolder() . "/controllers/"
		);
		$this->basePathModels = array( VFast::$config->getAppFolder() . "/models/" );
		$this->basePathViews = array( VFast::$config->getAppFolder() . "/views/" );
		$this->loadedClasses = array();
	}

	/**
	 * Get Instance Of Load
	 * 
	 * @return \vFast\Core\Load
	 */
	public static function getInstance() {
		if(Load::$instance == null) {
			Load::$instance = new Load();
		}
		return Load::$instance;
	}

	/**
	 * get models's base path
	 *
	 * @access			public
	 * @return 			array
	 */
	public function getBasePathModels() {
		return $this->basePathModels;
	}

	/**
	 * get views's base path
	 *
	 * @access			public
	 * @return			array
	 */
	public function getBasePathViews() {
		return $this->basePathViews;
	}

	/**
	 *
	 */
	public function getBasePathHelpers() {
		return $this->basePathHelpers;
	}

	/**
	 * get libraries's base path
	 *
	 * @access			public
	 * @return 			array
	 */
	public function getBasePathLibraries() {
		return $this->basePathLibraries;
	}

	/**
	 * @param $filePath
	 * @return bool
	 */
	public function isLoadedFile ($filePath) {
		return (strlen($filePath) > 0 && isset(Load::$loadedFiles[$filePath])) ? true : false;
	}

	/**
	 * @param $className
	 * @return bool
	 */
	public function isLoadedClass ($className) {
		return (strlen($className) > 0 && isset(Load::$loadedClass[$className])) ? true : false;
	}

	/**
	 * @param $objectName
	 * @return bool
	 */
	public function isLoadedObject ($objectName) {
		return (strlen($objectName) > 0 && isset(Load::$loadedObjects[$objectName]));
	}

	/**
	 * @return array
	 */
	public function getLoadedFiles() {
		return self::$loadedFiles;
	}

	/**
	 * @return array
	 */
	public function getLoadedClass() {
		return self::$loadedClass;
	}

	/**
	 * @return array
	 */
	public function getLoadedObjects() {
		return self::$loadedObjects;
	}

	/**
	 * @param $filePath
	 */
	public function newLoadedFile($filePath) {
		self::$loadedFiles[$filePath] = array(
			"data" => $filePath,
			"time" => time()
		);
	}

	/**
	 * @return array
	 */
	public function getBasePathControllers() {
		return $this->basePathControllers;
	}

	/**
	 * @param array $basePathControllers
	 */
	public function setBasePathControllers($basePathControllers) {
		$this->basePathControllers = $basePathControllers;
	}

	/**
	 * @param String $className
	 * @param String $objectName
	 */
	public function loadClass( $className, $objectName = NULL ) {

		//Version 1.0
		//$inst =& getControllerInstance();
		//$objectName = ( $objectName !== NULL ) ? $objectName : $className;
		//$objectName[0] = strtolower( $objectName[0] );

		Load::$loadedClass[$className] = array(
			"data" => $className,
			"time" => time()
		);

		// version 1.0
		//$inst->{$objectName} = new $className();

		Load::$loadedObjects[$objectName] = array(
			"data" => $objectName,
			"time" => time()
		);
	}

	/**
	 * Load view
	 * 
	 * @param		String
	 * @access		public
	 * @return		void
	 *
	 * @version	 	1.0
	 * */
	public function view( $viewName, $fwdata = array() ) {
		if( !empty( $fwdata ) ) {
			foreach ( $fwdata as $key => $value ) {
				${$key} = $value;
			}
		}
		unset( $fwdata );
		foreach( $this->basePathViews as $item ) {
			$pathHaveExt = 	( VFast::$config->getViewFileExtension() == NULL ) ?
							( $item . $viewName . ".php" ) :
							( $item . $viewName . "." . VFast::$config->getViewFileExtension() . ".php" );
			$pathHaveNoExt = $item . $viewName;
			$this->file($pathHaveExt);
			$this->file($pathHaveNoExt);
		}
	}

	/**
	 * Load model
	 * 
	 * @param		String
	 * @access		public
	 * @return		void
	 * @property	
	 */
	public function model( $modelName, $className = NULL, $objectName = NULL ) {
		if( $className == NULL ) $className = $modelName;
		foreach( $this->basePathModels as $item ) {
		 	$pathHaveExt = $item . $modelName . "." . VFast::$config->getSubclassPrefix() . ".php";
		 	$pathHaveNoExt = $item . $modelName;
			$this->file($pathHaveNoExt);
			$this->file($pathHaveExt);
		}
		$className = VFast::$config->getNamespaceAppModel() . "\\" . $className;
		if( class_exists( $className ) ) {
			$objectName = ( $objectName == NULL ) ? $modelName : $objectName;
			$this->loadClass( $className, $objectName );
		}
	}

	/**
	 * Load controller
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 * @property
	 */
	public function controller( $controllerName, $className = NULL, $objectName = NULL ) {
		if( $className == NULL ) $className = $controllerName;
		foreach( $this->basePathControllers as $item ) {
			$pathHaveExt = $item . $controllerName . "." . VFast::$config->getSubclassPrefix() . ".php";
			$pathHaveNoExt = $item . $controllerName;
			$this->file($pathHaveExt);
			$this->file($pathHaveNoExt);
		}
		$className = VFast::$config->getNamespaceAppController() . "\\" . $className;
		if( class_exists( $className ) ) {
			$objectName = ( $objectName == NULL ) ? $controllerName : $objectName;
			$this->loadClass( $className, $objectName );
		}
	}

	/**
	 * Load helper
	 * 
	 * @param		String
	 * @access		public
	 * @return		void 
	 */
	public function helper( $filePath, $className = NULL, $objectName = NULL ) {
		foreach( $this->basePathHelpers as $item ) {
			if( $className == NULL ) {
				$class = $filePath;
				$xClass = $class;
			}
			$pathHaveExt = $item . $filePath . "." . VFast::$config->getSubclassPrefix() . ".php";
			$pathHaveNoExt = $item . $filePath;
			$this->file($pathHaveExt);
			$this->file($pathHaveNoExt);
			$class = VFast::$config->getNamespaceAppHelper() . "\\" . $class;
			if( class_exists( $class ) ) {
				$objectName = ( $objectName == NULL ) ? $xClass : $objectName;
				$this->loadClass( $class, $objectName );
			}
		}
	}

	/**
	 * library
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 */
	public function library( $filePath, $className = NULL, $objectName = NULL ) {
		$libClass = $filePath;
		if( $className == NULL ) $className = $filePath;
		foreach( $this->basePathLibraries as $item ) {
			$pathHaveExt = $item . $filePath . "." . VFast::$config->getSubclassPrefix() . ".php";
			$pathHaveNoExt = $item . $filePath;
			$this->file($pathHaveExt);
			$this->file($pathHaveNoExt);

			$className = VFast::$config->getNamespaceSystemLibarary() . "\\" . $className;
			if( class_exists( $className ) ) {
				$objectName = ( $objectName == NULL ) ? $filePath : $objectName;
				$this->loadClass( $className, $objectName );
			}
		}
	}

	/**
	 * load a file
	 * 
	 * @param		String
	 * @access		public
	 * @return 		void
	 */
	public function file ($filePath) {
		if ($this->isFileExists ($filePath) && !$this->isLoadedFile($filePath)) {
			Load::$loadedFiles[$filePath] = array(
					"data" => $filePath,
					"time" => time()
			);
			include_once($filePath);
		} elseif ($this->isFileExists($filePath . ".php") && !$this->isLoadedFile($filePath . ".php")) {
			Load::$loadedFiles[$filePath] = array(
					"data" => $filePath . ".php",
					"time" => time()
			);
			include_once ( $filePath . ".php" );
		}
	}

	/**
	 * include all files in a package
	 * 
	 * @param		String
	 * @access		public
	 * @return 		void
	 * @property	include all file in a package if it is exists
	 */
	public function package( $packagePath ) {
		if( is_dir( $packagePath ) ) {
			$files = scandir( $packagePath );
			foreach ( $files as $file ) {
				$filePath = $packagePath . "/" . $file;
				$this->view($filePath);
			}
		}
	}

	/**
	 * @param $filePath
	 * @return bool
	 */
	public function isFileExists($filePath) {
		return (file_exists($filePath) && filesize($filePath) > 0 ) ? true : false;
	}
}
/*end of file Load.class.php*/
/*location: ./boostraps/Load.class.php*/