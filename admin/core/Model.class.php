<?php
namespace		vFast\Core;
use				vFast\Core\Database\MySql;
use				vFast\Core\Database\NoSql;
use				vFast\Core\Database\Database;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */

if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * >> Get Instance
 * @param
 * @return		Object
 *
 */
function &getModelInstance() {
	return Model::getInstance();
}
class Model {
	/**
	 * Attribute database
	 * 
	 * @var		Database
	 */
	public $db;
	/**
	 * Istance of this
	 */
	private static $instance;
	/**
	 * List of path to load libraries from
	 *
	 * @var 		array
	 * @access		private
	 */
	private $basePathLibraries = array();
	/**
	 * Constuctor
	 */
	public function __construct() {
		$this->basePathLibraries = array(
				VFast::$config->getAppFolder() . "/libraries/",
				VFast::$config->getCoreFolder() . "/libraries/"
		);
		self::$instance =& $this;
		$this->loadDatabase();
	}
	/**
	 * Get instance
	 * @access		public
	 * @return		object
	 */
	public static function &getInstance() {
		return self::$instance;
	}
	/**
	 * Load database class
	 * 
	 * @access		private
	 * @return		void
	 */
	private function loadDatabase() {
		require_once ( 'database/Database.class.php' );
		switch ( strtolower( VFast::$config->getDatabaseType() ) ) {
			case "mysql": {
				//load driver
				require_once ( 'database/MySQL.class.php' );
				$this->db = new MySql();
				break;
			}
			case "sqlserver": {
				//load driver
				require_once ( 'database/SQLServer.class.php' );
				$this->db = new SqlServer();
				break;
			}
			case "oraclesql": {
				//load driver
				require_once ( 'database/OracleSQL.class.php' );
				$this->db = new SqlServer();
				break;
			}
			case "nosql": {
				//load driver
				require_once ( 'database/NoSQL.class.php' );
				$this->db = new NoSql();
				break;
			}
			case "sqlite": {
				//load driver
				require_once ( 'database/SQLite.class.php' );
				$this->db = new Sqlite();
				break;
			}
			default: {
				$error = new Error( 
						"database",
						VFast::$lang->getString( "Error" ),
						VFast::$lang->getString( "system_not_support_this_database_type" ),
						VFast::$lang->getString( "Error" ) );
				$error->showError();
				die();
			}
		}
	}
	/**
	 * 
	 * @param unknown $className
	 * @param string $objectName
	 */
	public function loadModel( $className, $objectName = null ) {
		if( $objectName == null ) $objectName = $className;
		$objectName[0] = strtolower( $objectName[ 0 ] );
		$filePath = VFast::$config->getAppFolder()
					. "/models/"
					. $className . "." . VFast::$config->getSubclassPrefix() . ".php";
		if( file_exists( $filePath ) ) {
			include_once( $filePath );
		}
		$className = VFast::$config->getNamespaceAppModel() . "\\" . $className;
		if( class_exists( $className ) ) {
			$inst =& getModelInstance();
			$inst->{$objectName} = new $className();
			return true;
		} else {
			return false;
		}
	}
	/**
	 * library
	 *
	 * @param		String
	 * @access		public
	 * @return		void
	 */
	public function loadLibrary( $filePath, $className = NULL, $objectName = NULL ) {
		$libClass = $filePath;
		if( $className == NULL ) $className = $filePath;
		if( $objectName == null ) $objectName = $className;
		$objectName[0] = strtolower( $objectName[ 0 ] );
		foreach( $this->basePathLibraries as $item ) {
			$pathHaveExt = $item . $filePath . "." . VFast::$config->getSubclassPrefix() . ".php";
			$pathHaventExt = $item . $filePath;
			if( file_exists( $pathHaveExt ) ) {
				include_one( $pathHaveExt );
				$className = System::$config->namespaceSystemLibarary . "\\" . $className;
				if( class_exists( $className ) ) {
					$inst =& getModelInstance();
					$inst->{$objectName} = new $className();
					return true;
				} else {
					return false;
				}
			} elseif( is_file( $pathHaventExt ) && file_exists( $pathHaventExt )) {
				include_once( $pathHaventExt );
				$className = VFast::$config->getNamespaceSystemLibarary() . "\\" . $className;
				if( class_exists( $className ) ) {
					$inst =& getModelInstance();
					$inst->{$objectName} = new $className();
					return true;
				} else {
					return false;
				}
			}
		}
	}
}