<?php
namespace       vFast\Core;

/**
 * Class Enviroment
 *
 */
class AppEnviroment {

    /**
     * The application has been published
     *
     * @var String
     */
    public static $PRODUCTION = "production";

    /**
     * The application is in the process of development
     *
     * @var String
     */
    public static $DEVELOPMENT = "development";

    /**
     * The application is in the process of testing
     *
     * @var String
     */
    public static $TESTING = "testing";
}