<?php
namespace 			vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
class Config {

	/**
	 * @var String
	 */
	private $enviroment;
	/**
	 * >> autoload 
	 * 
	 * @var 	array
	 * @access 	private
	 */
	private $autoload;

	/**
	 * @var 	string
	 * @access	private
	 */
	private $baseUrl;

	/**
	 * Allowed file type to upload
	 * 
	 * @var		string
	 * @access	private
	 */
	private $allowedFileType;

	/**
	 * >> cache's status
	 * 
	 * @var 	boolean
	 * @access	private
	 */
	private $cacheStatus;

	/**
	 * >> cache folder
	 * 
	 * @var		string
	 * @access	private
	 */
	private $cacheFolder;

	/**
	 * @var 	int
	 * @access	private
	 */
	private $cacheTime;

	/**
	 * @var Array
	 */
	private $memcachedServer;

	/**
	 * 
	 * @var		string
	 * @access	private
	 */
	private $host;

	/**
	 * @var		int
	 */
	private $post;

	/**
	 * @var		string : mysqli, sqlsever, nosql, sqlite, or oraclesql
	 * @access	private
	 */
	private $databaseType;

	/**
	 * @var		string
	 * @access	private
	 */
	private $databaseName;

	/**
	 * @var 	string
	 * @access	private
	 */
	private $userName;

	/**
	 * @var 	string
	 * @access	private
	 */
	private $userPassword;

	/**
	 * @var 	bool
	 * @access	private
	 */
	private $port;

	/**
	 * @var 	boolean
	 * @access	private
	 */
	private $mail;

	/**
	 * @var 	string 		email
	 * @access	private
	 */
	private $mailFrom;

	/**
	 * @var		string		email
	 * @access	private
	 */
	private $mailFromName;

	/**
	 * @var		string
	 * @access	private
	 */
	private $mailReply;

	/**
	 * @var 	string
	 * @access	private
	 */
	private $defaultControler;

	/**
	 * @var		string
	 * @access	private
	 */
	private $urlSuffix;

	/**
	 * @var		string
	 * @access	private
	 */
	private $charset;

	/**
	 * @var		string
	 * @access	private
	 */
	private $subclassPrefix;

	/**
	 * @var		string
	 * @access	private
	 */
	private $viewFileExtension;

	/**
	 * @var		string
	 * @access	private
	 */
	private $cacheFileExtension;

	/**
	 * @var		string
	 * @access	private
	 */
	private $permittedURIChar;

	/**
	 * @var		string
	 * @access	private
	 */
	private $timeZone;
	/**
	 * @var		string
	 * @access	private
	 */
	private $dateFormat;

	/**
	 * @var		string
	 * @access	private
	 */
	private $encryptionKey;

	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceAppController;
	
	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceAppModel;

	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceSystemLibarary;

	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceAppLibarary;

	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceSystemHelper;

	/**
	 * @var		string
	 * @access	private
	 */
	private $namespaceAppHelper;

	/**
	 * @var		string
	 * @access	private
	 */
	private $appFolder;

	/**
	 * @var		string
	 * @access	private
	 */
	private $coreFolder;

	/**
	 * @var  	string
	 * @access	private
	 */
	private $templateName;

	/**
	 * @var		String
	 * @access	private
	 */
	private $templatePath;

	/**
	 * @var		String
	 * @access	private
	 */
	private $viewPath;

	/**
	 * @var		String
	 * @access	private
	 */
	private $controllerClass;

	/**
	 * @var 	String
	 * @access	private
	 */
	private $controllerMethod;

	/**
	 * Construct
	 */
	public function __construct() {
		//autoload
		global $autoload;
		$this->autoload = $autoload;
		//app folder
		$this->appFolder = APP_FOLDER;
		//core folder
		$this->coreFolder = CORE_FOLDER;
		//base url
		$this->baseUrl = defined( "BASE_URL" ) ? BASE_URL : NULL;
		//allowed file type
		$this->allowedFileType = defined( "ALLOWED_FILE_TYPE_UPLOAD" ) ? ALLOWED_FILE_TYPE_UPLOAD : NULL;

		//cache
		$this->cacheStatus = defined( "CACHE_STATUS" ) ? CACHE_STATUS : false;
		//cache folder
		$this->cacheFolder = defined( "CACHE_FOLDER" ) ? CACHE_FOLDER : "cache";
		//cache time
		$this->cacheTime = defined( "CACHE_TIME" ) ? CACHE_TIME : 3600 ;

		//memcached
		global $vFastConfigGlobal;
		$this->memcachedServer = isset($vFastConfigGlobal['memcached']) ? $vFastConfigGlobal['memcached'] : null;

		//db host
		$this->host = defined( "HOST" ) ? HOST : 3600 ;
		//database type
		$this->databaseType = defined( "DATABASE_TYPE" ) ? DATABASE_TYPE : "mysql";
		//database type
		$this->port = defined( "PORT" ) ? PORT : null;
		//database name
		$this->databaseName = defined( "DATABASE_NAME" ) ? DATABASE_NAME : NULL;
		//user name
		$this->userName = defined( "USER_NAME" ) ? USER_NAME : NULL;
		//user password
		$this->userPassword = defined( "USER_PASSWORD" ) ? USER_PASSWORD : NULL;

		//mail
		$this->mail = defined( "MAIL" ) ? MAIL : NULL;
		//mail from
		$this->mailFrom = defined( "FROM_MAIL" ) ? FROM_MAIL : NULL;
		//mail from name
		$this->mailFromName = defined( "FROM_NAME" ) ? FROM_NAME : NULL;
		//mail reply
		$this->mailReply = defined( "MAIL_REPLY" ) ? MAIL_REPLY : NULL;

		//default controller
		$this->defaultControler = defined( "DEFAULT_CONTROLER" ) ? DEFAULT_CONTROLER : NULL;
		//url suffix
		$this->urlSuffix = defined( "URL_SUFFIX" ) ? URL_SUFFIX : NULL;
		//charset
		$this->charset = defined( "CHARSET" ) ? CHARSET : "utf-8";
		//subclass prefix
		$this->subclassPrefix = defined( "SUBCLASS_PREFIX" ) ? SUBCLASS_PREFIX : NULL;
		//extension of view file
		$this->viewFileExtension = defined( "VIEW_FILE_EXT" ) ? VIEW_FILE_EXT : NULL;
		//extension of cache file
		$this->cacheFileExtension = defined( "CACHE_FILE_EXT" ) ? CACHE_FILE_EXT : NULL; 
		//permitted url char
		$this->permittedURIChar = defined( "PERMITTED_URI_CHAR" ) ? PERMITTED_URI_CHAR : NULL;
		//time zone
		$this->timeZone = defined( "TIME_ZONE" ) ? TIME_ZONE : "Asia/Ho_Chi_Minh";
		//date format
		$this->dateFormat = defined( "DATE_FORMAT" ) ? DATE_FORMAT : "Y-m-d H-i-s";
		//encryption key
		$this->encryptionKey = defined( "ENCRYPTION__KEY" ) ? ENCRYPTION__KEY : "V#$(**D#SFB#%#^";
		//namespace controller
		$this->namespaceAppController = defined( "NAMESPACE_APPLICATON_CONTROLLER" ) ? NAMESPACE_APPLICATON_CONTROLLER : "App\\Controllers";
		//namespacemodel
		$this->namespaceAppModel = defined( "NAMESPACE_APPLICATION_MODEL" ) ? NAMESPACE_APPLICATION_MODEL : "App\\Models";
		//namespaceAppLibrary
		$this->namespaceAppLibarary = defined( "NAMESPACE_LIBRARY" ) ? NAMESPACE_APPLICATION_LIBRARY : "App\\Libraries";
		//namespaceSystemLibrary
		$this->namespaceSystemLibarary = defined( "NAMESPACE_SYSTEM_LIBRARY" ) ? NAMESPACE_SYSTEM_LIBRARY : "BKFW\\Libraries";
		//namespace system helper
		$this->namespaceSystemHelper = defined( "NAMESPACE_SYSTEM_HELPER" ) ? NAMESPACE_SYSTEM_HELPER : "BKFW\\Helpers";
		//namespace application helper
		$this->namespaceAppHelper = defined( "NAMESPACE_APPLICATION_HELPER" ) ? NAMESPACE_APPLICATION_HELPER : "App\Helpers";
		//templateName
		$this->templateName = defined("TEMPLATE") ? TEMPLATE : null;
		//template path
		$this->setTemplatePath($this->appFolder . "/views/" . $this->templateName . "/");
		//View path
		$this->viewPath = "";
		//controller class
		$this->controllerClass = "";
		//controller method
		$this->controllerMethod = "";
	}

	/**
	 * @return 	array
	 */
	public function getAutoload() {
		return $this->autoload;
	}

	/**
	 * @param array $autoload
	 */
	public function setAutoload($autoload) {
		$this->autoload = $autoload;
	}

	/**
	 * @return string
	 */
	public function getBaseUrl() {
		return $this->baseUrl;
	}

	/**
	 * @param string $baseUrl
	 */
	public function setBaseUrl($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	/**
	 * @return string
	 */
	public function getAllowedFileType() {
		return $this->allowedFileType;
	}

	/**
	 * @param string $allowedFileType
	 */
	public function setAllowedFileType($allowedFileType) {
		$this->allowedFileType = $allowedFileType;
	}

	/**
	 * @return boolean
	 */
	public function isCacheOn() {
		return $this->cacheStatus;
	}

	/**
	 * @return boolean
	 */
	public function isCacheStatus() {
		return $this->cacheStatus;
	}

	/**
	 * Getter
	 *
	 * @return	String
	 */
	public function getAppEnviroment() {
		return $this->enviroment;
	}
	/**
	 * Setter
	 *
	 * @param String $enviroment
	 */
	public function setAppEnviroment($enviroment) {
		$this->enviroment = $enviroment;
	}

	/**
	 * @param boolean $cacheStatus
	 */
	public function setCacheStatus($cacheStatus) {
		$this->cacheStatus = $cacheStatus;
	}

	/**
	 * Get cache status
	 *
	 * @return boolean
	 */
	public function getCacheStatus() {
		return $this->cacheStatus;
	}

	/**
	 * @return string
	 */
	public function getCacheFolder() {
		return $this->cacheFolder;
	}

	/**
	 * @param string $cacheFolder
	 */
	public function setCacheFolder($cacheFolder) {
		$this->cacheFolder = $cacheFolder;
	}

	/**
	 * @return int
	 */
	public function getCacheTime() {
		return $this->cacheTime;
	}

	/**
	 * @param int $cacheTime
	 */
	public function setCacheTime($cacheTime) {
		$this->cacheTime = $cacheTime;
	}

	/**
	 * @return string
	 */
	public function getHost() {
		return $this->host;
	}

	/**
	 * @param string $host
	 */
	public function setHost($host) {
		$this->host = $host;
	}

	/**
	 * @return int
	 */
	public function getPost() {
		return $this->post;
	}

	/**
	 * @param int $post
	 */
	public function setPost($post) {
		$this->post = $post;
	}


	/**
	 * @return string
	 */
	public function getDatabaseType() {
		return $this->databaseType;
	}

	/**
	 * @param string $databaseType
	 */
	public function setDatabaseType($databaseType) {
		$this->databaseType = $databaseType;
	}

	/**
	 * @return string
	 */
	public function getDatabaseName() {
		return $this->databaseName;
	}

	/**
	 * @param string $databaseName
	 */
	public function setDatabaseName($databaseName) {
		$this->databaseName = $databaseName;
	}

	/**
	 * @return string
	 */
	public function getUserName() {
		return $this->userName;
	}

	/**
	 * @param string $userName
	 */
	public function setUserName($userName) {
		$this->userName = $userName;
	}

	/**
	 * @return string
	 */
	public function getUserPassword() {
		return $this->userPassword;
	}

	/**
	 * @param string $userPassword
	 */
	public function setUserPassword($userPassword) {
		$this->userPassword = $userPassword;
	}

	/**
	 * @return boolean
	 */
	public function isPort() {
		return $this->port;
	}

	/**
	 * @param boolean $port
	 */
	public function setPort($port) {
		$this->port = $port;
	}

	/**
	 * @return boolean
	 */
	public function isMail() {
		return $this->mail;
	}

	/**
	 * @param boolean $mail
	 */
	public function setMail($mail) {
		$this->mail = $mail;
	}

	/**
	 * @return string
	 */
	public function getMailFrom() {
		return $this->mailFrom;
	}

	/**
	 * @param string $mailFrom
	 */
	public function setMailFrom($mailFrom) {
		$this->mailFrom = $mailFrom;
	}

	/**
	 * @return string
	 */
	public function getMailFromName() {
		return $this->mailFromName;
	}

	/**
	 * @param string $mailFromName
	 */
	public function setMailFromName($mailFromName) {
		$this->mailFromName = $mailFromName;
	}

	/**
	 * @return string
	 */
	public function getMailReply() {
		return $this->mailReply;
	}

	/**
	 * @param string $mailReply
	 */
	public function setMailReply($mailReply) {
		$this->mailReply = $mailReply;
	}

	/**
	 * @return string
	 */
	public function getDefaultControler() {
		return $this->defaultControler;
	}

	/**
	 * @param string $defaultControler
	 */
	public function setDefaultControler($defaultControler) {
		$this->defaultControler = $defaultControler;
	}

	/**
	 * @return string
	 */
	public function getUrlSuffix() {
		return $this->urlSuffix;
	}

	/**
	 * @param string $urlSuffix
	 */
	public function setUrlSuffix($urlSuffix) {
		$this->urlSuffix = $urlSuffix;
	}

	/**
	 * @return string
	 */
	public function getCharset() {
		return $this->charset;
	}

	/**
	 * @param string $charset
	 */
	public function setCharset($charset) {
		$this->charset = $charset;
	}

	/**
	 * @return string
	 */
	public function getSubclassPrefix() {
		return $this->subclassPrefix;
	}

	/**
	 * @param string $subclassPrefix
	 */
	public function setSubclassPrefix($subclassPrefix) {
		$this->subclassPrefix = $subclassPrefix;
	}

	/**
	 * @return string
	 */
	public function getViewFileExtension() {
		return $this->viewFileExtension;
	}

	/**
	 * @param string $viewFileExtension
	 */
	public function setViewFileExtension($viewFileExtension) {
		$this->viewFileExtension = $viewFileExtension;
	}

	/**
	 * @return string
	 */
	public function getCacheFileExtension() {
		return $this->cacheFileExtension;
	}

	/**
	 * @param string $cacheFileExtension
	 */
	public function setCacheFileExtension($cacheFileExtension) {
		$this->cacheFileExtension = $cacheFileExtension;
	}

	/**
	 * @return string
	 */
	public function getPermittedURIChar() {
		return $this->permittedURIChar;
	}

	/**
	 * @param string $permittedURIChar
	 */
	public function setPermittedURIChar($permittedURIChar) {
		$this->permittedURIChar = $permittedURIChar;
	}

	/**
	 * @return string
	 */
	public function getTimeZone() {
		return $this->timeZone;
	}

	/**
	 * @param string $timeZone
	 */
	public function setTimeZone($timeZone) {
		$this->timeZone = $timeZone;
	}

	/**
	 * @return string
	 */
	public function getDateFormat() {
		return $this->dateFormat;
	}

	/**
	 * @param string $dateFormat
	 */
	public function setDateFormat($dateFormat) {
		$this->dateFormat = $dateFormat;
	}

	/**
	 * @return string
	 */
	public function getEncryptionKey() {
		return $this->encryptionKey;
	}

	/**
	 * @param string $encryptionKey
	 */
	public function setEncryptionKey($encryptionKey) {
		$this->encryptionKey = $encryptionKey;
	}

	/**
	 * @return string
	 */
	public function getNamespaceAppController()
	{
		return $this->namespaceAppController;
	}

	/**
	 * @param string $namespaceAppController
	 */
	public function setNamespaceAppController($namespaceAppController) {
		$this->namespaceAppController = $namespaceAppController;
	}

	/**
	 * @return string
	 */
	public function getNamespaceAppModel() {
		return $this->namespaceAppModel;
	}

	/**
	 * @param string $namespaceAppModel
	 */
	public function setNamespaceAppModel($namespaceAppModel) {
		$this->namespaceAppModel = $namespaceAppModel;
	}

	/**
	 * @return string
	 */
	public function getNamespaceSystemLibarary() {
		return $this->namespaceSystemLibarary;
	}

	/**
	 * @param string $namespaceSystemLibarary
	 */
	public function setNamespaceSystemLibarary($namespaceSystemLibarary) {
		$this->namespaceSystemLibarary = $namespaceSystemLibarary;
	}

	/**
	 * @return string
	 */
	public function getNamespaceAppLibarary() {
		return $this->namespaceAppLibarary;
	}

	/**
	 * @param string $namespaceAppLibarary
	 */
	public function setNamespaceAppLibarary($namespaceAppLibarary) {
		$this->namespaceAppLibarary = $namespaceAppLibarary;
	}

	/**
	 * @return string
	 */
	public function getNamespaceSystemHelper() {
		return $this->namespaceSystemHelper;
	}

	/**
	 * @param string $namespaceSystemHelper
	 */
	public function setNamespaceSystemHelper($namespaceSystemHelper) {
		$this->namespaceSystemHelper = $namespaceSystemHelper;
	}

	/**
	 * @return string
	 */
	public function getNamespaceAppHelper() {
		return $this->namespaceAppHelper;
	}

	/**
	 * @param string $namespaceAppHelper
	 */
	public function setNamespaceAppHelper($namespaceAppHelper) {
		$this->namespaceAppHelper = $namespaceAppHelper;
	}

	/**
	 * @return string
	 */
	public function getAppFolder() {
		return $this->appFolder;
	}

	/**
	 * @param string $appFolder
	 */
	public function setAppFolder($appFolder) {
		$this->appFolder = $appFolder;
	}

	/**
	 * @return string
	 */
	public function getCoreFolder() {
		return $this->coreFolder;
	}

	/**
	 * @param string $coreFolder
	 */
	public function setCoreFolder($coreFolder) {
		$this->coreFolder = $coreFolder;
	}

	/**
	 * @return mixed
	 */
	public function getTemplateName() {
		return $this->templateName;
	}

	/**
	 * @param mixed $templateName
	 */
	public function setTemplateName($templateName) {
		$this->templateName = $templateName;
		$this->setTemplatePath(
				$this->getAppFolder()
				. "/views/"
				. $this->getTemplateName() . "/"
		);
	}

	/**
	 * @return String
	 */
	public function getViewPath()
	{
		return $this->viewPath;
	}

	/**
	 * @param String $viewPath
	 */
	public function setViewPath($viewPath)
	{
		$this->viewPath = $viewPath;
	}

	/**
	 * @return String
	 */
	public function getControllerClass()
	{
		return $this->controllerClass;
	}

	/**
	 * @param String $controllerClass
	 */
	public function setControllerClass($controllerClass) {
		$this->controllerClass = $controllerClass;
	}

	/**
	 * @return String
	 */
	public function getControllerMethod() {
		return $this->controllerMethod;
	}

	/**
	 * @param String $controllerMethod
	 */
	public function setControllerMethod($controllerMethod) {
		$this->controllerMethod = $controllerMethod;
	}



	/**
	 * >> set item config
	 * 
	 * @param		string		$key
	 * @param		String
	 * @access		public
	 * @return		void
	 */
	public function setItem( $key, $value ) {
		$this->{$key} = $value;
	}

	/**
	 * @return String
	 */
	public function getTemplatePath() {
		return $this->templatePath;
	}

	/**
	 * @return string
	 */
	public function getTemplateRealPath() {
		return $this->baseUrl . $this->templatePath;
	}

	/**
	 * @param String $templatePath
	 */
	public function setTemplatePath($templatePath) {
		$this->templatePath = $templatePath;
	}

	/**
	 * @return Array
	 */
	public function getMemcachedServer() {
		return $this->memcachedServer;
	}

	/**
	 * @param Array $memcachedServer
	 */
	public function setMemcachedServer($memcachedServer) {
		$this->memcachedServer = $memcachedServer;
	}

	/**
	 * @return String
	 */
	public function getEnviroment() {
		return $this->enviroment;
	}

	/**
	 * @param String $enviroment
	 */
	public function setEnviroment($enviroment) {
		$this->enviroment = $enviroment;
	}
}
/*end of file Config.class.php*/
/*location: ./bootstraps/Config.class.php*/