<?php
namespace	vFast\Core;
use 		vFast\Core\Router;
/**
 * vFasr Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );

class VFast {
	/**
	 * @var		VFast
	 * @access 	private
	 */
	private static $instance;
	/**
	 * @var 	string
	 * @access	private
	 */
	private static $langCode;
	/**
	 * @var 	Lang
	 * @access	public static
	 */
	public static $lang;
	/**
	 * @var 	Security
	 * @access	public static
	 */
	public static $security;
	/**
	 * @var 	Config
	 * @access	public static
	 */
	public static $config;
	/**
	 * @var 	Cache
	 * @access	public static
	 */
	public static $cache;
	/**
	 * @var 	Load
	 * @access	public static
	 */
	public static $load;
	/**
	 * @var		Router
	 * @access	public static
	 */	
	public static $router;
	/**
	 * Constructor
	 */
	private function __construct() {
	}
	/**
	 * Get instance
	 * 
	 * @return \vFast\Core\vFast
	 */
	public static function getInstance() {
		if(VFast::$instance == null) {
			VFast::$instance = new VFast();
		}
		return VFast::$instance;
	}

	/**
	 * Set load object
	 */
	public function setLoad() {
		VFast::$load = Load::getInstance();
	}
	/**
	 * Set langcode
	 * 
	 * @param	string		$langCode
	 * @access	public static
	 */
	public function setLangCode( $langCode ) {
		VFast::$langCode = $langCode;
		VFast::$lang = Lang::getInstance();
		VFast::$lang->changeLanguageByCookie($langCode);
	}
	/**
	 * >> get lang code
	 * 
	 * @return	string	Lang code
	 * @access	public static
	 */
	public function getLangCode() {
		return $this->langCode;
	}
	/**
	 * >> set lang
	 *  
	 * @param	Lang	$lang
	 * @access	public static
	 */
	public function setLang() {
		VFast::$lang = Lang::getInstance();
		VFast::$langCode = VFast::$lang->getLangCode();
	}
	/**
	 * >> get Lang
	 * 
	 * @return		Lang
	 * @access	 	public static
	 */
	public function getLang() {
		return VFast::$lang;
	}
	/**
	 * >> set security
	 * 
	 * @param	Security	$security
	 * @access	public static
	 */
	public function setSecurity(){
	 	 VFast::$security = new Security();
	}
	/**
	 * >> set config
	 * 
	 * @param	Config		$config
	 * @access	public static
	 */
	public function setConfig() {
		VFast::$config = new Config();
	}
	/**
	 * >> set cache
	 * 
	 * @param	Cache	$cache
	 * @access	public static
	 */
	public function setCache() {
		VFast::$cache = new Cache();
	}
	/**
	 * >> get cache
	 * 
	 * @return		Cache
	 * @access	 	public static
	 */
	public function getCache() {
		return VFast::$cache;
	}
	/**
	 * >> set router
	 * 
	 * @param		Router 		$router
	 * @access		public static
	 */
	public function setRouter() {
		VFast::$router = new Router();
	}
	/**
	 * >> get router
	 * 
	 * @return		Router
	 * @access		public static
	 */
	public function getRouter() {
		return VFast::$router;
	}
	/**
	 * Set error report
	 */
	public function setErrorReport($enviroment = null) {
		if($enviroment == AppEnviroment::$PRODUCTION) {
			//disable error message
			error_reporting(0);
		} else {
			//enable report all error
			error_reporting(E_ALL);
		}
	}
}
/*end of file VFast.class.php*/
/*location: ./bootstraps/VFast.class.php*/