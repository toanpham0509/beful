<?php
namespace	vFast\Core;
use			vFast\Core\View;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * >> Get Instance
 * @param
 * @return		Object
 *
 */
function &getControllerInstance() {
	return Controller::getInstance();
}
/**
 * Class Controller
 * 
 */
class Controller {
	/**
	 * A instance of class Load
	 * @var		Load
	 * @access	public
	 */
	public $load;

	/**
	 * A instance of class Lang
	 * @var		Lang
	 * @access	Public
	 */
	public $lang;

	/**
	 * A instance of class Router
	 * @var		Router
	 * @access	Public
	 */
	public $router;

	/**
	 * Instance of controller class
	 */
	private static $instance;

	/**
	 * @var 	View
	 */
	public $view;

	/**
	 * Constructor
	 */
 	public function __construct() {

		//Version 1.0
 		//self::$instance =& $this;

 		$this->load = VFast::$load;
 		$this->lang = VFast::$lang;
 		$this->router = VFast::$router;
		$this->view = View::getInstance();
	}

	/**
	 * Get instance
	 * @access		public
	 * @return		object
	 */
	public static function &getInstance() {
		return self::$instance;
	}
}
/*end of file Controller.class.php */
/*location: */