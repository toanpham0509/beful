<?php
namespace	vFast\Core;
use 		vFast\Core\URI;
use 		vFast\Core\Error;
use 		vFast\Core\VFast;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
ob_start();
/**
 * Router Class
 *
 * Parses URIs and determines routing
 *
 * @package		bootstraps
 * @subpackage		
 * @category	bootstraps
 * @link		http://bk-fw.bk-webs.com/documentation/routing.html
 */
class Router {
	/**
	 * @var		string
	 * @access	private
	 */
	private $uri;
	/**
	 * @var 	string
	 * @access	private
	 */
	private $controllerName;
	/**
	 * @var 	string
	 * @access	private
	 */
	private $controllerPath;
	/**
	 * @var 	string
	 * @access	private
	 */
	private $methodName;
	/**
	 * @var 	array
	 * @access	private
	 */
	private $parameter;
	/**
	 * @var		array
	 * @access	private
	 */
	private $error;
	/**
	 * @var		array
	 */
	private $segments;
	/**
	 * @var		array
	 */
	private $controllersConfig;
	/**
	 * @var		array
	 */
	private $methodsConfig;
	/**
	 * >> construct
	 */
	public function __construct() {
		global $CONTROLLERS;
		global $METHODS;
		$uri = new URI();
		$this->uri  = $uri->getURI();
		$this->error = array();
		$this->controllersConfig = $CONTROLLERS;
		$this->methodsConfig = $METHODS;
	}
	/**
	 * @return		string
	 * @access		public
	 */
	public function getURI() {
		return $this->uri;
	}
	/**
	 * >> set uri
	 * 
	 * @parameter		string		$uri
	 * @access		public
	 */
	public function setURI( $uri ) {
		$this->uri = $uri;
	}
	/**
	 * >> get controller name
	 * 
	 * @return 		string
	 * @access		public
	 */
	public function getControllerName() {
		return $this->controllerName;
	}
	/**
	 * >> get controller path
	 * 
	 * @return 		string
	 * @access		public
	 */
	public function getControllerPath() {
		return $this->controllerPath;
	}
	/**
	 * >> get method name
	 * 
	 * @return 		array
	 * @access		public
	 */
	public function getMethodName() {
		return $this->methodName;
	}
	/**
	 * >> get method's parameter
	 * 
	 * @return		array
	 * @access		public
	 */
	public function getParameter() {
		return $this->parameter;
	}
	/**
	 * >> get segments
	 * 
	 * @return		array
	 * @access		public
	 */
	public function getSegments() {
		return $this->segments;
	}
	/**
	 * >> get error
	 * @return 		array
	 * @access		public
	 */
	public function getError() {
		return $this->error;
	}
	/**
	 * >> load
	 * @access		public
	 * @property	Process url to detect segments, controller name, controller path, method, method's parameter
	 * @return		void
	 */
	public function load() {
		$this->loadSegments();
		$phpSelf = $this->segments;
		if( sizeof ( $phpSelf ) > 0 ) {
			//get controller, function, parameter
			$i = 0;
			$foundController = false;
			$foundFunction = false;
			$size = sizeof( $phpSelf );
			for( $i = 0; $i < $size; $i++ ) {
				$item = $phpSelf[ $i ];
				if( $i == 0 ) {
					if( isset( $this->controllersConfig[ "$item" ] ) ) {
						$pathFolder = $pathFile = VFast::$config->getAppFolder() . "/controllers/" . $this->controllersConfig[ "$item" ];
						
					} else {
						//path folder
						$pathFolder = VFast::$config->getAppFolder() . "/controllers/" . $item ;
						
						//path file
						$item = VFast::$security->validateControllerName( $item );
						// Does the requested controller exist in the root folder?
						$pathFile = VFast::$config->getAppFolder() . "/controllers/" . $item ;
						
					}
					$filePath = $pathFile . "." . VFast::$config->getSubclassPrefix() . ".php";
					if( is_dir( $pathFolder ) && $size > 1 ) {
						$i++;
						// Is the controller in a sub-folder?
						if( $i < $size ) {
							if( isset( $this->controllersConfig[ "{$phpSelf[ $i ]}" ] ) ) {
								$item = $phpSelf[ $i ];
								$path = VFast::$config->getAppFolder() 
										. "/controllers/" 
										. $phpSelf[ $i - 1 ] 
										. "/" . $this->controllersConfig[ "$item" ] 
										. "." 
										. VFast::$config->getSubclassPrefix() . ".php";
								if( file_exists( $path ) ) {
									$this->controllerPath = $path;
									$this->controllerName = $this->controllersConfig[ "$item" ];
									$foundController = true;
								} else {
									$this->error[] = "loadController";
									break;
								}
							} else {
								$item = VFast::$security->validateControllerName( $phpSelf[ $i ] );
								$path = VFast::$config->getAppFolder() . "/controllers/"
										. $phpSelf[ $i - 1 ] . "/" . $item 
										. "." . VFast::$config->getSubclassPrefix() . ".php" ;
								if( file_exists( $path ) ) {
									$this->controllerPath = $path;
									$this->controllerName = $item;
									$foundController = true;
								} else {
									$i--;
									$item = $phpSelf[ $i ];
									if( isset( $this->controllersConfig[ "$item" ] ) ) {
										$path = VFast::$config->getAppFolder() . "/controllers/" . $this->controllersConfig[ "$item" ];
									} else {
										$item = VFast::$security->validateControllerName( $item );
										// Does the requested controller exist in the root folder?
										$path = VFast::$config->getAppFolder() . "/controllers/" . $item ;
									}
									$filePath = $path . "." . VFast::$config->getSubclassPrefix() . ".php";
									if( file_exists ( $filePath ) ) {
										$this->controllerPath = $filePath;
										if( isset( $this->controllersConfig[ "$item" ] ) ) {
											$this->controllerName =  $this->controllersConfig[ "$item" ];
										} else {
											$this->controllerName =  $item;
										}
										$foundController = true;
									} else {
										$this->error[] = "loadController";
									}
								}
							}
						} else {
							$this->error[] = "loadController";
							break;
						}
					} elseif( file_exists ( $filePath ) ) {
						$this->controllerPath = $filePath;
						if( isset( $this->controllersConfig[ "$item" ] ) ) {
							$this->controllerName =  $this->controllersConfig[ "$item" ];
						} else {
							$this->controllerName =  $item;
						}
						$foundController = true;
					} else {
						$this->error[] = "loadController";
					}
				} else {
					//load function name
					if( $foundFunction == false && $foundController == true ) {
						if( isset( $this->methodsConfig[ "$item" ] ) ) {
							$this->methodName = $this->methodsConfig[ "$item" ];
						} else {
							$this->methodName = $item;
						}
						$foundFunction = true;
					} else {
						//load parameter
						if( $foundFunction == true ) {
							$this->parameter[] = $item;
						}
					}
				}
			}
			if( $foundController == true && $foundFunction == false ) {
				$this->methodName = "index";
			}
		} else {
			//Load default controler
			if( isset( $this->controllersConfig[ '/' ] ) ) {
				$path = VFast::$config->getAppFolder()
						. "/controllers/" 					
						. $this->controllersConfig[ '/' ] 
						. "." 
						. VFast::$config->getSubclassPrefix() .".php" ;
				if( file_exists( $path ) ) {
					$this->controllerPath = $path;
					$this->controllerName = $this->controllersConfig[ '/' ];
					if( isset( $this->methodsConfig[ '/' ] ) ) {
						$this->methodName = $this->methodsConfig[ '/' ];
					} else {
						$this->methodName = "index";
					}
				} else {
					$this->error[] = "loadController";
				}
			} else {
				$this->error[] = "loadController";
			}
		}
		VFast::$config->setControllerClass($this->controllerName);
		VFast::$config->setControllerMethod($this->methodName);

		$this->controllerName = VFast::$config->getNamespaceAppController() . "\\" . $this->controllerName;
	}
	/**
	 * >> router
	 * 
	 * @access		public
	 * @return		void
	 * @property	Process controller name, controller path, method name, 
	 * 				parameter to load class controller, create a object...
	 */
	public function router( $url = NULL ) {
		if( $url !== NULL ) {
			header("Location: $url");
			exit();
		}
		if( empty( $this->error ) ) {
			require_once( $this->controllerPath );
			try {
				//check class controller
				if( class_exists( $this->controllerName ) ) {
					//create object controller
					$controller = new $this->controllerName();
					//check method
					if( method_exists( $controller , $this->methodName ) ) {
						//call method
						if( empty( $this->parameter ) ) {
							call_user_func( array( $controller, $this->methodName ), $this->parameter );
						} else {
							call_user_func_array( array( $controller, $this->methodName ), $this->parameter );
						}
					} else {
						$error = new Error(
								"404",
								"404",
								VFast::$lang->getString( "method_not_found" ) . ": {$this->methodName}",
								"404 -" . VFast::$lang->getString( "method_not_found" )
						);
						$error->showError();
						die();
					}
				} else {
					$error = new Error(
							"404",
							"404",
							VFast::$lang->getString( "class_not_found" ) . ": {$this->controllerName}",
							"404 -" . VFast::$lang->getString( "class_not_found" )
					);
					$error->showError();
					die();
				}
			} catch( Exception $e ) {
				$error = new Error(
						"404",
						"404",
						VFast::$lang->getString( "class_not_found" ) . ": {$this->controllerName}",
						VFast::$lang->getString( "class_not_found" )
				);
				$error->showError();
			}
		} else {
			//error 404
			$error = new Error(
					"404",
					"404",
					VFast::$lang->getString( "page_not_found" ),
					VFast::$lang->getString( "page_not_found" )
			);
			$error->showError();
		}
	}
	/**
	 * >> load segments
	 * @access		private
	 * @return		void
	 * @property	Prosecc uri to get segment
	 */
	private function loadSegments() {
		$phpSelf = $this->uri;
		$phpSelf = preg_split ( "/\//", $phpSelf );
		if( sizeof ( $phpSelf ) ) {
			foreach ( $phpSelf as $key => $value ) {
				if( $value == null || ( strlen( $value ) == 0 ) ) {
					unset( $phpSelf[ $key ] );
				}
			}
		}
		$this->segments = array_values( $phpSelf );
		return null;
	}
}
/*end of file Router.class.php*/
/*location: ./bootstraps/Router.class.php*/