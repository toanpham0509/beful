<?php
namespace 		vFast\Core;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
class Cache {
	/**
	 * >> cache's status
	 * 
	 * @var			boolean
	 * @property	It is cache's status. It's value type is boolean
	 * @access		private
	 */
	private $cacheStatus;
	/**
	 * >> cache time
	 * @var			int
	 * @property	It is time to cache file is valid
	 * @access		private
	 */
	private $cacheTime;
	/**
	 * >> Cache folder
	 * @property  	It is cache folder name. This folder will store all cache file.
	 * @access		private
	 */
	
	private $cacheFolder;
	/**
	 * >> cache folder's path
	 * @property 	It is path of cache
	 * @access		private 
	 */
	private $cachePath;
	/**
	 * >> self
	 * @property	It is a string, is client request url
	 * @access		private 
	 */
	private $self;
	/**
	 * >>  file path
	 * @property	It is cache file's path of current client visit
	 * @access		private   
	 */
	private $filePath;
	/**
	 * >> cache size
	 * @property	It is size of cache system
	 * @access		private
	 */
	private $cacheSize;
	/**
	 * >> Number of file in cache system
	 * @property	Number of file in cache system
	 * @access		private
	 */
	private $cacheNumOfFile;
	/**
	 * >> construct
	 * 
	 */
	public function __construct() {
		global $system;
		//get cache status
		$this->setCache( VFast::$config->getCacheStatus() );
		//cache time
		$this->setCacheTime( VFast::$config->getCacheTime() );
		//cache folder path
		$this->setCacheFolder( VFast::$config->getCacheFolder() );
		$this->setCachePath( VFast::$config->getCacheFolder() );
		//file path
		$self = strtolower ( str_replace( " ", "", $_SERVER[ 'PHP_SELF' ] ) );
		$self = substr( $self ,  stripos ( $self, "index.php" ) + 10 ) ;
		$this->setSelf( $self );
		$this->setFilePath( $self );
	}
	/**
	 * >> set cache's status
	 * @param	boolean		$cacheStatus
	 */
	public function setCache( $cacheStatus ) {
		$this->cacheStatus = $cacheStatus;
	}
	/**
	 * >> get cache's status
	 * @return 	boolean
	 */
	public function getCache() {
		return $this->cacheStatus;
	}
	/**
	 * >> set cache's time
	 * @param	int		$cacheTime
	 */
	public function setCacheTime( $cacheTime ) {
		$this->cacheTime = $cacheTime;
	}
	/**
	 * >> get cache time
	 * @return	int
	 */
	public function getCacheTime() {
		return $this->cacheTime;
	}
	/**
	 * >> set cache folder
	 * @param	string		$cacheFolder
	 */
	public function setCacheFolder( $cacheFolder ) {
		$this->cacheFolder = VFast::$config->getAppFolder() . "/" . $cacheFolder;
	} 
	/**
	 * >> get cache folder
	 * @return	string
	 */
	public function getCacheFolder() {
		return $this->cacheFolder;
	}
	/**
	 * >> Set cache folder
	 * @param	string		$cacheFolder
	 */
	public function setCachePath( $cacheFolder ) {
		$this->cachePath = VFast::$config->getAppFolder() . "/" . $cacheFolder;
	}
	/**
	 * >> get cache path
	 * @return		string
	 */
	public function getCachePath() {
		return $this->cachePath;
	}
	/**
	 * >> set self
	 * @param		string		$self
	 */
	public function setSelf( $self ) {
		$this->self = $self;
	}
	/**
	 * >> get self
	 * @return		string
	 */
	public function getSelf() {
		return $this->self;
	}
	/**
	 * >> set file path 
	 * @param	string		$self: request url
	 */
	public function setFilePath( $self ) {
		global $system;
		if( VFast::$config->getCacheFileExtension() == NULL )
			$this->filePath = $this->cachePath . "/" . md5( $this->self ) . ".html";
		else
			$this->filePath = $this->cachePath . "/" . md5( $this->self ) . VFast::$config->getCacheFileExtension() . ".html";
	}
	/**
	 * >> get file path ( cahce file )
	 * @return 		string
	 */
	public function getFilePath() {
		return $this->filePath;
	}
	/**
	 * >> get cach size
	 * @property	It return system cache's size
	 * @return		int 
	 */
	public function getCacheSize() {
		if( is_dir( $this->cachePath ) ) {
			$size = 0;
			$files = scandir( $this->cachePath );
			foreach ( $files as $file ) {
				if( file_exists( $file ) ) {
					$size += filesize( $file );
				}
			}
			return round( $size / 8192, 2 );
		} else {
			return 0;
		}
	}
	/**
	 * >> getCacheNumOfFile
	 * @property 	
	 */
	public function getCacheNumOfFile() {
		if( is_dir( $this->cachePath ) ) {
			$files = scandir( $this->cachePath );
			return sizeof( $files ) - 2;
		}
	}
	/**
	 * >> start cache
	 * 
	 * @property	If file cache is exits and is val then include that cache file. Else start the output buffer
	 */
	public function start() {
		if( file_exists( $this->filePath ) && ( time() - CACHE_TIME <  filemtime( $this->filePath ) ) ) {
			require_once ( $this->filePath );
			exit();
		}
		//start output buffer 
		ob_start();
	}
	/**
	 * >> push
	 * @property	Get output buffer content then write to file cache
	 */
	public function push(){
		if( $this->cacheStatus == true ) {
			$fileWrite = fopen ( $this->filePath , "w");
			fwrite ( $fileWrite, ob_get_contents () );
			fclose ( $fileWrite );
		}
	}
	/**
	 * >> finish
	 * 
	 * @property	end of output buffer
	 */
	public function finish() {
		ob_end_flush ();
	}
}
/*end of file Cache.class.php*/
/*location: ./bootstraps/Cache.class.php*/