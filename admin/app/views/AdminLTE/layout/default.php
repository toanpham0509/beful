<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->loadComponent("Header"); ?>
    </head>
    <body class="skin-blue sidebar-mini <?= $this->getDataItem("bodyClass"); ?>">
        <div class="wrapper">
            <?php $this->loadComponent("Navbar"); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="right_main_content">
                    <?php $this->loadComponent($this->loadView()); ?>
                </div>
            </div>
            <?php $this->loadComponent("Footer"); ?>
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->
    </body>
</html>