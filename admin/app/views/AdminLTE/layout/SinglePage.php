<?php $this->loadComponent("Header"); ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <?php $this->loadComponent("Sidebar"); ?>
                </div>
                <div class="col-sm-9 padding-right">
                    <?php $this->loadComponent($this->loadView()); ?>
                </div>
            </div>
        </div>
    </section>
<?php $this->loadComponent("Footer"); ?>