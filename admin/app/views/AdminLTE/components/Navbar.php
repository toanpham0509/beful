<?php
use vFast\Core\VFast;
$view = \vFast\Core\View::getInstance();
$staticPath = VFast::$config->getBaseUrl() . VFast::$config->getTemplatePath() . "public";
?>
<header class="main-header">
	<!-- Logo -->
	<a href="<?= VFast::$config->getBaseUrl() ?>" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>Beful</b></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Beful</b> Admin</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?= $staticPath ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
						<span class="hidden-xs"><?= $this->getDataItem("userName") ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="<?= $staticPath ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
							<p>
								<?= $this->getDataItem("userName") ?> - <?= $this->getDataItem("userType"); ?>
								<small>Login time: <?= date("H:i:s d/M/Y", $this->getDataItem("loginTime")); ?></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="<?= VFast::$config->getBaseUrl() ?>SignOut" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="<?= $view->getDataItem("menuDashboard"); ?>">
				<a href="<?= VFast::$config->getBaseUrl() ?>">
					<i class="fa fa-dashboard"></i> <span><?= VFast::$lang->getString("Dashboard"); ?></span></i>
				</a>
			</li>
			<li class="<?= $view->getDataItem("menuRuleByPosition"); ?>">
				<a href="<?= VFast::$config->getBaseUrl() ?>phan-quyen-theo-chuc-vu">
					<i class="fa fa-files-o"></i>
					<span><?= VFast::$lang->getString("RuleByPosition"); ?></span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>