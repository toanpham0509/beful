<?php
    $view = \vFast\Core\View::getInstance();
?>
<section class="content-header">
    <h1>
        <?php
            echo $view->getDataItem("pageName");
        ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $view->getDataItem("pageName") ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                Thống kê
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">

        </div>
        <div class="box-footer">

        </div>
    </div>
</section>
