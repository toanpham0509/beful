<?php
use     vFast\Core\VFast;
$view = \vFast\Core\View::getInstance();
?>
<section class="content-header">
    <h1>
        <?php
        echo $view->getDataItem("pageName");
        ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $view->getDataItem("pageName") ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $view->getDataItem("pageName"); ?>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th><?= VFast::$lang->getString("PositionName") ?></th>
                    <th>Action</th>
                </tr>
                    <?php
                        $positions = $view->getDataItem("positions");
                        if(!empty($positions)) {
                            $i = 0;
                            foreach($positions as $position) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $position['position_name'] ?></td>
                                    <td>
                                        <a href="">
                                            Show
                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                    ?>

                </tbody>
            </table>
        </div>
        <div class="box-footer">

        </div>
    </div>
</section>
