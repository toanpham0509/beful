<?php
use vFast\Core\VFast;
$staticPath = VFast::$config->getBaseUrl() . VFast::$config->getTemplatePath() . "public";
?>
<footer class="">
	<div class="pull-right hidden-xs">
		<b>Version</b> 2.2.0
	</div>
	<strong>Copyright &copy; 2014-2015 <a href="<?= BASE_URL ?>">AzStack</a>.</strong> All rights reserved.
</footer>
<!-- jQuery 2.1.4 -->
<script src="<?= $staticPath ?>/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?= $staticPath ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?= $staticPath ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="<?= $staticPath ?>/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?= $staticPath ?>/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?= $staticPath ?>/dist/js/app.min.js" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= $staticPath ?>/dist/js/demo.js" type="text/javascript"></script>
<script src="<?= $staticPath ?>/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= $staticPath ?>libs/ckfinder/ckfinder.js">
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});
</script>