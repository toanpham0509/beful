<?php
namespace       App\Models;
use             vFast\Core\Model;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * Class MPosition
 * @package App\Models
 */
class MPosition extends Model {

    /**
     * @var String
     */
    private $tableName;

    /**
     * MPosition constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->tableName = "position";
        $this->db->connect();
    }

    /**
     *
     */
    public function getPositions() {
        return $this->db->get($this->tableName);
    }
}