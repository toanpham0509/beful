<?php
namespace       App\Models;
use             vFast\Core\Model;
/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */

if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );
/**
 * Class MUser
 * @package App\Models
 */
class MUser extends Model {

    /**
     * MUser constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
    }

    /**
     * @return array
     */
    public function getUsers() {
        return $this->db->get("user");
    }
}
/*end of file MUser.class.php*/
/*location: MUser.class.php*/