<!--
<!DOCTYPE html> 
<html lang="de" class="no-js">
<head>
	<meta charset="utf-8">
	<title>Error 404 | Freelance-Webdesigner &amp; Konzepter Christoph Sch��ler</title>
	<meta name="description" content="Christoph Sch��ler gestaltet und programmiert User-Interfaces im Web" />
	<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
	<meta name="google-site-verification" content="y3eQhoi3ZZf-mjnU2uH-GZbXwIFrCgRVYgAd66pZs9k" />
	<link rel="Shortcut Icon" type="image/icon" href="http://herrschuessler.de/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Domine:700|Oswald:700|Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<script src="http://herrschuessler.de/js/modernizr.min.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="http://herrschuessler.de/css/style.css">
	<script>
		var gaProperty = 'UA-266037-30';
		var disableStr = 'ga-disable-' + gaProperty;
		if (document.cookie.indexOf(disableStr + '=true') > -1) {
			window[disableStr] = true;
		}
		function gaOptout() {
			document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
			window[disableStr] = true;
		}
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-266037-30', 'herrschuessler.de');
		ga('set', 'anonymizeIp', true);
		ga('send', 'pageview');
	</script>
</head>

<body>
	<article>
		<header class="container">
			<div class="logo-wrapper" id="logo">
				<div class="logo"></div>
			</div>
			<h1>Error 404</h1>
		</header>
		<section class="about container">
			<p><a class="link" href="http://herrschuessler.de/">Weitergehen</a>, hier gibt es nichts zu sehen!</p>
		</section>
	</article>
</body>
</html>
 -->
 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" /> 
<title><?php echo $pageTitle ?></title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding-bottom:10px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	padding:10px;
	border: 1px solid #D0D0D0;
	-webkit-box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $errorHeader; ?></h1>
		<?php echo $errorMessage; ?>
	</div>
</body>
</html>