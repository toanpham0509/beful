<?php
namespace 		App\Controllers;
use 			vFast\Core\Libraries\HttpRequest;

/**
 * vFast Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'CORE_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Home
 * @package App\Controllers
 */
class Home extends Login {

	/**
	 * Home constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->view->setDataItem("menuDashboard", "active");
	}

	/**
	 * Index
	 */
	public function index() {
		$this->view->setDataItem("pageName", $this->lang->getString("Dashboard"));
		$this->view->generateView();
	}
}
/*end of file Home.class.php*/
/*location: Home.class.php*/