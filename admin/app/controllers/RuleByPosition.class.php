<?php
namespace       App\Controllers;
use             App\Models\MPosition;

/**
 * Class RuleByPosition
 * @package App\Controllers
 */
class RuleByPosition extends Login {

    private $mPosition;
    /**
     * RuleByPosition constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->view->setDataItem("menuRuleByPosition", "active");
        $this->mPosition = new MPosition();
    }

    /**
     * Index
     */
    public function index() {
        $this->view->setDataItem("pageName", $this->lang->getString("ListPosition"));
        $this->view->setDataItem("positions", $this->mPosition->getPositions());
        $this->view->generateView();
    }
}