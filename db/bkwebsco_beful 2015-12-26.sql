-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2015 at 09:34 AM
-- Server version: 5.5.45
-- PHP Version: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bkwebsco_beful`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjust_inventory`
--

CREATE TABLE IF NOT EXISTS `adjust_inventory` (
  `adjust_inventory_id` int(10) NOT NULL AUTO_INCREMENT,
  `adjust_inventory_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(10) DEFAULT NULL,
  `adjust_inventory_date` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`adjust_inventory_id`),
  KEY `warehouse_id` (`warehouse_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`warehouse_id`) REFER `demo_befult_3' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `adjust_inventory_line`
--

CREATE TABLE IF NOT EXISTS `adjust_inventory_line` (
  `adjust_inventory_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `product_old_amount` int(11) NOT NULL DEFAULT '0',
  `product_amount` int(10) DEFAULT NULL,
  `input_order_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `expired_date` int(11) DEFAULT NULL,
  `adjust_type_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `adjust_inventory_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`adjust_inventory_line_id`),
  KEY `adjust_type_id` (`adjust_type_id`) USING BTREE,
  KEY `adjust_inventory_id` (`adjust_inventory_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`adjust_type_id`) REFER `demo_befult' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `adjust_type`
--

CREATE TABLE IF NOT EXISTS `adjust_type` (
  `adjust_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `adjust_type_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`adjust_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE IF NOT EXISTS `bank_account` (
  `account_id` int(10) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `account_number` int(20) DEFAULT NULL,
  `bank_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `open_date` int(10) DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `fax` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `note` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `district_id` int(10) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `account_status` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`account_id`, `account_name`, `account_number`, `bank_name`, `address`, `open_date`, `phone`, `fax`, `note`, `district_id`, `create_time`, `last_update`, `account_status`, `status`) VALUES
(1, 'Tài khoản tiền mặt', 1, '1', 'VP3 - Linh Đàm - Hoàng Mai - Hà Nội', 1262278800, '0904374656', '0904374656', '', 2, 1448522789, 1448522862, 5, 0),
(2, 'Tài khoàn Techcombank', 1234567, 'Techcombank', 'VP3 - Linh Đàm - Hoàng Mai - Hà Nội', 1262278800, '0904374656', '0904374656', '', 5, 1448522865, 1448522910, 5, 0),
(3, 'Tài khoản Vietcombank', 2147483647, 'Vietcombank', 'VP3 - Linh Đàm - Hoàng Mai - Hà Nội', 1262278800, '0904374656', '0904374656', '', 5, 1448522914, 1448522950, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `book_id` int(10) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`book_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buy_price`
--

CREATE TABLE IF NOT EXISTS `buy_price` (
  `buy_price_id` int(10) NOT NULL AUTO_INCREMENT,
  `buy_price_code` varchar(20) DEFAULT NULL,
  `buy_price_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_id` int(10) DEFAULT NULL,
  `time_start` int(10) DEFAULT NULL,
  `time_end` int(10) DEFAULT NULL,
  `validity` tinyint(1) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `buy_price_status` int(3) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`buy_price_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `buy_price`
--

INSERT INTO `buy_price` (`buy_price_id`, `buy_price_code`, `buy_price_name`, `supplier_id`, `time_start`, `time_end`, `validity`, `create_time`, `last_update`, `buy_price_status`, `status`) VALUES
(1, 'GM-1', 'Bảng giá mua GM-1', 6, 1448902800, 1609347600, 1, 1448986597, 1448986660, 5, 0),
(2, 'GM-2', 'Bảng giá mua GM-2', 5, 1448902800, 1577725200, 1, 1448986668, 1448986713, 5, 0),
(3, 'GM-3', 'Bảng giá mua GM-3', 4, 1448902800, 1609347600, 1, 1448986717, 1448986768, 5, 0),
(4, 'GM-4', NULL, NULL, NULL, NULL, NULL, 1450700477, NULL, 1, 0),
(5, 'GM-5', NULL, NULL, NULL, NULL, NULL, 1450700630, NULL, 1, 0),
(6, 'GM-6', 'Bảng giá Kyowa', 7, 1450630800, 1453222800, 1, 1450700637, 1450700701, 5, 0),
(7, 'GM-7', 'Bacai Giảm cân', 9, 1450803600, 1453395600, 1, 1450841193, 1450841232, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `buy_price_line`
--

CREATE TABLE IF NOT EXISTS `buy_price_line` (
  `buy_price_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `buy_price_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`buy_price_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `buy_price_line`
--

INSERT INTO `buy_price_line` (`buy_price_line_id`, `buy_price_id`, `product_id`, `product_price`, `create_time`, `last_update`, `status`) VALUES
(1, 1, 5, 500000, 1448986660, NULL, 0),
(2, 1, 4, 400000, 1448986660, NULL, 0),
(3, 1, 3, 300000, 1448986660, NULL, 0),
(4, 1, 2, 200000, 1448986660, NULL, 0),
(5, 1, 1, 100000, 1448986660, NULL, 0),
(6, 2, 5, 450000, 1448986713, NULL, 0),
(7, 2, 4, 350000, 1448986713, NULL, 0),
(8, 2, 3, 250000, 1448986713, NULL, 0),
(9, 2, 2, 150000, 1448986713, NULL, 0),
(10, 2, 1, 130000, 1448986713, NULL, 0),
(11, 3, 5, 490000, 1448986768, NULL, 0),
(12, 3, 4, 390000, 1448986768, NULL, 0),
(13, 3, 3, 290000, 1448986768, NULL, 0),
(14, 3, 2, 190000, 1448986768, NULL, 0),
(15, 3, 1, 140000, 1448986768, NULL, 0),
(16, 6, 7, 600000, 1450700701, NULL, 0),
(17, 7, 8, 600000, 1450841232, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `category_type_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `category_type_id` (`category_type_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`category_type_id`) REFER `demo_befu' AUTO_INCREMENT=27 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `status`, `create_time`, `last_update`, `category_type_id`) VALUES
(1, 'Khách lẻ', 0, 1439103091, 1439174134, 1),
(2, 'Nhà cung cấp', 0, 1439103103, 1439110212, 1),
(3, 'Khách hàng công ty', 0, 1439103120, 1439110202, 1),
(4, 'Khối Kinh doanh', 0, 1439103512, 1439118939, 3),
(5, 'Làm đẹp', 0, 1439110749, NULL, 2),
(6, 'Sức khỏe', 0, 1439110854, NULL, 2),
(7, 'Dầu gội', 0, 1439110884, NULL, 2),
(8, 'Xà phòng', 0, 1439110893, NULL, 2),
(9, 'Mỹ phẩm', 0, 1439110913, NULL, 2),
(10, 'Khối văn phòng', 0, 1439124758, NULL, 3),
(11, 'Khối phụ trợ', 0, 1439124777, NULL, 3),
(12, 'Dưỡng da', 0, 1439125049, 1439125144, 2),
(13, 'Dưỡng tóc', 0, 1439125057, NULL, 2),
(14, 'Móng', 0, 1439125099, NULL, 2),
(15, 'Cửa hàng', 0, 1439129491, NULL, 3),
(16, 'Thuốc tăng cơ', 0, 1439709469, 1439709482, 2),
(17, 'Khách hàng', 0, 1439721865, NULL, 1),
(18, 'Khối sản xuất', 0, 1439976191, 1439976230, 3),
(19, 'Test category', 0, 1439976263, 1439976858, 5),
(20, 'Dưỡng da 2', 0, 1442393639, 1443064584, 2),
(21, 'test123', 1, 1442393664, 1442393899, 2),
(22, '', 1, 1442497955, 1442500461, 2),
(23, 'helllo', 0, 1442498093, NULL, 2),
(24, 'dffdsfsfd', 1, 1442499425, 1442499786, 2),
(25, 'aa', 1, 1442499456, 1442500465, 2),
(26, 'New category', 1, 1442500474, 1442500485, 2);

-- --------------------------------------------------------

--
-- Table structure for table `category_relation`
--

CREATE TABLE IF NOT EXISTS `category_relation` (
  `category_relation_id` int(10) NOT NULL AUTO_INCREMENT,
  `child_id` int(10) DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`category_relation_id`),
  UNIQUE KEY `parent_id` (`parent_id`,`child_id`) USING BTREE,
  KEY `child_id` (`child_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`parent_id`) REFER `demo_beful2/cate' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `category_relation`
--

INSERT INTO `category_relation` (`category_relation_id`, `child_id`, `parent_id`, `status`, `create_time`, `last_update`) VALUES
(1, 13, 5, 0, 1439110926, 1439125322),
(2, 9, 5, 0, 1439110927, 1439125290),
(3, 8, 6, 0, 1439112411, NULL),
(4, 14, 9, 0, 1439117907, 1439125234),
(5, 15, 4, 0, 1439129503, NULL),
(6, 16, 5, 0, 1439709501, NULL),
(7, 16, 6, 0, 1439710254, NULL),
(8, 12, 6, 0, 1439710277, NULL),
(9, 12, 5, 0, 1439710295, NULL),
(10, 3, 17, 0, 1439721906, NULL),
(11, 1, 17, 0, 1439721911, NULL),
(12, 5, 6, 1, 1442395144, 1442395344);

-- --------------------------------------------------------

--
-- Table structure for table `category_type`
--

CREATE TABLE IF NOT EXISTS `category_type` (
  `category_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_type_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) NOT NULL,
  PRIMARY KEY (`category_type_id`),
  UNIQUE KEY `category_type_name` (`category_type_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `category_type`
--

INSERT INTO `category_type` (`category_type_id`, `category_type_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'partner', 0, 1439109826, 0),
(2, 'product', 0, 1439109841, 1439972866),
(3, 'Bộ máy tổ chức Beful', 0, 1439118874, 1439701877),
(5, 'test cat type', 1, 1439976371, 1439976637),
(6, 'quotation', 0, 1440061499, 0);

-- --------------------------------------------------------

--
-- Table structure for table `changing_policy`
--

CREATE TABLE IF NOT EXISTS `changing_policy` (
  `changing_policy_id` int(10) NOT NULL AUTO_INCREMENT,
  `changing_policy_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`changing_policy_id`),
  UNIQUE KEY `changing_policy_name` (`changing_policy_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(10) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_name` (`city_name`) USING BTREE,
  KEY `country_id` (`country_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`country_id`) REFER `demo_beful2/cou' AUTO_INCREMENT=63 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `country_id`, `status`, `create_time`, `last_update`) VALUES
(1, 'Hà Nội', 1, 0, 1439133737, NULL),
(2, 'Đà Nẵng', 1, 0, 1439133746, NULL),
(3, 'TP Hồ Chí Minh', 1, 0, 1439133754, NULL),
(4, 'An Giang', 1, 0, 1443027102, NULL),
(5, 'Bà Rịa Vũng Tàu', 1, 0, 1443027113, NULL),
(6, 'Bình Dương', 1, 0, 1443027129, NULL),
(7, 'Bình Phước', 1, 0, 1443027169, NULL),
(8, 'Bình Thuận', 1, 0, 1443027178, NULL),
(9, 'Bình Định', 1, 0, 1443027188, NULL),
(10, 'Bắc Giang', 1, 0, 1443027238, NULL),
(11, 'Bắc Kạn', 1, 0, 1443027245, NULL),
(12, 'Bắc Ninh', 1, 0, 1443027253, NULL),
(13, 'Bến Tre', 1, 0, 1443027262, NULL),
(14, 'Cao Bằng', 1, 0, 1443027271, NULL),
(15, 'Cà Mau', 1, 0, 1443027279, NULL),
(16, 'Cần Thơ', 1, 0, 1443027521, NULL),
(17, 'Gia Lai', 1, 0, 1443027530, NULL),
(18, 'Hà Giang', 1, 0, 1443027541, NULL),
(19, 'Hà Nam', 1, 0, 1443027549, NULL),
(20, 'Hà Tĩnh', 1, 0, 1443027560, NULL),
(21, 'Hòa Bình', 1, 0, 1443027568, NULL),
(22, 'Hưng Yên', 1, 0, 1443027627, NULL),
(23, 'Hải Dương', 1, 0, 1443027635, NULL),
(24, 'Hải Phòng', 5, 1, 1443027641, 1443027656),
(25, 'Khánh Hòa', 1, 0, 1443027734, NULL),
(26, 'Kiên Giang', 1, 0, 1443027744, NULL),
(27, 'Kon Tum', 1, 0, 1443027752, NULL),
(28, 'Lai Châu', 1, 0, 1443027759, NULL),
(29, 'Long An', 1, 0, 1443027767, NULL),
(30, 'Lào Cai', 1, 0, 1443027781, NULL),
(31, 'Lâm Đồng', 1, 0, 1443027790, NULL),
(32, 'Lạng Sơn', 1, 0, 1443027800, NULL),
(33, 'Nam Định', 1, 0, 1443027807, NULL),
(34, 'Nghệ An', 1, 0, 1443027816, NULL),
(35, 'Ninh Bình', 1, 0, 1443027824, NULL),
(36, 'Ninh Thuận', 1, 0, 1443027830, NULL),
(37, 'Phú Thọ', 1, 0, 1443027837, NULL),
(38, 'Phú Yên', 1, 0, 1443027845, NULL),
(39, 'Quảng Nam', 1, 0, 1443027852, NULL),
(40, 'Quảng Ngãi', 1, 0, 1443027861, NULL),
(41, 'Quảng Ninh', 1, 0, 1443027869, NULL),
(42, 'Quảng Trị', 1, 0, 1443027877, NULL),
(43, 'Sơn La', 1, 0, 1443027884, NULL),
(44, 'Thanh Hóa', 1, 0, 1443027890, NULL),
(45, 'Thái Bình', 1, 0, 1443027898, NULL),
(46, 'Thái Nguyên', 1, 0, 1443027906, NULL),
(47, 'Thừa Thiên Huế', 1, 0, 1443027913, NULL),
(48, 'Tiền Giang', 1, 0, 1443027921, NULL),
(49, 'Trà Vinh', 1, 0, 1443027928, NULL),
(50, 'Tuyên Quang', 1, 0, 1443027934, NULL),
(51, 'Tây Ninh', 1, 0, 1443027941, NULL),
(52, 'Vĩnh Long', 1, 0, 1443027949, NULL),
(53, 'Vĩnh Phúc', 1, 0, 1443027956, NULL),
(54, 'Yên Bái', 1, 0, 1443027965, NULL),
(55, 'Đắk Lắk', 1, 0, 1443027973, NULL),
(56, 'Đồng Nai', 1, 0, 1443027980, NULL),
(57, 'Đồng Tháp', 1, 0, 1443027988, NULL),
(58, 'Bạc Liêu', 1, 0, 1443027996, NULL),
(59, 'Sóc Trăng', 1, 0, 1443028004, NULL),
(60, 'Hậu Giang', 1, 0, 1443028012, NULL),
(61, 'Đắk Nông', 1, 0, 1443028018, NULL),
(62, 'Điện Biên', 1, 0, 1443028028, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(10) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_name` (`country_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'Việt Nam', 0, 1439133060, NULL),
(2, 'Mỹ', 0, 1439133076, 1439133116),
(3, 'Nhật Bản', 0, 1439133097, NULL),
(4, 'Hàn Quốc', 0, 1439133101, NULL),
(5, 'Thái Lan', 0, 1439133106, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency_unit`
--

CREATE TABLE IF NOT EXISTS `currency_unit` (
  `currency_unit_id` int(10) NOT NULL AUTO_INCREMENT,
  `currency_unit_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`currency_unit_id`),
  UNIQUE KEY `currency_unit_name` (`currency_unit_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `currency_unit`
--

INSERT INTO `currency_unit` (`currency_unit_id`, `currency_unit_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'Euro', 0, 1439348840, 1439349130),
(2, 'dollar', 0, 1439348901, NULL),
(3, 'Dong', 0, 1439348918, 1439349123);

-- --------------------------------------------------------

--
-- Table structure for table `customer_pay`
--

CREATE TABLE IF NOT EXISTS `customer_pay` (
  `pay_id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `have_to_pay` int(11) NOT NULL,
  `pay_money` int(11) DEFAULT NULL,
  `owed_money` int(11) DEFAULT NULL,
  `pay_date` int(10) DEFAULT NULL,
  `pay_status_id` int(10) DEFAULT NULL,
  `pay_type_id` int(10) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`pay_status_id`) REFER `hoang_beful/' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `customer_pay`
--

INSERT INTO `customer_pay` (`pay_id`, `pay_code`, `customer_id`, `have_to_pay`, `pay_money`, `owed_money`, `pay_date`, `pay_status_id`, `pay_type_id`, `create_time`, `last_update`, `status`) VALUES
(1, 'TT-1', 8, 23344600, 22000000, NULL, 1450976400, 3, 1, 1451025643, 1451025687, 0),
(2, 'TT-2', 8, 1344600, 1300000, NULL, 1450976400, 3, 1, 1451025749, 1451025768, 0),
(3, 'TT-3', 0, 0, 0, NULL, 1451025772, 1, 0, 1451025772, NULL, 0),
(4, 'TT-4', 0, 0, 0, NULL, 1451025821, 1, 0, 1451025821, NULL, 0),
(5, 'TT-5', 0, 0, 0, NULL, 1451025922, 1, 0, 1451025922, NULL, 0),
(6, 'TT-6', 0, 0, 0, NULL, 1451026834, 1, 0, 1451026834, NULL, 0),
(7, 'TT-7', 8, 44600, 44600, NULL, 1450976400, 3, 1, 1451026889, 1451026922, 0),
(8, 'TT-8', 0, 0, 0, NULL, 1451026927, 1, 0, 1451026927, NULL, 0),
(9, 'TT-9', 3, 21600000, 21000000, NULL, 1450976400, 3, 1, 1451026971, 1451026985, 0),
(10, 'TT-10', 3, 600000, 600000, NULL, 1450976400, 3, 2, 1451026987, 1451026998, 0);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `district_id` int(10) NOT NULL AUTO_INCREMENT,
  `district_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`district_id`),
  UNIQUE KEY `district_name` (`district_name`) USING BTREE,
  KEY `city_id` (`city_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`city_id`) REFER `demo_beful2/city`(' AUTO_INCREMENT=38 ;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`district_id`, `district_name`, `city_id`, `status`, `create_time`, `last_update`) VALUES
(1, 'Quận Ba Đình', 1, 0, 1439133987, NULL),
(2, 'Quận Hoàng Mai', 1, 0, 1439133996, NULL),
(3, 'Quận 1', 3, 0, 1439134027, NULL),
(4, 'Quận 2', 3, 0, 1439134031, NULL),
(5, 'Quận Hoàn Kiếm', 1, 0, 1439981180, NULL),
(6, 'Ba Đình', 1, 1, 1443029419, 1443029432),
(7, 'Quận Bắc Từ Liêm', 1, 0, 1443029458, NULL),
(8, 'Quận Cầu Giấy', 1, 0, 1443029472, NULL),
(9, 'Quận Đống Đa', 1, 0, 1443029488, NULL),
(10, 'Quận Hà Đông', 1, 0, 1443029515, NULL),
(11, 'Quận Hai Bà Trưng', 1, 0, 1443029528, NULL),
(12, 'Quận Long Biên', 1, 0, 1443029583, NULL),
(13, 'Quận Nam Từ Liêm', 1, 0, 1443029603, NULL),
(14, 'Quận Tây Hồ', 1, 0, 1443029616, NULL),
(15, 'Quận Thanh Xuân', 1, 0, 1443029627, NULL),
(16, 'Quận 12', 3, 0, 1443029756, NULL),
(17, 'Quận Thủ Đức', 3, 0, 1443029764, NULL),
(18, 'Quận 9', 3, 0, 1443029773, NULL),
(19, 'Quận Gò Vấp', 3, 0, 1443029782, NULL),
(20, 'Quận Bình Thạnh', 3, 0, 1443029799, NULL),
(21, 'Quận Tân Bình', 3, 0, 1443029811, NULL),
(22, 'Quận Tân Phú', 3, 0, 1443029820, NULL),
(23, 'Quận Phú Nhuận', 3, 0, 1443029830, NULL),
(24, 'Quận 3', 3, 0, 1443029841, NULL),
(25, 'Quận 10', 3, 0, 1443029850, 1443029861),
(26, 'Quận 11', 3, 0, 1443029876, NULL),
(27, 'Quận 4', 3, 0, 1443029884, NULL),
(28, 'Quận 5', 3, 0, 1443029893, NULL),
(29, 'Quận 6', 3, 0, 1443029905, NULL),
(30, 'Quận 8', 3, 0, 1443029914, NULL),
(31, 'Quận Bình Tân', 3, 0, 1443029924, NULL),
(32, 'Quận 7', 3, 0, 1443029933, NULL),
(33, 'Huyện Củ Chi', 3, 0, 1443029940, NULL),
(34, 'Huyện Hóc Môn', 3, 0, 1443029948, NULL),
(35, 'Huyện Bình Chánh', 3, 0, 1443029957, NULL),
(36, 'Huyện Nhà Bè', 3, 0, 1443029967, NULL),
(37, 'Huyện Cần Giờ', 3, 0, 1443029981, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_work`
--

CREATE TABLE IF NOT EXISTS `employee_work` (
  `employee_work_id` int(10) NOT NULL AUTO_INCREMENT,
  `workplace_id` int(10) DEFAULT NULL,
  `position_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `start_workdate` int(10) DEFAULT NULL,
  `end_workdate` int(10) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) NOT NULL,
  PRIMARY KEY (`employee_work_id`),
  UNIQUE KEY `user_id` (`user_id`,`position_id`,`workplace_id`,`start_workdate`) USING BTREE,
  KEY `position_id` (`position_id`) USING BTREE,
  KEY `workplace_id` (`workplace_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`user_id`) REFER `demo_beful2/user`(' AUTO_INCREMENT=52 ;

--
-- Dumping data for table `employee_work`
--

INSERT INTO `employee_work` (`employee_work_id`, `workplace_id`, `position_id`, `user_id`, `start_workdate`, `end_workdate`, `status`, `create_time`, `last_update`) VALUES
(1, 3, 5, 12, 1, 0, 0, 1439006039, 1439006460),
(2, 2, 5, 11, 2, 0, 0, 1439006108, 1439006473),
(3, 3, 4, 9, NULL, 1438876800, 0, 1439006440, 1439008245),
(4, 2, 5, 1, 1430064000, 0, 0, 1439006487, 1439008555),
(5, 5, 4, 12, NULL, 0, 0, 1439870490, 1445446788),
(6, 3, 5, 11, NULL, 0, 0, 1442504543, 1442507707),
(7, 12, 7, 12, NULL, 0, 0, 1442504791, 1442527767),
(8, 12, 6, 9, NULL, 0, 0, 1442542060, 0),
(9, 3, 5, 1, NULL, 0, 1, 1442574947, 1442654075),
(10, 3, 4, 1, NULL, 0, 1, 1442653887, 1442657170),
(11, 3, 2, 1, NULL, 0, 0, 1442656989, 0),
(12, 16, 4, 12, NULL, 0, 0, 1443021165, 1443024150),
(13, 9, 4, 12, NULL, 0, 0, 1443021587, 1445446732),
(14, 18, 4, 5, NULL, 0, 0, 1443021849, 0),
(15, 18, 4, 12, NULL, 0, 0, 1443021855, 0),
(16, 18, 4, 12, NULL, 0, 0, 1443022093, 0),
(17, 21, 4, 1, NULL, 0, 1, 1443022888, 1443023283),
(18, 21, 4, 12, NULL, 0, 0, 1443023132, 0),
(19, 19, 4, 11, NULL, 0, 0, 1443024059, 0),
(20, 17, 4, 1, NULL, 0, 0, 1443024093, 1443024134),
(21, 20, 4, 1, NULL, 0, 0, 1443024159, 1443024167),
(22, 22, 4, 1, NULL, 0, 0, 1443024205, 1445446705),
(23, 23, 4, 11, NULL, 0, 0, 1443024291, 1445446670),
(24, 12, 7, 13, NULL, 0, 0, 1443666006, 1443759973),
(25, 12, 7, 14, NULL, 0, 0, 1443683340, 1443866267),
(26, 23, 5, 16, NULL, 0, 0, 1443866694, 0),
(27, 24, 4, 16, NULL, 0, 0, 1443874970, 0),
(28, 25, 4, 16, NULL, 0, 0, 1443874971, 0),
(29, 25, 7, 9, NULL, 0, 0, 1443880357, 0),
(30, 8, 4, 16, NULL, 0, 0, 1445446757, 0),
(31, 2, 7, 17, NULL, 0, 0, 1437249898, 0),
(32, 23, 7, 17, NULL, 0, 0, 1448512856, 0),
(33, 22, 7, 17, NULL, 0, 0, 1448512860, 0),
(34, 20, 7, 17, NULL, 0, 0, 1448512862, 0),
(35, 19, 7, 17, NULL, 0, 0, 1448512865, 0),
(36, 17, 7, 17, NULL, 0, 0, 1448512867, 0),
(37, 16, 7, 17, NULL, 0, 0, 1448512869, 0),
(38, 11, 7, 17, NULL, 0, 0, 1448512873, 0),
(39, 10, 7, 17, NULL, 0, 0, 1448512875, 0),
(40, 9, 7, 17, NULL, 0, 0, 1448512877, 0),
(41, 8, 7, 17, NULL, 0, 0, 1448512879, 0),
(42, 5, 7, 17, NULL, 0, 0, 1448512882, 0),
(43, 3, 7, 17, NULL, 0, 0, 1448512884, 0),
(44, 26, 4, 16, NULL, 0, 0, 1448513490, 1449300639),
(45, 27, 4, 11, NULL, 0, 0, 1448513525, 1449300635),
(46, 28, 4, 16, NULL, 0, 0, 1448513558, 1449300630),
(47, 28, 7, 17, NULL, 0, 0, 1448514623, 0),
(48, 27, 7, 17, NULL, 0, 0, 1448514625, 0),
(49, 26, 7, 17, NULL, 0, 0, 1448514627, 0),
(50, 12, 7, 17, NULL, 0, 0, 1448514631, 0),
(51, 28, 7, 13, NULL, 0, 0, 1448588792, 0);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `expense_id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `expense_date` int(10) DEFAULT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_type_id` int(10) DEFAULT NULL,
  `expense_status_id` int(10) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`pay_type_id`) REFER `hoang_beful/pa' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`expense_id`, `expense_code`, `partner_id`, `expense_date`, `receiver_name`, `pay_type_id`, `expense_status_id`, `description`, `create_time`, `last_update`, `status`) VALUES
(1, 'PC-1', 9, 1450976400, 'Ông ABC', 1, 2, 'Thu của ông abc', 1451029921, 1451029987, 0),
(2, 'PC-2', 0, 1451033794, '', 0, 1, '', 1451033794, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `expense_line`
--

CREATE TABLE IF NOT EXISTS `expense_line` (
  `expense_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_id` int(10) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`expense_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `expense_line`
--

INSERT INTO `expense_line` (`expense_line_id`, `expense_id`, `partner_id`, `money`, `description`, `create_time`, `last_update`, `status`) VALUES
(1, 1, 0, 50000, 'Nộp khoản xxxx', 1451029987, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `input_lines`
--

CREATE TABLE IF NOT EXISTS `input_lines` (
  `input_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `input_order_id` int(10) DEFAULT NULL,
  `warehouse_id` int(10) NOT NULL,
  `product_id` int(10) DEFAULT NULL,
  `input_line_amount` int(10) DEFAULT NULL,
  `buy_amount` int(11) NOT NULL DEFAULT '0',
  `product_price` int(10) DEFAULT NULL,
  `intended_date` int(10) DEFAULT NULL,
  `product_tax` int(10) DEFAULT NULL,
  `product_total_price` int(10) DEFAULT NULL,
  `expire_date` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  PRIMARY KEY (`input_line_id`),
  KEY `warehouse_id` (`warehouse_id`) USING BTREE,
  KEY `input_order_id` (`input_order_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`warehouse_id`) REFER `demo_beful2/w' AUTO_INCREMENT=27 ;

--
-- Dumping data for table `input_lines`
--

INSERT INTO `input_lines` (`input_line_id`, `input_order_id`, `warehouse_id`, `product_id`, `input_line_amount`, `buy_amount`, `product_price`, `intended_date`, `product_tax`, `product_total_price`, `expire_date`, `last_update`, `create_time`, `status`) VALUES
(1, 2, 1, 7, 5, 0, 600000, NULL, NULL, 3000000, NULL, NULL, 1450700774, 0),
(2, 3, 1, 7, 5, 4, 600000, NULL, NULL, 3000000, 0, 1450701256, 1450700840, 0),
(3, 4, 2, 7, 2, 1, 600000, NULL, NULL, 1200000, NULL, 1450703081, 1450700989, 0),
(4, 7, 1, 7, 1, 0, 600000, NULL, NULL, 600000, 0, 1450701523, 1450701520, 0),
(5, 9, 3, 5, 20, 0, 450000, NULL, NULL, 9000000, NULL, NULL, 1450804917, 0),
(6, 9, 3, 4, 20, 0, 350000, NULL, NULL, 7000000, NULL, NULL, 1450804917, 0),
(7, 9, 3, 3, 20, 0, 250000, NULL, NULL, 5000000, NULL, NULL, 1450804917, 0),
(8, 9, 3, 2, 20, 0, 150000, NULL, NULL, 3000000, NULL, NULL, 1450804917, 0),
(9, 9, 3, 1, 20, 0, 130000, NULL, NULL, 2600000, NULL, NULL, 1450804917, 0),
(10, 10, 3, 1, 20, 20, 130000, NULL, NULL, 2600000, 1483203600, 1450806779, 1450806709, 0),
(11, 10, 3, 2, 20, 20, 150000, NULL, NULL, 3000000, 1483203600, 1450806779, 1450806709, 0),
(12, 10, 3, 3, 20, 20, 250000, NULL, NULL, 5000000, 1483203600, 1450806779, 1450806709, 0),
(13, 10, 3, 4, 20, 20, 350000, NULL, NULL, 7000000, 1483203600, 1450806779, 1450806709, 0),
(14, 10, 3, 5, 20, 20, 450000, NULL, NULL, 9000000, 1483203600, 1450806779, 1450806709, 0),
(15, 11, 3, 5, 20, 0, 500000, NULL, NULL, 10000000, NULL, NULL, 1450807281, 0),
(16, 11, 3, 4, 20, 0, 400000, NULL, NULL, 8000000, NULL, NULL, 1450807281, 0),
(17, 11, 3, 3, 20, 0, 300000, NULL, NULL, 6000000, NULL, NULL, 1450807281, 0),
(18, 12, 3, 3, 20, 20, 300000, NULL, NULL, 6000000, 1514739600, 1450807412, 1450807302, 0),
(19, 12, 3, 4, 20, 20, 400000, NULL, NULL, 8000000, 1514739600, 1450807412, 1450807302, 0),
(20, 12, 3, 5, 20, 20, 500000, NULL, NULL, 10000000, 1514739600, 1450807412, 1450807302, 0),
(21, 13, 3, 3, 10, 10, 300000, NULL, NULL, 3000000, 1514739600, 1450809257, 1450807620, 0),
(22, 13, 3, 4, 10, 10, 400000, NULL, NULL, 4000000, 1514739600, 1450809257, 1450807620, 0),
(23, 13, 3, 5, 10, 10, 500000, NULL, NULL, 5000000, 1514739600, 1450809257, 1450807620, 0),
(24, 15, 1, 8, 5, 0, 600000, NULL, NULL, 3000000, NULL, NULL, 1450841272, 0),
(25, 16, 1, 8, 5, 2, 600000, NULL, NULL, 3000000, 0, 1450841413, 1450841286, 0),
(26, 17, 1, 8, 1, 0, 600000, NULL, NULL, 600000, 0, 1450842226, 1450842225, 0);

-- --------------------------------------------------------

--
-- Table structure for table `input_order`
--

CREATE TABLE IF NOT EXISTS `input_order` (
  `input_order_id` int(10) NOT NULL AUTO_INCREMENT,
  `input_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `input_date` int(10) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `status_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `total_sale_no_tax` int(10) DEFAULT NULL,
  `purchase_order` int(10) DEFAULT NULL,
  `input_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_sale` int(10) DEFAULT NULL,
  `intended_date` int(10) DEFAULT NULL,
  PRIMARY KEY (`input_order_id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `partner_id` (`partner_id`) USING BTREE,
  KEY `status_id` (`status_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`user_id`) REFER `demo_beful2/user`(' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `input_order`
--

INSERT INTO `input_order` (`input_order_id`, `input_code`, `input_date`, `partner_id`, `status_id`, `user_id`, `status`, `create_time`, `last_update`, `total_sale_no_tax`, `purchase_order`, `input_note`, `total_sale`, `intended_date`) VALUES
(1, 'RO-1', 1450700081, NULL, 15, NULL, 0, 1450700081, 1450700081, NULL, NULL, NULL, NULL, 1450700081),
(2, 'RO-2', 1450630800, 7, 15, NULL, 3, 1450700720, 1450700780, NULL, NULL, 'Thanh toán sau khi nhận hoá đơn', 3000000, 1450630800),
(3, 'PO-3', 1450630800, 7, 14, NULL, 3, 1450700831, 1450700841, 3000000, 2, NULL, 3000000, NULL),
(4, 'PO-4', 1450630800, 26, 4, NULL, 3, 1450700989, 1450700992, 0, NULL, 'Chuyển kho', 1200000, NULL),
(5, 'PO-5', 1450701041, NULL, 5, NULL, 0, 1450701041, 1450701041, 0, NULL, NULL, 0, NULL),
(6, 'PO-6', 1450701313, NULL, 5, NULL, 0, 1450701313, 1450701313, 0, NULL, NULL, 0, NULL),
(7, 'PO-7', 1450630800, 27, 5, NULL, 3, 1450701495, 1450701523, 0, NULL, NULL, 0, NULL),
(8, 'PO-8', 1450755388, NULL, 14, NULL, 0, 1450755388, 1450755388, 0, NULL, NULL, 0, NULL),
(9, 'RO-9', 1450803600, 5, 15, NULL, 3, 1450804884, 1450804919, NULL, NULL, NULL, 26600000, 1450803600),
(10, 'PO-10', 1450803600, 5, 14, NULL, 3, 1450806684, 1450806710, 26600000, 9, NULL, 26600000, NULL),
(11, 'RO-11', 1450803600, 6, 15, NULL, 3, 1450807260, 1450807282, NULL, NULL, NULL, 24000000, 1450803600),
(12, 'PO-12', 1450803600, 6, 14, NULL, 3, 1450807285, 1450807304, 24000000, 11, NULL, 24000000, NULL),
(13, 'PO-13', 1450803600, 27, 5, NULL, 3, 1450807600, 1450807622, 0, NULL, NULL, 0, NULL),
(14, 'RO-14', 1450841168, NULL, 15, NULL, 0, 1450841168, 1450841168, NULL, NULL, NULL, NULL, 1450841168),
(15, 'RO-15', 1450803600, 9, 15, NULL, 3, 1450841244, 1450841274, NULL, NULL, 'Thanh toán khi giao hàng', 3000000, 1450803600),
(16, 'PO-16', 1450803600, 9, 14, NULL, 3, 1450841280, 1450841288, 3000000, 15, NULL, 3000000, NULL),
(17, 'PO-17', 1450803600, 27, 5, NULL, 3, 1450842211, 1450842226, 0, NULL, NULL, 0, NULL),
(18, 'PO-18', 1451035384, NULL, 14, NULL, 0, 1451035384, 1451035384, 0, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `inventory_id` int(10) NOT NULL AUTO_INCREMENT,
  `inventory_date` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `opening_stock` int(10) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `inventory_amount` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `warehouse_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`inventory_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `warehouse_id` (`warehouse_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`product_id`) REFER `demo_befult_3/p' AUTO_INCREMENT=32 ;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inventory_id`, `inventory_date`, `order_id`, `product_id`, `opening_stock`, `amount`, `inventory_amount`, `status`, `description`, `create_time`, `last_update`, `warehouse_id`) VALUES
(1, 1450630800, 3, 7, 0, 5, 5, 0, 'Nhập kho', 1450700841, NULL, 1),
(2, 1450630800, 1, 7, 5, -2, 3, 0, 'Chuyển kho', 1450700992, NULL, 1),
(3, 1450630800, 4, 7, 0, 2, 2, 0, 'Chuyển kho', 1450700992, NULL, 2),
(4, 1450701252, 4, 7, 3, -2, 1, 0, 'Đơn hàng bán', 1450701256, NULL, 1),
(5, 1450630800, 7, 7, 1, 1, 2, 0, 'Hàng trả về', 1450701523, NULL, 1),
(6, 1450703079, 5, 7, 2, -1, 1, 0, 'Đơn hàng bán', 1450703081, NULL, 2),
(7, 1450803600, 10, 5, 0, 20, 20, 0, 'Nhập kho', 1450806710, NULL, 3),
(8, 1450803600, 10, 4, 0, 20, 20, 0, 'Nhập kho', 1450806710, NULL, 3),
(9, 1450803600, 10, 3, 0, 20, 20, 0, 'Nhập kho', 1450806710, NULL, 3),
(10, 1450803600, 10, 2, 0, 20, 20, 0, 'Nhập kho', 1450806710, NULL, 3),
(11, 1450803600, 10, 1, 0, 20, 20, 0, 'Nhập kho', 1450806710, NULL, 3),
(12, 1450806776, 7, 1, 20, -20, 0, 0, 'Đơn hàng bán', 1450806779, NULL, 3),
(13, 1450806776, 7, 2, 20, -20, 0, 0, 'Đơn hàng bán', 1450806779, NULL, 3),
(14, 1450806776, 7, 3, 20, -20, 0, 0, 'Đơn hàng bán', 1450806779, NULL, 3),
(15, 1450806776, 7, 4, 20, -20, 0, 0, 'Đơn hàng bán', 1450806779, NULL, 3),
(16, 1450806776, 7, 5, 20, -20, 0, 0, 'Đơn hàng bán', 1450806779, NULL, 3),
(17, 1450803600, 12, 5, 0, 20, 20, 0, 'Nhập kho', 1450807304, NULL, 3),
(18, 1450803600, 12, 4, 0, 20, 20, 0, 'Nhập kho', 1450807304, NULL, 3),
(19, 1450803600, 12, 3, 0, 20, 20, 0, 'Nhập kho', 1450807304, NULL, 3),
(20, 1450807410, 8, 3, 20, -20, 0, 0, 'Đơn hàng bán', 1450807412, NULL, 3),
(21, 1450807410, 8, 4, 20, -20, 0, 0, 'Đơn hàng bán', 1450807412, NULL, 3),
(22, 1450807410, 8, 5, 20, -20, 0, 0, 'Đơn hàng bán', 1450807412, NULL, 3),
(23, 1450803600, 13, 5, 0, 10, 10, 0, 'Hàng trả về', 1450807622, NULL, 3),
(24, 1450803600, 13, 4, 0, 10, 10, 0, 'Hàng trả về', 1450807622, NULL, 3),
(25, 1450803600, 13, 3, 0, 10, 10, 0, 'Hàng trả về', 1450807622, NULL, 3),
(26, 1450809255, 9, 3, 10, -10, 0, 0, 'Đơn hàng bán', 1450809257, NULL, 3),
(27, 1450809255, 9, 4, 10, -10, 0, 0, 'Đơn hàng bán', 1450809257, NULL, 3),
(28, 1450809255, 9, 5, 10, -10, 0, 0, 'Đơn hàng bán', 1450809257, NULL, 3),
(29, 1450803600, 16, 8, 0, 5, 5, 0, 'Nhập kho', 1450841288, NULL, 1),
(30, 1450841411, 12, 8, 5, -2, 3, 0, 'Đơn hàng bán', 1450841413, NULL, 1),
(31, 1450803600, 17, 8, 3, 1, 4, 0, 'Hàng trả về', 1450842226, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `url` varchar(256) NOT NULL,
  `view_path` varchar(256) DEFAULT NULL,
  `access` varchar(256) NOT NULL,
  `create_time` int(10) NOT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `name`, `url`, `view_path`, `access`, `create_time`, `last_update`) VALUES
(1, 0, 'BÁN HÀNG', 'ban-hang/don-hang', NULL, '1,2,3', 1437249898, NULL),
(2, 0, 'MUA HÀNG', 'mua-hang/don-hang', NULL, '1,2,3', 1437249898, NULL),
(3, 0, 'KHÁCH HÀNG', 'ql-khach-hang/khach-hang', NULL, '1,2,3', 1437249898, NULL),
(4, 0, 'QUẢN LÝ KHO', 'ql-kho/nhap-kho', NULL, '1,2,3', 1437249898, NULL),
(5, 0, 'KẾ TOÁN', 'ke-toan/don-hang', NULL, '1,2,3', 1437249898, NULL),
(6, 0, 'BÁO CÁO', 'bao-cao/top-san-pham', NULL, '1,2,3', 1437249898, NULL),
(7, 1, 'Đơn hàng', 'ban-hang/don-hang', 'sale management/order management/ViewList', '1,5,7', 1437249898, NULL),
(8, 1, 'Sản phẩm', 'ban-hang/san-pham', 'sale management/product management/ViewList', '1,5,7', 1437249898, NULL),
(9, 1, 'Khuyến mại', 'ban-hang/khuyen-mai', NULL, '1,5', 1437249898, NULL),
(10, 1, 'Quản lý giá', 'ban-hang/bao-gia', 'sale management/price management/ViewList', '1,5', 1437249898, NULL),
(11, 1, 'Hệ thống cửa hàng', 'ban-hang/cua-hang', NULL, '1,5', 1437249898, NULL),
(12, 2, 'Đơn đặt hàng', 'mua-hang/don-hang', 'buy management/order management/ViewList', '1,5,7', 1437249898, NULL),
(13, 2, 'Nhập kho', 'mua-hang/nhap-kho', 'buy management/import stock management/ViewList', '', 1437249898, NULL),
(14, 2, 'Chuyển kho', 'mua-hang/chuyen-kho', 'buy management/transfer stock managment/ViewList', '', 1437249898, NULL),
(15, 2, 'Trả về', 'mua-hang/tra-ve', 'buy management/refund product management/ViewRefund', '', 1437249898, NULL),
(16, 2, 'Nhà cung cấp', 'mua-hang/nha-cung-cap', 'buy management/suppliers management/ViewList', '', 1437249898, NULL),
(17, 3, 'Khách hàng', 'ql-khach-hang/khach-hang', 'customer management/customer/ViewCustomer', '', 1437249898, NULL),
(18, 3, 'Đơn hàng', 'ql-khach-hang/don-hang', 'customer management/order/ViewOrder', '', 1437249898, NULL),
(19, 4, '<b>Phiếu nhập / Xuất kho</b>', '', NULL, '', 1437249898, NULL),
(20, 19, 'Nhập kho', 'ql-kho/nhap-kho', 'stock management/import export/ViewImport', '', 1437249898, NULL),
(21, 19, 'Đơn hàng / Xuất kho', 'ql-kho/xuat-kho', 'stock management/import export/ViewExport', '', 1437249898, NULL),
(22, 19, 'Chuyển kho', 'ql-kho/chuyen-kho', 'stock management/transfer stock managment/ViewList', '', 1437249898, NULL),
(23, 19, 'Hàng trả về', 'ql-kho/tra-ve', 'stock management/refund product management/ViewRefund', '', 1437249898, NULL),
(24, 4, '<b>Kiểm kê kho hàng</b>', '', NULL, '', 1437249898, NULL),
(25, 4, '<b>Sản phẩm</b>', '', NULL, '', 0, NULL),
(26, 4, '<b>Kho hàng</b>', 'ql-kho/kho-hang', 'stock management/warehouse/ViewWarehouse', '', 1437249898, NULL),
(28, 24, 'Điều chỉnh tồn kho', 'ql-kho/ton-kho', 'stock management/inventory/ViewList', '', 1437249898, NULL),
(29, 25, 'Sản phẩm', 'ql-kho/san-pham', 'stock management/product/ViewProduct', '', 1437249898, NULL),
(30, 25, 'Nhóm sản phẩm', 'ql-kho/nhom-san-pham', 'stock management/product/ViewProductCategory', '', 1437249898, NULL),
(31, 25, 'Đơn vị tính', 'ql-kho/don-vi-san-pham', 'stock management/product/ViewUnit', '', 1437249898, NULL),
(32, 5, '<b>Khách hàng</b>', '', NULL, '', 1437249898, NULL),
(33, 5, '<b>Nhà cung cấp</b>', '', NULL, '', 0, NULL),
(35, 5, '<b>Ngần hàng & tiền mặt</b>', '', NULL, '', 1437249898, NULL),
(36, 5, '<b>Hạch toán tài khoản</b>', '', NULL, '', 1437249898, NULL),
(37, 32, 'Đơn hàng', 'ke-toan/don-hang', 'account/customer/order/ViewOrder', '', 1437249898, NULL),
(38, 32, 'Hàng trả lại', 'ke-toan/tra-ve', 'account/customer/refund/ViewRefund', '', 1437249898, NULL),
(39, 32, 'Khách hàng thanh toán', 'ke-toan/khach-hang-thanh-toan', 'account/customer/pay/ViewList', '', 1437249898, NULL),
(40, 33, 'Đơn hàng mua', 'ke-toan/don-hang-mua', 'account/supplier/order/ViewList', '', 1437249898, NULL),
(41, 33, 'Thanh toán nhà cung cấp', 'ke-toan/thanh-toan-nha-cung-cap', 'account/supplier/pay/ViewList', '', 1437249898, NULL),
(42, 35, 'Phiếu thu', 'ke-toan/phieu-thu', 'account/bank money/receipts/ViewList', '', 1437249898, NULL),
(43, 35, 'Phiếu chi', 'ke-toan/phieu-chi', 'account/bank money/expenses/ViewList', '', 1437249898, NULL),
(44, 36, 'TK phải thu', 'ke-toan/tai-khoan-phai-thu-khach-hang', 'account/accounting/ViewHaveToThu', '', 1437249898, NULL),
(45, 36, 'TK phải trả', 'ke-toan/tai-khoan-phai-tra-nha-cung-cap', 'account/accounting/ViewHaveToPay', '', 1437249898, NULL),
(46, 36, 'TK tiền mặt', 'ke-toan/tien-mat', 'account/accounting/Money', '', 1437249898, NULL),
(47, 36, 'TK ngân hàng', 'ke-toan/ngan-hang', 'account/accounting/Bank', '', 1437249898, NULL),
(48, 6, 'Biều đồ', '', NULL, '', 1437249898, NULL),
(49, 48, 'Top sản phẩm', 'bao-cao/top-san-pham', 'report/chart/TopProduct', '', 1437249898, NULL),
(50, 48, 'Top khách hàng', 'bao-cao/top-khach-hang', 'report/chart/TopCustomer', '', 1437249898, NULL),
(51, 48, 'Top nhân viên bán hàng', 'bao-cao/top-nhan-vien-ban-hang', 'report/chart/TopSaler', '', 1437249898, NULL),
(52, 48, 'Top nhập kho', 'bao-cao/top-nhap-kho', 'report/chart/TopImport', '', 1437249898, NULL),
(53, 6, 'Doanh thu bán hàng', 'bao-cao/doanh-thu-ban-hang', 'report/income/ViewList', '', 1437249898, NULL),
(54, 6, 'Kho hàng', '', '', '', 1437249898, NULL),
(56, 54, 'Bảng kê nhập kho', 'bao-cao/bang-ke-nhap-kho', 'report/warehouse/Import', '', 1437249898, NULL),
(57, 54, 'Bảng kê xuất kho', 'bao-cao/bang-ke-xuat-kho', 'report/warehouse/Export', '', 1437249898, NULL),
(58, 54, 'Hàng tồn kho - HSD', 'bao-cao/hang-ton-kho-hsd', 'report/warehouse/Inventory', '', 1437249898, NULL),
(59, 54, 'Bảng kê nhập - xuất', 'bao-cao/bang-ke-nhap-xuat', 'report/warehouse/ImportExport', '', 1437249898, NULL),
(60, 5, '<b>Quản lý tài khoản ngân hàng</b>', 'ke-toan/tai-khoan-ngan-hang', 'account/bank/ViewList', '', 1437249898, NULL),
(61, 2, 'Bảng giá mua', 'mua-hang/bang-gia-mua', 'buy management/price management/ViewList', '', 1437249898, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `opening_stock`
--

CREATE TABLE IF NOT EXISTS `opening_stock` (
  `opening_stock_id` int(10) NOT NULL AUTO_INCREMENT,
  `opening_stock_date` int(10) DEFAULT NULL,
  `input_line_id` int(10) DEFAULT NULL,
  `opening_stock_amount` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`opening_stock_id`),
  UNIQUE KEY `opening_stock_date` (`opening_stock_date`,`input_line_id`) USING BTREE,
  KEY `input_line_id` (`input_line_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`input_line_id`) REFER `demo_befult_' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `output_lines`
--

CREATE TABLE IF NOT EXISTS `output_lines` (
  `output_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `output_line_amount` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `product_price` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `product_tax_id` int(10) DEFAULT NULL,
  `product_discount` int(10) DEFAULT NULL,
  `product_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(10) DEFAULT NULL,
  `input_line_id` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `input_lines` text COLLATE utf8_unicode_ci,
  `product_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`output_line_id`),
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `product_tax_id` (`product_tax_id`) USING BTREE,
  KEY `warehouse_id` (`warehouse_id`) USING BTREE,
  KEY `input_line_id` (`input_line_id`(333)) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`output_id`) REFER `demo_beful2/outp' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `output_lines`
--

INSERT INTO `output_lines` (`output_line_id`, `output_line_amount`, `order_id`, `product_price`, `status`, `create_time`, `last_update`, `product_tax_id`, `product_discount`, `product_note`, `warehouse_id`, `input_line_id`, `input_lines`, `product_id`) VALUES
(1, 2, 1, 600000, 0, 1450700989, NULL, NULL, 0, NULL, 1, NULL, '[{"product_id":"7","input_line_id":"2","amount":"2"}]', 7),
(2, 2, 4, 1450000, 0, 1450701252, NULL, NULL, 0, NULL, 1, NULL, '[{"product_id":"7","input_line_id":"2","amount":"2"}]', 7),
(3, 1, 5, 1450000, 0, 1450703079, NULL, NULL, 0, NULL, 2, NULL, '[{"product_id":"7","input_line_id":"3","amount":"1"}]', 7),
(4, 10, 6, 900000, 1, 1450755360, 1450755399, NULL, 0, NULL, 3, NULL, NULL, 5),
(5, 10, 6, 800000, 1, 1450755360, 1450755399, NULL, 0, NULL, 3, NULL, NULL, 4),
(6, 10, 6, 700000, 1, 1450755360, 1450755399, NULL, 0, NULL, 3, NULL, NULL, 3),
(7, 20, 7, 900000, 0, 1450806777, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"5","input_line_id":"14","amount":20}]', 5),
(8, 20, 7, 800000, 0, 1450806777, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"4","input_line_id":"13","amount":20}]', 4),
(9, 20, 7, 700000, 0, 1450806777, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"3","input_line_id":"12","amount":20}]', 3),
(10, 20, 7, 600000, 0, 1450806777, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"2","input_line_id":"11","amount":20}]', 2),
(11, 20, 7, 500000, 0, 1450806777, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"1","input_line_id":"10","amount":20}]', 1),
(12, 20, 8, 900000, 0, 1450807410, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"5","input_line_id":"20","amount":20}]', 5),
(13, 20, 8, 800000, 0, 1450807410, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"4","input_line_id":"19","amount":20}]', 4),
(14, 20, 8, 700000, 0, 1450807410, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"3","input_line_id":"18","amount":20}]', 3),
(15, 10, 9, 900000, 0, 1450809256, NULL, NULL, 6, NULL, 3, NULL, '[{"product_id":"5","input_line_id":"23","amount":10}]', 5),
(16, 10, 9, 800000, 0, 1450809256, NULL, NULL, 6, NULL, 3, NULL, '[{"product_id":"4","input_line_id":"22","amount":10}]', 4),
(17, 10, 9, 700000, 0, 1450809256, NULL, NULL, 0, NULL, 3, NULL, '[{"product_id":"3","input_line_id":"21","amount":10}]', 3),
(18, 2, 12, 1080000, 0, 1450841411, NULL, NULL, 0, NULL, 1, NULL, '[{"product_id":"8","input_line_id":"25","amount":"2"}]', 8);

-- --------------------------------------------------------

--
-- Table structure for table `output_order`
--

CREATE TABLE IF NOT EXISTS `output_order` (
  `order_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_date` int(10) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `status_id` int(10) DEFAULT NULL,
  `salesperson_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `order_price` int(11) DEFAULT '0',
  `quotation_id` int(10) DEFAULT NULL,
  `output_note` text COLLATE utf8_unicode_ci,
  `order_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_start` int(10) DEFAULT NULL,
  `time_end` int(10) DEFAULT NULL,
  `shop_id` int(10) DEFAULT NULL,
  `promote_id` int(10) DEFAULT NULL,
  `discount` int(3) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `partner_id` (`partner_id`) USING BTREE,
  KEY `status_id` (`status_id`) USING BTREE,
  KEY `salesperson_id` (`salesperson_id`) USING BTREE,
  KEY `shop_id` (`shop_id`) USING BTREE,
  KEY `order_code` (`order_code`) USING BTREE,
  KEY `promote_id` (`promote_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`user_id`) REFER `demo_beful2/user`(' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `output_order`
--

INSERT INTO `output_order` (`order_id`, `order_code`, `order_date`, `partner_id`, `status_id`, `salesperson_id`, `status`, `create_time`, `last_update`, `order_price`, `quotation_id`, `output_note`, `order_name`, `time_start`, `time_end`, `shop_id`, `promote_id`, `discount`) VALUES
(1, 'CH-26-1', 1450630800, NULL, 4, NULL, 3, 1450700989, 1450700992, 1200000, 0, 'Chuyển kho', NULL, NULL, NULL, 26, NULL, NULL),
(2, '-2', 1450701059, NULL, 3, NULL, 0, 1450701059, 1450701059, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '-3', 1450701085, NULL, 3, NULL, 0, 1450701085, 1450701085, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'CH-26-4', 1450701252, 8, 3, 17, 3, 1450701188, 1450701252, 2900000, 1, NULL, NULL, NULL, NULL, 26, 4, 0),
(5, 'CH-26-5', 1450703079, 1, 3, 17, 3, 1450703046, 1450703079, 1450000, 1, NULL, NULL, NULL, NULL, 26, 4, 7),
(6, 'CH-26-6', 1450755360, 2, 3, 17, 1, 1450755269, 1450755399, 24000000, 1, NULL, NULL, NULL, NULL, 26, 2, 10),
(7, 'CH-26-7', 1450806776, 2, 3, 17, 3, 1450806726, 1450806777, 70000000, 1, NULL, NULL, NULL, NULL, 26, 3, 9),
(8, 'CH-26-8', 1450807410, 3, 3, 17, 3, 1450807374, 1450807410, 48000000, 1, NULL, NULL, NULL, NULL, 26, 2, 10),
(9, 'CH-26-9', 1450809255, 8, 3, 17, 3, 1450809232, 1450809256, 22980000, 1, NULL, NULL, NULL, NULL, 26, 3, 9),
(10, '-10', 1450809593, NULL, 3, NULL, 0, 1450809593, 1450809593, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '-11', 1450841295, NULL, 3, NULL, 0, 1450841295, 1450841295, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'CH-26-12', 1450841411, 8, 3, 17, 3, 1450841365, 1450841411, 2160000, 1, NULL, NULL, NULL, NULL, 26, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE IF NOT EXISTS `partner` (
  `partner_id` int(10) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_phone_number` text COLLATE utf8_unicode_ci,
  `partner_email` text COLLATE utf8_unicode_ci,
  `partner_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_tax_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `district_id` int(10) DEFAULT NULL,
  `contact_person_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person_phone` text COLLATE utf8_unicode_ci,
  `contact_person_email` text COLLATE utf8_unicode_ci,
  `partner_fax` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_number` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `partner_name` (`partner_name`) USING BTREE,
  KEY `district_id` (`district_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`district_id`) REFER `demo_beful2/di' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `partner`
--

INSERT INTO `partner` (`partner_id`, `partner_name`, `partner_phone_number`, `partner_email`, `partner_address`, `partner_tax_code`, `status`, `create_time`, `last_update`, `district_id`, `contact_person_name`, `contact_person_phone`, `contact_person_email`, `partner_fax`, `bank_number`, `bank_name`, `note`, `partner_code`) VALUES
(1, 'Công ty T&T Hà Nội', '0904374656', NULL, '22A- Hai Bà Trưng - Hoàn Kiếm', '09171570957057', 0, 1448986023, NULL, 5, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '7419474041401', 'Vietcombank', NULL, NULL),
(2, 'Công ty TNH Hà Nội', '0904374656', NULL, '43/4 Hoàng Hoa Thám', '75705920257257', 0, 1448986070, NULL, 1, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '90175097075', 'Vietinbank', NULL, NULL),
(3, 'Công ty TNHH Thương Mại và Dịch Vụ KS', '0904374656', NULL, 'VP3 - Linh Đàm - Hoàng Mai - Hà Nội', '097419641969146', 0, 1448986112, NULL, 2, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '1904174901471', 'Techcombank', NULL, NULL),
(4, 'Công ty TNHH Thương Mại KS', '0904374656', NULL, '123 Hai Bà Trưng', '19861956160099070', 0, 1448986161, NULL, 5, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '1986468914198', 'Ngân hàng HSBC', NULL, NULL),
(5, 'Công ty TNHH Thương Mại và Dịch Vụ Hoa Đất Việt', '0904374656', NULL, '43 ngõ 4, Hoàng Hoa Thám', '9879864861498614', 0, 1448986210, NULL, 1, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '244177725364', 'Ngân hàng Á Châu ACB', NULL, NULL),
(6, 'Công ty TNHH Thương Mại và Dịch Vụ Lâm Thành', '0904374656', NULL, 'VP6 - Linh Đàm - Hoàng Mai - Hà Nội', '09446545875849', 0, 1448986262, NULL, 2, 'Mr Tuấn', '0904374656', 'tuantd.tnh@gmail.com', '0904374656', '546754141888797', 'Agribank', NULL, NULL),
(7, 'KYOWA Co, Ltd', '0987654321', NULL, 'Japan', '342434325435', 0, 1450700623, 1451027040, 37, 'Mr Naruto', '0987654321', 'naruto@gmail.com', '3243242', '009988776655', 'Japan Bank', NULL, NULL),
(8, 'Nguyễn Văn A', '0999999999', NULL, '123 Trần Phú', '0123456789', 0, 1450701180, NULL, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'BACAI Inc', '0099887766', NULL, 'Arizona', '112233445566', 0, 1450841157, NULL, 37, 'Mr Yaiba', '7788990011', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partner_category`
--

CREATE TABLE IF NOT EXISTS `partner_category` (
  `partner_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `partner_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`partner_category_id`),
  UNIQUE KEY `partner_id` (`partner_id`,`category_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`partner_id`) REFER `demo_beful2/par' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `partner_category`
--

INSERT INTO `partner_category` (`partner_category_id`, `partner_id`, `category_id`, `status`, `create_time`, `last_update`) VALUES
(1, 1, 3, 0, 1448986023, NULL),
(2, 2, 3, 0, 1448986070, NULL),
(3, 3, 3, 0, 1448986112, NULL),
(4, 4, 2, 0, 1448986161, NULL),
(5, 5, 2, 0, 1448986210, NULL),
(6, 6, 2, 0, 1448986262, NULL),
(7, 7, 2, 0, 1450700623, NULL),
(8, 8, 3, 0, 1450701180, NULL),
(9, 9, 2, 0, 1450841157, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pay_line`
--

CREATE TABLE IF NOT EXISTS `pay_line` (
  `pay_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_id` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `have_to_pay` int(11) NOT NULL,
  `amount_paid` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `pay_status_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`pay_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pay_line`
--

INSERT INTO `pay_line` (`pay_line_id`, `pay_id`, `order_id`, `have_to_pay`, `amount_paid`, `create_time`, `last_update`, `pay_status_id`, `status`) VALUES
(1, 1, 4, 1450000, 1450000, 1451025683, NULL, 0, 0),
(2, 1, 9, 20911800, 20550000, 1451025683, NULL, 0, 0),
(3, 1, 12, 982800, 0, 1451025683, NULL, 0, 0),
(4, 2, 9, 361800, 361800, 1451025765, NULL, 0, 0),
(5, 2, 12, 982800, 938200, 1451025765, NULL, 0, 0),
(6, 7, 12, 44600, 44600, 1451026920, NULL, 0, 0),
(7, 9, 8, 21600000, 21000000, 1451026983, NULL, 0, 0),
(8, 10, 8, 600000, 600000, 1451026997, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pay_status`
--

CREATE TABLE IF NOT EXISTS `pay_status` (
  `pay_status_id` int(10) NOT NULL,
  `pay_status_name` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pay_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pay_status`
--

INSERT INTO `pay_status` (`pay_status_id`, `pay_status_name`) VALUES
(0, 'Hoạt động'),
(1, 'Lưu tạm'),
(2, 'Chưa thanh toán'),
(3, 'Đã xác nhận thanh toán'),
(4, 'Đang thiếu'),
(5, 'Hoạt động'),
(6, 'Không hoạt động');

-- --------------------------------------------------------

--
-- Table structure for table `pay_type`
--

CREATE TABLE IF NOT EXISTS `pay_type` (
  `pay_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_type_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pay_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pay_type`
--

INSERT INTO `pay_type` (`pay_type_id`, `pay_type_name`) VALUES
(1, 'Thanh toán bằng tiền mặt'),
(2, 'Thanh toán bằng tài khoản hàng');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `position_id` int(10) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `create_time` int(10) NOT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`position_id`),
  UNIQUE KEY `position_name` (`position_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'Trưởng phòng Marketing', 0, 1438767135, 1442473251),
(2, 'Kế toán kho', 0, 1438767152, 1442473225),
(3, 'Kế toán trưởng', 0, 1438767172, 1442473207),
(4, 'Quản lý cửa hàng', 0, 1438767176, 1442473178),
(5, 'Nhân viên bán hàng', 0, 1438767185, NULL),
(6, 'Admin', 0, 1442473266, NULL),
(7, 'Super Admin ', 0, 1442473293, NULL),
(8, 'test', 1, 1442578814, 1443031862);

-- --------------------------------------------------------

--
-- Table structure for table `position_rule`
--

CREATE TABLE IF NOT EXISTS `position_rule` (
  `position_rule_id` int(10) NOT NULL AUTO_INCREMENT,
  `position_id` int(10) DEFAULT NULL,
  `rule_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`position_rule_id`),
  KEY `rule_id` (`rule_id`) USING BTREE,
  KEY `position_id` (`position_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`rule_id`) REFER `demo_befult_3/rule' AUTO_INCREMENT=220 ;

--
-- Dumping data for table `position_rule`
--

INSERT INTO `position_rule` (`position_rule_id`, `position_id`, `rule_id`, `status`, `create_time`, `last_update`) VALUES
(1, 8, 4, 1, 1442826334, 1443031848),
(2, 8, 3, 1, 1442826339, 1443031808),
(3, 5, 69, 0, 1443031819, NULL),
(4, 5, 68, 0, 1443031889, NULL),
(5, 5, 60, 0, 1443031936, NULL),
(6, 4, 69, 1, 1443031947, 1443577730),
(7, 5, 66, 0, 1443577364, NULL),
(8, 5, 65, 0, 1443577387, NULL),
(9, 5, 63, 0, 1443577419, NULL),
(10, 5, 61, 0, 1443577450, NULL),
(11, 5, 58, 0, 1443577464, NULL),
(12, 5, 56, 0, 1443577843, NULL),
(13, 5, 53, 0, 1443577856, NULL),
(14, 5, 51, 0, 1443577870, NULL),
(15, 5, 46, 0, 1443577925, NULL),
(16, 5, 36, 0, 1443577959, NULL),
(17, 5, 28, 0, 1443577987, NULL),
(18, 5, 25, 0, 1443578012, NULL),
(19, 5, 24, 0, 1443578031, NULL),
(20, 4, 69, 0, 1443578303, NULL),
(21, 4, 68, 0, 1443578314, NULL),
(22, 4, 66, 0, 1443578327, 1443578364),
(23, 4, 65, 0, 1443578337, 1443578354),
(24, 4, 64, 0, 1443578392, NULL),
(25, 4, 63, 0, 1443578400, NULL),
(26, 4, 62, 0, 1443578411, NULL),
(27, 4, 61, 0, 1443578427, NULL),
(28, 4, 58, 0, 1443578438, NULL),
(29, 4, 57, 0, 1443578449, NULL),
(30, 4, 56, 0, 1443578455, NULL),
(31, 4, 55, 0, 1443578466, NULL),
(32, 4, 54, 0, 1443578471, NULL),
(33, 4, 53, 0, 1443578479, NULL),
(34, 4, 52, 0, 1443578511, NULL),
(35, 4, 51, 0, 1443578519, NULL),
(36, 4, 46, 0, 1443578548, NULL),
(37, 4, 36, 0, 1443578556, NULL),
(38, 4, 28, 0, 1443578570, NULL),
(39, 4, 25, 0, 1443578591, NULL),
(40, 4, 24, 0, 1443578599, NULL),
(41, 4, 9, 0, 1443578617, NULL),
(42, 3, 68, 0, 1443578802, NULL),
(43, 3, 65, 0, 1443578812, NULL),
(44, 3, 61, 0, 1443578818, NULL),
(45, 3, 60, 0, 1443578824, NULL),
(46, 3, 59, 0, 1443578836, NULL),
(47, 3, 57, 0, 1443578850, NULL),
(48, 3, 56, 0, 1443578858, NULL),
(49, 3, 53, 0, 1443578873, NULL),
(50, 3, 51, 0, 1443578883, NULL),
(51, 3, 50, 0, 1443578892, NULL),
(52, 3, 49, 0, 1443578912, NULL),
(53, 3, 48, 0, 1443578922, NULL),
(54, 3, 47, 0, 1443578957, NULL),
(55, 3, 46, 0, 1443578969, NULL),
(56, 3, 45, 0, 1443579815, NULL),
(57, 3, 44, 0, 1443579831, NULL),
(58, 3, 39, 0, 1443579843, NULL),
(59, 3, 38, 0, 1443579856, NULL),
(60, 3, 37, 0, 1443579880, NULL),
(61, 3, 36, 0, 1443579895, NULL),
(62, 3, 35, 0, 1443579911, NULL),
(63, 3, 34, 0, 1443579924, NULL),
(64, 3, 32, 0, 1443579939, NULL),
(65, 3, 31, 0, 1443579949, NULL),
(66, 3, 30, 0, 1443579965, NULL),
(67, 3, 28, 0, 1443579982, NULL),
(68, 3, 25, 0, 1443579992, NULL),
(69, 3, 24, 0, 1443580004, NULL),
(70, 3, 22, 0, 1443580013, NULL),
(71, 3, 21, 0, 1443580028, NULL),
(72, 3, 20, 0, 1443580036, NULL),
(73, 3, 18, 0, 1443580060, NULL),
(74, 3, 16, 0, 1443580073, NULL),
(75, 3, 15, 0, 1443580081, NULL),
(76, 3, 14, 0, 1443580094, NULL),
(77, 3, 13, 0, 1443580110, NULL),
(78, 3, 12, 0, 1443580121, NULL),
(79, 3, 11, 0, 1443580137, NULL),
(80, 3, 9, 0, 1443580216, NULL),
(81, 3, 8, 0, 1443580224, NULL),
(82, 3, 7, 0, 1443580230, NULL),
(83, 3, 6, 0, 1443580238, NULL),
(84, 3, 5, 0, 1443580245, NULL),
(85, 3, 10, 0, 1443580753, NULL),
(86, 2, 68, 0, 1443581018, NULL),
(87, 2, 67, 0, 1443581030, NULL),
(88, 2, 61, 0, 1443581042, NULL),
(89, 2, 60, 0, 1443581052, NULL),
(90, 2, 56, 0, 1443581073, NULL),
(91, 2, 47, 0, 1443581084, NULL),
(92, 2, 46, 0, 1443581099, NULL),
(93, 2, 43, 0, 1443581109, NULL),
(94, 2, 42, 0, 1443581118, NULL),
(95, 2, 41, 0, 1443581125, NULL),
(96, 2, 40, 0, 1443581134, NULL),
(97, 2, 39, 0, 1443581154, NULL),
(98, 2, 38, 0, 1443581165, NULL),
(99, 2, 37, 0, 1443581175, NULL),
(100, 2, 36, 0, 1443581186, NULL),
(101, 2, 35, 0, 1443581195, NULL),
(102, 2, 34, 0, 1443581208, NULL),
(103, 2, 32, 0, 1443581219, NULL),
(104, 2, 30, 0, 1443581233, NULL),
(105, 2, 28, 0, 1443581241, NULL),
(106, 2, 18, 0, 1443581256, NULL),
(107, 1, 68, 0, 1443581286, NULL),
(108, 1, 60, 0, 1443581297, NULL),
(109, 1, 56, 0, 1443581305, NULL),
(110, 1, 53, 0, 1443581314, NULL),
(111, 1, 36, 0, 1443581337, NULL),
(112, 1, 28, 0, 1443581350, NULL),
(113, 1, 26, 0, 1443581361, NULL),
(114, 1, 25, 0, 1443581374, NULL),
(115, 1, 24, 0, 1443581389, NULL),
(116, 1, 23, 0, 1443581400, NULL),
(117, 1, 9, 0, 1443581412, NULL),
(118, 7, 69, 1, 1443758989, 1443865651),
(119, 7, 68, 1, 1443758993, 1443865637),
(120, 7, 69, 1, 1443758994, 1443865633),
(121, 7, 67, 1, 1443758998, 1443865630),
(122, 7, 66, 1, 1443759000, 1443865627),
(123, 7, 64, 1, 1443759003, 1443865624),
(124, 7, 63, 1, 1443759005, 1443865621),
(125, 7, 60, 1, 1443759007, 1443865617),
(126, 7, 57, 1, 1443759009, 1443865614),
(127, 7, 52, 1, 1443759011, 1443865610),
(128, 7, 51, 1, 1443759012, 1443865607),
(129, 7, 50, 1, 1443759014, 1443865603),
(130, 7, 33, 1, 1443759016, 1443865599),
(131, 7, 33, 1, 1443759018, 1443865595),
(132, 7, 13, 1, 1443759020, 1443865591),
(133, 7, 50, 1, 1443759021, 1443865587),
(134, 7, 53, 1, 1443759023, 1443865584),
(135, 7, 25, 1, 1443759477, 1443865580),
(136, 7, 32, 1, 1443759549, 1443865575),
(137, 7, 32, 1, 1443759549, 1443759613),
(138, 7, 43, 1, 1443760079, 1443865570),
(139, 7, 41, 1, 1443760107, 1443865566),
(140, 7, 62, 1, 1443835004, 1443865562),
(141, 7, 29, 1, 1443836029, 1443865556),
(142, 7, 27, 1, 1443836123, 1443865551),
(143, 7, 17, 1, 1443836261, 1443865547),
(144, 7, 19, 1, 1443836374, 1443865540),
(145, 7, 69, 0, 1443865660, NULL),
(146, 7, 68, 0, 1443865667, NULL),
(147, 7, 67, 0, 1443865669, NULL),
(148, 7, 66, 0, 1443865671, NULL),
(149, 7, 65, 0, 1443865673, NULL),
(150, 7, 64, 0, 1443865675, NULL),
(151, 7, 63, 0, 1443865678, NULL),
(152, 7, 62, 0, 1443865680, NULL),
(153, 7, 61, 0, 1443865683, NULL),
(154, 7, 60, 0, 1443865686, NULL),
(155, 7, 59, 0, 1443865690, NULL),
(156, 7, 58, 0, 1443865692, NULL),
(157, 7, 57, 0, 1443865694, NULL),
(158, 7, 56, 0, 1443865697, NULL),
(159, 7, 55, 0, 1443865701, NULL),
(160, 7, 54, 0, 1443865703, NULL),
(161, 7, 53, 0, 1443865745, NULL),
(162, 7, 52, 0, 1443865748, NULL),
(163, 7, 51, 0, 1443865755, NULL),
(164, 7, 50, 0, 1443865757, NULL),
(165, 7, 49, 0, 1443865767, NULL),
(166, 7, 48, 0, 1443865771, NULL),
(167, 7, 47, 0, 1443865776, NULL),
(168, 7, 46, 0, 1443865788, NULL),
(169, 7, 45, 0, 1443865925, NULL),
(170, 7, 44, 0, 1443865932, NULL),
(171, 7, 43, 0, 1443865936, NULL),
(172, 7, 42, 0, 1443865940, NULL),
(173, 7, 41, 0, 1443865943, NULL),
(174, 7, 40, 0, 1443865947, NULL),
(175, 7, 39, 0, 1443865951, NULL),
(176, 7, 38, 0, 1443865955, NULL),
(177, 7, 37, 0, 1443865958, NULL),
(178, 7, 36, 0, 1443865962, NULL),
(179, 7, 35, 0, 1443865970, NULL),
(180, 7, 34, 0, 1443865978, NULL),
(181, 7, 33, 0, 1443865985, NULL),
(182, 7, 32, 0, 1443865988, NULL),
(183, 7, 31, 0, 1443865993, NULL),
(184, 7, 30, 0, 1443865998, NULL),
(185, 7, 29, 0, 1443866004, NULL),
(186, 7, 28, 0, 1443866007, NULL),
(187, 7, 27, 0, 1443866012, NULL),
(188, 7, 26, 0, 1443866019, NULL),
(189, 7, 25, 0, 1443866027, NULL),
(190, 7, 24, 0, 1443866032, NULL),
(191, 7, 23, 0, 1443866037, NULL),
(192, 7, 22, 0, 1443866044, NULL),
(193, 7, 21, 0, 1443866052, NULL),
(194, 7, 20, 0, 1443866058, NULL),
(195, 7, 19, 0, 1443866063, NULL),
(196, 7, 18, 0, 1443866069, NULL),
(197, 7, 18, 0, 1443866074, NULL),
(198, 7, 17, 0, 1443866078, NULL),
(199, 7, 16, 0, 1443866081, NULL),
(200, 7, 15, 0, 1443866084, NULL),
(201, 7, 14, 0, 1443866087, NULL),
(202, 7, 13, 0, 1443866091, NULL),
(203, 7, 12, 0, 1443866094, NULL),
(204, 7, 11, 0, 1443866097, NULL),
(205, 7, 10, 0, 1443866101, NULL),
(206, 7, 9, 0, 1443866104, NULL),
(207, 7, 8, 0, 1443866108, NULL),
(208, 7, 7, 0, 1443866116, NULL),
(209, 7, 6, 0, 1443866118, NULL),
(210, 7, 5, 0, 1443866121, NULL),
(211, 7, 4, 0, 1443866125, NULL),
(212, 7, 3, 0, 1443866130, NULL),
(213, 7, 2, 0, 1443866134, NULL),
(214, 7, 1, 0, 1443866137, NULL),
(215, 5, 57, 0, 1443867165, NULL),
(216, 5, 68, 0, 1443867288, NULL),
(217, 7, 65, 0, 1444104336, NULL),
(218, 7, 65, 0, 1444104339, NULL),
(219, 7, 61, 0, 1444104345, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `price_list`
--

CREATE TABLE IF NOT EXISTS `price_list` (
  `price_id` int(10) NOT NULL AUTO_INCREMENT,
  `price_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `time_start` int(10) DEFAULT NULL,
  `time_end` int(10) DEFAULT NULL,
  `validity` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `price_list`
--

INSERT INTO `price_list` (`price_id`, `price_name`, `status`, `create_time`, `last_update`, `time_start`, `time_end`, `validity`) VALUES
(1, 'Bảng giá 1', 0, 1448513931, 1450841352, 1448470800, 1606323600, 1),
(2, 'ahfoha', 1, 1450809601, 1450841335, 1450803600, 1450803600, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8_unicode_ci,
  `expired_notification` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guarantee` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_time` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `changing_policy` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_id` int(10) DEFAULT NULL,
  `product_images` text COLLATE utf8_unicode_ci,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `product_thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_status_id` int(3) DEFAULT NULL,
  `inventory_min` int(10) DEFAULT NULL,
  `inventory_max` int(10) DEFAULT NULL,
  `product_barcode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `product_code` (`product_code`) USING BTREE,
  KEY `unit_id` (`unit_id`) USING BTREE,
  KEY `product_status` (`product_status`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`unit_id`) REFER `demo_beful2/unit`(' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_code`, `product_name`, `product_description`, `expired_notification`, `guarantee`, `shipping_time`, `changing_policy`, `unit_id`, `product_images`, `status`, `create_time`, `last_update`, `product_thumbnail`, `product_status_id`, `inventory_min`, `inventory_max`, `product_barcode`, `product_status`) VALUES
(1, 'P-1', 'Sản phẩm 1', NULL, '30', '12', '15', NULL, 2, NULL, 2, 1448513681, 1449729466, 'application/upload/2015/11/ILCIESSEMRXQYZHDKVEP-1448513793.jpg', 10, NULL, NULL, '097419791141', NULL),
(2, 'P-2', 'Sản phẩm 2', NULL, '30', '12', '15', NULL, 2, NULL, 2, 1448513802, 1449279885, 'application/upload/2015/12/OGFNFNCZFHAJEIFEFUQJ-1449279885.jpg', 10, NULL, NULL, '111111111111', NULL),
(3, 'P-3', 'Sản phẩm 3', NULL, '30', '12', '15', NULL, 2, NULL, 2, 1448513853, 1449279911, 'application/upload/2015/12/GVXJDKCWBAEBCXZYWUWZ-1449279911.jpg', 10, NULL, NULL, '1222223455', NULL),
(4, 'P-4', 'Sản phẩm 4', NULL, '30', '12', '15', NULL, 2, NULL, 2, 1448513879, 1449279834, 'application/upload/2015/12/JMOVNMEDOWNOPXNAGHMA-1449279834.jpg', 10, NULL, NULL, '12345784747', NULL),
(5, 'P-5', 'Sản phẩm 5', NULL, '30', '12', '15', NULL, 2, NULL, 2, 1448513907, 1449279812, 'application/upload/2015/12/EGPSQXMPBJLGZKUCOFKD-1449279812.jpg', 10, NULL, NULL, '14750957952', NULL),
(6, 'P-6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1449288706, 1449288706, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'P-7', 'Fracora Placenta', 'Khuyên dùng cho người trên 25 tuổi để mang lại hiệu quả tối ưu.', '180', NULL, NULL, NULL, 2, NULL, 2, 1450700090, 1450700303, 'application/upload/2015/12/ZVCTQWFEOQCFLABGPQTB-1450700273.jpg', 10, NULL, NULL, '1234567890', NULL),
(8, 'P-8', 'Slimfit', 'Trên 18+', '180', NULL, NULL, NULL, 2, NULL, 2, 1450840901, 1451114360, 'application/upload/2015/12/XPOZTTAIACLDEJCKAICM-1451114360.jpg', 10, NULL, NULL, '445566778899', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `product_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`product_category_id`),
  UNIQUE KEY `product_id` (`product_id`,`category_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`product_id`) REFER `demo_befult_3/p' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `product_id`, `category_id`, `status`, `create_time`, `last_update`) VALUES
(1, 1, 9, 0, 1448513793, NULL),
(2, 2, 9, 0, 1448513848, NULL),
(3, 3, 9, 0, 1448513876, NULL),
(4, 4, 9, 0, 1448513902, NULL),
(5, 5, 9, 0, 1448513926, NULL),
(6, 7, 5, 0, 1450700273, NULL),
(7, 8, 6, 0, 1450841019, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE IF NOT EXISTS `product_price` (
  `product_price_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `product_price` int(10) DEFAULT '0',
  `product_price_start_time` int(10) DEFAULT NULL,
  `product_price_end_time` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `currency_unit_id` int(10) DEFAULT NULL,
  `input_line_id` int(10) DEFAULT NULL,
  `price_list_id` int(10) DEFAULT NULL,
  `product_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`product_price_id`),
  KEY `partner_id` (`partner_id`) USING BTREE,
  KEY `currency_unit_id` (`currency_unit_id`) USING BTREE,
  KEY `input_line_id` (`input_line_id`) USING BTREE,
  KEY `price_list_id` (`price_list_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`currency_unit_id`) REFER `demo_befu' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`product_price_id`, `product_id`, `partner_id`, `product_price`, `product_price_start_time`, `product_price_end_time`, `status`, `create_time`, `last_update`, `currency_unit_id`, `input_line_id`, `price_list_id`, `product_note`) VALUES
(1, 5, NULL, 900000, NULL, NULL, 0, 1448513975, 1450841352, NULL, NULL, 1, NULL),
(2, 4, NULL, 800000, NULL, NULL, 0, 1448513975, 1450841352, NULL, NULL, 1, NULL),
(3, 3, NULL, 700000, NULL, NULL, 0, 1448513975, 1450841352, NULL, NULL, 1, NULL),
(4, 2, NULL, 600000, NULL, NULL, 0, 1448514060, 1450841352, NULL, NULL, 1, NULL),
(5, 1, NULL, 500000, NULL, NULL, 0, 1448514085, 1450841352, NULL, NULL, 1, NULL),
(6, 7, NULL, 1450000, NULL, NULL, 0, 1450700444, 1450841352, NULL, NULL, 1, 'Không trả giá'),
(7, 8, NULL, 1080000, NULL, NULL, 0, 1450841352, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_refund`
--

CREATE TABLE IF NOT EXISTS `product_refund` (
  `refund_id` int(10) NOT NULL AUTO_INCREMENT,
  `output_line_id` int(10) DEFAULT NULL,
  `input_line_id` int(10) DEFAULT NULL,
  `refund_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`refund_id`),
  KEY `output_line_id` (`output_line_id`) USING BTREE,
  KEY `input_line_id` (`input_line_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`output_line_id`) REFER `demo_befult' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product_refund`
--

INSERT INTO `product_refund` (`refund_id`, `output_line_id`, `input_line_id`, `refund_note`, `status`, `create_time`, `last_update`, `order_id`) VALUES
(1, 2, 4, NULL, 3, 1450701520, 1450701523, NULL),
(2, 14, 21, NULL, 3, 1450807620, 1450807622, NULL),
(3, 13, 22, NULL, 2, 1450807621, NULL, NULL),
(4, 12, 23, NULL, 2, 1450807621, NULL, NULL),
(5, 18, 26, NULL, 3, 1450842225, 1450842226, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_tax`
--

CREATE TABLE IF NOT EXISTS `product_tax` (
  `product_tax_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_tax_value` int(3) DEFAULT NULL,
  `product_tax_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`product_tax_id`),
  UNIQUE KEY `product_tax_name` (`product_tax_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_transfer`
--

CREATE TABLE IF NOT EXISTS `product_transfer` (
  `transfer_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) DEFAULT NULL,
  `input_order_id` int(10) DEFAULT NULL,
  `warehouse_from_id` int(10) DEFAULT NULL,
  `warehouse_to_id` int(10) DEFAULT NULL,
  `transfer_note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`transfer_id`),
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `input_order_id` (`input_order_id`) USING BTREE,
  KEY `warehouse_from_id` (`warehouse_from_id`) USING BTREE,
  KEY `warehouse_to_id` (`warehouse_to_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`order_id`) REFER `demo_befult_3/out' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_transfer`
--

INSERT INTO `product_transfer` (`transfer_id`, `order_id`, `input_order_id`, `warehouse_from_id`, `warehouse_to_id`, `transfer_note`, `status`, `create_time`, `last_update`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 0, 1450700934, 1450700934),
(2, 1, 4, 1, 2, 'Chuyển kho nội bộ', 3, 1450700969, 1450700992);

-- --------------------------------------------------------

--
-- Table structure for table `promote`
--

CREATE TABLE IF NOT EXISTS `promote` (
  `promote_id` int(10) NOT NULL AUTO_INCREMENT,
  `promote_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_start` int(10) DEFAULT NULL,
  `time_end` int(10) DEFAULT NULL,
  `validity` tinyint(1) DEFAULT '0',
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`promote_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `promote`
--

INSERT INTO `promote` (`promote_id`, `promote_name`, `time_start`, `time_end`, `validity`, `status`, `create_time`, `last_update`) VALUES
(1, 'Chào mừng năm mới 2016', 1446310800, 1459357200, 1, 0, 1448514135, 1448514983),
(2, 'Khuyến mại mừng tết Dương lịch 2016', 1448902800, 1454173200, 1, 0, 1448520742, 1449729506),
(3, 'Khuyến mại mới', 1448902800, 1451494800, 1, 0, 1449730988, NULL),
(4, 'Chúc mừng giáng sinh', 1450717200, 1451494800, 1, 0, 1450700399, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promote_order`
--

CREATE TABLE IF NOT EXISTS `promote_order` (
  `promote_order_id` int(10) NOT NULL AUTO_INCREMENT,
  `promote_id` int(10) DEFAULT NULL,
  `order_price_from` int(10) DEFAULT NULL,
  `order_price_to` int(10) DEFAULT NULL,
  `discount` int(3) DEFAULT NULL,
  `promote_product_id` int(10) DEFAULT NULL,
  `promote_product_amount` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`promote_order_id`),
  KEY `promote_id` (`promote_id`) USING BTREE,
  KEY `promote_product_id` (`promote_product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`promote_id`) REFER `demo_befult_3/p' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `promote_order`
--

INSERT INTO `promote_order` (`promote_order_id`, `promote_id`, `order_price_from`, `order_price_to`, `discount`, `promote_product_id`, `promote_product_amount`, `status`, `create_time`, `last_update`) VALUES
(1, 1, 1, 1000000000, 10, 5, NULL, 0, 1448514135, 1448514983),
(2, 2, 1, 1000000000, 10, 5, NULL, 0, 1448520742, 1449729506),
(3, 3, 1, 1000000000, 9, 5, NULL, 0, 1449730989, NULL),
(4, 4, 1000000, 2000000, 7, 7, NULL, 0, 1450700399, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promote_product`
--

CREATE TABLE IF NOT EXISTS `promote_product` (
  `promote_product_id` int(10) NOT NULL AUTO_INCREMENT,
  `promote_id` int(10) DEFAULT NULL,
  `product_id` int(10) DEFAULT NULL,
  `min_amount` int(10) DEFAULT NULL,
  `discount` int(3) DEFAULT NULL,
  `promote_product` int(10) DEFAULT NULL,
  `promote_product_amount` int(10) DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`promote_product_id`),
  KEY `promote_id` (`promote_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `promote_product` (`promote_product`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 11264 kB; (`promote_id`) REFER `demo_befult_3/p' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `promote_product`
--

INSERT INTO `promote_product` (`promote_product_id`, `promote_id`, `product_id`, `min_amount`, `discount`, `promote_product`, `promote_product_amount`, `status`, `create_time`, `last_update`) VALUES
(1, 2, 5, 5, 3, 5, NULL, 0, 1448589318, 1449729506),
(2, 2, 4, 4, 1, 5, NULL, 0, 1449569191, 1449729506),
(3, 3, 5, 1, 6, 5, NULL, 0, 1449730989, NULL),
(4, 3, 4, 1, 6, 5, NULL, 0, 1449730989, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE IF NOT EXISTS `receipt` (
  `receipt_id` int(10) NOT NULL AUTO_INCREMENT,
  `receipt_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `receipt_date` int(10) DEFAULT NULL,
  `payer_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_type_id` int(10) DEFAULT NULL,
  `receipt_status_id` int(10) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`pay_type_id`) REFER `hoang_beful/pa' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`receipt_id`, `receipt_code`, `partner_id`, `receipt_date`, `payer_name`, `pay_type_id`, `receipt_status_id`, `description`, `create_time`, `last_update`, `status`) VALUES
(1, 'PT-1', 8, 1450630800, 'Nguyễn Văn A', 1, 3, '', 1450701733, 1450701788, 0);

-- --------------------------------------------------------

--
-- Table structure for table `receipt_line`
--

CREATE TABLE IF NOT EXISTS `receipt_line` (
  `receipt_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(10) DEFAULT NULL,
  `partner_id` int(10) DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`receipt_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `receipt_line`
--

INSERT INTO `receipt_line` (`receipt_line_id`, `receipt_id`, `partner_id`, `money`, `description`, `create_time`, `last_update`, `status`) VALUES
(1, 1, 0, 1450000, 'Thu tiền đơn hàng CH-0124', 1450701786, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE IF NOT EXISTS `rule` (
  `rule_id` int(10) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`rule_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`rule_id`, `rule_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'xóa người dùng', 0, 1442825744, NULL),
(2, 'sửa thông tin người dùng', 0, 1442825749, NULL),
(3, 'phân quyền người dùng', 0, 1442825759, NULL),
(4, 'xem thông tin người dùng', 0, 1442825765, NULL),
(5, 'Xem báo cáo tài khoản', 0, 1443028857, NULL),
(6, 'Xem báo cáo các khoản phải thu/chi', 0, 1443028864, NULL),
(7, 'Xem báo cáo chi phí', 0, 1443028870, NULL),
(8, 'Xem báo cáo lợi nhuận (Income statement)', 0, 1443028876, NULL),
(9, 'Xem báo cáo doanh thu', 0, 1443028925, NULL),
(10, 'Tăng giảm các khoản tiền trong tài khoản', 0, 1443028945, NULL),
(11, 'Xem và in các phiếu mua/bán/ trả/thanh toán', 0, 1443028961, NULL),
(12, 'Xem và in các phiếu thanh toán, thu chi', 0, 1443028969, NULL),
(13, 'Quản lý chi phí (thêm, xem, sửa, xoá)', 0, 1443028978, NULL),
(14, 'Quản lý các tài khoản tiền mặt, ngân hàng...', 0, 1443028982, NULL),
(15, 'Thực hiện thanh toán cho các đơn mua/bán hàng', 0, 1443028987, NULL),
(16, 'Xem danh sách các đơn  mua/bán hàng chưa thanh toá', 0, 1443028993, NULL),
(17, 'Sửa thông tin nhà cung cấp', 0, 1443029000, NULL),
(18, 'Xem thông tin nhà cung cấp', 0, 1443029004, NULL),
(19, 'Thêm nhà cung cấp mới', 0, 1443029009, NULL),
(20, 'Sưả thông tin cửa hàng', 0, 1443029014, NULL),
(21, 'Xem thông tin cửa hàng', 0, 1443029018, NULL),
(22, 'Thêm cửa hàng mới', 0, 1443029022, NULL),
(23, 'Sửa thông tin khuyến mãi', 0, 1443029027, NULL),
(24, 'Xem thông tin khuyến mãi', 0, 1443029031, NULL),
(25, 'Truy cập trực tiếp trang quản lý khuyến mãi', 0, 1443029035, NULL),
(26, 'Thêm khuyến mãi', 0, 1443029040, NULL),
(27, 'Sửa thông tin sản phẩm', 0, 1443029045, NULL),
(28, 'Xem thông tin sản phẩm', 0, 1443029050, NULL),
(29, 'Thêm sản phẩm', 0, 1443029054, NULL),
(30, 'Xuất chi tiết phiếu mua hàng ra file excel', 0, 1443029061, NULL),
(31, 'Huỷ phiếu mua hàng (nếu chưa nhận hàng)', 0, 1443029065, NULL),
(32, 'Tìm kiếm và xem phiếu mua hàng', 0, 1443029070, NULL),
(33, 'Tạo phiếu mua hàng', 0, 1443029073, NULL),
(34, 'Xem đơn giá tồn kho hiện tại của sản phẩm', 0, 1443029080, NULL),
(35, 'Xem báo cáo giá trị hàng tồn kho', 0, 1443029084, NULL),
(36, 'Xem số lượng tồn kho', 0, 1443029089, NULL),
(37, 'Xem báo cáo các hoạt động kiểm kê hàng hoá', 0, 1443029093, NULL),
(38, 'Xem báo cáo xuất nhập tồn', 0, 1443029097, NULL),
(39, 'Tìm kiếm và xem các phiếu nhập/ xuất/ chuyển kho', 0, 1443029102, NULL),
(40, 'Kiểm kê hàng hoá', 0, 1443029107, NULL),
(41, 'Chuyển kho/ In phiếu', 0, 1443029112, NULL),
(42, 'Xuất kho/ In phiếu', 0, 1443029133, NULL),
(43, 'Nhập kho/ In phiếu', 0, 1443029137, NULL),
(44, 'Vô hiệu hoá kho hàng', 0, 1443029141, NULL),
(45, 'Sửa thông tin kho hàng', 0, 1443029145, NULL),
(46, 'Xem thông tin kho hàng', 0, 1443029151, NULL),
(47, 'Truy cập trực tiếp trang quản lý kho', 0, 1443029154, NULL),
(48, 'Tạo kho hàng', 0, 1443029158, NULL),
(49, 'Xuất lịch sử mua hàng của KH ra file excel', 0, 1443029164, NULL),
(50, 'Xuất thông tin KH ra file excel', 0, 1443029168, NULL),
(51, 'Xem báo giá của KH', 0, 1443029172, NULL),
(52, 'Tạo báo giá cho khách hàng', 0, 1443029176, NULL),
(53, 'Xem lịch sử mua hàng của KH', 0, 1443029180, NULL),
(54, 'Vô hiệu hoá khách hàng', 0, 1443029184, NULL),
(55, 'Sửa thông tin khách hàng', 0, 1443029190, NULL),
(56, 'Xem thông tin khách hàng', 0, 1443029194, NULL),
(57, 'Truy cập trực tiếp trang quản lý khách hàng', 0, 1443029198, NULL),
(58, 'Thêm khách hàng mới', 0, 1443029202, NULL),
(59, 'Xuất chi tiết các đơn bán hàng ra file excel', 0, 1443029207, NULL),
(60, 'Xem các đơn trả hàng bán', 0, 1443029210, NULL),
(61, 'In phiếu trả hàng bán', 0, 1443029214, NULL),
(62, 'Trả đơn bán hàng', 0, 1443029218, NULL),
(63, 'Áp dụng khuyến mãi, giảm giá ', 0, 1443029222, NULL),
(64, 'Sửa giá trong khi tạo đơn bán hàng', 0, 1443029226, NULL),
(65, 'In đơn bán hàng', 0, 1443029230, NULL),
(66, 'Duyệt đơn bán hàng tự động nếu xuất tại cửa hàng', 0, 1443029253, NULL),
(67, 'Duyệt/huỷ (nếu chưa duyệt) đơn bán hàng không xuất', 0, 1443029257, NULL),
(68, 'Xem đơn bán hàng', 0, 1443029266, NULL),
(69, 'Tạo đơn bán hàng', 0, 1443029270, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(10) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_type_id` int(10) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  KEY `status_type_id` (`status_type_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`status_type_id`) REFER `demo_beful2' AUTO_INCREMENT=18 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`, `status_type_id`, `status`, `create_time`, `last_update`) VALUES
(1, 'Disable', 1, 0, 1439128644, NULL),
(2, 'Pending', 1, 0, 1439128663, NULL),
(3, 'Đơn hàng', 2, 0, 1439128823, 1439129401),
(4, 'Chuyển kho', 2, 0, 1439128832, 1439129389),
(5, 'Hàng trả về', 2, 0, 1439128841, 1439129371),
(6, 'valid', 4, 0, 1440061564, NULL),
(7, 'expired', 4, 0, 1440061586, NULL),
(8, 'Product status 1', 5, 0, 1441079394, NULL),
(9, 'Product status 2', 5, 0, 1441079403, NULL),
(10, 'Product status 3', 5, 0, 1441079407, NULL),
(11, 'purchase order status (1)', 6, 0, 1441193395, NULL),
(12, 'purchase order status (2)', 6, 0, 1441193402, NULL),
(13, 'purchase order status (3)', 6, 0, 1441193409, NULL),
(14, 'Đơn mua hàng', NULL, 0, NULL, NULL),
(15, 'Đơn đặt hàng', NULL, 0, NULL, NULL),
(16, 'Đã thanh toán', 2, 0, NULL, NULL),
(17, 'Inventory', 7, 0, 1437249898, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_type`
--

CREATE TABLE IF NOT EXISTS `status_type` (
  `status_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `status_type_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  PRIMARY KEY (`status_type_id`),
  UNIQUE KEY `status_type_name` (`status_type_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `status_type`
--

INSERT INTO `status_type` (`status_type_id`, `status_type_name`, `status`, `create_time`, `last_update`) VALUES
(1, 'System', 0, 1439128566, NULL),
(2, 'Output Order', 0, 1439128581, NULL),
(3, 'Input Order', 0, 1439128585, NULL),
(4, 'quotation', 0, 1440061530, NULL),
(5, 'Product', 0, 1441079367, NULL),
(6, 'purchase order', 0, 1441193294, NULL),
(7, 'Inventory', 0, 1437249898, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_pay`
--

CREATE TABLE IF NOT EXISTS `supplier_pay` (
  `pay_id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(10) DEFAULT NULL,
  `pay_type_id` int(10) DEFAULT NULL,
  `pay_status_id` int(10) DEFAULT NULL,
  `pay_date` int(10) DEFAULT NULL,
  `have_to_pay` int(11) NOT NULL,
  `pay_money` int(11) DEFAULT NULL,
  `owed_money` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`pay_type_id`) REFER `hoang_beful/pa' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `supplier_pay`
--

INSERT INTO `supplier_pay` (`pay_id`, `pay_code`, `supplier_id`, `pay_type_id`, `pay_status_id`, `pay_date`, `have_to_pay`, `pay_money`, `owed_money`, `create_time`, `last_update`, `status`) VALUES
(1, 'TT-1', 0, 0, 1, 1451036774, 0, 0, NULL, 1451036774, NULL, 0),
(2, 'TT-2', 0, 0, 1, 1451036819, 0, 0, NULL, 1451036819, NULL, 0),
(3, 'TT-3', 9, 1, 3, 1450976400, 3000000, 2500000, NULL, 1451037151, 1451037605, 0),
(4, 'TT-4', 9, 1, 3, 1450976400, 500000, 300000, NULL, 1451037177, 1451037619, 0),
(5, 'TT-5', 5, 1, 3, 1450976400, 26600000, 20000000, NULL, 1451037483, 1451037495, 0),
(6, 'TT-6', 0, 0, 1, 1451037500, 0, 0, NULL, 1451037500, NULL, 0),
(7, 'TT-7', 0, 0, 1, 1451037874, 0, 0, NULL, 1451037874, NULL, 0),
(8, 'TT-8', 5, 1, 3, 1450976400, 6600000, 3000000, NULL, 1451037895, 1451037908, 0),
(9, 'TT-9', 5, 1, 3, 1450976400, 3600000, 3000000, NULL, 1451037912, 1451037924, 0);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_pay_line`
--

CREATE TABLE IF NOT EXISTS `supplier_pay_line` (
  `pay_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_id` int(10) DEFAULT NULL,
  `order_id` int(10) DEFAULT NULL,
  `have_to_pay` int(11) NOT NULL,
  `amount_paid` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `pay_status_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`pay_line_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 12288 kB; (`pay_id`) REFER `hoang_beful/supplie' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `supplier_pay_line`
--

INSERT INTO `supplier_pay_line` (`pay_line_id`, `pay_id`, `order_id`, `have_to_pay`, `amount_paid`, `create_time`, `last_update`, `pay_status_id`, `status`) VALUES
(3, 3, 16, 3000000, 2500000, 1451037169, NULL, 0, 0),
(4, 4, 16, 500000, 300000, 1451037184, NULL, 0, 0),
(5, 5, 10, 26600000, 20000000, 1451037492, NULL, 0, 0),
(6, 8, 10, 6600000, 3000000, 1451037904, NULL, 0, 0),
(7, 9, 10, 3600000, 3000000, 1451037923, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
  `unit_id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `validity` int(1) DEFAULT '0',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`unit_id`),
  UNIQUE KEY `unit_name` (`unit_name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unit_id`, `unit_name`, `status`, `create_time`, `last_update`, `validity`, `note`) VALUES
(1, 'cái', 0, 1439173323, NULL, NULL, NULL),
(2, 'hộp', 0, 1439173325, NULL, NULL, NULL),
(3, 'thùng', 0, 1439173327, NULL, NULL, NULL),
(4, 'két', 0, 1439173329, NULL, NULL, NULL),
(5, 'lọ', 0, 1439173343, NULL, NULL, NULL),
(6, 'km', 0, 1441681994, 1442290022, 0, 'hhh'),
(7, 'cm', 0, 1442288894, 1442290006, 1, NULL),
(8, 'mét', 0, 1442289001, 1442289999, 0, NULL),
(9, 'kg', 0, 1442289991, 1442289995, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_phonenumber` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_birthday` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `create_time` int(10) NOT NULL,
  `last_update` int(10) DEFAULT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district_id` int(10) DEFAULT NULL,
  `user_code` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `employee_name` (`user_name`) USING BTREE,
  UNIQUE KEY `user_code` (`user_code`) USING BTREE,
  KEY `district_id` (`district_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`district_id`) REFER `demo_beful2/di' AUTO_INCREMENT=18 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `first_name`, `last_name`, `user_password`, `user_phonenumber`, `user_birthday`, `user_address`, `status`, `create_time`, `last_update`, `user_email`, `district_id`, `user_code`) VALUES
(1, 'lan.nt', 'Lan', 'Nguyễn Thị', '123', '0975630001', '16/3/1989', 'Số 3 Hàng Đào', 0, 1438769230, NULL, NULL, NULL, 'A-1'),
(2, 'hien.nt', '333', '', '333', '', '', '', 0, 0, 1439215019, '', NULL, 'A-2'),
(3, '789', '', '', '', '', '', '', 0, 1438911466, NULL, NULL, NULL, 'A-3'),
(4, '123', '', '', '', '', '', '', 0, 1438913124, NULL, NULL, NULL, 'A-4'),
(5, '1239', '', '', '', '', '', '', 0, 1438926150, NULL, NULL, NULL, 'A-5'),
(6, '1232', '', '', '', '', '', '', 0, 1438926187, NULL, NULL, NULL, 'A-6'),
(7, '12321', '', '', '', '', '', '', 0, 1438926273, NULL, NULL, NULL, 'A-7'),
(8, 'dung.vt', 'Dung', 'Vũ Thị', '23456', '89645', '', '', 0, 1438926438, 1442502493, NULL, 5, 'A-8'),
(9, 'admin1', '', '', '111', '', '', '', 0, 1438927491, 1442545404, NULL, 5, 'A-9'),
(10, 'hien.nt4', '', '', '789', '', '', '', 0, 1438946160, NULL, NULL, NULL, 'A-10'),
(11, 'hien.nt344', 'ABC', 'XYZ', '444', '', '', '', 0, 1438946959, 1442475877, NULL, 5, 'A-11'),
(12, 'hien.ntkkk', 'hhh1', '', '333', '', '', '', 0, 1438947417, 1442484101, NULL, 4, 'A-12'),
(13, 'toanpham0509', 'Toản', 'Phạm', 'Aa1111111111', '', '', '', 0, 1443662239, NULL, NULL, 11, 'A-13'),
(14, 'tuantd36', 'Tuấn', 'Trần Đình', '123456', '0904374656', '05/12/1987', 'VP3 - Linh Đàm', 0, 1443662323, NULL, 'tuantd.tnh@gmail.com', 2, 'A-14'),
(15, 'viethoang', '', '', 'viethoang', '', '', '', 0, 1443666869, NULL, NULL, 37, 'A-15'),
(16, 'tuantd', 'Tuấn', 'Trần Đình', '123', '0904374656', '05/12/1987', 'VP6 Linh Đàm - Hoàng Mai - Hà Nội', 0, 1443866678, NULL, 'tuantd@kool-soft.com', 2, 'A-16'),
(17, 'vothang', '', '', 'vothang', '', '', '', 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE IF NOT EXISTS `warehouse` (
  `warehouse_id` int(10) NOT NULL AUTO_INCREMENT,
  `warehouse_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workplace_id` int(10) DEFAULT NULL,
  `warehouse_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(3) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_update` int(10) DEFAULT NULL,
  `is_runing` int(1) DEFAULT '1',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district_id` int(10) DEFAULT NULL,
  `warehouse_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`warehouse_id`),
  KEY `workplace_id` (`workplace_id`) USING BTREE,
  KEY `district_id` (`district_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 15360 kB; (`workplace_id`) REFER `demo_beful2/w' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`warehouse_id`, `warehouse_name`, `workplace_id`, `warehouse_address`, `status`, `create_time`, `last_update`, `is_runing`, `description`, `district_id`, `warehouse_code`) VALUES
(1, 'Kho hàng A', 26, '28B - Phố Huế', 2, 1448513590, 1449300639, 1, NULL, 5, 'S-1'),
(2, 'Kho hàng B', 27, '67 - Kim Mã', 2, 1448513621, 1449300635, 1, NULL, 1, 'S-2'),
(3, 'Kho hàng C', 28, '176B - Cầu Giấy', 2, 1448513649, 1449300630, 1, NULL, 8, 'S-3');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_out`
--

CREATE TABLE IF NOT EXISTS `warehouse_out` (
  `warehouse_out_id` int(10) NOT NULL AUTO_INCREMENT,
  `output_line_id` int(10) DEFAULT NULL,
  `input_line_id` int(10) DEFAULT NULL,
  `warehouse_out_amount` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `last_udpate` int(10) DEFAULT NULL,
  PRIMARY KEY (`warehouse_out_id`),
  KEY `output_line_id` (`output_line_id`) USING BTREE,
  KEY `input_line_id` (`input_line_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 10240 kB; (`output_line_id`) REFER `demo_befult' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `workplace`
--

CREATE TABLE IF NOT EXISTS `workplace` (
  `workplace_id` int(10) NOT NULL AUTO_INCREMENT,
  `workplace_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `workplace_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `workplace_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `create_time` int(10) NOT NULL,
  `last_update` int(10) DEFAULT NULL,
  `district_id` int(10) DEFAULT NULL,
  `workplace_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quittance_header` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quittance_footer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`workplace_id`),
  UNIQUE KEY `workplace_code` (`workplace_code`) USING BTREE,
  KEY `workplace_category_id` (`category_id`) USING BTREE,
  KEY `district_id` (`district_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='InnoDB free: 16384 kB; (`workplace_category_id`) REFER `demo' AUTO_INCREMENT=29 ;

--
-- Dumping data for table `workplace`
--

INSERT INTO `workplace` (`workplace_id`, `workplace_name`, `workplace_code`, `workplace_address`, `category_id`, `status`, `create_time`, `last_update`, `district_id`, `workplace_phone`, `quittance_header`, `quittance_footer`) VALUES
(1, 'Phòng Kế toán', NULL, 'Số 1 phố Huỳnh Thúc Kháng', 10, 0, 1438768020, 1439129458, NULL, '111', NULL, NULL),
(4, 'Phòng Chăm sóc khách hàng', NULL, '', 4, 0, 1438768224, 1439118732, NULL, '444', NULL, NULL),
(12, 'Tổng Công ty', NULL, '', 10, 0, 1442527752, NULL, 3, NULL, NULL, NULL),
(28, 'Cửa hàng C', 'CH-28', '176B - Cầu Giấy', 15, 0, 1448513558, 1449300630, 8, '0904374656', NULL, NULL),
(27, 'Cửa hàng B', 'CH-27', '67 - Kim Mã', 15, 0, 1448513525, 1449300635, 1, '0904374656', NULL, NULL),
(26, 'Cửa hàng A', 'CH-26', '28B - Phố Huế', 15, 0, 1448513490, 1449300639, 5, '0904374656', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
