<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>

<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Thẻ Kho</h4>
            <div class="search-ban-hang non-display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
            </div>
            <div class="nav-right">
            </div>
        </div>
    </div>
    <form action="" method="get">
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 535px;">
            <div class="new_product report-table" style="border-color: gray">
                <div class="form-report">
                    <table>
                        <tr>
                            <td class="report-left"><label>Năm tài chính</label></td>
                            <td class="report-right">
                                <select name="year">
                                    <?php
                                    $currentYear = date("Y");
                                    for ($i = 0; $i < 10; $i++) {
                                        ?>
                                        <option value="<?php echo $currentYear - $i; ?>"><?php
                                            echo $currentYear - $i;
                                            ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="report-left"><label>Sản phẩm</label></td>
                            <td class="report-right">
                                <select name="product_id">
                                   <option>-- Lựa chọn sản phẩm --</option>
                                    <?php
                                    if (isset($data['products']) && !empty($data['products'])) {
                                        foreach ($data['products'] as $item) {
                                            ?>
                                            <option value="<?php echo $item->product_id ?>"><?php
                                                echo $item->product_name;
                                                ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="report-left"><label>Đối tác</label></td>
                            <td class="report-right">
                                <div style="">
                                    <div class="tp-search-box">
                                        <input type="hidden" name="base_url" value="<?php
                                        echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                                        ?>" />
                                        <input name="part_name" style="width: 90%;" class="input-search form-control" placeholder="Tên hoặc mã số khách hàng..." />
                                        <div class="result">
                                            <a class="closeSearch">Đóng</a>
                                            <?php
                                            if (isset($data['customers'])) {
                                                foreach ($data['customers'] as $customer) {
                                                    $prefix = " (" .
                                                            $customer->customer_id
                                                            . ") - ";
                                                    if (isset($customer->phone)) {
                                                        $prefix .= $customer->phone;
                                                    }
                                                    ?>
                                                    <div
                                                        class="item-result"
                                                        data-name="<?php
                                                        echo unicode_str_filter(
                                                                strtolower(
                                                                        $customer->customer_name
                                                                        . $prefix
                                                                )
                                                        );
                                                        ?>"
                                                        data-id="<?php echo $customer->customer_id; ?>"
                                                        data-display="<?php
                                                        echo $customer->customer_name . $prefix
                                                        ?>"
                                                        data-input-name="customer_id"
                                                        >
                                                            <?php echo $customer->customer_name . $prefix ?>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <input name="partner_id" type="hidden" value="" />
                                    </div>
                                </div>
                            </td>
                        </tr>
<!--                        <tr>
                            <td class="report-left"><label>Nhóm sản phẩm</label></td>
                            <td class="report-right">
                                <select name="product_category_id">
                                    <option value="0">-- Lựa chọn nhóm sản phẩm --</option>
                        <?php
                        if (isset($data['categories'])) {
                            foreach ($data['categories'] as $cateogry) {
                                ?>
                                                                    <option value="<?php
                                echo $cateogry->product_category_id
                                ?>"><?php
                                echo $cateogry->product_category_name
                                ?></option>
                                <?php
                            }
                        }
                        ?>
                                </select>
                            </td>
                        </tr>-->
                        <!--
                        <tr>
                            <td class="report-left"><label>Sản phẩm</label></td>
                            <td class="report-right">
                                <select name="product_id">
                                    <option>Sản phẩm 1</option>
                                    <option>Sản phẩm 2</option>
                                    <option>Sản phẩm 3</option>
                                    <option>Sản phẩm 4</option>
                                    <option>Sản phẩm 5</option>
                                </select>
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td class="report-left"><label>Kho hàng</label></td>
                            <td class="report-right">
                                <select name="warehouse_id">
                                    <option value="0">-- Lựa chọn kho hàng --</option>
                                    <?php
                                    if (isset($data['warehouses']) && !empty($data['warehouses'])) {
                                        foreach ($data['warehouses'] as $item) {
                                            ?>
                                            <option value="<?php echo $item->warehouse_id ?>"><?php
                                                echo $item->warehouse_name;
                                                ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="report-left"><label>Ngày bắt đầu</label></td>
                            <td class="report-right">
                                <input type="date" name="date_from" value="<?php
                                if (isset($_REQUEST['date_from']) && filter_var($_REQUEST['date_from'], FILTER_VALIDATE_INT)) {
                                    echo date("Y-m-d", $_REQUEST['date_from']);
                                } else {
                                    echo date("Y-m-d", time() - 24 * 60 * 60 * 365);
                                }
                                ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="report-left"><label>Ngày kết thúc</label></td>
                            <td class="report-right">
                                <input type="date" name="date_to" value="<?php
                                if (isset($_REQUEST['date_to']) && filter_var($_REQUEST['date_to'], FILTER_VALIDATE_INT)) {
                                    echo date("Y-m-d", $_REQUEST['date_to']);
                                } else {
                                    echo date("Y-m-d", time());
                                }
                                ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="report-left"></td>
                            <td class="report-right" style="text-align: center;">

                                <a target="_blank" class="btn btn-success btnGetReportNewWindow" href="<?php
                                echo $data['urlPrint']
                                ?>">
                                    In hóa đơn</a>

                                <!--
                                   <input style="
                                                   width: auto !important;
                                                   border: none !important;
                                                   padding: 7px 30px !important;"
                                           type="submit"
                                           value="In hóa đơn"
                                           class="btn btn-success" />
                                -->
                                <input
                                    style="
                                    width: auto !important;
                                    border: none !important;
                                    padding: 7px 30px !important;"
                                    type="reset"
                                    class="btn btn-warning"
                                    value="Bỏ qua"
                                    name="destroy"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file ViewIncome.php*/
/*location: ViewIncome.php*/
