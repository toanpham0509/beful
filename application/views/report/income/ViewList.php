<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Báo cáo</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            body{
                padding: 0px;
                margin-left: 5%;
                margin-right: 5%;
                margin-top: 0px;
            }
            header{
                padding: 0px;
                margin: auto;
                height: auto;
            }
            .logo{
                background-color: #7b287c;
                width: 20%;
                position:relative;
                text-align: center;
                float: left;
            }
            .logo img{
                width: 70%;
                height:50px;
                padding: 15px;
            }
            .line{
                height: 40px;
                padding-left: 22%;
            }
            nav{
                text-align: center;
            }
            nav>h2{
                text-transform: uppercase;
                margin-bottom: 0px;
            }
            .clear{
                clear: both;
            }
            .container{
                width: 100%;
                position: relative;
                margin-top: 30px;
            }
            .table{
                width: 100%;
                background: black;
                text-align: center;
            }
            .table-header{
                background: #a9d08e;
            }.tbl-row{
                background: white;
            }
            .footer-center,.footer-left,.footer-right{
                width: 33%;
                height: 150px;
                text-align: center;
            }
            footer{
                margin-top: 20px;
            }
            .footer-left,.footer-center{
                float:left;
            }
            .footer-left{
                margin-right: 0.5%;
            }
            .footer-right{
                float:right;
            }
            .footer-right>div>h4,.footer-left>div>h4,.footer-center>div>h4{
                margin: 0px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="logo">
                <img src="../../images/logo.png"/>
            </div>
            <div style="height: 40px"></div>
            <div class="line">
                <label>Địa chỉ:</label>
                <br/>
                <label>Điện thoại:</label>
            </div>
        </header>
        <nav>
            <h2>Doanh thu bán hàng</h2>
            <span>Từ ngày <?php echo date("d/m/Y", $_REQUEST['date_from']); ?> đến ngày <?php echo date("d/m/Y", $_REQUEST['date_to']); ?></span>
        </nav>
        <div class="container">
            <table class="table">
                <tr class="table-header">
                    <th>STT</th>
                    <th>Mã sản phẩm</th>
                    <th>Tên sản phẩm</th>
                    <th>ĐVT</th>
                    <th>Số lượng</th>
                    <th>Giá vốn</th>
                    <th>Giá bán</th>
                    <th>Chiết khấu</th>
                    <th>Thành tiền</th>
                    <th>Lợi nhuận</th>
                    <th>Tỷ suất lợi nhuận</th>
                </tr>
                <?php
                if (isset($data['report'])) {
                    for ($i = 0; $i < sizeof($data['report']); $i++) {
                        $totalSale = (int) ($data['report'][$i]['sale_price'] * $data['report'][$i]['amount']);
                        $totalSaleDisPro = (int) $totalSale * ($data['report'][$i]['product_discount'] / 100);
                        $totalSaleDisOrd = (int) ($totalSale - $totalSaleDisPro) * ($data['report'][$i]['order_discount'] / 100);
                        $totalDisCount = $totalSaleDisPro + $totalSaleDisOrd;
                        $sale = $totalSale - $totalDisCount;
                        $rootPrice = 0;
                        for ($j = 0; $j < sizeof($data['report'][$i]['input_lines']); $j++) {
                            if (isset($data['report'][$i]['input_lines'][$j]->root_price) && $data['report'][$i]['input_lines'][$j]->root_price != 0) {
                                $rootPrice = $rootPrice + $data['report'][$i]['input_lines'][$j]->root_price;
                            }
                        }
                        $loiNhuan = $sale - $rootPrice;
                        ?>
                        <tr class="tbl-row">
                            <td><?php echo $i + 1; ?></td>
                            <td><a target="_blank" href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/san-pham/chinh-sua/"
                                . $data['report'][$i]['product_id']
                                . System::$config->urlSuffix;
                                ?>"><?php echo $data['report'][$i]['product_code'] ?></a></td>
                            <td><?php echo $data['report'][$i]['product_name'] ?></td>
                            <td><?php echo $data['report'][$i]['product_unit_name'] ?></td>
                            <td><?php echo $data['report'][$i]['amount'] ?></td>
                            <td>
                                <?php
                                for ($j = 0; $j < sizeof($data['report'][$i]['input_lines']); $j++) {
                                    if (isset($data['report'][$i]['input_lines'][$j]->root_price)  && $data['report'][$i]['input_lines'][$j]->root_price != 0) {
                                        ?>
                                        <a target="_blank" href="<?php
                                        echo System::$config->baseUrl
                                        . "mua-hang/nhap-kho/chinh-sua/"
                                        . $data['report'][$i]['input_lines'][$j]->input_order_id
                                        . System::$config->urlSuffix;
                                        ?>"><?php echo $data['report'][$i]['input_lines'][$j]->input_code; ?></a>
                                           <?php
                                           echo ("\t");
                                           echo (" : ");
                                           echo $data['mMoney']->addDotToMoney($data['report'][$i]['input_lines'][$j]->product_price);
                                           echo ('   x   ');
                                           echo $data['mMoney']->addDotToMoney($data['report'][$i]['input_lines'][$j]->amount);
                                           echo ('   =   ');
                                           echo $data['mMoney']->addDotToMoney($data['report'][$i]['input_lines'][$j]->root_price);
                                           echo ('<br/>');
                                       }
                                   }
                                   echo ('<hr/>');
                                   echo $data['mMoney']->addDotToMoney($rootPrice);
                                   ?>
                            </td>
                            <td><?php echo $data['mMoney']->addDotToMoney($data['report'][$i]['sale_price']); ?></td>
                            <td><?php echo $data['mMoney']->addDotToMoney($totalDisCount) ?></td>
                            <td><?php echo $data['mMoney']->addDotToMoney($sale); ?></td>
                            <td><?php
                                echo $data['mMoney']->addDotToMoney($loiNhuan);
                                ?></td>
                            <td><?php
                                if ($totalSale > 0) {
                                    echo round($loiNhuan * 100 / $sale, 2) . "%";
                                }
                                ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>
        <footer>
            <div class="footer-left">
                <div style="height:30px;"></div>
                <div>
                    <h4>Người lập báo cáo</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-center">
                <div style="height:30px;"></div>
                <div>
                    <h4>Kế toán</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-right">
                <div style="height:30px;">Ngày <?php echo date("d") ?> Tháng <?php echo date("m"); ?> Năm <?php echo date("Y"); ?></div>
                <div>
                    <h4>Giám đốc</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
        </footer>
    </body>
</html>


<?php
/*end of file ViewIncome.php*/
/*location: ViewIncome.php*/