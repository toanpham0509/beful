<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Báo cáo</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            body{
                padding: 0px;
                margin: auto;
                width: 100%;
            }
            header{
                padding: 0px;
                margin: auto;
                height: auto;
            }
            .logo{
                background-color: #7b287c;
                width: 30%;
                position:relative;
                text-align: center;
                float: left;
            }
            .logo img{
                width: 70%;
                height:50px;
                padding: 15px;
            }
            .line{
                height: 40px;
                padding-left: 32%;
            }
            nav{
                text-align: center;
            }
            nav>h2{
                text-transform: uppercase;
                margin-bottom: 0px;
            }
            nav>h4{
                margin: 0px;
            }
            .clear{
                clear: both;
            }
            .container{
                width: 100%;
                position: relative;
                margin-top: 20px;
            }
            .table{
                width: 100%;
                background: black;
                text-align: center;
            }
            .table-header{
                background: #a9d08e;
            }.tbl-row{
                background: white;
            }
            .footer-center,.footer-left,.footer-right{
                width: 33%;
                height: 150px;
                text-align: center;
            }
            footer{
                margin-top: 20px;
            }
            .footer-left,.footer-center{
                float:left;
            }
            .footer-left{
                margin-right: 0.5%;
            }
            .footer-right{
                float:right;
            }
            .footer-right>div>h4,.footer-left>div>h4,.footer-center>div>h4{
                margin: 0px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="logo">
                <img src="../../images/logo.png"/>
            </div>
            <div style="height: 40px"></div>
            <div class="line">
                <label>Địa chỉ:</label>
                <br/>
                <label>Điện thoại:</label>
            </div>
        </header>
        <nav>
            <h2>Bảng kê xuất kho</h2>
            <span>Từ ngày <?php
                if (isset($_REQUEST['date_from']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_from']))
                    echo date("d/m/Y", strtotime($_REQUEST['date_from']));
                ?> đến ngày <?php
                if (isset($_REQUEST['date_to']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_to']))
                    echo date("d/m/Y", strtotime($_REQUEST['date_to']));
                ?></span>
            <h4>Tất cả kho hàng</h4>
        </nav>
        <div class="container">
            <table class="table">
                <tr class="table-header">
                    <th rowspan="2">STT</th>
                    <th colspan="2">CHỨNG TỪ</th>
                    <th rowspan="2">DIỄN GIẢI</th>
                    <th rowspan="2">ĐVT</th>
                    <th rowspan="2">SỐ LƯỢNG</th>
                </tr>
                <tr class="table-header">
                    <th>Ngày</th>
                    <th>Số phiếu</th>
                </tr>
                <?php
                if (isset($data['reports'])) {
                    for ($i = 0; $i < sizeof($data['reports']); $i++) {
                        ?>
                        <tr class="tbl-row">
                            <td><?php echo $i + 1; ?></td>
                            <td ><?php echo date("d/m/Y", $data['reports'][$i]['order_date']) ?></td>
                            <td ><?php echo $data['reports'][$i]['order_code']; ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php
                        if (isset($data['reports'][$i]['product']) && !empty($data['reports'][$i]['product'])) {
                            foreach ($data['reports'][$i]['product'] as $product) {
                                ?>
                                <tr class="tbl-row">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><?php
                                        echo $product['product_name'];
                                        ?></td>
                                    <td><?php
                                        echo $product['unit_name'];
                                        ?></td>
                                    <td><?php
                                        echo $product['product_amount'];
                                        ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <?php
                    }
                }
                ?>
                <!--
                <tr class="table-header">
                    <th>Ngày</th>
                    <th>Số phiếu</th>
                </tr>
                <tr class="tbl-row">
                    <td rowspan="5" style="padding-bottom: 96px">01/01/2015</td>
                    <td rowspan="5" style="padding-bottom: 96px">ĐHA-1234</td>
                    <td></td>
                    <td></td>
                    <td>500</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 1</td>
                    <td>Hộp</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 2</td>
                    <td>Cái</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 3</td>
                    <td>Thùng</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 4</td>
                    <td>Cái</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td rowspan="5" style="padding-bottom: 96px">01/01/2015</td>
                    <td rowspan="5" style="padding-bottom: 96px">ĐHA-1234</td>
                    <td></td>
                    <td></td>
                    <td>500</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 1</td>
                    <td>Hộp</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 2</td>
                    <td>Cái</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 3</td>
                    <td>Thùng</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td>Sản phẩm 4</td>
                    <td>Cái</td>
                    <td>100</td>
                </tr>
                <tr class="tbl-row">
                    <td rowspan="2">.......</td>
                    <td rowspan="2">.......</td>
                    <td></td>
                    <td></td>
                    <td>...</td>
                </tr>
                <tr class="tbl-row">
                    <td>.......</td>
                    <td>.......</td>
                    <td>.......</td>
                </tr>
                -->
            </table>
        </div>
        <footer>
            <div class="footer-left">
                <div style="height:30px;"></div>
                <div>
                    <h4>Người lập báo cáo</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-center">
                <div style="height:30px;"></div>
                <div>
                    <h4>Kế toán</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-right">
                <div style="height:30px;">Ngày ... Tháng ... Năm 20...</div>
                <div>
                    <h4>Giám đốc</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
        </footer>
    </body>
</html>


<?php
/*end of file ExportStock.php*/
/*location: ExportStock.php*/
