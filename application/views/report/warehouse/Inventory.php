<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Báo cáo</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            body{
                padding: 0px;
                margin: auto;
                width: 100%; 
            }
            header{
                padding: 0px;
                margin: auto;
                height: auto;
            }
            .logo{
                background-color: #7b287c;
                width: 30%;
                position:relative;
                text-align: center;
                float: left;
            }
            .logo img{
                width: 70%;
                height:50px;
                padding: 15px;
            }
            .line{
                height: 40px;
                padding-left: 32%;
            }
            nav{
                text-align: center;
            }
            nav>h2{
                text-transform: uppercase;
                margin-bottom: 0px;
            }
            nav>h4{
                margin: 0px;
            }
            .clear{
                clear: both;
            }
            .container{
                width: 100%;
                position: relative;
                margin-top: 20px;
            }
            .table{
                width: 100%;
                background: black;
                text-align: center;
            }
            .table-header{
                background: #a9d08e;
            }.tbl-row{
                background: white;
            }
            .footer-center,.footer-left,.footer-right{
                width: 33%;
                height: 150px;
                text-align: center;
            }
            footer{
                margin-top: 20px;
            }
            .footer-left,.footer-center{
                float:left;
            }
            .footer-left{
                margin-right: 0.5%;
            }
            .footer-right{
                float:right;
            }
            .footer-right>div>h4,.footer-left>div>h4,.footer-center>div>h4{
                margin: 0px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="logo">
                <img src="../../images/logo.png"/>
            </div>
            <div style="height: 40px"></div>
            <div class="line">
                <label>Địa chỉ:</label>
                <br/>
                <label>Điện thoại:</label>
            </div>
        </header>
        <nav>
            <h2>Tồn kho - Hạn sử dụng</h2>
            <span>Từ ngày <?php
                if (isset($_REQUEST['date_from']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_from']))
                    echo date("d/m/Y", strtotime($_REQUEST['date_from']));
                ?> đến ngày <?php
                if (isset($_REQUEST['date_to']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_to']))
                    echo date("d/m/Y", strtotime($_REQUEST['date_to']));
                ?></span>
            <h4>Kho hàng: Tất cả</h4>
        </nav>
        <div class="container">
            <table class="table">
                <tr class="table-header">
                    <th>STT</th>
                    <th>Mã sản phẩm</th>
                    <th>Tên sản phẩm</th>
                    <th>ĐVT</th>
                    <th>Ngày nhập</th>
                    <th>Hạn sử dụng</th>
                    <th>Còn hạn(Ngày)</th>
                    <th>Tồn cuối kỳ</th>
                </tr>
                <?php
                if (isset($data['reports'])) {
                    for ($i = 0; $i < sizeof($data['reports']); $i++) {
                        ?>
                        <tr class="tbl-row">
                            <td><?php echo $i + 1; ?></td>
                            <td ><?php echo $data['reports'][$i]['product_code']; ?></td>
                            <td ><?php echo $data['reports'][$i]['product_name']; ?></td>
                            <td><?php echo $data['reports'][$i]['product_unit_name']; ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?php echo $data['reports'][$i]['product_inventory']; ?></td>
                        </tr>
                        <?php
                        if (isset($data['reports'][$i]['order']) && !empty($data['reports'][$i]['order'])) {
                            foreach ($data['reports'][$i]['order'] as $order) {
                                ?>
                                <tr class="tbl-row">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <?php
                                        if (isset($order['input_date'])) {
                                            echo date("d/m/Y", $order['input_date']);
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (isset($order['expire_date']) && $order['expire_date'] > 0) {
                                            echo date("d/m/Y", $order['expire_date']);
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (isset($order['expire_date']) && $order['expire_date'] > 0) {
                                            echo floor(($order['expire_date'] - time()) / 86400);
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                    <td></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <?php
                    }
                }
                ?>
            </table>
        </div>
        <footer>
            <div class="footer-left">
                <div style="height:30px;"></div>
                <div>
                    <h4>Người lập báo cáo</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-center">
                <div style="height:30px;"></div>
                <div>
                    <h4>Kế toán</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
            <div class="footer-right">
                <div style="height:30px;">Ngày ... Tháng ... Năm 20...</div>
                <div>
                    <h4>Giám đốc</h4>
                    <span>(Ký và ghi rõ họ tên)</span>
                </div>
            </div>
        </footer>
    </body>
</html>
<?php
/*end of file TonKho.php*/
/*location: TonKho.php*/