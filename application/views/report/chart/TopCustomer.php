<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Top khách hàng</h4>
            <div class="search-ban-hang non-display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
            </div> 
            <div class="nav-right">
            </div>
        </div>
    </div>
    <div class="main_new_order main_report">
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                	echo System::$config->baseUrl
                		. "bao-cao/top-khach-hang"
                		. System::$config->urlSuffix;
                ?>" class="column-charst" style="<?php 
                	if( $data['viewType'] == "column" ) echo "background:#c0c0c0";
                ?>">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-cot.png"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "bao-cao/top-khach-hang/line"
                		. System::$config->urlSuffix;
                ?>" class="line-charts" style="<?php 
                	if( $data['viewType'] == "line" ) echo "background:#c0c0c0";
                ?>">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-duong-thang.png"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "bao-cao/top-khach-hang/pie"
                		. System::$config->urlSuffix;
                ?>" class="pie-charts" style="<?php 
                	if( $data['viewType'] == "pie" ) echo "background:#c0c0c0";
                ?>">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-hinh-tron.png"/>
                </a>
            </div> 
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <script type="text/javascript">
            $(function () {
                $('#column').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: ''
                    },
                    series: [{
                            name: 'Population',
                            data: [
								<?php 
							    	if( isset( $data[ 'reportColumn'] ) ) {
							    		$size = sizeof( $data[ 'reportColumn'] );
							    		$i = 0;
							    		for ($i = 0; $i < sizeof($data['reportColumn']); $i++) {
							    			if( !filter_var($data['reportColumn'][$i]['partner_id'], FILTER_VALIDATE_INT) ) continue;
							    			?>
							    			['<?php echo $data['reportColumn'][$i]['partner_name']; ?>', <?php echo $data['reportColumn'][$i]['total_price'] ?>]
							    			<?php
							    			if( $i < $size ) echo ",";
							    		}
							    	};
							    ?>
                            ],
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:.1f}', // one decimal
                                y: 10, // 10 pixels down from the top
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        }]
                });
                $('#pie').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            name: "Brands",
                            colorByPoint: true,
                            data: [
								<?php 
							    	if( isset( $data[ 'reportColumn'] ) ) {
							    		$size = sizeof( $data[ 'reportColumn'] );
							    		$i = 0;
							    		for ($i = 0; $i < sizeof($data['reportColumn']); $i++) {
							    			if( !filter_var($data['reportColumn'][$i]['partner_id'], FILTER_VALIDATE_INT) ) continue;
							    			?>
							    			{name: '<?php 
							    				echo $data['reportColumn'][$i]['partner_name']; 
							    			?>', y: <?php 
							    				echo $data['reportColumn'][$i]['total_price'] 
							    			?>}
							    			<?php
							    			$i++;
							    			if( $i < $size ) echo ",";
							    		}
							    	};
							    ?>
                            ]
                    }]
                });
                $('#line').highcharts({
                    title: {
                        text: '',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '',
                        x: -20
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
						<?php
					    	if( isset( $data['reportLine'] ) ) {
					    		$data['reportLine'] = $data['reportLine']['data']['item_list'];
					    		$size = sizeof( $data['reportLine'] );
					    		$i = 0;
								foreach ( $data['reportLine'] as $line ) {
									if(!isset($line['partner_name'])) break;
								?>
								{
										name: '<?php echo $line["partner_name"]; ?>',
										data: [<?php 
											for( $j = 1; $j <= 12; $j++ ) {
												$is = false;
												if( isset( $line["doanhthu"] ) && !empty( $line["doanhthu"] ) ) {
													foreach ( $line["doanhthu"] as $k => $v ) {
														if( $j == $k ) {
															if(strlen($v) == 0 ) {
																echo "0";
															} else {
																echo $v;
															}
															$is = true;
															break;
														}
													}
													
													if( $is == false ) {
														echo 0;
													}
												} else  {
													echo 0;
												};
												if( $j < 12 ) echo ",";
											}
										?>]
					        		}
					    			<?php
					    			if( $i < $size )
										echo ",";                        				
					    			$i++;
					    		}
					    	}
					    ?>
                    ]
                });
            });
        </script>
        <div id="column" class="<?php 
        	if( $data['viewType'] != "column" ) echo "non-display"; 
        ?>" style="min-width: 300px; height: 400px; padding:0px;"></div>
        <div id="pie" class=<?php 
        	if( $data['viewType'] != "pie" ) echo "non-display"; 
        ?> style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <div id="line" class="<?php 
        	if( $data['viewType'] != "line" ) echo "non-display"; 
        ?>" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
<?php
/*end of file TopCustomer.php*/
/*location: TopCustomer.php*/