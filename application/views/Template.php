<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

//get header
System::$load->view( 
	"includes/Header", 
	array( 
		"menus" => $data['menus'],
		"menuID" => $data['menuID'],
		"pageID" => $data['pageID'], 
		"pageTitle" => $data['pageTitle'],
		"menuLefts" => $data['menuLefts'],
		"menuLeftCurrent" => $data['menuLeftCurrent'],
		"userInfo" => $data['userInfo']
	)
);

//get main content
System::$load->view(
	$data[ 'viewPath' ],
	array(
		"data" => $data[ 'data' ]
	)
);

?>
<?php
System::$load->view( "includes/Footer" );
/*end of file Template.php*/
/*location: Template.php*/