<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<!DOCTYPE html>
<html>
    <head>
        <?php $baseUrl = "http://localhost/ProjectBanHang/" ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Đăng Nhập</title>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap-theme.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/index.css"/>
        <link rel="icon" href="<?php echo System::$config->baseUrl; ?>images/rellicon.png" sizes="32x32" type="image/png">
    </head>
    <body style="background: gray;">
        <div class="container">
            <form class="form-signin frmlogin" style=background:#ffffff method="POST" action="" name="form-login">

                <h2 class="form-signin-heading abc" style="color:red">Đăng nhập</h2>

                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>
                    <input type="text" name="user_name" class="form-control" id="exampleInputAmount" placeholder="Tài khoản">

                </div>
                <br />
                </span>
                <div class="input-group">
                    <div class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></div>
                    <input type="password" name="password" class="form-control datphatminh" id="exampleInputAmount" placeholder="Mật khẩu" value="<?php if (isset($_POST['password'])) echo $_POST['password']; ?>">
                </div>
                
                </span>
                <br />
                <span class="errors">
                <?php 
                	if( isset( $data[ 'message' ] ) ) echo $data['message'] . "<br /><br />";
                ?>
                </span>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="signin">Đăng nhập</button>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo System::$config->baseUrl; ?>public/js/bootstrap.min.js"></script>
    </body>
</html>
<?php
/*end of file Login.class.php*/
/*location: Login.class.php*/