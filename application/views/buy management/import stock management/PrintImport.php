<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Báo cáo</title>
        <style>
            body{
                padding: 0px;
                margin: auto;
                width: 100%;
            }
            header{
                padding: 0px;
                margin: auto;
                height: auto;
            }
            .logo{
                background-color: #7b287c;
                width: 30%;
                position:relative;
                text-align: center;
                float: left;
            }
            .logo img{
                width: 70%;
                height:50px;
                padding: 15px;
            }
            .line{
                height: 40px;
                padding-left: 32%;
            }
            nav{
                text-align: center;
            }
            nav>h2{
                text-transform: uppercase;
                margin-bottom: 0px;
            }
            nav>h4{
                margin: 0px;
            }
            .clear{
                clear: both;
            }
            .container{
                width: 100%;
                position: relative;
                margin-top: 20px;
            }
            .table{
                width: 100%;
                text-align: center;
                border-collapse: collapse;
            }
            .table td, .table th {
                border: 1px solid #000;
                border-spacing: 0;
                border-collapse: collapse;
            }
            .table-header{
            }
            .tbl-row{
            }
            .footer-center,.footer-left,.footer-right{
                width: 33%;
                height: 150px;
                text-align: center;
            }
            footer{
                margin-top: 20px;
            }
            .footer-left,.footer-center{
                float:left;
            }
            .footer-left{
                margin-right: 0.5%;
            }
            .footer-right{
                float:right;
            }
            .footer-right>div>h4,.footer-left>div>h4,.footer-center>div>h4{
                margin: 0px;
            }
        </style>
    </head>
    <body>
    <header>
        <div style="width: 50%; float: left;">
            <?php \BKFW\Bootstraps\System::$load->view("includes/bill/Header"); ?>
        </div>
        <div style="width: 25%; float: right;">
            Số: <?= $data['import']->import_code; ?><br />
            Ngày: <?= date("d/m/Y"); ?><br />
        </div>
        <div class="clear"></div>
    </header>
    <nav>
        <h2>Phiếu nhập kho</h2>
    </nav>
    <div class="container">
        <div style="width: 50%; float: left;">
            Nhà cung cấp: <b><?= $data['import']->supplier_name; ?></b><br />
            Địa chỉ: <b><?=
                $data['import']->supplier_address
                . " "
                . $data['import']->district_name
                . " "
                . $data['import']->city_name;
                ?></b><br />
            Số điện thoại: <b><?= $data['import']->supplier_phone ?></b><br />
            Ghi chú: <b><?= $data['import']->order_note; ?></b><br />
        </div>
        <div style="width: 30%; float: right;">
            Ngày nhập: <?= date("d/m/Y", $data['import']->import_date); ?><br />
        </div>
        <div class="clear"></div>
        <br />
        <table class="table">
            <tr class="table-header">
                <th>STT</th>
                <th>Tên sản phẩm</th>
                <th>ĐVT</th>
                <th>SỐ LƯỢNG</th>
                <th>Kho</th>
                <th>Ngày hết hạn</th>
                <th>Giá tiền</th>
                <th>Tổng tiền</th>
            </tr>
            <?php
            $totalimport = 0;
            if(isset($data['importLines']) && !empty($data['importLines'])) {
                $i = 0;
                foreach($data['importLines'] as $line) {
                    $i++;
                    $totalimport += $line->totalLine = $line->product_amount * $line->product_price;
                    ?>
                    <tr class="tbl-row">
                        <td><?= $i ?></td>
                        <td><?= $line->product_name; ?></td>
                        <td><?= $line->product_unit_name; ?></td>
                        <td><?= $line->product_amount; ?></td>
                        <td><?= $line->warehouse->warehouse_name; ?></td>
                        <td><?= date("d/m/Y", $line->expire_date); ?></td>
                        <td><?= $data['mMoney']->addDotToMoney($line->product_price); ?> VNĐ</td>
                        <td style="text-align: right"><?= $data['mMoney']->addDotToMoney($line->totalLine);  ?> VNĐ</td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
    <?php \BKFW\Bootstraps\System::$load->view("includes/bill/Footer"); ?>
    </body>
    </html>
<?php
/*end of file PrintImport.php*/
/*location: PrintImport.php*/