<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action=""  method="post">
        <div class="nav-ban-hang">
            <div class="line">
            <?php 
            	if( $data['refund'] == false ) {
			?>
                <h4 style="color: #2DAE4A;">Nhập kho</h4><h4>/ Mới</h4>
            <?php } else { ?>
            	<h4 style="color: #2DAE4A;">Hàng trả về</h4><h4>/ Tạo mới</h4>
            <?php } ?>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit"
                    	name="save" 
                    	class="btn btn-success" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    	value="Lưu"/>
                    <a 
                    	href="<?php
                        if( $data['refund'] == true ) :
	                        echo System::$config->baseUrl
	                        . "mua-hang/tra-ve"
                        	. System::$config->urlSuffix;
                        else:
	                        echo System::$config->baseUrl
	                        . "mua-hang/nhap-kho"
	                        . System::$config->urlSuffix;
                        endif;
                        ?>" 
                    	class="btn btn-warning" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
                <div class="nav-center">
                	<!--
                    <a href="" class="btn btn-default" style="color:#000; border-radius: 7px; margin-right: 5px;">In phiếu nhập</a>
                    <a href="" class="btn btn-default" style="color:#000; border-radius: 7px; margin-right: 5px;">Xác nhận</a>
                    -->
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                            <?php  if( $data['refund'] == false ) : ?>
                                <p>Nhà cung cấp</p>
                            <?php else : ?>
                            	<p>Khách hàng</p>
                            <?php endif; ?>
                            </div>
                            <div class="info-post">
                            	<div style="">
                            		<div class="tp-search-box">
                            			<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl 
                            					. "ajax/supplier/getSupplier/"
 												. System::$config->urlSuffix
                            			?>" />
                                        <input class="input-search" <?php 
                                        	if( $data['refund'] == true ) {
                                        		echo 'disabled="disabled" name="customer_name"';
                                        	} else {
                                        		echo 'disabled="disabled" name="supplier_name"';
                                        	}
                                        ?> placeholder="Tên hoặc mã số nhà cung cấp..." value="<?php 
                                        ?>" />
                                        <?php 
                                        	if( $data['refund'] == false ) {
                                        ?>
                                        <div class="result">
                                        	<?php
											if (isset($data['suppliers'])) {
												foreach ($data['suppliers'] as $supplier) {
													?>
													<div 
                                                    	class="item-result" 
                                                    	data-name="<?php 
                                                    		echo unicode_str_filter(strtolower ( $supplier->supplier_name)); 
                                                    	?>" 
                                                        data-id="<?php echo $supplier->customer_id; ?>"
                                                        data-display="<?php echo $supplier->supplier_name; ?>" 
                                                        data-input-name="supplier_id"
                                                        >
                                                        <?php echo $supplier->supplier_name; ?>
                                                    </div> 
													<?php
												}
											}
											
											?>
                                        </div>
                                        <?php 
                                        	}
                                        ?>
                                        <input name="supplier_id" type="hidden" value="<?php 
                                        	if( $data['refund'] == true )
                                        		echo 27;
                                        ?>" />
                                    </div>
								</div>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Số phiếu nhập</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	value="<?php 
                                		if( isset( $data['importCode'] ) ) echo $data['importCode'];
                                	?>"
                                	name="import_code" 
                                	disabled="disabled"
                                	style="width: 80%;"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="address" 
                                	placeholder="Địa chỉ" 
                                	style="width: 100%;"
                                	disabled="disabled"
                                	value="" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày tạo</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="date" 
                                	name="import_date" 
                                	style="width: 80%;"
                                	value="<?php echo date( "Y-m-d" ); ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input
                                	disabled="disabled" 
                                	type="text" 
                                	name="district_id" 
                                	style="width:47%; padding-left: 5px; " 
                                	placeholder="TP của Tỉnh/Quận/Huyện" 
                                	value="" />
                                <input 
                                	disabled="disabled"
                                	type="text" 
                                	name="city_id" 
                                	style="width:30%; 
                                	padding-left: 5px; " 
                                	placeholder="Thành phố/Tỉnh" 
                                	value="" />
                                <input 
                                	disabled="disabled"
                                	type="text" 
                                	name="country_id" 
                                	style="width:20%; padding-left: 5px; " 
                                	placeholder="Quốc gia"
                                	value="" />
                            </div>
                        </td>
                        <td class="order-right">
	                            <div>
	                            <?php 
	                            	if( $data['refund'] == true ) {
	                            ?>
	                                <p class="don-hang-mua">Số đơn hàng bán</p>
	                            <?php } else { ?>
	                                <p class="">Số đơn đặt hàng</p>
	                            <?php } ?>
	                            </div>
	                            <div class="info-post">
	                            <?php 
	                            	if( $data['refund'] == false ) {
	                            ?>
	                                <select
	                                	name="order_id"
	                                	id="order_id"
	                                	style="width: 80%;"
	                                	class="don-hang-mua" >
	                                	<option value="0">Chọn đơn hàng mua</option>
	                                	<?php 
	                                		if( isset( $data[ 'buyOrders' ] ) ) {
	                                			foreach ( $data[ 'buyOrders' ] as $buyOrder ) {
	                                				?>
	                                				<option value="<?php 
	                                					echo $buyOrder->order_id
	                                				?>"><?php echo $buyOrder->order_code; ?></option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>	
								<?php } else { ?>
	                                <select
	                                	name="sale_order_id"
	                                	id="sale_order_id"
	                                	style="width: 80%;"
	                                	class="" >
	                                	<option value="0">Chọn đơn hàng bán</option>
	                                	<?php 
	                                		if( isset( $data[ 'saleOrders' ] ) ) {
	                                			foreach ( $data[ 'saleOrders' ] as $buyOrder ) {
	                                				?>
	                                				<option value="<?php 
	                                					echo $buyOrder->order_id
	                                				?>"><?php echo $buyOrder->order_code; ?></option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
								<?php } ?>
	                         </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                	disabled="disabled"
                                	type="text" 
                                	name="phone" 
                                	placeholder="Số điện thoại..." 
                                	style="width: 100%;"
                                	value="" />
                            </div>
                        </td>
                        <!--  
                        <td class="order-right">
                            <div>
                                <p>Kho hàng đến</p>
                            </div>
                            <div class="info-post">
                                 <select name="warehouse_id" style="width: 80%">
					                <?php 
					                    foreach( $data[ 'warehouses' ] as $warehouse ) {
					                        ?>
					                        <option
					                            value="<?php echo $warehouse->warehouse_id ?>">
					                                <?php echo $warehouse->warehouse_name; ?>
					                            </option>
					                        <?php
					                    }
					                ?>
					            </select>
                            </div>
                        </td>
                       -->
                    </tr>
                </table>  
            </div>
            <div class="buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table order_line tbl-order-detail">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
							<?php  if( $data['refund'] == false ) : ?>
                            	<th style="width: 15%">Đơn giá</th>
							<?php endif; ?>
                            <th style="width: 15%">Kho</th>
                            <th style="width: 15%">Ngày hết hạn sử dụng</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info product_detail_info_import">
                    <br/>
                    <textarea name="product_detail_info" placeholder="Các ghi chú bổ sung"></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file NewImport.php*/
/*location: NewImport.php*/