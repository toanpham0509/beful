<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action=""  method="post">
        <div class="nav-ban-hang">
            <div class="line">
            	<?php if( $data['refund'] == false ) : ?>
                <h4 style="color: #2DAE4A;">Nhập kho</h4><h4>/ Chỉnh sửa</h4>
                <?php else: ?>
                <h4 style="color: #2DAE4A;">Hàng trả về</h4><h4>/ Chỉnh sửa</h4>
                <?php endif; ?>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit"
                        name="save" 
                        class="btn btn-success" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                        value="Lưu"/>
                    <a 
                        href="<?php
                        if( $data['refund'] == true ) :
	                        echo System::$config->baseUrl
	                        . "mua-hang/tra-ve"
                        	. System::$config->urlSuffix;
                        else:
	                        echo System::$config->baseUrl
	                        . "mua-hang/nhap-kho"
	                        . System::$config->urlSuffix;
                        endif;
                        ?>" 
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
                        <?php
                        if (isset($data['message']))
                            echo "<br />" . $data['message'] . "<br />";
                        ?>
                    </div>
                </div>
                <div class="nav-center">
                	<?php
                		if( $data['refund'] == true )
                    		$linkConfirm = System::$config->baseUrl
                    			. "mua-hang/tra-ve/xac-nhan/"
                    			. $data['refund_']->refund_id
                    			. System::$config->urlSuffix;
                    	else
                    		$linkConfirm = System::$config->baseUrl
                    			. "mua-hang/nhap-kho/xac-nhan/"
 								. $data['import']->import_id
                    			. System::$config->urlSuffix;
                		System::$load->view("includes/PrintConfirm", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => $linkConfirm
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <?php  if( $data['refund'] == false ) : ?>
	                                <p>Nhà cung cấp</p>
	                            <?php else : ?>
	                            	<p>Khách hàng</p>
	                            <?php endif; ?>
                            </div>
                            <div class="info-post">
                            	<div style="">
                            		<div class="tp-search-box">
                            			<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl 
                            					. "ajax/supplier/getSupplier/"
 												. System::$config->urlSuffix
                            			?>" />
                                        <input 
                                        	class="input-search" 
                                        	placeholder="Tên hoặc mã số nhà cung cấp..."
                                        	<?php 
	                                        	if( $data['refund'] == true ) {
	                                        		echo 'disabled="disabled" name="customer_name"';
	                                        	} else {
	                                        		echo 'disabled="disabled" name="supplier_name"';
	                                        	}
                                        	?>
                                        	value="<?php 
												if( $data['refund'] == true ) {
													echo $data['refund_']->customer_name;
												} else
                                        			echo $data['import']->supplier_name 
                                        	?>" />
                                        <div class="result">
                                        	<?php
											if (isset($data['suppliers'])) {
												foreach ($data['suppliers'] as $supplier) {
													?>
													<div 
                                                    	class="item-result" 
                                                    	data-name="<?php 
                                                    		echo unicode_str_filter(strtolower ($supplier->supplier_name)); 
                                                    	?>" 
                                                        data-id="<?php echo $supplier->customer_id; ?>"
                                                        data-display="<?php echo $supplier->supplier_name; ?>" 
                                                        data-input-name="supplier_id"
                                                        >
                                                        <?php echo $supplier->supplier_name; ?>
                                                    </div> 
													<?php
												}
											}
											?>
											<input name="supplier_id" type="hidden" value="<?php 
												echo $data['import']->supplier_id
											?>" />
                                        </div>
                                    </div>
								</div>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Số phiếu nhập</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="import_code" 
                                    disabled="disabled"
                                    value="<?php
                                    if (isset($data['import']->import_code)) {
                                        echo $data['import']->import_code;
                                    }
                                    ?>"
                                    style="width: 80%;"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="address" 
                                    placeholder="Địa chỉ" 
                                    style="width: 100%;"
                                    disabled="disabled"
                                    value="<?php
                                    if( $data['refund'] == true ) {
                                    	echo $data['refund_']->customer_address;
                                    } elseif (isset($data['import']->supplier_address)) {
                                        echo $data['import']->supplier_address;
                                    }
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày tạo</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    name="import_date" 
                                    style="width: 80%;" 
                                    value="<?php
                                    echo date("Y-m-d", $data['import']->import_date);
                                    ?>"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input
                                    disabled="disabled" 
                                    type="text" 
                                    name="district_id" 
                                    style="width:47%; padding-left: 5px; " 
                                    placeholder="TP của Tỉnh/Quận/Huyện" 
                                    value="<?php
                                    if( $data['refund'] == true ) {
                                    	echo $data['refund_']->district_name;
                                    } elseif (isset($data['import']->district_name)) {
                                        echo $data['import']->district_name;
                                    }
                                    ?>" />
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="city_id" 
                                    style="width:30%; 
                                    padding-left: 5px; " 
                                    placeholder="Thành phố/Tỉnh" 
                                    value="<?php
                                    if( $data['refund'] == true ) {
                                    	echo $data['refund_']->city_name;
                                    } else
                                    if (isset($data['import']->city_name)) {
                                        echo $data['import']->city_name;
                                    }
                                    ?>" />
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="country_id" 
                                    style="width:20%; padding-left: 5px; " 
                                    placeholder="Quốc gia"
                                    value="<?php
                                    if( $data['refund'] == true ) {
                                    	echo $data['refund_']->country_name;
                                    } else
                                    if (isset($data['import']->country_name)) {
                                        echo $data['import']->country_name;
                                    }
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                        <?php  if( $data['refund'] == true ) { ?>
	                        <div>
	                            <p>Số đơn hàng</p>
	                        </div>
	                        <div class="info-post">
	                            <a target="_blank" href="<?php 
	                            	echo System::$config->baseUrl
	                            		. "ban-hang/don-hang/chinh-sua/"
 										. $data['refund_']->order_id
	                            		. System::$config->urlSuffix;
	                            ?>">
	                            	<?php echo $data['refund_']->order_code; ?>
	                            </a>
	                        </div>
	                    <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="phone" 
                                    placeholder="Số điện thoại..." 
                                    style="width: 100%;"
                                    value="<?php
                                    if( $data['refund'] == true ) {
                                    	echo $data['refund_']->customer_phone;
                                    } else
                                    if (isset($data['import']->supplier_phone)) {
                                        echo $data['import']->supplier_phone;
                                    }
                                    ?>" />
                            </div>
                        </td>
                        <!--  
                        <td class="order-right">
                            <div>
                                <p>Kho hàng đến</p>
                            </div>
                            <div class="info-post">
                                 <select name="warehouse_id" style="width: 80%">
                        <?php
                        foreach ($data['warehouses'] as $warehouse) {
                            ?>
                                                                                    <option
                                                                                        value="<?php echo $warehouse->warehouse_id ?>">
                            <?php echo $warehouse->warehouse_name; ?>
                                                                                        </option>
                            <?php
                        }
                        ?>
                                                    </select>
                            </div>
                        </td>
                        -->
                    </tr>
                </table>  
            </div>
            <div class="buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table order_line tbl-order-detail">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <?php  if( $data['refund'] == false ) : ?>
                                <th style="width: 15%">Đơn giá</th>
                            <?php endif; ?>
                            <th style="width: 15%">Kho</th>
                            <th style="width: 15%">Ngày hết hạn sử dụng</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($data['importLines'])) :
                            foreach ($data['importLines'] as $importLine) :
                                ?>
                                <tr>
                                    <td style="width: 10%; border-left: none;">
                                        <input 
                                            type="hidden" 
                                            class="input_table"
                                            name="order_line_id[]" 
                                            value="<?php
                                            echo $importLine->import_line_id;
                                            ?>" />
                                        <input 
                                            style="width: 100%" 
                                            name="product_id[]" 
                                            type="hidden"
                                            class="product_id"
                                            value="<?php
                                            echo $importLine->product_id;
                                            ?>" />
                                        <div class="html_product_id"><?php
                                            echo $importLine->product_id;
                                            ?></div>
                                    </td>
                                    <td style="width: 15%">
                                        <div class="html_product_name"><?php
                                            echo $importLine->product_name;
                                            ?></div>
                                    </td>
                                    <td style="width: 10%">
                                        <input 
                                            style="width: 80%" 
                                            class="input_table"
                                            type="number" 
                                            name="product_amount[]"
                                            value="<?php
                                            echo $importLine->product_amount;
                                            ?>"
                                            required="required" />
                                    </td>
                                    <td style="width: 10%">
                                        <div class="html_product_unit"><?php
                                            echo $importLine->product_unit_name;
                                            ?></div>
                                    </td>
                                    <?php if( $data['refund'] != true ) : ?>
                                    <td style="width: 10%">
                                        <input 
                                            style="width: 80%" 
                                            class="input_table"
                                            type="number" 
                                            name="product_price[]"
                                            required="required" 
                                            value="<?php
                                            echo $importLine->product_price;
                                            ?>" />
                                    </td>
                                    <?php endif; ?>
                                    <td>
                                        <select name="warehouse_id[]" style="width: 100%">
                                            <?php
                                            foreach ($data['warehouses'] as $warehouse) {
                                                ?>
                                                <option
                                                    value="<?php echo $warehouse->warehouse_id ?>"
                                                    <?php
                                                    if ($warehouse->warehouse_id == $importLine->warehouse_id)
                                                        echo "selected='seletected'";
                                                    ?>>
                                                        <?php echo $warehouse->warehouse_name; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td style="width: 10%">
                                        <input 
                                            style="width: 80%" 
                                            type="date" 
                                            class="input_table"
                                            name="expire_date[]"
                                            value="<?php
                                            if ($importLine->expire_date > 0)
                                                echo date("Y-m-d", $importLine->expire_date);
                                            ?>" />
                                    </td>
                                    <!-- 
                                    <td style="width: 15%;">
                                        <input
                                            style="width: 90%" 
                                            type="hidden" 
                                            name="product_total_price[]" />
                                    </td>
                                    -->
                                    <td style="width: 5%;  border-right: none">
                                        <a class="delete_order_line" onclick="deleteOrderLine(this)"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info product_detail_info_import">
                    <br/>
                    <textarea name="product_detail_info" placeholder="Các ghi chú bổ sung"></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file EditImport.php*/
/*location: EditImport.php*/