<?php
use 			BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;"><?php 
                	echo System::$lang->getString("buy_price");
                ?> / <?php echo System::$lang->getString("add_new") ?></h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        name="update" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Lưu" />
                    <a href="<?php
	                    echo System::$config->baseUrl
	                    . "mua-hang/bang-gia-mua"
	                    . System::$config->urlSuffix;
                    ?>">
                    <input 
                    	type="button" 
                    	class="btn btn-warning" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    	value="Bỏ qua"/></a>
                </div>
                <div class="nav-right">
                </div>
                <div>
                    <?php
	                    if (isset($data['message']))
	                        echo "" . $data['message'] . "<br />";
                    ?>
                </div>
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product new_sale_off">
                <div class="quotes_info product_info">
                    <table>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                                <div class="tbl-left-quotes">
                                    <label>Tên bảng giá</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="text" 
                                        name="buy_price_name"
                                        required="required"
                                        placeholder="Tên bảng giá bán..."
                                        value="<?php
                                        if (isset($data['buyPrice']["buy_price_name"]))
                                            echo $data['buyPrice']["buy_price_name"];
                                        ?>"
                                        />
                                </div>
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Thời gian bắt đầu</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="date" 
                                        name="time_start"
                                        value="<?php
	                                        if (isset($data['buyPrice']["time_start"]) 
	                                        	&& $data['buyPrice']["time_start"] > 0) {
	                                            echo date("Y-m-d", $data['buyPrice']["time_start"]);
	                                        } else { 
	                                        	echo date("Y-m-d", time());
	                                        }
                                        ?>"
                                        />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                            	<div class="tbl-left-quotes">
                                    <label>Mã bảng giá</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <?php
                                    if (isset($data['buyPrice']["buy_price_code"]))
                                        echo $data['buyPrice']["buy_price_code"];
                                    ?>
                                </div>
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Thời gian kết thúc</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="date" 
                                        name="time_end"
                                        value="<?php
                                        if (isset($data['buyPrice']["time_start"]) && $data['buyPrice']["time_end"] > 0) {
                                            echo date("Y-m-d", $data['buyPrice']["time_end"]);
                                        } else {
                                        	echo date("Y-m-d", time() + 30*24*3600);
                                        }
                                        ?>"
                                        />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                            	<div class="tbl-left-quotes">
	                                <label>Nhà cung cấp</label>
	                            </div>
	                            <div class="tbl-right-quotes">
	                            	<div style="width: 85%">
	                            		<div class="tp-search-box">
	                            			<input type="hidden" name="base_url" value="<?php 
	                            				echo System::$config->baseUrl 
	                            					. "ajax/supplier/getSupplier/"
	 												. System::$config->urlSuffix
	                            			?>" />
	                                        <input 
	                                        	class="input-search" 
	                                        	placeholder="Tên hoặc mã số nhà cung cấp..."
	                                        	required="required"
	                                        	value="<?php if(isset($data['buyPrice']["supplier_name"])) 
	                                        		echo $data['buyPrice']["supplier_name"] ?>" />
	                                        <div class="result">
	                                        	<a class="closeSearch">Đóng</a>
	                                        	<?php
												if (isset($data['suppliers'])) {
													foreach ($data['suppliers'] as $supplier) {
														?>
														<div 
	                                                    	class="item-result" 
	                                                    	data-name="<?php 
	                                                    		echo unicode_str_filter(strtolower ($supplier->supplier_name)); 
	                                                    	?>" 
	                                                        data-id="<?php echo $supplier->customer_id; ?>"
	                                                        data-display="<?php echo $supplier->supplier_name; ?>" 
	                                                        data-input-name="supplier_id"
	                                                        >
	                                                        <?php echo $supplier->supplier_name; ?>
	                                                    </div> 
														<?php
													}
												}
												?>
												<input name="supplier_id" type="hidden" value="<?php
													if(isset($data['buyPrice']["supplier_id"]))
														echo $data['buyPrice']["supplier_id"]
												?>" />
	                                        </div>
	                                    </div>
	                                    <br />
	                                    <input
		                                    disabled="disabled" 
		                                    type="text" 
		                                    name="district_id" 
		                                    style="width:47%; padding-left: 5px; " 
		                                    placeholder="TP của Tỉnh/Quận/Huyện" 
		                                    value="<?php
		                                    	if(isset($data['buyPrice']["district_name"]))
		                                    		echo $data['buyPrice']["district_name"];
		                                    ?>" />
		                                <input 
		                                    disabled="disabled"
		                                    type="text" 
		                                    name="city_id" 
		                                    style="width:30%; 
		                                    padding-left: 5px; " 
		                                    placeholder="Thành phố/Tỉnh" 
		                                    value="<?php
			                                    if(isset($data['buyPrice']["city_name"]))
			                                    	echo $data['buyPrice']["city_name"];
		                                    ?>" />
		                                <input 
		                                    disabled="disabled"
		                                    type="text" 
		                                    name="country_id" 
		                                    style="width:20%; padding-left: 5px; " 
		                                    placeholder="Quốc gia"
		                                    value="<?php
			                                    if(isset($data['buyPrice']["country_name"]))
			                                    	echo $data['buyPrice']["country_name"];
		                                    ?>" />
									</div>
	                            </div>
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Hiệu lực</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <div class="checkbox_wrapper">
                                        <input 
                                            name="validity" 
                                            type="checkbox"
                                            value="1"
                                            <?php 
                                            	if (isset($data['buyPrice']["validity"])) { 
                                            		if($data['buyPrice']["validity"] == 1) 
                                            			echo "checked='checked'"; 
                                            	} else {
                                            		echo "checked='checked'";
                                            	}
                                            ?> />
                                        <label></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="quotes_detail">
                    <table class="table table-bordered tbl-sale-off-product tbl-quotes order_line">
                        <thead>
                            <tr style="background: #e6e7e8;">
                            	<th>Mã sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá mua</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		if( isset( $data[ 'priceLines' ] ) ) {
                        			foreach ( $data[ 'priceLines' ] as $priceLine ) {
                        				?>
                        				<tr>
											<td>
												<input 
													type="hidden" 
													name="buy_price_line_id[]"
													value="<?php 
														echo $priceLine["buy_price_line_id"];
													?>" />
												<input 
													type="hidden" 
													class="product_id" 
													name="product_id[]" 
													value="<?php 
														echo $priceLine["product_id"];
													?>" />
												<div class="html_product_id"><?php 
													echo $priceLine["product_id"];
												?> (<?php 
													echo $priceLine["product_code"];
												?>)</div>
												<!--  
												<select required="required" name="product_id[]">
													<?php 
														if( isset( $data[ 'products' ] ) )
														foreach ( $data[ 'products' ] as $product ) {
															?>
															<option 
																value="<?php echo $product->product_id ?>"
																<?php 
																	if( $product->product_id == $priceLine->product_id )
																		echo "selected='selected'";
																?>
															><?php 
																echo $product->product_name;
															?></option>
															<?php 
														}
													?>
												</select>
												-->
											</td>
											<td><?php 
												echo $priceLine["product_name"];
											?></td>
											<td>
												 <input 
												 	type="text" 
												 	required="required" 
												 	placeholder="Giá bán..." 
												 	class='input_table' 
												 	name="product_price[]"
												 	value="<?php 
												 		echo $priceLine["product_price"];
												 	?>" 
												 	/>
											</td>
											<td><a class="delete_order_line" onclick="deleteOrderLine( this )">
												<i class="glyphicon glyphicon-trash"></i>
											</a></td>
										</tr>
                        				<?php
                        			}
                        		}
                        	?>
                            <tr style="height: 50px; background: #E6E7E8;">
	                            <td colspan="9" style="line-height: 50px;border-right: none;">
	                            	<a id="add_product">
					                	<h4 style="	text-align: left; 
					                				margin-left: 10px;
					                				border-bottom: #000 1px solid; 
					                				padding-bottom: 3px; 
					                				margin-right: 10px; ">
					                		Thêm sản phẩm
					                	</h4>
					                </a>
	                            </td>
	                        </tr>
                        </tbody>
                    </table>
                    <div class="add_product">
	                    <div class="product_detail">
	                        <?php
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        	?>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
		<td>
			<input type="hidden" class="product_id" name="product_id[]" value="" />
			<input type="hidden" class="" name="buy_price_line_id[]" value="0" />
			<div class="html_product_id"></div>
		</td>
		<td><div class="html_product_name"></div></td>
		<!-- 
		<td>
			<select required="required" name="product_id[]">
				<?php 
					if( isset( $data[ 'products' ] ) )
					foreach ( $data[ 'products' ] as $product ) {
						?>
						<option value="<?php echo $product->product_id ?>"><?php 
							echo $product->product_name;
						?></option>
						<?php 
					}
				?>
			</select>
		</td>
		 -->
		<td>
			 <input type="text" required="required" placeholder="Giá bán..." class='input_table' name="product_price[]"/>
		</td>
		<td><a class="delete_order_line" onclick="deleteOrderLine( this )">
			<i class="glyphicon glyphicon-trash"></i>
		</a></td>
	</tr>
</table>
</div>
<?php
/*end of file EditPrice.php*/
/*location: EditPrice.php*/