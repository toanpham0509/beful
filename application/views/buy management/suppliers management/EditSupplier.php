<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
	<form action="" method="post">
		<div class="nav-ban-hang">
			<div class="line">
				<h4 style="color: #2DAE4A;">Khách hàng</h4>
				<h4>/ Mới</h4>
				<div class="search-ban-hang">
					<div>
						<img
							src="<?php echo System::$config->baseUrl; ?>images/icon_search.png" />
						<input type="text" name="search" id="search" class="search" />
					</div>
				</div>
			</div>
			<div class="line">
				<div class="nav-left">
					<input type="submit" class="btn btn-success"
						style="color: #fff; border-radius: 7px; margin-right: 5px; width: 100px;"
						value="Lưu" name="save" /> <a
						href="<?php
						echo System::$config->baseUrl . "mua-hang/nha-cung-cap" . System::$config->urlSuffix;
						?>"
						class="btn btn-warning"
						style="color: #fff; border-radius: 7px; margin-right: 5px;">Trở về</a>
					<div>
		                <?php
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
				</div>
				<div class="nav-right"></div>
			</div>
		</div>
		<div class="main_new_product">
			<div class="new_product">
				<div class="branches_info">
					<h4 style="text-align: left; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
					<table>
						<tr>
							<td class="order-left branches_left" style="padding-top: 10px;">
								<div class="branches_info_left">
									<p>Tên khách hàng</p>
								</div>
								<div class="info-post  branches_info_right">
									<input 
										type="text" 
										name="customer_name" 
										required="required"
										placeholder="Nhập tên nhà cung cấp (Tên công ty/cửa hàng/đại lý/..)"
										style="width: 100%"
										value="<?php 
											if( isset( $data[ 'supplier' ]->supplier_name ) ) 
												echo $data[ 'supplier' ]->supplier_name;
										?>" />
								</div>
							</td>
							<td class="order-right branches_right" style="padding-top: 10px;">
								<div class="branches_info_left">
									<p>Mã số thuế</p>
								</div>
								<div class="info-post branches_info_right">
									<input 
										type="text" 
										name="tax_code"
										style="width: 80%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->tax_code ) ) 
												echo $data[ 'supplier' ]->tax_code;
										?>" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="order-left branches_left">
								<div class="branches_info_left">
									<p>Địa chỉ</p>
								</div>
								<div class="info-post  branches_info_right">
									<input 
										type="text" 
										name="address"
										required="required"
										placeholder="Địa chỉ..." 
										style="width: 100%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->address ) ) 
												echo $data[ 'supplier' ]->address;
										?>" />
								</div>
							</td>
							<td class="order-right branches_right">
								<div>
									<p>Số tài khoản NH</p>
								</div>
								<div class="info-post">
									<input 
										type="text" 
										name="bank_number"
										style="width: 80%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->bank_number ) ) 
												echo $data[ 'supplier' ]->bank_number;
										?>" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="order-left branches_left">
								<div class="branches_info_left">
									<p></p>
								</div>
								<div class="info-post  branches_info_right">
									<select name="district_id" style="width:47%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'districts' ] ) ) {
	                                			foreach ( $data[ 'districts' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->district_id; ?>"
	                                					<?php 
	                                						if( $item->district_id == $data[ 'supplier' ]->district_id )
	                                							echo "selected='selected'";
	                                					?>>
	                                					<?php echo $item->district_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="city_id" style="width:30%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'cities' ] ) ) {
	                                			foreach ( $data[ 'cities' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->city_id; ?>"
	                                					<?php 
	                                    					if( $item->city_id == $data[ 'supplier' ]->city_id )
	                                    						echo "selected='selected'";
	                                    				?> 
	                                					>
	                                					<?php echo $item->city_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="country_id" style="width:20%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'countries' ] ) ) {
	                                			foreach ( $data[ 'countries' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->country_id; ?>"
	                                					<?php 
	                                    					if( $item->country_id == $data[ 'supplier' ]->country_id )
	                                    						echo "selected='selected'";
	                                    				?> 
	                                					>
	                                					<?php echo $item->country_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
								</div>
							</td>
							<td class="order-right branches_right">
								<div>
									<p>Ngân hàng</p>
								</div>
								<div class="info-post">
									<input 
										type="text" 
										name="bank_name" 
										style="width: 80%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->bank_name ) ) 
												echo $data[ 'supplier' ]->bank_name;
										?>" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="order-left branches_left">
								<div>
									<p>Liên hệ</p>
								</div>
								<div class="info-post">
									<input 
										type="text" 
										name="phone"
										placeholder="Số điện thoại" style="width: 49%;"
										required="required"
										value="<?php 
											if( isset( $data[ 'supplier' ]->phone ) ) 
												echo $data[ 'supplier' ]->phone;
										?>" /> 
									<input
										type="text" 
										name="fax" 
										placeholder="Fax"
										style="width: 49%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->fax ) ) 
												echo $data[ 'supplier' ]->fax;
										?>" />
								</div>
							</td>
							<td class="order-right branches_right"></td>
						</tr>
					</table>
				</div>
				<div class="branches_receipt">
					<h4 style="text-align: left; margin-bottom: 20px;">NGƯỜI LIÊN HỆ</h4>
					<table style="width: 55%; position: relative;">
						<tr>
							<td class="order-left branches_left" style="padding-left: 0px;">
								<div class="contact_person_name">
									<p>Người liên hệ</p>
								</div>
								<div class="info-post  branches_info_right">
									<input 
										type="text" 
										name="contact_person_name"
										style="width: 70%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->contact_person_name ) ) 
												echo $data[ 'supplier' ]->contact_person_name;
										?>" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="order-left branches_left" style="padding-left: 0px;">
								<div class="branches_info_left">
									<p>Số điện thoại</p>
								</div>
								<div class="info-post  branches_info_right">
									<input 
										type="text" 
										name="contact_person_phone" 
										style="width: 70%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->contact_person_phone ) ) 
												echo $data[ 'supplier' ]->contact_person_phone;
										?>" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="order-left branches_left" style="padding-left: 0px;">
								<div class="branches_info_left">
									<p>Email</p>
								</div>
								<div class="info-post  branches_info_right">
									<input 
										type="text" 
										name="contact_person_email" 
										style="width: 70%;"
										value="<?php 
											if( isset( $data[ 'supplier' ]->contact_person_email ) ) 
												echo $data[ 'supplier' ]->contact_person_email;
										?>" />
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
				<div class="product_detail_info product_detail_info_import"
					style="margin-left: 20px; margin-top: 40px; margin-bottom: 50px;">
					<br />
					<textarea name="note" placeholder="Các ghi chú bổ sung"><?php 
						if( isset( $data[ 'supplier' ]->note ) )
							echo $data[ 'supplier' ]->note;
					?></textarea>
				</div>
			</div>
		</div>
	</form>
</div>
<?php
/*end of file EditSupplier.php*/
/*location: EditSupplier.php*/