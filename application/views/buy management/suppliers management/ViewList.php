<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Nhà cung cấp</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="providers_name icon-search non-display">Tên nhà cung cấp 
                        <a href="#" id="providers_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_address icon-search non-display">Địa chỉ 
                        <a href="#" id="providers_address" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_phone icon-search non-display">Số fax/Điện thoại 
                        <a href="#" id="providers_phone" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_code_tax icon-search non-display">Mã số thuế 
                        <a href="#" id="providers_code_tax" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_contact icon-search non-display">Người liên hệ 
                        <a href="#" id="providers_contact" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_email icon-search non-display">Email 
                        <a href="#" id="providers_email" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_telephone icon-search non-display">Số điện thoại 
                        <a href="#" id="providers_telephone" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "mua-hang/nha-cung-cap/them-moi"
                . System::$config->urlSuffix
                ?>">
                    <button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "mua-hang/nha-cung-cap/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        <label>Tên nhà cung cấp 
                            <br/>
                            <a id="providers_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Địa chỉ
                            <br/>
                            <a id="providers_address" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_address" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Số fax/Điện thoại 
                            <br/>
                            <a id="providers_phone" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_phone" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label> Mã số thuế 
                            <br/>
                            <a id="providers_code_tax" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_code_tax" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Người liên hệ 
                            <br/>
                            <a id="providers_contact" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_contact" class="search-detail search-infor non-display"/>
                    </th> 
                    <th class="search-order">
                        <label>Email 
                            <br/>
                            <a id="providers_email" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_email" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Số điện thoại 
                            <br/>
                            <a id="providers_telephone" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_telephone" class="search-detail search-infor non-display"/>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['suppliers']) && !empty($data['suppliers'])) {
                    $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                    foreach ($data['suppliers'] as $supplier) {
                        $data['startList'] ++;
                        ?>
                        <tr>
                            <td>
                                <?php echo $data['startList'] ?>
                            </td>
                            <td><a class="" href="<?php
                                echo System::$config->baseUrl
                                . "mua-hang/nha-cung-cap/chinh-sua/"
                                . $supplier->supplier_id
                                . System::$config->urlSuffix;
                                ?>"><?php
                                       echo $supplier->supplier_name;
                                       ?></a></td>
                            <td><?php
                                echo $supplier->address;
                                ?></td>
                            <td style="text-align: center;"><?php
                                echo $supplier->fax . " / " . $supplier->phone;
                                ?></td>
                            <td><?php
                                echo $supplier->tax_code;
                                ?></td>
                            <td><?php
                                echo $supplier->contact_person_name;
                                ?></td>
                            <td><?php
                                echo $supplier->contact_person_email;
                                ?></td>
                            <td><?php
                                echo $supplier->contact_person_phone;
                                ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewSupply.php*/
/*location: ViewSupply.php*/