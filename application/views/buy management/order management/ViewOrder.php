<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Đơn đặt hàng</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a 
                        href="<?php
                        echo System::$config->baseUrl
                        . "mua-hang/don-hang"
                        . System::$config->urlSuffix;
                        ?>" 
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">
                        Trở về
                    </a>
                    <div>
                        <?php
                        if (isset($data['message']))
                            echo "<br />" . $data['message'] . "<br />";
                        ?>
                    </div>
                </div>
                <div class="nav-center">
                   <?php
                		System::$load->view("includes/PrintConfirmed", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => "#"
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Nhà cung cấp</p>
                            </div>
                            <div class="info-post">
                            	<div style="">
                            		<div class="tp-search-box">
                            			<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl 
                            					. "ajax/supplier/getSupplier/"
 												. System::$config->urlSuffix
                            			?>" />
                                        <input
                                        	disabled="disabled"
                                        	class="input-search" 
                                        	placeholder="Tên hoặc mã số nhà cung cấp..."
                                        	value="<?php echo $data['order']->supplier_name ?>" />
                                    </div>
								</div>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Số đơn đặt hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="order_code" 
                                    style="width: 80%;"
                                    disabled="disabled"
                                    value="<?php
                                    echo $data['order']->order_code;
                                    ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="supplier_address" 
                                    placeholder="Địa chỉ" 
                                    style="width: 100%;"
                                    disabled="disabled"
                                    value="<?php
                                    echo $data['order']->supplier_address;
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày đặt hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    required="required"
                                    name="order_date" 
                                    disabled="disabled"
                                    style="width: 80%;"
                                    value="<?php
                                    if ($data['order']->order_date > 0)
                                        echo date("Y-m-d", $data['order']->order_date);
                                    ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input
                                    disabled="disabled" 
                                    type="text" 
                                    name="district_id" 
                                    style="width:47%; padding-left: 5px; " 
                                    placeholder="TP của Tỉnh/Quận/Huyện" 
                                    value="<?php
                                    echo $data['order']->district_name;
                                    ?>" />
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="city_id" 
                                    style="width:30%; 
                                    padding-left: 5px; " 
                                    placeholder="Thành phố/Tỉnh" 
                                    value="<?php
                                    echo $data['order']->city_name;
                                    ?>" />
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="country_id" 
                                    style="width:20%; padding-left: 5px; " 
                                    placeholder="Quốc gia"
                                    value="<?php
                                    echo $data['order']->country_name;
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày nhận hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    name="order_intended_date"
                                    disabled="disabled"
                                    style="width: 80%;"
                                    value="<?php
                                    if ($data['order']->intended_date > 0)
                                        echo date("Y-m-d", $data['order']->intended_date);
                                    ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="supplier_phone" 
                                    placeholder="Số điện thoại..." 
                                    style="width: 100%;"
                                    value="<?php
                                    echo $data['order']->supplier_phone;
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table tbl-order-detail order_line">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%; border-left: none;">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Ngày ước tính</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 10%">Đơn giá</th>
                            <!-- <th style="width: 15%">Tiền thuế</th> -->
                            <th style="width: 15%">Kho hàng</th>
                            <th style="width: 15%">Tổng tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($data['orderLines'])) {
                            foreach ($data['orderLines'] as $orderLine) {
                                ?>
                                <tr>
                                    <td style="width: 10%; border-left: none;">
                                        <input type="hidden" name="order_line_id[]" value="<?php
                                        echo $orderLine->order_line_id;
                                        ?>" />
                                        <input 
                                            style="width: 100%" 
                                            name="product_id[]" 
                                            type="hidden"
                                            value="<?php
                                            echo $orderLine->product_id;
                                            ?>" />
                                            <?php
                                            echo $orderLine->product_id;
                                            ?>
                                    </td>
                                    <td style="width: 15%">
                                        <div class="">
                                            <?php echo $orderLine->product_name; ?>
                                        </div>
                                    </td>
                                    <td style="width: 10%">
                                        <input 
                                            type="date" 
                                            style="width: 90%" 
                                            name="intended_date[]"
                                            disabled="disabled"
                                            value="<?php
                                            echo date("Y-m-d", $orderLine->intended_date);
                                            ?>" />
                                    </td>
                                    <td style="width: 10%">
                                        <input 
                                            style="width: 80%" 
                                            type="number" 
                                            name="product_amount[]"
                                            onchange="calTotalPriceItem(this)"
                                            disabled="disabled"
                                            value="<?php
                                            echo $orderLine->product_amount;
                                            ?>" />
                                    </td>
                                    <td style="width: 10%">
                                        <?php
                                        echo $orderLine->product_unit_name;
                                        ?>
                                    </td>
                                    <td style="width: 10%">
                                        <input 
                                            style="width: 80%" 
                                            type="text" 
                                            name="product_price[]"
                                            disabled="disabled"
                                            onchange="calTotalPriceItem(this)"
                                            value="<?php
                                            if ($orderLine->product_price > 0)
                                                echo $orderLine->product_price;
                                            ?>" />
                                    </td>
                                    <!--  
                                    <td style="width: 15%">
                                        <select style="width: 100%" name="tax" style="border: none;">
                                            <option>Thuế GTGT 10%</option>
                                            <option>Thuế GTGT 10%</option>
                                            <option>Thuế GTGT 10%</option>
                                        </select>
                                    </td>
                                    -->
                                    <td>
                                        <select name="warehouse_id[]" style="width: 100%" disabled="disabled">
                                            <?php
                                            foreach ($data['warehouses'] as $warehouse) {
                                                ?>
                                                <option
                                                    value="<?php echo $warehouse->warehouse_id ?>"
                                                    <?php
                                                    if ($warehouse->warehouse_id == $orderLine->warehouse_id)
                                                        echo "selected='selected'";
                                                    ?>>
                                                        <?php echo $warehouse->warehouse_name; ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td style="width: 15%;">
                                            <div class="orderLinePrice">
	                                           	<?php 
		                            				echo $data[ 'mMoney' ]->addDotToMoney( 
		                            						$orderLine->product_price * $orderLine->product_amount 
		                            				) 
		                            				. " " 
 													; ?>
                                            </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        
                    </tbody>
                </table>
                <div class="add_product">
                    <div class="product_detail">
                        <?php 
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        	?>
                    </div>
                </div>
                <table class="payment">
                    <tr style="font-size: large; border-top: #000 1px solid;">
                        <td class="payment-left">Tổng:</td>
                        <td class="payment-right"><span class="totalPrice"><?php 
                        	echo $data[ 'mMoney' ]->addDotToMoney( $data[ 'order' ]->total_sale );
                        ?></span></td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info">
                    <label>Thông tin thêm</label>
                    <br/>
                    <textarea 
                    	name="order_note" 
                    	placeholder="Các ghi chú bổ sung"
                    	disabled="disabled" ><?php
                        echo $data['order']->order_note;
                        ?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
    <table>
        <tr>
            <td style="width: 10%; border-left: none;">
                <input type="hidden" name="order_line_id[]" value="0" />
                <input 
                    style="width: 100%" 
                    name="product_id[]" 
                    type="hidden"
                    class="product_id"
                    value="" />
                <div class="html_product_id"></div>
            </td>
            <td style="width: 15%">
                <div class="html_product_name"></div>
            </td>
            <td style="width: 10%">
                <input 
                    type="date" 
                    style="width: 90%" 
                    name="intended_date[]"
                    value="" />
            </td>
            <td style="width: 10%">
                <input 
                    style="width: 80%" 
                    type="number" 
                    name="product_amount[]"
                    onchange="calTotalPriceItem(this)"
                    value=""
                    required="required" />
            </td>
            <td style="width: 10%">
                <div class="html_product_unit"></div>
            </td>
            <td style="width: 10%">
                <input 
                	style="width: 80%" 
                	type="text" 
                	name="product_price[]" 
                	required="required"
                	onchange="calTotalPriceItem(this)"
                	/>
            </td>
            <td>
                <select name="warehouse_id[]" style="width: 100%">
                    <?php
                    foreach ($data['warehouses'] as $warehouse) {
                        ?>
                        <option
                            value="<?php echo $warehouse->warehouse_id ?>">
                                <?php echo $warehouse->warehouse_name; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </td>
            <td style="width: 15%;">
                <div class="orderLinePrice"></div>
            </td>
            <td style="width: 5%;  border-right: none">
                <a class="delete_order_line" onclick="deleteOrderLine(this)"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
    </table>
</div>
<?php
/*end of file Edit.php*/
/*location: Edit.php*/