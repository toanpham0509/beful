<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Đơn đặt hàng</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        class="btn btn-success" 
                        name="save"
                        style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                        value="Lưu đơn hàng"/>
                    <a
                        href="<?php
                        echo System::$config->baseUrl
                        . "mua-hang/don-hang"
                        . System::$config->urlSuffix;
                        ?>" 
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">
                        Trở về
                    </a>
                </div>
                <div class="nav-right">

                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Nhà cung cấp</p>
                            </div>
                            <div class="info-post">
                            	<?php 
                            		System::$load->view(
                            			"includes/SearchSupplier",
                            			array( "data" => array( "suppliers" => $data['suppliers'] ) )
                            		);
                            	?>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Số đơn đặt hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    value="<?php 
                                    	if( isset( $data['orderCode'] ) )
                                    		echo $data['orderCode'];
                                    ?>"
                                    name="order_code" 
                                    style="width: 80%;"
                                    disabled="disabled" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="address" 
                                    placeholder="Địa chỉ" 
                                    style="width: 100%;"
                                    disabled="disabled" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày đặt hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    required="required"
                                    name="order_date"
                                    value="<?php echo date("Y-m-d") ?>" 
                                    style="width: 80%;"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input
                                    disabled="disabled" 
                                    type="text"
                                    name="district_id" 
                                    style="width:47%; padding-left: 5px; " 
                                    placeholder="TP của Tỉnh/Quận/Huyện"/>
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="city_id" 
                                    style="width:30%; 
                                    padding-left: 5px; "placeholder="Thành phố/Tỉnh"/>
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="country_id" 
                                    style="width:20%; padding-left: 5px; "placeholder="Quốc gia"/>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Ngày nhận hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    name="order_intended_date"
                                    style="width: 80%;"
                                    value="<?php echo date("Y-m-d") ?>"  />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    disabled="disabled"
                                    type="text" 
                                    name="phone" 
                                    placeholder="Số điện thoại..." 
                                    style="width: 100%;"/>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Bảng giá mua</p>
                            </div>
                            <div class="info-post"> 
                                <select name="buy_price_id" style="width: 80%;">
                                	<option value="-1">-- Lựa chọn bảng giá mua --</option>
                                	<?php 
                                		if(isset($data['buyPrices']) 
                                			&& !empty($data['buyPrices'])) {
                                			foreach ($data['buyPrices'] as $item) {
                                				?>
                                				<option value="<?php 
                                					echo $item['buy_price_id'];
                                				?>"><?php 
                                					echo $item['buy_price_name'] . " (" . $item['buy_price_code'] . ")"
                                				?></option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table tbl-order-detail order_line">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%; border-left: none;">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Ngày ước tính</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 10%">Đơn giá</th>
                            <!-- <th style="width: 15%">Tiền thuế</th> -->
                            <th style="width: 15%">Kho hàng</th>
                            <th style="width: 15%">Tổng tiền</th>
                            <th style="width: 5%; border-right: none"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="height: 50px; background: #E6E7E8;">
                            <td colspan="9" style="line-height: 50px;border-right: none;">
                                <a id="add_product" data-buy-price="false">
                                    <h4 
                                        style="	text-align: left; 
                                        margin-left: 10px;
                                        border-bottom: #000 1px solid; 
                                        padding-bottom: 3px; 
                                        margin-right: 10px; ">
                                        Thêm sản phẩm
                                    </h4>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="add_product">
                    <div class="product_detail">
                        <?php 
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        	?>
                    </div>
                </div>
                <table class="payment">
                    <tr style="font-size: large; border-top: #000 1px solid;">
                        <td class="payment-left">Tổng: <span class="totalPrice"></span></td>
                        <td class="payment-right"></td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info">
                    <label>Thông tin thêm</label>
                    <br/>
                    <textarea name="order_note" placeholder="Các ghi chú bổ sung"></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
    <table>
        <tr>
            <td style="width: 10%; border-left: none;">
                <input type="hidden" name="order_line_id[]" value="0" />
                <input 
                    style="width: 100%" 
                    name="product_id[]" 
                    type="hidden"
                    class="product_id"
                    value="" />
                <div class="html_product_id"></div>
            </td>
            <td style="width: 15%">
                <div class="html_product_name"></div>
            </td>
            <td style="width: 10%">
                <input 
                    type="date" 
                    style="width: 90%" 
                    name="intended_date[]"
                    value="" />
            </td>
            <td style="width: 10%">
                <input 
                    style="width: 80%" 
                    type="number" 
                    name="product_amount[]"
                    onchange="calTotalPriceItem(this)"
                    value=""
                    required="required" />
            </td>
            <td style="width: 10%">
                <div class="html_product_unit"></div>
            </td>
            <td style="width: 10%">
                <input 
                	style="width: 80%" 
                	type="text" 
                	name="product_price[]" 
                	required="required" 
                	onchange="calTotalPriceItem(this)"
                	/>
            </td>
            <td>
                <select name="warehouse_id[]" style="width: 100%">
                    <?php
                    foreach ($data['warehouses'] as $warehouse) {
                        ?>
                        <option
                            value="<?php echo $warehouse->warehouse_id ?>">
                                <?php echo $warehouse->warehouse_name; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </td>
            <td style="width: 15%;">
                <div class="orderLinePrice"></div>
            </td>
            <td style="width: 5%;  border-right: none">
                <a class="delete_order_line" onclick="deleteOrderLine(this)"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
    </table>
</div>
<?php
/*end of file NewOrder.php*/
/*location: NewOrder.php*/