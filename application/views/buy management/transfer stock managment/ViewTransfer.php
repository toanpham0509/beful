<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Chuyển kho</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php
                    echo System::$config->baseUrl
                    . "mua-hang/chuyen-kho"
                    . System::$config->urlSuffix;
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                </div>
                <div class="nav-center">
                    <?php
                		System::$load->view("includes/PrintConfirmed", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => "#"
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Số phiếu nhập</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="transfer_code" 
                                    style="width: 100%;" 
                                    disabled="disabled"
                                    value="<?php
                                    echo $data['transfer']->transfer_code;
                                    ?>">
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tháng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    name="import_date" 
                                    style="width: 80%;"
                                    disabled="disabled"
                                    value="<?php
                                    if ($data['transfer']->transfer_date > 0) {
                                        echo date("Y-m-d", $data['transfer']->transfer_date);
                                    } else {
                                    	echo date("Y-m-d", time());
                                    }
                                    ?>"
                                    />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Kho hàng đi</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_from_id" style="width: 100%;" disabled="disabled">
                                    <?php
                                    foreach ($data['warehouses'] as $warehouse) {
                                        ?>
                                        <option
                                            value="<?php echo $warehouse->warehouse_id ?>"
                                            <?php
                                            if ($warehouse->warehouse_id == $data['transfer']->warehouse_from_id)
                                                echo "selected='selected'";
                                            ?>>
                                                <?php echo $warehouse->warehouse_name; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Kho hàng đến</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_to_id" style="width: 80%;" disabled="disabled">
                                    <?php
                                    foreach ($data['warehouses'] as $warehouse) {
                                        ?>
                                        <option
                                            value="<?php echo $warehouse->warehouse_id ?>"
                                            <?php
                                            if ($warehouse->warehouse_id == $data['transfer']->warehouse_to_id)
                                                echo "selected='selected'";
                                            ?>>
                                                <?php echo $warehouse->warehouse_name; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table tbl-order-detail order_line">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 15%">Đơn giá</th>
                            <th style="width: 15%">Tổng tiền</th>
                        </tr>
                        <?php 
if( isset( $data['importOrderLines'] )
                        		&& !empty( $data['importOrderLines'] ) ) {
                        			$i = 0;
                        			foreach ( $data['importOrderLines'] as $item ) {
	                        		$i++;
	                        		?>
	                        		<tr>
							            <td style="width: 10%; border-left: none;">
							                <input type="hidden" name="order_line_id[]" value="<?php 
							                	echo $item->import_line_id;
							                ?>" />
							                <input 
							                    style="width: 100%" 
							                    name="product_id[]" 
							                    type="hidden"
							                    class="product_id"
							                    value="<?php 
							                    	echo $item->product_id;
							                    ?>" />
							                <div class="html_product_id"><?php 
							                	echo $item->product_id;
							                ?></div>
							            </td>
							            <td style="width: 15%">
							                <div class="html_product_name"><?php 
							                	echo $item->product_name;
							                ?></div>
							            </td>
							            <td style="width: 10%">
							                <input 
							                    style="width: 80%" 
							                    type="number" 
							                    name="product_amount[]"
							                    disabled="disabled"
							                    onchange="calTotalPriceItem(this)"
							                    value="<?php 
							                    	echo $item->product_amount;
							                    ?>"
							                    required="required" />
							            </td>
							            <td style="width: 10%">
							                <div class="html_product_unit"><?php 
							                	echo $item->product_unit_name
							                ?></div>
							            </td>
							            <td style="width: 10%">
							                <input 
							                	style="width: 80%" 
							                	type="number" 
							                	name="product_price[]"
							                	disabled="disabled"
							                	onchange="calTotalPriceItem(this)" 
							                	required="required"
							                	value="<?php 
							                		echo $item->product_price;
							                	?>" />
							            </td>
							            <td style="width: 15%;">
							                <div class='orderLinePrice'><?php 
							                	echo $item->product_price * $item->product_amount;
							                ?></div>
							            </td>
							        </tr>
	                        		<?php
	                        	}
                        	}
                        ?>
                    </thead>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info product_detail_info_import">
                    <br/>
                    <textarea disabled="disabled" name="transfer_note" placeholder="Các ghi chú bổ sung"><?php
                        echo $data['transfer']->transfer_note;
                        ?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file EditTransfer.php*/
/*location: EditTransfer.php*/