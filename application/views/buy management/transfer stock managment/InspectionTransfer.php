<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Chuyển kho</h4><h4>/ <?php echo System::$lang->getString("inspection"); ?></h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php
                    echo System::$config->baseUrl
                    . "mua-hang/chuyen-kho/chinh-sua/"
 					. $data['transferId']
                    . System::$config->urlSuffix;
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
    </form>
    <div class="main_new_order">
            <div class="form-new-order" >
                  <div class="text-danger">
                  		<?php echo System::$lang->getString("error_message_inspection"); ?>
                  </div>
            </div>
            <br />
            <table class="table table-striped table-bordered tbl-main">
            	<thead>
	            	<tr>
		            	<th class="search-order">
		            		STT
		            	</th>
		            	<th>
		            		Tên sản phẩm
		            	</th>
		            	<th>
		            		Kho hàng
		            	</th>
		            	<th>
		            		Số lượng bán
		            	</th>
		            	<th>
		            		Số lượng tồn trong kho
		            	</th>
	            	</tr>
	            </thead>
	            <tbody>
	            	<?php 
	            		if(isset($data['checkInspection']) && !empty($data['checkInspection'])) {
	            			$i = 1;
	            			foreach ($data['checkInspection'] as $item) {
	            				?>
	            				<tr>
	            					<td><?php echo $i; ?></td>
	            					<td><?php echo $item->product_name; ?></td>
	            					<td><?php echo $item->warehouse_from_name; ?></td>
	            					<td><?php echo $item->product_amount . " " . $item->product_unit_name; ?></td>
	            					<td><?php echo $item->currentInventoryAmount . " " . $item->product_unit_name; ?></td>
	            				</tr>
	            				<?php
	            				$i++;
	            			}
	            		}
	            	?>
            	</tbody>
            </table>
        </div>
</div>
<?php
/*end of file InspectionTransfer.php*/
/*location: InspectionTransfer.php*/