<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Chuyển kho</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="tranfer_id icon-search non-display">Số phiếu 
                        <a href="#" id="tranfer_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="warehouse_from icon-search non-display">Kho chuyển đi 
                        <a href="#" id="warehouse_from" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="warehouse_to icon-search non-display">Kho chuyển đến 
                        <a href="#" id="warehouse_to" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "mua-hang/chuyen-kho/them-moi"
                . System::$config->urlSuffix;
                ?>">
                    <button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "mua-hang/chuyen-kho/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                	<th>STT</th>
		            <th class="search-order">
		                <label>Số phiếu 
		                    <a id="tranfer_id" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="tranfer_id" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
                        <label>Ngày tháng
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
			            <div class="search_date search-infor non-display">
			                Từ ngày <input type="date" name="date-from">
			                <br/>
			                Đến ngày <input type="date" name="date-to">
			            </div>
		            </th>
		            <th class="search-order">
		                <label>Kho chuyển đi 
		                    <a id="warehouse_from" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="warehouse_from" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Kho chuyển đến 
		                    <a id="warehouse_to" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="warehouse_to" class="search-detail search-infor non-display"/>
		            </th>
		            <th>Ghi chú</th>
		            <th>Trạng thái</th>
		            <th><i class="glyphicon glyphicon-trash"></i></th>
            </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['transfers']) && !empty($data['transfers'])) {
                	$data[ 'startList' ] = (isset( $data[ 'startList' ] )) ? $data[ 'startList' ] : 0;
                    foreach ($data['transfers'] as $transfer) {
                    	$data[ 'startList' ] ++;
                        ?>
                        <tr>
                        	<td>
		                        <?php echo $data[ 'startList' ] ?>
		                    </td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
	                                . "mua-hang/chuyen-kho/chinh-sua/"
    	                            . $transfer->transfer_id
        	                        . System::$config->urlSuffix;
                                ?>" title="Xem chi tiết, chỉnh sửa"><?php
                                       echo $transfer->transfer_code
                                       ?></td>
                            <td><?php
                                if ($transfer->transfer_date > 0) {
                                    echo date("d-m-Y", $transfer->transfer_date);
                                }
                                ?></td>
                            <td><a target="_blank" href="<?php 
                            	echo System::$config->baseUrl
                            		. "ql-kho/kho-hang/chinh-sua/"
 									. $transfer->warehouse_from_id
                            		. System::$config->urlSuffix;
                            ?>"><?php
                                echo $transfer->warehouse_from_name
                                ?></a></td>
                            <td><a target="_blank" href="<?php 
                            	echo System::$config->baseUrl
                            		. "ql-kho/kho-hang/chinh-sua/"
 									. $transfer->warehouse_to_id
                            		. System::$config->urlSuffix;
                            ?>"><?php
                                echo $transfer->warehouse_to_name
                                ?></a></td>
                            <td><?php
                                echo $transfer->transfer_note
                                ?></td>
                            <td><?php 
		                        if( $transfer->status == 2 )
		                        	echo "Đang chờ";
		                        else
		                        	echo "Hoàn thành";
	                        ?></td>
                            <td>
                            <?php if( $transfer->status == 2 ): ?>
                            	<a class="delete_order_line" 
                            		onclick="return confirm('Bạn có chắc chắn muốn xóa đơn hàng này không?')" 
                            		href="<?php 
                            			echo System::$config->baseUrl
                            				. "mua-hang/chuyen-kho/xoa/"
 											. $transfer->transfer_id
                            				. System::$config->urlSuffix;
                            		?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>
                       		<?php endif; ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/