<?php	
	class Promote_Controller extends Base_Controller {
		public function __construct() {		
			parent::__construct();
			$this->this_view = new Promote_View;
		}
		
		function update_promote($new_info){
		//	if($_REQUEST["sale_off_name"] == null)
		//		$_REQUEST["sale_off_name"]= "---";
			
			if(isset($_REQUEST['sale_off_effective']) and $_REQUEST['sale_off_effective'] == 'on')
				$_REQUEST['sale_off_effective'] = 1;
			else
				$_REQUEST['sale_off_effective'] = 0;
			
			$sale_off_date_start	= str_replace('/', '-', $_REQUEST['sale_off_date_start']);
			$sale_off_date_end		= str_replace('/', '-', $_REQUEST['sale_off_date_end']);
			
			
			$_REQUEST['sale_off_date_start'] = strtotime($sale_off_date_start);
			$_REQUEST['sale_off_date_end'] = strtotime($sale_off_date_end);
			$new_info["data_post"]['promote_name']	= $_REQUEST['sale_off_name'];
			$new_info["data_post"]['validity']	= $_REQUEST['sale_off_effective'];
			$new_info["data_post"]['time_start']	= $_REQUEST['sale_off_date_start'];
			$new_info["data_post"]['time_end']	= $_REQUEST['sale_off_date_end'];
			return $this->api_caller($new_info);
		}
		
		function get_promotes(){
			if(isset($_REQUEST["sale_off_name"])){			
				$new_info	= array (
					'controller'	=> 'promote', 
				);
				if(isset($_REQUEST["promote_id"]) and  $_REQUEST["promote_id"] != null){
					$new_info["data_post"]["promote_id"]	= $_REQUEST["promote_id"];
					$new_info['action']	= 'update_promote';
				}else{
					$new_info['action']	= 'new_promote';
				}
				$update_promote =	$this->update_promote($new_info);
				$promote_id		= $update_promote ['data']['promote_id'];
				if(isset($_REQUEST['ppr']) and $_REQUEST['ppr'] != null){
					foreach($_REQUEST['ppr'] as $v){
						$v['promote_id']	= $promote_id;
						$this->update_ppr($v);
					}
				}
				if(isset($_REQUEST['por']) and $_REQUEST['por'] != null){
					foreach($_REQUEST['por'] as $v){
						$v['promote_id']	= $promote_id;
						$this->update_por($v);
					}
				}
			}		
			
			$args	= array (
				'controller'	=> 'promote', 
				'action'		=> 'get_promotes'
			);
			$data	= $this->api_caller($args);						
			$this->this_view->item_list($data['data']);
		//	echo "<pre>";
		//	print_r($_REQUEST);
		//	echo "</pre>";
		}
		
		function promote_add_new_form(){
			$args	= array (
				'controller'	=> 'category', 
				'action'		=> 'get_product_category'
			);
			$product_category	= $this->api_caller($args);
			$data['product_category']	= $product_category['data'];	
			
			$args	= array (
				'controller'	=> 'product', 
				'action'		=> 'get_list_products'
			);
			$product_list	= $this->api_caller($args);
			$data['product_list']	= $product_list['data']['item_list'];
			
			$this->this_view->promote_add_new_form($data);
		}
		
		function promote_edit_form(){
			$args	= array (
				'controller'	=> 'promote', 
				'action'		=> 'get_promote'
			);
			$args["data_post"]['promote_id']	= $_REQUEST['args'];
		//	$this->update_promote($update_info);
			
			$data	= $this->api_caller($args);	
			$data = $data['data'][0];
		//	echo $data[0]['time_start'] = date('m/d/Y',$data[0]['time_start']);
			if($data['time_start'] != 0)
				$data['time_start'] = date('m/d/Y',$data['time_start']);
			else
				$data['time_start'] = null;
			if($data['time_end'] != 0)
				$data['time_end'] = date('m/d/Y',$data['time_end']);
			else
				$data['time_end'] = null;
			
			$data['ppr_info'] = $this->get_ppr_info($args["data_post"]['promote_id']);
			$data['por_info'] = $this->get_por_info($args["data_post"]['promote_id']);
			
			
			$args	= array (
				'controller'	=> 'category', 
				'action'		=> 'get_product_category'
			);
			$product_category	= $this->api_caller($args);
			$data['product_category']	= $product_category['data'];
			
			$args	= array (
				'controller'	=> 'product', 
				'action'		=> 'get_list_products'
			);
			$product_list	= $this->api_caller($args);
			$data['product_list']	= $product_list['data']['item_list'];
			
			$this->this_view->promote_edit_form($data);
		//	$this->this_view->promote_add_new_form();
		}
		
		function update_ppr($args){
			if(isset($args['promote_product_id']) and $args['promote_product_id']==null){
				unset($args['promote_product_id']);
				unset($args['delete']);
				$new_info['action']	= 'new_promote_product';
			}elseif(isset($args['delete']) and $args['delete'] == 1){
				$new_info['action']	= 'delete_promote_product';
			}else{
				unset($args['delete']);
				$new_info['action']	= 'update_promote_product';
			}
			$new_info["data_post"]= $args;
			$new_info['controller']	= 'promote_product';
			return $this->api_caller($new_info);
		}
		
		function update_por($args){
			if($args['promote_order_id']==null){
				unset($args['promote_order_id']);
				unset($args['delete']);
				$new_info['action']	= 'new_promote_order';
			}elseif(isset($args['delete']) and $args['delete'] == 1){
				$new_info['action']	= 'delete_promote_order';
			}else{
				unset($args['delete']);
				$new_info['action']	= 'update_promote_order';
			}
			$new_info["data_post"]= $args;
			$new_info['controller']	= 'promote_order';
			return $this->api_caller($new_info);
			/*
			echo "<pre>";
			print_r($new_info);
			echo "</pre>";*/
		}
		
		function get_ppr_info($promote_id){			
			$args	= array (
				'controller'	=> 'promote_product', 
				'action'		=> 'get_promote_products'
			);
			$args['data_post']['promote_id'] = $promote_id;
			$data	= $this->api_caller($args);	
			if(isset($data['data']['item_list']))
				return $data['data']['item_list'];
			else
				return false;
		}
		
		function get_por_info($promote_id){			
			$args	= array (
				'controller'	=> 'promote_order', 
				'action'		=> 'get_promote_orders'
			);
			$args['data_post']['promote_id'] = $promote_id;
			$data	= $this->api_caller($args);	
			if(isset($data['data']['item_list']))
				return $data['data']['item_list'];
			else
				return false;
		}
	}