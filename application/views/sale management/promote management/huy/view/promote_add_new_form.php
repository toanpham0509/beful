     <?php 
		if(isset($data['ppr_info'])){
			$row_ppr_class = count($data['ppr_info']);
		}else{
			$row_ppr_class = 0;
		}
		
		if(isset($data['por_info'])){
			$row_por_class = count($data['por_info']);
		}else{
			$row_por_class = 0;
		}
	?>
	<table style='display:none;'>
		<tbody id='ppr_tmp'  class="<?php echo $row_ppr_class; ?>">
		 <?php require_once('promote_product_row.php');?>
		</tbody>
		<tbody id='por_tmp'  class="<?php echo $row_por_class; ?>">
		 <?php require_once('promote_order_row.php');?>
		</tbody>
	</table>
	<form action="" method="post">
        <?php
		if(isset($data['promote_id']) and $data['promote_id'] !=null){
		?>
		<input type='text' style='display:None;' name='promote_id' value="
		<?php
			echo $data['promote_id'];
		?>
		">
		<?php	
		}
		?>
		<div class="nav-ban-hang nav-sale-off">
            <div class="line">
                <h4 style="color: #2DAE4A;">Khuyến mại / Giảm giá</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo $this->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input type="submit" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Lưu"/>
                    <a href=""><input type="button" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;" value="Bỏ qua"/></a>
                </div> 
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product new_sale_off">
                <div class="sale_off_info product_info">
                    <table>
                        <tr>
                            <td class="tbl-left sale_off_info_left">
                                <label>Tên chương trình Khuyến mại/Giảm giá</label>
                            </td>
                            <td class="tbl-right sale_off_info_right">
                                <input value="<?php 
								if(isset($data['promote_name']) and  $data['promote_name'] != null) 
									echo $data['promote_name']; ?>"
								type="text" name="sale_off_name" style="width: 80%"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left sale_off_info_left">
                                <label>Ngày bắt đầu</label>
                            </td>
                            <td class="tbl-right sale_off_info_right">
                                <input value="<?php 
								if(isset($data['time_start']) and  $data['time_start'] != null)
								echo $data['time_start']; ?>"type="text" name="sale_off_date_start"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left sale_off_info_left">
                                <label>Ngày kết thúc</label>
                            </td>
                            <td class="tbl-right sale_off_info_right">
                                <input value="<?php 
								if(isset($data['time_end']) and  $data['time_end'] != null)
								echo $data['time_end']; ?>"type="text" name="sale_off_date_end"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left sale_off_info_left">
                                <label>Có hiệu lực</label>
                            </td>
                            <td class="tbl-right sale_off_info_right">
                                <?php 
									if(isset($data['validity']) and $data['validity'] == 1) $check = "checked";
									else		$check = null;
								?>
								<input <?php  echo $check; ?> type="checkbox" name="sale_off_effective" >
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="product_information sale_off_information">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#product_apply" data-toggle="tab">Áp dụng với sản phẩm</a></li>
                        <li><a href="#order_apply" data-toggle="tab">Áp dụng với đơn hàng</a></li>
                    </ul>
                    <div class="tab-content">
                        <!--Tab Áp dụng với sản phẩm-->
                        <div class="tab-pane active" id="product_apply"> 
                           
							<table class="table table-bordered tbl-sale-off-product">
                                <thead>
                                    <tr style="background: #e6e7e8;">
                                        <th>Nhóm sản phẩm áp dụng</th>
                                        <th>Sản phẩm áp dụng</th>
                                        <th>Số lượng mua tối thiểu</th>
                                        <th>(%) Chiết khấu</th>
                                        <th>Sản phẩm khuyến mại</th>
                                        <th>Số lượng khuyến mại</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id='promote_product_tab'>
                                    <?php //require_once('promote_product_row.php');?>
                                    <?php
								if(isset($data['ppr_info']) and $data['ppr_info'] != null)	
								foreach($data['ppr_info'] as $k=>$v){
									include('promote_product_row_2.php');
								}
									?>
                                </tbody>
                            </table>
							<table>
							<tr style="background: #e6e7e8;">
								<td colspan="7" style="text-align: left;">
								<a onclick="javascript:add_line('ppr_tmp','promote_product_tab');" 
								href="javascript:void();"class="sale_off_add">
								<label style="color:#2DAE4A;">Thêm sản phẩm</label></a></td>
							</tr>
							<tr>
								<td colspan="7"></td>
							</tr>
							<tr style="background: #e6e7e8;">
								<td colspan="7"></td>
							</tr>
							</table>
                        </div>
                        <!--Tab Áp dụng với đơn hàng-->
                        <div class="tab-pane" id="order_apply"> 
                            <table class="table table-bordered tbl-sale-off-product">
                                <thead>
                                    <tr style="background: #e6e7e8;">
                                        <th>Từ tổng tiền</th>
                                        <th>Đến tổng tiền</th>
                                        <th>(%) Chiết khấu</th>
                                        <th>Sản phẩm khuyến mại</th>
                                        <th>Số lượng khyến mại</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id='promote_order_tab'>
                                    <?php //require_once('promote_order_row.php');?>
									<?php
								if(isset($data['por_info']) and $data['por_info'] !=null)	
								foreach($data['por_info'] as $k=>$v){
									include('promote_order_row_2.php');
								}
									?>
                                   
                                </tbody>
                            </table>
							<table>
							 <tr style="background: #e6e7e8;">
                                        <td colspan="6" style="text-align: left;">
										<a onclick="javascript:add_line('por_tmp','promote_order_tab');" 
								href="javascript:void();"class="sale_off_add">
								<label style="color:#2DAE4A;">Thêm sản phẩm</label></a>
										</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"></td>
                                    </tr>
                                    <tr style="background: #e6e7e8;">
                                        <td colspan="6"></td>
                                    </tr>
							</table>		
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
