<?php
//config start---------------------------------------------------------------------	
	use 			BKFW\Bootstraps\System;
	$_REQUEST['baseUrl'] 	= System::$config->baseUrl;
	$_REQUEST['api_url']	= SERVER_API;
//config end---------------------------------------------------------------------	
	$controller = $_REQUEST['controller'] ;
	$action		= $_REQUEST['action'] ;
	
	require_once('controller/base_controller.php');
	require_once('controller/'.$controller."_controller.php");
	require_once('view/'.$controller."_view.php");
	
	$class_name	= $controller."_Controller";
	$cur_class	= new $class_name;
	$main = $cur_class->$action();