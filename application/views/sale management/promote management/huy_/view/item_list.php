<div class="content" id='right_main'>
   <div class="nav-ban-hang nav-sale-off">
        <div class="line">
            <h4>Khuyến mại / Giảm giá</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo $this->baseUrl; ?>images/icon_search.png"/>
                    <span class="saleoff icon-search non-display">Tên chương trình Khuyến mại / Giảm giá 
                        <a href="#" id="saleoff" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="start_date icon-search non-display">Ngày bắt đầu 
                        <a href="#" id="start_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="end_date icon-search non-display">Ngày kết thúc 
                        <a href="#" id="end_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
		
        <div class="line">
            <div class="nav-left">
                <?php
					$args['controller'] 	= 'promote';
					$args['action']			= 'promote_add_new_form';
					$args['position_id']	= 'right_main';
					$args['args']		= null;
				?>
				<a  <?php echo $this->a_link($args);?>>
                    <input 
                        type="button" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Tạo mới"/>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <p style="float: left;">1-1 của 
				
				<?php echo $data['num_rows_total']; ?>
				</p>
                <ul class="page">
                    <li style="border-left: gray 1px solid;;font-weight: bold; border-right: gray 1px solid;"><a href="">&lt;</a></li> 
                    <li style="border-right: gray 1px solid; font-weight: bold;"><a href="">&gt;</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>Tên chương trình Khuyến mại / Giảm giá 
                            <br/>
                            <a id="saleoff" href="#">
                                <img src="<?php echo $this->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="saleoff" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày bắt đầu 
                            <br/>
                            <a id="start_date" href="#">
                                <img src="<?php echo $this->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
            <div class="search-infor date_1 non-display">
                <input type="date" name="start_date"/>
            </div>
            </th>
            <th class="search-order">
                <label> Ngày kết thúc 
                    <br/>
                    <a id="end_date" href="#">
                        <img src="<?php echo $this->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
            <div class="search-infor date_2 non-display">
                <input type="date" name="end_date"/>
            </div>
            </th>
            </tr>
            </thead>
            <tbody>
                 <?php
					$args['action']			= 'promote_edit_form';
				?>
				<?php 
					foreach($data['item_list'] as $k=>$v){ 
						$args['args']		= $v['promote_id'];
				?>
					<tr>
						<td><a <?php echo $this->a_link($args);?>>
						<?php 
							if($v["promote_name"] == null) $v["promote_name"] = "---";
							echo $v["promote_name"]; 
						?></a></td>
						<td><?php 
						if($v["time_start"] != 0)	echo date('m/d/Y',$v["time_start"]); 
						?></td>
						<td><?php 
						if($v["time_end"] != 0)	echo date('m/d/Y',$v["time_end"]); 
						?></td>
					</tr>
				<?php }?>
				
               
            </tbody>
        </table> 
    </div>
</div>
<script>
function add_line(tmp,pos){
	var tmp_class 	= $("#"+tmp).attr("class");
	var row_html	= $("#"+tmp).html();
	row_html		= row_html.split("$row_name").join(tmp_class);
	$("#"+tmp).attr("class", parseInt(tmp_class)+1);	
	
	$("#"+pos).append( row_html );
}
	function _(el){
		return document.getElementById(el);
	}
</script>