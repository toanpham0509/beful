<?php	
	class Base_Controller{
		public function __construct() {		
			$this->api_url	= $_REQUEST['api_url'];
		}
		
		function api_caller($args){
			$content = http_build_query ($args);
			$opts = array(
			  'http'=>array(
				'method'=>"POST",				
				'content' => $content	,
				 'header'=>  "Content-Type: application/x-www-form-urlencoded\r\n" .
                "Accept: application/x-www-form-urlencoded\r\n"	
			  )
			);
			$context = stream_context_create($opts);
			$fp = fopen($this->api_url, 'r', false, $context);
			 $response = @stream_get_contents($fp);
			fclose($fp);
			return json_decode($response,true);
		}
	}