<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Hệ thống cửa hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php
                    echo System::$config->baseUrl;
                    ;
                    ?>images/icon_search.png"/>
                    <span class="shop_name icon-search non-display">Tên địa điểm bán hàng 
                        <a href="#" id="shop_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="shop_address icon-search non-display">Địa chỉ 
                        <a href="#" id="shop_address" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="shop_telephone icon-search non-display">Số điện thoại 
                        <a href="#" id="shop_telephone" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="warehouse icon-search non-display">Kho hàng 
                        <a href="#" id="warehouse" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="warehouse_management icon-search non-display">Quản lý cửa hàng 
                        <a href="#" id="warehouse_management" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "ban-hang/cua-hang/them-moi"
                . System::$config->urlSuffix;
                ?>"><input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ban-hang/cua-hang/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                	<th>STT</th>
                	<th class="search-order"*>
                		<label>Mã cửa hàng 
                            <a id="shop_code" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="shop_code" class="search-detail search-infor non-display"/>
                	</th>
                    <th class="search-order">
                        <label>Tên địa điểm bán hàng 
                            <a id="shop_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="shop_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Địa chỉ 
                            <a id="shop_address" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="shop_address" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Số điện thoại 
                            <a id="shop_telephone" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="shop_telephone" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Kho hàng 
                            <a id="warehouse" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="warehouse" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Quản lý cửa hàng 
                            <a id="warehouse_management" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="warehouse_management" class="search-detail search-infor non-display"/>
                    </th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['shops'])) {
                	$data[ 'startList' ] = (isset( $data[ 'startList' ] )) ? $data[ 'startList' ] : 0;
                    foreach ($data['shops'] as $shop) {
                    	$data[ 'startList' ] ++;
                        ?>
                        <tr>
                        	<td><?php echo $data[ 'startList' ] ?></td>
                        	<td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/cua-hang/chinh-sua/"
                                . $shop->shop_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa"><?php echo $shop->shop_code; ?></a></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/cua-hang/chinh-sua/"
                                . $shop->shop_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa"><?php echo $shop->shop_name; ?></a></td>
                            <td><?php echo $shop->shop_address; ?></td>
                            <td><?php echo $shop->phone; ?></td>
                            <td><?php 
                            	if(isset($shop->warehouse) && !empty($shop->warehouse)) {
                            		foreach ($shop->warehouse as $warehouse) {
                            			?>
                            			<a href="<?php 
                            				echo System::$config->baseUrl
                            					. "ql-kho/kho-hang/chinh-sua/"
 												. $warehouse->warehouse_id
                            					. System::$config->urlSuffix;
                            			?>" target="_blank"><?php 
                            				echo $warehouse->warehouse_name;
                            			?></a><br />
                            			<?php
                            		}
                            	}
                            ?></td>
                            <td><?php echo $shop->shop_manager_name ?></td>
                            <td>
                                <a href="<?=
                                    System::$config->baseUrl . "ban-hang/cua-hang/xem/" . $shop->shop_id . System::$config->urlSuffix;
                                ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            	<a class="delete_order_line" 
                            		onclick="return confirm('Bạn có chắc chắn muốn xóa đơn hàng này không?')" 
                            		href="<?php  
                            			echo System::$config->baseUrl
                            				. "ban-hang/cua-hang/xoa/"
 											. $shop->shop_id
                            				. System::$config->urlSuffix;
                            		?>">
									<i class="glyphicon glyphicon-trash"></i>
								</a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>

<?php
/*end of file ViewShops.php*/
/*location: ViewShops.php*/