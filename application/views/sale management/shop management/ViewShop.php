<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Hệ thống cửa hàng/<?php echo $data[ 'shop' ]->shop_name ?></h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php echo System::$config->baseUrl . "ban-hang/cua-hang" . System::$config->urlSuffix ?>">
                    	<input 
                    		type="button" 
                    		class="btn btn-warning" 
                    		style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    		value="Bỏ qua"/>
                    </a>
                </div> 
                <div class="nav-right">
                </div>
                <div>
	                <?php 
	                	if( isset( $data[ 'message' ] ) ) echo "" . $data[ 'message' ] . "<br />";
	                ?>
	            </div>
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product">
                <div class="branches_info">
                    <h4 style="text-align: left; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên địa điểm bán hàng</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="shop_name" 
                                    	required="required"
										disabled="disabled"
                                    	placeholder="Nhập tên địa điểm bán hàng" 
                                    	style="width: 100%"
                                    	value="<?php 
                                    		if( isset( $data[ 'shop' ]->shop_name  ) ) echo $data[ 'shop' ]->shop_name; 
                                    	?>" />
                                </div>
                            </td>
                            <!-- 
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Kho hàng</p>
                                </div>
                                <div class="info-post branches_info_right">
                                    <input type="text" name="branches_warehouse" required="required"  style="width: 80%;">
                                </div>
                            </td>
                            -->
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Mã cửa hàng</p>
                                </div>
                                <div class="info-post branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="shop_code"
                                    	disabled="disabled"
                                    	style="width: 80%;"
                                    	value="<?php 
                                    		if( isset( $data[ 'shop' ]->shop_code  ) ) echo $data[ 'shop' ]->shop_code; 
                                    	?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="shop_address" 
                                    	placeholder="Địa chỉ"
										disabled="disabled"
                                    	style="width: 100%;"
                                    	required="required"
                                    	value="<?php 
                                    		if( isset( $data[ 'shop' ]->shop_address  ) ) echo $data[ 'shop' ]->shop_address; 
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Người quản lý</p>
                                </div>
                                <div class="info-post">
                                    <select name="shop_manager_id" style="width: 80%;" disabled="disabled">
	                                    <?php
	                                    if (isset($data['salesPeople'])) {
	                                        foreach ($data['salesPeople'] as $item) {
	                                            ?>
	                                            <option 
	                                            	value="<?php echo $item->salesperson_id; ?>"
	                                            	<?php 
	                                            		if( $item->salesperson_id == $data[ 'shop' ]->shop_manager_id )
	                                            			echo "selected='selected'";
	                                            	?>>
	                                                <?php echo $item->salesperson_name; ?>
	                                            </option>
	                                            <?php
	                                        }
	                                    }
	                                    ?>
	                                </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p></p>
                                </div>
                                <div class="info-post  branches_info_right">
                                	<select name="district_id" style="width:47%; padding-left: 5px; " disabled="disabled">
                                	<?php 
                                		if( isset( $data[ 'districts' ] ) ) {
                                			foreach ( $data[ 'districts' ] as $item ) {
                                				?>
                                				<option 
                                					value="<?php echo $item->district_id; ?>"
                                					<?php 
                                    					if( $item->district_id == $data[ 'shop' ]->district_id )
                                    						echo "selected='selected'";
                                    				?> >
                                					<?php echo $item->district_name ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                	</select>
	                                <select name="city_id" style="width:30%; padding-left: 5px; " disabled="disabled">
	                                	<?php 
	                                		if( isset( $data[ 'cities' ] ) ) {
	                                			foreach ( $data[ 'cities' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->city_id; ?>"
	                                					<?php 
	                                    					if( $item->city_id == $data[ 'shop' ]->city_id )
	                                    						echo "selected='selected'";
	                                    				?>  >
	                                					<?php echo $item->city_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="country_id" style="width:20%; padding-left: 5px; " disabled="disabled">
	                                	<?php 
	                                		if( isset( $data[ 'countries' ] ) ) {
	                                			foreach ( $data[ 'countries' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->country_id; ?>"
	                                					<?php 
	                                    					if( $item->country_id == $data[ 'shop' ]->country_id )
	                                    						echo "selected='selected'";
	                                    				?> >
	                                					<?php echo $item->country_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
                                    <!-- <input type="text" name="branches_district" style="width:47%; padding-left: 5px; " placeholder="TP của Tỉnh/Quận/Huyện"/>
                                    <input type="text" name="branches_city" style="width:30%; padding-left: 5px; "placeholder="Thành phố/Tỉnh"/>
                                    <input type="text" name="branches_country" style="width:20%; padding-left: 5px; "placeholder="Quốc gia"/>
                                    -->
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Kho hàng</p>
                                </div>
                                <div class="info-post">
                                    <select multiple name="warehouse_id[]" style="width: 80%;" disabled="disabled">
		                            	<?php 
		                            		foreach ( $data[ 'warehouses' ] as $warehouse ) {
		                            			?>
		                            			<option
		                            				<?php 
		                            					if(isset($data['shop']->warehouse) && !empty($data['shop']->warehouse)) {
		                            						foreach ($data['shop']->warehouse as $item) {
		                            							if($item->warehouse_id == $warehouse->warehouse_id) {
		                            								echo "selected='selected'";
		                            								break;
		                            							}
		                            						}
		                            					}
		                            				?>
		                            				value="<?php echo $warehouse->warehouse_id ?>"
		                            				>
		                            				<?php echo $warehouse->warehouse_name ?>
		                            			</option>
		                            			<?php
		                            		}
		                            	?>
		                            </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div>
                                    <p>Số điện thoại</p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	name="phone"
										disabled="disabled"
                                    	placeholder="Số điện thoại"
                                    	required="required"  
                                    	style="width: 100%;"
                                    	value="<?php 
                                    		if( isset( $data[ 'shop' ]->phone  ) ) echo $data[ 'shop' ]->phone; 
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                            </td>
                        </tr>
                    </table>  
                </div>
                <div class="branches_receipt">
                    <h4 style="text-align: left;">BIÊN LAI</h4>
                    <div>
                        <label>Đầu trang biên lai</label>
                        <textarea 
                        	name="quittance_header"
							disabled="disabled"
                        	placeholder="Nhập nội dung thông tin..."><?php 
                        		if( isset( $data[ 'shop' ]->quittance_header ) ) echo $data[ 'shop' ]->quittance_header;
                        	?></textarea>
                    </div>
                    <div class="clear"></div>
                    <div style="margin-bottom: 50px">
                        <label>Cuối trang biên lai</label>
                        <textarea 
                        	name="quittance_footer"
							disabled="disabled"
                        	placeholder="Nhập nội dung..."><?php 
                        		if( isset( $data[ 'shop' ]->quittance_footer ) ) echo $data[ 'shop' ]->quittance_footer;
                        	?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file ViewShop.php*/
/*location: ViewShop.php*/