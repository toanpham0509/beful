<?php
use 			BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Đơn bán hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="order_id icon-search non-display">Số đơn hàng
                        <a href="#" id="order_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="customer_name icon-search non-display">Tên khách hàng
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="sale_employee icon-search non-display">Nhân viên bán hàng
                        <a href="#" id="sale_employee" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="money_number icon-search non-display">Số tiền
                        <a href="#" id="money_number" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_status icon-search non-display">Trạng thái đơn hàng
                        <a href="#" id="order_status" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a 
                    href="<?php echo System::$config->baseUrl ?>ban-hang/don-hang/them-moi<?php
                    echo System::$config->urlSuffix
                    ?>">
                    <button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div>
            <div class="nav-right">
                <?php
                	System::$load->view(
                        "includes/Pagination", array(
		                    "data" => array(
		                        "pages" => $data['pages'],
		                        "page" => $data['page'],
		                        "dataUrl" => $data['paginationPath']
		                    )
                        )
                	)
                ?>
                <div class="nav-icon" style="float:right">
                    <a 
                        class="icon index list-item" 
                        style="border:#000 1px solid; border-radius: 5px 0px 0px 5px" 
                        href="<?php
                        echo System::$config->baseUrl
                        . "ban-hang/don-hang"
                        . System::$config->urlSuffix
                        ?>">
                        <i class="glyphicon glyphicon-align-justify"></i>
                    </a>
                    <a class="option-window icon" href="#option-box" style="border:#000 1px solid ;">
                        <i class="glyphicon glyphicon-calendar"></i>
                    </a>
                    <a  
                        class="icon report" 
                        style="border:#000 1px solid; border-radius: 0px 5px 5px 0px" 
                        href="<?php
                        echo System::$config->baseUrl
                        . "ban-hang/don-hang/bieu-do"
                        . System::$config->urlSuffix
                        ?>">
                        <i class="glyphicon glyphicon-stats"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>
                            STT
                        </label>
                    </th>
                    <th class="search-order">
                        <label>Số đơn hàng
                            <br/>
                            <a id="order_id" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="order_id" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày tháng
                            <br/>
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
            <div class="search_date search-infor non-display">
                Từ<input type="date" name="date-from">
                <br/>
                Đến<input type="date" name="date-to">
            </div>
            </th>
            <th class="search-order">
                <label>
                    Tên khách hàng
                    <br/>
                    <a id="customer_name" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="customer_name" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Nhân viên bán hàng
                    <br/>
                    <a id="sale_employee" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="sale_employee" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Số tiền
                    <br/>
                    <a id="money_number" href="#" >
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="money_number" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Trạng thái đơn hàng
                    <br/>
                    <a id="order_status" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="order_status" class="search-detail search-infor non-display"/>
            </th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['orders'])) {
                    $total = 0;
                    $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                    foreach ($data['orders'] as $order) {
                        if (!is_object($order))
                            break;
                        $total += (int)($order->order_price * (100 - $order->discount ) / 100 );
                        $data['startList'] ++;
                        ?>
                        <tr>
                            <td><?php echo $data['startList'] ?></td>
                            <td>
                                 <?php echo $order->order_code; ?>
                            </td>
                            <td><?php if( $order->order_date > 0 ) echo date("H:i:s d/m/Y", $order->order_date); ?></td>
                            <td><?php echo $order->customer_name; ?></td>
                            <td><?php echo $order->salesperson_name; ?></td>
                            <td><?php 
								if( $order->discount == null ) $order->discount = 0;
								echo $data[ 'mMoney' ]->addDotToMoney( (int)($order->order_price * (100 - $order->discount ) / 100 ) - $order->discount_amount );
							?></td>
                            <td><?php 
                            	if( $order->order_status == 2 )
                            		echo '<span class="text-danger">Đang chờ</span></span>';
                            	else
                            		echo '<span class="text-success">Hoàn thành</span></span>';
                            ?></td>
                            <td>
                                <a
                                    href="<?php
                                    echo System::$config->baseUrl
                                        . "ban-hang/don-hang/xem/"
                                        . $order->order_id
                                        . System::$config->urlSuffix;
                                    ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            <?php if( $order->order_status == 2 ) : ?>
                                <a
                                   href="<?php
                                   echo System::$config->baseUrl
                                       . "ban-hang/don-hang/chinh-sua/"
                                       . $order->order_id
                                       . System::$config->urlSuffix;
                                   ?>">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                            	<a class="delete_order_line" 
                                   onclick="return confirm('Bạn có chắc chắn muốn xóa đơn hàng này không?')" 
                                   href="<?php
                                   echo System::$config->baseUrl
                                   . "ban-hang/don-hang/xoa/"
                                   . $order->order_id
                                   . System::$config->urlSuffix;
                                   ?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                             <?php endif; ?>
                             </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                    <td style="border: none"><?php echo $data[ 'mMoney' ]->addDotToMoney( $total ); ?></td>
                    <td style="border: none"></td>
                    <td style="border: none"></td>
                </tr>
            </tbody>
        </table> 
        <div class="clear"></div>
        <div class="option" id="option-box">
            <p class="option-title">Tùy chọn</p>
            <form class="option-content" action="#" method="get">
                <table>
                    <tbody>
                        <tr>
                            <td class="tbl-left-option">Từ   </td>
                            <td class="tbl-right-option"><input type="date" style="width: 200px;" id="date-from" name="time_from" value="<?php 
                            	if( isset( $_REQUEST[ 'time_from' ] ) ) echo $_REQUEST[ 'time_from' ]; 
                            ?>"></td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Đến   </td>
                            <td class="tbl-right-option"><input type="date" style="width: 200px;" id="date-to" name="time_to" value="<?php 
                            	if( isset( $_REQUEST[ 'time_from' ] ) ) echo $_REQUEST[ 'time_from' ]; 
                            ?>"></td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Nhân viên bán hàng</td>
                            <td class="tbl-right-option"> 
                                <select style="width: 200px;" name="salesperson_id">
                                	<option value="0">-- Lựa chọn nhân viên bánh hàng --</option>
                                    <?php 
                                		if( isset( $data[ 'salesPeople' ] ) ) {
                                			foreach ( $data[ 'salesPeople' ] as $item ) {
                                				?>
                                				<option
                                					value="<?php echo $item->salesperson_id; ?>" <?php 
													if( isset( $_REQUEST[ 'salesperson_id' ] ) && $_REQUEST[ 'salesperson_id' ] == $item->salesperson_id )
														echo "selected='selected'";
													?> >
                                					<?php echo $item->salesperson_name; ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Tên khách hàng</td>
                            <td class="tbl-right-option">
                                <select style="width: 200px;" name="customer_id">
                                	<option value="0">-- Lựa chọn khách hàng --</option>
                                    <?php
										if (isset($data['customers'])) {
											foreach ($data['customers'] as $customer) {
												?>
												<option value="<?php echo $customer->customer_id; ?>" <?php 
													if( isset( $_REQUEST[ 'customer_id' ] ) && $_REQUEST[ 'customer_id' ] == $customer->customer_id )
														echo "selected='selected'";
												?>><?php 
													echo $customer->customer_name;
												?></option>
												<?php
											}
										}
									?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Số điện thoại</td>
                            <td class="tbl-right-option"><input type="text" style="width: 200px;" name="phone" value="<?php 
                            	if( isset( $_REQUEST[ 'phone' ] ) ) echo $_REQUEST[ 'phone' ];
                            ?>" /></td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Số tiền từ</td>
                            <td class="tbl-right-option"><input type="text" style="width: 200px;" name="order_price_from" value="<?php 
                            	if( isset( $_REQUEST[ 'order_price_from' ] ) ) echo $_REQUEST[ 'order_price_from' ];
                            ?>" /></td>
                        </tr>
                        <tr>
                            <td class="tbl-left-option">Số tiền đến</td>
                            <td class="tbl-right-option"><input type="text" style="width: 200px;" name="order_price_to" value="<?php 
                            	if( isset( $_REQUEST[ 'order_price_to' ] ) ) echo $_REQUEST[ 'order_price_to' ];
                            ?>" /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><button type="submit" name="btn_submit_option" class="btn btn-success">Xác nhận</button>
                                <a class="exit" href=""><button type="button" class="btn btn-warning">Hủy</button></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/