<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 */
if (! defined ( 'BOOTSTRAP_PATH' ))
	exit ( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post" class="new_order">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Chỉnh sửa đơn hàng</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a 
                    	href="<?php 
                    			echo System::$config->baseUrl 
                    				. "ban-hang/don-hang"
 									. System::$config->urlSuffix; ?>" 
                    	class="btn btn-warning" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;">
                    	Bỏ qua
                    </a>
                </div>
                <div class="nav-center">
                	<?php
                		System::$load->view("includes/PrintConfirmed", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => "#"
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order">
            <div class="form-new-order" >
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Tên cửa hàng</p>
                            </div>
                            <div class="info-post">
                            	<?php
                                	$count = 0;
                                	if( isset( $data[ 'shops' ] ) ) {
                                			foreach ( $data[ 'shops' ] as $shop ) {                                			
                                				if( $shop->shop_id == $data[ 'order' ]->shop_id ) {
                                					$count++;
	                                				?>
	                                				<input type="hidden" name="shop_id" value="<?php echo $shop->shop_id ?>" />
	                                				<?php echo $shop->shop_name ?>
	                                				<?php
	                                				break;
                                				}
                                			}
                                	}
                                	if($count == 0) {
                                		?>
                                		<input type="hidden" name="shop_id" value="<?php if(isset($data['shops'][0]->shop_id)) echo $data['shops'][0]->shop_id; ?>" />
		                            	<?php 
		                            		if(isset($data['shops'][0]->shop_name)) echo $data['shops'][0]->shop_name;
		                            	?>
                                		<?php
                                	}
                                ?>
                            	<!-- 
                                <input 
                                	name="shop_id" 
                                	style="width: 100%;" 
                                	disabled="disabled"
                                	value="<?php echo $data[ 'order' ]->shop_name ?>" />
                                -->
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tháng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="date" 
                                	name="order_date" 
                                	disabled="disabled"
                                	style="width: 90%;"
                                	value="<?php echo date( "Y-m-d", $data['order']->order_date ); ?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Số đơn hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="" 
                                	disabled="disabled" 
                                	placeholder="" 
                                	style="width: 100%;"
                                	value="<?php echo $data[ 'order' ]->order_code; ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Bảng báo giá</p>
                            </div>
                            <div class="info-post">
                                <select name="price_id" disabled="disabled" style="width: 90%;">
                                    <?php 
                                    	if( isset( $data[ 'orderPrices' ] ) ) {
                                    		foreach ( $data[ 'orderPrices' ] as $item ) {
                                    			?>
                                    			<option 
                                    				value="<?php echo $item->price_id; ?>" 
                                    				<?php
                                    					if( $item->price_id == $data[ 'order' ]->price_id )
                                    						echo "selected='selected'";
                                    				?> >
                                    				<?php echo $item->price_name; ?>
                                    			</option>
                                    			<?php
                                    		}
                                    	}
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Khách hàng</p>
                            </div>
                            <div class="info-post">
                            	<div style="">
                                    <div class="tp-search-box">
                                    	<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                            			?>" />
                                        <input 
                                        	required="required"
                                        	class="input-search"
                                        	disabled="disabled" 
                                        	placeholder="Tên hoặc mã số khách hàng..."
                                        	value="<?php 
                                        		echo $data[ 'order' ]->customer_name;
                                        	?>" />
                                    </div>
                                </div>
                                <input name="customer_id" type="hidden" value="<?php 
                                	echo $data[ 'order' ]->customer_id;
                                ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                        	<div>
                                <p>Chương trình KM</p>
                            </div>
                            <div class="info-post">
                                <select name="promote_id" disabled="disabled" style="width: 90%;">
                                	<option value="0">-- Lựa chọn chương trình khuyến mại --</option> 
                                    <?php 
                                    	if( isset( $data[ 'promotes' ] ) ) {
                                    		foreach ( $data[ 'promotes' ] as $promote ) {
                                    			?>
                                    			<option value="<?php echo $promote->promote_id ?>"
                                    				<?php 
                                    					if( isset( $data['order']->promote_id ) 
                                    						&& $data['order']->promote_id == $promote->promote_id )
                                    						echo "selected='selected'";
                                    				?>><?php 
                                    				echo $promote->promote_name;
                                    			?></option>
                                    			<?php
                                    		}
                                    	}
                                    ?>
                                </select>
                            </div>
                        </td>
                        <!-- 
                        <td class="order-right">
                            <div>
                                <p>Kho hàng</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_id" style="width: 90%;">
                                   	<?php 
                                   		if( isset( $data[ 'warehouses' ] ) ) {
                                   			foreach ( $data[ 'warehouses' ] as $item ) {
                                   				?>
                                   				<option 
                                   					value="<?php echo $item->warehouse_id ?>"
                                   					<?php 
                                						if( $item->warehouse_id == $data[ 'order' ]->warehouse_id )
                                							echo "selected='selected'";
                                					?> >
                                   					<?php echo $item->warehouse_name ?>
                                   				</option>
                                   				<?php
                                   			}
                                   		}
                                   	?>
                                </select>
                            </div>
                        </td>
                        -->
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Địa chỉ</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="address"
                                	placeholder="Địa chỉ..." 
                                	style="width: 100%;"
                                	disabled="disabled"
                                	value="<?php echo $data[ 'order' ]->address ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Trạng thái đơn hàng</p>
                            </div>
                            <div class="info-post">
                            	Hoàn thành
                            	<!-- 
                                <select name="order_status_id" style="width: 90%;" disabled="disabled">
                                	<option <?php 
                                		if( $data['order']->status == 2 ) 
                                			echo "selected='selected'";
                                	?> value="2">Đang chờ</option>
                                    <option <?php 
                                		if( $data['order']->status == 3 ) 
                                			echo "selected='selected'";
                                	?>  value="3">Hoàn thành</option>
                                </select>
                                -->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                            	<input 
                            		name="district_id"
                            		style="width:47%; padding-left: 5px; " 
                            		disabled="disabled"
                            		value="<?php echo $data[ 'order' ]->district_name ?>" />
                            	<!-- 
                                <select name="district_id" style="width:47%; padding-left: 5px; " disabled="disabled">
                                	<?php 
                                		if( isset( $data[ 'districts' ] ) ) {
                                			foreach ( $data[ 'districts' ] as $item ) {
                                				?>
                                				<option 
                                					value="<?php echo $item->district_id; ?>"
                                					<?php 
                                    					if( $item->district_id == $data[ 'order' ]->district_id )
                                    						echo "selected='selected'";
                                    				?> >
                                					<?php echo $item->district_name ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                                -->
                                <input 
                                	name="city_id" 
                                	style="width:30%; padding-left: 5px; "
                                	disabled="disabled"
                                	value="<?php echo $data['order']->city_name  ?>" />
                                <!-- 
                                <select name="city_id" style="width:30%; padding-left: 5px; ">
                                	<?php 
                                		if( isset( $data[ 'cities' ] ) ) {
                                			foreach ( $data[ 'cities' ] as $item ) {
                                				?>
                                				<option 
                                					value="<?php echo $item->city_id; ?>"
                                					<?php 
                                    					if( $item->city_id == $data[ 'order' ]->city_id )
                                    						echo "selected='selected'";
                                    				?>  >
                                					<?php echo $item->city_name ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                                -->
                                <input 
                                	name="country_id" 
                                	style="width:20%; padding-left: 5px;"
                                	disabled="disabled"
                                	value="<?php echo $data[ 'order' ]->country_name ?>" />
                                <!-- 
                                <select name="country_id" style="width:20%; padding-left: 5px; ">
                                	<?php 
                                		if( isset( $data[ 'countries' ] ) ) {
                                			foreach ( $data[ 'countries' ] as $item ) {
                                				?>
                                				<option 
                                					value="<?php echo $item->country_id; ?>"
                                					<?php 
                                    					if( $item->country_id == $data[ 'order' ]->country_id )
                                    						echo "selected='selected'";
                                    				?> >
                                					<?php echo $item->country_name ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                                -->
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Nhân viên bán hàng</p>
                            </div>
                            <div class="info-post">
                            <?php 
                            	echo $data['order']->salesperson_name;
                            ?>
                            <!--  
                                <select name="salesperson_id" style="width: 90%;" disabled="disabled">
                                    <?php 
                                		if( isset( $data[ 'salesPeople' ] ) ) {
                                			foreach ( $data[ 'salesPeople' ] as $item ) {
                                				?>
                                				<option
                                					value="<?php echo $item->salesperson_id; ?>"
                                					<?php
                                						if( $item->salesperson_id == $data[ 'order' ]->salesperson_id  )
                                							echo "selected = 'selected'";
                                					?> >
                                					<?php echo $item->salesperson_name; ?>
                                				</option>
                                				<?php
                                			}
                                		}
                                	?>
                                </select>
                           	-->
                            </div>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>  
            </div>
            <div class="order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 10px; padding-top: 5px;">Thông tin chi tiết</h4>
                <table class="table table-bordered tbl-detail order_line">
                    <thead>
                        <tr>
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>Kho hàng</th>
                            <th>Số lượng</th>
                            <th>Đơn vị</th>
                            <th>Đơn giá</th>
<!--                        <th>Tiền thuế</th>-->
                            <th>Chiếu khấu (%)</th>
                            <th>Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php 
$i = 1;
                    		if( isset( $data[ 'order' ]->order_lines ) && !empty( $data[ 'order' ]->order_lines ) )
                    		foreach ( $data[ 'order' ]->order_lines as $item ) {
                    			?>
                    			<tr>
                    				<td>
                    					<?php echo $item->product_id ; ?>
                    					<input type="hidden" name="product_id[]" value="<?php echo $item->product_id ?>" />
                    					<input type="hidden" name="order_line_id[]" value="<?php echo $item->order_line_id ?>" />
                    				</td>
		                            <td><?php echo $item->product_name ?></td>
		                            <td>
		                            	<select name="product_warehouse[]" disabled="disabled"> 
		                            	<?php 
		                            		foreach ( $data[ 'warehouses' ] as $warehouse ) {
		                            			?>
		                            			<option 
		                            				value="<?php echo $warehouse->warehouse_id ?>"
		                            				<?php 
		                            					if( $item->warehouse_id == $warehouse->warehouse_id ) echo "selected = 'selected'";
		                            				?>
		                            				>
		                            				<?php echo $warehouse->warehouse_name ?>
		                            			</option>
		                            			<?php
		                            		}
		                            	?>
		                            	</select>
		                            </td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_amount[]"
		                            		disabled="disabled"
                                            onchange="calTotalPriceItem(this)"
		                            		value="<?php echo $item->product_amount; ?>"
		                            	/>
										<br />
										Đặt hàng: <?= $item->product_amount_old; ?>
										<br />
										Trả về: <?= $item->refundCount; ?>
		                            </td>
		                            <td><?php echo $item->product_unit ?></td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_price[]"
		                            		disabled="disabled"
                                            onchange="calTotalPriceItem(this)"
		                            		value="<?php echo $item->product_price; ?>"
		                            	/>
		                            	<?php echo $item->currency_unit_name; ?>
		                            </td>
		    						<!--<td>
		                                <select style="border: none;">
		                                    <option>Thuế GTGT 10%</option>
		                                    <option>Thuế GTGT 10%</option>
		                                    <option>Thuế GTGT 10%</option>
		                                </select>
		                            </td>-->
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_discount[]"
                                            onchange="calTotalPriceItem(this)"
                                            disabled="disabled"
		                            		value="<?php echo $item->product_discount; ?>"
		                            	/>
                                        <span class="orderLineDiscount">% = <?php
                                        	$discountItem = $item->product_price * $item->product_amount * $item->product_discount / 100; 
                                        	echo $data[ 'mMoney' ]->addDotToMoney($discountItem);
										?></span>
		                            </td>
		                            <td><div class='orderLinePrice'><?php 
		                            	echo $data[ 'mMoney' ]->addDotToMoney( $item->product_price * $item->product_amount - $discountItem )
		                            				. " "
 													. $item->currency_unit_name ?></div></td>
                    			</tr>
                    			<?php
                    			$i++;
                    		}
                    	?>
                    </tbody>
                </table>
                <div class="color"></div>
                <table class="payment">
                    <tr style="font-size: 14;">
                        <td class="payment-left">Tổng tiền:</td>
                        <td class="payment-right"><span class='totalPrice'><?php 
                        	echo $data[ 'mMoney' ]->addDotToMoney( $data[ 'order' ]->order_price );
                        ?></span></td>
                    </tr>
                    <tr style="font-size: 14;">
                        <td class="payment-left" style="font-weight: normal;">Chiết khấu:</td>
                        <input type="hidden" name="order_discount" value="" />
                        <td class="payment-right"><span class='totalDiscount'><?php 
                        	$discount = (int) ($data['order']->discount * $data['order']->order_price / 100 );
                        	echo $data[ 'mMoney' ]->addDotToMoney( $discount );
                        ?></span></td>
                    </tr>
                    <tr style="font-size: 14;">
                        <td class="payment-left">Thành tiền:</td>
                        <td class="payment-right"><span class='finalTotal'><?php 
                        	echo $data[ 'mMoney' ]->addDotToMoney( $data[ 'order' ]->order_price - $discount );
                        ?></span></td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info">
                    <label>Thông tin thêm</label>
                    <br/>
                    <textarea disabled="disabled" name="order_note" placeholder="Các ghi chú bổ sung"><?php 
						echo $data[ 'order' ]->order_note;
                    ?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<br /><br />
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
        <td>
                    					<input type="hidden" class="product_id" name="product_id[]" value="" />
                    					<input type="hidden" name="order_line_id[]" value="0" />
                    					<div class="html_product_id"></div>
                    				</td>
		                            <td><div class="html_product_name"></div></td>
		                            <td>
		                            	<select name="product_warehouse[]"> 
		                            	<?php 
		                            		foreach ( $data[ 'warehouses' ] as $warehouse ) {
		                            			?>
		                            			<option 
		                            				value="<?php echo $warehouse->warehouse_id ?>"
		                            				>
		                            				<?php echo $warehouse->warehouse_name ?>
		                            			</option>
		                            			<?php
		                            		}
		                            	?>
		                            	</select>
		                            </td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_amount[]"
		                            		required="required"
                                            onchange="calTotalPriceItem(this)"
		                            		value=""
		                            	/>
		                            </td>
		                            <td><div class="html_product_unit"></div></td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="text"
		                            		name="product_price[]"
                                            onchange="calTotalPriceItem(this)"
		                            		required="required"
		                            		value=""
		                            	/>
		                            </td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="text"
		                            		name="product_discount[]"
                                            onchange="calTotalPriceItem(this)"
		                            		value="0"
		                            	/>
		                            	%
		                            </td>
		                            <td><div class='orderLinePrice'></div></td>
		                            <td>
		                            	<a class="delete_order_line" onclick="deleteOrderLine( this )"><i class="glyphicon glyphicon-trash"></i>
		                            	</a>
		                            </td>
                    			</tr>
</table>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/