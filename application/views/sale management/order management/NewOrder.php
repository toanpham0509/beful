<?php
use 			BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post" class="new_order">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Đơn hàng </h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        name="save"
                        class="btn btn-success" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                        value="Lưu đơn hàng"/>
                    <a 
                        href="<?php
                        echo System::$config->baseUrl
                        . "ban-hang/don-hang"
                        . System::$config->urlSuffix;
                        ?>" 
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">
                        Bỏ qua
                    </a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div> 
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order">
            <div class="form-new-order" >
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Tên cửa hàng</p>
                            </div>
                            <div class="info-post">
                            <!-- 
                            	<input type="hidden" name="shop_id" value="<?php if(isset($data['shops'][0]->shop_id)) echo $data['shops'][0]->shop_id; ?>" />
                            	<?php 
                            		if(isset($data['shops'][0]->shop_name)) echo $data['shops'][0]->shop_name;
                            	?>
                            -->
                                <select name="shop_id" style="width: 100%;">
                                    <?php
                                    if (isset($data['shops'])) {
                                        foreach ($data['shops'] as $shop) {
                                            ?>
                                            <option value="<?php echo $shop->shop_id ?>">
                                                <?php echo $shop->shop_name ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>                            
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tháng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="date" 
                                	name="order_date" 
                                	style="width: 90%;"
                                	value="<?php 
                                		echo date("Y-m-d");
                                	?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Số đơn hàng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="" 
                                    disabled="disabled" 
                                    placeholder="Số đơn hàng sẽ được tự động tạo ra khi thêm mới..." 
                                    style="width: 100%;"
                                    value="<?php  
                                    	if( isset( $data[ 'orderID' ] ) ) echo $data[ 'orderID' ];
                                    ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Bảng báo giá</p>
                            </div>
                            <div class="info-post">
                                <select name="price_id" style="width: 90%;">
                                	<option value="-1">-- Chọn báo giá --</option>
                                    <?php
                                    if (isset($data['orderPrices'])) {
                                        foreach ($data['orderPrices'] as $item) {
                                            ?>
                                            <option value="<?php echo $item->price_id; ?>">
                                                <?php echo $item->price_name; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Khách hàng</p>
                            </div>
                            <div class="info-post">
                            <?php 
                        		System::$load->view( 
                        			'includes/SearchCustomer',
 									array( 'data' => array('customers' => $data['customers'] )) 
                        		);
                        	?>
                        	</div>
                            <!--
                            <div class="info-post">
                            	<div style="">
                                    <div class="tp-search-box">
                                    	<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                            			?>" />
                                        <input required="required" class="input-search" placeholder="Tên hoặc mã số khách hàng..." />
                                        <div class="result">
                                        	
                                        	<?php
											if (isset($data['customers'])) {
												foreach ($data['customers'] as $customer) {
													$prefix = " (".
                                                        		$customer->customer_id
                                                        		. ") - ";
													if( isset( $customer->phone ) ) {
														$prefix .= $customer->phone;
													}
													?>
													<div 
                                                    	class="item-result" 
                                                    	data-name="<?php echo unicode_str_filter(
                                                    		strtolower ( 
                                                    			$customer->customer_name
                                                    			. $prefix
                                                    		)
                                                    	); ?>"
                                                        data-id="<?php echo $customer->customer_id; ?>"
                                                        data-display="<?php 
                                                        	echo $customer->customer_name . $prefix
                                                        ?>" 
                                                        data-input-name="customer_id"
                                                        >
                                                        <?php echo $customer->customer_name . $prefix ?>
                                                    </div> 
													<?php
												}
											}
											?>
                                        </div>
                                        <input name="customer_id" type="hidden" value="" />
                                    </div>
                                </div>
                            </div>
                            -->
                        </td>
                        <td class="order-right">
                        	<div>
                                <p>Chương trình KM</p>
                            </div>
                            <div class="info-post">
                                <select name="promote_id" style="width: 90%;">
                                	<option value="-1">-- Lựa chọn chương trình khuyến mại --</option> 
                                    <?php 
                                    	if( isset( $data[ 'promotes' ] ) ) {
                                    		foreach ( $data[ 'promotes' ] as $promote ) {
                                    			?>
                                    			<option value="<?php echo $promote->promote_id ?>"><?php 
                                    				echo $promote->promote_name;
                                    			?></option>
                                    			<?php
                                    		}
                                    	}
                                    ?>
                                </select>
                            </div>
                        </td>
                        <!-- 
                        <td class="order-right">
                            <div>
                                <p>Kho hàng</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_id" style="width: 90%;">
                        <?php
                        if (isset($data['warehouses'])) {
                            foreach ($data['warehouses'] as $item) {
                                ?>
                                                                                                                                                                                                                                                                <option value="<?php echo $item->warehouse_id ?>">
                                <?php echo $item->warehouse_name ?>
                                                                                                                                                                                                                                                                </option>
                                <?php
                            }
                        }
                        ?>
                                </select>
                            </div>
                        </td>
                        -->
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Địa chỉ</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="address"
                                    placeholder="Địa chỉ..." 
                                    style="width: 100%;"
                                    disabled="disabled"
                                    value="" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Trạng thái đơn hàng</p>
                            </div>
                            <div class="info-post">
                            	 Đang chờ
                            	<!-- 
                                <select name="status" style="width: 90%;">
                                    <option value="2">Đang chờ</option>
                                    <option value="3">Hoàn thành</option>
                                </select>
                                -->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p></p>
                            </div>
                            <div class="info-post">
                                <input 
                                    name="district_id"
                                    style="width:47%; padding-left: 5px; " 
                                    disabled="disabled"
                                    value="" />
                                <input 
                                    name="city_id" 
                                    style="width:30%; padding-left: 5px; "
                                    disabled="disabled"
                                    value="" />
                                <input 
                                    name="country_id" 
                                    style="width:20%; padding-left: 5px;"
                                    disabled="disabled"
                                    value="" />
                                <input 
                                    name="phone" 
                                    style="width:100%; padding-left: 5px; margin-top: 10px;"
                                    disabled="disabled"
                                    value="<?php if( isset( $data[ 'order' ]->phone ) ) echo $data[ 'order' ]->phone ?>" />
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Nhân viên bán hàng</p>
                            </div>
                            <div class="info-post">
                            	<!--
                                <select name="salesperson_id" style="width: 90%;">
                                    <?php
                                    if (isset($data['salesPeople'])) {
                                        foreach ($data['salesPeople'] as $item) {
                                            ?>
                                            <option value="<?php echo $item->salesperson_id; ?>">
                                                <?php echo $item->salesperson_name; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                -->
                                <input type="hidden" name="salesperson_id" value="<?php echo $_COOKIE['beful_user_id'] ?>" />
                                <?php 
									echo $_COOKIE['beful_user_name'];
								?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left"></td>
                        <td class="order-right">
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 10px; padding-top: 5px;">Thông tin chi tiết</h4>
                <table class="table table-bordered tbl-detail order_line">
                    <thead>
                        <tr>
                            <th>Mã sản phẩm</th>
                            <th>Tên sản phẩm</th>
                            <th>Kho hàng</th>
                            <th>Số lượng</th>
                            <th>Đơn vị</th>
                            <th>Đơn giá</th>
<!--                        <th>Tiền thuế</th>-->
                            <th>Chiếu khấu (%)</th>
                            <th>Thành tiền</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
						<tr style="height: 50px; background: #E6E7E8;">
                            <td colspan="9" style="line-height: 50px;border-right: none;">
                            	<a id="add_product" data-price="false">
				                	<h4
				                		style="	text-align: left; 
				                				margin-left: 10px;
				                				border-bottom: #000 1px solid; 
				                				padding-bottom: 3px; 
				                				margin-right: 10px; ">
				                		Thêm sản phẩm
				                	</h4>
				                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="add_product">
                    <div class="product_detail">
                    	<?php 
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        ?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="promote" style="text-align:right; margin-right:30px;"></div>
                <div class="clear"></div>
                <div class="color"></div>
                <table class="payment">
                    <tr style="font-size: 14;">
                        <td class="payment-left">Tổng tiền:</td>
                        <td class="payment-right"><span class='totalPrice'></span></td>
                    </tr>
                    <tr style="font-size: 14;">
                        <td class="payment-left" style="font-weight: normal;">Chiết khấu theo số tiền:</td>
                        <td class="payment-right">
                            <input onchange="calTotalPrice()" class="form-control" name="discount_amount" type="number" value="0" min="0" />
                        </td>
                    </tr>
                    <tr style="font-size: 14;">
                        <td class="payment-left" style="font-weight: normal;">Chiết khấu theo %:</td>
                        <input type="hidden" name="order_discount" value="" />
                        <td class="payment-right"><span class='totalDiscount'></span></td>
                    </tr>
                    <tr style="font-size: 14;">
                        <td class="payment-left">Thành tiền:</td>
                        <td class="payment-right"><span class='finalTotal'></span></td>
                    </tr>
                </table>
                <div class="clear"></div>
                <div class="product_detail_info">
                    <label>Thông tin thêm</label>
                    <br/>
                    <textarea name="product_detail_info" placeholder="Các ghi chú bổ sung"></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<br /><br />
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
        <td>
                    					<input type="hidden" class="product_id" name="product_id[]" value="" />
                    					<input type="hidden" name="order_line_id[]" value="0" />
                    					<div class="html_product_id"></div>
                    				</td>
		                            <td><div class="html_product_name"></div></td>
		                            <td>
		                            	<select name="product_warehouse[]"> 
		                            	<?php 
		                            		foreach ( $data[ 'warehouses' ] as $warehouse ) {
		                            			?>
		                            			<option 
		                            				value="<?php echo $warehouse->warehouse_id ?>"
		                            				>
		                            				<?php echo $warehouse->warehouse_name ?>
		                            			</option>
		                            			<?php
		                            		}
		                            	?>
		                            	</select>
		                            </td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_amount[]"
		                            		required="required"
                                            onchange="calTotalPriceItem(this)"
		                            		value=""
		                            	/>
		                            </td>
		                            <td><div class="html_product_unit"></div></td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_price[]"
                                            onchange="calTotalPriceItem(this)"
		                            		required="required"
		                            		value=""
		                            	/>
		                            </td>
		                            <td>
		                            	<input
		                            		style="width: 80px;"
		                            		type="number"
		                            		name="product_discount[]"
                                            onchange="calTotalPriceItem(this)"
		                            		value="0"
		                            	/>
		                            	<span class="orderLineDiscount">% =</span>
		                            </td>
		                            <td><div class='orderLinePrice'></div></td>
		                            <td>
		                            	<a class="delete_order_line" onclick="deleteOrderLine( this )"><i class="glyphicon glyphicon-trash"></i>
		                            	</a>
		                            </td>
                    			</tr>
</table>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/