<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Form Báo cáo</title>
        <style>
            body{
                padding: 0px;
                margin: auto;
                width: 100%;
            }
            header{
                padding: 0px;
                margin: auto;
                height: auto;
            }
            .logo{
                background-color: #7b287c;
                width: 30%;
                position:relative;
                text-align: center;
                float: left;
            }
            .logo img{
                width: 70%;
                height:50px;
                padding: 15px;
            }
            .line{
                height: 40px;
                padding-left: 32%;
            }
            nav{
                text-align: center;
            }
            nav>h2{
                text-transform: uppercase;
                margin-bottom: 0px;
            }
            nav>h4{
                margin: 0px;
            }
            .clear{
                clear: both;
            }
            .container{
                width: 100%;
                position: relative;
                margin-top: 20px;
            }
            .table{
                width: 100%;
                text-align: center;
                border-collapse: collapse;
            }
            .table td, .table th {
                border: 1px solid #000;
                border-spacing: 0;
                border-collapse: collapse;
            }
            .table-header{
            }
            .tbl-row{
             }
            .footer-center,.footer-left,.footer-right{
                width: 33%;
                height: 150px;
                text-align: center;
            }
            footer{
                margin-top: 20px;
            }
            .footer-left,.footer-center{
                float:left;
            }
            .footer-left{
                margin-right: 0.5%;
            }
            .footer-right{
                float:right;
            }
            .footer-right>div>h4,.footer-left>div>h4,.footer-center>div>h4{
                margin: 0px;
            }
        </style>
    </head>
    <body>
    <header>
        <div style="width: 50%; float: left;">
            <b>CÔNG TY TNHH BEFUL</b><br />
            142 Võ Văn Tần, P.6, Q.3, HCM<br />
            ĐT: (08) 3930 3990<br />
        </div>
        <div style="width: 25%; float: right;">
            Số: <?= $data['order']->order_code; ?><br />
            Ngày: <?= date("d/m/Y", $data['order']->order_date); ?><br />
        </div>
        <div class="clear"></div>
    </header>
    <nav>
        <h2>HÓA ĐƠN BÁN HÀNG</h2>
    </nav>
    <div class="container">
        Khách hàng: <b><?= $data['order']->customer_name; ?></b><br />
        Địa chỉ: <b><?= $data['order']->address; ?></b><br />
        Số điện thoại: <b><?= $data['order']->phone ?></b><br />
        Nhân viên bán hàng: <b><?= $data['order']->salesperson_name ?></b><br />
        Ghi chú: <b><?= $data['order']->order_note; ?></b><br />
        <br />
        <table class="table">
            <tr class="table-header">
                <th>STT</th>
                <th>Tên sản phẩm</th>
                <th>ĐVT</th>
                <th>SỐ LƯỢNG</th>
                <th>Đơn giá</th>
                <th>Giảm giá</th>
                <th>Thành tiền</th>
            </tr>
            <?php
                $totalOrder = 0;
                if(isset($data['order']->order_lines) && !empty($data['order']->order_lines)) {
                    $i = 0;
                    foreach($data['order']->order_lines as $line) {
                        $i++;
                        $line->totalLine = $line->product_amount * $line->product_price;
                        $line->discount = (int)($line->product_discount * $line->totalLine)/100;
                        $totalOrder += $line->totalLine = $line->totalLine - $line->discount;
                        ?>
                        <tr class="tbl-row">
                            <td><?= $i ?></td>
                            <td><?= $line->product_name; ?></td>
                            <td><?= $line->product_unit; ?></td>
                            <td><?= $line->product_amount; ?></td>
                            <td><?= $data['mMoney']->addDotToMoney($line->product_price); ?> VNĐ</td>
                            <td><?= $data['mMoney']->addDotToMoney($line->discount); ?> VNĐ</td>
                            <td style="text-align: right"><?= $data['mMoney']->addDotToMoney($line->totalLine);  ?> VNĐ</td>
                        </tr>
                        <?php
                    }
                }
            ?>
            <tr>
                <td colspan="3" style="border: none;"></td>
                <td colspan="3" style="text-align: left;">Tổng cộng</td>
                <td style="text-align: right"><?= $data['mMoney']->addDotToMoney($totalOrder); ?> VNĐ</td>
            </tr>
            <tr>
                <td colspan="3" style="border: none;"></td>
                <td colspan="3" style="text-align: left;">Giảm giá</td>
                <td style="text-align: right"><?php
                    $discountOrder = (int)($data['order']->discount * $totalOrder)/100;
                    $totalOrder = $totalOrder - $discountOrder;
                    echo $data['mMoney']->addDotToMoney($discountOrder); ?> VNĐ</td>
            </tr>
            <tr>
                <td colspan="3" style="border: none;"></td>
                <td colspan="3" style="text-align: left;">Thành tiền</td>
                <td style="text-align: right"><?= $data['mMoney']->addDotToMoney($totalOrder); ?> VNĐ</td>
            </tr>
        </table>
    </div>
    <footer>
        <br />
        <div style="height:30px; float:right;">Ngày ... Tháng ... Năm 20...</div>
        <br /><br />
        <div class="footer-left">
            <div>
                <h4>Khách hàng</h4>
                <span>(Ký và ghi rõ họ tên)</span>
            </div>
        </div>
        <div class="footer-center">
            <div>
                <h4>Người giao hàng</h4>
                <span>(Ký và ghi rõ họ tên)</span>
            </div>
        </div>
        <div class="footer-right">
            <div>
                <h4>Thủ trưởng</h4>
                <span>(Ký và ghi rõ họ tên)</span>
            </div>
        </div>
    </footer>
    </body>
</html>
<?php
/*end of file print-order.php*/
/*location: print-order.php*/