<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Top sản phẩm</h4>
            <div class="search-ban-hang non-display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
            </div> 
            <div class="nav-right">
            </div>
        </div>
    </div>
    <div class="main_new_order main_report">
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ban-hang/don-hang/bieu-do"
                		. System::$config->urlSuffix ;
                ?>" class="column-charst" style="background:#c0c0c0">
                    <img class="icon" style="width: 50px;" src="<?php
                    	echo System::$config->baseUrl . "images/Icon_Bieu-do-cot.png";
                    	  ?>"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl 
                		. "ban-hang/don-hang/bieu-do-hang"
 						. System::$config->urlSuffix;
                ?>" class="line-charts">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-duong-thang.png"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ban-hang/don-hang/bieu-do-manh"
 						. System::$config->urlSuffix;
                ?>" class="pie-charts">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-hinh-tron.png"/>
                </a>
            </div> 
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <script type="text/javascript">
            $(function () {
                $('#column').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: ''
                    },
                    series: [{
                            name: 'Population',
                            data: [
                                <?php 
                                	if( isset( $data[ 'reportSale'] ) ) {
                                		$size = sizeof( $data[ 'reportSale'] );
                                		$i = 0;
                                		foreach ( $data[ 'reportSale' ] as $item ) {
                                			?>
                                			['<?php echo $item->customer_name ?>', <?php echo $item->order_price ?>]
                                			<?php
                                			$i++;
                                			if( $i < $size ) echo ",";
                                		}
                                	};
                                ?>
                            ],
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:.1f}', // one decimal
                                y: 10, // 10 pixels down from the top
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        }]
                });
                $('#pie').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            name: "Brands",
                            colorByPoint: true,
                            data: [{
                                    name: "Sản phẩm 1",
                                    y: 54.33
                                }, {
                                    name: "Sản phẩm 2",
                                    y: 22.030000000000005,
                                    sliced: true,
                                    selected: true
                                }, {
                                    name: "Sản phẩm 3",
                                    y: 10.38
                                }, {
                                    name: "Sản phẩm 4",
                                    y: 4.77
                                }, {
                                    name: "Sản phẩm 5",
                                    y: 0.9100000000000001
                                }, {
                                    name: "Sản phẩm 6",
                                    y: 1.2
                                }, {
                                    name: "Sản phẩm 7",
                                    y: 1.2
                                }, {
                                    name: "Sản phẩm 8",
                                    y: 1.2
                                }, {
                                    name: "Sản phẩm 9",
                                    y: 1.2
                                }
                            ]
                        }]
                });
                $('#line').highcharts({
                    title: {
                        text: '',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '',
                        x: -20
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                            name: 'Sản phẩm 1',
                            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                        }, {
                            name: 'Sản phẩm 2',
                            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                        }, {
                            name: 'Sản phẩm 3',
                            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
                        }, {
                            name: 'Sản phẩm 4',
                            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                        }, {
                            name: 'Sản phẩm 5',
                            data: [3.9, 5.1, 9.7, 10.5, 15.9, 20.2, 29.0, 17.6, 8.2, 9.3, 7.6, 8.8]
                        }, {
                            name: 'Sản phẩm 6',
                            data: [9.2, 10.8, 11.7, 15.5, 20.9, 1.2, 18.0, 6.6, 1.2, 15.3, 8.6, 9.8]
                        }, {
                            name: 'Sản phẩm 7',
                            data: [7.9, 8.2, 10.7, 7.5, 6.9, 8.2, 9.3, 8.6, 16.2, 14.3, 2.6, 6.8]
                        }, {
                            name: 'Sản phẩm 8',
                            data: [5.9, 4.9, 6.7, 10.5, 15.9, 13.2, 12.6, 17.6, 15.2, 14.3, 7.6, 9.8]
                        }, {
                            name: 'Sản phẩm 9',
                            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                        }]
                });
            });
        </script>
        <div id="column" style="min-width: 300px; height: 400px; padding:0px;"></div>
        <div id="pie" class="non-display" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        <div id="line" class="non-display" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/