<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Top sản phẩm</h4>
            <div class="search-ban-hang non-display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
            </div> 
            <div class="nav-right">
            </div>
        </div>
    </div>
    <div class="main_new_order main_report">
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ban-hang/don-hang/bieu-do"
                		. System::$config->urlSuffix ;
                ?>" class="column-charst">
                    <img class="icon" style="width: 50px;" src="<?php
                    	echo System::$config->baseUrl . "images/Icon_Bieu-do-cot.png";
                    	  ?>"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl 
                		. "ban-hang/don-hang/bieu-do-hang"
 						. System::$config->urlSuffix;
                ?>" class="line-charts" style="background:#c0c0c0">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-duong-thang.png"/>
                </a>
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ban-hang/don-hang/bieu-do-manh"
 						. System::$config->urlSuffix;
                ?>" class="pie-charts">
                    <img class="icon" style="width: 50px;" src="<?php echo System::$config->baseUrl; ?>images/Icon_Bieu-do-hinh-tron.png"/>
                </a>
            </div> 
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <script type="text/javascript">
            $(function () {
                $('#line').highcharts({
                    title: {
                        text: '',
                        x: -20 //center
                    },
                    subtitle: {
                        text: '',
                        x: -20
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        <?php 
                        	if( isset( $data['reportSale'] ) ) {
                        		$size = sizeof( $data['reportSale'] );
                        		$i = 0;
                        		foreach ( $data['reportSale'] as $line ) {
                        			?>
                        			{
										name: '<?php echo $line->customer_name; ?>',
										data: [<?php 
											for( $j = 1; $j <= 12; $j++ ) {
												$is = false;
												if( isset( $line->order_list ) && !empty( $line->order_list ) ) {
													foreach ( $line->order_list as $item ) {
														if( $j == date('m', $item->order_date) ) {
															echo $item->order_price;
															$is = true;
															break;
														}
													}
													if( $is == false ) {
														echo 0;
													}
												} else  {
													echo 0;
												};
												if( $j < 12 ) echo ",";
											}
										?>]
                            		}
                        			<?php
                        			if( $i < $size )
										echo ",";                        				
                        			$i++;
                        		}
                        	}
                        ?>     
                        ]
                });
            });
        </script>  
        <div id="line" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
</div>
<?php
/*end of file ViewChartLine.php*/
/*location: ViewChartLine.php*/