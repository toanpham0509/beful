<?php
use 			BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post" class="new_order">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Đơn hàng </h4><h4>/ <?php echo System::$lang->getString("inspection") ?></h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a 
                        href="<?php
                        echo System::$config->baseUrl
                        . "ban-hang/don-hang/chinh-sua/"
 						. $data['orderId']
                        . System::$config->urlSuffix;
                        ?>"
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">
                        <?php echo System::$lang->getString("cancel") ?>
                    </a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div> 
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order">
            <div class="form-new-order" >
                  <div class="text-danger">
                  		<?php echo System::$lang->getString("error_message_inspection"); ?>
                  </div>
            </div>
            <br />
            <table class="table table-striped table-bordered tbl-main">
            	<thead>
	            	<tr>
		            	<th class="search-order">
		            		STT
		            	</th>
		            	<th>
		            		Tên sản phẩm
		            	</th>
		            	<th>
		            		Kho hàng
		            	</th>
		            	<th>
		            		Số lượng bán
		            	</th>
		            	<th>
		            		Số lượng tồn trong kho
		            	</th>
	            	</tr>
	            </thead>
	            <tbody>
	            	<?php 
	            		if(isset($data['check']) && !empty($data['check'])) {
	            			$i = 1;
	            			foreach ($data['check'] as $item) {
	            				?>
	            				<tr>
	            					<td><?php echo $i; ?></td>
	            					<td><?php echo $item->product_name; ?></td>
	            					<td><?php echo $item->warehouse_name; ?></td>
	            					<td><?php echo $item->product_amount . " " . $item->product_unit; ?></td>
	            					<td><?php echo $item->currentInventoryAmount . " " . $item->product_unit; ?></td>
	            				</tr>
	            				<?php
	            				$i++;
	            			}
	            		}
	            	?>
            	</tbody>
            </table>
        </div>
    </form>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/