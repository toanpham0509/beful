<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="clear"></div>
<div class="left-side">
    <ul>
        <li class="header-active">
        	<a href="<?php echo System::$config->baseUrl ?>sale/index.php">
        		Đơn hàng
        	</a></li>
        <li><a href="<?php echo System::$config->baseUrl ?>sale/chose_warehouse.php">Sản phẩm</a></li>
        <li><a href="<?php echo System::$config->baseUrl ?>sale/sale_off.php">Khuyến mại/Giảm giá</a></li>
        <li><a href="<?php echo System::$config->baseUrl ?>sale/quotes.php">Báo giá</a></li>
        <li><a href="<?php echo System::$config->baseUrl ?>sale/branches.php">Hệ thống cửa hàng</a></li>
    </ul>
</div>
<?php
/*end of file MenuLeft.php*/
/*location: MenuLeft.php*/