<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Sản phẩm</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php
                    echo System::$config->baseUrl;
                    ;
                    ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a 
                                        href="<?php
                echo System::$config->baseUrl
                . "ban-hang/san-pham/them-moi"
                . System::$config->urlSuffix;
                ?>">
                                        <input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/>
                                </a>
                                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-choose">
                <form class="option-content" 
                      action="<?php
                      echo System::$config->baseUrl
                      . "ban-hang/san-pham/index/"
                      . System::$config->urlSuffix;
                      ?>" 
                      method="GET">
                    <div class="choose_orderPrices">
                        <span>Chọn bảng báo giá</span>
                        <select name="price_id">
                            <?php
                            foreach ($data['orderPrices'] as $item) {
                                ?>
                                <option 
                                	value="<?php echo $item->price_id ?>"
                                	>
                                    <?php echo $item->price_name ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="choose_warehouse">
                        <span> Chọn kho hàng</span>
                        <select name="warehouse_id">
                            <?php
                            foreach ($data['warehouses'] as $item) {
                                ?>
                                <option 
                                	value="<?php echo $item->warehouse_id ?>" 
                                	>
                                    <?php echo $item->warehouse_name ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" name="btn_submit_option" class="btn btn-success">Xác nhận</button>  
                </form>
            </div>
            <div class="nav-right">
                <?php
                System::$load->view(
                    "includes/Pagination",
                    array(
                        "data" => array(
                            "pages" => $data['pages'],
                            "page" => $data['page'],
                            "dataUrl" => "ban-hang/san-pham/trang/"
                        )
                    )
                )
                ?>
            </div>
        </div>
    </div>
   	<div class="main_order">
        <table class="table table-striped table-bordered tbl-main-product tbl-main">
            <thead>
                <tr>
                	<th>STT</th>
                    <th class="search-order">
                        <label>Mã sản phẩm 
                            <a id="product_id" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_id" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Tên sản phẩm
                            <a id="product_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Mã vạch
                            <a id="product_qrcode" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_qrcode" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Đơn vị tính
                            <a id="product_unit" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_unit" class="search-detail search-infor non-display"/>
                    </th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['products'])) {
                    $i = 0;
                    foreach ($data['products'] as $product) {
                    	$i++;
                        ?>
                        <tr>
                        	<td><?php echo $i; ?></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/san-pham/chinh-sua/"
                                . $product->product_id
                                . System::$config->urlSuffix;
                                ?>"><?php echo $product->product_code; ?></a></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/san-pham/chinh-sua/"
                                . $product->product_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa sản phẩm">
                                       <?php echo $product->product_name; ?>
                                </a></td>
                            <td><?php echo $product->barcode; ?></td>
                            <td><?php echo $product->unit_name; ?></td>
                            <td style="min-width: 80px;">
                                <a href="<?= BASE_URL ?>ban-hang/san-pham/xem/<?= $product->product_id; ?>.bf">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                                <a href="<?= BASE_URL ?>ban-hang/san-pham/chinh-sua/<?= $product->product_id; ?>.bf">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                                <a onclick="return confirm('Bạn có chắc chắn muốn xóa sản phẩm này không?')"
                                   href="<?= BASE_URL ?>ban-hang/san-pham/xoa/<?= $product->product_id; ?>.bf" title="">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ChooseWarehousePrice.php*/
/*location: ChooseWarehousePrice.php*/