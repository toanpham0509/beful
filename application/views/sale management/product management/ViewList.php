<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Sản phẩm</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="product_id icon-search non-display">Mã sản phẩm 
                        <a href="#" id="product_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="product_name icon-search non-display">Tên sản phẩm
                        <a href="#" id="product_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="product_description icon-search non-display">Mô tả sản phẩm
                        <a href="#" id="product_description" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="product_qrcode icon-search non-display">Mã vạch 
                        <a href="#" id="product_qrcode" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="product_unit icon-search non-display">Đơn vị tính 
                        <a href="#" id="product_unit" class="glyphicon glyphicon-remove img-circle"></a>
                    </span> 
                    <span class="product_inventory icon-search non-display">Tồn kho 
                        <a href="#" id="product_inventory" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="product_price icon-search non-display">Giá bán
                        <a href="#" id="product_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a 
                    href="<?php
                    echo System::$config->baseUrl
                    . "ban-hang/san-pham/them-moi"
                    . System::$config->urlSuffix;
                    ?>">
                    <input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-choose">
                <form class="option-content" 
                      action="<?php
                      echo System::$config->baseUrl
                      . "ban-hang/san-pham/index/"
                      . System::$config->urlSuffix;
                      ?>" 
                      method="GET">
                    <div class="choose_orderPrices" style="margin-left: 0;">
                        <span>Chọn bảng báo giá</span>
                        <select name="price_id">
                            <?php
                            foreach ($data['orderPrices'] as $item) {
                                ?>
                                <option 
                                	value="<?php echo $item->price_id ?>"
                                	<?php  
                                		if( $data['price_id'] == $item->price_id )
                                			echo "selected='selected'";
                                	?>
                                	>
                                    <?php echo $item->price_name ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="choose_warehouse">
                        <span>Chọn kho hàng</span>
                        <select name="warehouse_id">
                            <?php
                            foreach ($data['warehouses'] as $item) {
                                ?>
                                <option 
                                	value="<?php echo $item->warehouse_id ?>" 
                                	<?php  
                                		if( $data['warehouse_id'] == $item->warehouse_id )
                                			echo "selected='selected'";
                                	?>
                                	>
                                    <?php echo $item->warehouse_name ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <button type="submit" name="btn_submit_option" class="btn btn-success">Xác nhận</button>  
                </form>
            </div>
            <div class="nav-right">
                <?php
                System::$load->view(
                    "includes/Pagination",
                    array(
                        "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ban-hang/san-pham/index/"
                        . $data['price_id'] . "/"
                        . $data['warehouse_id'] . "/"
                    )
                        )
                )
                ?>
                <a class="icon_product list-item" id="gird"><i class="glyphicon glyphicon-th-large"></i></a>
                <a class="icon_product" id="list"><i class="glyphicon glyphicon-align-justify"></i></a>
            </div>
        </div>
    </div>
    <div class="main_order product_grid">
        <table>
            <?php
            if (isset($data['products'])) {
                $i = 0;
                foreach ($data['products'] as $product) {
                    if ($i % 3 == 0)
                        echo "<tr>";
                    ?>
                    <td>
                        <div class="product_info_left">
                            <a href="<?php
                            echo System::$config->baseUrl
                            . "ban-hang/san-pham/chinh-sua/"
                            . $product->product_id
                            . System::$config->urlSuffix;
                            ?>" title="Chỉnh sửa sản phẩm">
                                <img src="<?php echo System::$config->baseUrl . $product->product_thumbnail; ?>"
                                	class="img-responsive" style="height: 150px;"
                                	alt="<?php echo $product->product_name ?>"
                                />
                            </a>
                        </div>
                        <div class="product_info_right">
                            <h4 style="color: green">
                                <a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/san-pham/chinh-sua/"
                                . $product->product_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa sản phẩm">
                                       <?php echo $product->product_name; ?>
                                </a>
                            </h4>
                            <span>Mã SP</span>: <?php echo $product->product_code; ?><br />
                            <span>Giá:</span>: <?php echo $product->product_price; ?> <br />
                            <span>Đang có:</span> <?php echo $product->inventory_amount . " " . $product->unit_name ?>
                        </div>
                    </td>
                    <?php
                    if ($i % 3 == 2) {
                        echo "</tr>";
                    }
                    $i++;
                }
            }
            ?>
        </table>
    </div>
    <div class="main_order product_list">
        <table class="table table-striped table-bordered tbl-main-product tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>Mã sản phẩm 
                            <br/>
                            <a id="product_id" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_id" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Tên sản phẩm
                            <br/>
                            <a id="product_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Mô tả sản phẩm
                            <br/>
                            <a id="product_description" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_description" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Mã vạch
                            <br/>
                            <a id="product_qrcode" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_qrcode" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Đơn vị tính
                            <br/>
                            <a id="product_unit" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_unit" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Tồn kho
                            <br/>
                            <a id="product_inventory" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_inventory" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Giá bán
                            <br/>
                            <a id="product_price" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="product_price" class="search-detail search-infor non-display"/>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['products'])) {
                    $i = 0;
                    foreach ($data['products'] as $product) {
                        ?>
                        <tr>
                            <td><a href=""><?php echo $product->product_code; ?></a></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/san-pham/chinh-sua/"
                                . $product->product_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa sản phẩm">
                                       <?php echo $product->product_name; ?>
                                </a></td>
                            <td><?php echo theExcerpt($product->product_description, 200) ?></td>
                            <td><?php echo $product->barcode; ?></td>
                            <td><?php echo $product->unit_name; ?></td>
                            <td><?php echo $product->inventory_amount ?></td>
                            <td><?php echo $product->product_price ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/