<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Sản phẩm</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Lưu sản phẩm"
                        name="save" />
                    <a 	href="<?php
	                    echo System::$config->baseUrl
	                    . "ban-hang/san-pham"
	                    . System::$config->urlSuffix;
                    ?>" 
                        class="btn btn-warning" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
                        <?php
                        if (isset($data['message']))
                            echo "<br />" . $data['message'] . "<br />";
                        ?>
                    </div>
                </div>
                <div class="nav-right non-display">
                    <a class="icon_product list-item" id="gird"><i class="glyphicon glyphicon-th-large"></i></a>
                    <a class="icon_product" id="list"><i class="glyphicon glyphicon-align-justify"></i></a>
                </div>
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product">
                <div class="product_info">
                    <div>
                        <label>Tên sản phẩm</label>
                        <br/>
                        <input 
                            type="text" 
                            name="product_name" 
                            placeholder="Tên sản phẩm..."
                            required="required"
                            value="<?php if (isset($data['dataSubmit']['product_name'])) echo $data['dataSubmit']['product_name']; ?>"
                            /> 
						<b style="margin-left: 50px">Mã sản phẩm</b>
                        <input 
                            type="text" 
                            name="product_code" 
                            disabled="disabled"
                            placeholder="Mã sản phẩm..."
                            value="<?php 
                            	if( isset( $data[ 'dataSubmit' ][ 'productCode' ] ) ) echo $data[ 'dataSubmit' ][ 'productCode' ]; 
                            ?>"
                            />
                        <br/><br />
                        <label>Ảnh sản phẩm</label>
                        <input 
                            type="file" 
                            name="product_thumbnail" 
                            value=""
                            /> 
                        <br/>
                        <label>Nhóm sản phẩm</label>
                        <br/>
                        <div class="row">
                            <?php
                            foreach ($data['productCategories'] as $category) {
                                ?>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <input type="checkbox" name="product_categories[]" value="<?php
                                    echo $category->product_category_id;
                                    ?>" /> <?php echo $category->product_category_name; ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="product_information">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#product_infomation" data-toggle="tab">Thông tin</a></li>
                        <li><a href="#product_warehouse" data-toggle="tab">Tồn kho</a></li>
                        <li><a href="#product_sale" data-toggle="tab">Bán hàng</a></li>
                    </ul>
                    <div class="tab-content">
                        <!--Tab Thông Tin-->
                        <div class="tab-pane active" id="product_infomation"> 
                            <table>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Đơn vị tính</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <select name="product_unit_id">
                                                <?php
                                                foreach ($data['productUnits'] as $unit) {
                                                    ?>
                                                    <option value="<?php echo $unit->unit_id ?>">
                                                        <?php echo $unit->unit_name ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="tbl-right">
                                        <div class="tbl-left-new-product">
                                            <label>Mã sản phẩm</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="product_code" disabled="disabled"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Cảnh báo hạn sử dụng trước</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="expired_notification"/> Ngày
                                        </div>
                                        <!--  
                                        <div class="tbl-left-new-product">
                                            <label>Đơn vị tính tính mua sắm</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <a href="main_new_order.php"></a>
                                            <select name="unit_sale">
                                                <option>Đơn vị tính 1</option>
                                                <option>Đơn vị tính 2</option>
                                                <option>Đơn vị tính 3</option>
                                                <option>Đơn vị tính 4</option>
                                                <option>Đơn vị tính 5</option>
                                            </select>
                                        </div>
                                        -->
                                    </td>
                                    <td class="tbl-right">
                                        <div class="tbl-left-new-product">
                                            <label>Mã vạch</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="barcode"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                    </td>
                                    <td class="tbl-right">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                    </td>
                                    <td class="tbl-right"></td>
                                </tr>
                            </table>
                            <div class="product_description">
                                <label>Mô tả sản phẩm</label>
                                <br/>
                                <textarea name="product_description" placeholder="Mô tả sản phẩm"></textarea>
                            </div>
                        </div>
                        <!--Tab Nhập Kho-->
                        <div class="tab-pane" id="product_warehouse">
                            <table>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Tồn kho tối đa</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="number" name="inventory_max"/>
                                        </div>
                                    </td>
                                    <td class="tbl-right">
                                        <div class="tbl-left-new-product">
                                            <label>Tình trạng sản phẩm</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <select name="product_status_id">
                                                <?php
                                                foreach ($data['productStatus'] as $item) {
                                                    ?>
                                                    <option value="<?php echo $item->status_id ?>">
                                                        <?php echo $item->status_name ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <!--  
                                            <div class="tbl-left-new-product">
                                                <label>Quản lý sản phẩm</label>
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="product_manager">
                                            <?php
                                            print_r($data['salesPeople']);
                                            foreach ($data['salesPeople'] as $item) {
                                                ?>
                                                                                        <option value="<?php echo $item->salesperson_id ?>">
                                                <?php echo $item->salesperson_name ?>
                                                                                        </option>
                                                <?php
                                            }
                                            ?>
                                                </select>
                                            </div>
                                            -->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Tồn kho tối thiểu</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="number" name="inventory_min" />
                                        </div>
                                    </td>
                                    <td class="tbl-right">
                                        <!--                                        <div class="tbl-left-new-product">
                                                                                    <label>Tình trạng sản phẩm</label>
                                                                                </div>
                                                                                <div class="tbl-right-new-product">
                                                                                    <select name="product_status_id">
                                        <?php
                                        foreach ($data['productStatus'] as $item) {
                                            ?>
                                                                                                        <option value="<?php echo $item->status_id ?>">
                                            <?php echo $item->status_name ?>
                                                                                                        </option>
                                            <?php
                                        }
                                        ?>
                                                                                    </select>-->
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                    </td>
                                    <td class="tbl-right">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                    </td>
                                    <td class="tbl-right">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--Tab Bán Hàng-->
                        <div class="tab-pane" id="product_sale">
                            <table>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Bảo hành</label> 
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="guarantee"/> Tháng
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Thời gian chuyển hàng</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="shipping_time"/> Ngày
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="tbl-left">
                                        <div class="tbl-left-new-product">
                                            <label>Chính sách đổi hàng</label>
                                        </div>
                                        <div class="tbl-right-new-product">
                                            <input type="text" name="changing_policy"/> Ngày
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane" id="product_accountant">
                            <!--
                                <table>
                                    <tr>
                                        <td class="tbl-left">
                                            <div class="tbl-left-new-product">
                                                <label>Tài khoản thu nhập</label> 
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="income_account">
                                                    <option>Tài khoản thu nhập 1</option>
                                                    <option>Tài khoản thu nhập 2</option>
                                                    <option>Tài khoản thu nhập 3</option>
                                                    <option>Tài khoản thu nhập 4</option>
                                                    <option>Tài khoản thu nhập 5</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td class="tbl-right">
                                            <div class="tbl-left-new-product">
                                                <label>Tài khoản chi phí</label> 
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="costs_account">
                                                    <option>Tài khoản chi phí 1</option>
                                                    <option>Tài khoản chi phí 2</option>
                                                    <option>Tài khoản chi phí 3</option>
                                                    <option>Tài khoản chi phí 4</option>
                                                    <option>Tài khoản chi phí 5</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbl-left">
                                            <div class="tbl-left-new-product">
                                                <label>Tài khoản KH trả lại</label> 
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="customer_return_account">
                                                    <option>Tài khoản KH trả lại 1</option>
                                                    <option>Tài khoản KH trả lại 2</option>
                                                    <option>Tài khoản KH trả lại 3</option>
                                                    <option>Tài khoản KH trả lại 4</option>
                                                    <option>Tài khoản KH trả lại 5</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td class="tbl-right">
                                            <div class="tbl-left-new-product">
                                                <label>Thuế mua hàng</label> 
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="purchase_tax">
                                                    <option>Thuế mua hàng 1</option>
                                                    <option>Thuế mua hàng 2</option>
                                                    <option>Thuế mua hàng 3</option>
                                                    <option>Thuế mua hàng 4</option>
                                                    <option>Thuế mua hàng 5</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tbl-left">
                                            <div class="tbl-left-new-product">
                                                <label>Thuế bán hàng</label> 
                                            </div>
                                            <div class="tbl-right-new-product">
                                                <select name="sales_tax">
                                                    <option>Thuế bán hàng 1</option>
                                                    <option>Thuế bán hàng 2</option>
                                                    <option>Thuế bán hàng 3</option>
                                                    <option>Thuế bán hàng 4</option>
                                                    <option>Thuế bán hàng 5</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td class="tbl-right"></td>
                                    </tr>
                                </table>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/