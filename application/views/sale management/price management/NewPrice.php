<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Tạo mới báo giá</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        name="save" 
                        class="btn btn-success" 
                        style="border-radius: 7px; margin-right: 5px; color: white;" 
                        value="Lưu"/>
                    <a href="<?php
                    echo System::$config->baseUrl
                    . "ban-hang/bao-gia"
                    . System::$config->urlSuffix;
                    ?>">
                    	<input type="button" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;" value="Bỏ qua"/>
                    </a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div> 
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product new_sale_off">
                <div class="quotes_info product_info">
                    <table>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                                <div class="tbl-left-quotes">
                                    <label>Tên bảng giá</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="text" 
                                        name="price_name" 
                                        required="required"
                                        placeholder="Tên bảng giá bán..."
                                        value="<?php
                                        if (isset($data['price']->price_name))
                                            echo $data['price']->price_name;
                                        ?>"
                                        />
                                </div>
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Thời gian bắt đầu</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="date" 
                                        name="time_start"
                                        required="required"
                                        value="<?php
                                            echo date("Y-m-d");
                                        ?>"
                                        />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Thời gian kết thúc</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <input 
                                        type="date" 
                                        name="time_end"
                                        required="required"
                                        value="<?php
                                            echo date("Y-m-d");
                                        ?>"
                                        />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbl-left quotes_info_left">
                                <div class="tbl-left-quotes">
                                    <label>Mã bảng giá</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <?php
                                    if (isset($data['priceID']))
                                        echo $data['priceID'];
                                    ?>
                                </div>
                            </td>
                            <td class="tbl-right quotes_info_right">
                                <div class="tbl-left-quotes">
                                    <label>Hiệu lực</label>
                                </div>
                                <div class="tbl-right-quotes">
                                    <div class="checkbox_wrapper">
                                        <input 
                                            name="validity" 
                                            type="checkbox"
                                            checked="checked"
                                            value="1" />
                                        <label></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="quotes_detail">
                    <table class="table table-bordered tbl-sale-off-product tbl-quotes order_line">
                            <tr style="background: #e6e7e8;">
                            	<th>Mã sản phẩm</th>
                                <th>Sản phẩm</th>
                                <th>Giá bán</th>
                                <th>Ghi chú</th>
                                <th></th>
                            </tr>
							<tr style="height: 50px; background: #E6E7E8;">
	                            <td colspan="9" style="line-height: 50px;border-right: none;">
	                            	<a id="add_product">
					                	<h4 style="	text-align: left; 
					                				margin-left: 10px;
					                				border-bottom: #000 1px solid; 
					                				padding-bottom: 3px; 
					                				margin-right: 10px; ">
					                		Thêm sản phẩm
					                	</h4>
					                </a>
	                            </td>
	                        </tr>
                    </table>
                    <div class="add_product">
	                    <div class="product_detail">
	                        <?php 
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        	?>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
		<td>
			<input type="hidden" class="product_id" name="product_id[]" value="" />
			<div class="html_product_id"></div>
		</td>
		<td><div class="html_product_name"></div></td>
		<!-- 
		<td>
			<select required="required" name="product_id[]">
				<?php 
					if( isset( $data[ 'products' ] ) )
					foreach ( $data[ 'products' ] as $product ) {
						?>
						<option value="<?php echo $product->product_id ?>"><?php 
							echo $product->product_name;
						?></option>
						<?php 
					}
				?>
			</select>
		</td>
		 -->
		<td>
			 <input type="text" required="required" placeholder="Giá bán..." class='input_table' name="product_price[]"/>
		</td>
		<td>
			<input type="text" placeholder="Ghi chú..." class='input_table' name="product_note[]"/>
		</td>
		<td><a class="delete_order_line" onclick="deleteOrderLine( this )">
			<i class="glyphicon glyphicon-trash"></i>
		</a></td>
	</tr>
</table>
</div>
<?php
/*end of file NewPrice.php*/
/*location: NewPrice.php*/