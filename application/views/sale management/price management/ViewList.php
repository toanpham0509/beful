<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Báo giá</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="price_name icon-search non-display">Tên báo giá 
                        <a href="#" id="price_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="start_date icon-search non-display">Ngày bắt đầu 
                        <a href="#" id="start_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="end_date icon-search non-display">Ngày kết thúc 
                        <a href="#" id="end_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "ban-hang/bao-gia/them-moi"
                . System::$config->urlSuffix;
                ?>"><input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ban-hang/bao-gia/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        <label>Tên báo giá 
                            <a id="price_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="price_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày bắt đầu 
                            <a id="start_date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
            <div class="search-infor date_1 non-display">
                <input type="date" name="start_date"/>
            </div>
            </th>
            <th class="search-order">
                <label> Ngày kết thúc 
                    <a id="end_date" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
            <div class="search-infor date_2 non-display">
                <input type="date" name="end_date"/>
            </div>
            </th>
            <th></th>
            </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data['prices'])) {
                    $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                    foreach ($data['prices'] as $price) {
                        $data['startList'] ++;
                        ?>
                        <tr>
                            <td><?php echo $data['startList']; ?></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/bao-gia/chinh-sua/"
                                . $price->price_id
                                . System::$config->urlSuffix;
                                ?>" title="Chỉnh sửa báo giá"><?php echo $price->price_name ?></a></td>
                            <td>
        <?php if ($price->time_start > 0) echo date("d/m/Y", $price->time_start); ?>
                            </td>
                            <td>
        <?php if ($price->time_end > 0) echo date("d/m/Y", $price->time_end); ?>
                            </td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "ban-hang/bao-gia/xoa/"
                                . $price->price_id
                                . System::$config->urlSuffix;
                                ?>" onclick="return confirm('Bạn có chắc chắn muốn xóa bảng giá này không?')">
                                    <i class="glyphicon glyphicon-trash"/></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/