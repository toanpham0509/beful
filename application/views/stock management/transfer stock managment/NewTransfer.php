<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
        	<div class="line">
                <h4 style="color: #2DAE4A;">Chuyển kho</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "mua-hang/chuyen-kho"
 							. System::$config->urlSuffix; 
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
        </div>
    </form>
    <div class="" style="margin-left: 20px;">
    	<br />
    	<h4>Quá trình nhập kho gồm 2 bước, xuất từ kho A, nhập vào kho B. Để chuyển kho, hay thực hiện các bước sau:</h4>
    	<b>B1</b>. Vào <b>Bán hàng</b> => <b>Đơn hàng</b> => <b>Tạo mới</b>
    	<br />Chọn <b>Khách hàng</b> là <b>Chuyển kho</b>
    	<br /><br />
    	<b>B2</b>. Vào <b>Mua hàng</b> => <b>Nhập hàng</b> => <b>Tạo mới</b>
    	<br />
    	Chọn <b>Nhà cung cấp</b> là <b>Chuyển kho</b>
    	<br /><br />
    	Hoàn tất chuyển kho.
    </div>
</div>
<?php
/*end of file NewTransfer.php*/
/*location: NewTransfer.php*/