<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Chuyển kho</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                        type="submit" 
                        class="btn btn-success" 
                        style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                        value="Lưu" 
                        name="save" />
                    <a href="<?php
                    echo System::$config->baseUrl
                    . "mua-hang/chuyen-kho"
                    . System::$config->urlSuffix;
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
                        <?php
                        if (isset($data['message']))
                            echo "<br />" . $data['message'] . "<br />";
                        ?>
                    </div>
                </div>
                <div class="nav-center">
                    <a href="" class="btn btn-default" style="color:#000; border-radius: 7px; margin-right: 5px;">In phiếu nhập</a>
                    <a href="" class="btn btn-default" style="color:#000; border-radius: 7px; margin-right: 5px;">Xác nhận</a>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
            <div class="form-new-order form-buy-new-order" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Số phiếu nhập</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="text" 
                                    name="transfer_code" 
                                    style="width: 100%;" 
                                    disabled="disabled"
                                    value="<?php
                                    echo $data['transfer']->transfer_id;
                                    ?>">
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tháng</p>
                            </div>
                            <div class="info-post">
                                <input 
                                    type="date" 
                                    name="import_date" 
                                    style="width: 80%;"
                                    disabled="disabled"
                                    <?php
                                    if ($data['transfer']->transfer_date > 0) {
                                        echo date("Y-m-d", $data['transfer']->transfer_date);
                                    }
                                    ?>
                                    />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Kho hàng đi</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_from_id" style="width: 100%;">
                                    <?php
                                    foreach ($data['warehouses'] as $warehouse) {
                                        ?>
                                        <option
                                            value="<?php echo $warehouse->warehouse_id ?>"
                                            <?php
                                            if ($warehouse->warehouse_id == $data['transfer']->warehouse_from_id)
                                                echo "selected='selected'";
                                            ?>>
                                                <?php echo $warehouse->warehouse_name; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Kho hàng đến</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_to_id" style="width: 80%;">
                                    <?php
                                    foreach ($data['warehouses'] as $warehouse) {
                                        ?>
                                        <option
                                            value="<?php echo $warehouse->warehouse_id ?>"
                                            <?php
                                            if ($warehouse->warehouse_id == $data['transfer']->warehouse_to_id)
                                                echo "selected='selected'";
                                            ?>>
                                                <?php echo $warehouse->warehouse_name; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                <table class="table tbl-order-detail order_line">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 15%">Đơn giá</th>
                            <th style="width: 15%">Tổng tiền</th>
                            <th style="width: 5%; border-right: none"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="height: 50px; background: #E6E7E8;">
                            <td colspan="9" style="line-height: 50px;border-right: none;">
                                <a id="add_product">
                                    <h4 
                                        style="	text-align: left; 
                                        margin-left: 10px;
                                        border-bottom: #000 1px solid; 
                                        padding-bottom: 3px; 
                                        margin-right: 10px; ">
                                        Thêm sản phẩm
                                    </h4>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="add_product">
                    <div class="product_detail">
                        <span>Mã sản phẩm</span>
                        <input type="text" id="addProduct_ProductID" name="addProduct_ProductID" />
                        <span>Tên sản phẩm</span>
                        <input type="text" id="addProduct_Name" name="addProduct_Name" />
                        <!--  
                        <span style="margin-left: 30px;">Số lượng</span>
                        <input type="number" id="addProduct_ProductAmount" name="addProduct_ProductAmount"/>
                        -->
                        <a style="margin-left: 30px;">
                            <input 
                                type="button" 
                                id="btnAddProductToOrder" 
                                class="btn btn-success" 
                                value="Thêm sản phẩm"/>
                        </a>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="product_detail_info product_detail_info_import">
                    <br/>
                    <textarea name="transfer_note" placeholder="Các ghi chú bổ sung"><?php
                        echo $data['transfer']->transfer_note;
                        ?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
    <table>
        <tr>
            <td style="width: 10%; border-left: none;">
                <input type="hidden" name="order_line_id[]" value="0" />
                <input 
                    style="width: 100%" 
                    name="product_id[]" 
                    type="hidden"
                    class="product_id"
                    value="" />
                <div class="html_product_id"></div>
            </td>
            <td style="width: 15%">
                <div class="html_product_name"></div>
            </td>
            <td style="width: 10%">
                <input 
                    style="width: 80%" 
                    type="number" 
                    name="product_amount[]"
                    value=""
                    required="required" />
            </td>
            <td style="width: 10%">
                <div class="html_product_unit"></div>
            </td>
            <td style="width: 10%">
                <input style="width: 80%" type="text" name="product_price[]" required="required"/>
            </td>
            <td style="width: 15%;">
                <input 
                    style="width: 90%" 
                    type="hidden" 
                    name="product_total_price[]" />
            </td>
            <td style="width: 5%;  border-right: none">
                <a class="delete_order_line" onclick="deleteOrderLine(this)"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
    </table>
</div>
<?php
/*end of file EditTransfer.php*/
/*location: EditTransfer.php*/