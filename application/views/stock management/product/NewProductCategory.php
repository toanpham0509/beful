<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Nhóm sản phẩm</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;" 
                    	value="Lưu" 
                    	name="save" />
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "ql-kho/nhom-san-pham"
                    		. System::$config->urlSuffix; 
                    ?>">
                    	<input type="button"  class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;" value="Bỏ qua" />
                    </a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 587px;">
            <div class="form-new-order form-buy-new-order " >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Tên nhóm sản phẩm</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="category_name" 
                                	style="width: 100%;"
                                	required="required" 
                                	/>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tạo</p>
                            </div>
                            <div class="info-post">
                                <?php echo date( "d/m/Y" ) ?>
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="form-new-order form-buy-new-order " style="border-bottom: none;">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">Định khoản kế toán</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Tài khoản doanh thu</p>
                            </div>
                            <div class="info-post">
                                <select name="turnover_account" style="width: 80%;">
                                    <option>Tài khoản doanh thu 1</option>
                                    <option>Tài khoản doanh thu 2</option>
                                    <option>Tài khoản doanh thu 3</option>
                                    <option>Tài khoản doanh thu 4</option>
                                    <option>Tài khoản doanh thu 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>

                            </div>
                            <div class="info-post">

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Tài khoản KH trả lại</p>
                            </div>
                            <div class="info-post">
                                <select name="return_customer_account" style="width: 80%;">
                                    <option>Tài khoản KH trả lại 1</option>
                                    <option>Tài khoản KH trả lại 2</option>
                                    <option>Tài khoản KH trả lại 3</option>
                                    <option>Tài khoản KH trả lại 4</option>
                                    <option>Tài khoản KH trả lại 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                            </div>
                            <div class="info-post">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Tài khoản chi phí</p>
                            </div>
                            <div class="info-post">
                                <select name="cost_account" style="width: 80%;">
                                    <option>Tài khoản chi phí 1</option>
                                    <option>Tài khoản chi phí 2</option>
                                    <option>Tài khoản chi phí 3</option>
                                    <option>Tài khoản chi phí 4</option>
                                    <option>Tài khoản chi phí 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                            </div>
                            <div class="info-post">
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="form-new-order form-buy-new-order " style="border-bottom: none;">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">Định khoản hàng hóa</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>TK hàng nhập kho</p>
                            </div>
                            <div class="info-post">
                                <select name="product_import_account" style="width: 80%;">
                                    <option>TK hàng nhập kho 1</option>
                                    <option>TK hàng nhập kho 2</option>
                                    <option>TK hàng nhập kho 3</option>
                                    <option>TK hàng nhập kho 4</option>
                                    <option>TK hàng nhập kho 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>TK kiểm kê thiếu</p>
                            </div>
                            <div class="info-post">
                                <select name="account_inventory_short" style="width: 80%;">
                                    <option>TK kiểm kê thiếu 1</option>
                                    <option>TK kiểm kê thiếu 2</option>
                                    <option>TK kiểm kê thiếu 3</option>
                                    <option>TK kiểm kê thiếu 4</option>
                                    <option>TK kiểm kê thiếu 5</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>TK hàng xuất kho</p>
                            </div>
                            <div class="info-post">
                                <select name="product_export_account" style="width: 80%;">
                                    <option>TK hàng xuất kho 1</option>
                                    <option>TK hàng xuất kho 2</option>
                                    <option>TK hàng xuất kho 3</option>
                                    <option>TK hàng xuất kho 4</option>
                                    <option>TK hàng xuất kho 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>TK kiểm kê thừa</p>
                            </div>
                            <div class="info-post">
                                <select name="account_inventory_excess" style="width: 80%;">
                                    <option>TK kiểm kê thiếu 1</option>
                                    <option>TK kiểm kê thiếu 2</option>
                                    <option>TK kiểm kê thiếu 3</option>
                                    <option>TK kiểm kê thiếu 4</option>
                                    <option>TK kiểm kê thiếu 5</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>TK định giá hàng tồn kho</p>
                            </div>
                            <div class="info-post">
                                <select name="inventory_product_accont" style="width: 80%;">
                                    <option>TK định giá hàng tồn kho 1</option>
                                    <option>TK định giá hàng tồn kho 2</option>
                                    <option>TK định giá hàng tồn kho 3</option>
                                    <option>TK định giá hàng tồn kho 4</option>
                                    <option>TK định giá hàng tồn kho 5</option>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                                <p>Số nhập kí kho</p>
                            </div>
                            <div class="info-post">
                                <select name="import_warehouse_number" style="width: 80%;">
                                    <option>Số nhập kí kho 1</option>
                                    <option>Số nhập kí kho 2</option>
                                    <option>Số nhập kí kho 3</option>
                                    <option>Số nhập kí kho 4</option>
                                    <option>Số nhập kí kho 5</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                </table>  
            </div>
        </div>
    </form>
</div>
<?php
/*end of file NewProductCategory.php*/
/*location: NewProductCategory.php*/