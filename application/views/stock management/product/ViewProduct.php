<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Danh sách sản phẩm</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="import_id icon-search non-display">Số phiếu nhập 
                        <a href="#" id="import_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="customer_name icon-search non-display">Tên khách hàng 
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="cost_non_tax icon-search non-display">Giá chưa thuế 
                        <a href="#" id="cost_non_tax" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng tiền 
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_number_buy icon-search non-display">Số đơn hàng mua 
                        <a href="#" id="order_number_buy" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>

                    <span class="warehouse icon-search non-display">Kho hàng 
                        <a href="#" id="warehouse" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
            <!--  
                <a href="<?php
                echo System::$config->baseUrl
                . "ban-hang/san-pham/them-moi"
                . System::$config->urlSuffix;
                ?>"><button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            -->
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ql-kho/san-pham/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        Mã sản phẩm
            		</th>
		            <th class="search-order">
		               Tên sản phẩm
		            </th>
		            <th class="search-order">
		            	Đơn vị tính
		            </th>
		            <th class="search-order">
		                Barcode
		            </th>
		            <th></th>
            	</tr>
            </thead>
            <tbody>
                <?php
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['products'] as $product ) {
                    $data['startList'] ++;
                    ?>
                    <tr>
                        <td>
                            <?php echo $data['startList'] ?>
                        </td>
                        <td><?php
                                echo $product->product_code;
                            ?></td>
                        <td><a href="<?php
                            echo System::$config->baseUrl
                            . "ban-hang/san-pham/chinh-sua/"
                            . $product->product_id
                            . System::$config->urlSuffix;
                            ?>" title=""><?php
                                   echo $product->product_name;
                                   ?></a></td>
                        <td><?php
                            echo $product->unit_name;
                            ?></td>
                        <td><?php 
                        	echo $product->barcode;
                        ?></td>
                        <td><a
                        onclick="return confirm('Bạn có chắc chắn muốn xóa sản phẩm này không?')" 
                        href="<?php
                            echo System::$config->baseUrl
                            . "ban-hang/san-pham/xoa/"
                            . $product->product_id
                            . System::$config->urlSuffix;
                            ?>" title="">
                                  <i class="glyphicon glyphicon-trash"></i>
                        </a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewProduct.php*/
/*location: ViewProduct.php*/