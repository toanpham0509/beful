<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Nhóm sản phẩm</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;" 
                    	value="Lưu"
                    	name="save" />
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "ql-kho/don-vi-san-pham"
 							. System::$config->urlSuffix; 
                    ?>">
                    	<input type="button"  class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;" value="Bỏ qua"/>
                    </a>
                    <div>
		                <?php
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 587px;">
            <div class="form-new-order form-buy-new-order " style="border-bottom: none;" >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px; padding-left: 40px;">
                            <div>
                                <p>Đơn vị tính</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="unit_name" 
                                	style="width: 100%;"
                                	required="required" />
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>                      
                            </div>
                            <div class="info-post">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left" style="padding-left: 40px;">
                            <div>
                                <p>Có hiệu lực</p>
                            </div>
                            <div class="info-post">
                                <div class="checkbox_wrapper">
                                    <input type="checkbox" name="validity" value="1" />
                                    <label></label>
                                </div>
                            </div>
                        </td>
                        <td class="order-right">
                            <div>
                            </div>
                            <div class="info-post">
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="padding-left: 40px; text-align: left;">
                    <label>Lưu ý:</label>
                    <br/>
                    <br/>
                    <p>
                        Một số đơn vị chuẩn và thường được sử dụng, ví dụ:
                        <br/>
                        <br/>
                        -Đơn vị: Đơn vị
                        <br/>
                        <br/>
                        -Khối lượng: Kg
                        <br/>
                        <br/>
                        -Thời gian: Ngày
                        <br/>
                        <br/>
                        -Chiều dài: Mét
                        <br/>
                        <br/>
                        -Thể tích: Lít
                        <br/>
                        <br/>
                        -...
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file NewUnit.php*/
/*location: NewUnit.php*/