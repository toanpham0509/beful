<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Nhóm sản phẩm</h4>
            <div class="search-ban-hang display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ql-kho/nhom-san-pham/them-moi"
 						. System::$config->urlSuffix; 
                ?>">
                	<button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Nhóm sản phẩm</th>
                    <th>Ghi chú</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                	if( isset( $data[ 'productCategories' ] )
                		&& !empty( $data[ 'productCategories' ] ) ) :
                		$i = 0;
                		foreach ( $data[ 'productCategories' ] as $category ) {
                			$i++; 
                			?>
                			<tr>
			                    <td>
			                        <?php echo $i; ?>
			                    </td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ql-kho/nhom-san-pham/chinh-sua/"
 										. $category->product_category_id
 										. System::$config->urlSuffix;
			                    ?>" title=""><?php 
			                    	echo $category->product_category_name;
			                    ?></a></td>
			                    <td></td>
			                    <td><a 
			                    		class="delete_order_line" 
			                    		onclick="return confirm('Bạn có chắc chắn muốn xóa phiếu chuyển kho này không?')" 
			                    		href="<?php 
			                    			echo System::$config->baseUrl
			                    				. "ql-kho/nhom-san-pham/xoa/"
 												. $category->product_category_id
 												. System::$config->urlSuffix
			                    		?>">
									<i class="glyphicon glyphicon-trash"></i>
								</a></td>
			                </tr>
                			<?php
                		}			
                	endif;
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewCategory.php*/
/*location: ViewCategory.php*/