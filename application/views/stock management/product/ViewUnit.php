<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Đơn vị tính</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                	. "ql-kho/don-vi-san-pham/them-moi"
 					. System::$config->urlSuffix;
                 ?>"><input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <td>
                        STT
                    </td>
                    <th>Đơn vị tính</th>
                    <th>Ghi chú</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            		if( isset( $data[ 'units' ] ) && !empty( $data[ 'units' ] ) ) {
            			$i = 0;
            			foreach ( $data[ 'units' ] as $unit ) {
            				$i++;
            				?>
            				<tr>
            					<td><?php echo $i; ?></td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ql-kho/don-vi-san-pham/chinh-sua/"
 										. $unit->unit_id
 										. System::$config->urlSuffix;
			                    ?>" title="Chỉnh sửa"><?php echo $unit->unit_name ?></a></td>
			                    <td><?php echo $unit->note;  ?></td>
			                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewUnit.php*/
/*location: ViewUnit.php*/