<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Kho hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ql-kho/kho-hang/them-moi"
 						. System::$config->urlSuffix; ?>">
 						<input type="button" class="btn btn-success" style="border-radius: 7px; margin-right: 5px; color: white;" value="Tạo mới"/>
 				</a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
	                System::$load->view(
	                        "includes/Pagination", 
	                        array(
			                    "data" => array(
			                        "pages" => $data['pages'],
			                        "page" => $data['page'],
			                        "dataUrl" => "ql-kho/kho-hang/trang/"
			                    )
	                        )
	                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <td>
                        STT
                    </td>
                    <td>
                        Mã kho
                    </td>
                    <th>Tên kho hàng</th>
                    <th>Trạng thái</th>
                    <td></td>
	                </tr>
            </thead>
            <tbody>
            	<?php 
            		if( isset( $data[ 'warehouses' ] ) && !empty( $data[ 'warehouses' ] ) ) {
            			$data[ 'startList' ] = (isset( $data[ 'startList' ] )) ? $data[ 'startList' ] : 0;
            			foreach ( $data[ 'warehouses' ] as $warehouse ) {
            				$data[ 'startList' ] ++;
            				?>
            				<tr>
			                    <td>
			                        <?php echo $data[ 'startList' ] ?>
			                    </td>
			                    <td>
			                    	<a href="<?php 
			                    	echo System::$config->baseUrl 
			                    		. "ql-kho/kho-hang/chinh-sua/"
 										. $warehouse->warehouse_id
			                    		. System::$config->urlSuffix;
			                    ?>" title=""><?php 
			                    	echo $warehouse->warehouse_code;
			                    ?></a>
			                    </td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl 
			                    		. "ql-kho/kho-hang/chinh-sua/"
 										. $warehouse->warehouse_id
			                    		. System::$config->urlSuffix;
			                    ?>" title=""><?php 
			                    	echo $warehouse->warehouse_name;
			                    ?></a></td>
			                    <td><?php 
			                    	if( $warehouse->is_runing == 1 ){
			                    		echo "<span class='text-success'>Đang hoạt động</span>";
			                    	} else {
			                    		echo "<span class='text-danger'>Không hoạt động</span>";
			                    	}
			                    ?></td>
			                    <td><a 
			                    		class="delete_order_line" 
			                    		onclick="return confirm('Bạn có chắc chắn muốn xóa phiếu chuyển kho này không?')" 
			                    		href="<?php 
			                    			echo System::$config->baseUrl
			                    				. "ql-kho/kho-hang/xoa/"
 												. $warehouse->warehouse_id
 												. System::$config->urlSuffix
			                    		?>">
									<i class="glyphicon glyphicon-trash"></i>
								</a></td>
			                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewStock.php*/
/*location: ViewStock.php*/