<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Kho hàng</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;" 
                    	value="Lưu"
                    	name="save" />
                    <a href="<?php 
                    		echo System::$config->baseUrl
                    			. "ql-kho/kho-hang"
 								. System::$config->urlSuffix; 
                    	?>">
                    	<input type="button"  class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;" value="Bỏ qua"/>
                    </a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 587px;">
            <div class="new_product" style="border-color: gray">
                <div class="form-new-order form-buy-new-order " >
                    <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left" style="padding-top: 10px;">
                                <div>
                                    <p>Tên kho hàng</p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	name="warehouse_name" 
                                    	style="width: 100%;" 
                                    	required="required"
                                    	value="<?php 
                                    		if( isset( $data[ 'warehouse' ]->warehouse_name ) ) 
                                    			echo $data[ 'warehouse' ]->warehouse_name;
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right" style="padding-top: 10px;">
                            	<div>
                                    <p>Mã kho</p>
                                </div>
                                <div class="info-post">
                                    <input 
	                            		type="text" 
	                            		name="" 
	                            		placeholder="Mã kho" 
	                            		style="width: 100%;"
	                            		disabled="disabled"
	                            		value="<?php 
                                    		if( isset( $data[ 'warehouse' ]->warehouse_code ) ) 
                                    			echo $data[ 'warehouse' ]->warehouse_code;
                                    	?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left">
                                <div>
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	name="warehouse_address" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;"
                                    	required="required"
                                    	value="<?php 
                                    		if( isset( $data[ 'warehouse' ]->warehouse_address ) ) 
                                    			echo $data[ 'warehouse' ]->warehouse_address;
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right">
                            	<div>
                                    <p>Đang hoạt động</p>
                                </div>
                                <div class="info-post">
                                    <input type="checkbox" value="1" name="is_runing" style=" text-align: left;"
                                    <?php if( $data[ 'warehouse' ]->is_runing == 1 ) echo "checked='checked'"; ?> />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left">
                                <div>
                                    <p></p>
                                </div>
                                <div class="info-post">
                                    <select name="district_id" style="width:47%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'districts' ] ) ) {
	                                			foreach ( $data[ 'districts' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->district_id; ?>"
	                                					<?php 
	                                						if( $item->district_id == $data[ 'warehouse' ]->district_id )
	                                							echo "selected='selected'";
	                                					?>
	                                					>
	                                					<?php echo $item->district_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="city_id" style="width:30%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'cities' ] ) ) {
	                                			foreach ( $data[ 'cities' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->city_id; ?>"
	                                					<?php 
	                                						if( $item->city_id == $data[ 'warehouse' ]->city_id )
	                                							echo "selected='selected'";
	                                					?>
	                                					>
	                                					<?php echo $item->city_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="country_id" style="width:20%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'countries' ] ) ) {
	                                			foreach ( $data[ 'countries' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->country_id; ?>"
	                                					<?php 
	                                						if( $item->country_id == $data[ 'warehouse' ]->country_id )
	                                							echo "selected='selected'";
	                                					?>
	                                					>
	                                					<?php echo $item->country_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
                                </div>
                            </td>
                            <td class="order-right">
                            </td>
                        </tr>
                    </table>  
                </div>
                <div class="product_detail_info product_detail_info_import"  style="border-bottom: none;">
                    <h4 style="text-align: left; font-weight: bold; margin-left: 20px; padding-top: 5px;">Thông tin bổ sung</h4>
                    <textarea 
                    	name="description" 
                    	style="padding-left: 10px; margin-bottom: 50px; margin-left: 20px;" 
                    	placeholder="Các ghi chú bổ sung"><?php 
                    		echo $data[ 'warehouse' ]->description;
                    	?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file EditWarehouse.php*/
/*location: EditWarehouse.php*/