<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Hàng trả về</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="import_id icon-search non-display">Số phiếu nhập 
                        <a href="#" id="import_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="customer_name icon-search non-display">Tên khách hàng 
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="cost_non_tax icon-search non-display">Giá chưa thuế 
                        <a href="#" id="cost_non_tax" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng tiền 
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_number_buy icon-search non-display">Số đơn hàng mua 
                        <a href="#" id="order_number_buy" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>

                    <span class="warehouse icon-search non-display">Kho hàng 
                        <a href="#" id="warehouse" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "mua-hang/tra-ve/them-moi"
                . System::$config->urlSuffix;
                ?>"><button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ql-kho/tra-ve/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        <label>Ngày tháng
                            <br/>
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
            <div class="search_date search-infor non-display">
                Từ<input type="date" name="date-from">
                <br/>
                Đến<input type="date" name="date-from">
            </div>
            </th>
            <th class="search-order">
                <label>Số phiếu nhập 
                    <br/>
                    <a id="import_id" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="import_id" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Tên khách hàng 
                    <br/>
                    <a id="customer_name" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="customer_name" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Giá chưa thuế 
                    <br/>
                    <a id="cost_non_tax" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="cost_non_tax" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Tổng tiền 
                    <br/>
                    <a id="total_price" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="total_price" class="search-detail search-infor non-display"/>
            </th> 
            <th class="search-order">
                <label>Số đơn hàng mua 
                    <br/>
                    <a id="order_number_buy" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="order_number_buy" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Kho hàng 
                    <br/>
                    <a id="warehouse" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="warehouse" class="search-detail search-infor non-display"/>
            </th>
            </tr>
            </thead>
            <tbody>
                <?php
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refunds'] as $refund) {
                    $data['startList'] ++;
                    ?>
                    <tr>
                        <td>
                            <?php echo $data['startList'] ?>
                        </td>
                        <td><?php
                            if ($refund->refund_date > 0)
                                echo date("d/m/Y", $refund->refund_date);
                            ?></td>
                        <td><a href="<?php
                            echo System::$config->baseUrl
                            . "mua-hang/nhap-kho/chinh-sua/"
                            . $refund->import_id
                            . System::$config->urlSuffix;
                            ?>" title=""><?php
                                   echo $refund->import_code
                                   ?></a></td>
                        <td><?php
                            echo $refund->customer_name;
                            ?></td>
                        <td></td>
                        <td></td>
                        <td><a href="<?php
                            echo System::$config->baseUrl
                            . "ban-hang/don-hang/chinh-sua/"
                            . $refund->order_id
                            . System::$config->urlSuffix;
                            ?>" title=""><?php
                                   echo $refund->order_code;
                                   ?></td>
                        <td><a target="_blank" href="<?php
                            echo System::$config->baseUrl
                            . "ql-kho/kho-hang/chinh-sua/"
                            . $refund->warehouse_id
                            . System::$config->urlSuffix;
                            ?>" title=""><?php
                                   echo $refund->warehouse_name;
                                   ?></a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewRefund.php*/
/*location: ViewRefund.php*/