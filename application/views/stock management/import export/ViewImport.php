<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Nhập kho</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="import_id icon-search non-display">Số phiếu nhập 
                        <a href="#" id="import_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers icon-search non-display">Nhà cung cấp
                        <a href="#" id="providers" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="cost_non_tax icon-search non-display">Giá chưa thuế
                        <a href="#" id="cost_non_tax" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng tiền
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_number_buy icon-search non-display">Số đơn đặt hàng
                        <a href="#" id="order_number_buy" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ql-kho/nhap-kho/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th class="search-order">
                        <label>Ngày tháng
                            <br/>
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
            <div class="search_date search-infor non-display">
                Từ<input type="date" name="date-from">
                <br/>
                Đến<input type="date" name="date-from">
            </div>
            </th>
            <th class="search-order">
                <label>Số phiếu nhập 
                    <br/>
                    <a id="import_id" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="import_id" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Nhà cung cấp
                    <br/>
                    <a id="providers" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="providers" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Giá chưa thuế
                    <br/>
                    <a id="cost_non_tax" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="cost_non_tax" class="search-detail search-infor non-display"/>
            </th>
            <th class="search-order">
                <label>Tổng tiền
                    <br/>
                    <a id="total_price" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="total_price" class="search-detail search-infor non-display"/>
            </th> 
            <th class="search-order">
                <label>Số đơn đặt hàng
                    <br/>
                    <a id="order_number_buy" href="#">
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    </a>
                </label>
                <br/>
                <input type="text" name="order_number_buy" class="search-detail search-infor non-display"/>
            </th>
            <th></th>
            </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data['imports'])) {
                    foreach ($data['imports'] as $import) {
                        ?>
                        <tr>
                            <td><?php
                                if ($import->import_date > 0) {
                                    echo date("d-m-Y", $import->import_date);
                                }
                                ?></td>
                            <td><a href="<?php
                                echo System::$config->baseUrl
                                . "mua-hang/nhap-kho/chinh-sua/"
                                . $import->import_id
                                . System::$config->urlSuffix;
                                ?>" title=""><?php
                                       echo $import->import_code;
                                       ?></a></td>
                            <td><?php
                                echo $import->supplier_name;
                                ?></td>
                            <td><?php echo $data[ 'mMoney' ]->addDotToMoney( $import->total_sale_no_tax ); ?></td>
                            <td><?php echo $data[ 'mMoney' ]->addDotToMoney( $import->total_sale ); ?></td>
                            <td></td>
                            <td><a class="delete_order_line"
                                   onclick="return confirm('Bạn có chắc chắn muốn xóa phiếu chuyển kho này không?')" 
                                   href="<?php
                                   echo System::$config->baseUrl
                                   . "mua-hang/nhap-kho/xoa/"
                                   . $import->import_id
                                   . System::$config->urlSuffix;
                                   ?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewImport.php*/
/*location: ViewImport.php*/