<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<input value="1" id="adjustInventory" type="hidden" />
<div class="content">
    <form action=" " method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Điều chỉnh tồn kho</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;"
                    	name="save" 
                    	value="Lưu"/>
                    <a href="<?php 
                    	echo System::$config->baseUrl 
                    		. "ql-kho/ton-kho"
 							. System::$config->urlSuffix;	
                    ?>">
                    	<input type="button"  
                    		class="btn btn-warning" 
                    		style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    		value="Bỏ qua"/>
                    </a>
                </div>
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 587px;">
            <div class="form-new-order form-buy-new-order " >
                <h4 style="text-align: left; padding-left: 20px; font-weight: bold;" >THÔNG TIN CƠ BẢN</h4>
                <table>
                    <tr>
                        <td class="order-left" style="padding-top: 10px;">
                            <div>
                                <p>Tên điều chỉnh kho</p>
                            </div>
                            <div class="info-post">
                                <input 
                                	type="text" 
                                	name="adjust_inventory_name" 
                                	placeholder="VD: Điều chỉnh tồn kho quý 1" style="width: 100%;"
                                	required="required"
                                	value="<?php 
                                		if( isset( $_POST[ 'adjust_inventory_name' ] ) ){
                                			echo $_POST[ 'adjust_inventory_name' ];
                                		}
                                	?>" />
                                <div class="text-danger">
                                	<?php
                                		if( isset( $data['errors']['adjust_inventory_name'] ) ) {
                                			echo $data['errors']['adjust_inventory_name'];
                                		}
                                	?>
                                </div>
                            </div>
                        </td>
                        <td class="order-right" style="padding-top: 10px;">
                            <div>
                                <p>Ngày tạo</p>
                            </div>
                            <div class="info-post">
                                <input type="date" name="inventory_date" style="width: 80%;" value="<?php 
                                	echo date("Y-m-d", time());
                                ?>" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-left">
                            <div>
                                <p>Kho hàng</p>
                            </div>
                            <div class="info-post">
                                <select name="warehouse_id" style="width: 100%;">
			                        <?php
			                        if (isset($data['warehouses'])) {
			                            foreach ($data['warehouses'] as $item) {
			                                ?>
			                                <option value="<?php echo $item->warehouse_id; ?>"> 
			                                	<?php echo $item->warehouse_name; ?>
			                               	</option>
			                                <?php
			                            }
			                        }
			                        ?>
                                </select>
                            </div>
                        </td>
                        <td class="order-right">
                        </td>
                    </tr>
                </table>  
            </div>
            <div class="order-detail buy-order-detail">
                <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">CHI TIẾT ĐIỀU CHỈNH</h4>
                <table class="table tbl-order-detail order_line">
                    <thead>
                        <tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 6%">Số lượng hiện có</th>
                            <th style="width: 6%">Số lượng mới</th>
                            <th style="width: 10%">Đơn giá</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 10%">Ngày hết hạn sử dụng</th>
                            <th style="width: 5%"></th>
                            <!--  
                            <th style="width: 10%">Chênh lệch</th>
                            <th style="width: 10%">Đơn giá</th>
                            <th style="width: 15%; border-right: none">Tổng tiền</th>
                            -->
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="height: 50px; background: #E6E7E8;">
                            <td colspan="9" style="line-height: 50px;border-right: none;">
                            	<a id="add_product">
				                	<h4 
				                		style="	text-align: left; 
				                				margin-left: 10px;
				                				border-bottom: #000 1px solid; 
				                				padding-bottom: 3px; 
				                				margin-right: 10px; ">
				                		Thêm sản phẩm
				                	</h4>
				                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="add_product">
                    <div class="product_detail">
                    	<?php 
                        		System::$load->view( 
                        			'includes/AddProductToOrder',
 									array( 'products' => $data['products'] ) 
                        		);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<br /><br />
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
        <td>
        	<input type="hidden" name="adjust_inventory_line_id[]" value="0" />
			<input type="hidden" class="product_id" name="product_id[]" value="" />
            <div class="html_product_id"></div>
        </td>
		<td><div class="html_product_name"></div></td>
		<td><div class="product_inventory_amount">0</div></td>
		<td>
			<input
				style="width: 80px;"
				type="number"
				name="product_amount[]"
				required="required"
     			onchange="calTotalPriceItem(this)"
				value=""
		   />
		</td>
		<td>
			<input 
				style="width: 80%" 
				class="input_table" 
				type="number" 
				name="product_price[]" 
				required="required" />
		</td>
		<td><div class="html_product_unit"></div></td>
		<td>
			<input style="width: 80%" type="date" class="input_table" name="expire_date[]" value="">
		</td>
		<td>
			<a class="delete_order_line" onclick="deleteOrderLine( this )">
				<i class="glyphicon glyphicon-trash"></i>
		    </a>
		</td>
	</tr>
</table>
</div>
<?php
/*end of file NewChangeInventory.php*/
/*location: NewChangeInventory.php*/