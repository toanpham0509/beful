<?php
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Điều chỉnh tồn kho</h4>
            <div class="search-ban-hang display">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl 
                		. "ql-kho/ton-kho/them-moi"
 						. System::$config->urlSuffix	
                ?>">
                	<button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới
                	</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", 
                        array(
                    		"data" => array(
		                        "pages" => $data['pages'],
		                        "page" => $data['page'],
		                        "dataUrl" => "ql-kho/ton-kho/trang/"
	                    	)
                  		)
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <th>
                        STT
                    </th>
                    <th>Ngày tháng</th>
                    <th>Tên điều chỉnh</th>
                    <th>Kho hàng</th> 
                    <th>Trạng thái</th>
                    <th><i class="glyphicon glyphicon-trash"></i></th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            		if( isset( $data['adjustInventory'] ) 
            			&& !empty( $data[ 'adjustInventory' ] ) ){
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ( $data[ 'adjustInventory' ] as $item ) {
            				$data['startList'] ++;
            				?>
	            			<tr>
			                    <td>
			                        <?php echo $data[ 'startList' ]; ?>
			                    </td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ql-kho/ton-kho/chinh-sua/"
 										. $item->adjust_inventory_id
			                    		. System::$config->urlSuffix;
			                    ?>" title="Chỉnh sửa"><?php 
			                    	echo $item->adjust_inventory_name;
			                    ?></a></td>
			                    <td><?php 
			                    	if( $item->adjust_inventory_date > 0 ) 
			                    		echo date("d/m/Y", $item->adjust_inventory_date);
			                    ?></td>
			                    <td><a target="_blank" href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ql-kho/kho-hang/chinh-sua/"
 										. $item->warehouse_id
			                    		. System::$config->urlSuffix;
			                    ?>"><?php
			                    	echo $item->warehouse_name;
			                    ?></a></td>
			                    <td><?php
			                    	if( $item->status == 3 )
			                    		echo "<span class='text-success'>Hoàn thành</span>";
			                    	else
			                    		echo "<span class='text-danger'>Đang chờ</span>";
			                    ?></td>
			                    <td>
			                    <?php if( $item->status == 2 ) : ?>
			                    	<a class="delete_order_line" 
	                                   onclick="return confirm('Bạn có chắc chắn muốn xóa điều chỉnh tồn kho này không?')" 
	                                   href="<?php
	                                   		echo System::$config->baseUrl
	                                   			. "ql-kho/ton-kho/xoa/"
	                                   			. $item->adjust_inventory_id
	                                   			. System::$config->urlSuffix;
	                                   ?>">
                                    <i class="glyphicon glyphicon-trash"></i>
	                                </a>
	                            <?php endif; ?>
			                    </td>
			                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/