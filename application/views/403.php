<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Truy cập trái phép</title>
        <style type="text/css" rel="stylesheet" media="screen">
            body {
                color: $F00;
            }
            h1 {
                color: #F00;
                text-align: center;
            }
        </style>
    </head>
    <body>
    <h1>
	    Truy cập trái phép!
    </h1>
    <h1>
        <a href="<?= $_SERVER['HTTP_REFERER'] ?>">Trở lại</a>
    </h1>
	</body>
</html>
<?php
/*end of file 403.php*/
/*location: 403.php*/