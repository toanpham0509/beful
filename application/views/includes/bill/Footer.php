<footer>
    <br />
    <div style="height:30px; float:right;">Ngày ... Tháng ... Năm 20...</div>
    <br /><br />
    <div class="footer-left">
        <div>
            <h4>Khách hàng</h4>
            <span>(Ký và ghi rõ họ tên)</span>
        </div>
    </div>
    <div class="footer-center">
        <div>
            <h4>Người giao hàng</h4>
            <span>(Ký và ghi rõ họ tên)</span>
        </div>
    </div>
    <div class="footer-right">
        <div>
            <h4>Thủ trưởng</h4>
            <span>(Ký và ghi rõ họ tên)</span>
        </div>
    </div>
</footer>