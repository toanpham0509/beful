<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<a class="btn btn-default btnPrint" href="<?php echo $urlPrint; ?>" style="color:#000; border-radius: 7px; margin-right: 5px;">In đơn hàng</a>
<a class="btn btn-success text-success" style="color:#000; border-radius: 7px; margin-right: 5px;"> Đã xác nhận</a>
<?php
/*end of file PrintConfirm.php*/
/*location: PrintConfirm.php*/