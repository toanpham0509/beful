<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div style="">
<div class="tp-search-box">
    <input type="hidden" name="base_url" value="<?php 
        echo System::$config->baseUrl 
            . "ajax/supplier/getSupplier/";
    ?>" />
    <input class="input-search" placeholder="Tên hoặc mã số nhà cung cấp..." />
    <div class="result">
    	<a class="closeSearch">Đóng</a>
        <?php
        if (isset($data['suppliers'])) {
            foreach ($data['suppliers'] as $supplier) {
                $prefix = " (".
                    $supplier->supplier_id
                    . ") - ";
                if( isset( $supplier->phone ) ) {
                    $prefix .= $supplier->phone;
                }
                ?>
                <div 
                    class="item-result" 
                    data-name="<?php 
                        echo unicode_str_filter(
                                strtolower ( $supplier->supplier_name)
                        ) . $prefix; 
                    ?>" 
                    data-id="<?php echo $supplier->supplier_id; ?>"
                    data-display="<?php echo $supplier->supplier_name . $prefix; ?>" 
                    data-input-name="supplier_id" >
                    <?php echo $supplier->supplier_name . $prefix; ?>
                </div> 
                <?php
            }
        }
        ?>
        <input name="supplier_id" type="hidden" value="" />
    </div>
</div>
</div>
<?php
/*end of file SearchSupplier.php*/
/*location: SearchSupplier.php*/