<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
	<div class="mask"></div>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		
		<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js?random=<?php echo uniqid(); ?>"></script>
		<script type="text/javascript" src="<?php echo System::$config->baseUrl; ?>public/js/style.js"></script>
		<script type="text/javascript" src="<?php echo System::$config->baseUrl; ?>public/js/search.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/highcharts.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/modules/exporting.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/Config.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/Utinity.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl ?>public/js/Pay.js?random=<?php echo uniqid(); ?>"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/jquery.printPage.js"></script>
        <script src="<?php echo System::$config->baseUrl; ?>public/js/Print.js?random=<?php echo uniqid(); ?>"></script>
		<script src="<?php echo System::$config->baseUrl; ?>public/js/SaleOrderSelectChangePrice.js?random=<?php echo uniqid(); ?>"></script>
		<script src="<?php echo System::$config->baseUrl; ?>public/js/BuyOrderSelectChangePrice.js?random=<?php echo uniqid(); ?>"></script>
		<script src="<?php echo System::$config->baseUrl; ?>public/js/Validate.js?random=<?php echo uniqid(); ?>"></script>
		<script src="<?php echo System::$config->baseUrl; ?>public/js/search-title.js?random=<?php echo uniqid(); ?>"></script>
	</body>
</html>
<?php
/*end of file Footer.php*/
/*location: Footer.php*/