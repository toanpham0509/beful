<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="row">
                        		<div class="col-lg-4 col-md-4 col-sm-4">
                        			<span>Nhập mã đơn hàng</span>
                        		</div>
                        		<div class="col-lg-4 col-md-4 col-sm-4">
	                        		<div>
	                        			<input type="hidden" id="addOrder_OrderID" name="addOrder_OrderID" value="" />
										<div class="tp-search-box">
			                                <input 
			                                	class="input-search form-control"
			                                	style="height: 40px;" 
			                                    placeholder="Tên hoặc mã đơn hàng..."
			                                    value="" />
			                                <div class="result" style=" top: 40px;">
			                                	<?php
													if (isset( $orders)) {
														foreach ($orders as $order) {
												?>
														<div 
															class="item-result" 
			                                                data-name="<?php echo unicode_str_filter(strtolower ( 
			                                                		$order->product_name 
			                                                		. " (" 
			                                                		. $order->product_code .") "
			                                                )); ?>" 
			                                                data-id="<?php echo $order->product_id; ?>"
			                                                data-display="<?php 
			                                                	echo $order->product_name 
			                                                		. " (" 
			                                                		. $order->product_code .") "; ?>" 
			                                                data-input-name="addProduct_ProductID"
			                                            >
			                                            	<?php 
			                                                	echo $order->product_name 
			                                                		. "(" 
			                                                		. $order->product_code .")"; 
			                                                ?>
			                                            </div> 
															<?php
														}
													}
												?>
											</div>
										</div>
	                        		</div>
                        		</div>
                        		<div class="col-lg-4 col-md-4">
                        			<a style="margin-left: 30px;">
					                        	<input 
					                        		type="button"
					                        		name="search_product_id"
					                        		id="btnAddPOrderToPay" 
					                        		class="btn btn-success" 
					                        		value="Thêm đơn hàng"/>
					                        </a>
                        		</div>
                        	</div>
<?php
/*end of file AddOrderToPay.php*/
/*location: AddOrderToPay.php*/