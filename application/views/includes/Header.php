<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?php if( isset( $pageTitle ) ) echo $pageTitle . " - Beful"; else echo "Beful"; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap-theme.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/overight.css"/>
        
        <link rel="stylesheet" type="text/css" href="<?php echo System::$config->baseUrl; ?>public/css/search.css"/>
        
        <script type="text/javascript" src="<?php echo System::$config->baseUrl; ?>public/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript">
			var baseUrl = "<?php echo System::$config->baseUrl ?>";
        </script>
    </head>
    <body>
        <div class="container-header">
            <div style="margin-left: -40px; ">
                <header>
                    <ul class="header-menu">
                    	<?php
							$cUser = new \App\Controllers\User();
                    		$menuID = isset( $menuID ) ? $menuID : 0; 
                    		if( isset( $menus ) ) {
                    			foreach ( $menus as $menu ) {
									if(!$cUser->isAccessAllowed($menu['access'])) {
										continue;
									}
                    				?>
                    				<li class="<?php if( $menuID == $menu[ 'id' ] ) echo "header-active"; ?>">
                    					<a 
                    						href="<?php echo System::$config->baseUrl 
                    									. $menu[ 'url' ] 
                    									. System::$config->urlSuffix ?>"
                    						title="<?php echo $menu[ 'name' ]; ?>">
                    						<?php echo $menu[ 'name' ]; ?>
                    					</a>
                    				</li>
                    				<?php
                    			}
                    		}
                    	?>
                    </ul>
                    <div class="user_info">
                        <img src="<?php echo System::$config->baseUrl; ?>images/user.png" class="img-circle"/>
                        <div class="user  dropdown">
                            <div class="dropdown-toggle"> 
                                <?php 
                                	echo $userInfo['user_name'];
                                ?>
                                <span class="caret"></span>
                            </div>
                            <ul class="dropdown-menu">
                                <li><a href="">Thiết Lập Tài Khoản</a></li>
                                <li><a href="<?php 
                                	echo System::$config->baseUrl
                                		. "dang-xuat"
                                		. System::$config->urlSuffix
                                ?>">Đăng Xuất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </header>
            </div>
        </div>
		<div class="left-side left-side-warehouse-manage">
			<div class="logo">
	            <img src="<?php echo System::$config->baseUrl; ?>images/logo.png"/>
	        </div>
		    <ul>
		    	<?php
		    		$menuLeftCurrent = isset( $menuLeftCurrent ) ? $menuLeftCurrent : array();
					$cUser = new \App\Controllers\User();
		    		if( isset( $menuLefts ) ) {
		    			foreach ( $menuLefts as $k => $menuLeft ) {
		    				if( !is_int( $k ) && $k != 0 ) continue;
		    				if( !isset( $menuLeft['id'] ) ) continue;
		    				if(!$cUser->isAccessAllowed($menuLeft['access'])) {
								continue;
							}
		    				?>
		    				<li class="<?php if( $menuLeftCurrent['id'] == $menuLeft['id'] ) echo "header-active" ?>">
					        	<a href="<?php
					        	if( strlen( $menuLeft[ 'url' ] ) > 0 )
					        		echo System::$config->baseUrl 
					        			. $menuLeft[ 'url' ] 
					        			. System::$config->urlSuffix ;
					        	else 
					        		echo "#";
					        	?>">
					        		<?php echo $menuLeft[ 'name' ] ?>
					        	</a>
					        	<?php 
					        		$key = 'item_' . $menuLeft[ 'id' ];
					        		if( isset( $menuLefts[ $key ] ) && is_array( $menuLefts[ $key ] ) ) {
					        			echo "<ul>";
					        			foreach ( $menuLefts[ $key ] as $menuItem ) {
					        				?>
					        				<li class="<?php if( $menuLeftCurrent['id'] == $menuItem['id'] ) 
					        									echo "header-active" ?>">
									        	<a href="<?php
									        	if( strlen( $menuItem[ 'url' ] ) > 0 )
									        		echo System::$config->baseUrl 
									        			. $menuItem[ 'url' ] 
									        			. System::$config->urlSuffix ;
									        	else 
									        		echo "#";
									        	?>">
									        		<?php echo $menuItem[ 'name' ] ?>
									        	</a>
									        </li>
					        				<?php
					        			}
					        			echo "</ul>";
					        			unset( $menuLefts[ $key ] );
					        		}
					        	?>
					        	</li>
		    				<?php
		    			}
		    		}
		    	?>
		    </ul>
		</div>
<?php
function unicode_str_filter ($str){
	$unicode = array(
		'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
		'd'=>'đ',
		'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		'i'=>'í|ì|ỉ|ĩ|ị',
		'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		'D'=>'Đ',
		'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
		'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
	);
	foreach($unicode as $nonUnicode=>$uni){
		$str = preg_replace("/($uni)/i", $nonUnicode, $str);
	}
	return $str;
}
/*end of file Header.php*/
/*location: Header.php*/