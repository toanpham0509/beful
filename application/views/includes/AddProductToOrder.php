<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="row">
                        		<div class="col-lg-4 col-md-4 col-sm-4">
                        			<span>Nhập mã, tên sản phẩm để tìm kiếm</span>
                        		</div>
                        		<div class="col-lg-4 col-md-4 col-sm-4">
	                        		<div>
	                        			<input type="hidden" id="addProduct_ProductID" name="addProduct_ProductID" value="" />
										<div class="tp-search-box">
			                                <input 
			                                	class="input-search form-control"
			                                	style="height: 40px;" 
			                                    placeholder="Tên hoặc mã sản phẩm..."
			                                    value="" />
			                                <div class="result" style=" top: 40px;">
                                            	<a class="closeSearch">Đóng</a>
			                                	<?php
													if (isset( $products)) {
														foreach ($products as $product) {
												?>
														<div 
															class="item-result" 
			                                                data-name="<?php echo unicode_str_filter(strtolower ( 
			                                                		$product->product_name 
			                                                		. " (" 
			                                                		. $product->product_code .") "
			                                                )); ?>" 
			                                                data-id="<?php echo $product->product_id; ?>"
			                                                data-display="<?php 
			                                                	echo $product->product_name 
			                                                		. " (" 
			                                                		. $product->product_code .") "; ?>" 
			                                                data-input-name="addProduct_ProductID"
			                                            >
			                                            	<?php 
			                                                	echo $product->product_name 
			                                                		. "(" 
			                                                		. $product->product_code .")"; 
			                                                ?>
			                                            </div> 
															<?php
														}
													}
												?>
											</div>
										</div>
	                        		</div>
                        		</div>
                        		<div class="col-lg-4 col-md-4">
                        			<a style="margin-left: 30px;">
					                        	<input 
					                        		type="button"
					                        		name="search_product_id"
					                        		id="btnAddProductToOrder" 
					                        		class="btn btn-success" 
					                        		value="Thêm sản phẩm"/>
					                        </a>
                        		</div>
                        	</div>
<?php
/*end of file AddProductToOrder.php*/
/*location: AddProductToOrder.php*/