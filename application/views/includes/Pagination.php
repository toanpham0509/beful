<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
if ($data[ 'pages' ] == 0) $data[ 'pages' ] = 1;
?>
<p style="float: left;">Trang <?php echo $data[ 'page' ] ?> của <?php echo $data[ 'pages' ] ?></p>
	<ul class="page">
		<?php 
			$urlSuffix = "";
			if(isset($_REQUEST) && !empty($_REQUEST)) {
				$i = 0;
				foreach ($_REQUEST as $k => $v) {
					if($i == 0) {
						$urlSuffix .= "?";
					} else {
						$urlSuffix .= "&";
					}
					$urlSuffix .= $k . "=" . $v;
					$i++;
				}
			}
		?>
		<li style="border-left: gray 1px solid;font-weight: bold; border-right: gray 1px solid;">
			<a href="<?php
				echo System::$config->baseUrl
					. $data[ 'dataUrl' ] . "1"
					. System::$config->urlSuffix
					. $urlSuffix;
			?>">
				&lt;&lt;
			</a>
		</li>
       	<li style="border-left: gray 1px solid;font-weight: bold; border-right: gray 1px solid;">
           	<a href="<?php 
				if( $data['page'] > 1 )
					echo System::$config->baseUrl
						. $data[ 'dataUrl' ]
						. ($data['page'] - 1)
						. System::$config->urlSuffix
						. $urlSuffix;
            	?>">
            	&lt;
            </a>
		</li>
		<?php
			if($data[ 'page' ] > 2 && $data['page'] <= $data['pages']) {
				?>
					<li>...</li>
				<?php
			}
		?>
		<?php
           	for( $i = $data['page'] - 2; $i <= $data[ 'page' ] + 2 && $i <= $data['pages']; $i++ ) {
				if($i <= 0) continue;
           	?>
           		<li style="border-left: gray 1px solid; border-right: gray 1px solid;">
		              	<a href="<?php
		              		echo System::$config->baseUrl
		           			. $data[ 'dataUrl' ]
 							. $i
		           			. System::$config->urlSuffix
		           			. $urlSuffix; ?>"
		          		style="<?php if( $data['page'] == $i ) echo "color: red; font-weight: bold;"; ?>">
		      		<?php echo $i; ?>
		           	</a>
		              </li>
           			<?php
           		}
           	?>
		<?php
			if($data['page'] < $data['pages'] - 2 && $data['page'] > 0) {
				?>
				<li>...</li>
				<?php
			}
		?>
		<li style="border-right: gray 1px solid; font-weight: bold;">
              	<a href="<?php 
					if( $data['page'] < $data['pages'] )
						echo System::$config->baseUrl
							. $data[ 'dataUrl' ] 
							. ($data['page'] + 1)
							. System::$config->urlSuffix
							. $urlSuffix;
	               	?>">
             		&gt;
               	</a>
		</li>
		<li style="border-right: gray 1px solid; font-weight: bold;">
			<a href="<?php
				echo System::$config->baseUrl
						. $data[ 'dataUrl' ]
						. $data['pages']
						. System::$config->urlSuffix
						. $urlSuffix;
			?>">
				&gt;&gt;
			</a>
		</li>
	</ul>
<?php
/*end of file Pagination.php*/
/*location: Pagination.php*/