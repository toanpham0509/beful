<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="tp-search-box">
    <input type="hidden" name="base_url" value="<?php 
        echo System::$config->baseUrl . "ajax/customer/getCustomer/"
    ?>" />
    <input required="required" style="width: 100%;" class="input-search" placeholder="Tên hoặc mã số khách hàng..." />
    <div class="result">
    	<a class="closeSearch">Đóng</a>
        <?php
        if (isset($data['customers'])) {
            foreach ($data['customers'] as $customer) {
                $prefix = " (".
                            $customer->customer_id
                            . ") - ";
                if( isset( $customer->phone ) ) {
                    $prefix .= $customer->phone;
                }
                ?>
                <div 
                    class="item-result" 
                    data-name="<?php echo unicode_str_filter(
                        strtolower ( 
                            $customer->customer_name
                            . $prefix
                        )
                    ); ?>"
                    data-id="<?php echo $customer->customer_id; ?>"
                    data-display="<?php 
                        echo $customer->customer_name . $prefix
                    ?>" 
                    data-input-name="customer_id"
                    >
                    <?php echo $customer->customer_name . $prefix ?>
                </div> 
                <?php
            }
        }
        ?>
    </div>
    <input name="customer_id" type="hidden" value="" />
</div>
<?php
/*end of file SearchCustomer.php*/
/*location: SearchCustomer.php*/