<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Đơn đặt hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="order_number_buy icon-search non-display">Số đơn đặt hàng
                        <a href="#" id="order_number_buy" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers icon-search non-display">Nhà cung cấp
                        <a href="#" id="providers" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_date icon-search non-display">Ngày đặt hàng
                        <a href="#" id="order_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="estimate_date icon-search non-display">Ngày ước tính
                        <a href="#" id="estimate_date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="cost_non_tax icon-search non-display">Giá chưa thuế
                        <a href="#" id="cost_non_tax" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng tiền
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="status icon-search non-display">Trạng thái
                        <a href="#" id="status" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
            <div class="search-date search-buy-date">
                <form>
                    <div class="search-date-right"><label>Ngày</label><input class="input-date" type="date" name="search-date-from"/></div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "mua-hang/don-hang/them-moi"
                . System::$config->urlSuffix;
                ?>">
                    <button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ke-toan/don-hang-mua/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order" style="width:10% ">
                        <label>Số đơn đặt hàng
                            <br/>
                            <a id="order_number_buy" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="order_number_buy" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Nhà cung cấp
                            <br/>
                            <a id="providers" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Ngày đặt hàng
                            <br/>
                            <a id="order_date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
			            <div class="search-infor date_1 non-display">
			                <input type="date" name="order_date"/>
			            </div>
            		</th>
		            <th class="search-order">
		                <label>Ngày ước tính
		                    <br/>
		                    <a id="estimate_date" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		            <div class="search-infor date_2 non-display">
		                <input type="date" name="estimate_date_buy"/>
		            </div>
		            </th>
		            <th class="search-order">
		                <label>Giá chưa thuế
		                    <br/>
		                    <a id="cost_non_tax" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="cost_non_tax" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Tổng tiền
		                    <br/>
		                    <a id="total_price" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="total_price" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Trạng thái
		                    <br/>
		                    <a id="status" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="status" class="search-detail search-infor non-display"/>
		            </th>
		            <th>
		            </th>
            	</tr>
            </thead>
            <tbody>
                <?php
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['orders'] as $order) {
                    $data['startList'] ++;
                    ?>
                    <tr>
                        <td>
                            <?php echo $data['startList'] ?>
                        </td>
                        <td><a href="<?php
                            echo System::$config->baseUrl
                            . "mua-hang/don-hang/chinh-sua/"
                            . $order->order_id
                            . System::$config->urlSuffix;
                            ?>" title="Chỉnh sửa">
                                   <?php echo $order->order_code ?>
                            </a></td>
                        <td><?php echo $order->supplier_name ?></td>
                        <td><?php if ($order->order_date > 0) echo date("d/m/Y", $order->order_date); ?></td>
                        <td><?php if ($order->intended_date > 0) echo date("d/m/Y", $order->intended_date); ?></td>
                        <td><?php echo $data[ 'mMoney' ]->addDotToMoney( $order->total_sale ); ?></td>
                        <td><?php echo $data[ 'mMoney' ]->addDotToMoney( $order->total_sale ); ?></td>
                        <td><?php 
	                        if( $order->status == 2 )
	                        	echo "Đang chờ";
	                        else
	                        	echo "Hoàn thành";
                        ?></td>
                        <td><a class="delete_order_line" 
                                   onclick="return confirm('Bạn có chắc chắn muốn xóa đơn hàng này không?')" 
                                   href="<?php
                                   echo System::$config->baseUrl
                                   . "mua-hang/don-hang/xoa/"
                                   . $order->order_id
                                   . System::$config->urlSuffix;
                                   ?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                       </a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/