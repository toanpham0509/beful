<?php
use			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Thanh toán nhà cung cấp</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl 
                		. "ke-toan/thanh-toan-nha-cung-cap/them-moi"
 						. System::$config->urlSuffix;
 		 			?>">
                	<input 
                		type="button" 
                		class="btn btn-success" 
                		style="border-radius: 7px; margin-right: 5px; color: white;" 
                		value="Tạo mới"/>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php 
	                System::$load->view(
		                "includes/Pagination", array(
		                "data" => array(
			                "pages" => $data['pages'],
			                "page" => $data['page'],
			                "dataUrl" => "ke-toan/thanh-toan-nha-cung-cap/trang/"
			             	)
			           	)
		            );
                ?>
            </div>
        </div>
    </div>

    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Ngày tháng</th>
                    <th>Số phiếu thanh toán</th>
                    <th>Tên nhà cung cấp</th>
                    <th>Phiếu nhập</th>
                    <th>Phải thanh toán</th>
                    <th>Đã thanh toán</th>
                    <th>Còn nợ</th>
					<th>Tk thanh toán</th>
                    <th>Tình trạng</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            		if( isset( $data['supplierPay'] ) && !empty($data['supplierPay']) ) {
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ( $data['supplierPay'] as $item ) {
            				$data['startList'] ++;
            				?>
            					<tr>
				                    <td><?php echo $data['startList'] ?></td>
				                    <td><?php 
				                    	if( $item['pay_date'] > 0 ) {
				                    		echo date("d/m/Y", $item['pay_date']);
				                    	}
				                    ?></td>
				                    <td><a href="<?php 
				                    	echo System::$config->baseUrl
				                    		. "ke-toan/thanh-toan-nha-cung-cap/chinh-sua/"
 											. $item['pay_id']
				                    		. System::$config->urlSuffix;
				                    ?>"><?php echo $item['pay_code'] ?></a></td>
				                    <td><a target="_blank" href="<?php 
				                    	echo System::$config->baseUrl
				                    		. "mua-hang/nha-cung-cap/chinh-sua/"
 											. $item['partner_id']
				                    		. System::$config->urlSuffix;
				                    ?>"><?php 
				                    	echo $item['partner_name'];
				                    ?></a></td>
				                    <td><?php
				                    	if(isset($item['order']) && !empty($item['order'])) {
				                    		for($i = 0; $i < sizeof($item['order']); $i++) {
				                    			if(filter_var($item['order'][$i]['order_id'], FILTER_VALIDATE_INT)) {
				                    				?>
				                    				<p><a target="_blank" href="<?php
														echo System::$config->baseUrl
															. "mua-hang/nhap-kho/chinh-sua/"
	 														. $item['order'][$i]['order_id']
															. System::$config->urlSuffix;
				                    				?>"><?php 
				                    					echo $item['order'][$i]['input_code'];
				                    				?></a> </p>
				                    				<?php
				                    			}
				                    		}
				                    	}
				                    ?></td>
				                    <td><?php 
				                    	echo $data['mMoney']->addDotToMoney($item['total_order']);
				                    ?></td>
				                    <td><?php 
				                    	echo $data['mMoney']->addDotToMoney($item['total_price']);
				                    ?></td>
				                    <td><?php 
				                    	echo $data['mMoney']->addDotToMoney($item['owed_money']);
				                    ?></td>
				                    <td><?php 
				                    	echo $item['pay_type_name']; 
				                    ?></td>
				                    <td><?php 
				                    	echo $item['status'];
				                    ?></td>
				                    <td>
										<?php if($item['pay_status_id'] != 3) : ?>
				                    	<a 
				                    		class="delete_order_line" 
				                    		onclick="return confirm('Bạn có chắc chắn muốn xóa phiếu thanh toán này không?')" 
					                    	href="<?php 
					                    		echo System::$config->baseUrl
					                    			. "ke-toan/thanh-toan-nha-cung-cap/xoa/"
	 												. $item['pay_id']
					                    			. System::$config->urlSuffix;
					                    	?>">
				                             <i class="glyphicon glyphicon-trash"></i>
				                       </a>
										<?php endif; ?>
				                    </td>
				                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/