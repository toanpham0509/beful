<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">

    <form action="" method="POST">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Phiếu thu</h4><h4>/ Chỉnh sửa</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    	. "ke-toan/phieu-thu"
                    	. System::$config->urlSuffix;
                    ?>"><input 
                    	type="button"  
                    	class="btn btn-warning" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    	value="Bỏ qua"/>
                    </a>
                </div>
                <div class="nav-center">
                	<?php
                		System::$load->view("includes/PrintConfirmed", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => System::$config->baseUrl
							. "ke-toan/phieu-thu/xac-nhan/"
					        . $data[ 'receipt' ]['receipt_id']
					        . System::$config->urlSuffix
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 678px;">
            <div class="new_product" style="border-color: gray">
                <div class="form-new-order form-buy-new-order " >
                    <h4 style="text-align: left;font-weight: bold; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên đối tác</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <div class="info-post  branches_info_right">
                                    <div class="tp-search-box">
                                    	<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                            			?>" />
                                        <input 
                                        	required="required"
                                        	disabled="disabled"
                                        	class="input-search" 
                                        	placeholder="Tên hoặc mã số khách hàng..."
                                        	value="<?php 
                                        	if(isset($data[ 'receipt' ]['partner_name']))
                                        		echo $data[ 'receipt' ]['partner_name'];
                                        	?>" />
	                                        <input name="partner_id" onchange="addOrderToPay(this)" type="hidden" value="<?php
	                                        if(isset($data[ 'receipt' ]['partner_id'])) 
			                                	echo $data[ 'receipt' ]['partner_id'];
			                                ?>" />
                                    </div>
                                </div>
                                </div>
                            </td>
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Số phiếu</p>
                                </div>
                                <div class="info-post branches_info_right">
                                    <input type="text" name="" value="<?php 
	                                    if(isset($data[ 'receipt' ]['receipt_code']))
	                                    	echo $data[ 'receipt' ]['receipt_code'];
                                    ?>" disabled="disabled"  style="width: 100%;" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="address" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;"
                                    	value="<?php 
                                    		if(isset($data['receipt']['partner_address'])) {
                                    			echo $data['receipt']['partner_address'];
                                    		}
                                    	?>"
                                    	disabled="disabled" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Phương thức</p>
                                </div>
                                <div class="info-post">
                                    <select 
                                    	name="pay_type_id"
                                    	disabled="disabled"
                                    	style="width: 100%;">
                                        <?php 
                                        if( isset( $data[ 'payTypes' ] ) && !empty( $data['payTypes'] ) ) { 
                                        	foreach ( $data[ 'payTypes' ] as $payType ) {
                                        		?>
                                        		<option value="<?php echo $payType['pay_type_id'] ?>" <?php 
                                        			if( isset($data['receipt']['pay_type_id']) 
                                        				&& $data['receipt']['pay_type_id'] == $payType['pay_type_id'] )
                                        				echo "selected='selected'";
                                        		?>><?php 
                                        			echo $payType['pay_type_name'];
                                        		?></option>
                                        		<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Người nộp tiền</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input
                                    	disabled="disabled" 
                                    	placeholder="Người nộp tiền..." 
                                    	type="text" 
                                    	value="<?php 
                                    		if(isset($data['receipt']['payer_name'])) echo $data['receipt']['payer_name'];
                                    	?>" 
                                    	name="payer_name" 
                                    	style="width:100%; padding-left: 5px; "/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Ngày tháng</p>
                                </div>
                                <div class="info-post">
                                    <input
                                    	disabled="disabled" 
                                    	type="date" 
                                    	name="receipt_date" 
                                    	style="width: 100%;" value="<?php
                                    	if( isset($data['receipt']['receipt_date']) && $data['receipt']['receipt_date'] ) {
                                    		echo date( "Y-m-d", $data['receipt']['receipt_date']);
                                    	}  else {
                                    		echo date( "Y-m-d" );
                                    	}
                                    ?>"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Mô tả</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input
                                    	disabled="disabled" 
                                    	placeholder="Mô tả..." 
                                    	type="text" value="<?php 
                                    		if(isset($data['receipt']['description'])) echo $data['receipt']['description'];
                                    	?>" name="Bdescription" style="width:100%; padding-left: 5px; "/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="order-detail buy-order-detail" style="margin-bottom: 130px;">
                    <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN PHIẾU THU</h4>
                    <table class="table tbl-order-detail order_line">
                        <thead>
                            <tr style="background: #E6E7E8">
                                <th>Mô tả</th>
                                <!-- 
                                <th>Đối tác</th>
                                <th>Loại</th>
                                -->
                                <th>Số tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		$totalPrice = 0;
                        		if(isset($data['receipt']['lines']) 
                        			&& !empty($data['receipt']['lines'])) {
                        			foreach ($data['receipt']['lines'] as $line) {
                        				$totalPrice += $line["money"];
                        				?>
                        				<tr>
								            <td>
								            	<input name="line_id[]" value="0" type="hidden" />
								            	<input
								            		disabled="disabled" 
								            		placeholder="Mô tả..." type="text" name="description[]" style="width:100%" value="<?php 
								            		echo $line['description'];
								            	?>" />
											</td>
								            <td>
								            	<input
								            		disabled="disabled" 
								            		onchange="calTotalPricePay()"
								            		onkeyup="calTotalPricePay()"
									            	value="<?php 
								            			echo $line['money'];
								            	?>" placeholder="Số tiền..." type="number" name="money[]" style="width:100%" />
								            </td>
										</tr>
                        				<?php
                        			}
                        		}
                        	?>
                        </tbody>
                    </table>
                    <div class="clear"></div>
                    <table class="payment">
                        <tr style="font-size: large; border-bottom: #000 1px solid;">
                            <td class="payment-left">Tổng:</td>
                            <td class="payment-right totalPricePay"><?php 
                            	echo $data['mMoney']->addDotToMoney($totalPrice);
                            ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="htmlAddItemToPay" style="display: none;">
	<table>
		<tr>
            <td>
            	<input name="line_id[]" value="0" type="hidden" />
            	<input placeholder="Mô tả..." type="text" name="description[]" style="width:100%" />
			</td>
            <td>
            	<input 
            		placeholder="Số tiền..." 
            		onchange="calTotalPricePay()"
            		onkeyup="calTotalPricePay()"
            		type="text" 
            		name="money[]" 
            		style="width:100%" 
            		required="required" />
            </td>
			<td>
            	<a class="delete_order_line" onclick="deleteOrderLine( this )">
                	<i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
		</tr>
	</table>
</div>
<?php
/*end of file AddNew.php*/
/*location: AddNew.php*/