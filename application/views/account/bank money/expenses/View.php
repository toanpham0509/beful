<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">

    <form action="" method="POST">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Phiếu chi</h4><h4>/ Chỉnh sửa</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    	. "ke-toan/phieu-chi"
                    	. System::$config->urlSuffix;
                    ?>"><input 
                    	type="button"  
                    	class="btn btn-warning" 
                    	style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    	value="Bỏ qua"/>
                    </a>
                    <div>
			            <?php 
			            	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
						?>
		            </div>
                </div>
                <div class="nav-center">
		            <?php
                		System::$load->view("includes/PrintConfirmed", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => System::$config->baseUrl
							. "ke-toan/phieu-chi/xac-nhan/"
					        . $data[ 'expense' ]['expense_id']
					        . System::$config->urlSuffix
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 678px;">
            <div class="new_product" style="border-color: gray">
                <div class="form-new-order form-buy-new-order " >
                    <h4 style="text-align: left;font-weight: bold; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên đối tác</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <div class="info-post  branches_info_right">
                                    <div class="tp-search-box">
                                    	<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                            			?>" />
                                        <input 
                                        	required="required"
                                        	disabled="disabled"
                                        	class="input-search" 
                                        	placeholder="Tên hoặc mã số khách hàng..."
                                        	value="<?php 
                                        	if(isset($data[ 'expense' ]['partner_name']))
                                        		echo $data[ 'expense' ]['partner_name'];
                                        	?>" />
	                                        <input name="partner_id" onchange="addOrderToPay(this)" type="hidden" value="<?php
	                                        if(isset($data[ 'expense' ]['partner_id'])) 
			                                	echo $data[ 'expense' ]['partner_id'];
			                                ?>" />
                                    </div>
                                </div>
                                </div>
                            </td>
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Số phiếu</p>
                                </div>
                                <div class="info-post branches_info_right">
                                    <input type="text" name="" value="<?php 
	                                    if(isset($data[ 'expense' ]['expense_code']))
	                                    	echo $data[ 'expense' ]['expense_code'];
                                    ?>" disabled="disabled"  style="width: 100%;" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="address" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;"
                                    	value="<?php 
                                    		if(isset($data['expense']['partner_address'])) {
                                    			echo $data['expense']['partner_address'];
                                    		}
                                    	?>"
                                    	disabled="disabled" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Phương thức</p>
                                </div>
                                <div class="info-post">
                                    <select
                                    	disabled="disabled" 
                                    	name="pay_type_id" 
                                    	style="width: 100%;">
                                        <?php 
                                        if( isset( $data[ 'payTypes' ] ) && !empty( $data['payTypes'] ) ) { 
                                        	foreach ( $data[ 'payTypes' ] as $payType ) {
                                        		?>
                                        		<option value="<?php echo $payType['pay_type_id'] ?>" <?php 
                                        			if( isset($data['expense']['pay_type_id']) 
                                        				&& $data['expense']['pay_type_id'] == $payType['pay_type_id'] )
                                        				echo "selected='selected'";
                                        		?>><?php 
                                        			echo $payType['pay_type_name'];
                                        		?></option>
                                        		<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Người nhận tiền</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input
                                    	disabled="disabled" 
                                    	placeholder="Người nộp tiền..." type="text" value="<?php 
                                    	if(isset($data['expense']['receiver_name'])) echo $data['expense']['receiver_name'];
                                    ?>" name="receiver_name" style="width:100%; padding-left: 5px; "/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Ngày tháng</p>
                                </div>
                                <div class="info-post">
                                    <input
                                    	disabled="disabled" 
                                    	type="date" 
                                    	name="expense_date" style="width: 100%;" value="<?php
                                    	if( isset($data['expense']['expense_date']) && $data['expense']['expense_date'] ) {
                                    		echo date( "Y-m-d", $data['expense']['expense_date']);
                                    	}  else {
                                    		echo date( "Y-m-d" );
                                    	}
                                    ?>"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Mô tả</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <textarea
                                    	disabled="disabled" 
                                    	placeholder="Mô tả..."
                                        name="Bdescription"
                                        style="width:100%; padding-left: 5px; "><?php
                                        if(isset($data['expense']['description'])) echo $data['expense']['description'];
                                        ?></textarea>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="order-detail buy-order-detail" style="margin-bottom: 130px;">
                    <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN PHIẾU THU</h4>
                    <table class="table tbl-order-detail order_line">
                        <thead>
                            <tr style="background: #E6E7E8">
                                <th>Mô tả</th>
                                <th>Số tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
	                        	$totalPrice = 0;
                        		if(isset($data['expense']['lines']) 
                        			&& !empty($data['expense']['lines'])) {
                        			foreach ($data['expense']['lines'] as $line) {
                        				$totalPrice += $line["money"];
                        				?>
                        				<tr>
								            <td>
								            	<input name="line_id[]" value="0" type="hidden" />
								            	<input
								            		disabled="disabled" 
								            		placeholder="Mô tả..." type="text" name="description[]" style="width:100%" value="<?php 
								            		echo $line['description'];
								            	?>" />
											</td>
								            <td>
								            	<input 
								            		value="<?php 
								            		echo $line['money'];
								            	?>" 
								            		disabled="disabled"
								            		placeholder="Số tiền..." 
								            		type="number" 
								            		name="money[]" 
								            		onchange="calTotalPricePay()"
								            		onkeyup="calTotalPricePay()"
								            		style="width:100%" />
								            </td>
										</tr>
                        				<?php
                        			}
                        		}
                        	?>
                        </tbody>
                    </table>
                    <div class="clear"></div>
                    <table class="payment">
                        <tr style="font-size: large; border-bottom: #000 1px solid;">
                            <td class="payment-left">Tổng:</td>
                           	<td class="payment-right totalPricePay"><?php 
                            	echo $data['mMoney']->addDotToMoney($totalPrice);
                            ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="htmlAddItemToPay" style="display: none;">
	<table>
		<tr>
            <td>
            	<input name="line_id[]" value="0" type="hidden" />
            	<input placeholder="Mô tả..." type="text" name="description[]" style="width:100%" />
			</td>
            <td>
            	<input 
            		placeholder="Số tiền..." 
            		onchange="calTotalPricePay()"
            		onkeyup="calTotalPricePay()"
            		type="text" name="money[]" 
            		style="width:100%" 
            		required="required" />
            </td>
			<td>
            	<a class="delete_order_line" onclick="deleteOrderLine( this )">
                	<i class="glyphicon glyphicon-trash"></i>
                </a>
            </td>
		</tr>
	</table>
</div>
<?php
/*end of file AddNew.php*/
/*location: AddNew.php*/