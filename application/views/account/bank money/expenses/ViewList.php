<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Phiếu chi</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span> 
                    <span class="note_id icon-search non-display">Số phiếu
                        <a href="#" id="note_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="customer_name icon-search non-display">Tên đối tác
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_number_sale icon-search non-display">Lý do
                        <a href="#" id="order_number_sale" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng chi
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="account_payment icon-search non-display">TK thanh toán
                        <a href="#" id="account_payment" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="status icon-search non-display">Trạng thái
                        <a href="#" id="status" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php 
                	echo System::$config->baseUrl
                		. "ke-toan/phieu-chi/them-moi"
 						. System::$config->urlSuffix; 
                ?>">
                	<button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php 
	                System::$load->view(
		                "includes/Pagination", array(
		                "data" => array(
				                "pages" => $data['pages'],
				                "page" => $data['page'],
				                "dataUrl" => "ke-toan/phieu-chi/trang/"
		                	)
		                )
		            )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <th>
                        STT
                    </th>
                    <th class="search-order">
                        <label>Ngày tháng
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
		            <div class="search_date search_date_special search-infor non-display" style="text-align: center">
		                Từ ngày
		                <br/>
		                <input type="date" name="date-from">
		                <br/>
		                Đến ngày
		                <br/>
		                <input type="date" name="date-from">
		            </div>
		            </th>
		            <th class="search-order">
		                <label>Số phiếu
		                    <a id="note_id" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="note_id" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Tên khách hàng
		                    <a id="customer_name" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="customer_name" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Tổng chi
		                    <a id="total_price" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="total_price" class="search-detail search-infor non-display"/>
		            </th> 
		            <th class="search-order">
		                <label>TK thanh toán
		                    <a id="account_payment" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="account_payment" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Trạng thái
		                    <a id="status" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="status" class="search-detail search-infor non-display"/>
		            </th>
		            <th></th>
            	</tr>
            </thead>
            <tbody>
            <?php
            	if(isset($data['expenses']) && !empty($data['expenses'])) {
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ($data['expenses'] as $item) {
            				$data['startList'] ++;
	            			?>
	            			<tr>
			                    <td>
			                        <?php echo $data['startList'] ?>
			                    </td>
			                    <td><?php 
			                    	if(isset($item['expense_date']) && $item['expense_date'] > 0) {
			                    		echo date("d-m-Y", $item['expense_date']);
			                    	}
			                    ?></td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ke-toan/phieu-chi/chinh-sua/"
 										. $item['expense_id']
			                    		. System::$config->urlSuffix;
			                    ?>" title=""><?php
			                    	echo $item['expense_code'];
			                    ?></a></td>
			                    <td><a target="_blank" href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ql-khach-hang/khach-hang/chinh-sua/"
 										. $item['partner_id']
			                    		. System::$config->urlSuffix;
			                    ?>" title=""><?php 
			                    	echo $item['partner_name'];
			                    ?></a></td>
			                    <td><?php 
			                    	echo $item['total_price'];
			                    ?></td>
			                    <td><?php 
			                    	echo $item['pay_type_name'];
			                    ?></td>
			                    <td><?php 
			                    	echo $item['pay_status_name'];
			                    ?></td>
			                    <td>
			                    	<a
			                    		class="delete_order_line" 
			                    		onclick="return confirm('Bạn có chắc chắn muốn xóa phiếu thanh toán này không?')" 
				                    	href="<?php 
				                    		echo System::$config->baseUrl
				                    			. "ke-toan/phieu-chi/xoa/"
 												. $item['expense_id']
				                    			. System::$config->urlSuffix;
				                    	?>">
			                             <i class="glyphicon glyphicon-trash"></i>
			                       </a>
			                    </td>
			                </tr>
	            			<?php
            			}
            		}
            	?>
            </tbody>
        </table>
    </div>
</div>
<?php
/*end of file ViewList.php*/
/*location: ViewList.php*/