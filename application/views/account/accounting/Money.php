<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>TK tiền mặt</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
				
            </div> 
            <div class="nav-right">
				<?php 
	                System::$load->view(
		                "includes/Pagination", array(
		                "data" => array(
		                "pages" => $data['pages'],
		                "page" => $data['page'],
		                "dataUrl" => "ke-toan/tien-mat/trang/"
		                		)
		                )
		            )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order ">
        <table class="table table-striped table-bordered tbl-main-product tbl-main tbl-input-warehouse tbl-account" style="max-height: 620px;">
            <thead>
                <tr>
                    <th>
                    	STT
                    </th>
                    <th>Ngày tháng</th>
                    <th>Số phiếu</th>
                    <th>Đối tác</th>
                    <!-- 
                    <th>Lý do</th>
                     -->
                    <th>Tổng tiền</th>
                    <th>TK thanh toán</th>
                    <th>Tình trạng</th>
                </tr>
            </thead>
            <tbody>
            	<?php
					$totalPrice = 0;
            		if(isset($data['money']) && !empty($data['money'])) {
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ($data['money'] as $item) {
            				$data['startList'] ++;
            				if($item['type'] == "receipt") {
            					$totalPrice += $item['total_price'];
            				} else {
            					$totalPrice -= $item['total_price'];
            				}
            				?>
            					<tr>
				                    <td><?php 
				                    	echo $data['startList'];
				                    ?></td>
				                    <td><?php
				                    	if(isset($item['pay_date']) && $item['pay_date'] > 0) {
				                    		echo date("d/m/Y", $item['pay_date']);
				                    	}
				                    ?></td>
				                    <td><a href="<?php 
				                    	$url = "ke-toan/phieu-chi/chinh-sua/";
										if($item['type'] == "receipt") {
											$url = "ke-toan/phieu-thu/chinh-sua/";
										} elseif($item["type"] == "supplier_pay") {
											$url = "ke-toan/thanh-toan-nha-cung-cap/chinh-sua/";
										} elseif($item["type"] == "customer_pay") {
											$url = "ke-toan/khach-hang-thanh-toan/chinh-sua/";
										}
				                    	echo System::$config->baseUrl
				                    		 . $url
				                    		 . $item['pay_id']
				                    		 . System::$config->urlSuffix;
				                    ?>" title=""><?php 
				                    	echo $item['pay_code']
				                    ?></a></td>
				                    <td><a href="<?php 
				                    	echo System::$config->baseUrl
				                    		. "ql-khach-hang/khach-hang/chinh-sua/"
				                    		. $item['customer_id']
				                    		. System::$config->urlSuffix;
				                    ?>" title=""><?php 
				                    	echo $item['partner_name'];
				                    ?></a></td>
				                    <!-- 
				                    <td><?php 
				                    	
				                    ?></td>
				                    -->
				                    <td><?php 
				                    	echo $data['mMoney']->addDotToMoney($item['total_price']);
				                    ?></td>
				                    <td><?php 
				                    	echo $item['pay_type_name']
				                    ?></td>
				                    <td><?php 
				                    	echo $item['pay_status_name']
				                    ?></td>
				                </tr>
            				<?php
            			}
            		}
            	?>
                <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;" class="totalPrice hidden"><?php 
                    	echo $data['mMoney']->addDotToMoney($totalPrice);
                    ?></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                </tr>
            </tbody>
        </table> 
        <div class="account-footer">
            <div class="account-footer-left">
                    <label>Đầu kỳ</label>
                    <input class="startSession" onkeyup="processSession()" type="number" name="start"/>
            </div>
            <div class="account-footer-center">
                    <label>Phát sinh trong kỳ</label>
                    <i><b><?php 
                    	echo $data['mMoney']->addDotToMoney($totalPrice);
                    ?></i></b>
            </div>
            <div class="account-footer-right">
                    <label>Cuối kỳ</label>
                    <input class="endSession" type="text" name="end"/>
            </div>
        </div>
    </div>
</div>
<?php
/*end of file Money.php*/
/*location: Money.php*/