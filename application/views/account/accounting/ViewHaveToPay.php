<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Tài khoản phải trả nhà cung cấp</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="date icon-search non-display">Ngày tháng
                        <a href="#" id="date" class="glyphicon glyphicon-remove img-circle"></a>
                    </span> 
                    <span class="note_id icon-search non-display">Số phiếu
                        <a href="#" id="note_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="customer_name icon-search non-display">Tên khách hàng
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="order_number_sale icon-search non-display">Số đơn bán hàng
                        <a href="#" id="order_number_sale" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="total_price icon-search non-display">Tổng tiền
                        <a href="#" id="total_price" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="account_payment icon-search non-display">TK thanh toán
                        <a href="#" id="account_payment" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="status icon-search non-display">Trạng thái
                        <a href="#" id="status" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
            </div> 
            <div class="nav-right">
                <?php 
	                System::$load->view(
		                "includes/Pagination", array(
		                "data" => array(
		                "pages" => $data['pages'],
		                "page" => $data['page'],
		                "dataUrl" => "ke-toan/tai-khoan-phai-tra-nha-cung-cap/trang/"
		                		)
		                )
		            )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main tbl-input-warehouse">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        <label>Ngày tháng
                            <a id="date" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
		            <div class="search_date search_date_special search-infor non-display" style="text-align: center">
		                Từ ngày
		                <br/>
		                <input type="date" name="date-from">
		                <br/>
		                Đến ngày
		                <br/>
		                <input type="date" name="date-from">
		            </div>
		            </th>
					<th class="search-order">
						<label>Tk phải trả
							<a id="order_number_sale" href="#">
								<img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
							</a>
						</label>
						<br/>
						<input type="text" name="order_number_sale" class="search-detail search-infor non-display"/>
					</th>
		            <th class="search-order">
		                <label>Nhà cung cấp
		                    <a id="customer_name" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="customer_name" class="search-detail search-infor non-display"/>
		            </th>

		            <th class="search-order">
		                <label>Tổng đơn hàng
		                    <a id="total_price" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="total_price" class="search-detail search-infor non-display"/>
		            </th> 
		            <th class="search-order">
		                <label>Số tiền đã thanh toán
		                    <a id="account_payment" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="account_payment" class="search-detail search-infor non-display"/>
		            </th>
		            <th class="search-order">
		                <label>Số tiền phải thanh toán
		                    <a id="status" href="#">
		                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
		                    </a>
		                </label>
		                <br/>
		                <input type="text" name="status" class="search-detail search-infor non-display"/>
		            </th>
            	</tr>
            </thead>
            <tbody>
                <?php 
            		if(isset($data['haveTopReceipt']) && !empty($data['haveTopReceipt'])) {
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ( $data['haveTopReceipt'] as $item ) {
							$item = (array) $item;
            				$data['startList'] ++;
            				?>
			                <tr>
			                    <td>
			                        <?php echo $data['startList']; ?>
			                    </td>
			                    <td><?php if($item['import_date'] > 0) echo date('d/m/Y', $item['import_date']); ?></td>
			                    <td><a href="<?php
									if($item['status_id'] == 6) {
										echo System::$config->baseUrl
												. "ke-toan/phieu-chi/chinh-sua/"
												. $item['import_id']
												. System::$config->urlSuffix;
									} else
			                    	echo System::$config->baseUrl
			                    		. "mua-hang/nhap-kho/chinh-sua/"
			                    		. $item['import_id']
			                    		. System::$config->urlSuffix;
			                    ?>" title="" >
			                    	<?php echo $item['import_code'] ?>
			                    </a></td>
			                    <td><a target="_blank" href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "mua-hang/nha-cung-cap/chinh-sua/"
			                    		. $item['supplier_id']
			                    		. System::$config->urlSuffix;
			                    ?>" title=""><?php 
			                    	echo $item['supplier_name']
			                    ?></a></td>
								<td><?php echo $data['money']->addDotToMoney($item['total_sale']); ?></td>
								<td><?php echo $data['money']->addDotToMoney($item['amount_paid']); ?></td>
								<td><?php echo $data['money']->addDotToMoney($item['have_to_pay']); ?></td>
			                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewHaveToPay.php*/
/*location: ViewHaveToPay.php*/