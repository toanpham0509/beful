<?php
use 			BKFW\Bootstraps\System;
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Quản lý tài khoản ngân hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php echo System::$config->baseUrl; ?>ke-toan/tai-khoan-ngan-hang/them-moi"><button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">Tạo mới</button></a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php 
	                System::$load->view(
			                "includes/Pagination", array(
			                "data" => array(
			                "pages" => $data['pages'],
			                "page" => $data['page'],
			                "dataUrl" => "ke-toan/tai-khoan-ngan-hang/trang/"
		                	)
		                )
		            )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên tài khoản</th>
                    <th>Số tài khoản</th>
                    <th>Ngày mở TK</th>
                    <th>Tên ngân hàng</th>
                    <th>Địa chỉ ngân hàng</th>
                    <th>Trạng thái hoạt động</th> 
                    <th></th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            		if(isset($data['bankAccounts']) && !empty($data['bankAccounts'])) {
            			$data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
            			foreach ($data['bankAccounts'] as $account) {
            				$data['startList']++;
            				?>
            				<tr>
			                    <td><?php echo $data['startList'] ?></td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ke-toan/tai-khoan-ngan-hang/chinh-sua/"
    									. $account['account_id']
			                    		. System::$config->urlSuffix;
			                    ?>"><?php 
			                    	echo $account['account_name'];
			                    ?></a></td>
			                    <td><a href="<?php 
			                    	echo System::$config->baseUrl
			                    		. "ke-toan/tai-khoan-ngan-hang/chinh-sua/"
    									. $account['account_id']
			                    		. System::$config->urlSuffix;
			                    ?>"><?php 
			                    	echo $account['account_number'];
			                    ?></a></td>
			                    <td><?php 
			                    	echo date("d/m/Y", $account['open_date']);
			                    ?></td>
			                    <td><?php 
			                    	echo $account['bank_name'];
			                    ?></td>
			                    <td><?php 
			                    	echo $account['address'];
			                    ?></td>
			                    <td><?php 
			                    	echo $account['pay_status_name'];
			                    ?></td>
			                    <td><a class="delete_order_line" 
			                    	onclick="return confirm('Bạn có chắc chắn muốn xóa không?')" 
			                    	href="<?php 
			                    		echo System::$config->baseUrl
			                    			. "ke-toan/tai-khoan-ngan-hang/xoa/"
    										. $account['account_id']
			                    			. System::$config->urlSuffix
			                    	?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a></td>
			                </tr>
            				<?php
            			}
            		}
            	?>
            </tbody>
        </table> 
    </div>
</div>
