<?php 
use 		BKFW\Bootstraps\System;
?>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Quản lý tài khoản ngân hàng</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input type="submit" name="update" class="btn btn-success" style="color:#fff; border-radius: 7px; margin-right: 5px; width: 100px;" value="Lưu"/>
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "ke-toan/tai-khoan-ngan-hang"
    						. System::$config->urlSuffix; 
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div class="text-danger">
                    <?php
		               	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		            ?>
		            </div>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_product">
            <div class="new_product">
                <div class="branches_info">
                    <h4 style="text-align: left; margin-left: 20px; margin-top: 22px;margin-bottom: 25px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên tài khoản</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="account_name" 
                                    	value="<?php 
                                    		if(isset($data['account']['account_name'])) 
                                    			echo $data['account']['account_name'];
                                    	?>"
                                    	required="required" 
                                    	placeholder="Nhập tên tài khoản." 
                                    	style="width: 100%"/>
                                </div>
                            </td>
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Ngày mở tài khoản</p>
                                </div>
                                <div class="info-post branches_info_right">
                                    <input 
                                    	type="date" 
                                    	required="required"
                                    	name="open_date" 
                                    	style="width: 80%;"
                                    	value="<?php
                                    		if(isset($data['account']['open_date']) ) {
                                    				echo $data['account']['open_date'];
                                    		}
                                    			 
                                    	?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="address" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;"
                                    	value="<?php 
                                    		if(isset($data['account']['address'])) 
                                    			echo $data['account']['address'];
                                    	?>"
                                    	 />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Số tài khoản </p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	placeholder="Số tài khoản..." 
                                    	required="required" 
                                    	name="account_number" 
                                    	style="width: 80%;"
                                    	value="<?php 
                                    		if(isset($data['account']['account_number'])) 
                                    			echo $data['account']['account_number'];
                                    	?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p></p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <select name="district_id" style="width:47%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'districts' ] ) ) {
	                                			foreach ( $data[ 'districts' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->district_id; ?>"
	                                					<?php 
	                                						if( isset($data[ 'account' ]["district_id"]) && $item->district_id == $data[ 'account' ]["district_id"])
	                                							echo "selected='selected'";
	                                					?>>
	                                					<?php echo $item->district_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="city_id" style="width:30%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'cities' ] ) ) {
	                                			foreach ( $data[ 'cities' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->city_id; ?>"
	                                					<?php 
	                                    					if( isset($data[ 'account' ]["city_id"]) && $item->city_id == $data[ 'account' ]["city_id"] )
	                                    						echo "selected='selected'";
	                                    				?> 
	                                					>
	                                					<?php echo $item->city_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
	                                <select name="country_id" style="width:20%; padding-left: 5px; ">
	                                	<?php 
	                                		if( isset( $data[ 'countries' ] ) ) {
	                                			foreach ( $data[ 'countries' ] as $item ) {
	                                				?>
	                                				<option 
	                                					value="<?php echo $item->country_id; ?>"
	                                					<?php 
	                                    					if( isset($data[ 'account' ]["country_id"]) && $item->country_id == $data[ 'account' ]["country_id"] )
	                                    						echo "selected='selected'";
	                                    				?> 
	                                					>
	                                					<?php echo $item->country_name ?>
	                                				</option>
	                                				<?php
	                                			}
	                                		}
	                                	?>
	                                </select>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div>
                                    <p>Ngân hàng</p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	placeholder="Ngân hàng..." 
                                    	required="required" 
                                    	name="bank_name" 
                                    	style="width: 80%;"
                                    	value="<?php 
                                    		if(isset($data['account']['bank_name'])) 
                                    			echo $data['account']['bank_name'];
                                    	?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div>
                                    <p>Liên hệ</p>
                                </div>
                                <div class="info-post">
                                    <input 
                                    	type="text" 
                                    	name="phone" 
                                    	placeholder="Số điện thoại" 
                                    	style="width: 49%;"
                                    	value="<?php 
                                    		if(isset($data['account']['phone'])) 
                                    			echo $data['account']['phone'];
                                    	?>" />
                                    <input 
                                    	type="text" 
                                    	name="fax" 
                                    	placeholder="Fax" 
                                    	style="width: 49%;"
                                    	value="<?php 
                                    		if(isset($data['account']['fax'])) 
                                    			echo $data['account']['fax'];
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                            	<div>
                                    <p>Tình trạng hoạt động</p>
                                </div>
                                <div class="info-post">
                                    <select type="text" name="account_status" style="width: 80%;">
                                    	<option value="5" <?php 
                                    		if(isset($data['account']['account_status']) 
                                    			&& $data['account']['account_status'] == 5) {
                                    			echo "selected='selected'";
                                    		}
                                    	?>>Hoạt động</option>
                                    	<option value="6" <?php 
                                    		if(isset($data['account']['account_status']) 
                                    			&& $data['account']['account_status'] == 6) {
                                    			echo "selected='selected'";
                                    		}
                                    	?>>Không hoạt động</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr style="border:1px silver solid; margin-left: 20px; margin-right: 20px; margin-top: 50px;"/>
                <div class="clear"></div>
                <div 
                	class="product_detail_info product_detail_info_import" 
                	style="margin-left: 20px;margin-top: 40px;margin-bottom: 50px;">
                    <br/>
                    <textarea name="note" placeholder="Các ghi chú bổ sung"><?php 
                    	if(isset($data['account']['note']))
                    		echo $data['account']['note'];
                    ?></textarea>
                </div>
            </div>
        </div>
    </form>
</div>
