<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div id="pay" data-value="1"></div>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Khách hàng thanh toán</h4><h4>/ Chỉnh sửa</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	name="save"
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;"
                        onclick="return checkPayMoney()"
                    	value="Lưu"/>
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "ke-toan/khach-hang-thanh-toan"
                    		. System::$config->urlSuffix; 
                    ?>">
                    	<input 
                    		type="button"  
                    		class="btn btn-warning" 
                    		style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    		value="Bỏ qua" />
                    </a>
                    <div>
			                <?php 
			                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
			                ?>
		            </div>
                </div>
                <div class="nav-center">
                	<?php
                		System::$load->view("includes/PrintConfirm", array(
                			"urlPrint" => $data['urlPrint'],
                			"urlConfirm" => System::$config->baseUrl
							. "ke-toan/khach-hang-thanh-toan/xac-nhan/"
					        . $data[ 'customerPay' ]['pay_id']
					        . System::$config->urlSuffix
                		));
                	?>
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 678px;">
            <div class="new_product" style="border-color: gray">
                <div class="form-new-order form-buy-new-order " >
                    <h4 style="text-align: left;font-weight: bold; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên khách hàng</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <div class="tp-search-box">
                                    	<input type="hidden" name="base_url" value="<?php 
                            				echo System::$config->baseUrl . "ajax/customer/getCustomer/"
                            			?>" />
                                        <input 
                                        	required="required"
                                        	class="input-search" 
                                        	placeholder="Tên hoặc mã số khách hàng..."
                                        	value="<?php 
                                        		echo $data[ 'customerPay' ]['partner_name'];
                                        	?>" />
                                        <div class="result">
                                        	<a class="closeSearch">Đóng</a>
                                        	<?php
											if (isset($data['customers'])) {
												foreach ($data['customers'] as $customer) {
													$prefix = " (".
                                                        		$customer->customer_id
                                                        		. ") - ";
													if( isset( $customer->phone ) ) {
														$prefix .= $customer->phone;
													}
													?>
													<div 
                                                    	class="item-result" 
                                                    	data-name="<?php echo unicode_str_filter(
                                                    		strtolower ( 
                                                    			$customer->customer_name
                                                    			. $prefix
                                                    		)
                                                    	); ?>"
                                                        data-id="<?php echo $customer->customer_id; ?>"
                                                        data-display="<?php 
                                                        	echo $customer->customer_name . $prefix
                                                        ?>" 
                                                        data-input-name="customer_id"
                                                        >
                                                        <?php echo $customer->customer_name . $prefix ?>
                                                    </div> 
													<?php
												}
											}
											?>
                                        </div>
	                                        <input name="customer_id" onchange="addOrderToPay(this)" type="hidden" value="<?php 
			                                	echo $data[ 'customerPay' ]['customer_id'];
			                                ?>" />
                                    </div>
                                </div>
                            </td>
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left" style="width: 40%">
                                    <p>Số phiếu</p>
                                </div>
                                <div class="info-post branches_info_right" style="width: 60%">
                                    <input type="text" disabled="disabled"  style="width: 100%;" value="<?php 
                                    	if( isset( $data['customerPay']['pay_code'] ) ) echo $data['customerPay']['pay_code']; 
                                    ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="address"
                                    	disabled="disabled" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;"
                                    	value="<?php 
                                    		echo $data['customerPay']['partner_address'];
                                    	?>" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Phương thức thanh toán</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <select name="pay_type_id" style="width: 100%;">
                                        <?php 
                                        if( isset( $data[ 'payTypes' ] ) && !empty( $data['payTypes'] ) ) { 
                                        	foreach ( $data[ 'payTypes' ] as $payType ) {
                                        		?>
                                        		<option value="<?php echo $payType['pay_type_id'] ?>" <?php 
                                        			if( $data['customerPay']['pay_type_id'] == $payType['pay_type_id'] ) echo "selected='selected'";
                                        		?>><?php 
                                        			echo $payType['pay_type_name'];
                                        		?></option>
                                        		<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Mã số thuế</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input type="text" disabled="disabled" name="tax_code" value="<?php 
                                    	echo $data['customerPay']['partner_tax_code'];
                                    ?>" style="width:100%; padding-left: 5px; "/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Số tiền thanh toán</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <input type="number" value="<?php 
                                    	echo $data['customerPay']['pay_money'];
                                    ?>" name="pay_money" style="width: 100%;" required="required" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div>
                                    <p>Liên hệ</p>
                                </div>
                                <div class="info-post">
                                    <input type="text" name="phone" value="<?php 
                                    	echo $data['customerPay']['partner_phone_number'];
                                    ?>" disabled="disabled" placeholder="Số điện thoại" style="width: 49%;"/>
                                    <input type="text" name="fax" disabled="disabled" placeholder="Fax" style="width: 49%;" value="<?php 
                                    	echo $data['customerPay']['partner_fax'];
                                    ?>"/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Ngày tháng</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <input type="date" name="pay_date" style="width: 100%;" value="<?php 
                                    	echo date('Y-m-d', $data['customerPay']['pay_date']);
                                    ?>"/>
                                </div>
                            </td>
                        </tr>
                    </table>  
                </div>
                <div class="order-detail buy-order-detail" style="margin-bottom: 130px;">
                    <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                    <table class="table tbl-order-detail">
                        <thead>
                            <tr style="background: #E6E7E8">
                                <th>Số đơn hàng</th>
                                <th>Ngày đơn hàng</th>
                                <th>Số tiền phải thanh toán</th>
                                <th>Số tiền còn lại</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            	<?php 
                            		if( isset( $data['customerPayLines'] ) && !empty( $data['customerPayLines'] ) ) {
                            			$i = 0;
                            			foreach ( $data['customerPayLines'] as $item ) {
                            				$i++;
                            				?>
                            				<tr>
				                                <td>
				                                    <div class='text-order-code'><a target="_blank" href="<?php 
				                                    	echo System::$config->baseUrl
				                                    		. "ban-hang/don-hang/chinh-sua/"
 															. $item['order_id']
				                                    		. System::$config->urlSuffix
				                                    ?>"><?php echo $item['order_code'] ?></a></div>
				                                    <input type="hidden" name="pay_line_id[]" value="<?php 
				                                    	echo $item['pay_line_id'];
				                                    ?>" />
				                                    <input type="hidden" name="order_id[]" value="<?php 
				                                    	echo $item['order_id'];
				                                    ?>"/>
				                                </td>
				                                <td>
				                                    <div class='text-order-date'><?php 
				                                    	if( $item['order_date'] > 0 ) echo date("d/m/Y", $item['order_date']);
				                                    ?></div>
				                                </td>
				                                <td>
				                                    <div class='text-order-total-sale'>
				                                    <?php 
				                                    	$item['order_price'] = (int)(100 - $item['discount']) * $item['order_price'] / 100;
				                                    ?>
				                                    <input type="hidden" value="<?php echo $item['have_to_pay']; ?>" name="haveToPay[]" />
				                                    <?php 
				                                    	echo $data['mMoney']->addDotToMoney($item['have_to_pay']);
				                                    ?></div>
				                                </td>
				                                <td>
				                                    <input type="hidden" name="amount_paid[]" value="<?php 
				                                    	echo $item['amount_paid'];
				                                    ?>" />
				                                    <div class="text-money-owed"><?php
				                                    if( $item['have_to_pay'] > $item['amount_paid'])
				                                    	echo $data['mMoney']->addDotToMoney($item['have_to_pay'] - $item['amount_paid']);
				                                    else
				                                    	echo "0";
				                                    ?></div>
				                                </td>
				                                <td>
				                                   	<a class="delete_order_line" onclick="deleteOrderLine(this)">
				                                            <i class="glyphicon glyphicon-trash"></i>
				                                        </a>
				                                </td>
				                            </tr>
                            				<?php
                            			}
                            		}
                            	?>
                             <!-- 
                            <tr style="height: 50px; background: #E6E7E8;">
                                <td colspan="9" style="line-height: 50px;border-right: none;"><a><h4 style="color: #2DAE4A; text-align: left;">Thêm đơn hàng</h4></a></td>
                            </tr>
                            <tr style="height: 50px; ">
                                <td colspan="9" style="border-right: none;"></td>
                            </tr>
                            <tr style="height: 50px; background: #E6E7E8;">
                                <td colspan="9" style="border-right: none;"></td>
                            </tr>
                            -->
                        </tbody>
                    </table>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
/*end of file Edit.php*/
/*location: Edit.php*/