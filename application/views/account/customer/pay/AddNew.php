<?php
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div id="pay" data-value="1"></div>
<div class="content">
    <form action="" method="post">
        <div class="nav-ban-hang">
            <div class="line">
                <h4 style="color: #2DAE4A;">Khách hàng thanh toán</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                    <input 
                    	type="submit" 
                    	class="btn btn-success" 
                    	name="save"
                    	style="color:#fff; border-radius: 7px; margin-right: 5px; width: 80px;"
                        onclick="return checkPayMoney()"
                    	value="Lưu"/>
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "ke-toan/khach-hang-thanh-toan"
                    		. System::$config->urlSuffix; 
                    ?>">
                    	<input 
                    		type="button"  
                    		class="btn btn-warning" 
                    		style="color:#fff; border-radius: 7px; margin-right: 5px;" 
                    		value="Bỏ qua" />
                    </a>
                </div>
                <div class="nav-center">
                </div>
                <div class="nav-right">
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order tbl-input-warehouse" style="height: 678px;">
            <div class="new_product" style="border-color: gray">
                <div class="form-new-order form-buy-new-order " >
                    <h4 style="text-align: left;font-weight: bold; margin-left: 20px;">THÔNG TIN CƠ BẢN</h4>
                    <table>
                        <tr>
                            <td class="order-left branches_left" style="padding-top: 10px;">
                                <div class="branches_info_left">
                                    <p>Tên khách hàng</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <?php 
	                            		System::$load->view(
	                            			"includes/SearchCustomer",
	                            			array( "data" => array( "customers" => $data['customers'] ) )
	                            		);
	                            	?>
                                </div>
                            </td>
                            <td class="order-right branches_right" style="padding-top: 10px;">
                                <div class="branches_info_left" style="width: 40%">
                                    <p>Số phiếu</p>
                                </div>
                                <div class="info-post branches_info_right" style="width: 60%">
                                    <input type="text" disabled="disabled"  style="width: 100%;" value="<?php 
                                    	if( isset( $data['customerPay']['pay_code'] ) ) echo $data['customerPay']['pay_code']; 
                                    ?>" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Địa chỉ</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input 
                                    	type="text" 
                                    	name="address"
                                    	disabled="disabled" 
                                    	placeholder="Địa chỉ" 
                                    	style="width: 100%;" />
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Phương thức thanh toán</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <select name="pay_type_id" style="width: 100%;">
                                        <?php 
                                        if( isset( $data[ 'payTypes' ] ) && !empty( $data['payTypes'] ) ) { 
                                        	foreach ( $data[ 'payTypes' ] as $payType ) {
                                        		?>
                                        		<option value="<?php echo $payType['pay_type_id'] ?>"><?php 
                                        			echo $payType['pay_type_name'];
                                        		?></option>
                                        		<?php
                                        	}
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div class="branches_info_left">
                                    <p>Mã số thuế</p>
                                </div>
                                <div class="info-post  branches_info_right">
                                    <input type="text" disabled="disabled" name="tax_code" style="width:100%; padding-left: 5px; "/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Số tiền thanh toán</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <input type="number" name="pay_money" style="width: 100%;" required="required" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="order-left branches_left">
                                <div>
                                    <p>Liên hệ</p>
                                </div>
                                <div class="info-post">
                                    <input type="text" name="phone" disabled="disabled" placeholder="Số điện thoại" style="width: 49%;"/>
                                    <input type="text" name="fax" disabled="disabled" placeholder="Fax" style="width: 49%;"/>
                                </div>
                            </td>
                            <td class="order-right branches_right">
                                <div style="width: 40%">
                                    <p>Ngày tháng</p>
                                </div>
                                <div class="info-post" style="width: 60%">
                                    <input type="date" name="pay_date" style="width: 100%;" value="<?php 
                                    	echo date('Y-m-d', time());
                                    ?>"/>
                                </div>
                            </td>
                        </tr>
                    </table>  
                </div>
                <div class="order-detail buy-order-detail" style="margin-bottom: 130px;">
                    <h4 style="text-align: left; font-weight: bold; margin-left: 40px; padding-top: 5px;">THÔNG TIN ĐƠN HÀNG</h4>
                    <table class="table tbl-order-detail">
                        <thead>
                            <tr style="background: #E6E7E8">
                                <th>Số đơn hàng</th>
                                <th>Ngày đơn hàng</th>
                                <th>Số tiền phải trả</th>
                                <th>Số tiền còn lại</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="html_add_to_order" style="display: none;">
<table>
	<tr>
        <td>
            <div class="stt"></div>
            <input type="hidden" name="pay_line_id[]" value="0" />
        </td>
        <td>
            <input type="hidden" name="order_id[]"/>
            <div class='text-order-code'></div>
        </td>
        <td>
            <div class='text-order-date'></div>
        </td>
        <td>
            <div class='text-order-total-sale'></div>
        </td>
        <td>
            <input type="number" name="amount_paid[]" />
        </td>
        <td>
            <a class="delete_order_line" onclick="deleteOrderLine(this)">
                    <i class="glyphicon glyphicon-trash"></i>
                </a>
        </td>
    </tr>
</table>
</div>
<?php
/*end of file AddNew.php*/
/*location: AddNew.php*/