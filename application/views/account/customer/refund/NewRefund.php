<?php
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<div class="content">
	<form action="" method="post">
        <div class="nav-ban-hang">
        	<div class="line">
                <h4 style="color: #2DAE4A;">Chuyển kho</h4><h4>/ Mới</h4>
                <div class="search-ban-hang">
                    <div>
                        <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                        <input type="text" name="search" id="search" class="search" />
                    </div>
                </div>
            </div>
            <div class="line">
                <div class="nav-left">
                	<a href="<?php 
                    	echo System::$config->baseUrl
                    		. "mua-hang/nhap-kho/them-moi"
 							. System::$config->urlSuffix
                    		. "?refund=1"; 
                    ?>" class="btn btn-success" style="color:#fff; border-radius: 7px; margin-right: 5px;">Thêm mới trả về</a>
                    <a href="<?php 
                    	echo System::$config->baseUrl
                    		. "mua-hang/tra-ve"
 							. System::$config->urlSuffix; 
                    ?>" class="btn btn-warning" style="color:#fff; border-radius: 7px; margin-right: 5px;">Bỏ qua</a>
                    <div>
		                <?php 
		                	if( isset( $data[ 'message' ] ) ) echo "<br />" . $data[ 'message' ] . "<br />";
		                ?>
	                </div>
                </div>
            </div>
        </div>
        <div class="main_new_order main_buy_new_order">
        </div>
    </form>
	<div class="" style="padding: 20px;">
		<h4>HÀNG TRẢ VỀ:</h4>
		Tương tự phần CHUYỂN KHO, cách thực hiện nghiệp vụ HÀNG TRẢ VỀ như sau:
		<br />
		<ul>
			<li>Giả thiết: khách hàng trả lại 03 sản phẩm X1, X2, X3 cho cửa hàng Y. 
			Sản phẩm này, cửa hàng Y đã bán ở các ouput_line_id là Z1, Z2, Z3. 
			Lưu ý là một hóa đơn bán hàng có thể có nhiều dòng, mỗi dòng có id là output_line_id. 
			3 sản phẩm X1, X2, X3 có thể thuộc các đơn bán hàng khác nhau. 
			Cửa hàng Y nhận hàng X, trả lại tiền khách hàng – có thể trả lại 100% hoặc tính phí, 
			cái này để người người thực hiện nhập liệu tự nhập số tiền cho chủ động.</li>
			<li>Giải pháp: Để đơn giản vấn đề, có thể quy ước việc trả hàng là hoàn toàn 
			tương tự việc shop mua 3 product, trong đó người bán chính là khách hàng đã 
			mua sản phẩm X1, X2, X3.</li>
		</ul>
		<br />
		<h4>Trình tự thực hiện:</h4>
		<ol>
			<li>Bước 1: Làm thủ tục mua hàng (nhập kho) 03 sản phẩm X1, X2, X3.  
			Mỗi sản phẩm là 1 dòng trong phần chi tiết đơn mua hàng. Mỗi dòng này
			sẽ có ID tự sinh là ID1, ID2, ID3. Khi lựa chọn Supplier từ dropdown 
			list, sẽ chọn supplier tên là HÀNG TRẢ VỀ.</li>
			<li>Bước 2: tương ứng với 3 dòng có ID là I1, I2, I3, thêm 03 bản ghi trong bảng product_refund. Mỗi bản ghi:
		+ refund_id: tự sinh
		+ output_line_id : Z1 hoặc Z2 hoặc Z3
		+ input_line_id: ID1 hoặc ID2 hoặc ID3</li>
		</ol>
	</div>
</div>
<?php
/*end of file NewRefund.php*/
/*location: NewRefund.php*/