<?php
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
?>
<table class="table order_line tbl-order-detail">
	<thead>
		<tr style="background: #E6E7E8">
                            <th style="width: 10%">Mã SP</th>
                            <th style="width: 15%">Tên sản phẩm</th>
                            <th style="width: 10%">Số lượng</th>
                            <th style="width: 10%">Đơn vị tính</th>
                            <th style="width: 15%">Đơn giá</th>
                            <th style="width: 15%">Kho</th>
                            <th style="width: 15%">Ngày hết hạn sử dụng</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php 
                    		if( !empty( $saleOrder ) ) : 
                    		foreach ( $saleOrder as $importLine ) :
                    	?>
		                    	<tr>
							        <td style="width: 10%; border-left: none;">
							            <input 
							            	type="hidden" 
							            	name="order_line_id[]" 
							            	value="<?php 
							            		echo $importLine->order_line_id;
							            	?>" />
										<input 
											style="width: 100%" 
											name="product_id[]" 
											type="hidden"
											class="product_id"
											value="<?php 
												echo $importLine->product_id;
											?>" />
										<div class="html_product_id"><?php
											echo $importLine->product_id;
										?></div>
							        </td>
							        <td style="width: 15%">
							            <div class="html_product_name"><?php 
							            	echo $importLine->product_name;
							            ?></div>
							        </td>
							        <td style="width: 10%">
							            <input 
							                style="width: 80%" 
							                type="number" 
							                name="product_amount[]"
							                value="<?php 
							                	echo $importLine->product_amount;
							                ?>"
							                required="required" />
							        </td>
							        <td style="width: 10%">
							           	<div class="html_product_unit"><?php
							           	if( isset( $importLine->product_unit_name ) ) 
							           		echo $importLine->product_unit_name;
							           	elseif( isset( $importLine->product_unit ) ) 
							           		echo $importLine->product_unit;
							           	?></div>
							        </td>
							        <td style="width: 10%">
							            <input 
							            	style="width: 80%" 
							            	type="number" 
							            	name="product_price[]"
							            	required="required" 
							            	value="<?php 
							            		echo $importLine->product_price;
							            	?>" />
							        </td>
							        <td>
							            <select name="warehouse_id[]" style="width: 100%">
							                <?php 
							                    foreach( $data[ 'warehouses' ] as $warehouse ) {
							                        ?>
							                        <option
							                            value="<?php echo $warehouse->warehouse_id ?>"
							                            <?php 
							                            	if( $warehouse->warehouse_id == $importLine->warehouse_id )
							                            		echo "selected='seletected'";
							                            ?>>
							                                <?php echo $warehouse->warehouse_name; ?>
							                            </option>
							                        <?php
							                    }
							                ?>
							            </select>
							        </td>
							        <td style="width: 10%">
							            <input 
							            	style="width: 80%" 
							            	type="date" 
							            	name="expire_date[]"
							            	value="<?php 
							            		if( $importLine->expire_date > 0 )
							            			echo date( "Y-m-d", $importLine->expire_date );
							            	?>" />
							        </td>
							        <!-- 
							        <td style="width: 15%;">
							            <input
							                style="width: 90%" 
							                type="hidden" 
							                name="product_total_price[]" />
							        </td>
							        -->
							        <td style="width: 5%;  border-right: none">
							            <a class="delete_order_line" onclick="deleteOrderLine( this )"><i class="glyphicon glyphicon-trash"></i></a>
							        </td>
							    </tr>
                        <?php endforeach;
							endif;
                        ?>
                    </tbody>
                </table>
<?php
/*end of file ImportLineHtml.php*/
/*location: ImportLineHtml.php*/