<?php

use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");
?>
<div class="content">
    <div class="nav-ban-hang">
        <div class="line">
            <h4>Khách hàng</h4>
            <div class="search-ban-hang">
                <form method="post" action="">
                    <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                    <span class="customer_name icon-search non-display">Tên khách hàng
                        <a href="#" id="customer_name" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_address icon-search non-display">Địa chỉ 
                        <a href="#" id="providers_address" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_phone icon-search non-display">Số fax/Điện thoại 
                        <a href="#" id="providers_phone" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="price_id icon-search non-display">Báo giá
                        <a href="#" id="price_id" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_contact icon-search non-display">Người liên hệ 
                        <a href="#" id="providers_contact" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_telephone icon-search non-display">Số điện thoại 
                        <a href="#" id="providers_telephone" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <span class="providers_email icon-search non-display">Email 
                        <a href="#" id="providers_email" class="glyphicon glyphicon-remove img-circle"></a>
                    </span>
                    <input type="text" name="search" id="search" class="search" />
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="line">
            <div class="nav-left">
                <a href="<?php
                echo System::$config->baseUrl
                . "ql-khach-hang/khach-hang/them-moi"
                . System::$config->urlSuffix;
                ?>">
                    <button class="btn btn-success" style="border-radius: 7px; margin-right: 5px;">
                        Tạo mới
                    </button>
                </a>
                hoặc <a href="" style="padding-left: 5px;">Import</a>
            </div> 
            <div class="nav-right">
                <?php
                System::$load->view(
                        "includes/Pagination", array(
                    "data" => array(
                        "pages" => $data['pages'],
                        "page" => $data['page'],
                        "dataUrl" => "ql-khach-hang/khach-hang/trang/"
                    )
                        )
                )
                ?>
            </div>
        </div>
    </div>
    <div class="main_order">
        <table class="table table-striped table-bordered tbl-main">
            <thead>
                <tr>
                    <th>STT</th>
                    <th class="search-order">
                        <label>Tên khách hàng
                            <br/>
                            <a id="customer_name" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="customer_name" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Địa chỉ
                            <br/>
                            <a id="providers_address" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_address" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Số fax/Điện thoại 
                            <br/>
                            <a id="providers_phone" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_phone" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Báo giá
                            <br/>
                            <a id="price_id" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="price_id" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Người liên hệ 
                            <br/>
                            <a id="providers_contact" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_contact" class="search-detail search-infor non-display"/>
                    </th> 
                    <th class="search-order">
                        <label>Số điện thoại 
                            <br/>
                            <a id="providers_telephone" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_telephone" class="search-detail search-infor non-display"/>
                    </th>
                    <th class="search-order">
                        <label>Email 
                            <br/>
                            <a id="providers_email" href="#">
                                <img src="<?php echo System::$config->baseUrl; ?>images/icon_search.png"/>
                            </a>
                        </label>
                        <br/>
                        <input type="text" name="providers_email" class="search-detail search-infor non-display"/>
                    </th>
                    <th style="min-width: 80px;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($data['customers'])) {
                    $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                    foreach ($data['customers'] as $customer) {
                        $data['startList'] ++;
                        ?>
                        <tr>
                            <td>
                                <?php echo $data['startList'] ?>
                            </td>
                            <td>
                                <?php echo $customer->customer_name ?>
                            </td>
                            <td><?php echo $customer->address ?></td>
                            <td><?php echo $customer->fax . " / " . $customer->phone ?></td>
                            <td><?php ?></td>
                            <td><?php echo $customer->contact_person_name ?></td>
                            <td><?php echo $customer->contact_person_phone ?></td>
                            <td><?php echo $customer->contact_person_email ?></td>
                            <td>
                                <a href="<?php
                                echo System::$config->baseUrl
                                    . "ql-khach-hang/khach-hang/xem/"
                                    . $customer->customer_id
                                    . System::$config->urlSuffix;
                                ?>" title="Xem">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                                <a href="<?php
                                    echo System::$config->baseUrl
                                        . "ql-khach-hang/khach-hang/chinh-sua/"
                                        . $customer->customer_id
                                        . System::$config->urlSuffix;
                                    ?>" title="Chỉnh sửa">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                                <a href="<?php
                                    echo System::$config->baseUrl
                                        . "ql-khach-hang/khach-hang/xoa/"
                                        . $customer->customer_id
                                        . System::$config->urlSuffix;
                                    ?>" title="Xóa">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    </div>
</div>
<?php
/*end of file ViewCustomers.php*/
/*location: ViewCustomers.php*/