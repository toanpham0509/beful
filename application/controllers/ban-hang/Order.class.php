<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Order extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MProduct" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MSession" );
		$this->load->model( "MSalesPeople" );
		$this->load->model( "MCustomer" );
		$this->load->model( "MOrderStatus" );
		$this->load->model( "MShop" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MPrice" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		$this->load->model( "MMenu" );
		$this->load->model( "MMoney" );
		$this->load->model( "MPromote" );
		$this->load->model( "MInventory" );
		$this->load->model( "MImportWarehouse" );
		$this->load->model( "MRefund" );
		
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 7 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page['data'][ 'mMoney' ] = $this->mMoney;
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * Index
	 */
	public function index( $page = 1 ) {
		$this->page[ 'accessID' ] = SALE_ORDER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		
		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1; 
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		if( isset( $_REQUEST[ 'customer_id' ] ) && filter_var( $_REQUEST[ 'customer_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'customer_id' ] = $_REQUEST[ 'customer_id' ];
		}
		if( isset( $_REQUEST[ 'salesperson_id' ] ) && filter_var( $_REQUEST[ 'salesperson_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'salesperson_id' ] = $_REQUEST[ 'salesperson_id' ];
		}
		if( isset( $_REQUEST[ 'salesperson_name' ] ) && strlen( $_REQUEST[ 'salesperson_name' ] ) ) {
			$dataSearch[ 'salesperson_name' ] = strip_tags( $_REQUEST[ 'salesperson_name' ] );
		}
		if( isset( $_REQUEST[ 'time_form' ] ) && preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/", $_REQUEST[ 'time_form' ]) ) {
			$dataSearch[ 'time_form' ] = strtotime( $_REQUEST[ 'time_form' ] );
		}
		if( isset( $_REQUEST[ 'time_to' ] ) && preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/", $_REQUEST[ 'time_to' ]) ) {
			$dataSearch[ 'time_to' ] = strtotime( $_REQUEST[ 'time_to' ] );
		}
		if( isset( $_REQUEST[ 'order_id' ] ) && filter_var( $_REQUEST[ 'order_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'order_id' ] = $_REQUEST[ 'order_id' ]; 
		}
		if( isset( $_REQUEST[ 'order_status_id' ] ) && filter_var( $_REQUEST[ 'order_status_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'order_status_id' ] = $_REQUEST[ 'order_status_id' ];
		}
		
		if( isset( $_REQUEST[ 'order_price_from' ] ) ) {
			$dataSearch[ 'order_price_from' ] = $_REQUEST[ 'order_price_from' ];
		}
		if( isset( $_REQUEST[ 'order_price_to' ] ) ) {
			$dataSearch[ 'order_price_to' ] = $_REQUEST[ 'order_price_to' ];
		}
		if( isset( $_REQUEST[ 'phone' ] ) ) {
			$dataSearch[ 'phone' ] = $_REQUEST[ 'phone' ];
		}
		
		$this->page['data'][ 'orders' ] = $this->mSaleOrder->getOrders( $dataSearch );
		$this->page['data'][ 'orders' ] = $this->page['data'][ 'orders' ]->data;

		if(!empty($this->page['data'][ 'orders' ])) {
			foreach ($this->page['data'][ 'orders' ] as $key => $value) {
				if(!is_object($value)) continue;
				$this->page['data'][ 'orders' ]->$key->order_price = $this->mSaleOrder->getOrderTotalSale(
						$value->order_id
				);
			}
		}
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'orders' ]->order_count;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['paginationPath'] = "ban-hang/don-hang/trang/";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $page
	 */
	public function page( $page ) {
		$this->index( $page );
	}
	/**
	 * Chart
	 */
	public function chart() {
		$this->page[ 'pageTitle' ] = "Biểu đồ";
		$this->page[ 'viewPath' ] = "sale management/order management/ViewChart";
		$this->page['data'][ 'reportSale' ] = $this->mSaleOrder->reportSale();
		
		$this->load->view( "Template", array( "data" =>  $this->page ) );
	}
	public function reportLine() {
		$this->page[ 'pageTitle' ] = "Biểu đồ";
		$this->page[ 'viewPath' ] = "sale management/order management/ViewChartLine";
		$this->page['data'][ 'reportSale' ] = $this->mSaleOrder->reportSaleLine();
		
		$this->load->view( "Template", array( "data" =>  $this->page ) );
	}
	public function reportPie() {
		$this->page[ 'pageTitle' ] = "Biểu đồ";
		$this->page[ 'viewPath' ] = "sale management/order management/ViewChartPie";
		$this->page['data'][ 'reportSale' ] = $this->mSaleOrder->reportSale();
		
		$this->load->view( "Template", array( "data" =>  $this->page ) );
	}
	/**
	 * 
	 */
	public function addNew( $orderId = null ) {
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct( null );
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

		$this->page[ 'accessID' ] = SALE_ORDER_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'viewPath' ] = "sale management/order management/NewOrder";
		$this->page[ 'pageTitle' ] = "Thêm mới đơn hàng";
		if( $orderId == null || $orderId < 0 ) :
			$orderId = $this->page[ 'data' ][ 'orderID' ] = $this->mSaleOrder->addNew(array(
				"customer_id" => null,
				"promote_id" => null,
				"price_id" => null,
				"order_date" => time(),
				"order_lines" => null,
				"order_status_id" => 3,
				"order_price" => null,
				"order_note" => null
			));
			$this->router->router(
				System::$config->baseUrl
				. "ban-hang/don-hang/them-moi/"
				. $orderId
				. System::$config->urlSuffix
			);
		else:
			$this->page[ 'data' ][ 'orderID' ] = $orderId;
		endif;
		
		$this->page['data'][ 'orderCode' ] = $this->mSaleOrder->getOrder( $this->page[ 'data' ][ 'orderID' ] );
		
		//print_r( $this->page[ 'data' ][ 'orderCode' ] );
		
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		$this->page[ 'data' ][ 'orderStatus' ] = $this->mOrderStatus->getAllStatus();
 		$this->page[ 'data' ][ 'shops' ] = $this->mShop->getShopsBySalePerson( $this->page['userInfo']['id'] );
 		if(empty($this->page[ 'data' ][ 'shops' ])) {
 			?>
 			<!-- 
 			<script type="text/javascript">
				alert("Bạn không quản lý cửa hàng nào! Do đó, bạn không thể tạo đơn hàng!");
				window.location.assign("<?php echo System::$config->baseUrl ?>");
			</script>
		 	-->
 			<?php
//  			die();
 		}
// 		print_r($this->page[ 'data' ][ 'shops' ]);
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->page[ 'data' ][ 'orderPrices' ] = $this->mPrice->getPrices( null);

		$this->page[ 'data' ][ 'orderPrices' ] = (isset($this->page[ 'data' ][ 'orderPrices' ]->item_list)) ?
			$this->page[ 'data' ][ 'orderPrices' ]->item_list : null;
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		$this->page[ 'data' ][ 'promotes' ] = $this->mPromote->getAllPromotes();
// 		print_r($this->page[ 'data' ][ 'promotes' ]);
		if( isset( $_POST[ 'save' ] ) ) {
			$errors = array();
			$dataUpdate = array();
			$dataUpdate[ 'order_id' ] = $orderId;
			$dataUpdate['discount_amount'] = $_POST['discount_amount'];
			if( isset( $_POST[ 'shop_id' ] ) && filter_var( $_POST[ 'shop_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'shop_id' ] = $_POST[ 'shop_id' ];
			} else {
				//$errors[ 'shop_id' ] = "shop_id";
			}
			if( isset( $_POST[ 'customer_id' ] ) && filter_var( $_POST[ 'customer_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'customer_id' ] = $_POST[ 'customer_id' ];
			} else {
				$errors[ 'customer_id' ] = "customer_id";
			}
			// 			if( isset( $_POST[ 'address' ] ) && strlen( $_POST[ 'address' ] ) ) {
			// 				$dataUpdate[ 'address' ] = strip_tags( $_POST[ 'address' ] );
			// 			} else {
			// 				$errors[ 'address' ] = "address";
			// 			}
			// 			if( isset( $_POST[ 'district_id' ] ) && filter_var( $_POST[ 'district_id' ], FILTER_VALIDATE_INT ) ) {
			// 				$dataUpdate[ 'district_id' ] = $_POST[ 'district_id' ];
			// 			} else {
			// 				$errors[ 'district_id' ] = "district_id";
			// 			}
			// 			if( isset( $_POST[ 'city_id' ] ) && filter_var( $_POST[ 'city_id' ], FILTER_VALIDATE_INT ) ) {
			// 				$dataUpdate[ 'city_id' ] = $_POST[ 'city_id' ];
			// 			} else {
			// 				$errors[ 'city_id' ] = "city_id";
			// 			}
			// 			if( isset( $_POST[ 'country_id' ] ) && filter_var( $_POST[ 'country_id' ], FILTER_VALIDATE_INT ) ) {
			// 				$dataUpdate[ 'country_id' ] = $_POST[ 'country_id' ];
			// 			} else {
			// 				$errors[ 'country_id' ] = "country_id";
			// 			}
			if( isset( $_POST[ 'price_id' ] ) && filter_var( $_POST[ 'price_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'price_id' ] = $_POST[ 'price_id' ];
			} else {
				$errors[ 'price_id' ] = "price_id";
			}
			// 			if( isset( $_POST[ 'warehouse_id' ] ) && filter_var( $_POST[ 'warehouse_id' ], FILTER_VALIDATE_INT ) ) {
			// 				$dataUpdate[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
			// 			} else {
			// 				$errors[ 'warehouse_id' ] = "warehouse_id";
			// 			}
			if( isset( $_POST[ 'promote_id' ] )
				&& filter_var( $_POST[ 'promote_id' ], FILTER_VALIDATE_INT )
				&& $_POST[ 'promote_id' ] > 0 ) {
				$dataUpdate[ 'promote_id' ] = $_POST[ 'promote_id' ];
			} else {
				$dataUpdate[ 'promote_id' ] = null;
			}
			if( isset( $_POST[ 'salesperson_id' ] ) && filter_var( $_POST[ 'salesperson_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'salesperson_id' ] = $_POST[ 'salesperson_id' ];
			} else {
				$errors[ 'salesperson_id' ] = "salesperson_id";
			}
			if( isset( $_POST[ 'status' ] ) && filter_var( $_POST[ 'status' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'status' ] = $_POST[ 'status' ];
			} else {
				$dataUpdate[ 'status' ] = 2;
			}
			if( isset( $_POST[ 'order_note' ] ) && strlen( $_POST[ 'order_note' ] ) ) {
				$dataUpdate[ 'order_note' ] = strip_tags( $_POST[ 'order_note' ] );
			} else {
			}
			if( isset( $_POST[ 'order_date' ] ) ) {
				$dataUpdate[ 'order_date' ] = time( $_POST[ 'order_date' ] );
			} else {
			}
			if( isset( $_POST[ 'order_discount' ] ) && filter_var( $_POST[ 'order_discount' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'discount' ] = $_POST[ 'order_discount' ];
			} else {
				$dataUpdate[ 'discount' ] = 0;
			}
			//products
			$dataUpdate[ 'order_lines' ] = array();
			$dataUpdate[ 'order_price' ] = 0;
			if( isset( $_POST[ 'order_line_id' ] )
				&& isset( $_POST[ 'product_amount' ] )
				&& isset( $_POST[ 'product_price' ] )
				&& isset( $_POST[ 'product_discount' ] )
				&& isset( $_POST[ 'product_warehouse' ] ) ) {
									
				$oldOrderLines = $this->mSaleOrder->getOrder( $orderId );
				$oldOrderLines = $oldOrderLines->order_lines;
				if( !empty( $oldOrderLines ) ) {
					foreach ( $oldOrderLines as $orderLine ) {
						$c = 0;
						foreach ( $_POST[ 'order_line_id' ] as $new ) {
							if( $new == $orderLine->order_line_id )
								$c++;
						}
						if( $c == 0 ) {
							$this->mSaleOrder->deleteOrderLine( $orderLine->order_line_id );
						}
					}
				}
				$i = 0;
				foreach( $_POST[ 'order_line_id' ] as $orderLineID ) {
					if($_POST['product_id'][$i] == null) continue;
					if( $orderLineID == 0 ) $orderLineID = null;
					for($j = $i+1; $j < sizeof($_POST['order_line_id']); $j++) {
						if($_POST[ 'product_id' ][$j] == $_POST[ 'product_id' ][$i]) {
							$_POST[ 'product_amount' ][ $i ] += $_POST['product_amount'][$j];
							$_POST['product_id'][$j] = null;
						}
					}
					$dataUpdate[ 'order_lines' ][$i] = array(
							"order_line_id" => $orderLineID,
							"product_id"	=> $_POST[ 'product_id' ][$i],
							"product_amount" => $_POST[ 'product_amount' ][ $i ],
							"product_price" => $_POST[ 'product_price' ][ $i ],
							"product_discount" => $_POST[ 'product_discount' ][ $i ],
							"product_tax_id"   => null,
							"warehouse_id"     => $_POST[ 'product_warehouse' ][ $i ]
						);
					if( filter_var( $_POST[ 'product_discount' ][$i], FILTER_VALIDATE_INT )
						&& $_POST[ 'product_discount' ][ $i ] < 100 ) {
						$dataUpdate[ 'order_price' ] += (int)(
								$_POST[ 'product_amount' ][ $i ]
								* $_POST[ 'product_price' ][ $i ]
								* (100 - $_POST['product_discount'][$i]) / 100 );
					} else {
						$dataUpdate[ 'order_price' ] += $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						}
					$i++;
				}
			}
			//if( $dataUpdate[ 'order_discount' ] > 0
			//		&& $dataUpdate[ 'order_discount' ] < 100 ) {
			//		$dataUpdate[ 'order_price' ] = (int)$dataUpdate[ 'order_price' ] * $dataUpdate[ 'order_discount' ] / 100;
			//}
			if( empty( $errors ) ) {
// 				print_r($dataUpdate);
				if( $this->mSaleOrder->update( $orderId, $dataUpdate ) ) {
					
					$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới đơn hàng thành công.</div>";
					$this->router->router(
							System::$config->baseUrl
							. "ban-hang/don-hang/chinh-sua/" . $orderId
							. System::$config->urlSuffix
					);
				} else {
					$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới đơn hàng không thành công.</div>";
				}
			} else {
// 				print_r($errors);
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới đơn hàng không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit order 
	 * 
	 * @param unknown $orderId
	 */
	public function edit( $orderId ) {
		$this->page[ 'accessID' ] = SALE_ORDER_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct( );
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		
		$this->page[ 'viewPath' ] = "sale management/order management/EditOrder";
		$this->page[ 'pageTitle' ] = "Chỉnh sửa đơn hàng";
		
		$this->page[ 'data' ][ 'orderID' ] = $orderId;
		
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->page[ 'data' ][ 'orderStatus' ] = $this->mOrderStatus->getAllStatus();
		$this->page[ 'data' ][ 'shops' ] = $this->mShop->getShopsBySalePerson( $this->page['userInfo']['user_id'] );
 		
		$this->page[ 'data' ][ 'orderPrices' ] = $this->mPrice->getPrices( null);
		if(isset($this->page[ 'data' ][ 'orderPrices' ]->item_list))
			$this->page[ 'data' ][ 'orderPrices' ] = $this->page[ 'data' ][ 'orderPrices' ]->item_list;
		else
			$this->page[ 'data' ][ 'orderPrices' ] = null;
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		$this->page[ 'data' ][ 'promotes' ] = $this->mPromote->getAllPromotes();
		
		if( isset( $_POST[ 'update' ] ) ) {
			$errors = array();
			$dataUpdate = array();
			$dataUpdate[ 'order_id' ] = $orderId;
			$dataUpdate['discount_amount'] = $_POST['discount_amount'];
			if( isset( $_POST[ 'shop_id' ] ) && filter_var( $_POST[ 'shop_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'shop_id' ] = $_POST[ 'shop_id' ];
			} else {
				//$errors[ 'shop_id' ] = "shop_id";
			}
			if( isset( $_POST[ 'customer_id' ] ) && filter_var( $_POST[ 'customer_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'customer_id' ] = $_POST[ 'customer_id' ];
			} else {
				$errors[ 'customer_id' ] = "customer_id";
			}
// 			if( isset( $_POST[ 'address' ] ) && strlen( $_POST[ 'address' ] ) ) {
// 				$dataUpdate[ 'address' ] = strip_tags( $_POST[ 'address' ] );
// 			} else {
// 				$errors[ 'address' ] = "address";
// 			}
// 			if( isset( $_POST[ 'district_id' ] ) && filter_var( $_POST[ 'district_id' ], FILTER_VALIDATE_INT ) ) {
// 				$dataUpdate[ 'district_id' ] = $_POST[ 'district_id' ];
// 			} else {
// 				$errors[ 'district_id' ] = "district_id";
// 			}
// 			if( isset( $_POST[ 'city_id' ] ) && filter_var( $_POST[ 'city_id' ], FILTER_VALIDATE_INT ) ) {
// 				$dataUpdate[ 'city_id' ] = $_POST[ 'city_id' ];
// 			} else {
// 				$errors[ 'city_id' ] = "city_id";
// 			}
// 			if( isset( $_POST[ 'country_id' ] ) && filter_var( $_POST[ 'country_id' ], FILTER_VALIDATE_INT ) ) {
// 				$dataUpdate[ 'country_id' ] = $_POST[ 'country_id' ];
// 			} else {
// 				$errors[ 'country_id' ] = "country_id";
// 			}
			if( isset( $_POST[ 'price_id' ] ) && filter_var( $_POST[ 'price_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'price_id' ] = $_POST[ 'price_id' ];
			} else {
				$errors[ 'price_id' ] = "price_id";
			}
// 			if( isset( $_POST[ 'warehouse_id' ] ) && filter_var( $_POST[ 'warehouse_id' ], FILTER_VALIDATE_INT ) ) {
// 				$dataUpdate[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
// 			} else {
// 				$errors[ 'warehouse_id' ] = "warehouse_id";
// 			}
			if( isset( $_POST[ 'promote_id' ] ) 
				&& filter_var( $_POST[ 'promote_id' ], FILTER_VALIDATE_INT ) 
				&& $_POST[ 'promote_id' ] > 0 ) {
				$dataUpdate[ 'promote_id' ] = $_POST[ 'promote_id' ];
			} else {
				$dataUpdate[ 'promote_id' ] = null;
			}
			if( isset( $_POST[ 'salesperson_id' ] ) && filter_var( $_POST[ 'salesperson_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'salesperson_id' ] = $_POST[ 'salesperson_id' ];
			} else {
				$errors[ 'salesperson_id' ] = "salesperson_id";
			}
// 			if( isset( $_POST[ 'status' ] ) && filter_var( $_POST[ 'status' ], FILTER_VALIDATE_INT ) ) {
// 				$dataUpdate[ 'status' ] = $_POST[ 'status' ];
// 			} else {
// 				$errors[ 'status' ] = "status";
// 			}
			if( isset( $_POST[ 'order_note' ] ) && strlen( $_POST[ 'order_note' ] ) ) {
				$dataUpdate[ 'order_note' ] = strip_tags( $_POST[ 'order_note' ] );
			} else {
			}
			if( isset( $_POST[ 'order_date' ] ) ) {
				$dataUpdate[ 'order_date' ] = time( $_POST[ 'order_date' ] );
			} else {
			}
			if( isset( $_POST[ 'order_discount' ] ) && filter_var( $_POST[ 'order_discount' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'discount' ] = $_POST[ 'order_discount' ];
			} else {
				$dataUpdate[ 'discount' ] = 0;
			}
			//products
			$dataUpdate[ 'order_lines' ] = array();
			$dataUpdate[ 'order_price' ] = 0;
			if( isset( $_POST[ 'order_line_id' ] )
				&& isset( $_POST[ 'product_amount' ] )
				&& isset( $_POST[ 'product_price' ] )
				&& isset( $_POST[ 'product_discount' ] )
				&& isset( $_POST[ 'product_warehouse' ] ) ) {
					
				$oldOrderLines = $this->mSaleOrder->getOrder( $orderId );
				$oldOrderLines = $oldOrderLines->order_lines;
				
				if( !empty( $oldOrderLines ) )
					foreach ( $oldOrderLines as $orderLine ) {
						$c = 0;
						foreach ( $_POST[ 'order_line_id' ] as $new ) {
							if( $new == $orderLine->order_line_id )
								$c++;
					}
					if( $c == 0 ) {
						$this->mSaleOrder->deleteOrderLine( $orderLine->order_line_id );
					}
				}

				$i = 0;
				foreach( $_POST[ 'order_line_id' ] as $orderLineID ) {
					if($_POST['product_id'][$i] == null) continue;
					if( $orderLineID == 0 ) $orderLineID = null;
					for($j = $i+1; $j < sizeof($_POST['order_line_id']); $j++) {
						if($_POST[ 'product_id' ][$j] == $_POST[ 'product_id' ][$i]) {
							$_POST[ 'product_amount' ][ $i ] += $_POST['product_amount'][$j];
							$_POST['product_id'][$j] = null;
						}
					}
					$dataUpdate[ 'order_lines' ][$i] = array(
							"order_line_id" => $orderLineID,
							"product_id"	=> $_POST[ 'product_id' ][$i],
							"product_amount" => $_POST[ 'product_amount' ][ $i ],
							"product_price" => $_POST[ 'product_price' ][ $i ],
							"product_discount" => $_POST[ 'product_discount' ][ $i ],
							"product_tax_id"   => null,
							"warehouse_id"     => $_POST[ 'product_warehouse' ][ $i ]
					);
					if( filter_var( $_POST[ 'product_discount' ][$i], FILTER_VALIDATE_INT )
						&& $_POST[ 'product_discount' ][ $i ] < 100 ) {
						$dataUpdate[ 'order_price' ] += (int)( 
								$_POST[ 'product_amount' ][ $i ] 
								* $_POST[ 'product_price' ][ $i ] 
								* (100 - $_POST['product_discount'][$i]) / 100 );
					} else {
						$dataUpdate[ 'order_price' ] += $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
					}
					$i++;
				}
			}
// 			if( $dataUpdate[ 'order_discount' ] > 0 
// 				&& $dataUpdate[ 'order_discount' ] < 100 ) {
// 				$dataUpdate[ 'order_price' ] = (int)$dataUpdate[ 'order_price' ] * $dataUpdate[ 'order_discount' ] / 100;
// 			}
			if( empty( $errors ) ) {
				if( $this->mSaleOrder->update( $orderId, $dataUpdate ) )
					$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Cập nhật thay đổi thành công.</div>";
				else 
					$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Cập nhật thay đổi không thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Cập nhật thay đổi không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'order' ] = $this->mSaleOrder->getOrder( $orderId );
		$this->page[ 'data' ][ 'order' ]->order_id = $orderId;
// 		print_r($this->page[ 'data' ][ 'order' ]);
		$this->page[ 'data' ][ 'urlPrint' ] = System::$config->baseUrl 
												. "ban-hang/don-hang/in/" 
												. $orderId . System::$config->urlSuffix;
		
		if( empty( $this->page[ 'data' ][ 'order' ] ) ) {
			$this->router->router(
				System::$config->baseUrl
				. "404"
				. System::$config->urlSuffix
			);
			die();
		}
		
		//$this->page[ 'data' ][ 'shops' ] = $this->mShop->getShopsBySalePerson( $this->page[ 'data' ][ 'order' ]->salesperson_id );
// 		if( empty( $this->page[ 'data' ][ 'shops' ] ) ) {
// 			$this->page[ 'data' ][ 'warehouses' ] = array();
// 		} else {
// 			$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouseByShop( $this->page[ 'data' ][ 'order' ]->shop_id );
// 		}
		
		if( $this->page[ 'data' ][ 'order' ]->status == 3 ) {
			if(!empty($this->page[ 'data' ][ 'order' ]->order_lines)) {
				foreach($this->page[ 'data' ][ 'order' ]->order_lines as $key => $value) {
					$count = $this->mRefund->getCountProductRefundByOutputLineId($value->order_line_id);
					$this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount_old = $this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount;
					$this->page[ 'data' ][ 'order' ]->order_lines[$key]->refundCount = $count;
					$this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount = $this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount_old - $count;
				}
				$this->page[ 'data' ][ 'order' ]->order_price
						= $this->mSaleOrder->getOrderTotalSale($orderId);
			}
			$this->page[ 'viewPath' ] = "sale management/order management/ViewOrderInspected";
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $orderId
	 */
	public function view($orderId) {
		$this->page[ 'accessID' ] = SALE_ORDER_VIEW_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

		$this->page[ 'viewPath' ] = "sale management/order management/ViewOrder";
		$this->page[ 'pageTitle' ] = "Xem đơn hàng";

		$this->page[ 'data' ][ 'orderID' ] = $orderId;

		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->page[ 'data' ][ 'orderStatus' ] = $this->mOrderStatus->getAllStatus();
		$this->page[ 'data' ][ 'shops' ] = $this->mShop->getShopsBySalePerson( $this->page['userInfo']['user_id'] );

		$this->page[ 'data' ][ 'orderPrices' ] = $this->mPrice->getPrices( null);
		if(isset($this->page[ 'data' ][ 'orderPrices' ]->item_list))
			$this->page[ 'data' ][ 'orderPrices' ] = $this->page[ 'data' ][ 'orderPrices' ]->item_list;
		else
			$this->page[ 'data' ][ 'orderPrices' ] = null;
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		$this->page[ 'data' ][ 'promotes' ] = $this->mPromote->getAllPromotes();

		$this->page[ 'data' ][ 'order' ] = $this->mSaleOrder->getOrder( $orderId );
		$this->page[ 'data' ][ 'order' ]->order_id = $orderId;
// 		print_r($this->page[ 'data' ][ 'order' ]);
		$this->page[ 'data' ][ 'urlPrint' ] = System::$config->baseUrl
				. "ban-hang/don-hang/in/"
				. $orderId . System::$config->urlSuffix;

		if( empty( $this->page[ 'data' ][ 'order' ] ) ) {
			$this->router->router(
					System::$config->baseUrl
					. "404"
					. System::$config->urlSuffix
			);
			die();
		}

		if(!empty($this->page[ 'data' ][ 'order' ]->order_lines)) {
			foreach($this->page[ 'data' ][ 'order' ]->order_lines as $key => $value) {
				$count = $this->mRefund->getCountProductRefundByOutputLineId($value->order_line_id);
				$this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount_old = $this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount;
				$this->page[ 'data' ][ 'order' ]->order_lines[$key]->refundCount = $count;
				$this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount = $this->page[ 'data' ][ 'order' ]->order_lines[$key]->product_amount_old - $count;
			}
			$this->page[ 'data' ][ 'order' ]->order_price
					= $this->mSaleOrder->getOrderTotalSale($orderId);
		}
		$this->page[ 'viewPath' ] = "sale management/order management/ViewOrder";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * 
	 * @param unknown $orderId
	 */
	public function printOrder( $orderId ) {
		$this->page[ 'accessID' ] = SALE_ORDER_PRINT;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "In đơn hàng";
		$this->load->view( "sale management/order management/PrintOrder", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $orderId
	 */
	public function print1( $orderId ) {
		$this->page[ 'accessID' ] = SALE_ORDER_PRINT;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'data' ][ 'order' ] = $this->mSaleOrder->getOrder( $orderId );
		$this->page[ 'pageTitle' ] = "In đơn hàng";
		$this->load->view( "sale management/order management/PrintOrder", array( "data" => $this->page['data'] ) );
	}
	/**
	 * 
	 * @param unknown $orderId
	 */
	public function delete( $orderId ) {
		$this->page[ 'accessID' ] = SALE_ORDER_DELETE_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mSaleOrder->delete( $orderId );
		$this->router->router(
			System::$config->baseUrl
			. "ban-hang/don-hang"
			. System::$config->urlSuffix
		);
	}
	/**
	 * 
	 * @param unknown $orderId
	 */
	public function inspection( $orderId ) {
		$this->page[ 'accessID' ] = SALE_ORDER_INSPECTION;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'pageTitle' ] = "Xác nhận đơn hàng";
		$this->page['data']['orderId'] = $orderId;
		$this->page['data']['check'] = $this->checkInspection($orderId); 
		if( empty($this->page['data']['check']) ) {
 			$order = $this->mSaleOrder->getOrder( $orderId );
 			$lines = $order->order_lines;
 			if(!empty($lines))

 			foreach ($lines as $line) {
				$productInventory = $this->mProduct->getProductPriceInWarehouse($line->product_id, $line->warehouse_id);
				$this->mInventory->updateInventoryTotal(array(
						"inventory_date" => $order->order_date,
 						"order_id" => $orderId,
						"opening_stock" => $productInventory->data->item_list[0]->inventory_amount,
 						"product_id" => $line->product_id,
 						"amount" => "-" . $line->product_amount,
						"inventory_amount" => $productInventory->data->item_list[0]->inventory_amount - $line->product_amount,
 						"warehouse_id" => $line->warehouse_id,
						"create_time" => time(),
						"description" => "Đơn hàng bán"
				));

// 				$this->mInventory->newInventory(array(
// 					"inventory_date" => $order->order_date,
// 					"order_id" => $orderId,
// 					"product_id" => $line->product_id,
// 					"amount" => "-" . $line->product_amount,
// 					"warehouse_id" => $line->warehouse_id
// 				));
 			}

			$this->inspectionProcess($orderId);
			
//			$this->mSaleOrder->inspectionOrder( $orderId );

			$this->router->router(
					System::$config->baseUrl
					. "ban-hang/don-hang/chinh-sua/"
					. $orderId
					. System::$config->urlSuffix
			);
		} else {
			$this->page['viewPath'] = "sale management/order management/InspectionOrder";
			$this->load->view( "Template", array("data" => $this->page) );
		}
	}
	/**
	 * 
	 * @param unknown $orderId
	 */
	private function checkInspection($orderId) {
		$orderLines = $this->mSaleOrder->getOrder( $orderId );
		$orderLines = $orderLines->order_lines;
//		print_r($orderLines);
		$errorData = array();
		$i = 0;
		if(!empty($orderLines))
		foreach ($orderLines as $orderLine) {
			$productInventory = $this->mProduct->getProductPriceInWarehouse($orderLine->product_id, $orderLine->warehouse_id);
//			print_r($productInventory);
			if($productInventory->data->item_list[0]->inventory_amount < $orderLine->product_amount) {
				$errorData[$i] = $orderLine;
				$errorData[$i]->currentInventoryAmount = $productInventory->data->item_list[0]->inventory_amount;
				$i++;
			}
		}
		return $errorData;
	}
	/**
	 * Xác nhận đơn hàng, sử lý nhập trước xuất trước
	 * 
	 * @param unknown $orderId
	 */
	private function inspectionProcess($orderId) {
		$orderLines = $this->mSaleOrder->getOrder( $orderId );
		$orderLines = $orderLines->order_lines;
		if(!empty($orderLines))
		foreach ($orderLines as $ordeLine) {
// 			print_r($ordeLine);
			
			$arrayDataImport = array();
			$importsInStock = $this->mImportWarehouse->getImportsInStockByProduct(
					$ordeLine->product_id, $ordeLine->warehouse_id);
// 			echo "<pre>";
// 			print_r($importsInStock);
			
			foreach ($importsInStock as $importInStock) {
				
				if($ordeLine->product_amount <= 0) break;
				$inventory = $importInStock['input_line_amount'] - $importInStock['buy_amount'];
				if($ordeLine->product_amount < $inventory) {
					$amount = $ordeLine->product_amount;
				} else {
					$amount = $inventory;
				}
// 				echo "<br />Update import line id: " . $importInStock['input_line_id'] . " : " . ($amount + $importInStock['buy_amount']);
				$this->mImportWarehouse->updateImportLine(
						$importInStock['input_line_id'], 
						array(
							"buy_amount" => $amount + $importInStock['buy_amount']
						)
				);
				$arrayDataImport[] = array(
						'product_id' => $ordeLine->product_id,
						'input_line_id' => $importInStock['input_line_id'],
						'amount' => $amount
				);
				$ordeLine->product_amount = $ordeLine->product_amount - $inventory;
			}
			$arrayDataImport = json_encode($arrayDataImport);
// 			$arrayDataImport = str_replace("\"", "'", $arrayDataImport);
			$arrayDataImport = $arrayDataImport;
// 			echo "<br />Update order_line_id " . $ordeLine->order_line_id . " : " . $arrayDataImport;
			$this->mSaleOrder->updateOrderLineInputLineId($ordeLine->order_line_id, $arrayDataImport);
		}
 		$this->mSaleOrder->updateOrder($orderId, array(
 				"status" => 3
 		));
	}
}
/*end of file Order.class.php*/
/*location: Order.class.php*/