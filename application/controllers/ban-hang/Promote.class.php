<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 */
if (! defined ( 'BOOTSTRAP_PATH' ))
	exit ( "No direct script access allowed" );
class Promote extends Controller {
	/**
	 */
	public function __construct() {
		parent::__construct ();
		$this->page ['pageID'] = "page-promote";
		$this->load->library( "HttpRequest" );
		$this->load->model ( "MPromote" );
		
		$this->load->model( "MMenu" );
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 9 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = 1) {
		$this->page['pageTitle'] = "Danh sách khuyến mại";
		$this->page[ 'viewPath' ] = "sale management/promote management/ViewList";
		
		$data = array ();
		$data ['per_page_number'] = PER_PAGE_NUMBER;
		$data ['limit_number_start'] = ($page - 1) * $data ['per_page_number'];
		
		if (isset ( $_POST ['search'] )) {
			if (isset ( $_POST ['promote_name'] ) && strlen ( $_POST ['promote_name'] )) {
				$data ['promote_name'] = strip_tags ( $_POST ['promote_name'] );
			}
		}
		
// 		$this->page ['promotes'] = $this->mPromote->getPromotes ( $data );
		
		if(
			isset($_REQUEST['controller']) and $_REQUEST['controller']!=null and
			isset($_REQUEST['action']) and $_REQUEST['action']!=null		
		) {
			include('application/views/sale management/promote management/huy/main.php');
		//	echo $main;
			exit();
		}else{
			$this->load->view ( "Template", array( "data" => $this->page ) );	
		}
	}
	public function edit($promoteID) {
		$this->page ['pageTitle'] = "Chỉnh khuyến mại";
		
		if (isset ( $_POST ['update'] )) {
			$errors = array ();
			$data = array ();
			if (isset ( $_POST ['promote_name'] ) && strlen ( $_POST ['promote_name'] )) {
				$data ['promote_name'] = strip_tags ( $_POST ['promote_name'] );
			} else {
				$errors ['promote_name'] = "promote_name";
			}
			if (isset ( $_POST ['time_start'] )) {
				$data ['time_start'] = strtotime ( $_POST ['time_start'] );
			} else {
				$errors ['time_start'] = "time_start";
			}
			if (isset ( $_POST ['time_end'] )) {
				$data ['time_end'] = strtotime ( $_POST ['time_end'] );
			} else {
				$errors ['time_end'] = "time_end";
			}
			if (isset ( $_POST ['validity'] ) && $_POST ['validity'] == 1) {
				$data ['validity'] = 1;
			} else {
				$data ['validity'] = 0;
			}
			// products
			$data ['products'] = array ();
			if (isset ( $_POST ['products'] ) && isset ( $_POST ['min_amount'] ) && isset ( $_POST ['discount'] ) && isset ( $_POST ['promote_product_id'] ) && isset ( $_POST ['promote_product_amount'] )) {
				$i = 0;
				foreach ( $_POST ['products'] as $item ) {
					$data ['products'] [$i] = array (
							"product_id" => $item,
							"min_amount" => $_POST ['min_amount'] [$i],
							"discount" => $_POST ['discount'] [$i],
							"promote_product_id" => $_POST ['promote_product_id'] [$i],
							"promote_product_amount" => $_POST ['promote_product_amount'] [$i] 
					);
					$i ++;
				}
			}
			//orders
			$data ['orders'] = array ();
			if (isset ( $_POST ['order_price_from'] ) && isset ( $_POST ['order_price_to'] ) && isset ( $_POST ['discount'] ) && isset ( $_POST ['promote_product_id'] ) && isset ( $_POST ['promote_product_amount'] )) {
				$i = 0;
				foreach ( $_POST ['order_price_from'] as $item ) {
					$data ['orders'] = array (
							"order_price_from" => $_POST ['order_price_from'] [$i],
							"order_price_to" => $_POST ['order_price_to'] [$i],
							"discount" => $_POST ['discount'] [$i],
							"promote_product_id" => $_POST ['promote_product_id'] [$i],
							"promote_product_amount" => $_POST ['promote_product_amount'] [$i] 
					);
					$i ++;
				}
			}
			
			if (empty ( $errors )) {
				$productID = $this->mPromote->update ( $promoteID, $data );
				$this->page ['message'] = "Thêm mới khyến mại thành công";
			} else {
				$this->page ['message'] = "Thêm mới khuyến mại không thành công";
			}
		}
		
		$this->load->view ( "sale management/promote management/EditPromote", $this->page );
	}
	public function addNew() {
		$this->page ['pageTitle'] = "Thêm mới khuyến mại";
		
		if (isset ( $_POST ['addNew'] )) {
			$errors = array ();
			$data = array ();
			if (isset ( $_POST ['promote_name'] ) && strlen ( $_POST ['promote_name'] )) {
				$data ['promote_name'] = strip_tags ( $_POST ['promote_name'] );
			} else {
				$errors ['promote_name'] = "promote_name";
			}
			if (isset ( $_POST ['time_start'] )) {
				$data ['time_start'] = strtotime ( $_POST ['time_start'] );
			} else {
				$errors ['time_start'] = "time_start";
			}
			if (isset ( $_POST ['time_end'] )) {
				$data ['time_end'] = strtotime ( $_POST ['time_end'] );
			} else {
				$errors ['time_end'] = "time_end";
			}
			if (isset ( $_POST ['validity'] ) && $_POST ['validity'] == 1) {
				$data ['validity'] = 1;
			} else {
				$data ['validity'] = 0;
			}
			// products
			$data ['products'] = array ();
			if (isset ( $_POST ['products'] ) && isset ( $_POST ['min_amount'] ) && isset ( $_POST ['discount'] ) && isset ( $_POST ['promote_product_id'] ) && isset ( $_POST ['promote_product_amount'] )) {
				$i = 0;
				foreach ( $_POST ['products'] as $item ) {
					$data ['products'] [$i] = array (
							"product_id" => $item,
							"min_amount" => $_POST ['min_amount'] [$i],
							"discount" => $_POST ['discount'] [$i],
							"promote_product_id" => $_POST ['promote_product_id'] [$i],
							"promote_product_amount" => $_POST ['promote_product_amount'] [$i] 
					);
					$i ++;
				}
			}
			// orders
			$data ['orders'] = array ();
			if (isset ( $_POST ['order_price_from'] ) && isset ( $_POST ['order_price_to'] ) && isset ( $_POST ['discount'] ) && isset ( $_POST ['promote_product_id'] ) && isset ( $_POST ['promote_product_amount'] )) {
				$i = 0;
				foreach ( $_POST ['order_price_from'] as $item ) {
					$data ['orders'] = array (
							"order_price_from" => $_POST ['order_price_from'] [$i],
							"order_price_to" => $_POST ['order_price_to'] [$i],
							"discount" => $_POST ['discount'] [$i],
							"promote_product_id" => $_POST ['promote_product_id'] [$i],
							"promote_product_amount" => $_POST ['promote_product_amount'] [$i] 
					);
					$i ++;
				}
			}
			
			if (empty ( $errors )) {
				$productID = $this->mPromote->addNew ( $data );
				$this->page ['message'] = "Thêm mới khyến mại thành công";
			} else {
				$this->page ['message'] = "Thêm mới khuyến mại không thành công";
			}
		}
		
		$this->load->view ( "sale management/promote management/NewPromote", $this->page );
	}
}
/*end of file Promote.class.php*/
/*location: Promote.class.php*/