<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Shop extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
		$this->load->model( "MShop" );
		$this->load->model( "MMenu" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		$this->load->model( "MSalesPeople" );
		$this->load->model( "MShopManager" );
		$this->load->model( "MWarehouse" );
		
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 11 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page ['pageID'] = "sale-shop";
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * 
	 * @param number $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = SHOP;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page > 0 ) ? $page : 1;
		$this->page['pageTitle'] = "Hệ thống cửa hàng";
		$this->page[ 'viewPath' ] = "sale management/shop management/ViewShops";
		
		$data = array();
		$data ['per_page_number'] = PER_PAGE_NUMBER;
		$data ['limit_number_start'] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		if (isset ( $_POST ['search'] )) {
			if (isset ( $_POST ['shop_name'] ) && strlen ( $_POST ['shop_name'] )) {
				$data ['shop_name'] = strip_tags ( $_POST ['shop_name'] );
			}
			if (isset ( $_POST ['shop_address'] ) && strlen ( $_POST ['shop_address'] )) {
				$data ['shop_address'] = strip_tags ( $_POST ['shop_address'] );
			}
		}
		
		$this->page[ 'data' ]['shopsRe'] = $this->mShop->getShops( $data );
		$this->page[ 'data' ]['shops'] = $this->page[ 'data' ]['shopsRe']->item_list;
// 		print_r($this->page[ 'data' ]['shops']);
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'shopsRe' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $page
	 */
	public function page( $page = null ) {
		$page = ( $page > 0 ) ? $page : 1;
		$this->index( $page );
	}  
	/**
	 * Add new shop
	 */
	public function addNew() {
		$this->page[ 'accessID' ] = SHOP_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page ['pageTitle'] = "Thêm mới cửa hàng";
		$this->page[ 'viewPath' ] = "sale management/shop management/AddNew";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		if(!empty($this->page['data']['warehouses']))
		foreach ($this->page['data']['warehouses'] as $k => $v) {
			$w = $this->mWarehouse->getWarehouse($v->warehouse_id);
			if($w->workplace_id > 0) unset($this->page['data']['warehouses'][$k]);
		}
// 		print_r($this->page[ 'data' ][ 'warehouses' ]);
		
		if( isset( $_POST[ 'save' ] ) ) {
			$errors = array();
			$data = array();
			if( isset( $_POST[ 'shop_name' ] ) && strlen( $_POST[ 'shop_name' ] ) ) {
				$data[ 'shop_name' ] = strip_tags( $_POST[ 'shop_name' ] );
			} else {
				$errors[ 'shop_name' ] = "shop_name";
			}
			if( isset( $_POST[ 'shop_address' ] ) && strlen( $_POST[ 'shop_address' ] ) ) {
				$data[ 'shop_address' ] = strip_tags( $_POST[ 'shop_address' ] );
			} else {
				$errors[ 'shop_address' ] = "shop_address";
			}
// 			if( isset( $_POST[ 'shop_code' ] ) && strlen( $_POST[ 'shop_code' ] ) ) {
// 				$data[ 'shop_code' ] = strip_tags( $_POST[ 'shop_code' ] );
// 			} else {
// 				$errors[ 'shop_code' ] = "shop_code";
// 			}
			if( isset( $_POST[ 'district_id' ] ) && filter_var( $_POST[ 'district_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'district_id' ] = $_POST[ 'district_id' ];
			} else {
				$errors[ 'district_id' ] = "district_id";
			}
// 			if( isset( $_POST[ 'city_id' ] ) && filter_var( $_POST[ 'city_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'city_id' ] = $_POST[ 'city_id' ];
// 			} else {
// 				$errors[ 'city_id' ] = "city_id";
// 			}
// 			if( isset( $_POST[ 'country_id' ] ) && filter_var( $_POST[ 'country_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'country_id' ] = $_POST[ 'country_id' ];
// 			} else {
// 				$errors[ 'country_id' ] = "country_id";
// 			}
			if( isset( $_POST[ 'phone' ] ) && strlen( $_POST[ 'phone' ] ) ) {
				$data[ 'phone' ] = $_POST[ 'phone' ];
			} else {
				$errors[ 'phone' ] = "phone";
			}
// 			if( isset( $_POST[ 'warehouse_id' ] ) && filter_var( $_POST[ 'warehouse_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
// 			} else {
// 				$errors[ 'warehouse_id' ] = "warehouse_id";
// 			}
// 			if( isset( $_POST[ 'price_id' ] ) && filter_var( $_POST[ 'price_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'price_id' ] = $_POST[ 'price_id' ];
// 			} else {
// 				$errors[ 'price_id' ] = "price_id";
// 			}
			if( isset( $_POST[ 'shop_manager_id' ] ) && strlen( $_POST[ 'shop_manager_id' ] ) ) {
				$shopManagerId = $_POST[ 'shop_manager_id' ];
			} else {
				$errors[ 'shop_manager_id' ] = "shop_manager_id";
			}
			if( isset( $_POST[ 'quittance_footer' ] ) && strlen( $_POST[ 'quittance_footer' ] ) ) {
				$data[ 'quittance_footer' ] = strip_tags( $_POST[ 'quittance_footer' ] );
			} else {
				$data[ 'quittance_footer' ] = "";
			}
			if( isset( $_POST[ 'quittance_header' ] ) && strlen( $_POST[ 'quittance_header' ] ) ) {
				$data[ 'quittance_header' ] = strip_tags( $_POST[ 'quittance_header' ] );
			} else {
				$data[ 'quittance_header' ] = "";
			}
			if( empty( $errors ) ) {
				$shopId = $this->mShop->addNew( $data );
				if( $shopId > 0 ) {
					//shop manager
					$this->mShopManager->newShopManager( $shopManagerId, $shopId );
					
					//shop's warehouse
					if(isset($_POST['warehouse_id']) && !empty($_POST['warehouse_id'])) {
						foreach ($_POST['warehouse_id'] as $item ) {
							$this->mWarehouse->update($item, array(
								"workplace_id" => $shopId
							));
						}
					}
					
					$this->router->router(
						System::$config->baseUrl
						. "ban-hang/cua-hang"
						. System::$config->urlSuffix
					);
				} else {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Có lỗi! Thêm mới cửa hàng không thành công</b>";
				}
			} else {
				print_r($errors);
				$this->page[ 'errors' ] = $errors;
				$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Thêm mới cửa hàng không thành công. 
							Thông tin cửa hàng không hợp lệ</b>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit shop 
	 * 
	 * @param unknown $shopId
	 */
	public function edit( $shopId ) {
		$this->page[ 'accessID' ] = SHOP_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page ['pageTitle'] = "Chỉnh sửa thông tin cửa hàng";
		$this->page[ 'viewPath' ] = "sale management/shop management/EditShop";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
// 		print_r($this->page[ 'data' ][ 'warehouses' ]);
		if(isset($this->page['data']['warehouses']))
		foreach ($this->page['data']['warehouses'] as $k => $v) {
			$w = $this->mWarehouse->getWarehouse($v->warehouse_id);
			if($w == null) continue;
			if($w->workplace_id > 0 && $w->workplace_id != $shopId) unset($this->page['data']['warehouses'][$k]);
		}
// 		print_r($this->page[ 'data' ][ 'warehouses' ]);
		
		if( isset( $_POST[ 'save' ] ) ) {
			$errors = array();
			$data = array();
			$data[ 'shop_id' ] = $shopId;
			if( isset( $_POST[ 'shop_name' ] ) && strlen( $_POST[ 'shop_name' ] ) ) {
				$data[ 'shop_name' ] = strip_tags( $_POST[ 'shop_name' ] );
			} else {
				$errors[ 'shop_name' ] = "shop_name";
			}
			if( isset( $_POST[ 'shop_address' ] ) && strlen( $_POST[ 'shop_address' ] ) ) {
				$data[ 'shop_address' ] = strip_tags( $_POST[ 'shop_address' ] );
			} else {
				$errors[ 'shop_address' ] = "shop_address";
			}
// 			if( isset( $_POST[ 'shop_code' ] ) && strlen( $_POST[ 'shop_code' ] ) ) {
// 				$data[ 'shop_code' ] = strip_tags( $_POST[ 'shop_code' ] );
// 			} else {
// 				$errors[ 'shop_code' ] = "shop_code";
// 			}
			if( isset( $_POST[ 'district_id' ] ) && filter_var( $_POST[ 'district_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'district_id' ] = $_POST[ 'district_id' ];
			} else {
				$errors[ 'district_id' ] = "district_id";
			}
// 			if( isset( $_POST[ 'city_id' ] ) && filter_var( $_POST[ 'city_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'city_id' ] = $_POST[ 'city_id' ];
// 			} else {
// 				$errors[ 'city_id' ] = "city_id";
// 			}
// 			if( isset( $_POST[ 'country_id' ] ) && filter_var( $_POST[ 'country_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'country_id' ] = $_POST[ 'country_id' ];
// 			} else {
// 				$errors[ 'country_id' ] = "country_id";
// 			}
			if( isset( $_POST[ 'phone' ] ) && strlen( $_POST[ 'phone' ] ) ) {
				$data[ 'phone' ] = $_POST[ 'phone' ];
			} else {
				$errors[ 'phone' ] = "phone";
			}
// 			if( isset( $_POST[ 'warehouse_id' ] ) && filter_var( $_POST[ 'warehouse_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
// 			} else {
// 				$errors[ 'warehouse_id' ] = "warehouse_id";
// 			}
// 			if( isset( $_POST[ 'price_id' ] ) && filter_var( $_POST[ 'price_id' ], FILTER_VALIDATE_INT ) ) {
// 				$data[ 'price_id' ] = $_POST[ 'price_id' ];
// 			} else {
// 				$errors[ 'price_id' ] = "price_id";
// 			}
			if( isset( $_POST[ 'shop_manager_id' ] ) && strlen( $_POST[ 'shop_manager_id' ] ) ) {
				$shopManagerId = $_POST[ 'shop_manager_id' ];
			} else {
				$errors[ 'shop_manager_id' ] = "shop_manager_id";
			}
			if( isset( $_POST[ 'quittance_footer' ] ) && strlen( $_POST[ 'quittance_footer' ] ) ) {
				$data[ 'quittance_footer' ] = strip_tags( $_POST[ 'quittance_footer' ] );
			} else {
				$data[ 'quittance_footer' ] = "";
			}
			if( isset( $_POST[ 'quittance_header' ] ) && strlen( $_POST[ 'quittance_header' ] ) ) {
				$data[ 'quittance_header' ] = strip_tags( $_POST[ 'quittance_header' ] );
			} else {
				$data[ 'quittance_header' ] = "";
			}
			if( empty( $errors ) && $this->mShop->update( $data ) ) {
				//update shop manager
				$manager = $this->mShopManager->getShopManager( $shopId );
				if( isset( $manager[ 0 ] ) ) {
					$this->mShopManager->updateShopManager(  $manager[ 0 ]->employee_work_id, $shopManagerId, $shopId );
				} else {
					$this->mShopManager->newShopManager( $shopManagerId, $shopId );
				}
				
				//update shop's warehouse
				$oldWarehouses = $this->mShop->getShop( $shopId );
				$oldWarehouses = $oldWarehouses->warehouse;
// 				print_r($oldWarehouses);
					//Xóa
				if(!empty($oldWarehouses)) {
					foreach ($oldWarehouses as $itemOld) {
						if(empty($_POST['warehouse_id'])) {
// 							echo "Delete: " . $itemOld->warehouse_id;
							$this->mWarehouse->update($itemOld->warehouse_id, array(
								"workplace_id" => ""
							));
						} else {
							$count = 0;
							foreach ($_POST['warehouse_id'] as $item) {
								if($item == $itemOld->warehouse_id) {
									$count = 1;
									break;
								}
							}
							if($count == 0) {
// 								echo "Delete: " . $itemOld->warehouse_id;
								$this->mWarehouse->update($itemOld->warehouse_id, array(
										"workplace_id" => ""
								));
							}
						}
					}
				}
					//add new warehouse shop
				if(!empty($_POST['warehouse_id'])) {
					foreach ($_POST['warehouse_id'] as $item) {
						if(empty($oldWarehouses)) {
							//add new
// 							echo "Add new: " . $item;
							$this->mWarehouse->update($item, array(
									"workplace_id" => $shopId
							));
						} else {
							$count = 0;
							foreach ($oldWarehouses as $itemOld) {
								if($item == $itemOld->warehouse_id) {
									$count = 1;
									break;
								}
							}
							if($count == 0) {
// 								echo "Add new: " . $item;
								$this->mWarehouse->update($item, array(
										"workplace_id" => $shopId
								));
							}
						}
					}
				}
				
				$this->page[ 'data' ][ 'message' ] = "<b class='text-success'>Thay đổi thành công</b>";
			} else {
				$this->page[ 'data' ][ 'errors' ] = $errors;
				$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Thay đổi không thành công</b>";
			}
		}
		$this->page[ 'data' ][ 'shop' ] = $this->mShop->getShop( $shopId );
// 		print_r($this->page[ 'data' ][ 'shop' ]);
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $shopId
	 */
	public function view($shopId) {
		$this->page[ 'accessID' ] = SHOP_VIEW_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page ['pageTitle'] = "Chỉnh sửa thông tin cửa hàng";
		$this->page[ 'viewPath' ] = "sale management/shop management/ViewShop";

		$this->page[ 'data' ][ 'shop' ] = $this->mShop->getShop( $shopId );
// 		print_r($this->page[ 'data' ][ 'shop' ]);

		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Delete shop
	 * 
	 * @param unknown $shopId
	 */
	public function delete( $shopId ) {
		$this->page[ 'accessID' ] = SHOP_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mShop->delete( $shopId );
		$this->router->router(
				System::$config->baseUrl
				. "ban-hang/cua-hang/"
				. System::$config->urlSuffix
		);
	}
}
/*end of file Shop.class.php*/
/*location: Shop.class.php*/