<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 */
if (! defined ( 'BOOTSTRAP_PATH' )) exit ( "No direct script access allowed" );

/**
 * Class Price
 * @package App\Controllers
 */
class Price extends Controller {
	/**
	 * Construct
	 */
	public function __construct() {
		parent::__construct ();
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MPrice" );
		$this->load->model( "MMenu" );
		$this->load->model( "MProduct" );
		
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 10 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page ['pageID'] = "sale-price";
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param null $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = SALE_PRICE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách báo giá";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$data = array ();
		$data['per_page_number'] = PER_PAGE_NUMBER;
		$data['limit_number_start'] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		if (isset ( $_POST ['search'] )) {
			if (isset ( $_POST ['price_name'] ) && strlen ( $_POST ['price_name'] )) {
				$data ['price_name'] = strip_tags ( $_POST ['price_name'] );
			}
		}
		$dataPrice = $this->mPrice->getPrices( $data );
		if(isset($dataPrice->item_list))
			$this->page[ 'data' ]['prices'] = $dataPrice->item_list;
		else
			$this->page[ 'data' ]['prices'] = null;
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $dataPrice->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page = 1 ) {
		$page = ( $page > 0 ) ? $page : 1;
		$this->index( $page );
	}

	/**
	 * Add new price
	 * 
	 */
	public function addNew( $priceID = null ) {
		$this->page[ 'accessID' ] = SALE_PRICE_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page['pageTitle'] = "Thêm mới báo giá";
		$this->page[ 'viewPath' ] = "sale management/price management/NewPrice";
		
		$this->page['data'][ 'products' ] = $this->mProduct->getAllProduct();
		
		if( $priceID == null ) {
			$priceID = $this->mPrice->addNew(null);
			if( $priceID == 0 ) exit("Lỗi không thể tạo mới bảng báo giá.");
			System::$router->router(
				System::$config->baseUrl
				. "ban-hang/bao-gia/them-moi/"
				. $priceID
				. System::$config->urlSuffix
			);
			die();
		} 
		
		$this->page[ 'data' ][ 'priceID' ] = $priceID;
		
		if (isset ( $_POST ['save'] )) {
			$errors = array ();
			$dataUpdate = array ();
			if ( isset( $_POST['price_name'] ) && strlen( $_POST['price_name'] )) {
				$dataUpdate['price_name'] = strip_tags ( $_POST ['price_name'] );
			} else {
				$errors ['price_name'] = "price_name";
			}
			if (isset ( $_POST ['validity'] ) && $_POST ['validity'] == 1) {
				$dataUpdate['validity'] = 1;
			} else {
				$dataUpdate['validity'] = 0;
			}
			if (isset ( $_POST ['time_start'] ) 
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_start'] )) {
				$dataUpdate['time_start'] = strtotime ( $_POST ['time_start'] );
			} else {
				$errors ['time_start'] = "time_start";
			}
			if (isset( $_POST ['time_end'] ) 
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_end'] )) {
				$dataUpdate['time_end'] = strtotime ( $_POST ['time_end'] );
			} else {
				$errors ['time_end'] = "time_end";
			}
			// products
			if (isset ( $_POST ['product_id'] ) 
				&& ! empty ( $_POST ['product_id'] ) 
				&& isset ( $_POST ['product_price'] ) 
				&& isset ( $_POST ['product_note'] )) {
				$i = 0;
				foreach ( $_POST ['product_id'] as $item ) {
					$dataPriceLine = array (
							"product_id" => $item,
							"product_price" => $_POST ['product_price'] [$i],
							"product_note" => $_POST ['product_note'] [$i],
							"price_id" => $priceID
					);
					$this->mPrice->newPriceLine( $dataPriceLine );
					$i ++;
				}
			}
			if (empty ( $errors )) {
				if( $this->mPrice->update( $priceID, $dataUpdate ) ) {
					$this->router->router( 
							System::$config->baseUrl 
							. "ban-hang/bao-gia/chinh-sua/" 
							. $priceID
							. System::$config->urlSuffix );
				} else {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Thêm mới bảng báo giá không thành công.</b>";
				}
			} else {
				$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Thêm mới bảng báo giá không thành công.</b>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Edit price
	 * 
	 * @param unknown $priceID
	 */
	public function edit( $priceID ) {
		$this->page[ 'accessID' ] = SALE_PRICE_EDIT;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page ['pageTitle'] = "Chỉnh sửa báo giá";
		$this->page[ 'viewPath' ] = "sale management/price management/EditPrice";
		
		$this->page['data'][ 'products' ] = $this->mProduct->getAllProduct();
		
		if (isset ( $_POST ['update'] )) {
			$errors = array ();
			$dataUpdate = array ();
			if ( isset( $_POST['price_name'] ) && strlen( $_POST['price_name'] )) {
				$dataUpdate['price_name'] = strip_tags ( $_POST ['price_name'] );
			} else {
				$errors ['price_name'] = "price_name";
			}
			if (isset ( $_POST ['validity'] ) && $_POST ['validity'] == 1) {
				$dataUpdate['validity'] = 1;
			} else {
				$dataUpdate['validity'] = 0;
			}
			if (isset ( $_POST ['time_start'] )
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_start'] )) {
				$dataUpdate['time_start'] = strtotime ( $_POST ['time_start'] );
			} else {
				$errors ['time_start'] = "time_start";
			}
			if (isset ( $_POST ['time_end'] )
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_end'] )) {
				$dataUpdate['time_end'] = strtotime ( $_POST ['time_end'] );
			} else {
				$errors ['time_end'] = "time_end";
			}
			// products
			if (isset ( $_POST ['product_id'], $_POST[ 'product_price_id' ] )
				&& ! empty ( $_POST ['product_id'] )
				&& isset ( $_POST ['product_price'] )
				&& isset ( $_POST ['product_note'] )) {
					
				$priceLines = $this->mPrice->getPriceLines( $priceID );
				if( !empty( $priceLines ) )
					foreach ( $priceLines as $priceLine ) {
						$c = 0;
						foreach ( $_POST[ 'product_price_id' ] as $new ) {
							if( $new == $priceLine->product_price_id )
								$c++;
						}
						if( $c == 0 ) {
							$this->mPrice->deletePriceLine( $priceLine->product_price_id );
						}
					}
					
				$i = 0;
				foreach ( $_POST ['product_id'] as $item ) {
					if( isset( $_POST[ 'product_price_id' ][ $i ] ) 
						&& $_POST[ 'product_price_id' ][ $i ] > 0 ) {
						//update
						$dataPriceLine = array (
								"product_price_id" => $_POST[ 'product_price_id' ][ $i ],
								"product_id" => $item,
								"product_price" => $_POST ['product_price'] [$i],
								"product_note" => $_POST ['product_note'] [$i],
								"price_id" => $priceID
						);
						$this->mPrice->updatePriceLine( $dataPriceLine );
					} else {
						//add new
						$dataPriceLine = array (
								"product_id" => $item,
								"product_price" => $_POST ['product_price'] [$i],
								"product_note" => $_POST ['product_note'] [$i],
								"price_id" => $priceID
						);
						$this->mPrice->newPriceLine( $dataPriceLine );
					}
					$i++;
				}
			}
			if (empty ( $errors )) {
				if( $this->mPrice->update( $priceID, $dataUpdate ) ) {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-success'>Lưu thay đổi bảng báo giá thành công.</b>";
				} else {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Lưu thay đổi bảng báo giá không thành công.</b>";
				}
			} else {
				$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Lưu thay đổi bảng báo giá không thành công.</b>";
			}
		}
		
		$this->page[ 'data' ][ 'price' ] = $this->mPrice->getPrice( $priceID );
		$this->page[ 'data' ][ 'priceLines' ] = $this->mPrice->getPriceLines( $priceID );
		$i = 0;
		foreach ( $this->page[ 'data' ][ 'priceLines' ] as $item ) {
			$product = $this->mProduct->getProduct( 
				$this->page[ 'data' ][ 'priceLines' ][ $i ]->product_id 
			);
			if($product == null) {
				$this->page[ 'data' ][ 'priceLines' ][ $i ]->product_name = null;
				$this->page[ 'data' ][ 'priceLines' ][ $i ]->product_code = null;
			} else {
				$this->page['data']['priceLines'][$i]->product_name = $product->product_name;
				$this->page['data']['priceLines'][$i]->product_code = $product->product_code;
			}
			$i++;
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Delete price
	 * 
	 * @param unknown $priceID
	 */
	public function delete( $priceID ) {
		$this->page[ 'accessID' ] = SALE_PRICE_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mPrice->delete( $priceID );
		$this->router->router(
				System::$config->baseUrl
				. "ban-hang/bao-gia"
				. System::$config->urlSuffix
		);
	}
}
/*end of file Price.class.php*/
/*location: Price.class.php*/