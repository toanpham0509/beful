<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Product extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->library( "Upload" );
		
		$this->page[ 'pageID' ] = "sale-product";
		$this->load->model( "MProduct" );
		$this->load->model( "MProductCategory" );
		$this->load->model( "MProductUnit" );
		$this->load->model( "MProductStatus" );
		$this->load->model( "MMenu" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MPrice" );
		$this->load->model( "MSalesPeople" );
		
		$this->upload->config( array(
			"allowedTypes" 	=> 		" jpg png jpeg gif ",
			"encryptName"	=> 		true
		) );
		
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 8 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * 
	 * @param 		unknown 		$priceID
	 * @param 		unknown 		$wareHouseID
	 * @param 		number		 	$page
	 */
	public function index($page = null ) {
		$this->page[ 'accessID' ] = PRODUCT;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page > 0 ) ? $page : 1;

		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->page[ 'data' ][ 'orderPrices' ] = $this->mPrice->getPrices(null);
		if(isset($this->page[ 'data' ][ 'orderPrices' ]->item_list))
			$this->page[ 'data' ][ 'orderPrices' ] = $this->page[ 'data' ][ 'orderPrices' ]->item_list;
		else
			$this->page[ 'data' ][ 'orderPrices' ] = null;

		if( isset( $_REQUEST[ 'price_id' ], $_REQUEST[ 'warehouse_id' ] ) 
			&& filter_var( $_REQUEST[ 'price_id' ], FILTER_VALIDATE_INT )
			&& filter_var( $_REQUEST[ 'warehouse_id' ]. FILTER_VALIDATE_INT ) ){
			$this->router->router( 
				System::$config->baseUrl 
				. "ban-hang/san-pham/ton-trong-kho/"
				. $_REQUEST[ 'price_id' ]
				. "/"
				. $_REQUEST[ 'warehouse_id' ]
				. System::$config->urlSuffix 
			);
			die();
		}
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getListProducts(array(
			"per_page_number" => PER_PAGE_NUMBER,
			"limit_number_start" => $page
		));

		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'products' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		$this->page[ 'pageTitle' ] = "Lựa chọn kho, báo giá - Sản phẩm";
		$this->page[ 'viewPath' ] = "sale management/product management/ChooseWarehousePrice";
			
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $productId
	 */
	public function view($productId) {
		$this->page[ 'accessID' ] = PRODUCT_VIEW_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'pageTitle' ] = "Xem thông sản phẩm";
		$this->page[ 'viewPath' ] = "sale management/product management/ViewProduct";
		$this->page[ 'data' ][ 'productsCategory' ] = $this->mProduct->getProductsCategory( $productId );
		$this->page[ 'data' ][ 'product' ] = $this->mProduct->getProduct( $productId );

// 		print_r($this->page[ 'data' ][ 'product' ]);
		$path = System::$config->baseUrl . $this->page['data']['product']->product_thumbnail;
		if(!@fopen($path,"r")) {
			$this->page['data']['product']->product_thumbnail = "public/images/no_image.jpg";
		}

		$this->page[ 'data' ][ 'productsCategory' ] = $this->mProduct->getProductsCategory( $productId );
		$this->page[ 'data' ][ 'dataSubmit' ] = (array)$this->page[ 'data' ][ 'product' ];

		$this->page[ 'data' ][ 'productCategories' ] = $this->mProductCategory->getProductCategories();
		$this->page[ 'data' ][ 'productUnits' ] = $this->mProductUnit->getUnits();
		$this->page[ 'data' ][ 'productStatus' ] = $this->mProductStatus->getProductStatus();

		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page($page = 1) {
		$this->index($page);
	}

	/**
	 * @param null $priceID
	 * @param null $wareHouseID
	 * @param null $page
	 */
	public function viewInWarehouse($priceID = null, $wareHouseID = null, $page = null ) {
		$this->page[ 'accessID' ] = PRODUCT;
		$this->user->access( $this->page[ 'accessID' ] );

		$page = ( $page > 0 ) ? $page : 1;

		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->page[ 'data' ][ 'orderPrices' ] = $this->mPrice->getPrices(null);
		$this->page[ 'data' ][ 'orderPrices' ] = $this->page[ 'data' ][ 'orderPrices' ]->item_list;
		if( isset( $_REQUEST[ 'price_id' ], $_REQUEST[ 'warehouse_id' ] )
				&& filter_var( $_REQUEST[ 'price_id' ], FILTER_VALIDATE_INT )
				&& filter_var( $_REQUEST[ 'warehouse_id' ]. FILTER_VALIDATE_INT ) ){
			$this->router->router(
					System::$config->baseUrl
					. "ban-hang/san-pham/index/"
					. $_REQUEST[ 'price_id' ]
					. "/"
					. $_REQUEST[ 'warehouse_id' ]
					. System::$config->urlSuffix
			);
			die();
		} elseif( ( $priceID == null ) || ( $wareHouseID == null ) ) {
			if(isset($_REQUEST['page']) && filter_var($_REQUEST['page'], FILTER_VALIDATE_INT)) {
				$page = $_REQUEST['page'];
			} else {
				$page = 1;
			}
			$this->page[ 'data' ][ 'products' ] = $this->mProduct->getListProducts(array(
					"per_page_number" => PER_PAGE_NUMBER,
					"limit_number_start" => $page
			));

			//pagination
			$this->page[ 'data' ][ 'page' ] = $page;
			$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'products' ]->num_rows_total;
			if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
				$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
			} else {
				$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
			}

			$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

			$this->page[ 'pageTitle' ] = "Lựa chọn kho, báo giá - Sản phẩm";
			$this->page[ 'viewPath' ] = "sale management/product management/ChooseWarehousePrice";

			$this->load->view( "Template", array( "data" => $this->page ) );
			die();
		}

		$this->page[ 'pageTitle' ] = "Danh sách sản phẩm";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		$this->page[ 'data' ][ 'price_id' ] = $priceID;
		$this->page[ 'data' ][ 'warehouse_id' ] = $wareHouseID;

		$data = array();
		$data[ 'warehouse_id' ] = $wareHouseID;
		$data[ 'price_id' ] = $priceID;
		$data[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$data[ 'limit_number_start' ] = $page;

// 		print_r($data);
		$dataReturn =  $this->mProduct->getProducts( $data );
// 		print_r($dataReturn);

		if( isset( $dataReturn->item_list ) )
			$this->page[ 'data' ][ 'products' ] = $dataReturn->item_list;
		else
			$this->page[ 'data' ][ 'products' ] = null;
		if(!empty($this->page[ 'data' ][ 'products' ]))
			foreach ($this->page[ 'data' ][ 'products' ] as $k => $item) {
				if(!@fopen(System::$config->baseUrl . $item->product_thumbnail, "r")) {
					$this->page[ 'data' ][ 'products' ][$k]->product_thumbnail = "public/images/no_image.jpg";
				}
			}
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $dataReturn->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * add new product
	 */
	public function addNew( $productId = null ) {
		$this->page[ 'accessID' ] = PRODUCT_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Thêm mới sản phẩm";
		$this->page[ 'viewPath' ] = "sale management/product management/NewProduct";
		
		if( $productId == null || $productId < 0 ) :
			$this->page[ 'data' ][ 'dataSubmit' ][ 'productCode' ] = $this->mProduct->addNew(array(
				"product_name" => null,
				"product_thumbnail" => null,
				"product_unit_id" => null,
				"expired_notification" => null,
				"product_description" => null,
				"inventory_min" => null,
				"inventory_max" => null,
				"guarantee" => null,
				"shipping_time" => null,
				"product_status_id" => null,
				"changing_policy" => null,
				"barcode" => null
			));
			$this->router->router(
					System::$config->baseUrl
					. "ban-hang/san-pham/them-moi/"
					. $this->page[ 'data' ][ 'dataSubmit' ][ 'productCode' ]
					. System::$config->urlSuffix
			);
			die();
		else:
			$product = $this->mProduct->getProduct( $productId );
			$this->page[ 'data' ][ 'dataSubmit' ][ 'productCode' ] = $product->product_code;
		endif;
		
		if( isset( $_POST[ 'save' ] ) ) {
			$data = array();
			$errors = array();
			if( isset( $_POST[ 'product_name' ] ) && strlen( $_POST[ 'product_name' ] ) ) {
				$data[ 'product_name' ] = strip_tags( $_POST[ 'product_name' ] );
			} else {
				$errors[ 'product_name' ] = "product_name";
			}
			if( isset( $_FILES[ 'product_thumbnail' ] ) 
				&& strlen( $_FILES[ 'product_thumbnail' ][ 'name' ] ) ) {
				if( $this->upload->doUpload( "product_thumbnail" ) ) 
					$data[ 'product_thumbnail' ] = $this->upload->getFilePath();
				else {
					$errors[ 'product_thumbnail' ] = "product_thumbnail";
				}
			} else {
				$data[ 'product_thumbnail' ] = "";
			}
			if( isset( $_POST[ 'product_unit_id' ] ) && filter_var( $_POST[ 'product_unit_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'product_unit_id' ] = $_POST[ 'product_unit_id' ];
			} else {
				$errors[ 'product_unit_id' ] = "product_unit_id";
			}
			if( isset( $_POST[ 'barcode' ] )  ) {
				$data[ 'barcode' ] = strip_tags( $_POST[ 'barcode' ] );
			} else {
				$errors[ 'barcode' ] = "barcode";
			}
			if( isset( $_POST[ 'expired_notification' ] ) ) {
				$data[ 'expired_notification' ] = $_POST[ 'expired_notification' ];
			} else {
				$data[ 'expired_notification' ] = null;
			}
			if( isset( $_POST[ 'product_description' ] ) ) {
				$data[ 'product_description' ] = strip_tags( $_POST[ 'product_description' ] );
			} else {
				$data[ 'product_description' ] = "";
			}
			if( isset( $_POST[ 'product_status_id' ] ) && filter_var( $_POST[ 'product_status_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'product_status_id' ] = $_POST[ 'product_status_id' ];
			} else {
				$errors[ 'product_status_id' ] = "product_status_id";
			}
			if( isset( $_POST[ 'inventory_min' ] ) && filter_var( $_POST[ 'inventory_min' ], FILTER_VALIDATE_FLOAT ) ) {
				$data[ 'inventory_min' ] = $_POST[ 'inventory_min' ];
			} else {
				$data[ 'inventory_min' ] = null;
			}
			if( isset( $_POST[ 'inventory_max' ] ) && filter_var( $_POST[ 'inventory_max' ], FILTER_VALIDATE_FLOAT ) ) {
				$data[ 'inventory_max' ] = $_POST[ 'inventory_max' ];
			} else {
				$data[ 'inventory_max' ] = null;
			}
			if( isset( $_POST[ 'guarantee' ] ) && filter_var( $_POST[ 'guarantee' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'guarantee' ] = $_POST[ 'guarantee' ];
			} else {
				$data[ 'guarantee' ] = null;
			}
			if( isset( $_POST[ 'shipping_time' ] ) && filter_var( $_POST[ 'shipping_time' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'shipping_time' ] = $_POST[ 'shipping_time' ];
			} else {
				$data[ 'shipping_time' ] = null;
			}
			if( isset( $_POST[ 'changing_policy' ] ) && strlen( $_POST[ 'changing_policy' ] ) ) {
				$data[ 'changing_policy' ] = strip_tags( $_POST[ 'changing_policy' ] );
			} else {
				$data[ 'changing_policy' ] = "";
			}
			if( empty( $errors ) ) {
				$data[ 'status' ] = 2;
// 				print_r($data);
				$result = $this->mProduct->update( $productId, $data );
				if( $result > 0 ) {
					if( isset( $_POST[ 'product_categories' ] ) 
						&& !empty( $_POST[ 'product_categories' ] ) ) {
						for( $i = 0; $i < sizeof( $_POST[ 'product_categories' ] ); $i++ ) {
							echo $productId;
							echo $_POST[ 'product_categories' ][ $i ] . "<br />";
							$this->mProduct->newProductCategory( 
								$productId, 
								$_POST[ 'product_categories' ][ $i ] 
							);
						}
					}
					$this->page[ 'message' ] = "Thêm mới sản phẩm thành công";
					$this->router->router(
							System::$config->baseUrl
							. "ban-hang/san-pham/chinh-sua/"
							. $productId
							. System::$config->urlSuffix
					);
					die();
				} else {
					$this->page[ 'data' ][ 'message' ] = "<span class='text-danger'>Thêm mới sản phẩm không thành công</span>";
				}
			} else {
				$this->page[ 'data' ][ 'message' ] = "<span class='text-danger'>Thêm mới sản phẩm không thành công</span>";
			}
		}
		
		$this->page[ 'data' ][ 'productCategories' ] = $this->mProductCategory->getProductCategories();
		$this->page[ 'data' ][ 'productUnits' ] = $this->mProductUnit->getUnits();
		$this->page[ 'data' ][ 'productStatus' ] = $this->mProductStatus->getProductStatus();
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit product
	 * 
	 * @param unknown $productId
	 */
	public function edit( $productId ) {
		$this->page[ 'accessID' ] = PRODUCT_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chỉnh sửa sản phẩm";
		$this->page[ 'viewPath' ] = "sale management/product management/EditProduct";
		
		$this->page[ 'data' ][ 'productsCategory' ] = $this->mProduct->getProductsCategory( $productId );
		
		if( isset( $_POST[ 'save' ] ) ) {
			$data = array();
			$errors = array();
			if( isset( $_POST[ 'product_name' ] ) && strlen( $_POST[ 'product_name' ] ) ) {
				$data[ 'product_name' ] = strip_tags( $_POST[ 'product_name' ] );
			} else {
				$errors[ 'product_name' ] = "product_name";
			}
			if( isset( $_FILES[ 'product_thumbnail' ] ) 
				&& strlen( $_FILES[ 'product_thumbnail' ][ 'name' ] ) ) {
				if( $this->upload->doUpload( "product_thumbnail" ) ) 
					$data[ 'product_thumbnail' ] = $this->upload->getFilePath();
				else {
					$errors[ 'product_thumbnail' ] = "product_thumbnail";
				}
			} else {
			}
			if( isset( $_POST[ 'product_unit_id' ] ) && filter_var( $_POST[ 'product_unit_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'product_unit_id' ] = $_POST[ 'product_unit_id' ];
			} else {
				$errors[ 'product_unit_id' ] = "product_unit_id";
			}
			if( isset( $_POST[ 'barcode' ] )  ) {
				$data[ 'barcode' ] = strip_tags( $_POST[ 'barcode' ] );
			} else {
				$errors[ 'barcode' ] = "barcode";
			}
			if( isset( $_POST[ 'expired_notification' ] ) ) {
				$data[ 'expired_notification' ] = $_POST[ 'expired_notification' ];
			} else {
				$data[ 'expired_notification' ] = null;
			}
			if( isset( $_POST[ 'product_description' ] ) ) {
				$data[ 'product_description' ] = strip_tags( $_POST[ 'product_description' ] );
			} else {
				$data[ 'product_description' ] = "";
			}
			if( isset( $_POST[ 'product_status_id' ] ) && filter_var( $_POST[ 'product_status_id' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'product_status_id' ] = $_POST[ 'product_status_id' ];
			} else {
				$errors[ 'product_status_id' ] = "product_status_id";
			}
			if( isset( $_POST[ 'inventory_min' ] ) && filter_var( $_POST[ 'inventory_min' ], FILTER_VALIDATE_FLOAT ) ) {
				$data[ 'inventory_min' ] = $_POST[ 'inventory_min' ];
			} else {
				$data[ 'inventory_min' ] = null;
			}
			if( isset( $_POST[ 'inventory_max' ] ) && filter_var( $_POST[ 'inventory_max' ], FILTER_VALIDATE_FLOAT ) ) {
				$data[ 'inventory_max' ] = $_POST[ 'inventory_max' ];
			} else {
				$data[ 'inventory_max' ] = null;
			}
			if( isset( $_POST[ 'guarantee' ] ) && filter_var( $_POST[ 'guarantee' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'guarantee' ] = $_POST[ 'guarantee' ];
			} else {
				$data[ 'guarantee' ] = null;
			}
			if( isset( $_POST[ 'shipping_time' ] ) && filter_var( $_POST[ 'shipping_time' ], FILTER_VALIDATE_INT ) ) {
				$data[ 'shipping_time' ] = $_POST[ 'shipping_time' ];
			} else {
				$data[ 'shipping_time' ] = null;
			}
			if( isset( $_POST[ 'changing_policy' ] ) && strlen( $_POST[ 'changing_policy' ] ) ) {
				$data[ 'changing_policy' ] = strip_tags( $_POST[ 'changing_policy' ] );
			} else {
				$data[ 'changing_policy' ] = "";
			}
				
			if( empty( $errors ) ) {
// 				print_r($data);
				$res = $this->mProduct->update( $productId, $data );
				//product category
				if( isset( $_POST[ 'product_categories' ] ) )
				foreach ( $_POST[ 'product_categories' ] as $item ) {
					$c = 0;
					if( !empty( $this->page[ 'data' ][ 'productsCategory' ] ) ) 
					foreach ( $this->page[ 'data' ][ 'productsCategory' ] as $category ) {
						if( $item == $category->category_id ) $c++;
					}	
					if( $c == 0 ) {
						//add new
						$this->mProduct->newProductCategory( $productId, $item );
					}
				}
				if( isset( $this->page[ 'data' ][ 'productsCategory' ] ) 
					&& !empty( $this->page[ 'data' ][ 'productsCategory' ] ) )
				foreach ( $this->page[ 'data' ][ 'productsCategory' ] as $category ) {
					$c = 0;
					if( isset( $_POST[ 'product_categories' ] )
						&& !empty( $_POST[ 'product_categories' ] ) )
					foreach ( $_POST[ 'product_categories' ] as $item ) {
						if( $item == $category->category_id ) $c++;
					}
					if( $c == 0 ) {
						//delete
						$this->mProduct->deleteProductCategory( $category->product_category_id );
					}
				}
				
				$this->page[ 'data' ][ 'message' ] = "<span class='text-success'>Cập nhật thông tin sản phẩm thành công</span>";
			} else {
// 				print_r($errors);
				$this->page[ 'data' ][ 'message' ] = "<span class='text-danger'>Cập nhật thông tin sản phẩm không thành công</span>";
			}
// 			$this->page[ 'data' ][ 'dataSubmit' ] = $_POST;
		} else {
// 			$this->page[ 'data' ][ 'dataSubmit' ] = (array)$this->page[ 'data' ][ 'product' ];
		}
		
		$this->page[ 'data' ][ 'product' ] = $this->mProduct->getProduct( $productId );
		
		
// 		print_r($this->page[ 'data' ][ 'product' ]);
		$path = System::$config->baseUrl . $this->page['data']['product']->product_thumbnail;
		if(!@fopen($path,"r")) {
			$this->page['data']['product']->product_thumbnail = "public/images/no_image.jpg";
		}
		
		$this->page[ 'data' ][ 'productsCategory' ] = $this->mProduct->getProductsCategory( $productId );
		$this->page[ 'data' ][ 'dataSubmit' ] = (array)$this->page[ 'data' ][ 'product' ];
		
		$this->page[ 'data' ][ 'productCategories' ] = $this->mProductCategory->getProductCategories();
		$this->page[ 'data' ][ 'productUnits' ] = $this->mProductUnit->getUnits();
		$this->page[ 'data' ][ 'productStatus' ] = $this->mProductStatus->getProductStatus();
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Delete product 
	 * 
	 * @param unknown $productId
	 */
	public function delete( $productId ) {
		$this->page[ 'accessID' ] = PRODUCT_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mProduct->delete( $productId );
		$this->router->router( 
				System::$config->baseUrl 
				. "ban-hang/san-pham" 
				. System::$config->urlSuffix 
		);
	}
}
/*end of file Product.class.php*/
/*location: Product.class.php*/