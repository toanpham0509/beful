<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Promote extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MPromote" );
	}
	public function getPromoteProducts( $promoteId ) {
		echo json_encode( $this->mPromote->getPromoteProducts( $promoteId ) );
	}
	public function getPromoteOrders( $promoteId ) {
		echo json_encode( $this->mPromote->getPromoteOrders( $promoteId ) );
	}
	public function searchCustomer() {
		$data = array();
		if( isset( $_POST[ 'customer_name' ] ) ) {
			$data[ 'customer_name' ] = $_POST[ 'customer_name' ];
		}
		echo json_encode( $this->mCustomer->getCustomers(
			$data
		) );
	}
}
/*end of file Customer.class.php*/
/*location: Customer.class.php*/