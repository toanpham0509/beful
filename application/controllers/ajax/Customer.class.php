<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Customer extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MCustomer" );
	}
	public function getCustomer( $customerID ) {
		echo json_encode( $this->mCustomer->getCustomer( $customerID ) );
	}

	/**
	 *
	 */
	public function searchCustomer() {
		$data = array();
		if( isset( $_REQUEST[ 'customer_name' ] ) ) {
			$data[ 'customer_name' ] = $_REQUEST[ 'customer_name' ];
		}
		$response = array();
		$data = $this->mCustomer->getCustomers($data);
		if(isset($data->item_list))
			$response = $data->item_list;
		else
			$response = array();

		$data = $this->mCustomer->getCustomers(array(
			"phone" => $_REQUEST['customer_name']
		));
		if(isset($data->item_list))
			$response = array_unique(array_merge($data->item_list, $response), SORT_REGULAR);

		echo json_encode($response);
	}
	public function getCustomerByOrderId( $orderId ) {
		$order = $this->mSaleOrder->getOrder( $orderId );
		$customerId = $order->customer_id;
		$this->getCustomer( $customerId );
	}
}
/*end of file Customer.class.php*/
/*location: Customer.class.php*/