<?php
namespace 			App\Controllers;
use	 				BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class ImportOrder extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MBuyOrder" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MWarehouse" );
		$this->load->model("MRefund");
	}

	public function getBuyOrderLineHtml( $orderID ) {
		$this->page[ 'saleOrder' ] = $this->mBuyOrder->getOrderLines( $orderID );
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();	
		$this->load->view( "ajax/OrderLineHtml", $this->page );
	}

	/**
	 * @param $orderID
	 */
	public function getSaleOrderLineHtml( $orderID ) {
		$this->page[ 'saleOrder' ] = $this->mSaleOrder->getOrder( $orderID );
		$this->page[ 'saleOrder' ] = $this->page[ 'saleOrder' ]->order_lines;
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->load->view( "ajax/OrderLineHtml", $this->page );
	}

	/**
	 * @param $orderID
	 */
	public function getSaleOrderLineHtmlNotPrice( $orderID ) {
		$this->page[ 'saleOrder' ] = $this->mSaleOrder->getOrder( $orderID );
		$this->page[ 'saleOrder' ] = $this->page[ 'saleOrder' ]->order_lines;
		if(!empty($this->page[ 'saleOrder' ])) {
			foreach ($this->page['saleOrder'] as $key => $value) {
				$count = $this->mRefund->getCountProductRefundByOutputLineId($value->order_line_id);
				$this->page['saleOrder'][$key]->product_amount_old = $this->page['saleOrder'][$key]->product_amount;
				$this->page['saleOrder'][$key]->refundCount = $count;
				$this->page['saleOrder'][$key]->product_amount = $this->page['saleOrder'][$key]->product_amount_old - $count;
			}
		}
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		$this->load->view( "ajax/OrderLineHtmlNotPrice", $this->page );
	}
}
/*end of file ImportOrder.class.php*/
/*location: ImportOrder.class.php*/