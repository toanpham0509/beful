<?php
namespace 		App\Controllers;
use App\Models\MCustomerPay;
use 				BKFW\Bootstraps\Controller;
use				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class SaleOrder extends Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
 		$this->load->model("MSaleOrder");
		$this->load->model( "MMoney" );
		$this->load->model("MCustomerPay");

		$this->mCustomerPay = new MCustomerPay();
		$this->page['data']['mMoney'] = $this->mMoney;
	}

	/**
	 * @param $customerId
	 */
	public function getOrderHtmlByCustomerId($customerId) {
// 		$dataSearch[ 'customer_id' ] = $customerId;
// 		$this->page['data'][ 'orders' ] = $this->mSaleOrder->getOrders( $dataSearch );
// 		$this->page['data'][ 'orders' ] = $this->page['data'][ 'orders' ]->data;
		
		$this->page['data']['orders'] = $this->mCustomerPay->getCustomerOrdersUnpaid($customerId);
		$this->page['data']['orders'] = $this->page['data']['orders']['data'];

//		if(!empty($this->page['data']['orders'])) {
//			foreach ($this->page['data'][ 'orders' ] as $key => $value) {
//				$this->page['data'][ 'orders' ][$key]["order_price"] = $this->mSaleOrder->getOrderTotalSale(
//						$value["order_id"]
//				);
//				$this->page['data'][ 'orders' ][$key]["have_to_pay"] =
//						$this->page['data'][ 'orders' ][$key]["order_price"] * (
//								100 - $this->page['data'][ 'orders' ][$key]["discount"]
//						) / 100;
//
//			}
//		}
//		print_r($this->page['data']['orders']);

		echo "<table><tbody>";
		if( isset( $this->page['data']['orders'] ) 
			&& !empty($this->page['data']['orders']) ) {
			foreach ($this->page['data']['orders'] as $order) {
				if( !isset($order['order_id']) ) break;
				if($order['have_to_pay'] <= 0) continue;
			?>
			<tr>
				<td>
					<input type="hidden" name="pay_line_id[]" value="0" />
					<input type="hidden" name="order_id[]" value="<?php echo $order['order_id'] ?>" />
					<div class='text-order-code'><a target="_blank" href="<?php
                    	echo System::$config->baseUrl 
							. "ban-hang/don-hang/chinh-sua/"
							. $order["order_id"]
							. System::$config->urlSuffix;
					?>" title=""><?php echo $order["order_code"] ?></a></div>
				</td>
				<td>
					<div class='text-order-date'><?php 
						if( $order["order_date"] > 0 ) echo date("d/m/Y", $order["order_date"]);
					?></div>
				</td>
				<td>
					<div class='text-order-total-sale'><?php 
// 						$money = (int)($order["order_price"] * (100 - $order["discount"]) / 100);
						$money = $order['have_to_pay'];
						echo $this->mMoney->addDotToMoney($money); 
					?>
						<input type="hidden" value="<?php echo $money; ?>" name="haveToPay[]" />
					</div>
				</td>
				<td>
					<input type="hidden" name="amount_paid[]" value="0" />
					<div class="text-money-owed"><?php echo $this->mMoney->addDotToMoney($money); ?></div>
				</td>
				<td><a class="delete_order_line" onclick="deleteOrderLine(this)"> <i
						class="glyphicon glyphicon-trash"></i>
				</a></td>
			</tr>
		<?php
			}
		}
		echo "</tbody></table>";
	}
}
/*end of file SaleOrder.class.php*/
/*location: SaleOrder.class.php*/