<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Product extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MProduct" );
		$this->load->model("MPrice");
		$this->load->model("MBuyPrice");
	}
	/**
	 * Get product
	 * 
	 */
	public function getProduct( $productID, $priceID = 0 ) {
		$data = $this->mProduct->getProduct( $productID );
		if( empty( $data ) ) {
			echo json_encode( null );
			return;
		}
		if( $priceID > 0 )
			$data->product_price = $this->mProduct->getProductPrice( $productID, $priceID );
		else 
			$data->product_price = 0;
		echo json_encode( $data );
	}
	/**
	 * Get products in a sale price
	 */
	public function getJsonProductsInASalePrice($priceId) {
		$lines = $this->mPrice->getPriceLines($priceId);
		$size = sizeof($lines);
		$data = array();
		for($i = 0; $i < $size; $i++) {
			$data[] = $lines[$i]->product_id;
		}
		
		echo " " . implode( " ", $data) . " ";
	}
	/**
	 * Get products in a buy price
	 */
	public function getJsonProductsInABuyPrice($priceId) {
		$lines = $this->mBuyPrice->getBuyPriceLines($priceId);
		$lines = $lines['data']['item_list'];
		$size = sizeof($lines);
		$data = array();
		for($i = 0; $i < $size; $i++) {
			$data[] = $lines[$i]["product_id"];
		}
	
		echo " " . implode( " ", $data) . " ";
	}
	public function getProductPriceInWarehouse($productId, $warehouseId) {
		$price = $this->mProduct->getProductPriceInWarehouse($productId, $warehouseId);
// 		print_r($price);
		if(isset($price->data->item_list[0]))
			echo json_encode($price->data->item_list[0]);
		else 
			echo json_encode(array());
	}
	/**
	 * 
	 * @param unknown $productId
	 * @param unknown $buyPriceId
	 */
	public function getProductBuyPrice($productId, $buyPriceId) {
		$price = $this->mBuyPrice->getBuyProductPrice(array(
			"product_id" => $productId,
			"buy_price_id" => $buyPriceId
		));
		echo json_encode(array( "product_price" => $price['data'][0]['product_price'] ));
	}
}
/*end of file Product.class.php*/
/*location: Product.class.php*/ 