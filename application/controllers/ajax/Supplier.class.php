<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Supplier extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MSupplier" );
		$this->load->model( "MBuyOrder" );
	}
	public function getSupplier( $supplierID ) {
		echo json_encode( $this->mSupplier->getSupplier( $supplierID ) );
	}
	public function getSupplierByBuyOrderId( $orderId ) {
		$data = $this->mBuyOrder->getOrder( $orderId );
		if( isset( $data->supplier_id ) ) {
			$this->getSupplier( $data->supplier_id );
		} else {
			echo null;
		}
	}

	public function searchSupplier() {
		$data = array();
		$response = array();
		if( isset( $_REQUEST[ 'supplier_name' ] ) ) {
			$data[ 'supplier_name' ] = $_REQUEST[ 'supplier_name' ];
		}
		$data = $this->mSupplier->getSuppliers_($data);
		if(isset($data->item_list))
			$response = $data->item_list;
		else
			$response = array();

		$data = $this->mSupplier->getSuppliers_(array(
				"phone" => $_REQUEST['supplier_name']
		));
		if(isset($data->item_list)) {
			if(sizeof($response) == 0) {
				$response = $data->item_list;
			} else {
				$response = array_unique(array_merge($data->item_list, $response), SORT_REGULAR);
			}
		}


		echo json_encode($response);
	}
}
/*end of file Supplier.class.php*/
/*location: Supplier.class.php*/