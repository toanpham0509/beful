<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Test extends  Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library("HttpRequest");
		$this->load->model("MImportWarehouse");
	}
	public function index() {
		$importsInStock = $this->mImportWarehouse->getImportsInStockByProduct(
				2, 2
		);
		// 			echo "<pre>";
		print_r($importsInStock);
	}
}
/*end of file Test.class.php*/
/*location: Test.class.php*/