<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class BuyOrder extends Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
// 		$this->load->model("MBuyOrder");
		$this->load->model( "MSupplierPay" );
		$this->load->model( "MMoney" );
		
		$this->page['data']['mMoney'] = $this->mMoney;
	}
	public function getOrderHtmlBySupplierId($supplierId) {
// 		$dataSearch[ 'supplier_id' ] = $supplierId;
// 		$this->page['data'][ 'orders' ] = $this->mBuyOrder->getOrders( $dataSearch );
// 		if(!isset($this->page['data'][ 'orders' ]->item_list)) die(); 
// 		$this->page['data'][ 'orders' ] = $this->page['data'][ 'orders' ]->item_list;
		
		$this->page['data'][ 'orders' ] = $this->mSupplierPay->getSupplierOrdersUnpaid($supplierId);
		$this->page['data'][ 'orders' ] = $this->page['data'][ 'orders' ]['data'];
		
// 		print_r($this->page['data'][ 'orders' ]);
		
		echo "<table><tbody>";
		if( isset( $this->page['data']['orders'] ) 
			&& !empty($this->page['data']['orders']) ) {
			foreach ($this->page['data']['orders'] as $order) {
				if( !isset($order["input_order_id"]) ) break;
				if($order['have_to_pay'] <= 0) continue;
			?>
			<tr>
				<td>
					<input type="hidden" name="pay_line_id[]" value="0" />
					<input type="hidden" name="order_id[]" value="<?php echo $order["input_order_id"] ?>" />
					<div class='text-order-code'><a target="_blank" href="<?php
                    	echo System::$config->baseUrl 
							. "mua-hang/nhap-kho/chinh-sua/"
							. $order["input_order_id"]
							. System::$config->urlSuffix;
					?>" title=""><?php echo $order["input_code"] ?></a></div>
				</td>
				<td>
					<div class='text-order-date'><?php 
						if( $order["input_date"] > 0 ) echo date("d/m/Y", $order["input_date"]);
					?></div>
				</td>
				<td>
					<div class='text-order-total-sale'><?php 
						$money = $order["have_to_pay"];
						echo $this->mMoney->addDotToMoney($money); 
					?>
						<input type="hidden" value="<?php echo $money; ?>" name="haveToPay[]" />
					</div>
				</td>
				<td>
					<input type="hidden" name="amount_paid[]" value="0" />
					<div class="text-money-owed"><?php echo $this->mMoney->addDotToMoney($money); ?></div>
				</td>
				<td><a class="delete_order_line" onclick="deleteOrderLine(this)"> <i
						class="glyphicon glyphicon-trash"></i>
				</a></td>
			</tr>
		<?php
			}
		}
		echo "</tbody></table>";
	}
}
/*end of file SaleOrder.class.php*/
/*location: SaleOrder.class.php*/