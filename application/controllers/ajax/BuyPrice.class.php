<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class BuyPrice extends Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("MBuyPrice");
	}
	/**
	 * 
	 * @param unknown $supplierId
	 */
	public function getBuyPricesBySupplierId($supplierId) {
		$data = $this->mBuyPrice->getListBuyPrices(array(
			"supplier_id" => $supplierId
		));
// 		print_r($data);
		$data = $data['data']['item_list'];
		echo '<option value="-1">-- Lựa chọn bảng giá mua --</option>';
		if(!empty($data)) {
			foreach ($data as $item) {
				?>
				<option value="<?php 
					echo $item["buy_price_id"];
				?>"><?php 
					echo $item["buy_price_name"] . " (". $item["buy_price_code"] .")";
				?></option>
				<?php
			}
		}
	}
}
/*end of file BuyPrice.class.php*/
/*location: BuyPrice.class.php*/