<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class SearchOrder extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("HttpRequest");
// 		$this->load->model("MBuyOrder");
        $this->load->model("MSaleOrder");
        $this->load->model("MMoney");
        $this->load->model("MProduct");
        $this->load->model("MPrice");
        $this->load->model("MShop");
        $this->load->model("MSaleOrder");
        $this->load->model("MBuyOrder");
        $this->load->model("MImportWarehouse");
        $this->load->model("MTransferWarehouse");
        $this->load->model("MRefund");
        $this->load->model("MSupplier");
        $this->load->model("MBuyPrice");
        $this->load->model("MCustomer");
        $this->load->model("MSearch");

        $this->page['data']['mMoney'] = $this->mMoney;
    }

    public function getAllOrder($data = array()) {
//        $allOrder = $this->mSaleOrder->getOrders();
        $post = array();
//        echo '<pre>';
//        print_r($allpage);
//        echo '</pre>';

        if (isset($_POST)) {
//            $hint = array();
//            $size_hints = '';
            $condition = " ";
            $post[0] = $_POST;
//            $data = $allOrder->data;
//            $size = $data->order_count;
            //Search $order_code
            if (isset($post[0]['order_id']) && $post[0]['order_id'] != '') {
                $data["order_code"] = $post[0]['order_id'];
            }

            //Search $date
            if (isset($post[0]['date_from']) && $post[0]['date_from'] != '') {
                $date_from = strtotime($post[0]['date_from']);
                $data["date_from"] = $date_from;
                if ($post[0]['date_to'] == '') {
                    $date_to = time();
                    $data["date_to"] = $date_to;
                } else {
                    $date_to = strtotime($post[0]['date_to']) + 86399;
                    $data["date_to"] = $date_to;
                }
            }

            //Search customer_name
            if (isset($post[0]['customer_name']) && $post[0]['customer_name'] != '') {
                $customer_name = $post[0]['customer_name'];
                $data["customer_name"] = $customer_name;
            }

            //Search sale_emplloyee
            if (isset($post[0]['sale_employee']) && $post[0]['sale_employee'] != '') {
                $sale_employee = $post[0]['sale_employee'];
                $data["salesperson_name"] = $sale_employee;
            }

//            Search order_status
            if (isset($post[0]['order_status']) && $post[0]['order_status'] != '') {
                $order_status = $post[0]['order_status'];
                if ($order_status == "hoàn thành") {
                    $order_status_id = 3;
                } else if ($order_status == "đang chờ") {
                    $order_status_id = 2;
                }
                $data["order_status"] = $order_status_id;
            }
            $size_hints = 0;
            if (isset($data)) {
                $hint = $this->mSearch->searchAllOrder($data);
                $size_hints = sizeof($hint);
            }
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/don-hang/chinh-sua/"
                    . $hint[$i]['order_id']
                    . System::$config->urlSuffix;
                    echo "' title='Xem chi tiết đơn hàng'>";
                    echo $hint[$i]['order_code'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo date("H:i:s d/m/Y", $hint[$i]['order_date']);
                    echo "</td>";
                    echo "<td>" . $hint[$i]['customer_name'] . "</td>";
                    echo "<td>" . $hint[$i]['salesperson_name'] . "</td>";
                    echo "<td>";
                    echo $this->mMoney->addDotToMoney((int) $hint[$i]['order_price'] - ($hint[$i]['order_price'] * ($hint[$i]['discount'] / 100)));
                    echo"</td>";
                    echo "<td>";
                    if ($hint[$i]['order_status'] == 2)
                        echo '<span class="text-danger">Đang chờ</span></span>';
                    else
                        echo '<span class="text-success">Hoàn thành</span></span>';
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]['order_status'] == 2) {
                        echo '<a class="delete_order_line" 
                            		onclick="return confirm(' . "'Bạn có chắc chắn muốn xóa đơn hàng này không?'" . ')" 
                            		href="';
                        echo System::$config->baseUrl
                        . "ban-hang/don-hang/xoa/"
                        . $hint[$i]['order_id']
                        . System::$config->urlSuffix;
                        echo '">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>';
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllProduct($data = array()) {
        $post = array();

        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $product_id
            if (isset($post[0]['product_id']) && $post[0]['product_id'] != '') {
                $product_id = $post[0]['product_id'];
                $data["product_code"] = $product_id;
            }

            //Search $product_name
            if (isset($post[0]['product_name']) && $post[0]['product_name'] != '') {
                $product_name = $post[0]['product_name'];
                $data["product_name"] = $product_name;
            }

            //Search $product_qrcode
            if (isset($post[0]['product_qrcode']) && $post[0]['product_qrcode'] != '') {
                $product_qrcode = $post[0]['product_qrcode'];
                $data["product_barcode"] = $product_qrcode;
            }

            //Search $unit_name
            if (isset($post[0]['product_unit']) && $post[0]['product_unit'] != '') {
                $product_unit = $post[0]['product_unit'];
                $data["unit_name"] = $product_unit;
            }

            if (isset($data)) {
                $hint = $this->mSearch->searchAllProduct($data);
                $size_hints = sizeof($hint);
            }
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/san-pham/chinh-sua/"
                    . $hint[$i]['product_id']
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa sản phẩm'>";
                    echo $hint[$i]['product_code'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/san-pham/chinh-sua/"
                    . $hint[$i]['product_id']
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa sản phẩm'>";
                    echo $hint[$i]['product_name'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]['barcode'] . "</td>";
                    echo "<td>" . $hint[$i]['unit_name'] . "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllPriceName($data = array()) {
        $post = array();

        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $price_name
            if (isset($post[0]['price_name']) && $post[0]['price_name'] != '') {
                $price_name = $post[0]['price_name'];
                $data['price_name'] = $price_name;
            }

            //Search $start_date
            if (isset($post[0]['start_date']) && $post[0]['start_date'] != '') {
                $start_date = strtotime($post[0]['start_date']);
                $data['time_start'] = $start_date;
            }

            //Search $end_date
            if (isset($post[0]['end_date']) && $post[0]['end_date'] != '') {
                $end_date = strtotime($post[0]['end_date']);
                $data['time_end'] = $end_date;
            }

            if (isset($data)) {
                $hint = $this->mSearch->searchAllPriceName($data);
                $size_hints = sizeof($hint);
            }
            //Show reusult
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/bao-gia/chinh-sua//"
                    . $hint[$i]['price_id']
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa sản phẩm'>";
                    echo $hint[$i]['price_name'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]['time_start'] > 0)
                        echo date("d/m/Y", $hint[$i]['time_start']);
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]['time_end'] > 0)
                        echo date("d/m/Y", $hint[$i]['time_end']);
                    echo "</td>";
                    echo "<td>";
                    echo '<a class="delete_order_line" 
                            		onclick="return confirm(' . "'Bạn có chắc chắn muốn xóa bảng giá này không?'" . ')" 
                            		href="';
                    echo System::$config->baseUrl
                    . "ban-hang/bao-gia/xoa/"
                    . $hint[$i]['price_id']
                    . System::$config->urlSuffix;
                    echo '">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>';

                    echo "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllShop($data) {
        $allShop = $this->mShop->getShops(array());
        $post = array();

        if (isset($_POST)) {
            $hint = array();
            $size_hints = '';
            $post[0] = $_POST;
            $data = $allShop->item_list;
            $size = $allShop->num_rows_total;

            //Search $shop_code
            if (isset($post[0]['shop_code']) && $post[0]['shop_code'] != '') {
                $shop_code = $post[0]['shop_code'];
                $shop_code_len = strlen($shop_code);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($shop_code, substr($data[$i]->shop_code, 0, $shop_code_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $shop_name
            if (isset($post[0]['shop_name']) && $post[0]['shop_name'] != '') {
                $shop_name = $post[0]['shop_name'];
                $shop_name_len = strlen($shop_name);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($shop_name, substr($data[$i]->shop_name, 0, $shop_name_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $shop_address
            if (isset($post[0]['shop_address']) && $post[0]['shop_address'] != '') {
                $shop_address = $post[0]['shop_address'];
                $shop_address_len = strlen($shop_address);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($shop_address, substr($data[$i]->shop_address, 0, $shop_address_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $shop_telephone
            if (isset($post[0]['shop_phone']) && $post[0]['shop_phone'] != '') {
                $shop_phone = $post[0]['shop_phone'];
                $shop_phone_len = strlen($shop_phone);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($shop_phone, substr($data[$i]->phone, 0, $shop_phone_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $warehouse
            if (isset($post[0]['warehouse']) && $post[0]['warehouse'] != '') {
                $warehouse = $post[0]['warehouse'];
                $warehouse_len = strlen($warehouse);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($warehouse, substr($data[$i]->warehouse[0]->warehouse_name, 0, $warehouse_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $management
            if (isset($post[0]['management']) && $post[0]['management'] != '') {
                $management = $post[0]['management'];
                $management_len = strlen($management);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($management, substr($data[$i]->shop_manager_name, 0, $management_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Show result
            $size_hints = sizeof($hint);
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/cua-hang/chinh-sua/"
                    . $hint[$i]->shop_id
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa cửa hàng'>";
                    echo $hint[$i]->shop_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ban-hang/cua-hang/chinh-sua/"
                    . $hint[$i]->shop_id
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa cửa hàng'>";
                    echo $hint[$i]->shop_name;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]->shop_address . "</td>";
                    echo "<td>" . $hint[$i]->phone . "</td>";
                    echo "<td>";
                    if (isset($hint[$i]->warehouse) && !empty($hint[$i]->warehouse)) {
                        foreach ($hint[$i]->warehouse as $warehouse) {
                            echo "<a href=";
                            echo System::$config->baseUrl
                            . "ql-kho/kho-hang/chinh-sua/"
                            . $warehouse->warehouse_id
                            . System::$config->urlSuffix;
                            echo '" target="_blank">';
                            echo $warehouse->warehouse_name;
                            echo '</a><br />';
                        }
                    }
                    echo "</td>";
                    echo "<td>" . $hint[$i]->shop_manager_name . "</td>";
                    echo "<td></td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllInputOrder() {
        $post = array();

        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $providers
            if (isset($post[0]['providers']) && $post[0]['providers'] != '') {
                $providers = $post[0]['providers'];
                $data['supplier_name'] = $providers;
            }

            //Search $order_number_buy
            if (isset($post[0]['order_number_buy']) && $post[0]['order_number_buy'] != '') {
                $order_number_buy = $post[0]['order_number_buy'];
                $data['input_code'] = $order_number_buy;
            }

            //Search $cost_non_tax
            if (isset($post[0]['cost_non_tax']) && $post[0]['cost_non_tax'] != '') {
                $cost_non_tax = $post[0]['cost_non_tax'];
                $data['total_sale_no_tax'] = $cost_non_tax;
            }

            //Search $total_price
            if (isset($post[0]['total_price']) && $post[0]['total_price'] != '') {
                $total_price = $post[0]['total_price'];
                $data['total_sale'] = $total_price;
            }

            //Search $order_date
            if (isset($post[0]['order_date']) && $post[0]['order_date'] != '') {
                $order_date = strtotime($post[0]['order_date']);
                $data['input_date'] = $order_date;
            }

            //Search $estimate_date_buy
            if (isset($post[0]['estimate_date_buy']) && $post[0]['estimate_date_buy'] != '') {
                $estimate_date_buy = strtotime($post[0]['estimate_date_buy']);
                $data['intended_date'] = $estimate_date_buy;
            }

            //Search order_status
            if (isset($post[0]['status']) && $post[0]['status'] != '') {
                $order_status = $post[0]['status'];
                $order_status = strtolower($order_status);
                $order_status_id = '';
                if ($order_status == "hoàn thành") {
                    $order_status_id = 3;
                } else if ($order_status == "đang chờ") {
                    $order_status_id = 2;
                }
                $data['order_status'] = $order_status_id;
            }

            $size_hints = 0;
            if (isset($data)) {
                $hint = $this->mSearch->searchAllInputOrder($data);
                $size_hints = sizeof($hint);
            }

            //Show result
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/don-hang/chinh-sua/"
                    . $hint[$i]['order_id']
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa'>";
                    echo $hint[$i]['order_code'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]['supplier_name'] . "</td>";
                    echo "<td>";
                    if ($hint[$i]['order_date'] > 0)
                        echo date("d/m/Y", $hint[$i]['order_date']);
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]['intended_date'] > 0)
                        echo date("d/m/Y", $hint[$i]['intended_date']);
                    echo "</td>";
                    echo "<td>";
                    echo $this->mMoney->addDotToMoney((int) $hint[$i]['total_sale_no_tax']);
                    echo "</td>";
                    echo "<td>";
                    echo $this->mMoney->addDotToMoney((int) $hint[$i]['total_sale']);
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]['status'] == 2)
                        echo '<span class="text-danger">Đang chờ</span></span>';
                    else
                        echo '<span class="text-success">Hoàn thành</span></span>';
                    echo "</td>";
                    echo "<td></td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllImportWarehouse() {
        $allImportWarehouse = $this->mImportWarehouse->getImportWarehouses();
        $post = array();


        if (isset($_POST)) {
            $hint = array();
            $size_hints = '';
            $post[0] = $_POST;
            $data = $allImportWarehouse->item_list;
            $size = $allImportWarehouse->num_rows_total;

            //Search $date
            if (isset($post[0]['date_from']) && $post[0]['date_from'] != '') {
                $date_from = strtotime($post[0]['date_from']);
                if ($post[0]['date_to'] == '') {
                    $date_to = time();
                } else {
                    $date_to = strtotime($post[0]['date_to']);
                }
                for ($i = 0; $i < $size; $i++) {
                    if ($date_from <= $data[$i]->import_date && $data[$i]->import_date <= $date_to) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $order_number_buy
            if (isset($post[0]['order_number_buy']) && $post[0]['order_number_buy'] != '') {
                $order_number_buy = $post[0]['order_number_buy'];
                $order_number_buy_len = strlen($order_number_buy);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($order_number_buy, substr($data[$i]->purchase_order_code, 0, $order_number_buy_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $import_code
            if (isset($post[0]['import_code']) && $post[0]['import_code'] != '') {
                $import_code = $post[0]['import_code'];
                $import_code_len = strlen($import_code);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($import_code, substr($data[$i]->import_code, 0, $import_code_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $providers
            if (isset($post[0]['providers']) && $post[0]['providers'] != '') {
                $providers = $post[0]['providers'];
                $providers_len = strlen($providers);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($providers, substr($data[$i]->supplier_name, 0, $providers_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $cost_non_tax
            if (isset($post[0]['cost_non_tax']) && $post[0]['cost_non_tax'] != '') {
                $cost_non_tax = $post[0]['cost_non_tax'];
                $cost_non_tax_len = strlen($cost_non_tax);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($cost_non_tax, substr($data[$i]->total_sale, 0, $cost_non_tax_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $total_price
            if (isset($post[0]['total_price']) && $post[0]['total_price'] != '') {
                $total_price = $post[0]['total_price'];
                $total_price_len = strlen($total_price);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($total_price, substr($data[$i]->total_sale, 0, $total_price_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Show result
            $size_hints = sizeof($hint);
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->import_date > 0)
                        echo date("d/m/Y", $hint[$i]->import_date);
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/nhap-kho/chinh-sua/"
                    . $hint[$i]->import_id
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa'>";
                    echo $hint[$i]->import_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]->supplier_name . "</td>";
                    echo "<td>";
                    echo $this->mMoney->addDotToMoney((int) $hint[$i]->total_sale);
                    echo "</td>";
                    echo "<td>";
                    echo $this->mMoney->addDotToMoney((int) $hint[$i]->total_sale);
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/don-hang/chinh-sua/"
                    . $hint[$i]->purchase_order
                    . System::$config->urlSuffix;
                    echo "' title='Chỉnh sửa'>";
                    echo $hint[$i]->purchase_order_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->status == 2)
                        echo '<span class="text-danger">Đang chờ</span></span>';
                    else
                        echo '<span class="text-success">Hoàn thành</span></span>';
                    echo "</td>";
                    echo "<td></td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllTranfer() {
        $allTranfer = $this->mTransferWarehouse->getTransfers();
        $post = array();


        if (isset($_POST)) {
            $hint = array();
            $size_hints = '';
            $post[0] = $_POST;
            $data = $allTranfer->item_list;
            $size = $allTranfer->num_rows_total;

            //Search $tranfer_id
            if (isset($post[0]['tranfer_id']) && $post[0]['tranfer_id'] != '') {
                $tranfer_id = $post[0]['tranfer_id'];
                $tranfer_id_len = strlen($tranfer_id);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($tranfer_id, substr($data[$i]->transfer_code, 0, $tranfer_id_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $warehouse_from
            if (isset($post[0]['warehouse_from']) && $post[0]['warehouse_from'] != '') {
                $warehouse_from = $post[0]['warehouse_from'];
                $warehouse_from_len = strlen($warehouse_from);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($warehouse_from, substr($data[$i]->warehouse_from_name, 0, $warehouse_from_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $warehouse_to
            if (isset($post[0]['warehouse_to']) && $post[0]['warehouse_to'] != '') {
                $warehouse_to = $post[0]['warehouse_to'];
                $warehouse_to_len = strlen($warehouse_to);
                for ($i = 0; $i < $size; $i++) {
                    if (stristr($warehouse_to, substr($data[$i]->warehouse_to_name, 0, $warehouse_to_len))) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Search $date
            if (isset($post[0]['date_from']) && $post[0]['date_from'] != '') {
                $date_from = strtotime($post[0]['date_from']);
                if ($post[0]['date_to'] == '') {
                    $date_to = time();
                } else {
                    $date_to = strtotime($post[0]['date_to']);
                }
                for ($i = 0; $i < $size; $i++) {
                    if ($date_from <= $data[$i]->transfer_date && $data[$i]->transfer_date <= $date_to) {
                        $hint[] = $data[$i];
                    }
                }
            }

            //Show result
            $size_hints = sizeof($hint);
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/chuyen-kho/chinh-sua/"
                    . $hint[$i]->transfer_id
                    . System::$config->urlSuffix;
                    echo "' title='Xem chi tiết, chỉnh sửa'>";
                    echo $hint[$i]->transfer_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->transfer_date > 0)
                        echo date("d/m/Y", $hint[$i]->transfer_date);
                    echo "</td>";
                    echo "<td>";
                    echo "<a target='_blank' href='";
                    echo System::$config->baseUrl
                    . "ql-kho/kho-hang/chinh-sua/"
                    . $hint[$i]->warehouse_from_id
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]->warehouse_from_name;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo "<a target='_blank' href='";
                    echo System::$config->baseUrl
                    . "ql-kho/kho-hang/chinh-sua/"
                    . $hint[$i]->warehouse_to_id
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]->warehouse_to_name;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]->transfer_note . "</td>";
                    echo "<td>";
                    if ($hint[$i]->status == 2)
                        echo '<span class="text-danger">Đang chờ</span></span>';
                    else
                        echo '<span class="text-success">Hoàn thành</span></span>';
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->status == 2) {
                        echo '<a class="delete_order_line" 
                            		onclick="return confirm(' . "'Bạn có chắc chắn muốn xóa đơn hàng này không?'" . ')" 
                            		href="';
                        echo System::$config->baseUrl
                        . "mua-hang/chuyen-kho/xoa/"
                        . $hint[$i]->transfer_id
                        . System::$config->urlSuffix;
                        echo '">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>';
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllRefund() {
        $allRefund = $this->mRefund->getRefunds();
        $post = array();


        if (isset($_POST)) {
            $hint = array();
            $size_hints = '';
            $post[0] = $_POST;
            $data['refund'] = $allRefund->item_list;
            $size = $allRefund->num_rows_total;

            //Search $date
            if (isset($post[0]['date_from']) && $post[0]['date_from'] != '') {
                $date_from = strtotime($post[0]['date_from']);
                if ($post[0]['date_to'] == '') {
                    $date_to = time();
                } else {
                    $date_to = strtotime($post[0]['date_to']);
                }
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refund'] as $refund) {
                    $data['startList'] ++;
                    if ($date_from <= $refund->refund_date && $refund->refund_date <= $date_to) {
                        $hint[] = $refund;
                    }
                }

//                for ($i = 0; $i < $size; $i++) {
//                    if ($date_from <= $data[$i]->refund_date && $data[$i]->refund_date <= $date_to) {
//                        $hint[] = $data[$i];
//                    }
//                }
            }

            //Search $import_code
            if (isset($post[0]['import_code']) && $post[0]['import_code'] != '') {
                $import_code = $post[0]['import_code'];
                $import_code_len = strlen($import_code);
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refund'] as $refund) {
                    $data['startList'] ++;
                    if (stristr($import_code, substr($refund->import_code, 0, $import_code_len))) {
                        $hint[] = $refund;
                    }
                }
//                for ($i = 0; $i < $size; $i++) {
//                    if (stristr($import_code, substr($data[$i]->import_code, 0, $import_code_len))) {
//                        $hint[] = $data[$i];
//                    }
//                }
            }

            //Search $customer_name
            if (isset($post[0]['customer_name']) && $post[0]['customer_name'] != '') {
                $customer_name = $post[0]['customer_name'];
                $customer_name_len = strlen($customer_name);
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refund'] as $refund) {
                    $data['startList'] ++;
                    if (stristr($customer_name, substr($refund->customer_name, 0, $customer_name_len))) {
                        $hint[] = $refund;
                    }
                }
//                for ($i = 0; $i < $size; $i++) {
//                    if (stristr($customer_name, substr($data[$i]->customer_name, 0, $customer_name_len))) {
//                        $hint[] = $data[$i];
//                    }
//                }
            }

            //Search $order_number_buy
            if (isset($post[0]['order_number_buy']) && $post[0]['order_number_buy'] != '') {
                $order_number_buy = $post[0]['order_number_buy'];
                $order_number_buy_len = strlen($order_number_buy);
                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refund'] as $refund) {
                    $data['startList'] ++;
                    if (stristr($order_number_buy, substr($refund->order_code, 0, $order_number_buy_len))) {
                        $hint[] = $refund;
                    }
                }
//                for ($i = 0; $i < $size; $i++) {
//                    if (stristr($order_number_buy, substr($data[$i]->order_code, 0, $order_number_buy_len))) {
//                        $hint[] = $data[$i];
//                    }
//                }
            }

            //Search $warehouse
            if (isset($post[0]['warehouse']) && $post[0]['warehouse'] != '') {
                $warehouse = $post[0]['warehouse'];
                $warehouse_len = strlen($warehouse);

                $data['startList'] = (isset($data['startList'])) ? $data['startList'] : 0;
                foreach ($data['refund'] as $refund) {
                    $data['startList'] ++;
                    if (stristr($warehouse, substr($refund->warehouse_name, 0, $warehouse_len))) {
                        $hint[] = $refund;
                    }
                }

//                for ($i = 0; $i < $size; $i++) {
//                    if (stristr($warehouse, substr($data[$i]->warehouse[0]->warehouse_name, 0, $warehouse_len))) {
//                        $hint[] = $data[$i];
//                    }
//                }
            }

            //Show result
            $size_hints = sizeof($hint);
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->refund_date > 0)
                        echo date("d/m/Y", $hint[$i]->refund_date);
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/nhap-kho/chinh-sua/"
                    . $hint[$i]->import_id
                    . System::$config->urlSuffix
                    . "?refund=1&refund_id="
                    . $hint[$i]->refund_id
                    ;
                    echo "' title=''>";
                    echo $hint[$i]->import_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]->customer_name . "</td>";
                    echo "<td>";
                    echo "<a target='_blank' href='";
                    echo System::$config->baseUrl
                    . "qban-hang/don-hang/chinh-sua/"
                    . $hint[$i]->order_id
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]->order_code;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo "<a target='_blank' href='";
                    echo System::$config->baseUrl
                    . "ql-kho/kho-hang/chinh-sua/"
                    . $hint[$i]->warehouse_id
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]->warehouse_name;
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->status == 2)
                        echo '<span class="text-danger">Đang chờ</span></span>';
                    else
                        echo '<span class="text-success">Hoàn thành</span></span>';
                    echo "</td>";
                    echo "<td>";
                    if ($hint[$i]->status == 2) {
                        echo '<a class="delete_order_line" 
                            		onclick="return confirm(' . "'Bạn có chắc chắn muốn xóa đơn hàng này không?'" . ')" 
                            		href="';
                        echo System::$config->baseUrl
                        . "mua-hang/tra-ve/xoa/"
                        . $hint[$i]->refund_id
                        . System::$config->urlSuffix;
                        echo '">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>';
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllSupplier() {
        $post = array();


        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $providers_name
            if (isset($post[0]['providers_name']) && $post[0]['providers_name'] != '') {
                $providers_name = $post[0]['providers_name'];
                $data['supplier_name'] = $providers_name;
            }

            //Search $providers_address
            if (isset($post[0]['providers_address']) && $post[0]['providers_address'] != '') {
                $providers_address = $post[0]['providers_address'];
                $data['supplier_address'] = $providers_address;
            }

            //Search $providers_phone
            if (isset($post[0]['providers_phone']) && $post[0]['providers_phone'] != '') {
                $providers_phone = $post[0]['providers_phone'];
                $data['supplier_phone'] = $providers_phone;
            }

            //Search $providers_code_tax
            if (isset($post[0]['providers_code_tax']) && $post[0]['providers_code_tax'] != '') {
                $providers_code_tax = $post[0]['providers_code_tax'];
                $data['supplier_tax_code'] = $providers_code_tax;
            }

            //Search $providers_contact
            if (isset($post[0]['providers_contact']) && $post[0]['providers_contact'] != '') {
                $providers_contact = $post[0]['providers_contact'];
                $data['contact_person_name'] = $providers_contact;
            }

            //Search $providers_email
            if (isset($post[0]['providers_email']) && $post[0]['providers_email'] != '') {
                $providers_email = $post[0]['providers_email'];
                $data['contact_person_email'] = $providers_email;
            }

            //Search $providers_telephone
            if (isset($post[0]['providers_telephone']) && $post[0]['providers_telephone'] != '') {
                $providers_telephone = $post[0]['providers_telephone'];
                $data['contact_person_phone'] = $providers_telephone;
            }

            if (isset($data)) {
                $hint = $this->mSearch->searchAllSuppiler($data);
                $size_hints = sizeof($hint);
            }
            //Show reusult
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/nha-cung-cap/chinh-sua/"
                    . $hint[$i]['supplier_id']
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]['supplier_name'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]['address'] . "</td>";
                    echo "<td>" . $hint[$i]['fax'] . " / " . $hint[$i]['phone'] . "</td>";
                    echo "<td>" . $hint[$i]['tax_code'] . "</td>";
                    echo "<td>" . $hint[$i]['contact_person_name'] . "</td>";
                    echo "<td>" . $hint[$i]['contact_person_email'] . "</td>";
                    echo "<td>" . $hint[$i]['contact_person_phone'] . "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllBuyPrice() {
        $post = array();


        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $providers
            if (isset($post[0]['providers']) && $post[0]['providers'] != '') {
                $providers = $post[0]['providers'];
                $data['supplier_name'] = $providers;
            }

            //Search $price_name
            if (isset($post[0]['price_name']) && $post[0]['price_name'] != '') {
                $price_name = $post[0]['price_name'];
                $data['buy_price_name'] = $price_name;
            }

            //Search $start_date
            if (isset($post[0]['start_date']) && $post[0]['start_date'] != '') {
                $start_date = strtotime($post[0]['start_date']);
                $data['time_start'] = $start_date;
            }

            //Search $end_date
            if (isset($post[0]['end_date']) && $post[0]['end_date'] != '') {
                $end_date = strtotime($post[0]['end_date']);
                $data['time_end'] = $end_date;
            }

            if (isset($data)) {
                $hint = $this->mSearch->searchAllBuyPrice($data);
                $size_hints = sizeof($hint);
            }

            //Show reusult
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "mua-hang/bang-gia-mua/chinh-sua/"
                    . $hint[$i]["buy_price_id"]
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]["buy_price_name"] . "(" . $hint[$i]["buy_price_code"] . ")";
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo "<a target='_blank' href='";
                    echo System::$config->baseUrl
                    . "mua-hang/nha-cung-cap/chinh-sua/"
                    . $hint[$i]['supplier_id']
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]['supplier_name'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>";
                    echo date("d/m/Y", $hint[$i]["time_start"]);
                    echo "</td>";
                    echo "<td>";
                    echo date("d/m/Y", $hint[$i]["time_end"]);
                    echo "</td>";
                    echo "<td>";
                    echo '<a    onclick="return confirm(' . "'Bạn có chắc chắn muốn xóa bảng giá này không?'" . ')" 
                                href="';
                    echo System::$config->baseUrl
                    . "mua-hang/chuyen-kho/xoa/"
                    . $hint[$i]->transfer_id
                    . System::$config->urlSuffix;
                    echo '">
                                    <i class="glyphicon glyphicon-trash"></i>
                       			</a>';

                    echo "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

    public function getAllCustomer() {
        $post = array();

        if (isset($_POST)) {
            $size_hints = 0;
            $post[0] = $_POST;

            //Search $customer_name
            if (isset($post[0]['customer_name']) && $post[0]['customer_name'] != '') {
                $customer_name = $post[0]['customer_name'];
                $data['customer_name'] = $customer_name;
            }

            //Search $providers_address
            if (isset($post[0]['providers_address']) && $post[0]['providers_address'] != '') {
                $providers_address = $post[0]['providers_address'];
                $data['cusotmer_address'] = $providers_address;
            }

            //Search $providers_phone
            if (isset($post[0]['providers_phone']) && $post[0]['providers_phone'] != '') {
                $providers_phone = $post[0]['providers_phone'];
                $data['customer_phone'] = $providers_phone;
            }

            //Search $providers_contact
            if (isset($post[0]['providers_contact']) && $post[0]['providers_contact'] != '') {
                $providers_contact = $post[0]['providers_contact'];
                $data['contact_person_name'] = $providers_contact;
            }

            //Search $providers_email
            if (isset($post[0]['providers_email']) && $post[0]['providers_email'] != '') {
                $providers_email = $post[0]['providers_email'];
                $data['contact_person_email'] = $providers_email;
            }

            //Search $providers_telephone
            if (isset($post[0]['providers_telephone']) && $post[0]['providers_telephone'] != '') {
                $providers_telephone = $post[0]['providers_telephone'];
                $data['contact_person_phone'] = $providers_telephone;
            }

            if (isset($data)) {
                $hint = $this->mSearch->searchAllCustomer($data);
                $size_hints = sizeof($hint);
            }

            //Show reusult
            if ($size_hints == 0) {
                echo '<tr id="result_ajax">';
                echo '<td>';
                echo "Không có dữ liệu cần tìm";
                echo '</td>';
                echo '</tr>';
            } else {
                echo '<tbody>';
                for ($i = 0; $i < $size_hints; $i++) {
                    echo "<tr id='result_ajax'>";
                    echo "<td>";
                    echo $i + 1;
                    echo "</td>";
                    echo "<td>";
                    echo "<a href='";
                    echo System::$config->baseUrl
                    . "ql-khach-hang/khach-hang/chinh-sua/"
                    . $hint[$i]['customer_id']
                    . System::$config->urlSuffix;
                    echo "'>";
                    echo $hint[$i]['customer_name'];
                    echo "</a>";
                    echo "</td>";
                    echo "<td>" . $hint[$i]['address'] . "</td>";
                    echo "<td>" . $hint[$i]['fax'] . " / " . $hint[$i]['phone'] . "</td>";
                    echo "<td></td>";
                    echo "<td>" . $hint[$i]['contact_person_name'] . "</td>";
                    echo "<td>" . $hint[$i]['contact_person_phone'] . "</td>";
                    echo "<td>" . $hint[$i]['contact_person_email'] . "</td>";
                    echo "</tr>";
                }
                echo '</tbody>';
            }
        }
    }

}
