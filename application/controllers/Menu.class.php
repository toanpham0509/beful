<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Menu extends Controller {
	public function __construct() {
		parent::__construct();
	}
}
/*end of file Menu.class.php*/
/*location: Menu.class.php*/