<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
use					BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class LogOut extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
		$this->load->model("MUser");
	}
	public function index() {
		$this->mUser->logOut();
		$this->router->router(
				System::$config->baseUrl
				. "dang-nhap"
				. System::$config->urlSuffix
		);
	}
}
/*end of file LogOut.class.php*/
/*location: LogOut.class.php*/