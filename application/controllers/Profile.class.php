<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Profile extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
	}
	public function index(){
		$this->page[ 'pageTitle' ] = "Thiết lập tài khoản";
		$this->page[ 'viewPath' ] = "Profile";
		
		
		$this->load->view( "Template", array( "data" =>  $this->page ) );
	}
}
/*end of file Profile.class.php*/
/*location: Profile.class.php*/