<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			KFW\Libraries\HttpRequest;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class User extends Controller {
	/**
	 * 
	 * @var 	boolean
	 */
	private $isLogin;
	/**
	 * 
	 * @var unknown
	 */
	private $userName;
	/**
	 * 
	 * @var unknown
	 */
	private $userID;
	/**
	 * 
	 * @var unknown
	 */
	private $userEmail;
	/**
	 * 
	 * @var unknown
	 */
	private $userPassword;

	/**
	 * @var Array
	 */
	public $userInfo;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->model( "MUser" );
		$this->userInfo = $this->mUser->getUserInfo();
// 		print_r($this->userInfo);
		if( empty( $this->userInfo ) ) {
			$this->router->router(
				System::$config->baseUrl
				. "dang-nhap"
				. System::$config->urlSuffix
			);
		} else {
			$this->userInfo['id'] = $this->userInfo['user_id'];
		}
	}

	/**
	 * @param $accessID
	 */
	public function access( $accessID ) {
		if( $accessID == null )
			$accessed =  true;
		else  {
			$accessID = "-" . $accessID . "-";
			
			// kiểm tra truy cập
			if( strpos( $this->userInfo[ 'user_access' ], $accessID) > 0 )
				$accessed = true;
			else 
				$accessed = false;
		}
		
		if( $accessed == false ) {
			//không được phép truy cập
			$this->load->view("403");
			exit();
			die();
		}
	}

	/**
	 * @param $accessID
	 * @return bool
	 */
	public function isAccessAllowed( $accessID ) {
		if( $accessID == null )
			$accessed =  true;
		else  {
			$accessID = "-" . $accessID . "-";

			// kiểm tra truy cập
			if( strpos( $this->userInfo[ 'user_access' ], $accessID) > 0 )
				$accessed = true;
			else
				$accessed = false;
		}

		return $accessed;
	}
}
/*end of file User.class.php*/
/*location: User.class.php*/