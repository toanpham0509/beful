<?php

namespace 		App\Controllers;

use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class ReportExport extends Controller {

    /**
     *
     */
    public function __construct() {
        parent::__construct();

        $this->load->library("HttpRequest");
        $this->load->model("MMenu");
        $this->load->model("MSalesPeople");
        $this->load->model("MCustomer");
        $this->load->model("MWarehouse");
        $this->load->model("MProduct");
        $this->load->model("MProductCategory");
        $this->load->model("MPayType");
        $this->load->model("MReport");
        $this->load->model("MMoney");

        $this->page['menuID'] = 6;
        $this->page['menuLeftCurrent'] = $this->mMenu->getMenu(57);
        $this->page['menus'] = $this->mMenu->getMenus();
        $this->page['menuLefts'] = $this->mMenu->getMenus($this->page['menuID']);
        $this->page['data']['salesPeople'] = $this->mSalesPeople->getSalesPeople();
        $this->page['data']['customers'] = $this->mCustomer->getAllCustomer();
        $this->page['data']['warehouses'] = $this->mWarehouse->getWarehouses();
        $this->page['data']['categories'] = $this->mProductCategory->getProductCategories();

        $this->page['data']['payTypes'] = $this->mPayType->getAllPayTypes();
        $this->page['data']['payTypes'] = $this->page['data']['payTypes']['data'];

        $this->load->controller("User");
        $this->page['userInfo'] = $this->user->userInfo;
    }

    /**
     * Index
     */
    public function index() {
        $this->page['data']['pageTitle'] = "Bảng kê xuất kho";
        $this->page['data']['urlPrint'] = System::$config->baseUrl
                . "bao-cao/bang-ke-xuat-kho/in"
                . System::$config->urlSuffix;


        if (empty($_REQUEST)) {
            $this->page['viewPath'] = "report/includes/FormSelect";
        } else {
            $this->page['viewPath'] = $this->page['menuLeftCurrent']['view_path'];
        }

        $this->load->view("Template", array("data" => $this->page));
    }

    /**
     * Print
     */
    public function print1() {
        $this->page['viewPath'] = "report/warehouse/Export";

        $dataSearch = array();
        if (isset($_REQUEST['year']) && filter_var($_REQUEST['year'], FILTER_VALIDATE_INT)) {
            $dataSearch['year'] = $_REQUEST['year'];
        }
        if (isset($_REQUEST['sale_person_id']) && filter_var($_REQUEST['sale_person_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['sale_person_id'] = $_REQUEST['sale_person_id'];
        }
        if (isset($_REQUEST['partner_id']) && filter_var($_REQUEST['partner_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['partner_id'] = $_REQUEST['partner_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['pay_type_id']) && filter_var($_REQUEST['pay_type_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['pay_type_id'] = $_REQUEST['pay_type_id'];
        }
        if (isset($_REQUEST['date_from']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_from'])) {
            $dataSearch['date_from'] = strtotime($_REQUEST['date_from']);
        } else {
            $dataSearch['date_from'] = 0;
        }
        if (isset($_REQUEST['date_to']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_to'])) {
            $dataSearch['date_to'] = strtotime($_REQUEST['date_to']) + 86399;
        } else {
            $dataSearch['date_to'] = time() + 24 * 60 * 60 * 365;
        }

        $this->page['data']['reports'] = $this->mReport->statisticsExportWarehouse($dataSearch);
        $this->page['reports'] = $this->page['data']['reports']['data']['item_list'];

// 		print_r($this->page['reports']);

        $this->load->view($this->page['viewPath'], array("data" => $this->page));
    }

}

/*end of file ReportExport.class.php*/
/*location: ReportExport.class.php*/
