<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Config;
use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class ProductUnit
 * @package App\Controllers
 */
class ProductUnit extends Controller {

	/**
	 * ProductUnit constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MProductUnit" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 31 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 *
	 */
	public function index() {
		$this->page[ 'pageTitle' ] = "Danh sách đơn vị sản phẩm";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$this->page[ 'data' ][ 'units' ] = $this->mProductUnit->getUnits();
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 *
	 */
	public function addNew() {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Thêm mới đơn vị sản phẩm";
		$this->page[ 'viewPath' ] = "stock management/product/NewUnit";
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'unit_name' ] = $_POST[ 'unit_name' ];
			if( isset( $_POST[ 'validity' ] ) && $_POST[ 'validity' ] == 1 )
				$dataInsert[ 'validity' ] = 1;
			else
				$dataInsert[ 'validity' ] = 0;

			$unitID = $this->mProductUnit->addNew( $dataInsert );
			if( $unitID > 0 ) {
				System::$router->router( 
					System::$config->baseUrl
					. "ql-kho/don-vi-san-pham"
					. System::$config->urlSuffix
				);
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $unitID
	 */
	public function  edit( $unitID ) {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Chỉnh sửa đơn vị sản phẩm";
		$this->page[ 'viewPath' ] = "stock management/product/EditUnit";
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'unit_id' ] = $unitID;
			$dataInsert[ 'unit_name' ] = $_POST[ 'unit_name' ];
			if( isset( $_POST[ 'validity' ] ) && $_POST[ 'validity' ] == 1 )
				$dataInsert[ 'validity' ] = 1;
			else
				$dataInsert[ 'validity' ] = 0;
		
			$result = $this->mProductUnit->update( $dataInsert );
			if( $result > 0 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Cập nhật thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Cập nhật không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'unit' ] = $this->mProductUnit->getUnit( $unitID );
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
}
/*end of file ProductUnit.class.php*/
/*location: ProductUnit.class.php*/