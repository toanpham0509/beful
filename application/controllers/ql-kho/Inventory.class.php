<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Config;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Inventory
 * @package App\Controllers
 */
class Inventory extends Controller {

	/**
	 * Inventory constructor.
	 */
	public  function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MInventory" );
		$this->load->model( "MMenu" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MProduct" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MImportWarehouse" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 28 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list; 
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param null $page
	 */
	public function index( $page = null ) {
		$this->page ['accessID'] = INVENTORY;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách điều chỉnh tồn kho";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page[ 'data' ][ 'adjustInventory' ] = $this->mInventory->getAdjustedInventories( $dataSearch );
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'adjustInventory' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if(isset($this->page[ 'data' ][ 'adjustInventory' ]->item_list))
			$this->page[ 'data' ][ 'adjustInventory' ] = $this->page[ 'data' ][ 'adjustInventory' ]->item_list;
		else 
			$this->page[ 'data' ][ 'adjustInventory' ] = null;
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Add new idjust invetory
	 */
	public function addNew() {
		$this->page ['accessID'] = INVENTORY_NEW;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'viewPath' ] = "stock management/inventory/NewChangeInventory";
		$this->page[ 'pageTitle' ] = "Chỉnh sửa điều chỉnh tồn kho";
	
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$this->page[ 'data' ][ 'errors' ] = array();
			if( isset( $_POST[ 'adjust_inventory_name' ] ) && strlen( $_POST[ 'adjust_inventory_name' ] ) ) {
				$dataInsert[ 'adjust_inventory_name' ] = strip_tags( $_POST[ 'adjust_inventory_name' ] );
			} else {
				$this->page[ 'data' ][ 'errors' ]['adjust_inventory_name'] = "Tên điều chỉnh tồn kho không hợp lệ!";
			}
			$dataInsert[ 'adjust_inventory_date' ] = strtotime( $_POST[ 'inventory_date' ] );
			$dataInsert[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
			$dataInsert[ 'status' ] = 2;
			
			if( empty( $this->page['data']['errors'] ) ) {
				$adjustInventoryId = $this->mInventory->newAdjustedInventory( $dataInsert );
				if( $adjustInventoryId > 0 ) {
					
					if( isset( $_POST[ 'adjust_inventory_line_id' ] )
						&& !empty( $_POST[ 'adjust_inventory_line_id' ] ) ) {
						$i = 0;
						foreach ( $_POST[ 'adjust_inventory_line_id' ] as $item ) {
							if( $item == 0 ) {
								$this->mInventory->newAdjustedInventoryLine( array(
									"product_id" => $_POST[ 'product_id' ][ $i ],
									"product_amount" => $_POST[ 'product_amount' ][ $i ],
									"adjust_type_id" => null,
									"adjust_inventory_id" => $adjustInventoryId,
									"product_price"		=> $_POST['product_price'][$i],
									"expired_date"	=> strtotime($_POST["expire_date"][$i]),
									"create_time" => time()
								) );
							}
							$i++;
						}
					}
					
					System::$router->router(
						System::$config->baseUrl
						. "ql-kho/ton-kho/chinh-sua/"
						. $adjustInventoryId
						. System::$config->urlSuffix
					);
				}
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $adjustInventoryId
	 */
	public function edit( $adjustInventoryId ) {
		$this->page ['accessID'] = INVENTORY_EDIT;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'viewPath' ] = "stock management/inventory/EditInventory";
		$this->page[ 'pageTitle' ] = "Chỉnh sửa điều chỉnh tồn kho";
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$this->page[ 'data' ][ 'errors' ] = array();
			if( isset( $_POST[ 'adjust_inventory_name' ] ) && strlen( $_POST[ 'adjust_inventory_name' ] ) ) {
				$dataInsert[ 'adjust_inventory_name' ] = strip_tags( $_POST[ 'adjust_inventory_name' ] );
			} else {
				$this->page[ 'data' ][ 'errors' ]['adjust_inventory_name'] = "Tên điều chỉnh tồn kho không hợp lệ!";
			}
			$dataInsert[ 'adjust_inventory_date' ] = strtotime( $_POST[ 'inventory_date' ] );
			$dataInsert[ 'warehouse_id' ] = $_POST[ 'warehouse_id' ];
				
			if( empty( $this->page['data']['errors'] ) ) {
				$result = $this->mInventory->updateAdjustedInventory( $adjustInventoryId, $dataInsert );
				if( $result > 0 ) {
					if( isset( $_POST[ 'adjust_inventory_line_id' ] )
						&& !empty( $_POST[ 'adjust_inventory_line_id' ] ) ) {
						
						//delete line
						$oldOrderLines = $this->mInventory->getAdjustedInventoryLines( $adjustInventoryId );
						
						if( !empty( $oldOrderLines ) )
						foreach ( $oldOrderLines as $orderLine ) {
							$c = 0;
							foreach ( $_POST[ 'adjust_inventory_line_id' ] as $new ) {
								if( $new == $orderLine->adjust_inventory_line_id )
								$c++;
							}
							if( $c == 0 ) {
								echo "Delete line " . $orderLine->adjust_inventory_line_id;
								$this->mInventory->deleteAdjustedInventoryLine( $orderLine->adjust_inventory_line_id );
							}
						}
						
						$i = 0;
						foreach ( $_POST[ 'adjust_inventory_line_id' ] as $item ) {
							if(isset($_POST["expire_date"][$i]) && $_POST["expire_date"][$i] > 0) 
								$expiredDate = strtotime($_POST["expire_date"][$i]);
							else
								$expiredDate = null; 
							if( $item == 0 ) {
								//add new
								$this->mInventory->newAdjustedInventoryLine( array(
									"product_id" => $_POST[ 'product_id' ][ $i ],
									"product_amount" => $_POST[ 'product_amount' ][ $i ],
									"adjust_type_id" => null,
									"adjust_inventory_id" => $adjustInventoryId,
									"product_price"		=> $_POST['product_price'][$i],
									"expired_date"	=> $expiredDate,
									"create_time" => time()
								) );
							} else {
								//update
								$this->mInventory->updateAdjustedInventoryLine($item, array(
									"product_id" => $_POST[ 'product_id' ][ $i ],
									"product_amount" => $_POST[ 'product_amount' ][ $i ],
									"adjust_type_id" => null,
									"adjust_inventory_id" => $adjustInventoryId,
									"adjust_inventory_line_id" => $item,
									"product_price"		=> $_POST['product_price'][$i],
									"expired_date"	=> $expiredDate,
									"last_update"	=> time()
								) );
							}
							$i++;
						}
					}

					$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Cập nhật thay đổi thành công.</div>";
				} else {
					$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Cập nhật thay đổi không thành công.</div>";
				}
			}
		}
		
		$this->page[ 'data' ][ 'adjustInventory' ] = $this->mInventory->getAdjustedInventory( $adjustInventoryId );
		$this->page[ 'data' ][ 'adjustInventory' ]->adjustInventoryId = $adjustInventoryId;
		$this->page[ 'data' ][ 'adjustInventoryLines' ] = $this->mInventory->getAdjustedInventoryLines( $adjustInventoryId );
// 		print_r($this->page[ 'data' ][ 'adjustInventoryLines' ]);
		
		$i = 0;
		if(!empty($this->page[ 'data' ][ 'adjustInventoryLines' ]))
		foreach ($this->page[ 'data' ][ 'adjustInventoryLines' ] as $item) {
			$inventory = $this->mProduct->getProductPriceInWarehouse(
				$item->product_id, 
				$this->page[ 'data' ][ 'adjustInventory' ]->warehouse_id
			);
			$this->page[ 'data' ][ 'adjustInventoryLines' ][$i]->inventory_amount = $inventory->data->item_list[0]->inventory_amount;
			$i++;
		}
// 		print_r($this->page[ 'data' ][ 'adjustInventoryLines' ]);
// 		print_r($this->page[ 'data' ][ 'adjustInventory' ]);
		if( $this->page[ 'data' ][ 'adjustInventory' ]->status == 3 ) {
			$this->page[ 'viewPath' ] = "stock management/inventory/ViewInventory";
// 			print_r($this->page[ 'data' ][ 'adjustInventoryLines' ]);
			if(!empty($this->page[ 'data' ][ 'adjustInventoryLines' ])) {
				foreach ($this->page[ 'data' ][ 'adjustInventoryLines' ] as $k => $item) {
					$data = $this->mInventory->getAdjustInventoryLine($item->adjust_inventory_line_id);
					$this->page[ 'data' ][ 'adjustInventoryLines' ][$k]->inventory_amount = $data[0]["product_old_amount"];
				}
			}
		}
		
		$this->page[ 'data' ][ 'urlPrint' ] = System::$config->baseUrl
				. "ql-kho/ton-kho/in/"
				. $adjustInventoryId . System::$config->urlSuffix;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	public function page( $page = null ) {
		$this->index( $page );
	}

	/**
	 * @param $adjustInventoryId
	 */
	public function in( $adjustInventoryId ) {
		$this->page[ 'accessID' ] = MODULE_WAREHOUSE;
		$this->user->access( $this->page[ 'accessID' ] );
	
		$this->page[ 'data' ][ 'adjustInventory' ] = $this->mInventory->getAdjustedInventory( $adjustInventoryId );
		$this->page[ 'data' ][ 'adjustInventory' ]->adjustInventoryId = $adjustInventoryId;
		$this->page[ 'data' ][ 'adjustInventoryLines' ] = $this->mInventory->getAdjustedInventoryLines( $adjustInventoryId );
	
		$this->page[ 'pageTitle' ] = "In đơn hàng";
		$this->load->view( "stock management/inventory/PrintInventory" );
	}

	/**
	 * @param $adjustInventoryId
	 */
	public function delete( $adjustInventoryId ) {
		$this->page[ 'accessID' ] = INVENTORY_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mInventory->deleteAdjustedInventory( $adjustInventoryId );
		System::$router->router(
			System::$config->baseUrl
			. "ql-kho/ton-kho"
			. System::$config->urlSuffix
		);
	}
	/**
	 * Xác nhận điều chỉnh tồn kho
	 * 
	 * @param unknown $adjustInventoryId
	 */
	public function inspection( $adjustInventoryId ) {
		$this->page[ 'accessID' ] = INVENTORY_INSPECTION;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page['data']['adjustInventory'] = $this->mInventory->getAdjustedInventory($adjustInventoryId);
// 		print_r($this->page['data']['adjustInventory']);
		$this->page[ 'data' ][ 'adjustInventoryLines' ] = $this->mInventory->getAdjustedInventoryLines( $adjustInventoryId );
// 		echo "<pre>";
// 		print_r($this->page[ 'data' ][ 'adjustInventoryLines' ]);
// 		echo "</pre>";
// 		die();
		//get shop id
		$shopId = $this->mWarehouse->getWarehouse( $this->page['data']['adjustInventory']->warehouse_id );
		$shopId = $shopId->workplace_id;
		
		if(!empty($this->page[ 'data' ][ 'adjustInventoryLines' ])) {
			foreach ($this->page[ 'data' ][ 'adjustInventoryLines' ] as $item) {
				$amount1 = $this->mProduct->getProductPriceInWarehouse(
						$item->product_id, 
						$this->page['data']['adjustInventory']->warehouse_id
				);
				$amount = $amount1->data->item_list[0]->inventory_amount;
				$price = $amount1->data->item_list[0]->product_price;
				if($amount > $item->product_amount) {
					echo "Tạo phiếu xuất kho";
					$orderId = $this->mSaleOrder->addNew( array(
						"customer_id" => null,
						"promote_id" => null,
						"shop_id"	=> $shopId,
						"price_id" => null,
						"order_date" => time(),
						"order_lines" => array( array(
								"order_line_id" => null,
								"product_id"	=> $item->product_id,
								"product_amount" => $amount - $item->product_amount,
								"product_price" => $item->product_price,
								"product_discount" => 0,
								"product_tax_id"   => null,
								"warehouse_id"     => $this->page['data']['adjustInventory']->warehouse_id
						) ),
						"order_status_id" => 17,
						"status"		=> 3,
						"order_price" => $item->product_price * ($amount - $item->product_amount),
						"order_note" => "Điều chỉnh tồn kho"
					) );
					if($orderId > 0) {
						$this->inspectionOutputOrderProcess($orderId);
						$this->mInventory->updateAdjustInventoryLine($item->adjust_inventory_line_id, array(
							"product_old_amount" => $amount,
							"order_id"			=> $orderId
						));
					} else {
						die("Điều chỉnh không thành công!");
					}
				} elseif($amount < $item->product_amount) {
					echo "Tạo phiếu nhập kho";
					$importId = $this->mImportWarehouse->newImportWarehouse ( array (
							"supplier_id" => null,
							"import_date" => time (),
							"order_note" => "Điều chỉnh tồn kho",
							"total_sale" => $item->product_price * ($item->product_amount - $amount),
							"total_sale_no_tax" => 0,
							"status_id" => 17,
							"status" => 3
					) );
					$this->mImportWarehouse->newImportLine ( array (
						"product_id" => $item->product_id,
						"product_amount" => $item->product_amount - $amount,
						"order_id" => $importId,
						"warehouse_id" => $this->page['data']['adjustInventory']->warehouse_id,
						"product_tax" => null,
						"product_total_price" => $item->product_price * ($item->product_amount - $amount),
						"expire_date" => $item->expired_date,
						"product_price" => $item->product_price
					) );
					$this->mInventory->updateAdjustInventoryLine($item->adjust_inventory_line_id, array(
							"product_old_amount" => $amount,
							"input_order_id"	=> $importId 
					));
				}
				
				$this->mInventory->updateInventoryTotal(array(
					"inventory_date" => time(),
					"order_id" => $adjustInventoryId,
					"product_id" => $item->product_id,
					"opening_stock" => $amount,
					"amount" => $item->product_amount - $amount,
					"warehouse_id" => $this->page['data']['adjustInventory']->warehouse_id,
					"inventory_amount" => $item->product_amount,
						"description" => "Điều chỉnh tồn kho",
					"create_time"	=> time()
				));
			}
		}
		$this->mInventory->updateAdjustedInventory( $adjustInventoryId, array(
				"status" => 3
		) );
		System::$router->router(
			System::$config->baseUrl
			. "ql-kho/ton-kho/chinh-sua/"
			. $adjustInventoryId
			. System::$config->urlSuffix
		);
	}
	/**
	 * 
	 * @param unknown $adjustInventoryId
	 */
	public function checkInspectionAdjustedInventory($adjustInventoryId) {
		
	}
	/**
	 * Xác nhận đơn hàng, sử lý nhập trước xuất trước
	 *
	 * @param unknown $orderId
	 */
	public function inspectionOutputOrderProcess($orderId) {
		$orderLines = $this->mSaleOrder->getOrder( $orderId );
		$orderLines = $orderLines->order_lines;
		if(!empty($orderLines))
			foreach ($orderLines as $ordeLine) {
// 				print_r($ordeLine);
					
				$arrayDataImport = array();
				$importsInStock = $this->mImportWarehouse->getImportsInStockByProduct(
					$ordeLine->product_id, $ordeLine->warehouse_id
				);
				// 			echo "<pre>";
// 							print_r($importsInStock);
					
				foreach ($importsInStock as $importInStock) {
	
					if($ordeLine->product_amount <= 0) break;
					$inventory = $importInStock['input_line_amount'] - $importInStock['buy_amount'];
					if($ordeLine->product_amount < $inventory) {
						$amount = $ordeLine->product_amount;
					} else {
						$amount = $inventory;
					}
					// 				echo "<br />Update import line id: " . $importInStock['input_line_id'] . " : " . ($amount + $importInStock['buy_amount']);
					$this->mImportWarehouse->updateImportLine(
							$importInStock['input_line_id'],
							array(
									"buy_amount" => $amount + $importInStock['buy_amount']
							)
					);
					$arrayDataImport[] = array(
							'product_id' => $ordeLine->product_id,
							'input_line_id' => $importInStock['input_line_id'],
							'amount' => $amount
					);
					$ordeLine->product_amount = $ordeLine->product_amount - $inventory;
				}
				$arrayDataImport = json_encode($arrayDataImport);
				// 			$arrayDataImport = str_replace("\"", "'", $arrayDataImport);
				$arrayDataImport = $arrayDataImport;
				// 			echo "<br />Update order_line_id " . $ordeLine->order_line_id . " : " . $arrayDataImport;
				$this->mSaleOrder->updateOrderLineInputLineId($ordeLine->order_line_id, $arrayDataImport);
			}
		// 		$this->mSaleOrder->updateOrder($orderId, array(
		// 				"status" => 3
		// 		));
	}
}
/*end of file Inventory.php*/
/*location: Inventory.php*/