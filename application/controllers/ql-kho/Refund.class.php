<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Refund
 * @package App\Controllers
 */
class Refund extends Controller {

	/**
	 * Refund constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MRefund" );
		$this->load->model( "MMenu" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 23 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param int $page
	 */
	public function index( $page = 1 ) {
		$this->page ['accessID'] = WAREHOUSE_VIEW_IMPORT_EXPORT_TRANSFER_REFUND;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách hàng trả về";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'refunds' ] = $this->mRefund->getRefunds( $dataSearch );
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'refunds' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if(isset($this->page[ 'data' ][ 'refunds' ]->item_list))
			$this->page[ 'data' ][ 'refunds' ] = $this->page[ 'data' ][ 'refunds' ]->item_list;
		else 
			$this->page[ 'data' ][ 'refunds' ] = null;
		
		$this->page['data']['paginationPath'] = "ql-kho/tra-ve/trang/";
		$this->page[ 'viewPath' ] = "buy management/refund product management/ViewRefund.php";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file Refund.class.php*/
/*location: Refund.class.php*/