<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Product
 * @package App\Controllers
 */
class Product extends Controller {

	/**
	 * Product constructor.
	 */
	public function __construct() {
		parent::__construct();

		$this->load->library( "HttpRequest" );

		$this->load->model( "MProduct" );
		$this->load->model( "MMenu" );

		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 29 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 *
	 * @param string $page
	 */
	public function index( $page = null ) {
		$this->page ['accessID'] = PRODUCT;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page == null ) ? 1 : $page;
		$this->page[ 'pageTitle' ] = "Danh sách chuyển kho";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];

		$dataSearch = array();

		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;

		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getListProducts( $dataSearch );

		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'products' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}

		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 *
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file Product.class.php*/
/*location: Product.class.php*/