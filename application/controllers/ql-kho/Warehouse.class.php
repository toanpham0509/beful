<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Warehouse extends Controller {
	/**
	 * 
	 */
	public function __construct(){
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 26 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * 
	 * @param number $page
	 */
	public function index( $page = 1 ) {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách kho hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses_( $dataSearch );

		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'warehouses' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->page[ 'data' ][ 'warehouses' ]->item_list;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}

	/**
	 * @param null $warehouseId
	 */
	public function addNew( $warehouseId = null ) {
		$this->page ['accessID'] = WAREHOUSE_NEW;
		$this->user->access ( $this->page ['accessID'] );

		if( $warehouseId == null || $warehouseId < 0 ) :
			$warehouseId = $this->page[ 'data' ][ 'warehouseId' ] = $this->mWarehouse->addNew(array(
				"warehouse_name" => null,
				"warehouse_address" => null,
				"district_id" => null,
				"workplace_id" => null,
				"description" => null,
				"is_runing" => null
			));
			$this->router->router(
				System::$config->baseUrl
				. "ql-kho/kho-hang/them-moi/"
				. $warehouseId
				. System::$config->urlSuffix
			);
		else:
			$this->page[ 'data' ][ 'warehouseId' ] = $warehouseId;
		endif;
		
		$this->page['data'][ 'warehouse' ] = $this->mWarehouse->getWarehouse( $this->page[ 'data' ][ 'warehouseId' ] );
		
		$this->page[ 'pageTitle' ] = "Thêm mới kho hàng";
		$this->page[ 'viewPath' ] = "stock management/warehouse/NewWarehouse";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'warehouse_name' ] = $_POST[ 'warehouse_name' ];
			$dataInsert[ 'warehouse_address' ] = $_POST[ 'warehouse_address' ];
			$dataInsert[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataInsert[ 'workplace_id' ] = null;
			$dataInsert[ 'status' ] = 2;
			$dataInsert[ 'description' ] = $_POST[ 'description' ];
			if( isset( $_POST[ 'is_runing' ] ) && $_POST[ 'is_runing' ] == 1 ) {
				$dataInsert[ 'is_runing' ] = 1;
			} else {
				$dataInsert[ 'is_runing' ] = 0;
			}
			$result = $this->mWarehouse->update( $warehouseId, $dataInsert );
			if( $result > 0 ) {
				$this->router->router(
						System::$config->baseUrl
						. "ql-kho/kho-hang/"
						. System::$config->urlSuffix
				);
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $warehouseID
	 */
	public function edit( $warehouseID ) {
		$this->page ['accessID'] = WAREHOUSE_EDIT_ITEM;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Chỉnh sửa kho hàng";
		$this->page[ 'viewPath' ] = "stock management/warehouse/EditWarehouse";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'warehouse_name' ] = $_POST[ 'warehouse_name' ];
			$dataInsert[ 'warehouse_address' ] = $_POST[ 'warehouse_address' ];
			$dataInsert[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataInsert[ 'workplace_id' ] = null;
			$dataInsert[ 'description' ] = $_POST[ 'description' ];
			if( isset( $_POST[ 'is_runing' ] ) && $_POST[ 'is_runing' ] == 1 ) {
				$dataInsert[ 'is_runing' ] = 1;
			} else {
				$dataInsert[ 'is_runing' ] = 0;
			}
			$value = $this->mWarehouse->update( $warehouseID, $dataInsert );
			if( $value > 0 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Lưu thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Lưu không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'warehouse' ] = $this->mWarehouse->getWarehouse( $warehouseID );
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $warehouseID
	 */
	public function delete( $warehouseID ) {
		$this->page ['accessID'] = WAREHOUSE_DELETE_ITEM;
		$this->user->access ( $this->page ['accessID'] );

		$this->mWarehouse->delete( $warehouseID );
		$this->router->router(
				System::$config->baseUrl
				. "ql-kho/kho-hang"
				. System::$config->urlSuffix
		);
	}
}
/*end of file Warehouse.class.php*/
/*location: Warehouse.class.php*/