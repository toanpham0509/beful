<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class ProductCategory extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MProductCategory" );
		$this->load->model( "MMenu" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 30 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = 1 ) {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách nhóm sản phẩm";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$this->page[ 'data' ][ 'productCategories' ] = $this->mProductCategory->getProductCategories();
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	public function page() {
		
	}
	public function addNew() {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Thêm mới nhóm sản phẩm";
		$this->page[ 'viewPath' ] = "stock management/product/NewProductCategory";
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'category_name' ] = $_POST[ 'category_name' ];
			$categoryID = $this->mProductCategory->addNew( $dataInsert );
			if( $categoryID > 0 ) {
				$this->router->router( 
					System::$config->baseUrl
					. "ql-kho/nhom-san-pham/chinh-sua/"
					. $categoryID
					. System::$config->urlSuffix
				);
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'productCategories' ] = $this->mProductCategory->getProductCategories();
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $productCategoryID
	 */
	public function edit( $productCategoryID ){
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Chỉnh sửa nhóm sản phẩm";
		$this->page[ 'viewPath' ] = "stock management/product/EditProductCategory";
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'category_name' ] = $_POST[ 'category_name' ];
			$result = $this->mProductCategory->update( $productCategoryID, $dataInsert );
			if( $result > 0 )
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Lưu thay đổi thành công.</div>";
			else 
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Lưu thay đổi không thành công.</div>";
		}
		
		$this->page[ 'data' ][ 'category' ] = $this->mProductCategory->getProductCategory( $productCategoryID );
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $productCategoryID
	 */
	public function delete( $productCategoryID ) {
		$this->page ['accessID'] = MODULE_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$this->mProductCategory->delete( $productCategoryID );
		$this->router->router( 
			System::$config->baseUrl
			. "ql-kho/nhom-san-pham/"
			. System::$config->urlSuffix
		);
	}
}
/*end of file ProductCategory.class.php*/
/*location: ProductCategory.class.php*/