<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class ImportWarehouse extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MImportWarehouse" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MMoney" );
		
		$this->page[ 'menuID' ] = 4;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 20 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		
		$this->page['data'][ 'mMoney' ] = $this->mMoney;
		
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = 1 )  {
		$this->page ['accessID'] = WAREHOUSE_VIEW_IMPORT_EXPORT_TRANSFER_REFUND;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
// 		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		$this->page[ 'viewPath' ] = "buy management/import stock management/ViewList.php";
		$this->page['data']['paginationPath'] = "ql-kho/nhap-kho/trang/";
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		$this->page[ 'data' ][ 'imports' ] = $this->mImportWarehouse->getImportWarehouses( $dataSearch );
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'imports' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if( isset( $this->page[ 'data' ][ 'imports' ]->item_list ) )
			$this->page[ 'data' ][ 'imports' ] = $this->page[ 'data' ][ 'imports' ]->item_list;
		else 
			$this->page[ 'data' ][ 'imports' ] = array();
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	public function page( $page = null ) {
		$this->index( $page );
	}
}
/*end of file Import.class.php*/
/*location: Import.class.php*/