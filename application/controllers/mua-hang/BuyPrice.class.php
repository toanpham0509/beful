<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 */
if (! defined ( 'BOOTSTRAP_PATH' ))
	exit ( "No direct script access allowed" );
class BuyPrice extends  Controller {
	/**
	 * 
	 * @var unknown
	 */
	private $buyPriceId;
	/**
	 * 
	 * @var unknown
	 */
	private $buyPriceData;
	/**
	 * 
	 * @var unknown
	 */
	private $buyPriceLines;
	/**
	 * 
	 * @var unknown
	 */
	private $errors;
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library ( "HttpRequest" );
		$this->load->model ( "MMenu" );
		$this->load->model ( "MBuyPrice" );
		$this->load->model( "MProduct" );
		$this->load->model( "MSupplier" );
		
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct( null );
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		
		$this->page ['menuID'] = 2;
		$this->page ['menuLeftCurrent'] = $this->mMenu->getMenu(61);
		$this->page ['menus'] = $this->mMenu->getMenus ();
		$this->page ['menuLefts'] = $this->mMenu->getMenus ( $this->page ['menuID'] );
		
		$this->load->controller ( "User" );
		$this->page ['userInfo'] = $this->user->userInfo;
	}
	/**
	 * Index
	 */
	public function index($page = null) {
		$this->page ['accessID'] = BUY_PRICE;
		$this->user->access ( $this->page ['accessID'] );

		$this->page ['accessID'] = null;
		$this->user->access ( $this->page ['accessID'] );
		
		$page = ($page == null) ? 1 : $page;
		$this->page ['pageTitle'] = "Danh sách bảng giá";
		$this->page ['viewPath'] = $this->page ['menuLeftCurrent'] ['view_path'];
		
		$dataSearch = array ();
		
		$dataSearch ['per_page_number'] = PER_PAGE_NUMBER;
		$dataSearch ['limit_number_start'] = $page;
		$this->page ['data'] ['startList'] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page ['data'] ['buyPrices'] = $this->mBuyPrice->getBuyPrices ( $dataSearch );
		
		// pagination
		$this->page ['data'] ['page'] = $page;
		$this->page ['data'] ['countRecord'] = $this->page ['data'] ['buyPrices']['data']['numer_row_total'];
		if ($this->page ['data'] ['countRecord'] % PER_PAGE_NUMBER > 0) {
			$this->page ['data'] ['pages'] = ( int ) ($this->page ['data'] ['countRecord'] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page ['data'] ['pages'] = ( int ) ($this->page ['data'] ['countRecord'] / PER_PAGE_NUMBER);
		}

// 		print_r($this->page ['data'] ['buyPrices']);
		
		if (isset ( $this->page ['data'] ['buyPrices']['data']['item_list'] ))
			$this->page ['data'] ['buyPrices'] = $this->page ['data'] ['buyPrices']['data']["item_list"];
		else
			$this->page ['data'] ['buyPrices'] = array ();
		
		
		$this->load->view ( "Template", array (
				"data" => $this->page
		) );
	}
	/**
	 * Page
	 *
	 * @param string $page
	 */
	public function page($page = null) {
		$this->index ( $page );
	}
	/**
	 * Add new buy price
	 * 
	 * @param string $buyPriceId
	 */
	public function addNew($buyPriceId = null) {
		$this->page ['accessID'] = BUY_PRICE_NEW;
		$this->user->access ( $this->page ['accessID'] );

		if($buyPriceId == null) {
			$buyPriceId = $this->mBuyPrice->newBuyPrice(array(
				"buy_price_status" => 1
			));
			$buyPriceId = $buyPriceId['data']['buy_price_id'];
			System::$router->router(
				System::$config->baseUrl
				. "mua-hang/bang-gia-mua/them-moi/"
				. $buyPriceId
				. System::$config->urlSuffix
			);
			die();
		}
		
		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();
		
		
		$this->page['data']['buyPrice'] = $this->mBuyPrice->getBuyPrice($buyPriceId);
		$this->page['data']['buyPrice'] = $this->page['data']['buyPrice']['data'][0];
		$code = $this->page['data']['buyPrice']['buy_price_code'];
		
		$re = $this->processSubmit($buyPriceId);
		if(empty($r)) {
			System::$router->router(
					System::$config->baseUrl
					. "mua-hang/bang-gia-mua/chinh-sua/"
					. $buyPriceId
					. System::$config->urlSuffix
			);
			die();
		}
		if(isset($_POST) && !empty($_POST)) 
			$this->page['data']['buyPrice'] = $_POST;
		$this->page['data']['buyPrice']['buy_price_code'] = $code;
		
		$this->page['pageTitle'] = System::$lang->getString("add_new");
		$this->page['viewPath'] = "buy management/price management/EditPrice.php";
		$this->load->view ( "Template", array (
			"data" => $this->page
		) );
	}
	/**
	 * 
	 * @param unknown $buyPriceId
	 */
	public function edit($buyPriceId) {
		$this->page ['accessID'] = BUY_ORDER_EDIT;
		$this->user->access ( $this->page ['accessID'] );

		if(isset($_POST) && !empty($_POST)) {
			$this->processSubmit($buyPriceId);
		}
		
		$this->page['data']['buyPrice'] = $this->mBuyPrice->getBuyPrice($buyPriceId);
		$this->page['data']['buyPrice'] = $this->page['data']['buyPrice']['data'][0];
// 		print_r($this->page['data']['buyPrice']);
		$this->page['data']['priceLines'] = $this->mBuyPrice->getBuyPriceLines( $buyPriceId );
		$this->page['data']['priceLines'] = $this->page['data']['priceLines']['data']['item_list'];
		
		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();
		
		$this->page['pageTitle'] = System::$lang->getString("edit");
		$this->page['viewPath'] = "buy management/price management/EditPrice.php";
		$this->load->view ( "Template", array (
				"data" => $this->page
		) );
	}
	/**
	 * 
	 * @param unknown $buyPriceId
	 * @return multitype:
	 */
	public function processSubmit($buyPriceId) {
		if (isset($_POST ['update'])) {
// 			print_r($_POST);
			$errors = array ();
			$dataUpdate = array ();
			$dataUpdate['buy_price_status'] = 5;
			if ( isset( $_POST['buy_price_name'] ) && strlen( $_POST['buy_price_name'] )) {
				$dataUpdate['buy_price_name'] = strip_tags ( $_POST ['buy_price_name'] );
			} else {
				$errors ['buy_price_name'] = "buy_price_name";
			}
			if (isset ( $_POST ['supplier_id'] ) && filter_var($_POST['supplier_id'], FILTER_VALIDATE_INT)) {
				$dataUpdate['supplier_id'] = $_POST['supplier_id'];
			}
			if (isset ( $_POST ['validity'] ) && $_POST ['validity'] == 1) {
				$dataUpdate['validity'] = 1;
			} else {
				$dataUpdate['validity'] = 0;
			}
			if (isset ( $_POST ['time_start'] )
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_start'] )) {
				$dataUpdate['time_start'] = strtotime ( $_POST ['time_start'] );
			} else {
				$errors ['time_start'] = "time_start";
			}
			if (isset ( $_POST ['time_end'] )
				&& preg_match ( "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_POST ['time_end'] )) {
				$dataUpdate['time_end'] = strtotime ( $_POST ['time_end'] );
			} else {
				$errors ['time_end'] = "time_end";
			}
			// products
			if (isset ( $_POST ['product_id'] )
				&& !empty ( $_POST ['product_id'] )
				&& isset($_POST[ 'buy_price_line_id' ])
				&& isset( $_POST ['product_price'] ) ) {
				
				$priceLines = $this->mBuyPrice->getBuyPriceLines( $buyPriceId );
				$priceLines = $priceLines['data']['item_list'];
				
				if( !empty( $priceLines ) ) {
					foreach ( $priceLines as $priceLine ) {
// 						print_r($priceLine);
						$c = 0;
						foreach ( $_POST[ 'buy_price_line_id' ] as $new ) {
							if( $new == $priceLine["buy_price_line_id"] )
								$c++;
						}
						if($c == 0) {
							$this->mBuyPrice->deleteBuyPriceLine($priceLine["buy_price_line_id"]);
						}
					}
				}
				$i = 0;
				foreach( $_POST ['product_id'] as $item ) {
					if( isset( $_POST[ 'buy_price_line_id' ][ $i ] )
						&& $_POST[ 'buy_price_line_id' ][ $i ] > 0 ) {
						//update
						$dataPriceLine = array (
							"product_id" => $item,
							"product_price" => $_POST ['product_price'] [$i]
						);
						$this->mBuyPrice->updateBuyPriceLine($_POST[ 'buy_price_line_id' ][ $i ], $dataPriceLine );
					} else {
						//add new
						$dataPriceLine = array (
							"product_id" => $item,
							"product_price" => $_POST ['product_price'] [$i],
							"buy_price_id" => $buyPriceId
						);
						$this->mBuyPrice->newBuyPriceLine( $dataPriceLine );
					}
					$i++;
				}
			}
			if(empty($errors)) {
// 				print_r($dataUpdate);
				if( $this->mBuyPrice->updateBuyPrice( $buyPriceId, $dataUpdate ) ) {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-success'>Lưu thành công.</b>";
					return array();
				} else {
					$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Lưu không thành công.</b>";
					return array("status" => 0);
				}
			} else {
				$this->page[ 'data' ][ 'message' ] = "<b class='text-danger'>Lưu không thành công.</b>";
				return array("status" => 0);
			}
		} else {
			return array("status" => 0);
		}
	}
	/**
	 * 
	 * @param unknown $buyPriceId
	 */
	public function delete($buyPriceId) {
		$this->page ['accessID'] = BUY_PRICE_DELETE;
		$this->user->access ( $this->page ['accessID'] );

		$this->mBuyPrice->deleteBuyPrice($buyPriceId);
		$this->router->router ( 
				System::$config->baseUrl 
				. "mua-hang/bang-gia-mua" 
				. System::$config->urlSuffix 
		);
	}
}
/*end of file BuyPrice.class.php*/
/*location: BuyPrice.class.php*/