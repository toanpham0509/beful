<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class TransferWarehouse extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MTransferWarehouse" );
		$this->load->model( "MImportWarehouse" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MBuyOrder" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MMenu" );
		$this->load->model( "MProduct" );
		$this->load->model("MInventory");
		
		$this->page[ 'menuID' ] = 2;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 14 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * 
	 * @param string $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page == null ) ? 1 : $page;
		$this->page[ 'pageTitle' ] = "Danh sách chuyển kho";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page[ 'data' ][ 'transfers' ] = $this->mTransferWarehouse->getTransfers( $dataSearch ); 
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'transfers' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if( isset( $this->page[ 'data' ][ 'transfers' ]->item_list ) )
			$this->page[ 'data' ][ 'transfers' ] = $this->page[ 'data' ][ 'transfers' ]->item_list;
		else 
			$this->page[ 'data' ][ 'transfers' ] = 0;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
	/**
	 * 
	 */
	public function addNew( $transferId = null ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chuyển kho";
		$this->page[ 'viewPath' ] = "buy management/transfer stock managment/NewTransfer";
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( $transferId == null ) {
			$dataInsert = array();
			$dataInsert[ 'warehouse_from_id' ]  = null;
			$dataInsert[ 'warehouse_to_id' ] = null;
			$dataInsert[ 'transfer_note' ] = null;
			
			//$dataInsert[ 'transfer_date' ] = time();
			$transferId = $this->mTransferWarehouse->addNew( $dataInsert );
			if( $transferId > 0 ) {
				System::$router->router(
					System::$config->baseUrl
					. "mua-hang/chuyen-kho/them-moi/"
					. $transferId
					. System::$config->urlSuffix
				);
				die();
			} else {
				echo "Lỗi! Không thể tạo mới chuyển kho.";
				die();
			}
		}
		$this->page[ 'data' ][ 'transfer' ] = $this->mTransferWarehouse->getTransfer( $transferId );
// 		print_r($this->page[ 'data' ][ 'transfer' ]);
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'warehouse_from_id' ]  = $_POST[ 'warehouse_from_id' ];
			$dataInsert[ 'warehouse_to_id' ] = $_POST[ 'warehouse_to_id' ];
			$dataInsert[ 'transfer_note' ] = $_POST[ 'transfer_note' ];
			$dataInsert[ 'status' ] = 2;
			
			//create new sale order
			//get shop id
			$shopId = $this->mWarehouse->getWarehouse( $_POST[ 'warehouse_from_id' ] );
			$shopId = $shopId->workplace_id;
			
			$dataInsert[ 'order_id' ] = $this->mSaleOrder->addNew(array(
					"customer_id" => null,
					"promote_id" => null,
					"price_id" => null,
					"order_date" => strtotime( $_POST[ 'import_date' ] ),
					"order_lines" => null,
					"order_status_id" => null,
					"order_price" => null,
					"order_note" => "Chuyển kho",
					"shop_id" => $shopId,
					"order_status_id" => 4,
					"status" => 2
			));
			
			//create new import
			$dataInsert[ 'input_order_id' ] = $this->mImportWarehouse->newImportWarehouse(array(
					"supplier_id" => 26,
					"import_date" => strtotime( $_POST[ 'import_date' ] ),
					"order_note" => "Chuyển kho",
					"total_sale" => 0,
					"total_sale_no_tax" => 0,
					"status_id" => 4,
					"status" => 2
			));
			
			$result = $this->mTransferWarehouse->updateTransferWarehouse( $transferId, $dataInsert );
			if( $result > 0 ) {
				if( isset( $_POST[ 'order_line_id' ] )
					&& !empty( $_POST[ 'order_line_id' ] ) ) {
					$size = sizeof( $_POST[ 'order_line_id' ] );
					$update[ 'order_lines' ] = array();
					$update[ 'order_price' ] = 0;
					$totalSale = 0;
					for( $i = 0; $i < $size; $i++ ) {
						//add new sale order line
						$update[ 'order_lines' ][$i] = array(
								"order_line_id" => null,
								"product_id" => $_POST[ 'product_id' ][ $i ],
								"product_amount" => $_POST[ 'product_amount' ][ $i ],
								"product_price" => $_POST[ 'product_price' ][ $i ],
								"product_discount" => 0,
								"product_tax_id"   => null,
								"warehouse_id"     =>  $_POST[ 'warehouse_from_id' ],
								"status"			=> 0
						);
						
						$update[ 'order_price' ] += $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						
						//add new import order line
						$totalSaleLine = $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						$totalSale += $totalSaleLine;
						$this->mImportWarehouse->newImportLine( array(
								"product_id" 		=> 		$_POST[ 'product_id' ][ $i ],
								"product_amount" 	=> 		$_POST[ 'product_amount' ][ $i ],
								"order_id" 			=>  	$dataInsert[ 'input_order_id' ],
								"warehouse_id"		=> 		$_POST[ 'warehouse_to_id' ],
								"product_tax"		=>		null,
								"product_total_price"	=> 	$totalSaleLine,
								"expire_date" 		=> 		null,
								"product_price"		=> 		$_POST[ 'product_price' ][ $i ]
						) );
					}
					//update sale order
					$this->mSaleOrder->update( $dataInsert[ 'order_id' ], $update );
					//update buy order
					$this->mBuyOrder->update( $dataInsert[ 'input_order_id' ], array(
						"total_sale" => $totalSale
					) );
				}
				
				$this->router->router(
					System::$config->baseUrl
					. "mua-hang/chuyen-kho/chinh-sua/"
					. $transferId
					. System::$config->urlSuffix
				);
				
// 				$this->page[ 'data' ][ 'transfer' ] = $this->mTransferWarehouse->getTransfer( $transferId );
				
// 				print_r( $this->page[ 'data' ][ 'transfer' ] );
				
// 				$this->page[ 'data' ][ 'saleOrder' ]  =$this->mSaleOrder->getOrder(
// 						$this->page[ 'data' ][ 'transfer' ]->order_id
// 				);
				
// 				print_r( $this->page[ 'data' ][ 'saleOrder' ] );
				
// 				$this->page[ 'data' ][ 'import' ]  = $this->mBuyOrder->getOrder(
// 						$this->page[ 'data' ][ 'transfer' ]->input_order_id
// 				);
				
// 				print_r( $this->page[ 'data' ][ 'import' ] );
				
// 				$this->page[ 'data' ][ 'importOrderLines'] = $this->mBuyOrder->getOrderLines(
// 						$this->page[ 'data' ][ 'transfer' ]->input_order_id
// 				);
				
// 				print_r( $this->page[ 'data' ][ 'importOrderLines' ] );
				
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công chuyển kho.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới chuyển kho không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $transferId
	 */
	public function edit( $transferId ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chỉnh sửa chuyển kho";
		$this->page[ 'viewPath' ] = "buy management/transfer stock managment/EditTransfer";
		
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'warehouse_from_id' ]  = $_POST[ 'warehouse_from_id' ];
			$dataInsert[ 'warehouse_to_id' ] = $_POST[ 'warehouse_to_id' ];
			$dataInsert[ 'transfer_note' ] = $_POST[ 'transfer_note' ];
		
			$result = $this->mTransferWarehouse->updateTransferWarehouse( $transferId, $dataInsert );
			
			$this->page[ 'data' ][ 'transfer' ] = $this->mTransferWarehouse->getTransfer( $transferId );
			
			if( $result > 0 ) {
				if( isset( $_POST[ 'order_line_id' ] )
					&& !empty( $_POST[ 'order_line_id' ] ) ) {
					
					//delete old sale order line
					$oldOrderLines = $this->mSaleOrder->getOrder( $this->page[ 'data' ][ 'transfer' ]->order_id );
					if( isset( $oldOrderLines->order_lines ) )
						$oldOrderLines = $oldOrderLines->order_lines;
					else
						$oldOrderLines = null;
					if( !empty( $oldOrderLines ) ) {
						foreach ( $oldOrderLines as $orderLine ) {
							$c = 0;
							foreach ( $_POST[ 'order_line_id' ] as $new ) {
								if( $new == $orderLine->order_line_id )
									$c++;
								}
							if( $c == 0 ) {
								$this->mSaleOrder->deleteOrderLine( $orderLine->order_line_id );
							}
						}
					}
					
					//delete input line
					$importLines = $this->mImportWarehouse->getImportLines(
						$this->page[ 'data' ][ 'transfer' ]
					);
					if( !empty( $importLines ) ) {
						foreach ( $importLines as $importLine ) {
							$c = 0;
							foreach ( $_POST[ 'order_line_id' ] as $new ) {
								if( $new == $importLine->import_line_id )
									$c++;
							}
							if( $c == 0 ) {
								$this->mImportWarehouse->deleteImportLine( $importLine->import_line_id );
							}
						}
					}
					
					$size = sizeof( $_POST[ 'order_line_id' ] );
					
					$dataUpdate[ 'order_lines' ] = array();
					$dataUpdate[ 'order_price' ] = 0;
					
					$totalSale = 0;
					
					for( $i = 0; $i < $size; $i++ ) {
						//update, add new sale order line
						if( $_POST[ 'order_line_id' ][$i] == 0 ) 
							$_POST[ 'order_line_id' ][$i] = null;
						
						$dataUpdate[ 'order_lines' ][$i] = array(
								"order_line_id" => $_POST[ 'order_line_id' ][$i],
								"product_id"	=> $_POST[ 'product_id' ][$i],
								"product_amount" => $_POST[ 'product_amount' ][ $i ],
								"product_price" => $_POST[ 'product_price' ][ $i ],
								"product_discount" => null,
								"product_tax_id"   => null,
								"warehouse_id"     => $_POST[ 'warehouse_from_id' ]
						);
						$dataUpdate[ 'order_price' ] += $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						
						//update import order line
						$lineTotalSale = $_POST[ 'product_amount' ][ $i ] *  $_POST[ 'product_price' ][ $i ];
						$totalSale += $lineTotalSale;
						$dataX = array(
								"product_id" 		=> 		$_POST[ 'product_id' ][ $i ],
								"product_amount" 	=> 		$_POST[ 'product_amount' ][ $i ],
								"order_id" 			=>  	$this->page[ 'data' ][ 'transfer' ]->input_order_id,
								"warehouse_id"		=> 		$_POST[ 'warehouse_to_id' ],
								"product_tax"		=>		null,
								"product_total_price"	=> 	$lineTotalSale,
								"expire_date" 		=> 		null,
								"product_price"		=> 		$_POST[ 'product_price' ][ $i ]
						);
						if( $_POST[ 'order_line_id' ][$i] == 0 )
							$this->mImportWarehouse->newImportLine( $dataX );
						elseif( $_POST[ 'order_line_id' ][$i] > 0 )
							$this->mImportWarehouse->updateImportLine( $_POST[ 'order_line_id' ][$i], $dataX );
					}
					$this->mImportWarehouse->updateImportWarehouse(
						$this->page[ 'data' ][ 'transfer' ]->input_order_id, 
						array(
							"total_sale_no_tax" => $totalSale,
							"total_sale"		=> $totalSale
						) 
					);
					
					//get shop id
					$shopId = $this->mWarehouse->getWarehouse( $_POST[ 'warehouse_from_id' ] );
					$dataUpdate[ 'shop_id' ] = $shopId->workplace_id;
					$this->mSaleOrder->update( 
						$this->page[ 'data' ][ 'transfer' ]->order_id, 
						$dataUpdate 
					);
				}
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Chỉnh sửa thành công chuyển kho.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Chỉnh sửa chuyển kho không thành công.</div>";
			}
		}
		$this->page[ 'data' ][ 'transfer' ] = $this->mTransferWarehouse->getTransfer( $transferId );
// 		print_r( $this->page[ 'data' ][ 'transfer' ] );
		
		$this->page[ 'data' ][ 'saleOrder' ]  =$this->mSaleOrder->getOrder(
			$this->page[ 'data' ][ 'transfer' ]->order_id
		);
// 		echo "Sale order";
// 		print_r( $this->page[ 'data' ][ 'saleOrder' ] );
		
		$this->page[ 'data' ][ 'import' ]  = $this->mImportWarehouse->getImportWarehouse(
			$this->page[ 'data' ][ 'transfer' ]->input_order_id
		);
// 		print_r( $this->page[ 'data' ][ 'import' ] );
		
		$this->page[ 'data' ][ 'importOrderLines'] = $this->mImportWarehouse->getImportLines(
			$this->page[ 'data' ][ 'transfer' ]->input_order_id
		);
// 		print_r( $this->page[ 'data' ][ 'importOrderLines' ] );
		
		if( $this->page[ 'data' ][ 'transfer' ]->status == 3 ) {
			$this->page[ 'viewPath' ] = "buy management/transfer stock managment/ViewTransfer";
		}
		
		$this->page[ 'data' ][ 'urlPrint' ] = System::$config->baseUrl
				. "mua-hang/chuyen-kho/in/"
				. $transferId . System::$config->urlSuffix;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 *
	 * @param unknown $orderID
	 */
	public function in( $transferId ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE_PRINT;
		$this->user->access( $this->page[ 'accessID' ] );
	
		$this->page[ 'data' ][ 'transfer' ] = $this->mTransferWarehouse->getTransfer( $transferId );
		$this->page[ 'data' ][ 'import' ]  = $this->mImportWarehouse->getImportWarehouse(
				$this->page[ 'data' ][ 'transfer' ]->input_order_id
		);
		
		$this->page[ 'pageTitle' ] = "In đơn hàng";
		$this->load->view( "buy management/transfer stock managment/PrintTransfer" );
	}

	/**
	 * @param $transferId
	 */
	public function delete( $transferId ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mTransferWarehouse->delete( $transferId );
		$this->router->router(
			System::$config->baseUrl
			. "mua-hang/chuyen-kho"
			. System::$config->urlSuffix
		);
	}

	/**
	 * @param $transferId
	 */
	public function inspection( $transferId ) {
		$this->page[ 'accessID' ] = TRANSFER_WAREHOUSE_INSPECTION;
		$this->user->access( $this->page[ 'accessID' ] );
		$transfer = $this->mTransferWarehouse->getTransfer( $transferId );
		$lines = $this->mImportWarehouse->getImportLines($transfer->input_order_id);
		
// 		echo "<pre> Transfer: <br/>";
// 		print_r($transfer);
		
// 		echo "<br />Import line: <br />";
// 		print_r($lines);
		
// 		$order = $this->mSaleOrder->getOrder($transfer->order_id);
// 		echo "<br />Sale order: <br />";
// 		print_r($order);

// 		echo "</pre>";
		
		$this->page['data']['checkInspection'] = $this->checkInspection($transferId);
		$this->page['data']['transferId'] = $transferId;
		if(empty($this->page['data']['checkInspection'])) {
			foreach ( $lines as $line ) {
				$productInventoryFrom = $this->mProduct->getProductPriceInWarehouse($line->product_id, $transfer->warehouse_from_id);
				$productInventoryTo = $this->mProduct->getProductPriceInWarehouse($line->product_id, $transfer->warehouse_to_id);
//				$this->mInventory->newInventory ( array (
//						"inventory_date" => $transfer->transfer_date,
//						"order_id" => $transfer->input_order_id,
//						"product_id" => $line->product_id,
//						"amount" => $line->product_amount,
//						"warehouse_id" => $transfer->warehouse_to_id
//				) );
//
//				$this->mInventory->newInventory ( array (
//						"inventory_date" => $transfer->transfer_date,
//						"order_id" => $transfer->order_id,
//						"product_id" => $line->product_id,
//						"amount" => "-" . $line->product_amount,
//						"warehouse_id" => $transfer->warehouse_from_id
//				) );
				$this->mInventory->updateInventoryTotal(array(
						"inventory_date" => $transfer->transfer_date,
						"order_id" => $transfer->order_id,
						"product_id" => $line->product_id,
						"warehouse_id" => $transfer->warehouse_from_id,
						"opening_stock" => $productInventoryFrom->data->item_list[0]->inventory_amount,
						"amount" => "-" . $line->product_amount,
						"inventory_amount" => $productInventoryFrom->data->item_list[0]->inventory_amount
								- $line->product_amount,
						"description"	=> "Chuyển kho",
						"create_time"	=> time()
				));
				$this->mInventory->updateInventoryTotal(array(
						"inventory_date" => $transfer->transfer_date,
						"order_id" => $transfer->input_order_id,
						"product_id" => $line->product_id,
						"warehouse_id" => $transfer->warehouse_to_id,
						"opening_stock" => $productInventoryTo->data->item_list[0]->inventory_amount,
						"amount" => $line->product_amount,
						"inventory_amount" => $productInventoryTo->data->item_list[0]->inventory_amount
								+ $line->product_amount,
						"description"	=> "Chuyển kho",
						"create_time"	=> time()
				));
			}
			
			$this->mTransferWarehouse->updateTransferWarehouse( $transferId, array(
					"status" => 3
			) );
			
			//inspection import
			$this->mImportWarehouse->inspectionImport( $transfer->input_order_id );
			
			//inspection sale order
			$this->inspectionSaleOrderProcess($transfer->order_id);
			$this->mSaleOrder->update($transfer->order_id, array(
					"status" => 3
			));
			
			$this->router->router(
					System::$config->baseUrl
					. "mua-hang/chuyen-kho/chinh-sua/"
					. $transferId
					. System::$config->urlSuffix
			);
		} else {
			$this->page['pageTitle'] = "Xác nhận chuyển kho";
			$this->page[ 'viewPath' ] = "buy management/transfer stock managment/InspectionTransfer";
			$this->load->view( "Template", array( "data" => $this->page ) );
		}
	}
	/**
	 *
	 * @param unknown $orderId
	 */
	public function checkInspection($transferId) {
		$transfer = $this->mTransferWarehouse->getTransfer( $transferId );
// 		print_r($transfer);
		$lines = $this->mImportWarehouse->getImportLines($transfer->input_order_id);
// 		print_r($lines);
		$errorData = array();
		$i = 0;
		foreach ($lines as $orderLine) {
			$productInventory = $this->mProduct->getProductPriceInWarehouse($orderLine->product_id, $transfer->warehouse_from_id);
			if($productInventory->data->item_list[0]->inventory_amount < $orderLine->product_amount) {
				$errorData[$i] = $orderLine;
				$errorData[$i]->currentInventoryAmount = $productInventory->data->item_list[0]->inventory_amount;
				$errorData[$i]->warehouse_from_name = $transfer->warehouse_from_name;
				$i++;
			}
		}
		return $errorData;
	}
	/**
	 * Xác nhận đơn hàng, sử lý nhập trước xuất trước
	 *
	 * @param unknown $orderId
	 */
	public function inspectionSaleOrderProcess($orderId) {
		$orderLines = $this->mSaleOrder->getOrder( $orderId );
// 		echo "<pre>";
// 		print_r($orderLines);
		$orderLines = $orderLines->order_lines;
		if(!empty($orderLines))
			foreach ($orderLines as $ordeLine) {
// 				print_r($ordeLine);
				
				$arrayDataImport = array();
				$importsInStock = $this->mImportWarehouse->getImportsInStockByProduct(
						$ordeLine->product_id, $ordeLine->warehouse_id);
				// 			echo "<pre>";
				// 			print_r($importsInStock);
					
				foreach ($importsInStock as $importInStock) {
	
					if($ordeLine->product_amount <= 0) break;
					$inventory = $importInStock['input_line_amount'] - $importInStock['buy_amount'];
					if($ordeLine->product_amount < $inventory) {
						$amount = $ordeLine->product_amount;
					} else {
						$amount = $inventory;
					}
					// 				echo "<br />Update import line id: " . $importInStock['input_line_id'] . " : " . ($amount + $importInStock['buy_amount']);
					$this->mImportWarehouse->updateImportLine(
							$importInStock['input_line_id'],
							array(
									"buy_amount" => $amount + $importInStock['buy_amount']
							)
					);
					$arrayDataImport[] = array(
							'product_id' => $ordeLine->product_id,
							'input_line_id' => $importInStock['input_line_id'],
							'amount' => $amount
					);
					$ordeLine->product_amount = $ordeLine->product_amount - $inventory;
				}
				$arrayDataImport = json_encode($arrayDataImport);
				// 			$arrayDataImport = str_replace("\"", "'", $arrayDataImport);
				$arrayDataImport = $arrayDataImport;
				// 			echo "<br />Update order_line_id " . $ordeLine->order_line_id . " : " . $arrayDataImport;
				$this->mSaleOrder->updateOrderLineInputLineId($ordeLine->order_line_id, $arrayDataImport);
			}
		// 		$this->mSaleOrder->updateOrder($orderId, array(
		// 				"status" => 3
		// 		));
	}
}
/*end of file TransferWarehouse.class.php*/
/*location: TransferWarehouse.class.php*/