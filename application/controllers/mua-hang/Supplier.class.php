<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Supplier extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MMenu" );
		$this->load->model( "MSupplier" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		
		$this->page[ 'menuID' ] = 2;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 16 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = 1 ) {
		$this->page[ 'accessID' ] = SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Nhà cung cấp";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers_( $dataSearch );
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'suppliers' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if(isset($this->page[ 'data' ][ 'suppliers' ]->item_list))
			$this->page[ 'data' ][ 'suppliers' ] = $this->page[ 'data' ][ 'suppliers' ]->item_list;
		else
			$this->page[ 'data' ][ 'suppliers' ] = null;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page  = 1 ) {
		$this->index( $page );
	}

	/**
	 * @param $supplierID
	 */
	public function edit( $supplierID ) {
		$this->page[ 'accessID' ] = SUPPLIER_EDIT_ITEM;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chỉnh sửa thông tin nhà cung cấp";
		$this->page[ 'viewPath' ] = "buy management/suppliers management/EditSupplier";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'supplier_name' ] = $_POST[ 'customer_name' ];
			$dataInsert[ 'address' ] = isset( $_POST[ 'address' ] ) ? $_POST[ 'address' ] : "";
			$dataInsert[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataInsert[ 'phone' ] = isset( $_POST[ 'phone' ] ) ? $_POST[ 'phone' ] : "";
			$dataInsert[ 'fax' ] = isset( $_POST[ 'fax' ] ) ? $_POST[ 'fax' ] : "";
			$dataInsert[ 'tax_code' ] = isset( $_POST[ 'tax_code' ] ) ? $_POST[ 'tax_code' ] : "";
			$dataInsert[ 'bank_number' ] = isset( $_POST[ 'bank_number' ] ) ? $_POST[ 'bank_number' ] : "";
			$dataInsert[ 'bank_name' ] = isset( $_POST[ 'bank_name' ] ) ? $_POST[ 'bank_name' ] : "";
			$dataInsert[ 'contact_person_name' ] = isset( $_POST[ 'contact_person_name' ] ) ? $_POST[ 'contact_person_name' ] : "";
			$dataInsert[ 'contact_person_phone' ] = isset( $_POST[ 'contact_person_phone' ] ) ? $_POST[ 'contact_person_phone' ] : "";
			$dataInsert[ 'contact_person_email' ] = isset( $_POST[ 'contact_person_email' ] ) ? $_POST[ 'contact_person_email' ] : "";
			$dataInsert[ 'note' ] = isset( $_POST[ 'note' ] ) ? $_POST[ 'note' ] : "";
		
			$result = $this->mSupplier->updateSupplier( $supplierID, $dataInsert );
			
			if( $result > 0 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'supplier' ] = $this->mSupplier->getSupplier( $supplierID );
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Add new supplier
	 */
	public  function addNew() {
		$this->page[ 'accessID' ] = SUPPLIER_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chỉnh sửa thông tin nhà cung cấp";
		$this->page[ 'viewPath' ] = "buy management/suppliers management/NewSupplier";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'supplier_name' ] = $_POST[ 'customer_name' ];
			$dataInsert[ 'address' ] = isset( $_POST[ 'address' ] ) ? $_POST[ 'address' ] : "";
			$dataInsert[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataInsert[ 'phone' ] = isset( $_POST[ 'phone' ] ) ? $_POST[ 'phone' ] : "";
			$dataInsert[ 'fax' ] = isset( $_POST[ 'fax' ] ) ? $_POST[ 'fax' ] : "";
			$dataInsert[ 'tax_code' ] = isset( $_POST[ 'tax_code' ] ) ? $_POST[ 'tax_code' ] : "";
			$dataInsert[ 'bank_number' ] = isset( $_POST[ 'bank_number' ] ) ? $_POST[ 'bank_number' ] : "";
			$dataInsert[ 'bank_name' ] = isset( $_POST[ 'bank_name' ] ) ? $_POST[ 'bank_name' ] : "";
			$dataInsert[ 'contact_person_name' ] = isset( $_POST[ 'contact_person_name' ] ) ? $_POST[ 'contact_person_name' ] : "";
			$dataInsert[ 'contact_person_phone' ] = isset( $_POST[ 'contact_person_phone' ] ) ? $_POST[ 'contact_person_phone' ] : "";
			$dataInsert[ 'contact_person_email' ] = isset( $_POST[ 'contact_person_email' ] ) ? $_POST[ 'contact_person_email' ] : "";
			$dataInsert[ 'note' ] = isset( $_POST[ 'note' ] ) ? $_POST[ 'note' ] : "";
				
			$customerID = $this->mSupplier->newSupplier( $dataInsert );
			if( $customerID > 0 ) {
				$this->router->router(
						System::$config->baseUrl
						. "mua-hang/nha-cung-cap/chinh-sua/"
						. $customerID
						. System::$config->urlSuffix
				);
				die();
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
}
/*end of file Supplier.class.php*/
/*location: Supplier.class.php*/