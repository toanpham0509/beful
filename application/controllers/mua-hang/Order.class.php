<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Order extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MBuyOrder" );
		$this->load->model( "MMenu" );
		$this->load->model( "MSupplier" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MMoney" );
		$this->load->model( "MProduct" );
		$this->load->model("MBuyPrice");
		
		$this->page['data']['buyPrices'] = $this->mBuyPrice->getListBuyPrices();
		$this->page['data']['buyPrices'] = $this->page['data']['buyPrices']['data']['item_list'];
		
		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
//		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;
		
		$this->page[ 'menuID' ] = 2;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 12 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page['data'][ 'mMoney' ] = $this->mMoney;

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * Index
	 * 
	 * @param 		number 		$page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = BUY_ORDER;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page == null ) ? 1 : $page;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		if( isset( $_POST[ 'search' ] ) ) {
			
		}
		
		$this->page[ 'data' ][ 'orders' ] = $this->mBuyOrder->getOrders( $dataSearch );
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'orders' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if(isset($this->page[ 'data' ][ 'orders' ]->item_list))
			$this->page[ 'data' ][ 'orders' ] = $this->page[ 'data' ][ 'orders' ]->item_list;
		else 
			$this->page[ 'data' ][ 'orders' ] = array();
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Page
	 * 
	 * @param string $page
	 */
	public function page( $page = null ) {
		$this->index( $page );
	}

	/**
	 * @param null $orderID
	 */
	public function addNew( $orderID = null ) {
		$this->page[ 'accessID' ] = BUY_ORDER_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Thêm mới đơn hàng";
		$this->page[ 'viewPath' ] = "buy management/order management/NewOrder";
		
		if( $orderID == null || $orderID < 0 ) :
			$orderID = $this->page[ 'data' ][ 'orderID' ] = $this->mBuyOrder->addNew(array(
					"supplier_id" => null,
					"order_date" => time(),
					"intended_date" => time(),
					"status_id" => 15,
					"order_note" => null
			));
			$this->router->router(
					System::$config->baseUrl
					. "mua-hang/don-hang/them-moi/"
					. $orderID
					. System::$config->urlSuffix
			);
		else:
			$this->page[ 'data' ][ 'orderID' ] = $orderID;
			$this->page[ 'data' ][ 'orderCode' ] = $this->mBuyOrder->getOrder( $orderID );
			$this->page[ 'data' ][ 'orderCode' ] = $this->page[ 'data' ][ 'orderCode' ]->order_code;
		endif;
		
		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataInsert = array();
			$dataInsert[ 'supplier_id' ]  = $_POST[ 'supplier_id' ];
			$dataInsert[ 'order_date' ] = strtotime( $_POST[ 'order_date' ] );
			$dataInsert[ 'intended_date' ] = strtotime( $_POST[ 'order_intended_date' ] );
			$dataInsert[ 'order_note' ] = $_POST[ 'order_note' ];
			$dataInsert[ 'status' ] = 2;
			
			$result = $this->mBuyOrder->update( $orderID, $dataInsert );
			$dataUpdate = array();
			if( $result > 0 ) {
				$totalSale = 0;
				if( isset( $_POST[ 'order_line_id' ] ) 
					&& !empty( $_POST[ 'order_line_id' ] ) ) {
					$size = sizeof( $_POST[ 'order_line_id' ] );
					for( $i = 0; $i < $size; $i++ ) {
						$totalSaleLine = $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						$totalSale += $totalSaleLine;
						//add new
						$time = strtotime( $_POST[ 'intended_date' ][ $i ] );
						if( $time == 0 ) $time = null;
						$this->mBuyOrder->addNewOrderLine( $orderID, array(
							"warehouse_id" 		=> $_POST[ 'warehouse_id' ][ $i ],
							"product_id" 		=> $_POST[ 'product_id' ][ $i ],
							"product_amount" 	=> $_POST[ 'product_amount' ][ $i ],
							"product_price" 	=> $_POST[ 'product_price' ][ $i ],
							"product_tax"		=> null,
							"product_total_price" => $totalSaleLine,
							"intended_date"		=> $time
						) );
					}
				}
				
				$dataUpdate[ 'total_sate_no_tax' ]  = $totalSale;
				$total_tax = 0;
				$dataUpdate[ 'total_sale' ] = $dataUpdate[ 'total_sate_no_tax' ] + $total_tax;
				unset($dataUpdate[ 'total_sate_no_tax' ]);
				$result = $this->mBuyOrder->update( $orderID, $dataUpdate );
				
				$this->router->router( 
						System::$config->baseUrl 
						. "mua-hang/don-hang/chinh-sua/"
						. $orderID
						. System::$config->urlSuffix
				);
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công đơn hàng.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới đơn hàng không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param unknown $orderID
	 */
	public function edit( $orderID, $result = null ) {
		$this->page[ 'accessID' ] = BUY_ORDER_EDIT;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Chỉnh sửa đơn hàng";
		$this->page[ 'viewPath' ] = "buy management/order management/EditOrder";
		
		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();
		$this->page[ 'data' ][ 'warehouses' ] = $this->mWarehouse->getWarehouses();
		
		if( isset( $_POST[ 'save' ] ) ) {
			$dataUpdate = array();
			$dataUpdate[ 'supplier_id' ]  = $_POST[ 'supplier_id' ];
			$dataUpdate[ 'order_date' ] = strtotime( $_POST[ 'order_date' ] );
			$dataUpdate[ 'intended_date' ] = strtotime( $_POST[ 'order_intended_date' ] );
			$dataUpdate[ 'order_note' ] = $_POST[ 'order_note' ];
			$dataInsert[ 'status' ] = 2;
			
			$totalSale = 0;
			if( !empty( $_POST[ 'order_line_id' ] ) ) {
				$orderLines = $this->mBuyOrder->getOrderLines( $orderID );
				if( !empty( $orderLines ) )
				foreach ( $orderLines as $orderLine ) {
					$c = 0;
					foreach ( $_POST[ 'order_line_id' ] as $new ) {
						if( $new == $orderLine->order_line_id )
							$c++;
					}
					if( $c == 0 ) {
						$this->mBuyOrder->deleteOrderLine( $orderLine->order_line_id );
					}
				}
				$size = sizeof( $_POST[ 'order_line_id' ] );
				for( $i = 0; $i < $size; $i++ ) {
					if( $_POST[ 'order_line_id' ][ $i ] == 0 ) {
						$totalSaleLine = $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						$totalSale += $totalSaleLine;
						//add new
						$this->mBuyOrder->addNewOrderLine( $orderID, array(
							"warehouse_id" 		=> $_POST[ 'warehouse_id' ][ $i ],
							"product_id" 		=> $_POST[ 'product_id' ][ $i ],
							"product_amount" 	=> $_POST[ 'product_amount' ][ $i ],
							"product_price" 	=> $_POST[ 'product_price' ][ $i ],
							"product_tax"		=> null,
							"product_total_price" => $totalSaleLine,
							"intended_date"		=> strtotime( $_POST[ 'intended_date' ][ $i ] )
						) );
					} elseif( $_POST[ 'order_line_id' ][ $i ] > 0 ) {
						$totalSaleLine = $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ];
						$totalSale += $totalSaleLine;
						//update
						$this->mBuyOrder->updateOrderLine( $orderID, array(
							"input_line_id" 	=> $_POST[ 'order_line_id' ][ $i ],
							"warehouse_id" 		=> $_POST[ 'warehouse_id' ][ $i ],
							"product_id" 		=> $_POST[ 'product_id' ][ $i ],
							"product_amount" 	=> $_POST[ 'product_amount' ][ $i ],
							"product_price" 	=> $_POST[ 'product_price' ][ $i ],
							"product_tax"		=> null,
							"product_total_price" => $_POST[ 'product_amount' ][ $i ] * $_POST[ 'product_price' ][ $i ],
							"intended_date"		=> strtotime( $_POST[ 'intended_date' ][ $i ] )
						) );
					}
				}
			}
			
			$dataUpdate[ 'total_sate_no_tax' ]  = $totalSale;
			$total_tax = 0;
			$dataUpdate[ 'total_sale' ] = $dataUpdate[ 'total_sate_no_tax' ] + $total_tax;
			unset($dataUpdate[ 'total_sate_no_tax' ]);
			$result = $this->mBuyOrder->update( $orderID, $dataUpdate );
			
			if( $result > 0 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Cập nhật thành công đơn hàng.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Cập nhật không thành công.</div>";
			}
		} else {
			if( $result == 1 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Cập nhật thành công đơn hàng.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'order' ] = $this->mBuyOrder->getOrder( $orderID );
// 		print_r($this->page[ 'data' ][ 'order' ]);
		$this->page[ 'data' ][ 'order' ]->order_id = $orderID;
		$this->page[ 'data' ][ 'orderLines' ] = $this->mBuyOrder->getOrderLines( $orderID );
		
		$this->page[ 'data' ][ 'urlPrint' ] = System::$config->baseUrl
				. "mua-hang/don-hang/in/"
				. $orderID . System::$config->urlSuffix;
		
		if( $this->page[ 'data' ][ 'order' ]->status == 3 ) {
			$this->page[ 'viewPath' ] = "buy management/order management/ViewOrder";
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param unknown $orderID
	 */
	public function print1( $orderID ) {
		$this->page[ 'accessID' ] = BUY_ORDER_PRINT;
		$this->user->access( $this->page[ 'accessID' ] );
	
		$this->page[ 'data' ][ 'order' ] = $this->mBuyOrder->getOrder( $orderID );
		$this->page[ 'data' ][ 'orderLines' ] = $this->mBuyOrder->getOrderLines( $orderID );

		$this->page[ 'pageTitle' ] = "In đơn hàng";
		$this->load->view( "buy management/order management/PrintOrder", array( "data" => $this->page['data'] ) );
	}

	/**
	 * @param unknown $orderID
	 */
	public function delete( $orderID ) {
		$this->page[ 'accessID' ] = BUY_ORDER_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mBuyOrder->delete( $orderID );
		$this->router->router(
			System::$config->baseUrl
			. "mua-hang/don-hang"
			. System::$config->urlSuffix
		);
	}

	/**
	 * @param $orderId
	 */
	public function inspection( $orderId ) {
		$this->page[ 'accessID' ] = BUY_ORDER_INSPECTION;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mBuyOrder->inspectionOrder( $orderId );
		$this->router->router(
			System::$config->baseUrl
			. "mua-hang/don-hang/chinh-sua/"
			. $orderId
			. System::$config->urlSuffix
		);
	}
}
/*end of file Order.class.php*/
/*location: Order.class.php*/