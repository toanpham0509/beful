<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 */
if (! defined ( 'BOOTSTRAP_PATH' ))
	exit ( "No direct script access allowed" );

/**
 * Class ImportWarehouse
 * @package App\Controllers
 */
class ImportWarehouse extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct ();

		$this->load->library ( "HttpRequest" );
		$this->load->model ( "MImportWarehouse" );
		$this->load->model ( "MSupplier" );
		$this->load->model ( "MWarehouse" );
		$this->load->model ( "MBuyOrder" );
		$this->load->model ( "MSaleOrder" );
		$this->load->model ( "MRefund" );
		$this->load->model ( "MInventory" );
		$this->load->model ( "MMoney" );


		$this->load->model ( "MMenu" );

		$this->page ['menuID'] = 2;
		$this->page ['menuLeftCurrent'] = $this->mMenu->getMenu ( 13 );
		$this->page ['menus'] = $this->mMenu->getMenus ();
		$this->page ['menuLefts'] = $this->mMenu->getMenus ( $this->page ['menuID'] );

		$this->page['data'][ 'mMoney' ] = $this->mMoney;

		$this->load->controller ( "User" );
		$this->page ['userInfo'] = $this->user->userInfo;
	}
	/**
	 * Index
	 *
	 * @param string $page
	 */
	public function index($page = null) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE;
		$this->user->access ( $this->page ['accessID'] );

		$page = ($page == null) ? 1 : $page;
		$this->page ['pageTitle'] = "Danh sách nhập kho";
		$this->page ['viewPath'] = $this->page ['menuLeftCurrent'] ['view_path'];
		$this->page['data']['paginationPath'] = "mua-hang/nhap-kho/trang/";
		$dataSearch = array ();

		$dataSearch ['per_page_number'] = PER_PAGE_NUMBER;
		$dataSearch ['limit_number_start'] = $page;
		$this->page ['data'] ['startList'] = ($page - 1) * PER_PAGE_NUMBER;

		$this->page ['data'] ['imports'] = $this->mImportWarehouse->getImportWarehouses ( $dataSearch );

		// pagination
		$this->page ['data'] ['page'] = $page;
		$this->page ['data'] ['countRecord'] = $this->page ['data'] ['imports']->num_rows_total;
		if ($this->page ['data'] ['countRecord'] % PER_PAGE_NUMBER > 0) {
			$this->page ['data'] ['pages'] = ( int ) ($this->page ['data'] ['countRecord'] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page ['data'] ['pages'] = ( int ) ($this->page ['data'] ['countRecord'] / PER_PAGE_NUMBER);
		}

		if (isset ( $this->page ['data'] ['imports']->item_list ))
			$this->page ['data'] ['imports'] = $this->page ['data'] ['imports']->item_list;
		else
			$this->page ['data'] ['imports'] = array ();
// 		print_r($this->page['data']['imports']);
		foreach($this->page ['data'] ['imports'] as $k => $item) {
			$this->page ['data'] ['imports'][$k]->total_sale = $this->mMoney->addDotToMoney($this->page ['data'] ['imports'][$k]->total_sale);
			$this->page ['data'] ['imports'][$k]->total_sale_no_tax = $this->mMoney->addDotToMoney($this->page ['data'] ['imports'][$k]->total_sale_no_tax);
		}
		$this->load->view ( "Template", array (
				"data" => $this->page
		) );
	}
	/**
	 * Page
	 *
	 * @param string $page
	 */
	public function page($page = null) {
		$this->index ( $page );
	}
	/**
	 * Add new page
	 */
	public function addNew($importId = null) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE_NEW;
		$this->user->access ( $this->page ['accessID'] );

		$this->page ['pageTitle'] = "Nhập kho";
		$this->page ['viewPath'] = "buy management/import stock management/NewImport";

		if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) {
			$this->page ['data'] ['saleOrders'] = $this->mSaleOrder->getOrders ( array (
					"status" => 3
			) );
			$this->page ['data'] ['saleOrders'] = $this->page ['data'] ['saleOrders']->data;
		}

		if ($importId == null || $importId < 0) :
			$statusId = 14;
			$url = "";
			if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) {
				$url = "?refund=1";
				$statusId = 5;
			}
			$importId = $this->page ['data'] ['importID'] = $this->mImportWarehouse->newImportWarehouse ( array (
					"supplier_id" => null,
					"import_date" => time (),
					"order_note" => null,
					"total_sale" => 0,
					"total_sale_no_tax" => 0,
					"status_id" => $statusId
			) );
			$this->router->router ( System::$config->baseUrl . "mua-hang/nhap-kho/them-moi/" . $importId . System::$config->urlSuffix . $url );
		else :
			$this->page ['data'] ['importID'] = $importId;
			$this->page ['data'] ['importCode'] = $this->mImportWarehouse->getImportWarehouse ( $importId );
// 			echo $importId;
// 			print_r($this->page[ 'data' ][ 'importCode' ]);
			if (isset ( $this->page ['data'] ['importCode']->import_code ))
				$this->page ['data'] ['importCode'] = $this->page ['data'] ['importCode']->import_code;
			else
				$this->page ['data'] ['importCode'] = $importId;
		endif;

		if (isset ( $_POST ['save'] )) {
			$dataInsert = array ();
			$dataInsert ['supplier_id'] = $_POST ['supplier_id'];
			$dataInsert ['import_date'] = strtotime ( $_POST ['import_date'] );
			$dataInsert ['order_note'] = isset ( $_POST ['order_note'] ) ? $_POST ['order_note'] : "";
			$dataInsert ['purchase_order'] = $_POST ['order_id'];
			$dataInsert ['total_sale'] = 0;
			$dataInsert ['total_sale_no_tax'] = 0;
			$dataInsert ['status'] = 2;
			if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1)
				$dataInsert ['status_id'] = 5;
			else
				$dataInsert ['status_id'] = 14;

			$result = $this->mImportWarehouse->updateImportWarehouse ( $importId, $dataInsert );
			if ($result > 0) {
				// import line id
				if (isset ( $_POST ['order_line_id'] ) && ! empty ( $_POST ['order_line_id'] )) {
					$i = 0;
					$totalSale = 0;
					foreach ( $_POST ['order_line_id'] as $importLineID ) {
						if (isset ( $_POST ['expire_date'] [$i] )) {
							$expireDate = strtotime ( $_POST ['expire_date'] [$i] );
						} else {
							$expireDate = null;
						}
						$lineTotalSale = $_POST ['product_amount'] [$i] * $_POST ['product_price'] [$i];
						$totalSale += $lineTotalSale;
						$this->mImportWarehouse->newImportLine ( array (
								"product_id" => $_POST ['product_id'] [$i],
								"product_amount" => $_POST ['product_amount'] [$i],
								"order_id" => $importId,
								"warehouse_id" => $_POST ['warehouse_id'] [$i],
								"product_tax" => null,
								"product_total_price" => $lineTotalSale,
								"expire_date" => $expireDate,
								"product_price" => $_POST ['product_price'] [$i]
						) );
						$i ++;
					}
					if ($totalSale > 0) {
						$this->mImportWarehouse->updateImportWarehouse ( $importId, array (
								"total_sale_no_tax" => $totalSale,
								"total_sale" => $totalSale
						) );
					}
				}

				if ($dataInsert ['supplier_id'] == 27) {
					// hàng trả về
					$importLines = array_reverse($this->mImportWarehouse->getImportLines ( $importId ));
					$i = 0;
					$refundId = 0;
					foreach ( $_POST ['order_line_id'] as $importLineID ) {
						if ($importLineID > 0) {
							$re = $this->mRefund->newRefund ( array (
									"output_line_id" => $importLineID,
									"input_line_id" => $importLines [$i]->import_line_id,
									"status" => 2,
									"refund_note" => null
							) );
							if($i == 0) $refundId = $re;
						}
						$i ++;
					}
					$this->router->router (
							System::$config->baseUrl
							. "mua-hang/nhap-kho/chinh-sua/"
							. $importId
							. System::$config->urlSuffix
							. "?refund=1&refund_id="
							. $refundId);
				} else {
					$this->router->router (
							System::$config->baseUrl
							. "mua-hang/nhap-kho/chinh-sua/"
							. $importId
							. System::$config->urlSuffix );
				}
				$this->page ['data'] ['message'] = "<div class='text-success'>Thêm mới thành công phiếu nhập kho.</div>";
			} else {
				$this->page ['data'] ['message'] = "<div class='text-danger'>Thêm mới phiếu nhập kho không thành công.</div>";
			}
		}
		if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) :
			$this->page ['data'] ['refund'] = true;
			$this->page ['menuLeftCurrent'] = $this->mMenu->getMenu ( 15 );
			$this->page ['pageTitle'] = "Thêm mới hàng trả về";
		else :
			$this->page ['data'] ['refund'] = false;
		endif;

		$this->page ['data'] ['suppliers'] = $this->mSupplier->getSuppliers ();
		$this->page ['data'] ['buyOrders'] = $this->mBuyOrder->getOrders ();
		if(isset($this->page ['data'] ['buyOrders']->item_list))
			$this->page ['data'] ['buyOrders'] = $this->page ['data'] ['buyOrders']->item_list;
		else
			$this->page ['data'] ['buyOrders'] = array();

		unset ( $this->page ['data'] ['saleOrders']->order_count );
		$this->page ['data'] ['warehouses'] = $this->mWarehouse->getWarehouses ();

		$this->load->view ( "Template", array (
				"data" => $this->page
		) );
	}
	/**
	 * Edit post
	 *
	 * @param unknown $importId
	 */
	public function edit($importId) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE_EDIT;
		$this->user->access ( $this->page ['accessID'] );

		if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) {
			$this->page ['data'] ['refund'] = true;
			$this->page ['menuLeftCurrent'] = $this->mMenu->getMenu ( 15 );
		} else {
			$this->page ['data'] ['refund'] = false;
		}

		$this->page ['pageTitle'] = "Chỉnh sửa nhập kho";
		$this->page ['viewPath'] = "buy management/import stock management/EditImport";

		if (isset ( $_POST ['save'] )) {
			$dataInsert = array ();
			$dataInsert ['supplier_id'] = $_POST ['supplier_id'];
			$dataInsert ['import_date'] = strtotime ( $_POST ['import_date'] );
			$dataInsert ['order_note'] = isset ( $_POST ['order_note'] ) ? $_POST ['order_note'] : "";
			$dataInsert ['total_sale'] = 0;
			$dataInsert ['total_sale_no_tax'] = 0;
			$dataInsert ['status'] = 2;

			$status = $this->mImportWarehouse->updateImportWarehouse ( $importId, $dataInsert );
			if ($status > 0) {
				// import line id
				if (isset ( $_POST ['order_line_id'] ) && ! empty ( $_POST ['order_line_id'] )) {

					$importLines = $this->mImportWarehouse->getImportLines ( $importId );
					if (! empty ( $importLines )) {
						foreach ( $importLines as $importLine ) {
							$c = 0;
							foreach ( $_POST ['order_line_id'] as $new ) {
								if ($new == $importLine->import_line_id)
									$c ++;
							}
							if ($c == 0) {
								$this->mImportWarehouse->deleteImportLine ( $importLine->import_line_id );
							}
						}
					}

					$i = 0;
					$totalSale = 0;
					foreach ( $_POST ['order_line_id'] as $importLineID ) {
						if (isset ( $_POST ['expire_date'] [$i] )) {
							$expireDate = strtotime ( $_POST ['expire_date'] [$i] );
						} else {
							$expireDate = null;
						}

						//yêu cầu mới.
						// Không load giá phần trả về nữa bởi
						// 		- giá trả về  = giá gốc
						// 		- mỗi sản phẩm sẽ có nhiều giá gốc
						// Khi đó giá sẽ được sử lý khi xác nhận trả về.
						$_POST ['product_price'] [$i] = 0;

						$lineTotalSale = $_POST ['product_amount'] [$i] * $_POST ['product_price'] [$i];
						$totalSale += $lineTotalSale;
						$dataX = array (
								"product_id" => $_POST ['product_id'] [$i],
								"product_amount" => $_POST ['product_amount'] [$i],
								"order_id" => $importId,
								"warehouse_id" => $_POST ['warehouse_id'] [$i],
								"product_tax" => null,
								"product_total_price" => $lineTotalSale,
								"expire_date" => $expireDate,
								"product_price" => $_POST ['product_price'] [$i]
						);
						if ($importLineID == 0)
							$this->mImportWarehouse->newImportLine ( $dataX );
						elseif ($importLineID > 0)
							$this->mImportWarehouse->updateImportLine ( $importLineID, $dataX );
						$i ++;
					}
					if ($totalSale > 0) {
						$this->mImportWarehouse->updateImportWarehouse ( $importId, array (
								"total_sale_no_tax" => $totalSale,
								"total_sale" => $totalSale
						) );
					}
				}

				$this->page ['data'] ['message'] = "<div class='text-success'>Chỉnh sửa thành công phiếu nhập kho.</div>";
			} else {
				$this->page ['data'] ['message'] = "<div class='text-danger'>Chỉnh sửa phiếu nhập kho không thành công.</div>";
			}
		}

		$this->page ['data'] ['import'] = $this->mImportWarehouse->getImportWarehouse ( $importId );
		$this->page ['data'] ['importLines'] = $this->mImportWarehouse->getImportLines ( $importId );
		$this->page ['data'] ['suppliers'] = $this->mSupplier->getSuppliers ();
		$this->page ['data'] ['warehouses'] = $this->mWarehouse->getWarehouses ();

		if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) {
			$this->page ['data'] ['refund_'] = $this->mRefund->getRefund ( $_REQUEST ['refund_id'] );
			$this->page ['data'] ['urlPrint'] = System::$config->baseUrl . "mua-hang/tra-ve/in/" . $_REQUEST['refund_id'] . System::$config->urlSuffix;
		} else {
			$this->page ['data'] ['urlPrint'] = System::$config->baseUrl . "mua-hang/nhap-kho/in/" . $importId . System::$config->urlSuffix;
		}

		if ( (isset($this->page['data']['import']) && $this->page ['data'] ['import']->status == 3)
				|| (isset($this->page ['data'] ['refund_']) && $this->page ['data'] ['refund_']->status == 3 )) {
			if (isset ( $_REQUEST ['refund'] ) && $_REQUEST ['refund'] == 1) {
				$this->page ['viewPath'] = "buy management/refund product management/ViewItemRefund";
			} else {
				$this->page ['viewPath'] = "buy management/import stock management/ViewImport";
			}
		}

		$this->load->view ( "Template", array (
				"data" => $this->page
		) );
	}

	/**
	 *
	 * @param unknown $orderID
	 */
	public function print1($importId) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE_PRINT;
		$this->user->access ( $this->page ['accessID'] );

		$this->page ['data'] ['import'] = $this->mImportWarehouse->getImportWarehouse ( $importId );
		$this->page ['data'] ['importLines'] = $this->mImportWarehouse->getImportLines ( $importId );
		if(!empty($this->page ['data'] ['importLines']))
		foreach($this->page ['data'] ['importLines'] as $key => $value) {
			$this->page ['data'] ['importLines'][$key]->warehouse = $this->mWarehouse->getWarehouse(
					$value->warehouse_id
			);
		}
		$this->page ['pageTitle'] = "In đơn hàng";
		$this->load->view ( "buy management/import stock management/PrintImport", array("data" => $this->page['data']) );
	}

	/**
	 * Delete import
	 *
	 * @param unknown $importId
	 */
	public function delete($importId) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE_DELETE;
		$this->user->access ( $this->page ['accessID'] );

		$this->mImportWarehouse->deleteImportWarehouse ( $importId );
		$this->router->router ( System::$config->baseUrl . "mua-hang/nhap-kho" . System::$config->urlSuffix );
	}

	/**
	 * @param $importId
	 */
	public function inspection($importId) {
		$this->page ['accessID'] = IMPORT_WAREHOUSE_INSPECTION;
		$this->user->access ( $this->page ['accessID'] );

		$order = $this->mImportWarehouse->getImportWarehouse ( $importId );
		$lines = $this->mImportWarehouse->getImportLines ( $importId );
// 		print_r($order);
// 		print_r($lines);
		foreach ( $lines as $line ) {
// 			print_r(array (
// 					"inventory_date" => $order->import_date,
// 					"order_id" => $importId,
// 					"product_id" => $line->product_id,
// 					"amount" => $line->product_amount,
// 					"warehouse_id" => $line->warehouse_id 
// 			));
			$this->mInventory->newInventory ( array (
					"inventory_date" => $order->import_date,
					"order_id" => $importId,
					"product_id" => $line->product_id,
					"amount" => $line->product_amount,
					"warehouse_id" => $line->warehouse_id,
					"description" => "Nhập kho"
			) );
		}

		$this->mImportWarehouse->inspectionImport ( $importId );
		$this->router->router (
				System::$config->baseUrl
				. "mua-hang/nhap-kho/chinh-sua/"
				. $importId
				. System::$config->urlSuffix
		);
	}
}
/*end of file ImportWarehouse.class.php*/
/*location: ImportWarehouse.class.php*/