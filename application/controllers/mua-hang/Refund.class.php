<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Refund
 * @package App\Controllers
 */
class Refund extends Controller {

	/**
	 * Refund constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MRefund" );
		$this->load->model( "MMenu" );
		$this->load->model( "MImportWarehouse" );
		$this->load->model("MInventory");
		$this->load->model("MSaleOrder");
		$this->load->model("MProduct");
		$this->load->model("MExpense");
		$this->load->model("MWarehouse");

		$this->page[ 'menuID' ] = 2;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 15 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );


		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param int $page
	 */
	public function index( $page = 1 ) {
		$this->page[ 'accessID' ] = REFUND_ORDER;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'refunds' ] = $this->mRefund->getRefunds( $dataSearch );
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'refunds' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		if( isset( $this->page[ 'data' ][ 'refunds' ]->item_list ) )
			$this->page[ 'data' ][ 'refunds' ] = $this->page[ 'data' ][ 'refunds' ]->item_list;
		else 
			$this->page[ 'data' ][ 'refunds' ] = null;
		$this->page['data']['paginationPath'] = "mua-hang/tra-ve/trang/";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}

	/**
	 * Add new refund
	 */
	public  function addNew() {
		$this->page[ 'accessID' ] = REFUND_ORDER_NEW;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = "buy management/refund product management/NewRefund";
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * 
	 * @param unknown $refundID
	 */
	public function delete( $refundID ) {
		$this->page[ 'accessID' ] = REFUND_ORDER_DELETE;
		$this->user->access( $this->page[ 'accessID' ] );
		
		$this->mRefund->delete( $refundID );
		$this->router->router(
				System::$config->baseUrl
				. "mua-hang/tra-ve"
				. System::$config->urlSuffix
		);
	}

	/**
	 * @param $refundId
	 */
	public function print1($refundId) {
		$this->page ['accessID'] = REFUND_ORDER_PRINT;
		$this->user->access ( $this->page ['accessID'] );

		$this->page ['data'] ['import'] = $this->mRefund->getRefund ( $refundId );

		$this->page ['data'] ['importLines'] = $this->mImportWarehouse->getImportLines ( $this->page ['data'] ['import']->import_id );
		if(!empty($this->page ['data'] ['importLines']))
			foreach($this->page ['data'] ['importLines'] as $key => $value) {
				$this->page ['data'] ['importLines'][$key]->warehouse = $this->mWarehouse->getWarehouse(
						$value->warehouse_id
				);
			}
		$this->page ['pageTitle'] = "In phiếu trả về";
		$this->load->view (
				"buy management/refund product management/PrintRefund",
				array("data" => $this->page['data'])
		);
	}

	/**
	 * @param $refundID
	 */
	public function inspection( $refundID ) {
		$this->page[ 'accessID' ] = REFUND_ORDER_INSPECTION;
		$this->user->access( $this->page[ 'accessID' ] );

		$refund = $this->mRefund->getRefund( $refundID );
		$importId = $refund->import_id;

		$lines = $this->mImportWarehouse->getImportLines ( $importId );

		echo "<pre>";
//		print_r( $refund );
//		print_r($lines);

		foreach ($lines as $line) {
			$outputData = $this->mSaleOrder->getOrderLineByProductOrderId($refund->order_id, $line->product_id);
			$outputData = json_decode($outputData[0]['input_lines']);
			$i = 0;
			foreach($outputData as $item) {
				$rootPrice = $this->mImportWarehouse->getImportLine($item->input_line_id);
				$rootPrice = $rootPrice[0]['product_price'];

//				print_r($item);
//				echo $rootPrice;


				if($item->amount >= $line->product_amount) {
					$amount = $line->product_amount;
					if($i == 0) {
						//update
						$this->mImportWarehouse->updateImportLineCustom($line->import_line_id, array(
								"input_line_amount"	=> $amount,
								"product_price"	=> $rootPrice,
								"product_total_price"	=> $amount * $rootPrice,
								"last_update" => time()
						));
					} else {
						//add new record
						$this->mImportWarehouse->newImportLineCustom(array(
								"input_order_id" => $refund->import_id,
								"warehouse_id"	=> $line->warehouse_id,
								"product_id"	=> $line->product_id,
								"input_line_amount"	=> $amount,
								"buy_amount"	=> 0,
								"product_price"	=> $rootPrice,
								"product_total_price"	=> $amount * $rootPrice,
								"expire_date"	=> $line->expire_date,
								"create_time" => time(),
								"last_update" => time()
						));
					}
					break;
			 	} else {
					$amount = $item->amount;
					if($i == 0) {
						//update
						$this->mImportWarehouse->updateImportLineCustom($line->import_line_id, array(
								"input_line_amount"	=> $amount,
								"product_price"	=> $rootPrice,
								"product_total_price"	=> $amount * $rootPrice,
								"last_update" => time()
						));
					} else {
						$this->mImportWarehouse->newImportLineCustom(array(
								"input_order_id" => $refund->import_id,
								"warehouse_id"	=> $line->warehouse_id,
								"product_id"	=> $line->product_id,
								"input_line_amount"	=> $amount,
								"buy_amount"	=> 0,
								"product_price"	=> $rootPrice,
								"product_total_price"	=> $amount * $rootPrice,
								"expire_date"	=> $line->expire_date,
								"create_time" => time(),
								"last_update" => time()
						));
					}
					$line->product_amount = $line->product_amount - $item->amount;
				}
				$i++;
			}
//			print_r($outputData);
//			print_r($line);
		}

		$lines = $this->mImportWarehouse->getImportLines ( $importId );
//		die();

		foreach ( $lines as $line ) {
			$this->mInventory->newInventory ( array (
					"inventory_date" => $refund->refund_date,
					"order_id" => $importId,
					"product_id" => $line->product_id,
					"amount" => $line->product_amount,
					"warehouse_id" => $line->warehouse_id,
					"description" => "Hàng trả về"
			) );

//			$productInventory = $this->mProduct->getProductPriceInWarehouse($line->product_id, $line->warehouse_id);
//			$this->mInventory->updateInventoryTotal(array(
//					"inventory_date" => $refund->refund_date,
//					"order_id" => $importId,
//					"product_id" => $line->product_id,
//					"warehouse_id" => $line->warehouse_id,
//					"opening_stock" => $productInventory->data->item_list[0]->inventory_amount,
//					"amount" => $line->product_amount,
//					"inventory_amount" => $productInventory->data->item_list[0]->inventory_amount
//							+ $line->product_amount,
//					"description"	=> "Chuyển kho",
//					"create_time"	=> time()
//			));
		}
		
		$this->mImportWarehouse->inspectionImport( $importId );

		foreach ($lines as $line) {
			$this->mRefund->update(array(
				"input_line_id" => $line->import_line_id
			), array(
				"status" => 3
			));
		}

		$orderId = $refund->order_id;
		$dataOrderPay = $this->mSaleOrder->getOrderUnpaid($orderId);

//		print_r($dataOrderPay['data']);
//		die();
		if($dataOrderPay['data'][0]['have_to_pay'] < 0) {
			//Create new pay
			$price = (int)($dataOrderPay['data'][0]['order_price'] * (100 -
							$dataOrderPay['data'][0]['discount'])/100 );
			$des = "Mã đơn hàng trả về: " . $refund->import_code
					. " \r\n Đơn hàng bán: " . $dataOrderPay['data'][0]['order_code']
					. " \r\n Giá trị đơn hàng bán đã trừ trả về: " . $price
					. " \r\n Đã thanh toán: " . $dataOrderPay['data'][0]['amount_paid'];
			echo $des;
			$expenseId = $this->mExpense->addNew(array(
					"partner_id" => $refund->customer_id,
					"receiver_name" => $refund->customer_name,
					"description" => $des,
					"expense_date" => time(),
					"pay_type_id" => 1,
					"expense_status_id" => 2
			));
			$expenseId = $expenseId['data']['expense_id'];
			$this->mExpense->addNewLine( array(
					"expense_id" => $expenseId,
					"partner_id" => $refund->customer_id,
					"money" => abs($dataOrderPay['data'][0]['have_to_pay']),
					"description" => $des
			) );
		}

		$this->router->router (
				System::$config->baseUrl
				. "mua-hang/nhap-kho/chinh-sua/"
				. $importId
				. System::$config->urlSuffix
				. "?refund=1&refund_id="
				. $refundID);
	}
}
/*end of file Refund.class.php*/
/*location: Refund.class.php*/