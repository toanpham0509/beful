<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH')) exit("No direct script access allowed");

/**
 * Class Income
 * @package App\Controllers
 */
class Income extends Controller {

    /**
     * Income constructor.
     */
    public function __construct() {
        parent::__construct();

        $this->load->library("HttpRequest");

        $this->load->model("MMenu");
        $this->load->model("MSalesPeople");
        $this->load->model("MCustomer");
        $this->load->model("MWarehouse");
        $this->load->model("MProduct");
        $this->load->model("MProductCategory");
        $this->load->model("MPayType");
        $this->load->model("MReport");
        $this->load->model("MMoney");

        $this->page['menuID'] = 6;
        $this->page['menuLeftCurrent'] = $this->mMenu->getMenu(53);
        $this->page['menus'] = $this->mMenu->getMenus();
        $this->page['menuLefts'] = $this->mMenu->getMenus($this->page['menuID']);

        $this->load->controller("User");
        $this->page['userInfo'] = $this->user->userInfo;
    }

    /**
     * Index
     */
    public function index() {
        $this->page ['accessID'] = REPORT_INCOME;
        $this->user->access ( $this->page ['accessID'] );

        $this->page['pageTitle'] = "Doanh thu bán hàng";
        $this->page['data']['urlPrint'] = System::$config->baseUrl
                . "bao-cao/doanh-thu-ban-hang/in"
                . System::$config->urlSuffix;

        $this->page['data']['salesPeople'] = $this->mSalesPeople->getSalesPeople();
        $this->page['data']['customers'] = $this->mCustomer->getAllCustomer();
        $this->page['data']['warehouses'] = $this->mWarehouse->getWarehouses();
        $this->page['data']['categories'] = $this->mProductCategory->getProductCategories();

        $this->page['data']['payTypes'] = $this->mPayType->getAllPayTypes();
        $this->page['data']['payTypes'] = $this->page['data']['payTypes']['data'];

        if (empty($_REQUEST)) {
            $this->page['viewPath'] = "report/includes/FormSelect";
        } else {
            $this->page['viewPath'] = $this->page['menuLeftCurrent']['view_path'];
        }
        $this->load->view("Template", array("data" => $this->page));
    }

    /**
     * Print
     */
    public function print1() {
        $this->page['viewPath'] = "report/income/ViewList";
        $this->page['mMoney'] = $this->mMoney;
        if (isset($_REQUEST['date_from']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $_REQUEST['date_from'])) {
            $_REQUEST['date_from'] = strtotime($_REQUEST['date_from']);
        } else {
            $_REQUEST['date_from'] = 0;
        }
        if (isset($_REQUEST['date_to'])) {
            $_REQUEST['date_to'] = strtotime($_REQUEST['date_to']) + 86399;
        } else {
            $_REQUEST['date_to'] = time() + 24 * 60 * 60 * 365;
        }
        if (isset($_REQUEST['customer_id']) && $_REQUEST['customer_id'] <= 0)
            unset($_REQUEST['customer_id']);
        if (isset($_REQUEST['product_category_id']) && $_REQUEST['product_category_id'] <= 0)
            unset($_REQUEST['product_category_id']);
        if (isset($_REQUEST['pay_type_id']) && $_REQUEST['pay_type_id'] <= 0)
            unset($_REQUEST['pay_type_id']);
        if (isset($_REQUEST['sale_person_id']) && $_REQUEST['sale_person_id'] <= 0)
            unset($_REQUEST['sale_person_id']);

        $this->page['data']['report'] = $this->mReport->statisticsSales($_REQUEST);
        $this->page['report'] = $this->page['data']['report']['data']['item_list'];
        
        $this->load->view($this->page['viewPath'], array("data" => $this->page));
    }
}
/*end of file Income.class.php*/
/*location: Income.class.php*/