<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class ReportImportExport extends Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("HttpRequest");
        $this->load->model("MMenu");
        $this->load->model("MReport");
        $this->load->model("MWarehouse");
        $this->load->model("MProduct");

        $this->page['menuID'] = 6;
        $this->page['menuLeftCurrent'] = $this->mMenu->getMenu(59);
        $this->page['menus'] = $this->mMenu->getMenus();
        $this->page['menuLefts'] = $this->mMenu->getMenus($this->page['menuID']);
        $this->page['data']['warehouses'] = $this->mWarehouse->getWarehouses();
        $this->page['data']['products'] = $this->mProduct->getListProducts();
        $this->page['data']['products'] = $this->page['data']['products']->item_list;

        $this->load->controller("User");
        $this->page['userInfo'] = $this->user->userInfo;

        $this->page[ 'accessID' ] = REPORT_IMPORT_EXPORT_INVENTORY;
        $this->user->access( $this->page[ 'accessID' ] );
    }

    public function index() {
        $this->page['data']['pageTitle'] = "Bảng kê nhập xuất";
        $this->page['data']['urlPrint'] = System::$config->baseUrl
                . "bao-cao/bang-ke-nhap-xuat/in"
                . System::$config->urlSuffix;
        if (empty($_REQUEST)) {
            $this->page['viewPath'] = "report/includes/FormSelectImportExport";
        } else {
            $this->page['viewPath'] = $this->page['menuLeftCurrent']['view_path'];
        }

        $dataSearch = array();
        if (isset($_REQUEST['year']) && filter_var($_REQUEST['year'], FILTER_VALIDATE_INT)) {
            $dataSearch['year'] = $_REQUEST['year'];
        }
        if (isset($_REQUEST['sale_person_id']) && filter_var($_REQUEST['sale_person_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['sale_person_id'] = $_REQUEST['sale_person_id'];
        }
        if (isset($_REQUEST['partner_id']) && filter_var($_REQUEST['partner_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['partner_id'] = $_REQUEST['partner_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['pay_type_id']) && filter_var($_REQUEST['pay_type_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['pay_type_id'] = $_REQUEST['pay_type_id'];
        }
        if (isset($_REQUEST['date_from'])) {
            $dataSearch['date_from'] = $_REQUEST['date_from'];
        }
        if (isset($_REQUEST['date_to'])) {
            $dataSearch['date_to'] = $_REQUEST['date_to'];
        }

        $this->page['data']['reports'] = $this->mReport->statisticsSales($dataSearch);


        $this->load->view("Template", array("data" => $this->page));
    }

    public function print1() {


        $dataSearch = array();
        if (isset($_REQUEST['year']) && filter_var($_REQUEST['year'], FILTER_VALIDATE_INT)) {
            $dataSearch['year'] = $_REQUEST['year'];
        }
        if (isset($_REQUEST['sale_person_id']) && filter_var($_REQUEST['sale_person_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['sale_person_id'] = $_REQUEST['sale_person_id'];
        }
        if (isset($_REQUEST['partner_id']) && filter_var($_REQUEST['partner_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['partner_id'] = $_REQUEST['partner_id'];
        }
        if (isset($_REQUEST['product_id']) && filter_var($_REQUEST['product_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_id'] = $_REQUEST['product_id'];
        }
        if (isset($_REQUEST['warehouse_id']) && filter_var($_REQUEST['warehouse_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['warehouse_id'] = $_REQUEST['warehouse_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['pay_type_id']) && filter_var($_REQUEST['pay_type_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['pay_type_id'] = $_REQUEST['pay_type_id'];
        }
        if (isset($_REQUEST['date_from'])) {
            $dataSearch['date_from'] = strtotime($_REQUEST['date_from']);
        }
        if (isset($_REQUEST['date_to'])) {
            $dataSearch['date_to'] = strtotime($_REQUEST['date_to']);
        }



        if ($_REQUEST['warehouse_id'] != '0') {
            $this->page['reports'] = $this->mReport->statisticsImportExport($dataSearch);
            $this->page['viewPath'] = "report/warehouse/ImportExport";
            $this->load->view($this->page['viewPath'], array("data" => $this->page));
        }
        if ($_REQUEST['warehouse_id'] == '0') {
            $this->page['reports'] = $this->mReport->statisticsImportExport($dataSearch);
            $this->page['viewPath'] = "report/warehouse/ImportExportAll";
            $this->load->view($this->page['viewPath'], array("data" => $this->page));
        }
    }

}

/*end of file ReportImportExport.class.php*/
/*location: ReportImportExport.class.php*/
