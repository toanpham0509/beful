<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class TopProduct extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MReport" );
		$data = array(
				"date_from" => 0,
				"date_to" => time() + 24*60*60*365
		);
		$this->page['data']['reportColumn'] = $this->mReport->statisticsSaleTopProduct($data);
		
		$this->page['data']['reportColumn'] = $this->page['data']['reportColumn']['data']['item_list'];
		$this->page['data']['reportLine'] = $this->mReport->statisticsSaleTopProductLines($data);
		$this->page['data']['reportLine'] = $this->page['data']['reportLine']['data']['item_list'];
// 		print_r($this->page['data']['reportLine']);
		
		$this->page[ 'menuID' ] = 6;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 49 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $type = null ) {
		$this->page[ 'pageTitle' ] = "Top sản phẩm";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$this->page[ 'data' ][ 'viewType' ] = "column";
		if( $type == "line" ) {
			$this->page[ 'data' ][ 'viewType' ] = "line";
		} elseif( $type == "pie" ) {
			$this->page[ 'data' ][ 'viewType' ] = "pie";
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	public function line() {
		$this->index("line");
	}
	public function pie() {
		$this->index("pie");
	}
}
/*end of file TopProduct.class.php*/
/*location: TopProduct.class.php*/