<?php

namespace App\Controllers;

use BKFW\Bootstraps\Controller;
use BKFW\Bootstraps\System;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class ReportCard extends Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library("HttpRequest");
        $this->load->model("MMenu");
        $this->load->model("MReport");

        $this->load->model("MSalesPeople");
        $this->load->model("MCustomer");
        $this->load->model("MWarehouse");
        $this->load->model("MProduct");
        $this->load->model("MProductCategory");
        $this->load->model("MPayType");

        $this->page['data']['salesPeople'] = $this->mSalesPeople->getSalesPeople();
        $this->page['data']['customers'] = $this->mCustomer->getAllCustomer();
        $this->page['data']['warehouses'] = $this->mWarehouse->getWarehouses();
        $this->page['data']['categories'] = $this->mProductCategory->getProductCategories();
        $this->page['data']['products'] = $this->mProduct->getListProducts();
        $this->page['data']['products'] = $this->page['data']['products']->item_list;
        $this->page['data']['warehouses'] = $this->mWarehouse->getWarehouses();

        $this->page['data']['payTypes'] = $this->mPayType->getAllPayTypes();
        $this->page['data']['payTypes'] = $this->page['data']['payTypes']['data'];


        $this->page['menuID'] = 6;
        $this->page['menuLeftCurrent'] = $this->mMenu->getMenu(55);
        $this->page['menus'] = $this->mMenu->getMenus();
        $this->page['menuLefts'] = $this->mMenu->getMenus($this->page['menuID']);

        $this->load->controller("User");
        $this->page['userInfo'] = $this->user->userInfo;
    }

    public function index() {
        $this->page['pageTitle'] = "Thẻ kho";
        $this->page['data']['urlPrint'] = System::$config->baseUrl
                . "bao-cao/the-kho/in"
                . System::$config->urlSuffix;
        if (empty($_REQUEST)) {
            $this->page['viewPath'] = "report/includes/FormSelectInventory";
        } else {
            $this->page['viewPath'] = $this->page['menuLeftCurrent']['view_path'];
        }

        $this->load->view("Template", array("data" => $this->page));
    }

    public function print1() {
        $this->page['viewPath'] = "report/warehouse/Card";

        $dataSearch = array();
        if (isset($_REQUEST['year']) && filter_var($_REQUEST['year'], FILTER_VALIDATE_INT)) {
            $dataSearch['year'] = $_REQUEST['year'];
        }
        if (isset($_REQUEST['product_id']) && filter_var($_REQUEST['product_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_id'] = $_REQUEST['product_id'];
            $this->page['data']['product_name'] = $this->mProduct->getProduct($dataSearch['product_id']);
            $this->page['product_name'] = $this->page['data']['product_name']->product_name;
        }
        if (isset($_REQUEST['warehouse_id']) && filter_var($_REQUEST['warehouse_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['warehouse_id'] = $_REQUEST['warehouse_id'];
            $this->page['data']['warehouse_name'] = $this->mWarehouse->getWarehouse($dataSearch['warehouse_id']);
            $this->page['warehouse_name'] = $this->page['data']['warehouse_name']->warehouse_name;
        }
        if (isset($_REQUEST['sale_person_id']) && filter_var($_REQUEST['sale_person_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['sale_person_id'] = $_REQUEST['sale_person_id'];
        }
        if (isset($_REQUEST['partner_id']) && filter_var($_REQUEST['partner_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['partner_id'] = $_REQUEST['partner_id'];
        } else {
            $dataSearch['partner_id'] = null;
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['product_category_id']) && filter_var($_REQUEST['product_category_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['product_category_id'] = $_REQUEST['product_category_id'];
        }
        if (isset($_REQUEST['pay_type_id']) && filter_var($_REQUEST['pay_type_id'], FILTER_VALIDATE_INT)) {
            $dataSearch['pay_type_id'] = $_REQUEST['pay_type_id'];
        }
        if (isset($_REQUEST['date_from'])) {
            $dataSearch['date_from'] = strtotime($_REQUEST['date_from']);
        }
        if (isset($_REQUEST['date_to'])) {
            $dataSearch['date_to'] = strtotime($_REQUEST['date_to']);
        }

        $this->page['data']['reports'] = $this->mReport->statisticsWarehouseTag($dataSearch);
        $data = $this->page['data']['reports']['data']['item_list'];

        $size = sizeof($data);
        for ($i = 0; $i < $size; $i++) {
            for ($j = $i + 1; $j < $size; $j++) {
                if ($data[$i]["create_time"] > $data[$j]["create_time"]) {
                    $temp = $data[$i];
                    $data[$i] = $data[$j];
                    $data[$j] = $temp;
                }
            }
        }
//        echo ("<pre>");
//        print_r($data);
//        echo ('</pre>');
        $this->page['reports'] = $data;

// 		print_r($this->page['reports']);
        $this->load->view($this->page['viewPath'], array("data" => $this->page));
    }

    public function exportExcel() {
        
    }

}

/*end of file ReportCard.class.php*/
/*location: ReportCard.class.php*/