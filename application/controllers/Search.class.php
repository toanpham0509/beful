<?php
namespace 		App\Controllers;
use BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Search extends Controller {
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MMoney" );
		
		$this->page[ 'menuID' ] = 1;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 7 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
	}
	public function index() {
		$this->page[ 'viewPath' ] = "Search";
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
}
/*end of file Search.class.php*/
/*location: Search.class.php*/