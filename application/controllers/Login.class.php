<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Login extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->library( "HttpRequest" );
		$this->load->model("MUser");
	}
	public function index() {
		
		if( isset( $_POST[ 'signin' ] ) ) {
			$userName = $_POST[ 'user_name' ];
			$pasword = $_POST[ 'password' ];
			
			$userInfo = $this->mUser->login( array(
				"user_name" => $userName,
				"user_password" => $pasword
			) );
			
			if( empty( $userInfo ) ) {
				$this->page[ 'message' ] = "Tên đăng nhập hoặc mật khẩu sai.";
			} else {
				$this->router->router( 
					System::$config->baseUrl
				);
			}
		}
		
		$this->load->view( "Login", array( "data" => $this->page ) );
	}
}
/*end of file Login.class.php*/
/*location: Login.class.php*/