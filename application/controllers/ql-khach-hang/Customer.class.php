<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Customer
 * @package App\Controllers
 */
class Customer extends Controller {

	/**
	 * Customer constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MCustomer" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		$this->load->model( "MMenu" );
		
		$this->page[ 'menuID' ] = 3;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 17 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
		
	}
	public function index(  $page = 1 ) {
		$this->page ['accessID'] = CUSTOMER;
		$this->user->access ( $this->page ['accessID'] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách khách hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getCustomers( $dataSearch );
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'customers' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page[ 'data' ][ 'customers' ] = $this->page[ 'data' ][ 'customers' ]->item_list;
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}

	/**
	 * Add new
	 */
	public function addNew() {
		$this->page ['accessID'] = CUSTOMER_NEW;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Thêm mới khách hàng";
		$this->page[ 'viewPath' ] = "customer management/customer/NewCustomer";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
			
			$dataInsert = array();
			$dataInsert[ 'customer_name' ] = $_POST[ 'customer_name' ];
			$dataInsert[ 'address' ] = isset( $_POST[ 'address' ] ) ? $_POST[ 'address' ] : "";
			$dataInsert[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataInsert[ 'phone' ] = isset( $_POST[ 'phone' ] ) ? $_POST[ 'phone' ] : "";
			$dataInsert[ 'fax' ] = isset( $_POST[ 'fax' ] ) ? $_POST[ 'fax' ] : "";
			$dataInsert[ 'tax_code' ] = isset( $_POST[ 'tax_code' ] ) ? $_POST[ 'tax_code' ] : "";
			$dataInsert[ 'bank_number' ] = isset( $_POST[ 'bank_number' ] ) ? $_POST[ 'bank_number' ] : "";
			$dataInsert[ 'bank_name' ] = isset( $_POST[ 'bank_name' ] ) ? $_POST[ 'bank_name' ] : "";
			$dataInsert[ 'contact_person_name' ] = isset( $_POST[ 'contact_person_name' ] ) ? $_POST[ 'contact_person_name' ] : "";
			$dataInsert[ 'contact_person_phone' ] = isset( $_POST[ 'contact_person_phone' ] ) ? $_POST[ 'contact_person_phone' ] : "";
			$dataInsert[ 'contact_person_email' ] = isset( $_POST[ 'contact_person_email' ] ) ? $_POST[ 'contact_person_email' ] : "";
			$dataInsert[ 'note' ] = isset( $_POST[ 'note' ] ) ? $_POST[ 'note' ] : "";
			
			$customerId = $this->mCustomer->addNew( $dataInsert );
			if( $customerId > 0 ) {
				$this->router->router(
					System::$config->baseUrl
					. "ql-khach-hang/khach-hang"
					. System::$config->urlSuffix
				);
				die();
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Thêm mới thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Thêm mới không thành công.</div>";
			}
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Edit
	 */
	public function edit( $customerId ) {
		$this->page ['accessID'] = CUSTOMER_EDIT_ITEM;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Chỉnh sửa thông tin khách hàng";
		$this->page[ 'viewPath' ] = "customer management/customer/EditCustomer";
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		if( isset( $_POST[ 'save' ] ) ) {
				
			$dataUpdate = array();
			$dataUpdate[ 'customer_name' ] = $_POST[ 'customer_name' ];
			$dataUpdate[ 'address' ] = isset( $_POST[ 'address' ] ) ? $_POST[ 'address' ] : "";
			$dataUpdate[ 'district_id' ] = $_POST[ 'district_id' ];
			$dataUpdate[ 'phone' ] = isset( $_POST[ 'phone' ] ) ? $_POST[ 'phone' ] : "";
			$dataUpdate[ 'fax' ] = isset( $_POST[ 'fax' ] ) ? $_POST[ 'fax' ] : "";
			$dataUpdate[ 'tax_code' ] = isset( $_POST[ 'tax_code' ] ) ? $_POST[ 'tax_code' ] : "";
			$dataUpdate[ 'bank_number' ] = isset( $_POST[ 'bank_number' ] ) ? $_POST[ 'bank_number' ] : "";
			$dataUpdate[ 'bank_name' ] = isset( $_POST[ 'bank_name' ] ) ? $_POST[ 'bank_name' ] : "";
			$dataUpdate[ 'contact_person_name' ] = isset( $_POST[ 'contact_person_name' ] ) ? $_POST[ 'contact_person_name' ] : "";
			$dataUpdate[ 'contact_person_phone' ] = isset( $_POST[ 'contact_person_phone' ] ) ? $_POST[ 'contact_person_phone' ] : "";
			$dataUpdate[ 'contact_person_email' ] = isset( $_POST[ 'contact_person_email' ] ) ? $_POST[ 'contact_person_email' ] : "";
			$dataUpdate[ 'note' ] = isset( $_POST[ 'note' ] ) ? $_POST[ 'note' ] : "";
				
			$result = $this->mCustomer->update( $customerId, $dataUpdate );
			if( $result > 0 ) {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-success'>Lưu thành công.</div>";
			} else {
				$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'>Lưu không thành công.</div>";
			}
		}
		
		$this->page[ 'data' ][ 'customer' ] = $this->mCustomer->getCustomer( $customerId );
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $customerId
	 */
	public function view($customerId) {
		$this->page ['accessID'] = CUSTOMER_VIEW_ITEM;
		$this->user->access ( $this->page ['accessID'] );

		$this->page[ 'pageTitle' ] = "Xem thông tin khách hàng";
		$this->page[ 'viewPath' ] = "customer management/customer/ViewCustomer";

		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
		
		$this->page[ 'data' ][ 'customer' ] = $this->mCustomer->getCustomer( $customerId );
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $customerId
	 */
	public function delete($customerId) {
		$this->page ['accessID'] = CUSTOMER_DELETE_ITEM;
		$this->user->access ( $this->page ['accessID'] );

		$this->mCustomer->delete($customerId);
		System::$router->router(System::$config->baseUrl
				. "ql-khach-hang/khach-hang"
				. System::$config->urlSuffix );
	}
}
/*end of file Customer.class.php*/
/*location: Customer.class.php*/