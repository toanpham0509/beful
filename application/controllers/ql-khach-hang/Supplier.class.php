<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Supplier extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		
		$this->page[ 'menuID' ] = 3;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 18 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = 1 ) {
		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách nhà cung cấp";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = 10;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
}
/*end of file Supplier.class.php*/
/*location: Supplier.class.php*/