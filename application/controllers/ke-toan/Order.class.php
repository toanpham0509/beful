<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Order
 * @package App\Controllers
 */
class Order extends Controller {

	/**
	 * Order constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model( "MSaleOrder" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MSalesPeople" );
		$this->load->model( "MCustomer" );
		$this->load->model("MMoney");
		
		$this->page['data']['mMoney'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 37 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param int $page
	 */
	public function index( $page = 1 ) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_SALE_BUY_ORDER_REFUND;
		$this->user->access( $this->page[ 'accessID' ] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		$this->page[ 'data' ][ 'salesPeople' ] = $this->mSalesPeople->getSalesPeople();
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1; 
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		if( isset( $_REQUEST[ 'customer_id' ] ) && filter_var( $_REQUEST[ 'customer_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'customer_id' ] = $_REQUEST[ 'customer_id' ];
		}
		if( isset( $_REQUEST[ 'salesperson_id' ] ) && filter_var( $_REQUEST[ 'salesperson_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'salesperson_id' ] = $_REQUEST[ 'salesperson_id' ];
		}
		if( isset( $_REQUEST[ 'salesperson_name' ] ) && strlen( $_REQUEST[ 'salesperson_name' ] ) ) {
			$dataSearch[ 'salesperson_name' ] = strip_tags( $_REQUEST[ 'salesperson_name' ] );
		}
		if( isset( $_REQUEST[ 'time_form' ] ) && preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/", $_REQUEST[ 'time_form' ]) ) {
			$dataSearch[ 'time_form' ] = strtotime( $_REQUEST[ 'time_form' ] );
		}
		if( isset( $_REQUEST[ 'time_to' ] ) && preg_match("/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/", $_REQUEST[ 'time_to' ]) ) {
			$dataSearch[ 'time_to' ] = strtotime( $_REQUEST[ 'time_to' ] );
		}
		if( isset( $_REQUEST[ 'order_id' ] ) && filter_var( $_REQUEST[ 'order_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'order_id' ] = $_REQUEST[ 'order_id' ]; 
		}
		if( isset( $_REQUEST[ 'order_status_id' ] ) && filter_var( $_REQUEST[ 'order_status_id' ], FILTER_VALIDATE_INT ) ) {
			$dataSearch[ 'order_status_id' ] = $_REQUEST[ 'order_status_id' ];
		}
		
		if( isset( $_REQUEST[ 'order_price_from' ] ) ) {
			$dataSearch[ 'order_price_from' ] = $_REQUEST[ 'order_price_from' ];
		}
		if( isset( $_REQUEST[ 'order_price_to' ] ) ) {
			$dataSearch[ 'order_price_to' ] = $_REQUEST[ 'order_price_to' ];
		}
		if( isset( $_REQUEST[ 'phone' ] ) ) {
			$dataSearch[ 'phone' ] = $_REQUEST[ 'phone' ];
		}
		
		$this->page['data'][ 'orders' ] = $this->mSaleOrder->getOrders( $dataSearch );
		
		$this->page['data'][ 'orders' ] = $this->page['data'][ 'orders' ]->data;
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'orders' ]->order_count;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['paginationPath'] = "ke-toan/don-hang/trang/";
		$this->page['viewPath'] = "sale management/order management/ViewList.php";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file Order.class.php*/
/*location: Order.class.php*/