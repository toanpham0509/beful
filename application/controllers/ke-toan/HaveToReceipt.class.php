<?php
namespace 		App\Controllers;
use App\Models\MMoney;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class HaveToReceipt
 * @package App\Controllers
 */
class HaveToReceipt extends Controller {

	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MAccounting" );
		$this->load->model("MMoney");
		$this->load->model("MReceipt");

		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 44 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;

		$this->mMoney = new MMoney();
		$this->page['data']['money'] = $this->mMoney;
	}
	public function index( $page = null ) {
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page['data']['haveTopReceipt'] = $this->mAccounting->getCustomerPays($dataSearch);
		$this->page['data']['haveTopReceipt'] = $this->page['data']['haveTopReceipt']['data'];

		$this->page['data']['receipts'] = $this->mReceipt->getReceipts($dataSearch);
		$this->page['data']['haveTopReceipt']['numer_row_total']
				+= $this->page['data']['receipts']['data']['numer_row_total'];
		foreach($this->page['data']['receipts']['data']['item_list'] as $key => $value) {
			$this->page['data']['receipts']['data']['item_list'][$key]['orderId'] = $value['receipt_id'];
			$this->page['data']['receipts']['data']['item_list'][$key]['order_code'] = $value['receipt_code'];
			$this->page['data']['receipts']['data']['item_list'][$key]['customer_name'] = $value['partner_name'];
			$this->page['data']['receipts']['data']['item_list'][$key]['customer_id'] = $value['partner_id'];
			$this->page['data']['receipts']['data']['item_list'][$key]['order_date'] = $value['receipt_date'];
			$this->page['data']['receipts']['data']['item_list'][$key]['newPrice'] = $value['total_price'];
			$this->page['data']['receipts']['data']['item_list'][$key]['amount_paid'] = 0;
			$this->page['data']['receipts']['data']['item_list'][$key]['have_to_pay'] = $value['total_price'];
			$this->page['data']['receipts']['data']['item_list'][$key]['order_status_id'] = 6;
		}
		$this->page['data']['haveTopReceipt']['item_list']
				= array_merge($this->page['data']['haveTopReceipt']['item_list']
						, json_decode(json_encode($this->page['data']['receipts']['data']['item_list'])));

		//sort by date
		$size = sizeof($this->page['data']['haveTopReceipt']['item_list']);
		for($i = 0; $i < $size; $i++) {
			for($j = $i + 1; $j < $size; $j++) {
				if($this->page['data']['haveTopReceipt']['item_list'][$i]->order_date
						< $this->page['data']['haveTopReceipt']['item_list'][$j]->order_date) {
					$a = $this->page['data']['haveTopReceipt']['item_list'][$i];
					$this->page['data']['haveTopReceipt']['item_list'][$i]
							= $this->page['data']['haveTopReceipt']['item_list'][$j];
					$this->page['data']['haveTopReceipt']['item_list'][$j] = $a;
				}
			}
		}

		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['haveTopReceipt']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['haveTopReceipt'] = $this->page['data']['haveTopReceipt']['item_list'];
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file HaveToReceipt.class.php*/
/*location: HaveToReceipt.class.php*/