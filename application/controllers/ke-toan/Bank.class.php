<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Bank
 * @package App\Controllers
 */
class Bank extends Controller {

	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MAccounting" );
		$this->load->model("MMoney");
		$this->page['data']['mMoney'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 47 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_BANK_MONEY;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$dataSearch['pay_type_id'] = "-99";
		
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page['data']['banks'] = $this->mAccounting->account($dataSearch);
// 		print_r($this->page['data']['banks']);
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['banks']['data']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['banks'] = $this->page['data']['banks']['data']['item_list'];
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file Money.class.php*/
/*location: Money.class.php*/