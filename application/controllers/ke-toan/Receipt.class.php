<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
use 				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Receipt extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		$this->load->model("MMenu");
		$this->load->model( "MCustomer" );
		$this->load->model( "MReceipt" );
		$this->load->model( "MPayType" );
		$this->load->model("MMoney");
		
		$this->page['data']['mMoney'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 42 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->page[ 'data' ][ 'payTypes' ] = $this->mPayType->getAllPayTypes();
		$this->page[ 'data' ][ 'payTypes' ] = $this->page[ 'data' ][ 'payTypes' ]['data'];

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * 
	 * @param string $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách phiếu thu";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page['data']['receipts'] = $this->mReceipt->getReceipts($dataSearch);
		$this->page['data']['receipts'] = $this->page['data']['receipts']['data'];
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['receipts']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['receipts'] = $this->page['data']['receipts']['item_list'];
		foreach ($this->page['data']['receipts'] as $k => $v) {
			$this->page['data']['receipts'][$k]['total_price'] = $this->mMoney->addDotToMoney($this->page['data']['receipts'][$k]['total_price']);
		}
// 		print_r($this->page['data']['receipts']);
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Page
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}

	/**
	 * @param null $receiptId
	 */
	public function addNew( $receiptId = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		if( $receiptId == null ) {
			$receiptId = $this->mReceipt->addNew(array(
					"partner_id" => null,
					"payer_name" => null,
					"description"	=> null,
					"receipt_date" => time(),
					"pay_type_id" => null,
					"receipt_status_id" => 1
			));
			$receiptId = $receiptId['data']['receipt_id'];
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/phieu-thu/them-moi/"
				. $receiptId
				. System::$config->urlSuffix
			);
			die();
		}
		
		if( $this->processSave($receiptId) ) {
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/phieu-thu/chinh-sua/"
				. $receiptId
				. System::$config->urlSuffix
			);
		}
		
		$this->page['data'][ 'receipt' ] = $this->mReceipt->getReceipt($receiptId);
		$this->page['data'][ 'receipt' ] = $this->page['data'][ 'receipt' ]['data'][0];
		
		$this->page[ 'viewPath' ] = "account/bank money/receipts/AddNew";
		$this->page[ 'pageTitle' ] = "Thêm mới khách hàng thanh toán";
		
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		
		$this->page[ 'viewPath' ] = "account/bank money/receipts/Edit";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit
	 * 
	 * @param unknown $receiptId
	 */
	public function edit( $receiptId ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page['data']['urlPrint'] = System::$config->baseUrl
				. "ke-toan/phieu-thu/in/"
				. $receiptId;

		$this->page[ 'viewPath' ] = "account/bank money/receipts/Edit";
		$this->page[ 'pageTitle' ] = "Thêm mới khách hàng thanh toán";
		
		if( $this->processSave($receiptId) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-success'><b>Lưu thành công!</b></div>";
		} elseif( isset( $_POST ) && !empty( $_POST ) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'><b>Lưu không thành công!</b></div>";
		}
		
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		
		$this->page['data'][ 'receipt' ] = $this->mReceipt->getReceipt($receiptId);
		$this->page['data'][ 'receipt' ] = $this->page['data'][ 'receipt' ]['data'][0];
		
		$this->page['data'][ 'receipt' ]['lines'] = $this->mReceipt->getLines( $receiptId );
		$this->page['data'][ 'receipt' ]['lines'] = $this->page['data'][ 'receipt' ]['lines']['data']['item_list'];
		
// 		print_r($this->page['data'][ 'receipt' ]);
// 		print_r($this->page['data'][ 'receipt' ]['lines']);
		if($this->page['data']['receipt']['receipt_status_id'] == 3) {
			$this->page[ 'viewPath' ] = "account/bank money/receipts/View";
		} 
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $receiptId
	 */
	public function print1($receiptId) {
		$this->page['data'][ 'order' ] = $this->mReceipt->getReceipt($receiptId);
		$this->page['data'][ 'order' ] = $this->page['data'][ 'order' ]['data'][0];

		$this->page['data']['orderLines'] = $this->mReceipt->getLines( $receiptId );
		$this->page['data']['orderLines'] = $this->page['data']['orderLines']['data']['item_list'];

		$this->page['data']['order' ] = json_decode(json_encode($this->page['data'][ 'order' ]));
		$this->page['data']['orderLines'] = json_decode(json_encode($this->page['data'][ 'orderLines' ]));

		$this->load->view( "account/bank money/receipts/Print", array( "data" => $this->page['data'] ) );
	}

	/**
	 * Delete 
	 * 
	 * @param unknown $receiptId
	 */
	public function delete( $receiptId ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mReceipt->delete($receiptId);
		$this->router->router(
				System::$config->baseUrl
				. "ke-toan/phieu-thu/"
				. System::$config->urlSuffix
		);
	}
	/**
	 * Process save
	 * 
	 * @param unknown $receiptId
	 * @return multitype:string
	 */
	public function processSave( $receiptId ) {
		$errors = array();
		$dataUpdate = array();
		$dataUpdate['receipt_id'] = $receiptId;
		if( isset($_POST[ 'save' ]) ) {
			if( isset( $_POST[ 'partner_id' ] ) && filter_var( $_POST[ 'partner_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'partner_id' ] = $_POST['partner_id']; 
			} else {
				$errors['partner_id'] = "partner_id";
			}
			if( isset( $_POST[ 'payer_name' ] ) && strlen( $_POST['payer_name'] ) ) {
				$dataUpdate[ 'payer_name' ] = $_POST['payer_name'];
			} else {
				$errors['payer_name'] = "payer_name";
			}
			if( isset( $_POST[ 'Bdescription' ] ) && strlen( $_POST['Bdescription'] ) ) {
				$dataUpdate[ 'description' ] = $_POST['Bdescription'];
			} else {
				$dataUpdate[ 'description' ] = "";
			}
			if( isset( $_POST['receipt_date'] ) ) {
				$dataUpdate['receipt_date'] = strtotime($_POST['receipt_date']);
			} else {
				$errors['receipt_date'] = "receipt_date";
			}
			if( isset( $_POST[ 'pay_type_id' ] ) && filter_var( $_POST[ 'pay_type_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_type_id' ] = $_POST['pay_type_id'];
			} else {
				$errors['pay_type_id'] = "pay_type_id";
			}
			if (empty($errors)) {
				//delete old pay line
				$lines = $this->mReceipt->getLines( $receiptId );
				$lines = $lines['data']['item_list'];
				if( !empty( $lines ) ) {
					foreach ( $lines as $payLine ) {
						$c = 0;
						if(isset($_POST['line_id']) && !empty($_POST['line_id']))
						foreach ( $_POST[ 'line_id' ] as $new ) {
							if( $new == $payLine['receipt_line_id'])
								$c++;
						}
						if( $c == 0 ) {
							$this->mReceipt->deleteLine( $payLine['receipt_line_id']);
						}
					}
				}
				$i = 0;
				$size = 0;
				if(isset($_POST['line_id'])) 
					$size = sizeof( $_POST[ 'line_id' ] );
				for ( $i = 0; $i < $size; $i++ ) {
					if( $_POST[ 'line_id' ][$i] == 0 ) {
						//add new
						$this->mReceipt->addNewLine( array(
								"receipt_id" => $receiptId, 
								"partner_id" => null,
								"money" => $_POST[ 'money' ][$i],
								"description" => $_POST[ 'description' ][$i]
						) );
					} else {
						//update
						$this->mReceipt->updateLine( $_POST[ 'line_id' ][$i], array(
								"partner_id" => null,
								"money" => $_POST[ 'money' ][$i],
								"description" => $_POST[ 'description' ][$i]
						) );
					}
				}
				$dataUpdate["receipt_status_id"] = 2;
				
				return $this->mReceipt->update($receiptId, $dataUpdate);
			} else {
// 				print_r($errors);
				return $errors;
			}
		}
	}
	/**
	 * Inspection pay
	 * 
	 * @param unknown $payId
	 */
	public function inspection($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mReceipt->update($payId, array(
			"receipt_status_id" => 3
		));
		System::$router->router(
			System::$config->baseUrl
			. "ke-toan/phieu-thu/chinh-sua/"
			. $payId
			. System::$config->urlSuffix
		);
	}

	public function view($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}

	public function in($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}
}
/*end of file Receipt.class.php*/
/*location: Receipt.class.php*/