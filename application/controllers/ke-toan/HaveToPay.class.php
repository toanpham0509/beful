<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class HaveToPay extends Controller {
	/**
	 * Constructor
	 */
	public function __construct(){
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MAccounting" );
		$this->load->model("MSupplierPay");
		$this->load->model("MImportWarehouse");
		$this->load->model("MMoney");
		$this->load->model("MExpense");

		$this->page['data']['money'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 45 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = null ) {
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
//		$this->page['data']['haveTopReceipt'] = $this->mAccounting->getSupplierPays($dataSearch);
		$imports = $this->mSupplierPay->getSupplierOrdersUnpaidNot();
		$imports = array_reverse($imports["data"]);
		$dataReturn = array();
		$i = 0;
		if(!empty($imports)) {
			foreach($imports as $import) {
				if($i >= $this->page[ 'data' ][ 'startList' ]
						&& $i < $this->page[ 'data' ][ 'startList' ] + PER_PAGE_NUMBER
						&& $import["have_to_pay"] > 0
				) {
					$dataReturn[$i] = $this->mImportWarehouse->getImportWarehouse($import["input_order_id"]);
					$dataReturn[$i]->total_sale = $import["total_sale"];
					$dataReturn[$i]->amount_paid = $import["amount_paid"];
					$dataReturn[$i]->have_to_pay = $import["have_to_pay"];
				}
				$i++;
			}
		}

		$dataSearch['status_id'] = 2;
		$this->page['data']['expenses'] = $this->mExpense->getExpenses($dataSearch);
		$i += $this->page['data']['expenses']['data']['numer_row_total'];
		$this->page['data']['expenses'] = $this->page['data']['expenses']['data']['item_list'];

		foreach($this->page['data']['expenses'] as $key => $value) {
			$this->page['data']['expenses'][$key]['import_id'] = $value['expense_id'];
			$this->page['data']['expenses'][$key]['import_code'] = $value['expense_code'];
			$this->page['data']['expenses'][$key]['import_date'] = $value['expense_date'];
			$this->page['data']['expenses'][$key]['supplier_id'] = $value['partner_id'];
			$this->page['data']['expenses'][$key]['supplier_name'] = $value['partner_name'];
			$this->page['data']['expenses'][$key]['total_sale'] = $value['total_price'];
			$this->page['data']['expenses'][$key]['amount_paid'] = 0;
			$this->page['data']['expenses'][$key]['have_to_pay'] = $value['total_price'];
			$this->page['data']['expenses'][$key]['status_id'] = 6;
		}
		$dataReturn = array_merge($dataReturn, json_decode(json_encode($this->page['data']['expenses'])));

		//sort by date
		$size = sizeof($dataReturn);
		for($i = 0; $i < $size; $i++) {
			for($j = $i + 1; $j < $size; $j++) {
				if($dataReturn[$i]->import_date < $dataReturn[$j]->import_date) {
					$a = $dataReturn[$i];
					$dataReturn[$i] = $dataReturn[$j];
					$dataReturn[$j] = $a;
				}
			}
		}

		$this->page['data']['haveTopReceipt'] = array(
			"numer_row_total" => $i,
			"number_row_show" => PER_PAGE_NUMBER,
			"item_list" => $dataReturn
		);

//		$this->page['data']['haveTopReceipt'] = $this->page['data']['haveTopReceipt']['data'];
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['haveTopReceipt']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['haveTopReceipt'] = $this->page['data']['haveTopReceipt']['item_list'];
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param 		number 		$page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
}
/*end of file HaveToPay.class.php*/
/*location: HaveToPay.class.php*/