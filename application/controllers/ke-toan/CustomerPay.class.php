<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
use 				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class CustomerPay extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library( "HttpRequest" );
		
		$this->load->model( "MMenu" );
		$this->load->model( "MCustomerPay" );
		$this->load->model( "MCustomer" );
		$this->load->model( "MSaleOrder" );
		$this->load->model( "MPayType" );
		$this->load->model( "MMoney" );
		
		$this->page['data']['mMoney'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 39 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page[ 'pageTitle' ] = "Danh sách khách hàng thanh toán";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		//pagination
		$this->page[ 'data' ][ 'customerPays' ] = $this->mCustomerPay->getPays($dataSearch);
		$this->page[ 'data' ][ 'customerPays' ] = $this->page[ 'data' ][ 'customerPays' ]['data'];
		
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'customerPays' ]['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page[ 'data' ][ 'customerPays' ] = $this->page[ 'data' ][ 'customerPays' ]['item_list'];
		$size = sizeof($this->page[ 'data' ][ 'customerPays' ]);
		for ($i = 0; $i < $size; $i++) {
			$total = 0;
			foreach ($this->page[ 'data' ][ 'customerPays' ][$i]['order'] as $item) {
//				$item['order_price'] = $this->mSaleOrder->getOrderTotalSale($item["order_id"]);
				if($item['discount'] > 0) {
					$total += (int)((100 - $item['discount']) * $item['order_price'] / 100) - $item['discount_amount'];
				} else {
					$total += $item['order_price'] - $item['discount_amount'];
				}
			}
			$this->page[ 'data' ][ 'customerPays' ][$i]['total_order'] = $total;
			$this->page[ 'data' ][ 'customerPays' ][$i]['owed_money'] = $this->page[ 'data' ][ 'customerPays' ][$i]['total_order'] 
												- $this->page[ 'data' ][ 'customerPays' ][$i]['total_price']; 
		}

//		echo "<pre>";
//		print_r($this->page[ 'data' ][ 'customerPays' ]);
//		echo "</pre>";
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index($page);
	}
	/**
	 * Add new customer pay
	 * 
	 * @param string $customerPayId
	 */
	public function addNew( $customerPayId = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		if( $customerPayId == null ) {
			$payId = $this->mCustomerPay->newPay(array(
				"customer_id" => null,
				"pay_type_id" => null,
				"pay_money"	=> null,
				"pay_date" => time(),
				"pay_status_id" => 1
			));
			$payId = $payId['data'];
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/khach-hang-thanh-toan/them-moi/"
				. $payId
				. System::$config->urlSuffix
			);
			die();
		}
		
		$this->page['data'][ 'customerPay' ] = $this->mCustomerPay->getPay($customerPayId);
		$this->page['data'][ 'customerPay' ] = $this->page['data'][ 'customerPay' ]['data'][0];
		
		$this->page[ 'viewPath' ] = "account/customer/pay/AddNew";
		$this->page[ 'pageTitle' ] = "Thêm mới khách hàng thanh toán";
		
		if( $this->processSave($customerPayId) ) {
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/khach-hang-thanh-toan/chinh-sua/"
				. $customerPayId
				. System::$config->urlSuffix
			);
		}
		
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		$this->page[ 'data' ][ 'payTypes' ] = $this->mPayType->getAllPayTypes();
		$this->page[ 'data' ][ 'payTypes' ] = $this->page[ 'data' ][ 'payTypes' ]['data'];
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit customer pay
	 * 
	 * @param unknown $customerPayId
	 */
	public function edit( $customerPayId ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'viewPath' ] = "account/customer/pay/Edit";
		$this->page[ 'pageTitle' ] = "Chỉnh sửa khách hàng thanh toán";
		
		if( $this->processSave($customerPayId) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-success'><b>Lưu thành công!</b></div>";
		} elseif( isset( $_POST ) && !empty( $_POST ) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'><b>Lưu không thành công!</b></div>";
		}
		
		$this->page['data'][ 'customerPay' ] = $this->mCustomerPay->getPay($customerPayId);
		$this->page['data'][ 'customerPay' ] = $this->page['data'][ 'customerPay' ]['data'][0];
		
		$this->page['data'][ 'customerPayLines' ] = $this->mCustomerPay->getPayLines( $customerPayId );
		$this->page['data'][ 'customerPayLines' ] = $this->page['data'][ 'customerPayLines' ]['data']['item_list'];

//		if(!empty($this->page['data'][ 'customerPayLines' ])) {
//			foreach($this->page['data'][ 'customerPayLines' ] as $key => $value) {
//				$this->page['data'][ 'customerPayLines' ][$key]['order_price']
//				 = $this->mSaleOrder->getOrderTotalSale($value["order_id"]);
//			}
//		}

		$this->page['data'][ 'payTypes' ] = $this->mPayType->getAllPayTypes();
		$this->page['data'][ 'payTypes' ] = $this->page[ 'data' ][ 'payTypes' ]['data'];
		
		$this->page[ 'data' ][ 'customers' ] = $this->mCustomer->getAllCustomer();
		
		if($this->page['data']['customerPay']['pay_status_id'] == 3) {
			$this->page['viewPath'] = "account/customer/pay/ViewPay";
		}
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Delete customer pay
	 * 
	 * @param unknown $customerPayId
	 */
	public function delete( $customerPayId ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mCustomerPay->deletePay($customerPayId);
		$this->router->router(
			System::$config->baseUrl
			. "ke-toan/khach-hang-thanh-toan"
			. System::$config->urlSuffix
		);
	}
	/**
	 * Process
	 * 
	 * @param unknown $customerPayId
	 * @return unknown|multitype:string |number
	 */
	public function processSave( $customerPayId ){
		$errors = array();
		$dataUpdate = array();
		if(isset($_POST[ 'save' ])) {
			if( isset( $_POST[ 'customer_id' ] ) && filter_var( $_POST[ 'customer_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'customer_id' ] = $_POST[ 'customer_id' ];
			} else {
				$errors['customer_id'] = "customer_id";
			}
			if( isset( $_POST[ 'pay_type_id' ] ) && filter_var( $_POST[ 'pay_type_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_type_id' ] = $_POST[ 'pay_type_id' ];
			} else {
				$errors[ 'pay_type_id' ] = "pay_type_id";
			}
			if( isset( $_POST[ 'pay_money' ] ) && filter_var( $_POST[ 'pay_money' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_money' ] = $_POST[ 'pay_money' ];
			} else {
				$errors[ 'pay_money' ] = "pay_money";
			}
			if( isset( $_POST[ 'pay_date' ] ) ) {
				$dataUpdate[ 'pay_date' ] = strtotime($_POST[ 'pay_date' ]);
			} else {
				$errors[ 'pay_date' ] = "pay_date";
			}
			if( empty( $errors ) ) {
				$dataUpdate['have_to_pay'] = 0;

				if( isset( $_POST[ 'pay_line_id' ] ) && !empty( $_POST[ 'pay_line_id' ] )
					&& isset( $_POST[ 'order_id' ] ) && !empty( $_POST[ 'order_id' ] )
					&& isset( $_POST[ 'amount_paid' ] ) && !empty( $_POST[ 'amount_paid' ] )
				) {
					//delete old pay line
					$payLines = $this->mCustomerPay->getPayLines( $customerPayId );
					$payLines = $payLines['data']['item_list'];
					if( !empty( $payLines ) ) {
						foreach ( $payLines as $payLine ) {
							$c = 0;
							foreach ( $_POST[ 'pay_line_id' ] as $new ) {
								if( $new == $payLine['pay_line_id'] )
									$c++;
							}
							if( $c == 0 ) {
								$this->mCustomerPay->deletePayLine( $payLine['pay_line_id'] );
							}
						}
					}
					$i = 0;
					$size = sizeof( $_POST[ 'pay_line_id' ] );
					for ( $i = 0; $i < $size; $i++ ) {
						$dataUpdate['have_to_pay'] += $_POST['haveToPay'][$i];
						if( $_POST[ 'pay_line_id' ][$i] == 0 ) {
							//add new
							$this->mCustomerPay->addNewPayLine( array(
								"pay_id" => $customerPayId,
								"order_id" => $_POST[ 'order_id' ][$i],
								"amount_paid" => $_POST[ 'amount_paid' ][$i],
								"have_to_pay" => $_POST['haveToPay'][$i]
							) );
						} else {
							//update
							$this->mCustomerPay->updatePayLine( $_POST[ 'pay_line_id' ][$i], array(
								"order_id" => $_POST[ 'order_id' ][$i],
								"amount_paid" => $_POST[ 'amount_paid' ][$i],
								"have_to_pay" => $_POST['haveToPay'][$i]
							) );
						}
					}
				}
				
				$dataUpdate['pay_status_id'] = 2;
				$return = $this->mCustomerPay->updatePay($customerPayId, $dataUpdate);
				return $return['status'];
			} else {
				return $errors;
			}
		} else {
			return 0;
		}
	}
	/**
	 * Inspection
	 * 
	 * @param unknown $payId
	 */
	public function inspection($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mCustomerPay->updatePay($payId, array("pay_status_id" => 3));
		System::$router->router(
			System::$config->baseUrl
			. "ke-toan/khach-hang-thanh-toan/chinh-sua/"
			. $payId
			. System::$config->urlSuffix
		);
	}

	public function view($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}

	public function in($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}
}
/*end of file CustomerPay.php*/
/*location: CustomerPay.php*/