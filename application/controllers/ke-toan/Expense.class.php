<?php
namespace 			App\Controllers;
use 				BKFW\Bootstraps\Controller;
use 				BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class Expense
 * @package App\Controllers
 */
class Expense extends Controller {

	/**
	 * Expense constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library("HttpRequest");
		$this->load->model("MMenu");
		$this->load->model("MSupplier");
		$this->load->model("MExpense");
		$this->load->model("MPayType");
		$this->load->model("MMoney");
		
		$this->page['data']['mMoney'] = $this->mMoney;
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 43 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
		
		$this->page[ 'data' ][ 'payTypes' ] = $this->mPayType->getAllPayTypes();
		$this->page[ 'data' ][ 'payTypes' ] = $this->page[ 'data' ][ 'payTypes' ]['data'];
	}

	/**
	 * @param null $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$page = ( $page > 0 ) ? $page : 1;
		$this->page[ 'pageTitle' ] = "Danh sách phiếu chi";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page['data']['expenses'] = $this->mExpense->getExpenses($dataSearch);
		$this->page['data']['expenses'] = $this->page['data']['expenses']['data'];
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['expenses']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['expenses'] = $this->page['data']['expenses']['item_list'];
		foreach ($this->page['data']['expenses'] as $k => $v) {
			$this->page['data']['expenses'][$k]['total_price'] = $this->mMoney->addDotToMoney($this->page['data']['expenses'][$k]['total_price']);
		}
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * Page
	 * 
	 * @param number $page
	 */
	public function page( $page = 1 ) {
		$this->index( $page );
	}
	/**
	 * Add new
	 * 
	 * @param string $expenseId
	 */
	public function addNew( $expenseId = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		if( $expenseId == null ) {
			$expenseId = $this->mExpense->addNew(array(
					"partner_id" => null,
					"receiver_name" => null,
					"description"	=> null,
					"expense_date" => time(),
					"pay_type_id" => null,
					//"status" => 1
			));
			$expenseId = $expenseId['data']['expense_id'];
			System::$router->router(
			System::$config->baseUrl
			. "ke-toan/phieu-chi/them-moi/"
					. $expenseId
					. System::$config->urlSuffix
			);
			die();
		}
		
		$this->page[ 'viewPath' ] = "account/bank money/expenses/AddNew";
		$this->page[ 'pageTitle' ] = "Thêm mới khách hàng thanh toán";
	
		$this->page ['data'] ['suppliers'] = $this->mSupplier->getSuppliers ();
		
		$this->page['data'][ 'expense' ] = $this->mExpense->getExpense($expenseId);
		$this->page['data'][ 'expense' ] = $this->page['data'][ 'expense' ]['data'][0];
		
		if( $this->processSave($expenseId) ) {
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/phieu-chi/chinh-sua/"
				. $expenseId
				. System::$config->urlSuffix
			);
		}
		
		$this->page[ 'viewPath' ] = "account/bank money/expenses/Edit";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * 
	 * @param unknown $expenseId
	 */
	public function edit( $expenseId ) {
		$this->page['data']['urlPrint'] = System::$config->baseUrl
				. "ke-toan/phieu-chi/in/"
				. $expenseId;

		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'viewPath' ] = "account/bank money/expenses/Edit";
		$this->page[ 'pageTitle' ] = "Thêm mới phiếu chi";
	
		if( $this->processSave($expenseId) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-success'><b>Lưu thành công!</b></div>";
		} elseif( isset( $_POST ) && !empty( $_POST ) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'><b>Lưu không thành công!</b></div>";
		}
		
		$this->page['data'][ 'expense' ] = $this->mExpense->getExpense($expenseId);
		$this->page['data'][ 'expense' ] = $this->page['data'][ 'expense' ]['data'][0];
		
		$this->page['data'][ 'expense' ]['lines'] = $this->mExpense->getLines( $expenseId );
		$this->page['data'][ 'expense' ]['lines'] = $this->page['data'][ 'expense' ]['lines']['data']['item_list'];
		$this->page ['data'] ['suppliers'] = $this->mSupplier->getSuppliers ();
		
// 		print_r($this->page['data'][ 'expense' ]);
// 		print_r($this->page['data'][ 'expense' ]['lines']);

		if($this->page['data']['expense']['expense_status_id'] == 3) {
			$this->page[ 'viewPath' ] = "account/bank money/expenses/View";
		}
	
		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param $expenseId
	 */
	public function print1($expenseId) {
		$this->page['data'][ 'order' ] = $this->mExpense->getExpense($expenseId);
		$this->page['data'][ 'order' ] = $this->page['data'][ 'order' ]['data'][0];

		$this->page['data']['orderLines'] = $this->mExpense->getLines( $expenseId );
		$this->page['data']['orderLines'] = $this->page['data']['orderLines']['data']['item_list'];

		$this->page['data']['order' ] = json_decode(json_encode($this->page['data'][ 'order' ]));
		$this->page['data']['orderLines'] = json_decode(json_encode($this->page['data'][ 'orderLines' ]));

		$this->load->view( "account/bank money/expenses/Print", array( "data" => $this->page['data'] ) );
	}

	/**
	 * 
	 * @param unknown $expenseId
	 */
	public function delete( $expenseId ) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mExpense->delete($expenseId);
		$this->router->router(
				System::$config->baseUrl
				. "ke-toan/phieu-chi/"
				. System::$config->urlSuffix
		);
	}
	/**
	 * 
	 * @param unknown $expenseId
	 * @return multitype:string
	 */
	public function processSave( $expenseId ) {
	$errors = array();
		$dataUpdate = array();
		$dataUpdate['expense_id'] = $expenseId;
		if( isset($_POST[ 'save' ]) ) {
			if( isset( $_POST[ 'partner_id' ] ) && filter_var( $_POST[ 'partner_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'partner_id' ] = $_POST['partner_id']; 
			} else {
				$errors['partner_id'] = "partner_id";
			}
			if( isset( $_POST[ 'receiver_name' ] ) && strlen( $_POST['receiver_name'] ) ) {
				$dataUpdate[ 'receiver_name' ] = $_POST['receiver_name'];
			} else {
				$errors['receiver_name'] = "receiver_name";
			}
			if( isset( $_POST[ 'Bdescription' ] ) && strlen( $_POST['Bdescription'] ) ) {
				$dataUpdate[ 'description' ] = $_POST['Bdescription'];
			} else {
				$dataUpdate[ 'description' ] = "";
			}
			if( isset( $_POST['expense_date'] ) ) {
				$dataUpdate['expense_date'] = strtotime($_POST['expense_date']);
			} else {
				$errors['expense_date'] = "expense_date";
			}
			if( isset( $_POST[ 'pay_type_id' ] ) && filter_var( $_POST[ 'pay_type_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_type_id' ] = $_POST['pay_type_id'];
			} else {
				$errors['pay_type_id'] = "pay_type_id";
			}
			if (empty($errors)) {
				//delete old pay line
				$lines = $this->mExpense->getLines( $expenseId );
				$lines = $lines['data']['item_list'];
				if( !empty( $lines ) ) {
					foreach ( $lines as $payLine ) {
						$c = 0;
						if(isset($_POST['line_id']) && !empty($_POST['line_id']))
						foreach ( $_POST[ 'line_id' ] as $new ) {
							if( $new == $payLine['expense_line_id'])
								$c++;
						}
						if( $c == 0 ) {
							$this->mExpense->deleteLine( $payLine['expense_line_id']);
						}
					}
				}
				$i = 0;
				$size = 0;
				if(isset($_POST['line_id'])) 
					$size = sizeof( $_POST[ 'line_id' ] );
				for ( $i = 0; $i < $size; $i++ ) {
					if( $_POST[ 'line_id' ][$i] == 0 ) {
						//add new
						$this->mExpense->addNewLine( array(
								"expense_id" => $expenseId, 
								"partner_id" => null,
								"money" => $_POST[ 'money' ][$i],
								"description" => $_POST[ 'description' ][$i]
						) );
					} else {
						//update
						$this->mExpense->updateLine( $_POST[ 'line_id' ][$i], array(
								"partner_id" => null,
								"money" => $_POST[ 'money' ][$i],
								"description" => $_POST[ 'description' ][$i]
						) );
					}
				}
				$dataUpdate["expense_status_id"] = 2;
				
				return $this->mExpense->update($expenseId, $dataUpdate);
			} else {
				print_r($errors);
				return $errors;
			}
		}
	}
	/**
	 * Inspection pay
	 *
	 * @param unknown $payId
	 */
	public function inspection($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mExpense->update($payId, array(
				"expense_status_id" => 3
		));
		System::$router->router(
		System::$config->baseUrl
		. "ke-toan/phieu-chi/chinh-sua/"
				. $payId
				. System::$config->urlSuffix
		);
	}

	/**
	 * @param $payId
	 */
	public function in($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}

	/**
	 * @param $payId
	 */
	public function view($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}


}
/*end of file Expense.class.php*/
/*location: Expense.class.php*/