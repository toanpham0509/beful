<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class BankAccount extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->library("HttpRequest");
		
		$this->load->model( "MMenu" );
		$this->load->model( "MBankAccount" );
		$this->load->model( "MCountry" );
		$this->load->model( "MCity" );
		$this->load->model( "MDistrict" );
		
		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu(60);
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );
		
		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
		
		$this->page[ 'data' ][ 'countries' ] = $this->mCountry->getCountries();
		$this->page[ 'data' ][ 'cities' ] = $this->mCity->getCities();
		$this->page[ 'data' ][ 'districts' ] = $this->mDistrict->getDistricts();
	}
	public function index($page = null) {
		$this->page[ 'accessID' ] = ACCOUNT_BANK_MONEY;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];
		
		if($page == null) $page = 1;
		
		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;
		
		$this->page['data']['bankAccounts'] = $this->mBankAccount->getBankAccounts($dataSearch);
		
		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page['data']['bankAccounts']['data']['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		
		$this->page['data']['bankAccounts'] = $this->page['data']['bankAccounts']['data']['item_list'];
// 		print_r($this->page['data']['bankAccounts']);
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	public function page($page = 1) {
		$this->index($page);
	} 
	/**
	 * Add new
	 * 
	 * @param string $bankAccountId
	 */
	public function addNew($bankAccountId = null) {
		$this->page[ 'accessID' ] = ACCOUNT_BANK_MONEY;
		$this->user->access( $this->page[ 'accessID' ] );

		if($bankAccountId == null) {
			$bankAccountId = $this->mBankAccount->newBankAccount(array(
				"account_name" 		=> 		null,
				"account_number" 	=> 		null,
				"open_date" 		=> 		null,
				"bank_name" 		=> 		null,
				"account_status"	=> 		1,
				"address"			=> 		null,
				"district_id"		=> 		null,
				"status"			=> 		0,
				"phone"				=>		null,
				"fax"				=>		null,
				"note"				=>		null
			));
// 			print_r($bankAccountId);
			$bankAccountId = $bankAccountId['data'];
			System::$router->router(
				System::$config->baseUrl 
				. "ke-toan/tai-khoan-ngan-hang/them-moi/" 
				. $bankAccountId 
			);
			die();
		}
		
		$re = $this->processSubmit($bankAccountId);
		if(empty($re)) {
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/tai-khoan-ngan-hang/chinh-sua/"
				. $bankAccountId
				. System::$config->urlSuffix
			);
		}
		
		$this->page['data']['account'] = $_POST;
		
		$this->page['pageTitle'] = System::$lang->getString("add_new");
		$this->page['viewPath'] = "account/bank/NewAccount";
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit
	 * 
	 * @param unknown $bankAccountId
	 */
	public function edit($bankAccountId) {
		$this->page[ 'accessID' ] = ACCOUNT_BANK_MONEY;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page['pageTitle'] = System::$lang->getString("edit");
		$this->page['viewPath'] = "account/bank/NewAccount";
		
		$this->page['data']['account'] = $this->mBankAccount->getBankAccount($bankAccountId);
		$this->page['data']['account'] = $this->page['data']['account']['data'][0];
		if($this->page['data']['account']['open_date'] > 0)
			$this->page['data']['account']['open_date'] = date("Y-m-d", $this->page['data']['account']['open_date']);
		
		if(isset($_POST) && !empty($_POST)) {
			$this->page['data']['account'] = $_POST;
			$this->processSubmit($bankAccountId);
		}
		
// 		print_r($this->page['data']['account']);
		
		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Submit 
	 * 
	 * @param unknown $bankAccountId
	 * @return multitype:
	 */
	public function processSubmit($bankAccountId) {
		if (isset($_POST ['update'])) {
			$errors = array ();
			$dataUpdate = array ();
			
			$dataUpdate['account_status'] = 5;
			
			if(isset($_POST['account_name']) && strlen($_POST['account_name']) > 0) {
				$dataUpdate['account_name'] = strip_tags($_POST['account_name']);
			} else {
				$errors['account_name'] = System::$lang->getString("not_valid");
			}
			if(isset($_POST['account_number']) && strlen($_POST['account_number']) > 0) {
				$dataUpdate['account_number'] = strip_tags($_POST['account_number']);
			} else {
				$errors['account_number'] = System::$lang->getString("not_valid");
			}
			if(isset($_POST['bank_name']) && strlen($_POST['bank_name']) > 0) {
				$dataUpdate['bank_name'] = strip_tags($_POST['bank_name']);
			} else {
				$errors['bank_name'] = System::$lang->getString("not_valid");
			}
			if(isset($_POST['account_status']) && filter_var($_POST['account_status'], FILTER_VALIDATE_INT)) {
				$dataUpdate['account_status'] = $_POST['account_status'];
			} else {
				$errors['status'] = System::$lang->getString("not_valid");
			}
			if(isset($_POST['address'])) {
				$dataUpdate['address'] = strip_tags($_POST['address']);
			}
			if(isset($_POST['district_id']) && filter_var($_POST['district_id'], FILTER_VALIDATE_INT)) {
				$dataUpdate['district_id'] = $_POST['district_id'];
			}
// 			if(isset($_POST['city_id']) && filter_var($_POST['city_id'], FILTER_VALIDATE_INT)) {
// 				$dataUpdate['city_id'] = $_POST['city_id'];
// 			}
// 			if(isset($_POST['country_id']) && filter_var($_POST['country_id'], FILTER_VALIDATE_INT)) {
// 				$dataUpdate['country_id'] = $_POST['country_id'];
// 			}
			if(isset($_POST['phone'])) {
				$dataUpdate['phone'] = strip_tags($_POST['phone']);
			}
			if(isset($_POST['fax'])) {
				$dataUpdate['fax'] = strip_tags($_POST['fax']);
			}
			if(isset($_POST['open_date'])) {
				$dataUpdate['open_date'] = strtotime($_POST['open_date']);
			}
			if(isset($_POST['note'])) {
				$dataUpdate['note'] = $_POST['note'];
			} else {
				$dataUpdate['note'] = "";
			}
			if(empty($errors)) {
				$this->mBankAccount->updateBankAccount($bankAccountId, $dataUpdate);
				$this->page['data']['message'] = System::$lang->getString("save_success");
				return null;
			} else {
				$this->page['data']['message'] = System::$lang->getString("save_not_success");
// 				print_r($errors);
				return array($errors);
			}
		} else {
			return array("status" => 0);
		}
	}
	/**
	 * Delete
	 * 
	 * @param unknown $bankAccountId
	 */
	public function delete($bankAccountId) {
		$this->page[ 'accessID' ] = ACCOUNT_BANK_MONEY;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mBankAccount->deleteBankAccount($bankAccountId);
		$this->router->router(
				System::$config->baseUrl
				. "ke-toan/tai-khoan-ngan-hang"
				. System::$config->urlSuffix
		);
	}
}
/*end of file BankAccount.class.php*/
/*location: BankAccount.class.php*/