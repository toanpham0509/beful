<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
use 			BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class SupplierPay
 * @package App\Controllers
 */
class SupplierPay extends Controller {

	/**
	 * SupplierPay constructor.
	 */
	public function __construct() {
		parent::__construct();

		$this->load->library( "HttpRequest" );

		$this->load->model( "MMenu" );
		$this->load->model( "MSupplierPay" );
		$this->load->model( "MSupplier" );
		$this->load->model( "MMoney" );
		$this->load->model( "MPayType" );

		$this->page['data']['mMoney'] = $this->mMoney;

		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 41 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->page[ 'data' ][ 'payTypes' ] = $this->mPayType->getAllPayTypes();
		$this->page[ 'data' ][ 'payTypes' ] = $this->page[ 'data' ][ 'payTypes' ]['data'];

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}

	/**
	 * @param null $page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		if( !filter_var( $page, FILTER_VALIDATE_INT ) ) $page = 1;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;

		$this->page[ 'pageTitle' ] = "Danh sách khách hàng thanh toán";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];

		$dataSearch = array();
		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;

		//pagination
		$this->page[ 'data' ][ 'supplierPay' ] = $this->mSupplierPay->getPays($dataSearch);
		$this->page[ 'data' ][ 'supplierPay' ] = $this->page[ 'data' ][ 'supplierPay' ]['data'];
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'supplierPay' ]['numer_row_total'];
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}
		$this->page[ 'data' ][ 'supplierPay' ] = $this->page[ 'data' ][ 'supplierPay' ]['item_list'];
// 		print_r($this->page[ 'data' ][ 'supplierPay' ]);
		$size = sizeof($this->page[ 'data' ][ 'supplierPay' ]);
		for ($i = 0; $i < $size; $i++) {
			$total = 0;
//			foreach ($this->page[ 'data' ][ 'supplierPay' ][$i]['order'] as $item) {
//					$total += $item['total_sale'];
//			}
			$this->page[ 'data' ][ 'supplierPay' ][$i]['total_order'] = $this->page[ 'data' ][ 'supplierPay' ][$i]['have_to_pay'];
			$this->page[ 'data' ][ 'supplierPay' ][$i]['owed_money'] = $this->page[ 'data' ][ 'supplierPay' ][$i]['total_order']
											- $this->page[ 'data' ][ 'supplierPay' ][$i]['total_price'];
		}
// 		print_r($this->page[ 'data' ][ 'supplierPay' ]);

		$this->load->view( "Template", array( "data" => $this->page ) );
	}

	/**
	 * @param int $page
	 */
	public function page( $page = 1 ) {
		$this->index($page);
	}

	/**
	 * @param null $supplierPayId
	 */
	public function addNew( $supplierPayId = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		if( $supplierPayId == null ) {
			$payId = $this->mSupplierPay->newPay(array(
					"supplier_id" => null,
					"pay_type_id" => null,
					"pay_money"	=> null,
					"pay_date" => time(),
					"pay_status_id" => 1
			));
			$payId = $payId['data'];
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/thanh-toan-nha-cung-cap/them-moi/"
				. $payId
				. System::$config->urlSuffix
			);
			die();
		}

		$this->page['data'][ 'supplierPay' ] = $this->mSupplierPay->getPay($supplierPayId);
		$this->page['data'][ 'supplierPay' ] = $this->page['data'][ 'supplierPay' ]['data'][0];

		if( $this->processSave($supplierPayId) ) {
			System::$router->router(
				System::$config->baseUrl
				. "ke-toan/thanh-toan-nha-cung-cap/chinh-sua/"
				. $supplierPayId
				. System::$config->urlSuffix
			);
		}

		$this->page[ 'viewPath' ] = "account/supplier/pay/AddNew";
		$this->page[ 'pageTitle' ] = "Thêm mới thanh toán nhà cung cấp";

		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();

		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Edit customer pay
	 *
	 * @param unknown $payId
	 */
	public function edit( $payId ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->page[ 'viewPath' ] = "account/supplier/pay/Edit";
		$this->page[ 'pageTitle' ] = "Chỉnh sửa khách hàng thanh toán";

		if( $this->processSave($payId) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-success'><b>Lưu thành công!</b></div>";
		} elseif( isset( $_POST ) && !empty( $_POST ) ) {
			$this->page[ 'data' ][ 'message' ] = "<div class='text-danger'><b>Lưu không thành công!</b></div>";
		}

		$this->page[ 'data' ][ 'suppliers' ] = $this->mSupplier->getSuppliers();
		$this->page['data'][ 'supplierPay' ] = $this->mSupplierPay->getPay($payId);
		$this->page['data'][ 'supplierPay' ] = $this->page['data'][ 'supplierPay' ]['data'][0];

		$this->page['data'][ 'supplierPayLines' ] = $this->mSupplierPay->getPayLines( $payId );
		$this->page['data'][ 'supplierPayLines' ] = $this->page['data'][ 'supplierPayLines' ]['data']['item_list'];

// 		print_r($this->page['data'][ 'supplierPayLines' ]);
// 		print_r($this->page['data'][ 'supplierPay' ]);

		if($this->page['data']['supplierPay']['pay_status_id'] == 3) {
			$this->page['viewPath'] = "account/supplier/pay/ViewPay";
		}

		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Delete customer pay
	 *
	 * @param unknown $payId
	 */
	public function delete( $customerPayId ) {
		$this->page[ 'accessID' ] = ACCOUNT_PAY_CUSTOMER_SUPPLIER;
		$this->user->access( $this->page[ 'accessID' ] );

		$this->mSupplierPay->deletePay($customerPayId);
		$this->router->router (
			System::$config->baseUrl
			. "ke-toan/thanh-toan-nha-cung-cap"
			. System::$config->urlSuffix
		);
	}

	/**
	 * @param $payId
	 * @return array|int
	 */
	public function processSave( $payId ) {
		$errors = array();
		$dataUpdate = array();
		if(isset($_POST[ 'save' ])) {
			if( isset( $_POST[ 'supplier_id' ] ) && filter_var( $_POST[ 'supplier_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'supplier_id' ] = $_POST[ 'supplier_id' ];
			} else {
				$errors['supplier_id'] = "supplier_id";
			}
			if( isset( $_POST[ 'pay_type_id' ] ) && filter_var( $_POST[ 'pay_type_id' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_type_id' ] = $_POST[ 'pay_type_id' ];
			} else {
				$errors[ 'pay_type_id' ] = "pay_type_id";
			}
			if( isset( $_POST[ 'pay_money' ] ) && filter_var( $_POST[ 'pay_money' ], FILTER_VALIDATE_INT ) ) {
				$dataUpdate[ 'pay_money' ] = $_POST[ 'pay_money' ];
			} else {
				$errors[ 'pay_money' ] = "pay_money";
			}
			if( isset( $_POST[ 'pay_date' ] ) ) {
				$dataUpdate[ 'pay_date' ] = strtotime($_POST[ 'pay_date' ]);
			} else {
				$errors[ 'pay_date' ] = "pay_date";
			}
			if( empty( $errors ) ) {
				if(
					isset( $_POST[ 'pay_line_id' ] ) && !empty( $_POST[ 'pay_line_id' ] )
					&& isset( $_POST[ 'order_id' ] ) && !empty( $_POST[ 'order_id' ] )
					&& isset( $_POST[ 'amount_paid' ] ) && !empty( $_POST[ 'amount_paid' ] )
				) {
					//delete old pay line
					$payLines = $this->mSupplierPay->getPayLines( $payId );
					$payLines = $payLines['data']['item_list'];
					if( !empty( $payLines ) ) {
						foreach ( $payLines as $payLine ) {
							$c = 0;
							foreach ( $_POST[ 'pay_line_id' ] as $new ) {
								if( $new == $payLine['pay_line_id'] )
									$c++;
							}
							if( $c == 0 ) {
								$this->mSupplierPay->deletePayLine( $payLine['pay_line_id'] );
							}
						}
					}
					$i = 0;
					$size = sizeof( $_POST[ 'pay_line_id' ] );
					$dataUpdate['have_to_pay'] = 0;
					for ( $i = 0; $i < $size; $i++ ) {
						$dataUpdate['have_to_pay'] += $_POST['haveToPay'][$i];
						if( $_POST[ 'pay_line_id' ][$i] == 0 ) {
							//add new
							$this->mSupplierPay->addNewPayLine( array(
								"pay_id" => $payId,
								"order_id" => $_POST['order_id'][$i],
								"amount_paid" => $_POST['amount_paid'][$i],
								"have_to_pay" => $_POST['haveToPay'][$i]
							) );
						} else {
							//update
							$this->mSupplierPay->updatePayLine( $_POST[ 'pay_line_id' ][$i], array(
								"order_id" => $_POST[ 'order_id' ][$i],
								"amount_paid" => $_POST[ 'amount_paid' ][$i],
								"have_to_pay" => $_POST['haveToPay'][$i]
							) );
						}
					}
				}
				$dataUpdate['pay_status_id'] = 2;
				$return = $this->mSupplierPay->updatePay($payId, $dataUpdate);
				return $return['status'];
			} else {
				return $errors;
			}
		} else {
			return 0;
		}
	}
	/**
	 * Inspection
	 *
	 * @param unknown $payId
	 */
	public function inspection($payId) {
		$this->mSupplierPay->updatePay($payId, array("pay_status_id" => 3));
		System::$router->router(
			System::$config->baseUrl
			. "ke-toan/thanh-toan-nha-cung-cap/chinh-sua/"
			. $payId
			. System::$config->urlSuffix
		);
	}

	/**
	 * @param $payId
	 */
	public function view($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}

	/**
	 * @param $payId
	 */
	public function in($payId) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_PRINT_PAY_RECEIPT_EXPENSE;
		$this->user->access( $this->page[ 'accessID' ] );
	}
}
/*end of file SupplierPay.class.php*/
/*location: SupplierPay.class.php*/