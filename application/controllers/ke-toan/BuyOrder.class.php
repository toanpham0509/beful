<?php
namespace 		App\Controllers;
use 			BKFW\Bootstraps\Controller;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class BuyOrder extends Controller {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		$this->load->library( "HttpRequest" );

		$this->load->model( "MBuyOrder" );
		$this->load->model( "MMenu" );
		$this->load->model( "MSupplier" );
		$this->load->model( "MWarehouse" );
		$this->load->model( "MMoney" );
		$this->load->model( "MProduct" );

		$this->page[ 'data' ][ 'products' ] = $this->mProduct->getAllProduct();
		$this->page[ 'data' ][ 'products' ] = $this->page[ 'data' ][ 'products' ]->item_list;

		$this->page[ 'menuID' ] = 5;
		$this->page[ 'menuLeftCurrent' ] = $this->mMenu->getMenu( 40 );
		$this->page[ 'menus' ] = $this->mMenu->getMenus();
		$this->page[ 'menuLefts' ] = $this->mMenu->getMenus( $this->page[ 'menuID' ] );

		$this->page['data'][ 'mMoney' ] = $this->mMoney;

		$this->load->controller( "User" );
		$this->page['userInfo'] = $this->user->userInfo;
	}
	/**
	 * Index
	 *
	 * @param 		number 		$page
	 */
	public function index( $page = null ) {
		$this->page[ 'accessID' ] = ACCOUNT_VIEW_SALE_BUY_ORDER_REFUND;
		$this->user->access( $this->page[ 'accessID' ] );

		$page = ( $page == null ) ? 1 : $page;
		$this->page[ 'pageTitle' ] = "Danh sách đơn hàng";
		$this->page[ 'viewPath' ] = $this->page[ 'menuLeftCurrent' ][ 'view_path' ];

		$dataSearch = array();

		$dataSearch[ 'per_page_number' ] = PER_PAGE_NUMBER;
		$dataSearch[ 'limit_number_start' ] = $page;
		$this->page[ 'data' ][ 'startList' ] = ($page - 1) * PER_PAGE_NUMBER;

		if( isset( $_POST[ 'search' ] ) ) {
				
		}

		$this->page[ 'data' ][ 'orders' ] = $this->mBuyOrder->getOrders( $dataSearch );

		//pagination
		$this->page[ 'data' ][ 'page' ] = $page;
		$this->page[ 'data' ][ 'countRecord' ] = $this->page[ 'data' ][ 'orders' ]->num_rows_total;
		if( $this->page[ 'data' ][ 'countRecord' ] % PER_PAGE_NUMBER > 0 ) {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER) + 1;
		} else {
			$this->page[ 'data' ][ 'pages' ] = (int)($this->page[ 'data' ][ 'countRecord' ] / PER_PAGE_NUMBER);
		}

		if(isset($this->page[ 'data' ][ 'orders' ]->item_list))
			$this->page[ 'data' ][ 'orders' ] = $this->page[ 'data' ][ 'orders' ]->item_list;
		else
			$this->page[ 'data' ][ 'orders' ] = array();

		$this->load->view( "Template", array( "data" => $this->page ) );
	}
	/**
	 * Page
	 *
	 * @param string $page
	 */
	public function page( $page = null ) {
		$this->index( $page );
	}
}
/*end of file SaleOrder.class.php*/
/*location: SaleOrder.class.php*/