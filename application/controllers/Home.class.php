<?php
namespace 	App\Controllers;
use 		BKFW\Bootstraps\Controller;
use 		BKFW\Libraries\HttpRequest;
use 		BKFW\Bootstraps\System;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class Home extends Controller {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
	}
	public function index() {
		$this->router->router( 
			System::$config->baseUrl . "ban-hang/don-hang"
		);
	}
	public function testHttpRequest() {
		$this->load->library( "HttpRequest" );
		
		$this->httpRequest->setServer( "http://mycouper.com/api" );
		$this->httpRequest->setData( "ac", "login" );
		
		$response = $this->httpRequest->get();
		echo $response . "<br />";
		
		$response= $this->httpRequest->post();
		echo $response . "<br />";
	}
}
/*end of file Home.class.php*/
/*location: Home.class.php*/