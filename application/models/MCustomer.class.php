<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use 				BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MCustomer extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * Get all customer
	 */
	public function getAllCustomer() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_all_customer" );
		$data = json_decode( $this->httpRequest->send() );
		if(isset($data->data))
			return $data->data;
		else 
			return null;
	}
	/**
	 * Get customers
	 * 
	 * @param unknown $data
	 */
	public function getCustomers( $data = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_customers" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * Get customer
	 * 
	 * @param unknown $customerID
	 */
	public function getCustomer( $customerID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_customer" );
		$this->httpRequest->setData( array( "data_post" => array( "customer_id" => $customerID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( empty( $data ) ) return null;
		return $data->data[ 0 ];
	}
	/**
	 * Add new
	 * 
	 * @param unknown $data
	 * @return number
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=new_customer" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		if( $data->status == 1 ) {
			return $data->data->customer_id;
		} 
		return 0;
	}
	/**
	 * Update
	 * 
	 * @param unknown $customerID
	 * @param unknown $data
	 */
	public function update( $customerID, $data ) {
		$data[ 'customer_id' ] = $customerID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=update_customer" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Delete
	 * 
	 * @param unknown $customerID
	 */
	public function delete( $customerID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=delete_customer" );
		$this->httpRequest->setData( array( "data_post" => array( "customer_id" => $customerID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MCustomer.class.php*/
/*location: MCustomer.class.php*/