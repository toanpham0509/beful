<?php
namespace 		App\Models;
use App\Controllers\ImportWarehouse;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class MRefund
 * @package App\Models
 */
class MRefund extends Model {
	/**
	 * MRefund constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
		$this->loadModel("MImportWarehouse");
		$this->db->connect();
	}

	/**
	 * @param array $dataSearch
	 * @return mixed
	 */
	public function getRefunds( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_refund&action=get_refunds" );
		if( $dataSearch != null )
			$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function newRefund( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_refund&action=new_refund" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		if(isset($data->data->refund_id))
			return $data->data->refund_id;
		return 0;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function updateRefund( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_refund&action=update_refund" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}

	public function update($dataFilters = array(), $dataUpdate = array()) {
		$this->db->where($dataFilters);
		$this->db->update("product_refund", $dataUpdate);
	}

	/**
	 * @param $refundId
	 * @return null
	 */
	public function getRefund( $refundId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_refund&action=get_refund" );
		$this->httpRequest->setData( array( "data_post" => array(
			"refund_id" => $refundId
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data[0] ) )
			return $data->data[0];
		return null;
	}

	/**
	 * @param $refundId
	 * @return int
	 */
	public function delete( $refundId ) {
//		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_refund&action=delete_refund" );
//		$this->httpRequest->setData( array( "data_post" => array(
//				"refund_id" => $refundId
//		) ) );
//		$data = json_decode( $this->httpRequest->send() );
//		if( isset( $data->status ) )
//			return $data->status;
//		return 0;
//		$this->db->where("");


		$refund = $this->getRefund($refundId);
		$orderId = $refund->order_id;
		$inputId = $refund->import_id;


		$importLines = $this->mImportWarehouse->getImportLines($inputId);
		print_r($importLines);
		if(!empty($importLines)) {
			foreach ($importLines as $line) {
				$this->db->where(array(
					"input_line_id" => $line->import_line_id
				));
				$this->db->setItem("status", 1);
				$this->db->update("product_refund");
			}
		}
	}

	/**
	 * @param $outputLineId
	 */
	public function getCountProductRefundByOutputLineId($outputLineId) {
		$this->db->select("input_lines.input_order_id as inputId, product_id");
		$this->db->from("input_lines");
		$this->db->from("product_refund");
		$this->db->where(array(
			"product_refund.output_line_id = $outputLineId",
			"product_refund.input_line_id = input_lines.input_line_id",
			"product_refund.status" => 3
		));
		$data = $this->db->get();
//		print_r($data);
		$count = 0;
		if(!empty($data)) {
			foreach ($data as $item) {
				$this->db->selectSum("input_line_amount", "sum");
				$this->db->where("input_order_id", $item['inputId']);
				$this->db->where("product_id", $item["product_id"]);
				$s = $this->db->get("input_lines");
				$count += $s[0]['sum'];
			}
		}
		return $count;
	}
}
/*end of file MRefund.class.php*/
/*location: MRefund.class.php*/