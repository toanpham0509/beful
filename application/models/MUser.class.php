<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use					BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class MUser
 * @package App\Models
 */
class MUser extends Model {

	/**
	 * @var int
	 */
	private $userId;

	/**
	 * MUser constructor.
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );

		$this->userId = null;
	}

	/**
	 * @param $data
	 * @return null
	 */
	public function logIn( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=user&action=login" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
// 		print_r($data);
// 		die();
		if( isset( $data->data ) ) {
			//lấy các quyền của tài khoản.
			$access = "-0-";
				//get position id
			$positionId = 0;
			$this->userId = $data->data[0]->user_id;
			$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_user_work_list" );
			$this->httpRequest->setData( array( "data_post" => array(
				"user_id" => $data->data[0]->user_id
			) ) );
			$positionId = json_decode( $this->httpRequest->send() );
			
			if( isset( $positionId->data->item_list[0]->position_id ) ) {
				$positionId = $positionId->data->item_list[0]->position_id;
				//lấy các quyền.
				$this->httpRequest->setServer( SERVER_API . "index.php?controller=position_rule&action=get_position_rules" );
				$this->httpRequest->setData( array( "data_post" => array(
						"position_id" => $positionId
				) ) );
				$accessRe = json_decode( $this->httpRequest->send() );
				
				if( isset( $accessRe->data->item_list ) ) {
					foreach ( $accessRe->data->item_list as $item ) {
						$access .= "-" . $item->rule_id . "-";
					}
				} else {
					$access = "-0-";
				}
			} else { 
				$access = "-0-";
			}
			
			ob_start();
			$timeExpried = time() + 86400000 ;
			setcookie( "beful_id_code", time(), $timeExpried, "/", "", false, true );
			setcookie( "beful_user_id", $data->data[0]->user_id, $timeExpried, "/", "", false, true );
			setcookie( "beful_user_name", $data->data[0]->user_name, $timeExpried, "/", "", false, true );
			setcookie( "beful_user_access", $access, $timeExpried, "/", "", false, true );
			return $data->data[0];
		}
		return null;
	}

	/**
	 *
	 */
	public function logOut() {
		ob_start();
		setcookie( "beful_id_code", "", time() - 3600, "/", "", false, true );
		unset( $_COOKIE[ 'beful_id_code' ] );
		setcookie( "beful_user_id", "", time() - 3600, "/", "", false, true );
		unset( $_COOKIE[ 'beful_user_id' ] );
		setcookie( "beful_user_name", "", time() - 3600, "/", "", false, true );
		unset( $_COOKIE['beful_user_name'] );
		setcookie( "beful_user_access", "", time() - 3600, "/", "", false, true );
		unset( $_COOKIE['beful_user_access'] );
	}


	/**
	 * @return array|null
	 */
	public function getUserInfo() {
		ob_start();
		//lấy các quyền của tài khoản.
		$access = "-0-";
		//get position id
		$positionId = 0;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_user_work_list" );
		$this->httpRequest->setData( array( "data_post" => array(
				"user_id" => $_COOKIE['beful_user_id']
		) ) );
		$positionIds = json_decode( $this->httpRequest->send() );
		if(!empty($positionIds->data->item_list)) {
			foreach($positionIds->data->item_list as $positionId) {
				$positionId = $positionId->position_id;
				//lấy các quyền.
				$this->httpRequest->setServer( SERVER_API . "index.php?controller=position_rule&action=get_position_rules" );
				$this->httpRequest->setData( array( "data_post" => array(
						"position_id" => $positionId
				) ) );
				$accessRe = json_decode( $this->httpRequest->send() );

				if( isset( $accessRe->data->item_list ) ) {
					foreach ( $accessRe->data->item_list as $item ) {
						$access .= "-" . $item->rule_id . "-";
					}
				} else {
				}
			}
		} else {
			$access = "-0-";
		}

		if( !isset( $_COOKIE[ 'beful_id_code' ], 
				$_COOKIE[ 'beful_user_id' ], 
				$_COOKIE[ 'beful_user_name' ], 
				$_COOKIE[ 'beful_user_access' ] ) ) return null;
		$data = array();
		$data['beful_id_code']  =  $_COOKIE[ 'beful_id_code' ];
		$data[ 'user_id' ] = $_COOKIE[ 'beful_user_id' ];
		$data[ 'user_name' ] = $_COOKIE[ 'beful_user_name' ];
		$data[ 'user_access' ] = $access;
		$_COOKIE[ 'beful_user_access' ] = $access;
		return $data;
	}
}
/*end of file MUser.class.php*/
/*location: MUser.class.php*/