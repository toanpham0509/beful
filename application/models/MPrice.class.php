<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MPrice extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		//class HttpRequest was included constructor controller
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	public function getAllPrice() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=get_all_price" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	public function getPrices( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=price_list&action=get_prices" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	public function getPrice( $priceID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=price_list&action=get_price" );
		$this->httpRequest->setData( array( "data_post" => array( "price_id" => $priceID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=price_list&action=new_price" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		if( $data->status == 1 ) return $data->data->price_id;
		return 0;
	}
	public function update( $priceID, $data ) {
		$data[ 'price_id' ] = $priceID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=price_list&action=update_price" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function delete( $priceID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=price_list&action=delete_price" );
		$this->httpRequest->setData( array( "data_post" => array( "price_id" => $priceID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function getPriceLines( $priceID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_price&action=get_price_lines" );
		$this->httpRequest->setData( array( "data_post" => array( "price_id" => $priceID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) )
			return $data->data->item_list;
		return null;
	}
	public function newPriceLine( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_price&action=new_price_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function updatePriceLine( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_price&action=update_price_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function deletePriceLine( $priceLineID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_price&action=delete_price_line" );
		$this->httpRequest->setData( array( "data_post" => array( "product_price_id" => $priceLineID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MPrice.class.php*/
/*location: MPrice.class.php*/