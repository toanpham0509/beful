<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MProductCategory extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * 
	 */
	public function getProductCategories() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=category&action=get_product_categories" );
		$this->httpRequest->setData( array( "data_post" => array( "product_id" => 1 ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return ( $data == null ) ? null : $data->data;
	}
	/**
	 * 
	 * @param unknown $data
	 * @return number
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=category&action=new_product_category" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		return ( isset( $data->status ) && $data->status == 1 ) ? $data->data->category_id : 0; 
	}
	/**
	 * 
	 * @param unknown $categoryID
	 */
	public function getProductCategory( $categoryID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=category&action=get_product_category" );
		$this->httpRequest->setData( array( "data_post" => array( "product_category_id" => $categoryID ) ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		return $data->data[0];
	}
	/**
	 * 
	 * @param unknown $categoryID
	 * @param unknown $data
	 */
	public function update( $categoryID, $data ) {
		$data[ 'category_id' ] = $categoryID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=category&action=update_product_category" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 * @param unknown $categoryID
	 */
	public function delete( $categoryID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=category&action=delete_product_category" );
		$this->httpRequest->setData( array( "data_post" => array( "category_id" => $categoryID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MProductCatgory.class.php*/
/*location: MProductCatgory.class.php*/