<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MWarehouse extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * Get warehouses
	 */
	public function getWarehouses() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=get_all_warehouse" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * 
	 * @param unknown $shopID
	 */
	public function getWarehouseByShop( $shopID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=get_warehouse_by_shop" );
		$this->httpRequest->setData( array( "data_post" => array( "shop_id" => $shopID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * Get warehouses
	 */
	public function getWarehouses_( $dataSearch = null ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=get_warehouses" );
		if( $dataSearch == null ) {
			$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		}
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * 
	 * @param unknown $data
	 * @return number
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=new_warehouse" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		
		return ( isset( $data->status ) && $data->status == 1 ) ? $data->data->warehouse_id : 0;
	}
	/**
	 * 
	 * @param unknown $warehouseID
	 */
	public function getWarehouse( $warehouseID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=get_warehouse" );
		$this->httpRequest->setData( array( "data_post" => array( "warehouse_id" => $warehouseID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[0];
	}
	/**
	 * 
	 * @param unknown $warehouseID
	 * @param unknown $data
	 */
	public function update( $warehouseID, $data ) {
		$data[ 'warehouse_id' ] = $warehouseID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=update_warehouse" );
// 		print_r($data);
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 * @param unknown $warehouseID
	 */
	public function delete( $warehouseID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=warehouse&action=delete_warehouse" );
		$this->httpRequest->setData( array( "data_post" => array( "warehouse_id" => $warehouseID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		print_r( $data );
		return $data->status;
	}
}
/*end of file MWarehouse.class.php*/
/*location: MWarehouse.class.php*/