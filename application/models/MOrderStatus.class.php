<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use					BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MOrderStatus extends Model {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * Get all order status
	 */
	public function getAllStatus() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=status&action=get_all_order_status" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
}
/*end of file MOrderStatus.class.php*/
/*location: MOrderStatus.class.php*/