<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MCustomerPay extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
        $this->loadModel("MSaleOrder");
//        $this->httpRequest = new HttpRequest();
//        $this->httpRequest->setMethod("POST");
    }

    /**
     * 
     * @param unknown $data
     */
    public function getPays($data = array()) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE customer_pay.status != '1' AND customer_pay.pay_status_id !='1' ";
        if (isset($data["pay_code"])) {
            $condition = $condition . "AND
            customer_pay.pay_code LIKE '%" . $data["pay_code"] . "%'";
        }
        if (isset($data["customer_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["customer_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            output_order.order_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["order_price"])) {
            $condition = $condition . "AND
            output_order.order_price LIKE '%" . $data["order_price"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            bank_account.account_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }









        //ToanPham. Thêm trường pay_status_id, have_to_pay
        $query = "SELECT
                        customer_pay.pay_id,
                        customer_pay.pay_date,
                        customer_pay.pay_code,
                        customer_pay.have_to_pay,
                        customer_pay.pay_status_id,
                        partner.partner_id AS customer_id,
                        partner.partner_name AS customer_name,
                        bank_account.account_name AS pay_type_name,
                        pay_status.pay_status_name AS status,
                        customer_pay.pay_money AS total_price
                      FROM 
                        customer_pay
                      LEFT JOIN
                        partner
                      ON 
                        customer_pay.customer_id = partner.partner_id
                      LEFT JOIN 
                        pay_line
                      ON
                        customer_pay.pay_id = pay_line.pay_id
                      LEFT JOIN
                        output_order
                      ON
                        pay_line.order_id = output_order.order_id
                      LEFT JOIN
                        pay_status
                      ON
                        customer_pay.pay_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        bank_account
                      ON
                        customer_pay.pay_type_id = bank_account.account_id " . $condition . "
                      GROUP BY
                        customer_pay.pay_id
                      ORDER BY
                        pay_id DESC
                      LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $total = array();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            $q = "SELECT 
                    pay_line.order_id,
                    output_order.order_code,
                    output_order.order_price,
                    output_order.discount_amount,
                    output_order.discount
                   FROM
                    pay_line
                   LEFT JOIN
                    output_order
                   ON
                    pay_line.order_id = output_order.order_id
                   WHERE
                    pay_line.pay_id = " . $out[$i]["pay_id"] . "
                   AND
                    pay_line.status != '1'
                   GROUP BY
                    output_order.order_id
                   ORDER BY
                    pay_line.pay_id DESC";
            $this->db->query($q);
            $order = $this->db->getResult();








            //ToanPham. Lấy tổng tiền cho các đơn hàng (đã trừ trả về).
            if(!empty($order)) {
                foreach ($order as $key => $value) {
                    $order[$key]['order_price'] = $this->mSaleOrder->getOrderTotalSale($value['order_id']);
                }
            }












            $out[$i]["order"] = $order;

//            $t = "SELECT 
//                    SUM(output_order.order_price) AS total_price
//                   FROM
//                    pay_line
//                   LEFT JOIN
//                    output_order
//                   ON
//                    pay_line.order_id = output_order.order_id
//                   WHERE
//                    pay_line.pay_id = " . $out[$i]["pay_id"] . "
//                   GROUP BY
//                    pay_line.pay_id";
//            $this->db->query($t);
//            $total = $this->db->getResult();
//            $out[$i]["total_price"] = $total[0]["total_price"];
        }
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function getPay($payId) {
        $query = "SELECT
                    customer_pay.customer_id,
                    partner.partner_name,
                    partner.partner_address,
                    partner.partner_tax_code,
                    partner.partner_phone_number,
                    partner.partner_fax,    
                    customer_pay.pay_code, 
                    customer_pay.pay_id,
                    customer_pay.pay_type_id,
                    customer_pay.pay_date,
                    customer_pay.pay_money,
                    bank_account.account_name,
                    customer_pay.pay_status_id,
                    pay_status.pay_status_name
                  FROM 
                    customer_pay
                  LEFT JOIN
                    pay_status
                  ON
                    customer_pay.pay_status_id = pay_status.pay_status_id
                  LEFT JOIN 
                    bank_account
                  ON
                    customer_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN 
                    partner
                  ON 
                    customer_pay.customer_id = partner.partner_id
                  LEFT JOIN
                    pay_type
                  ON
                    customer_pay.pay_type_id = bank_account.account_id
                  WHERE
                    customer_pay.pay_id =" . $payId . "
                  AND
                    customer_pay.status !='1'
                  GROUP BY
                    customer_pay.pay_id";
        $this->db->query($query);
        $out = $this->db->getResult();
//        $o = json_encode($out);
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    public function getCustomerOrdersUnpaid($customerID) {
        $query = "SELECT
                    output_order.order_id,
                    output_order.order_code,
                    output_order.order_date,
                    output_order.order_price,
                    output_order.discount_amount,
                    output_order.discount
                  FROM
                    output_order
                  WHERE
                    output_order.partner_id = '" . $customerID . "'
                  AND
                    output_order.status = 3
                  AND
                    output_order.status != 1
                  GROUP BY
                    output_order.order_id";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            $this->db->selectSum("amount_paid", "sum");
            $this->db->where(array(
                "order_id" => $out[$i]["order_id"]
            ));
            $this->db->where("status", "1", "!=");
            $re = $this->db->get("pay_line");
            if(isset($re[0]['sum']))
                $re = $re[0]['sum'];
            else
                $re = 0;
            $out[$i]["amount_paid"] = $re;

            /*
             * A Hoàng làm. làm sai
             */
            /*
            $query2 = "SELECT
                        pay_line.amount_paid
                       FROM
                        pay_line
                       WHERE
                        pay_line.order_id = '" . $out[$i]["order_id"] . "'
                       AND
                        pay_line.status != 1
                       GROUP BY
                        pay_line.order_id";
            $this->db->query($query2);
            $paid = $this->db->getResult();
            if (isset($paid[0]["amount_paid"])) {
                 = $paid[0]["amount_paid"];
            } else {
                $out[$i]["amount_paid"] = 0;
            }
            */



            //ToanPham
            $out[$i]["order_price"] = $this->mSaleOrder->getOrderTotalSale($out[$i]["order_id"]);








            $out[$i]["have_to_pay"] = ($out[$i]["order_price"] * (100 - $out[$i]["discount"])) / 100
                - $out[$i]['discount_amount'] - $out[$i]["amount_paid"];
        }
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function deletePay($payId) {
        $query = "UPDATE
                    `customer_pay` 
                  SET 
                    `status` = '1'
                  WHERE
                    `pay_id`=" . $payId;
        $this->db->query($query);
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $payId
     * @param unknown $dataUpdate
     */
    public function updatePay($payId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('pay_id', $payId);
        $this->db->update("customer_pay");

        $data["status"] = $this->db->update("customer_pay");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $data
     */
    public function newPay($data) {
        $data["create_time"] = time();
//        $query = "INSERT INTO
//                    customer_pay
//                  VALUES 
//                  ('', 
//                  '', 
//                  '" . $data["customer_id"] . "', 
//                  '" . $data["pay_money"] . "', 
//                  '" . $data["pay_date"] . "', 
//                  '0', 
//                  '" . $data["pay_type_id"] . "',
//                  '" . $data["create_time"] . "',
//                  '',
//                  '0')
//                 ";
//        $this->db->query($query);
        $data["customer_id"] = "";
        $data["create_time"] = time();
        $data["status"] = "0";
        $this->db->insert('customer_pay', $data);


        $query_2 = "SELECT 
                    pay_id
                  FROM
                    customer_pay
                  ORDER BY
                    pay_id
                  DESC";
        $this->db->query($query_2);
        $pay_id = $this->db->getResult();
        $pay_code = "TT-" . $pay_id[0]["pay_id"];
//        $query_3 = "UPDATE 
//                        customer_pay
//                    SET 
//                        pay_code = '" . $pay_code . "'
//                    WHERE
//                        pay_id = " . $pay_id[0]["pay_id"];
//        $this->db->query($query_3);

        $update["pay_code"] = $pay_code;
        $this->db->setItem($update);
        $this->db->where('pay_id', $pay_id[0]["pay_id"]);
        $this->db->update("customer_pay");


        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $pay_id[0]["pay_id"];
        return $output;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function getPayLines($payId) {
        $query = "SELECT 
                    pay_line.pay_line_id,
                    output_order.order_id,
                    output_order.order_code,
                    output_order.order_date,
        			output_order.discount,
                    output_order.order_price,
                    pay_line.amount_paid,
                    pay_line.have_to_pay
                FROM
                    pay_line
                LEFT JOIN
                    output_order
                ON 
                    pay_line.order_id = output_order.order_id
                WHERE
                    pay_line.pay_id='" . $payId . "'
                AND
                    pay_line.status !='1'"
        ;
        $this->db->query($query);
        $data = $this->db->getResult();












        //ToanPham
        if(!empty($data)) {
            foreach ($data as $key => $value) {
                $data[$key]['order_price'] = $this->mSaleOrder->getOrderTotalSale($value['order_id']);
            }
        }

















        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $data
        );
        return $output;
    }

    /**
     * 
     * @param unknown $data
     */
    public function addNewPayLine($data) {
        $data["create_time"] = time();
        $data["status"] = "0";
        $data["pay_status_id"] = "0";
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "pay_line_id" => $this->db->insert("pay_line", $data)
        );
        return $output;
    }

    /**
     * 
     * @param unknown $payLineId
     * @param unknown $data
     */
    public function updatePayLine($payLineId, $data) {
        $data["last_update"] = time();
        $this->db->setItem($data);
        $this->db->where('pay_line_id', $payLineId);
        $output["status"] = $this->db->update("pay_line");
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    /**
     * 
     * @param unknown $payLineId
     */
    public function deletePayLine($payLineId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('pay_line_id', $payLineId);
        $data["status"] = $this->db->update("pay_line");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function getSaleOrderPaid($orderId) {
        $query2 = "SELECT
                        pay_line.amount_paid
                       FROM
                        pay_line
                       WHERE
                        pay_line.order_id = '" . $orderId . "'
                       GROUP BY
                        pay_line.order_id";
        $this->db->query($query2);
        $paid = $this->db->getResult();
        if (!isset($paid[0]["amount_paid"])) {
            $paid[0]["amount_paid"] = 0;
        }
        $paid[0]["order_id"] = $orderId;
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $paid[0];
        return $output;
    }

}

/*end of file MCustomerPay.class.php*/
/*location: MCustomerPay.class.php*/