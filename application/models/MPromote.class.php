<?php
namespace 		App\Models;
use		 		BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MPromote extends Model {
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	public function getAllPromotes() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=promote&action=get_promotes" );
		$this->httpRequest->setData( array( "data_post" => array(
			"validity" => 1
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) 
			return $data->data->item_list;
		return null;
	}
	public function getPromote( $promoteID ) {
		
	}
	public function getPromoteProducts( $promoteID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=promote_product&action=get_promote_products" );
		$this->httpRequest->setData( array( "data_post" => array(
			"promote_id" => $promoteID
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) {
			return $data->data->item_list;
		}
		return null;
	}
	public function getPromoteOrders( $promoteID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=promote_order&action=get_promote_orders" );
		$this->httpRequest->setData( array( "data_post" => array(
			"promote_id" => $promoteID
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) {
			return $data->data->item_list;
		}
		return null;
	}
	public function addNew( $data ) {
		
	}
	public function update( $promoteID, $data ) {
		
	}
	
}
/*end of file MPromote.class.php*/
/*location: MPromote.class.php*/