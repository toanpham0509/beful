<?php

namespace App\Models;

use BKFW\Bootstraps\Model;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MBankAccount extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();

        $this->db->connect();
    }

    public function getBankAccounts($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $query = "SELECT
                    bank_account.account_id,
                    bank_account.account_name,
                    bank_account.account_number,
                    bank_account.open_date,
                    bank_account.bank_name,
                    pay_status.pay_status_name,
                    bank_account.address
                 FROM
                    bank_account
                 LEFT JOIN
                    pay_status
                 ON
                    bank_account.account_status = pay_status.pay_status_id
                 WHERE
                    bank_account.status != 1
                 AND
                    bank_account.account_status !=1
                 ORDER BY
                    bank_account.account_id DESC
                 LIMIT
                    " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function getBankAccount($bankAccountId) {
        $query = "SELECT
                    bank_account.account_name,
                    bank_account.account_number,
                    bank_account.open_date,
                    bank_account.bank_name,
                    bank_account.account_status,
                    pay_status.pay_status_name,
                    bank_account.address,
                    bank_account.district_id,
                    city.city_id,
                    country.country_id,
                    bank_account.phone,
                    bank_account.fax,
                    bank_account.note
                 FROM
                    bank_account
                 LEFT JOIN
                    pay_status
                 ON
                    bank_account.account_status = pay_status.pay_status_id
                 LEFT JOIN
                    district
                 ON
                    bank_account.district_id = district.district_id
                 LEFT JOIN
                    city
                 ON
                    district.city_id = city.city_id
                 LEFT JOIN
                    country
                 ON
                    city.country_id = country.country_id
                 WHERE
                    bank_account.account_id = " . $bankAccountId . "
                 AND
                    bank_account.status != 1";
        $this->db->query($query);
        $out = $this->db->getResult();
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    public function newBankAccount($dataInsert) {

        $dataInsert["create_time"] = time();
        $dataInsert["account_id"] = "";
        $dataInsert["status"] = "0";
        $this->db->insert('bank_account', $dataInsert);

        $query_2 = "SELECT 
                    account_id
                  FROM
                    bank_account
                  ORDER BY
                    account_id
                  DESC";
        $this->db->query($query_2);
        $account_id = $this->db->getResult();

        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $account_id[0]["account_id"];
        return $output;
    }

    public function updateBankAccount($accountId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('account_id', $accountId);

        $data["status"] = $this->db->update("bank_account");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function deleteBankAccount($bankAccountId) {
        $dataUpdate["status"] = 1;
        $this->db->setItem($dataUpdate);
        $this->db->where('account_id', $bankAccountId);

        $data["status"] = $this->db->update("bank_account");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

}

/*end of file MBankAccount.class.php*/
/*location: MBankAccount.class.php*/