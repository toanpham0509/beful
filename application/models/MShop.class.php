<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use 				BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MShop extends Model {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		//class HttpRequest was included constructor controller
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * Get shops 
	 * 
	 * @param unknown $data
	 */
	public function getShops( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=workplace&action=get_shops" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * 
	 * @param unknown $shopID
	 */
	public function getShop( $shopID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=workplace&action=get_shop" );
		$this->httpRequest->setData( array( "data_post" => array( "shop_id" => $shopID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	/**
	 * Get shops by sale person
	 * 
	 * @param unknown $salePersonID
	 */
	public function getShopsBySalePerson( $salePersonID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_shop_by_salesperson" );
		$this->httpRequest->setData( array( "data_post" => array( "salesperson_id" => $salePersonID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data ) )
			return $data->data;
		return null;
	}
	/**
	 * 
	 * @param unknown $data
	 */
	public function update( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=workplace&action=update_shop" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * 
	 * @param unknown $data
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=workplace&action=new_shop" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data->shop_id;
	}
	/**
	 * 
	 * @param unknown $shopID
	 */
	public function delete( $shopID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=workplace&action=delete_shop" );
		$this->httpRequest->setData( array( "data_post" => array( "shop_id" => $shopID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MShop.class.php*/
/*location: MShop.class.php*/