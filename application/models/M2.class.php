<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class M2 extends Model {
	public function __construct() {
		parent::__construct();
	}
	public function getF1() {
		$this->loadModel( "M1" );
	}
}
/*end of file M2.class.php*/
/*location: M2.class.php*/