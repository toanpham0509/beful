<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class MSupplier
 * @package App\Models
 */
class MSupplier extends Model {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}

	/**
	 * @param array $dataSearch
	 * @return mixed
	 */
	public function getSuppliers( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_suppliers" );
		$data = json_decode( $this->httpRequest->send() );
		$data = isset($data->data->item_list) ? $data->data->item_list : null;
		$size = sizeof( $data );
		for( $i = 0; $i < $size; $i++ ) {
			$data[ $i ]->customer_id = $data[ $i ]->supplier_id;
			$data[ $i ]->customer_name = $data[ $i ]->supplier_name;
		}
		return $data;
	}

	/**
	 * @param array $dataSearch
	 * @return mixed
	 */
	public function getSuppliers_( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_suppliers" );
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}

	/**
	 * Get supplier
	 *
	 * @param unknown $customerID
	 */
	public function getSupplier( $supplierID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=get_supplier" );
		$this->httpRequest->setData( array( "data_post" => array( "supplier_id" => $supplierID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( empty( $data ) ) return null;
		return $data->data[ 0 ];
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function newSupplier( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=new_supplier" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		if( $data->status == 1 ) {
			return $data->data->supplier_id;
		} 
		return 0;
	}

	/**
	 * @param $supplierID
	 * @param $data
	 * @return mixed
	 */
	public function updateSupplier( $supplierID, $data  ) {
		$data[ 'supplier_id' ] = $supplierID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=update_supplier" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}

	/**
	 * Delete
	 *
	 * @param unknown $customerID
	 */
	public function delete( $supplierID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner&action=delete_supplier" );
		$this->httpRequest->setData( array( "data_post" => array( "supplier_id" => $supplierID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MSupplier.class.php*/
/*location: MSupplier.class.php*/