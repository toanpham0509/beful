<?php

namespace App\Models;

use BKFW\Bootstraps\Model;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MBuyPrice extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();

        $this->db->connect();
    }

    /**
     * 
     * @param unknown $dataSearch
     */
    public function getBuyPrices($dataSearch) {
        $page = ($dataSearch["limit_number_start"] - 1) * $dataSearch["per_page_number"];
        $query = "SELECT
                    buy_price.buy_price_id,
                    buy_price.buy_price_code,
                    buy_price.buy_price_name,
                    buy_price.supplier_id,
                    partner.partner_name AS supplier_name,
                    buy_price.time_start,
                    buy_price.time_end,
                    buy_price.validity
                  FROM
                    buy_price
                  LEFT JOIN
                    partner
                  ON
                    buy_price.supplier_id = partner.partner_id
                  WHERE
                    buy_price.buy_price_status !=1
                  AND
                    buy_price.status !=1
                  ORDER BY
                    buy_price.buy_price_id DESC";
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function getBuyPrice($buyPriceId) {
        $query = "SELECT
                    buy_price.buy_price_code,
                    buy_price.buy_price_name,
                    buy_price.supplier_id,
                    partner.partner_name AS supplier_name,
                    district.district_name,
                    city.city_name,
                    country.country_name,
                    buy_price.validity,
                    buy_price.time_start,
                    buy_price.time_end
                FROM
                    buy_price
                LEFT JOIN
                    partner
                ON
                    buy_price.supplier_id = partner.partner_id
                LEFT JOIN
                    district
                ON
                    partner.district_id = district.district_id
                LEFT JOIN
                    city
                ON
                    district.city_id = city.city_id
                LEFT JOIN
                    country
                ON
                    city.country_id = country.country_id
                WHERE
                    buy_price.status !=1
                AND
                    buy_price.buy_price_id = " . $buyPriceId;
        $this->db->query($query);
        $out = $this->db->getResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $out;
        return $output;
    }

    /**
     * 
     * @param unknown $buyPriceId
     */
    public function getListBuyPrices($data=array()) {
        $condition = ' ';
        if (isset($data["supplier_id"])) {
            $condition = $condition . "AND
                        buy_price.supplier_id = '" . $data["supplier_id"] . "' ";
        }
        $time = time();
        $query = "SELECT
                    buy_price.buy_price_id,
                    buy_price.buy_price_code,
                    buy_price.buy_price_name,
                    buy_price.supplier_id,
                    partner.partner_name AS supplier_name
                  FROM
                    buy_price
                  LEFT JOIN
                    partner
                  ON
                    buy_price.supplier_id = partner.partner_id
                  WHERE
                    buy_price.buy_price_status !=1
                  AND
                    buy_price.status !=1
                  AND 
                    buy_price.validity = 1
                  AND 
                    buy_price.time_start <= " . $time . "
                  AND
                    buy_price.time_end >= " . $time . "
                  " . $condition . "
                  ORDER BY
                    buy_price.buy_price_id DESC";
//         echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    /**
     * 
     * @param unknown $dataInsert
     */
    public function newBuyPrice($dataInsert) {

//        $dataInsert["buy_price_id"] = "";
        $dataInsert["create_time"] = time();
        $dataInsert["status"] = "0";
        $this->db->insert('buy_price', $dataInsert);

        $query_2 = "SELECT 
                        buy_price_id
                    FROM
                        buy_price
                    ORDER BY
                        buy_price_id DESC";
        $this->db->query($query_2);
        $buyId = $this->db->getResult();
        $buyCode = "GM-" . $buyId[0]["buy_price_id"];

        $update["buy_price_code"] = $buyCode;
        $this->db->setItem($update);
        $this->db->where('buy_price_id', $buyId[0]["buy_price_id"]);
        $this->db->update("buy_price");

        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "buy_price_id" => $buyId[0]["buy_price_id"],
            "buy_price_code" => $buyCode
        );
        return $output;
    }

    /**
     * 
     * @param unknown $dataUpdate
     */
    public function updateBuyPrice($buyPriceId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('buy_price_id', $buyPriceId);

        $data["status"] = $this->db->update("buy_price");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $buyPriceId
     */
    public function deleteBuyPrice($buyPriceId) {
        $dataUpdate["status"] = 1;
        $this->db->setItem($dataUpdate);
        $this->db->where('buy_price_id', $buyPriceId);

        $data["status"] = $this->db->update("buy_price");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $buyPriceId
     */
    public function getBuyPriceLines($buyPriceId) {
        $query = "SELECT
                    buy_price_line.buy_price_line_id,
                    buy_price_line.product_id,
                    product.product_code,
                    product.product_name,
                    buy_price_line.product_price
                FROM
                    buy_price_line
                LEFT JOIN
                    product
                ON
                    buy_price_line.product_id = product.product_id
                WHERE
                    buy_price_line.status != 1
                AND
                    buy_price_line.buy_price_id = " . $buyPriceId . "
                ORDER BY
                    buy_price_line.buy_price_line_id DESC";
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    /**
     * 
     * @param unknown $dataInsert
     */
    public function newBuyPriceLine($dataInsert) {
        $dataInsert["create_time"] = time();
        $dataInsert["status"] = "0";
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "buy_price_line_id" => $this->db->insert("buy_price_line", $dataInsert)
        );
        return $output;
    }

    /**
     * 
     * @param unknown $dataUpdate
     */
    public function updateBuyPriceLine($buyPriceLineId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('buy_price_line_id', $buyPriceLineId);
        $output["status"] = $this->db->update("buy_price_line", $dataUpdate);
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    /**
     * 
     * @param unknown $dataInsert
     */
    public function deleteBuyPriceLine($buyPriceLineId) {
        $dataUpdate["status"] = 1;
        $this->db->setItem($dataUpdate);
        $this->db->where('buy_price_line_id', $buyPriceLineId);
        $output["status"] = $this->db->update("buy_price_line", $dataUpdate);
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    public function getBuyProductPrice($data) {
        $query = "SELECT
                    buy_price_line.product_price
                  FROM
                    buy_price_line
                  WHERE
                    buy_price_line.product_id = " . $data["product_id"] . "
                  AND
                    buy_price_line.buy_price_id = " . $data["buy_price_id"] . "
                  AND
                    buy_price_line.status != 1";
        $this->db->query($query);
        $out = $this->db->getResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $out;
        return $output;
    }

}

/*end of file MBuyPrice.class.php*/
/*location: MBuyPrice.class.php*/