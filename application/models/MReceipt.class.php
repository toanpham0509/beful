<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MReceipt extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
//		$this->httpRequest = new HttpRequest();
//		$this->httpRequest->setMethod( "POST" );
    }

    public function getReceipts($data = array()) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE receipt.status !='1' AND receipt.receipt_status_id !='1' AND receipt_line.status !='1' ";
        if (isset($data["receipt_code"])) {
            $condition = $condition . "AND
            receipt.receipt_code LIKE '%" . $data["receipt_code"] . "%'";
        }
        if (isset($data["partner_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["partner_name"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        $query = "SELECT
                        receipt.receipt_code,
                        receipt.receipt_id,
                        receipt.receipt_date,
                        receipt.partner_id,
                        partner.partner_name,
                        receipt.description,
                        receipt.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        pay_status.pay_status_name,
                        SUM(receipt_line.money) AS total_price
                      FROM 
                        receipt
                      LEFT JOIN
                        partner
                      ON 
                        receipt.partner_id = partner.partner_id
                      LEFT JOIN
                        pay_status
                      ON
                        receipt.receipt_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        receipt_line
                      ON
                        receipt.receipt_id = receipt_line.receipt_id
                      LEFT JOIN
                        bank_account
                      ON
                        receipt.pay_type_id = bank_account.account_id " . $condition . "
                      GROUP BY
                        receipt.receipt_id, receipt_line.receipt_id
                      ORDER BY
                        receipt.receipt_id DESC
                      LIMIT
                        " . $page . "," . $data["per_page_number"];
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function getReceipt($receiptId) {
//        $select = array(
//            "receipt_id",
//            "receipt_code",
//            "pay_type_id",
//            "receipt_date",
//            "payer_name",
//            "partner_id",
//            "description"
//        );
//        $this->db->select($select);
//        $this->db->from("receipt");
//        $this->db->join("partner", 'receipt.`partner_id` = partner.`partner_id`', "LEFT");
//        $this->db->where("receipt_id", $receiptId);
//        $this->db->where("status", '1', "!=");
//        $this->db->where("receipt_status_id", '1', "!=");
        
        $query = "SELECT
                        receipt.receipt_id,
                        receipt.receipt_code,
                        receipt.pay_type_id,
                        receipt.receipt_date,
                        receipt.payer_name,
                        receipt.partner_id,
                        partner.partner_name,
                        partner.partner_address,
                        receipt.description,
                        receipt.receipt_status_id,
                        pay_status.pay_status_name
                      FROM 
                        receipt
                      LEFT JOIN
                        pay_status
                      ON
                        receipt.receipt_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        partner
                      ON 
                        receipt.partner_id = partner.partner_id
                      WHERE
                        receipt.receipt_id = " . $receiptId . "
                      AND
                        receipt.status != '1'";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    public function addNew($data) {


//        $query = "INSERT INTO
//                    receipt
//                  VALUES 
//                  (NULL, 
//                  '', 
//                  '" . $dataUpdate["partner_id"] . "', 
//                  '" . $dataUpdate["receipt_date"] . "',
//                  '" . $dataUpdate["payer_name"] . "',
//                  '" . $dataUpdate["pay_type_id"] . "',
//                  '0', 
//                  '" . $dataUpdate["description"] . "',
//                  '" . $dataUpdate["create_time"] . "',
//                  '',
//                  '0')
//                 ";
//        echo $query;
//        $this->db->query($query);
        $data["receipt_id"] = null;
        $data["create_time"] = time();
        $data["status"] = 0;
        $data["receipt_status_id"] = 1;
        $this->db->insert('receipt', $data);

        $query_2 = "SELECT 
                    receipt_id
                  FROM
                    receipt
                  ORDER BY
                    receipt_id
                  DESC";
        $this->db->query($query_2);
        $receipt_id = $this->db->getResult();
        $receipt_code = "PT-" . $receipt_id[0]["receipt_id"];
//        $query_3 = "UPDATE 
//                        receipt
//                    SET 
//                        receipt_code = '" . $receipt_code . "'
//                    WHERE
//                        receipt_id = " . $receipt_id[0]["receipt_id"];
//        $this->db->query($query_3);

        $update["receipt_code"] = $receipt_code;
        $this->db->setItem($update);
        $this->db->where('receipt_id', $receipt_id[0]["receipt_id"]);
        $this->db->update("receipt");

        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "receipt_id" => $receipt_id[0]["receipt_id"],
            "receipt_code" => $receipt_code
        );
        return $output;
    }

    public function update($receiptId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('receipt_id', $receiptId);
        $this->db->update("receipt");

        $data["status"] = $this->db->update("receipt");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function delete($receiptId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('receipt_id', $receiptId);
        $this->db->update("receipt");
        $data["status"] = $this->db->update("receipt");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function getLines($receiptId) {
        $query = "SELECT
                        receipt_line.receipt_line_id,
                        receipt_line.partner_id,
                        partner.partner_name,
                        category.category_name,
                        receipt_line.money,
                        receipt_line.description
                      FROM 
                        receipt_line
                      LEFT JOIN
                        partner
                      ON 
                        receipt_line.partner_id = partner.partner_id
                      LEFT JOIN
                        partner_category 
                      ON
                        partner_category.partner_id = partner.partner_id
                      LEFT JOIN
                        category
                      ON
                        category.category_id = partner_category.category_id
                      WHERE
                        receipt_line.receipt_id = " . $receiptId . "
                      AND
                        receipt_line.status != '1'";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function addNewLine($data) {
        $data["create_time"] = time();
        $data["status"] = "0";
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "receipt_line_id" => $this->db->insert("receipt_line", $data)
        );
        return $output;
    }

    public function updateLine($lineId, $data) {
        $data["last_update"] = time();
        $this->db->setItem($data);
        $this->db->where('receipt_line_id', $lineId);
        $output["status"] = $this->db->update("receipt_line");
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    public function deleteLine($lineId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('receipt_line_id', $lineId);
        $data["status"] = $this->db->update("receipt_line");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

}

/*end of file MReceipt.class.php*/
/*location: MReceipt.class.php*/