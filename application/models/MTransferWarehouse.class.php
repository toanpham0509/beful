<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MTransferWarehouse extends Model {
	/**
	 *
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	public function getTransfers( $dataSearch = null ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_transfer&action=get_transfers" );
		if( $dataSearch != null )
			$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_transfer&action=new_transfer" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		
		if( isset( $data->data->transfer_id ) )
			return $data->data->transfer_id;
		return 0;
	}
	public function getTransfer( $transferID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_transfer&action=get_transfer" );
		$this->httpRequest->setData( array( "data_post" => array( "transfer_id" => $transferID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	public function updateTransferWarehouse( $transferID, $dataUpdate ) {
		$dataUpdate[ 'transfer_id' ] = $transferID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_transfer&action=update_transfer" );
		$this->httpRequest->setData( array( "data_post" => $dataUpdate ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function delete( $transferID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product_transfer&action=delete_transfer" );
		$this->httpRequest->setData( array( "data_post" => array(
			"transfer_id" => $transferID
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MTransferWarehouse.class.php*/
/*location: MTransferWarehouse.class.php*/