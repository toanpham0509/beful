<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MSearch extends Model {

    public function __construct() {
        parent::__construct();
        $this->httpRequest = new HttpRequest();
        $this->httpRequest->setMethod("POST");

        $this->db->connect();
    }

    public function searchAllOrder($data) {
        $condition = "WHERE
                        output_order.status != 1
                      AND
                        output_order.status != 0 
                      AND
                        output_order.status_id = 3 ";
        //Search order_code
        if (isset($data["order_code"]) && $data["order_code"] != "") {
            $condition = $condition . "AND
                        output_order.order_code LIKE '%" . $data["order_code"] . "%' ";
        }

        //Search customer_name
        if (isset($data["customer_name"]) && $data["customer_name"] != "") {
            $condition = $condition . "AND
                        partner.partner_name LIKE '%" . $data["customer_name"] . "%' ";
        }

        //Search salesperson_name
        if (isset($data["salesperson_name"]) && $data["salesperson_name"] != "") {
            $condition = $condition . "AND
                        user.user_name LIKE '%" . $data["salesperson_name"] . "%' ";
        }

        //Search order_date
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition = $condition . "AND
                        output_order.order_date >= '" . $data["date_from"] . "'
                                       AND
                        output_order.order_date <= '" . $data["date_to"] . "' ";
        }

        //Search order_price
        if (isset($data["order_price"]) && $data["order_price"] != "") {
            $condition = $condition . "AND
                        output_order.order_price = '" . $data["order_price"] . "' ";
        }

        //Search order_status
        if (isset($data["order_status"]) && $data["order_status"] != "") {
            $condition = $condition . "AND
                        output_order.status = '" . $data["order_status"] . "' ";
        }

        $query = "SELECT
                    output_order.order_id,
                    output_order.order_code,
                    output_order.order_date,
                    partner.partner_name AS customer_name,
                    user.user_name AS salesperson_name,
                    output_order.status AS order_status,
                    output_order.order_price,
                    output_order.discount
                  FROM
                    output_order
                  LEFT JOIN
                    partner
                  ON
                    output_order.partner_id = partner.partner_id
                  LEFT JOIN
                    user
                  ON
                    output_order.salesperson_id = user.user_id
                  " . $condition . "
                  ORDER BY
                    output_order.order_id DESC";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

    public function searchAllProduct($data) {
        $condition = "WHERE
                        product.status = 2 ";

        //Search product_code
        if (isset($data["product_code"]) && $data["product_code"] != "") {
            $condition = $condition . "AND
                        product.product_code LIKE '%" . $data["product_code"] . "%' ";
        }

        //Search product_name
        if (isset($data["product_name"]) && $data["product_name"] != "") {
            $condition = $condition . "AND
                        product.product_name LIKE '%" . $data["product_name"] . "%' ";
        }

        //Search unit_name
        if (isset($data["unit_name"]) && $data["unit_name"] != "") {
            $condition = $condition . "AND
                        unit.unit_name LIKE '%" . $data["unit_name"] . "%' ";
        }

        //Search product_barcode
        if (isset($data["product_barcode"]) && $data["product_barcode"] != "") {
            $condition = $condition . "AND
                        product.product_barcode LIKE '%" . $data["product_barcode"] . "%' ";
        }

        $query = "SELECT
                    product.product_id,
                    product.product_code,
                    product.product_name,
                    product.product_barcode AS barcode,
                    unit.unit_name
                  FROM
                    product
                  LEFT JOIN
                    unit
                  ON
                    product.unit_id = unit.unit_id
                  " . $condition . "
                  ORDER BY
                    product.product_id DESC";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

    public function searchAllPriceName($data) {
        $condition = "WHERE
                        price_list.status != 1 ";

        //Search price_name
        if (isset($data["price_name"]) && $data["price_name"] != "") {
            $condition = $condition . "AND
                        price_list.price_name LIKE '%" . $data["price_name"] . "%' ";
        }

        //Search time_start
        if (isset($data["time_start"]) && $data["time_start"] != "") {
            $condition = $condition . "AND
                        price_list.time_start = '" . $data["time_start"] . "' ";
        }

        //Search time_end
        if (isset($data["time_end"]) && $data["time_end"] != "") {
            $condition = $condition . "AND
                        price_list.time_end = '" . $data["time_end"] . "' ";
        }

        $query = "SELECT
                    price_list.price_id,
                    price_list.price_name,
                    price_list.time_start,
                    price_list.time_end
                  FROM
                    price_list
                  " . $condition . "
                  ORDER BY
                    price_list.price_id DESC";
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

//    public function searchAllShop($data) {
//        $condition = "WHERE
//                        workplace.status !=1
//                      AND
//                        workplace.category_id = 15 ";
//
//        //Search workplace_code
//        if (isset($data["workplace_code"]) && $data["workplace_code"] != "") {
//            $condition = $condition . "AND
//                        workplace.workplace_code LIKE '%" . $data["workplace_code"] . "%' ";
//        }
//
//        //Search workplace_name
//        if (isset($data["workplace_name"]) && $data["workplace_name"] != "") {
//            $condition = $condition . "AND
//                        workplace.workplace_name LIKE '%" . $data["workplace_name"] . "%' ";
//        }
//
//        //Search workplace_phone
//        if (isset($data["workplace_phone"]) && $data["workplace_phone"] != "") {
//            $condition = $condition . "AND
//                        workplace.workplace_phone LIKE '%" . $data["workplace_phone"] . "%' ";
//        }
//
//        //Search workplace_address
//        if (isset($data["workplace_address"]) && $data["workplace_address"] != "") {
//            $condition = $condition . "AND
//                        workplace.workplace_address LIKE '%" . $data["workplace_address"] . "%' ";
//        }
//
//        //Search warehouse_name
//        if (isset($data["warehouse_name"]) && $data["warehouse_name"] != "") {
//            $condition = $condition . "AND
//                        warehouse.warehouse_name LIKE '%" . $data["warehouse_name"] . "%' ";
//        }
//
//        //Search warehouse_manager
//        if (isset($data["warehouse_manager"]) && $data["warehouse_manager"] != "") {
//            $condition = $condition . "AND
//                        employee_work.position_id = 4
//                                       AND
//                        user.user_name LIKE '%" . $data["warehouse_manager"] . "%' ";
//        }
//        
//        $query = "SELECT
//                    workplace.workplace_id AS shop_id,
//                    workplace.workplace_code AS shop_code,
//                    workplace.workplace_name AS shop_name,
//                    workplace.workplace_address AS shop_address,
//                    workplace.workplace_phone AS phone,
//                    ";
//    }

    public function searchAllInputOrder($data) {
        $condition = "WHERE
                        input_order.status != 0
                      AND
                        input_order.status != 1 
                      AND
                        input_order.status_id = 15 ";

        //Search input_code
        if (isset($data["input_code"]) && $data["input_code"] != "") {
            $condition = $condition . "AND
                        input_order.input_code LIKE '%" . $data["input_code"] . "%' ";
        }

        //Search supplier_name
        if (isset($data["supplier_name"]) && $data["supplier_name"] != "") {
            $condition = $condition . "AND
                        partner.partner_name LIKE '%" . $data["supplier_name"] . "%' ";
        }

        //Search input_date
        if (isset($data["input_date"]) && $data["input_date"] != "") {
            $condition = $condition . "AND
                        input_order.input_date = '" . $data["input_date"] . "' ";
        }

        //Search intended_date
        if (isset($data["intended_date"]) && $data["intended_date"] != "") {
            $condition = $condition . "AND
                        input_order.intended_date = '" . $data["intended_date"] . "' ";
        }

        //Search total_sale_no_tax
        if (isset($data["total_sale_no_tax"]) && $data["total_sale_no_tax"] != "") {
            $condition = $condition . "AND
                        input_order.total_sale_no_tax = '" . $data["total_sale_no_tax"] . "' ";
        }

        //Search total_sale
        if (isset($data["total_sale"]) && $data["total_sale"] != "") {
            $condition = $condition . "AND
                        input_order.total_sale = '" . $data["total_sale"] . "' ";
        }

        //Search order_status
        if (isset($data["order_status"]) && $data["order_status"] != "") {
            $condition = $condition . "AND
                        input_order.status = '" . $data["order_status"] . "' ";
        }

        $query = "SELECT
                    input_order.input_order_id AS order_id,
                    input_order.input_code AS order_code,
                    input_order.input_date AS order_date,
                    input_order.partner_id AS supplier_id,
                    input_order.status_id,
                    input_order.status,
                    input_order.total_sale_no_tax,
                    input_order.total_sale,
                    input_order.intended_date,
                    partner.partner_name AS supplier_name
                  FROM
                    input_order
                  LEFT JOIN
                    partner
                  ON
                    input_order.partner_id = partner.partner_id
                  " . $condition . "
                  ORDER BY
                    input_order.input_order_id DESC ";
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

//    public function searchAllImportWarehouse($data) {
//        $condition = "WHERE
//                        input_order.status != 0
//                      AND
//                        input_order.status != 1 
//                      AND
//                        input_order.status_id = 14 ";
//
//        //Search import_code
//        if (isset($data["import_code"]) && $data["import_code"] != "") {
//            $condition = $condition . "AND
//                        input_order.input_code LIKE '%" . $data["import_code"] . "%' ";
//        }
//
//        //Search supplier_name
//        if (isset($data["supplier_name"]) && $data["supplier_name"] != "") {
//            $condition = $condition . "AND
//                        partner.partner_name LIKE '%" . $data["supplier_name"] . "%' ";
//        }
//
//        //Search input_date
//        if (isset($data["input_date"]) && $data["input_date"] != "") {
//            $condition = $condition . "AND
//                        input_order.input_date = '" . $data["input_date"] . "' ";
//        }
//
//        //Search total_sale_no_tax
//        if (isset($data["total_sale_no_tax"]) && $data["total_sale_no_tax"] != "") {
//            $condition = $condition . "AND
//                        input_order.total_sale_no_tax = '" . $data["total_sale_no_tax"] . "' ";
//        }
//
//        //Search total_sale
//        if (isset($data["total_sale"]) && $data["total_sale"] != "") {
//            $condition = $condition . "AND
//                        input_order.total_sale = '" . $data["total_sale"] . "' ";
//        }
//
//
//        $query = "SELECT
//                    input_order.input_order_id AS order_id,
//                    input_order.input_code AS order_code,
//                    input_order.input_date AS order_date,
//                    input_order.partner_id AS supplier_id,
//                    input_order.status_id,
//                    input_order.status,
//                    input_order.total_sale_no_tax,
//                    input_order.total_sale,
//                    input_order.intended_date,
//                    partner.partner_name AS supplier_name
//                  FROM
//                    input_order
//                  LEFT JOIN
//                    partner
//                  ON
//                    input_order.partner_id = partner.partner_id
//                  " . $condition . "
//                  ORDER BY
//                    input_order.input_order_id DESC ";
//        $this->db->query($query);
//        $out = $this->db->getResult();
//        return $out;
//    }
//    public function searchAllTranfer($data){
//        
//    }

    public function searchAllSuppiler($data) {
        $condition = "WHERE
                        partner.status != 1
                      AND
                        partner_category.category_id = 2 ";

        //Search supplier_name
        if (isset($data["supplier_name"]) && $data["supplier_name"] != "") {
            $condition = $condition . "AND
                        partner.partner_name LIKE '%" . $data["supplier_name"] . "%' ";
        }

        //Search supplier_address
        if (isset($data["supplier_address"]) && $data["supplier_address"] != "") {
            $condition = $condition . "AND
                        partner.partner_address LIKE '%" . $data["supplier_address"] . "%' ";
        }

        //Search supplier_phone
        if (isset($data["supplier_phone"]) && $data["supplier_phone"] != "") {
            $condition = $condition . "AND
                        partner.partner_phone_number LIKE '%" . $data["supplier_phone"] . "%' 
                        OR
                        partner.partner_fax LIKE '%" . $data["supplier_phone"] . "%'  
                        AND
                        partner_category.category_id = 2 ";
        }

        //Search supplier_tax_code
        if (isset($data["supplier_tax_code"]) && $data["supplier_tax_code"] != "") {
            $condition = $condition . "AND
                        partner.partner_tax_code LIKE '%" . $data["supplier_tax_code"] . "%' ";
        }

        //Search contact_person_name
        if (isset($data["contact_person_name"]) && $data["contact_person_name"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_name LIKE '%" . $data["contact_person_name"] . "%' ";
        }

        //Search contact_person_phone
        if (isset($data["contact_person_phone"]) && $data["contact_person_phone"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_phone LIKE '%" . $data["contact_person_phone"] . "%' ";
        }

        //Search contact_person_email
        if (isset($data["contact_person_email"]) && $data["contact_person_email"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_email LIKE '%" . $data["contact_person_email"] . "%' ";
        }

        $query = "SELECT
                    partner.partner_id AS supplier_id,
                    partner.partner_name AS supplier_name,
                    partner.partner_address AS address,
                    partner.partner_phone_number AS phone,
                    partner.partner_fax AS fax,
                    partner.partner_tax_code AS tax_code,
                    partner.contact_person_name,
                    partner.contact_person_phone,
                    partner.contact_person_email
                  FROM
                    partner
                  LEFT JOIN
                    partner_category
                  ON
                    partner.partner_id = partner_category.partner_id
                  " . $condition . "
                  GROUP BY
                    partner.partner_id
                  ORDER BY
                    partner.partner_id DESC";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

    public function searchAllBuyPrice($data) {
        $condition = "WHERE
                        buy_price.buy_price_status !=1
                      AND
                        buy_price.status !=1 ";

        //Search supplier_name
        if (isset($data["supplier_name"]) && $data["supplier_name"] != "") {
            $condition = $condition . "AND
                        partner.partner_name LIKE '%" . $data["supplier_name"] . "%' ";
        }

        //Search buy_price_name
        if (isset($data["buy_price_name"]) && $data["buy_price_name"] != "") {
            $condition = $condition . "AND
                        buy_price.buy_price_name LIKE '%" . $data["buy_price_name"] . "%' ";
        }

        //Search time_start
        if (isset($data["time_start"]) && $data["time_start"] != "") {
            $condition = $condition . "AND
                        buy_price.time_start = '" . $data["time_start"] . "' ";
        }

        //Search time_end
        if (isset($data["time_end"]) && $data["time_end"] != "") {
            $condition = $condition . "AND
                        buy_price.time_end = '" . $data["time_end"] . "' ";
        }

        $query = "SELECT
                    buy_price.buy_price_id,
                    buy_price.buy_price_code,
                    buy_price.buy_price_name,
                    buy_price.supplier_id,
                    partner.partner_name AS supplier_name,
                    buy_price.time_start,
                    buy_price.time_end,
                    buy_price.validity
                  FROM
                    buy_price
                  LEFT JOIN
                    partner
                  ON
                    buy_price.supplier_id = partner.partner_id
                  " . $condition . "
                  ORDER BY
                    buy_price.buy_price_id DESC";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }
 
    public function searchAllCustomer($data) {
        $condition = "WHERE
                        partner.status != 1 ";

        //Search customer_name
        if (isset($data["customer_name"]) && $data["customer_name"] != "") {
            $condition = $condition . "AND
                        partner.partner_name LIKE '%" . $data["customer_name"] . "%' ";
        }

        //Search cusotmer_address
        if (isset($data["cusotmer_address"]) && $data["cusotmer_address"] != "") {
            $condition = $condition . "AND
                        partner.partner_address LIKE '%" . $data["cusotmer_address"] . "%' ";
        }

        //Search customer_phone
        if (isset($data["customer_phone"]) && $data["customer_phone"] != "") {
            $condition = $condition . "AND
                        partner.partner_phone_number = '%" . $data["customer_phone"] . "%' 
                        OR
                        partner.partner_fax = '%" . $data["customer_phone"] . "%' ";
        }

        //Search contact_person_name
        if (isset($data["contact_person_name"]) && $data["contact_person_name"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_name LIKE '%" . $data["contact_person_name"] . "%' ";
        }

        //Search contact_person_phone
        if (isset($data["contact_person_phone"]) && $data["contact_person_phone"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_phone LIKE '%" . $data["contact_person_phone"] . "%' ";
        }

        //Search contact_person_email
        if (isset($data["contact_person_email"]) && $data["contact_person_email"] != "") {
            $condition = $condition . "AND
                        partner.contact_person_email LIKE '%" . $data["contact_person_email"] . "%' ";
        }

        $query = "SELECT
                    partner.partner_id AS customer_id,
                    partner.partner_name AS customer_name,
                    partner.partner_address AS address,
                    partner.partner_phone_number AS phone,
                    partner.partner_fax AS fax,
                    partner.contact_person_name,
                    partner.contact_person_phone,
                    partner.contact_person_email
                  FROM
                    partner
                  LEFT JOIN
                    partner_category
                  ON
                    partner.partner_id = partner_category.partner_id
                  " . $condition . "
                  AND(
                    partner_category.category_id = 1
                  OR
                    partner_category.category_id = 3)
                  GROUP BY
                    partner.partner_id
                  ORDER BY
                    partner.partner_id DESC";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        return $out;
    }

}
