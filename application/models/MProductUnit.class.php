<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MProductUnit extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	public function getUnits() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=unit&action=get_product_units" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	public function getUnit( $unitID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=unit&action=get_product_unit" );
		$this->httpRequest->setData( array( "data_post" => array( "unit_id" => $unitID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=unit&action=new_product_unit" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		print_r( $data );
		if( $data->status == 1 ) {
			return $data->data->unit_id;
		}
		return 0;
	}
	public function update( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=unit&action=update_product_unit" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function delete( $unitID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=unit&action=update_product_unit" );
		$this->httpRequest->setData( array( "data_post" => array( "unit_id" => $unitID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MProductUnit.class.php*/
/*location: MProductUnit.class.php*/