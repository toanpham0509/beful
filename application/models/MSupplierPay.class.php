<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MSupplierPay extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
//        $this->httpRequest = new HttpRequest();
//        $this->httpRequest->setMethod("POST");
    }

    /**
     * 
     * @param unknown $data
     */
    public function getPays($data = array()) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE supplier_pay.status !='1' AND supplier_pay.pay_status_id !='1' ";
        if (isset($data["pay_code"])) {
            $condition = $condition . "AND
            supplier_pay.pay_code LIKE '%" . $data["pay_code"] . "%'";
        }
        if (isset($data["supplier_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["supplier_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            input_order.input_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["order_price"])) {
            $condition = $condition . "AND
            input_order.total_price LIKE '%" . $data["order_price"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            bank_account.account_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        $query = "SELECT
                        supplier_pay.pay_id,
                        supplier_pay.pay_date,
                        supplier_pay.pay_code,
                        supplier_pay.have_to_pay,
                        supplier_pay.pay_status_id,
                        supplier_pay.pay_money AS total_price,
                        partner.partner_id,
                        partner.partner_name,
                        bank_account.account_name AS pay_type_name,
                        pay_status.pay_status_name AS status
                      FROM 
                        supplier_pay
                      LEFT JOIN
                        partner
                      ON 
                        supplier_pay.supplier_id = partner.partner_id
                      LEFT JOIN 
                        pay_line
                      ON
                        supplier_pay.pay_id = pay_line.pay_id
                      LEFT JOIN
                        input_order
                      ON
                        pay_line.order_id = input_order.input_order_id
                      LEFT JOIN
                        pay_status
                      ON
                        supplier_pay.pay_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        bank_account
                      ON
                        supplier_pay.pay_type_id = bank_account.account_id " . $condition . "
                      GROUP BY
                        supplier_pay.pay_id
                      ORDER BY
                        supplier_pay.pay_id DESC
                      LIMIT
                        " . $page . "," . $data["per_page_number"];
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            $q = "SELECT 
                    supplier_pay_line.order_id,
                    input_order.input_code,
                    input_order.total_sale
                   FROM
                    supplier_pay_line
                   LEFT JOIN
                    input_order
                   ON
                    supplier_pay_line.order_id = input_order.input_order_id
                   WHERE
                    supplier_pay_line.pay_id = " . $out[$i]["pay_id"] . "
                   AND
                    supplier_pay_line.status != '1'
                   GROUP BY
                    input_order.input_order_id
                   ORDER BY
                    supplier_pay_line.pay_id DESC";
            $this->db->query($q);
            $order = $this->db->getResult();
            $out[$i]["order"] = $order;
//            $t = "SELECT 
//                    SUM(supplier_pay_line.pay_money) AS total_price
//                   FROM
//                    supplier_pay_line
//                   LEFT JOIN
//                    output_order
//                   ON
//                    supplier_pay_line.order_id = output_order.order_id
//                   WHERE
//                    supplier_pay_line.pay_id = " . $out[$i]["pay_id"] . "
//                   GROUP BY
//                    supplier_pay_line.pay_id";
//            $this->db->query($t);
//            $total = $this->db->getResult();
//            $out[$i]["total_price"] = $total[0]["total_price"];
        }
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function getPay($payId) {
        $query = "SELECT
                    supplier_pay.supplier_id,
                    supplier_pay.pay_code, 
                    supplier_pay.pay_id,
                    supplier_pay.pay_type_id,
                    supplier_pay.pay_date,
                    supplier_pay.pay_money,
                    bank_account.account_name,
                    partner.partner_name,
                    partner.partner_address,
                    partner.partner_tax_code,
                    partner.partner_phone_number,
                    partner.partner_fax,
                    supplier_pay.pay_status_id,
                    pay_status.pay_status_name
                  FROM 
                    supplier_pay
                  LEFT JOIN
                    pay_status
                  ON
                    supplier_pay.pay_status_id = pay_status.pay_status_id
                  LEFT JOIN 
                    bank_account
                  ON
                    supplier_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN 
                    partner
                  ON 
                    supplier_pay.supplier_id = partner.partner_id
                  LEFT JOIN
                    pay_type
                  ON
                    supplier_pay.pay_type_id = bank_account.account_id
                  WHERE
                    supplier_pay.pay_id =" . $payId . "
                  AND
                    supplier_pay.status !='1'
                    ";

        $this->db->query($query);
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $this->db->getResult();
        return $data;
    }

    public function getSupplierOrdersUnpaid($supplierId) {
        $query = "SELECT
                    input_order.input_order_id,
                    input_order.input_code,
                    input_order.input_date,
                    input_order.total_sale
                  FROM
                    input_order
                  WHERE
                    input_order.partner_id = '" . $supplierId . "'
                  AND
                    input_order.status_id = 14
                  AND
                    input_order.status = 3
                  GROUP BY
                    input_order.input_order_id";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);

        for ($i = 0; $i < $size; $i++) {

            //A Hoang lam sai.
            /*
            $query2 = "SELECT
                        supplier_pay_line.amount_paid
                       FROM
                        supplier_pay_line
                       WHERE
                        supplier_pay_line.order_id = '" . $out[$i]["input_order_id"] . "'
                       AND
                        supplier_pay_line.status != 1
                       GROUP BY
                        supplier_pay_line.order_id";
            $this->db->query($query2);
            $paid = $this->db->getResult();
            if (isset($paid[0]["amount_paid"])) {
                $out[$i]["amount_paid"] = $paid[0]["amount_paid"];
            } else {
                $out[$i]["amount_paid"] = 0;
            }
            */

            $this->db->selectSum("amount_paid", "sum");
            $this->db->where("order_id", $out[$i]["input_order_id"]);
            $this->db->where("status", 1, "!=");
            $re = $this->db->get("supplier_pay_line");
            if(isset($re[0]['sum'])) {
                $out[$i]["amount_paid"] = $re[0]['sum'];
            } else {
                $out[$i]["amount_paid"] = 0;
            }

            $out[$i]["have_to_pay"] = $out[$i]["total_sale"] - $out[$i]["amount_paid"];
        }

        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    public function getSupplierOrdersUnpaidNot() {
        $query = "SELECT
                    input_order.input_order_id,
                    input_order.input_code,
                    input_order.input_date,
                    input_order.total_sale
                  FROM
                    input_order
                  WHERE
                    input_order.status_id = 14
                  AND
                    input_order.status = 3
                  GROUP BY
                    input_order.input_order_id";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {

            /**
             * A Hoàng làm. Lỗi
             */
            /*
            $query2 = "SELECT
                        supplier_pay_line.amount_paid
                       FROM
                        supplier_pay_line
                       WHERE
                        supplier_pay_line.order_id = '" . $out[$i]["input_order_id"] . "'
                       AND
                        supplier_pay_line.status != 1
                       GROUP BY
                        supplier_pay_line.order_id";
            $this->db->query($query2);
            $paid = $this->db->getResult();
            if (isset($paid[0]["amount_paid"])) {
                $out[$i]["amount_paid"] = $paid[0]["amount_paid"];
            } else {
                $out[$i]["amount_paid"] = 0;
            }
            */

            //Toanpham
            $this->db->selectSum("amount_paid", "sum");
            $this->db->where("order_id", $out[$i]["input_order_id"]);
            $this->db->where("status", 1, "!=");
            $re = $this->db->get("supplier_pay_line");
            if(isset($re[0]['sum'])) {
                $out[$i]["amount_paid"] = $re[0]['sum'];
            } else {
                $out[$i]["amount_paid"] = 0;
            }

            $out[$i]["have_to_pay"] = $out[$i]["total_sale"] - $out[$i]["amount_paid"];
        }
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function deletePay($payId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('pay_id', $payId);
        $this->db->update("supplier_pay");
        $data["status"] = $this->db->update("supplier_pay");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $payId
     * @param unknown $dataUpdate
     */
    public function updatePay($payId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('pay_id', $payId);
        $this->db->update("supplier_pay");

        $data["status"] = $this->db->update("supplier_pay");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    /**
     * 
     * @param unknown $data
     */
    public function newPay($data) {
        $data["create_time"] = time();
//        $query = "INSERT INTO
//                    supplier_pay
//                  VALUES 
//                  (NULL, 
//                  '', 
//                  '" . $data["supplier_id"] . "', 
//                  '" . $data["pay_type_id"] . "', 
//                  '0', 
//                  '" . $data["pay_date"] . "', 
//                  '" . $data["pay_money"] . "',
//                  '" . $data["create_time"] . "',
//                  '',
//                  '0')";
//        $this->db->query($query);
        $data["supplier_id"] = "";
        $data["create_time"] = time();
        $data["status"] = "0";
        $this->db->insert('supplier_pay', $data);

        $query_2 = "SELECT 
                    pay_id
                  FROM
                    supplier_pay
                  ORDER BY
                    pay_id DESC";
        $this->db->query($query_2);
        $pay_id = $this->db->getResult();
        $pay_code = "TT-" . $pay_id[0]["pay_id"];
//        $query_3 = "UPDATE 
//                        supplier_pay
//                    SET 
//                        pay_code = '" . $pay_code . "'
//                    WHERE
//                        pay_id = " . $pay_id[0]["pay_id"];
//        $this->db->query($query_3);
        $update["pay_code"] = $pay_code;
//        echo $update["pay_code"];
//        echo $pay_id[0]["pay_id"];
        $this->db->setItem($update);
        $this->db->where('pay_id', $pay_id[0]["pay_id"]);
        $this->db->update("supplier_pay");
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $pay_id[0]["pay_id"];
        return $output;
    }

    /**
     * 
     * @param unknown $payId
     */
    public function getPayLines($payId) {
        $query = "SELECT 
                    supplier_pay_line.pay_line_id,
                    supplier_pay_line.order_id,
                    supplier_pay_line.have_to_pay,
                    input_order.input_code AS order_code,
                    input_order.input_date AS order_date,
                    input_order.total_sale,
                    supplier_pay_line.amount_paid
                FROM
                    supplier_pay_line
                LEFT JOIN
                    input_order
                ON 
                    supplier_pay_line.order_id = input_order.input_order_id
                WHERE
                    supplier_pay_line.pay_id='" . $payId . "'
                AND
                    supplier_pay_line.status !='1'"
        ;
        //echo $query;
        $this->db->query($query);
        $data = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $data
        );
        return $output;
    }

    /**
     * 
     * @param unknown $data
     */
    public function addNewPayLine($data) {
        $data["create_time"] = time();
        $data["status"] = "0";
        $data["pay_status_id"] = "0";
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "pay_line_id" => $this->db->insert("supplier_pay_line", $data)
        );
//         echo $this->db->getQueryString();
        return $output;
    }

    /**
     * 
     * @param unknown $payLineId
     * @param unknown $data
     */
    public function updatePayLine($payLineId, $data) {
        $data["last_update"] = time();
        $this->db->setItem($data);
        $this->db->where('pay_line_id', $payLineId);
        $output["status"] = $this->db->update("supplier_pay_line", $data);
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    /**
     * 
     * @param unknown $payLineId
     */
    public function deletePayLine($payLineId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('pay_line_id', $payLineId);
        $this->db->update("supplier_pay_line");
        $data["status"] = $this->db->update("supplier_pay_line");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function getImportOrderPaid($orderId) {
        $query2 = "SELECT
                        supplier_pay_line.amount_paid
                       FROM
                        supplier_pay_line
                       WHERE
                        supplier_pay_line.order_id = '" . $orderId . "'
                       GROUP BY
                        supplier_pay_line.order_id";
        $this->db->query($query2);
        $paid = $this->db->getResult();
        if (!isset($paid[0]["amount_paid"])) {
            $paid[0]["amount_paid"] = 0;
        }
        $paid[0]["order_id"] = $orderId;
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = $paid[0];
        return $output;
    }

}

/*end of file MSupplierPay.class.php*/
/*location: MSupplierPay.class.php*/