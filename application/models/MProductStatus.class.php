<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use				BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MProductStatus extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * 
	 * @return NULL
	 */
	public function getProductStatus() {
		$this->httpRequest->setServer( SERVER_API . "index.php&controller=status&action=get_product_status" );
		$res = $this->httpRequest->send();
		$data = json_decode( $res );
		if(isset($data->data))
			return $data->data;
		return null;
	}
}
/*end of file MProductStatus.class.php*/
/*location: MProductStatus.class.php*/