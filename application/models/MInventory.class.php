<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Bootstraps\System;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MInventory extends Model {
	public function __construct() {
		parent::__construct();
		//class HttpRequest was included constructor controller
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
		
		$this->db->connect();
	}
	public function getAdjustedInventories( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory&action=get_adjust_inventories" );
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$return = json_decode( $this->httpRequest->send() );
		if( isset( $return->data ) )
			return $return->data;
		return null;
	}
	public function getAdjustedInventory( $adjustInventoryId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory&action=get_adjust_inventory" );
		$this->httpRequest->setData( array( "data_post" => array(
				"adjust_inventory_id" => $adjustInventoryId
		) ) );
		$return = json_decode( $this->httpRequest->send() );
		if( isset( $return->data[0]) )
			return $return->data[0];
		return null;
	}
	public function newAdjustedInventory( $dataInsert ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory&action=new_adjust_inventory" );
		$this->httpRequest->setData( array( "data_post" => $dataInsert ) );
		$return = json_decode( $this->httpRequest->send() );
		if( isset( $return->data->adjust_inventory_id ) )
			return $return->data->adjust_inventory_id;
		else
			return null;
	}
	public function updateAdjustedInventory( $adjustInventoryId, $dataUpdate ) {
		$dataUpdate[ 'adjust_inventory_id' ] = $adjustInventoryId;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory&action=update_adjust_inventory" );
		$this->httpRequest->setData( array( "data_post" => $dataUpdate ) );
		$return = json_decode( $this->httpRequest->send() );
		if( isset( $return->status ) )
			return $return->status;
		else
			return null;
	}
	public function deleteAdjustedInventory( $adjustInventoryId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory&action=delete_adjust_inventory" );
		$this->httpRequest->setData( array( "data_post" => array(
			"adjust_inventory_id" => $adjustInventoryId
		) ) );
		$return = json_decode( $this->httpRequest->send() );
		return $return->status;
	}
	public function getAdjustedInventoryLines( $adjustInventoryId ) {
// 		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory_line&action=get_adjust_inventory_lines" );
// 		$this->httpRequest->setData( array( "data_post" => array(
// 				"adjust_inventory_id" => $adjustInventoryId
// 		) ) );
// 		$return = json_decode( $this->httpRequest->send() );
// // 		print_r($return);
// 		if( isset( $return->data->item_list ) ) {
// 			return $return->data->item_list;
// 		}
// 		return null;
		$this->db->from("product");
		$this->db->from("unit");
		$this->db->where("adjust_inventory_id", $adjustInventoryId);
		$this->db->where("adjust_inventory_line.product_id = product.product_id");
		$this->db->where("unit.unit_id = product.unit_id");
		$this->db->where("adjust_inventory_line.status", 0);
		
		$data = $this->db->get("adjust_inventory_line");
		return json_decode(json_encode($data));
	}
	public function newAdjustedInventoryLine( $dataInsert ) {
// 		$this->httpRequest->setServer( 
// 			SERVER_API . "index.php?controller=adjust_inventory_line&action=new_adjust_inventory_line" 
// 		);
// 		$this->httpRequest->setData( array( "data_post" => $dataInsert ) );
// 		$return = json_decode( $this->httpRequest->send() );
// 		if( isset( $return->data->adjust_inventory_line_id ) )
// 			return $return->data->adjust_inventory_line_id;
// 		else
// 			return null;
		$this->db->setItem($dataInsert);
		return $this->db->insert("adjust_inventory_line");
	}
	public function updateAdjustedInventoryLine($lineId, $dataUpdate ) {
// 		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory_line&action=update_adjust_inventory_line" );
// 		$this->httpRequest->setData( array( "data_post" => $dataUpdate ) );
// 		$return = json_decode( $this->httpRequest->send() );
// 		if( isset( $return->status ) )
// 			return $return->status;
// 		else
// 			return null;
		$this->db->setItem($dataUpdate);
		$this->db->where("adjust_inventory_line_id", $lineId);
		return $this->db->update("adjust_inventory_line");
	}
	public function deleteAdjustedInventoryLine( $adjustInventoryLineId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=adjust_inventory_line&action=delete_adjust_inventory_line" );
		$this->httpRequest->setData( array( "data_post" => array(
				"adjust_inventory_line_id" => $adjustInventoryLineId
		) ) );
		$return = json_decode( $this->httpRequest->send() );
		return $return->status;
	}
	/**
	 * 
	 * @param unknown $dataInsert
	 * 			- inventory_date
	 * 			- order_id
	 * 			- product_id
	 * 			- amount
	 * 			- warehouse_id
	 * @return NULL
	 */
	public function newInventory($dataInsert) {
//		$this->httpRequest->setServer(
//				SERVER_API . "index.php?controller=inventory&action=new_inventory"
//		);
//		$this->httpRequest->setData( array( "data_post" => $dataInsert ) );
//		$return = json_decode( $this->httpRequest->send() );
//		return $return;

		if(!isset($dataInsert['description'])) $dataInsert['description'] = "";

		//get tồn kho
		$this->db->where("product_id", $dataInsert['product_id']);
		$this->db->where("warehouse_id", $dataInsert['warehouse_id']);
		$this->db->order("inventory_id", "DESC");
		$this->db->select("inventory_amount");
		$data = $this->db->get("inventory");
		if(isset($data[0]['inventory_amount']))
			$openingStock = $data[0]['inventory_amount'];
		else
			$openingStock = 0;
		$inventoryAmount = $openingStock + $dataInsert['amount'];

		//lưu vào cơ sở dữ liệu tổng tồn mới
		$this->db->setItem(array(
			"inventory_date" => $dataInsert['inventory_date'],
			"order_id" => $dataInsert['order_id'],
			"product_id" => $dataInsert['product_id'],
			"opening_stock" => $openingStock,
			"amount" => $dataInsert['amount'],
			"inventory_amount" => $inventoryAmount,
			"status" => 0,
			"description" => $dataInsert['description'],
			"create_time" => time(),
			"warehouse_id" => $dataInsert['warehouse_id']
		));
		return $this->db->insert("inventory");
	}
	/**
	 * Cập nhận tồn kho khi xác nhận điều chỉnh tồn kho
	 * 
	 * @param unknown $dataInsert
	 * @return mixed
	 */
	public function adjustInventory($dataInsert) {
		$this->httpRequest->setServer(
				SERVER_API . "index.php?controller=inventory&action=adjust_inventory"
		);
		$this->httpRequest->setData( array( "data_post" => $dataInsert ) );
		$return = json_decode( $this->httpRequest->send() );
		return $return;
	}
	/**
	 * Cập nhật lại số lượng tồn kho
	 * 
	 * @param unknown $dataInsert
	 */
	public function updateInventoryTotal($dataInsert) {
		$this->db->setItem($dataInsert);
		return $this->db->insert("inventory");
	}
	public function getAdjustInventoryLine($inventoryLineId) {
		$this->db->where("adjust_inventory_line_id", $inventoryLineId);
		return $this->db->get("adjust_inventory_line");
	}
	/**
	 * 
	 * @param unknown $inventoryLineId
	 * @param unknown $dataUpdate
	 */
	public function updateAdjustInventoryLine($inventoryLineId, $dataUpdate) {
		$this->db->where("adjust_inventory_line_id", $inventoryLineId);
		$this->db->setItem($dataUpdate);
		return $this->db->update("adjust_inventory_line");
	}
}
/*end of file MInventory.class.php*/
/*location: MInventory.class.php*/