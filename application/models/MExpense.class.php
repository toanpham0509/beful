<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MExpense extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
    }

    public function getExpenses($data = array()) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE expense.status !='1' AND expense.expense_status_id !='1' AND expense_line.status !='1' ";
        if (isset($data["expense_code"])) {
            $condition = $condition . "AND
            expense.expense_code LIKE '%" . $data["expense_code"] . "%'";
        }
        if (isset($data["partner_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["partner_name"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        if (isset($data["status_id"])) {
            $condition = $condition . "AND
            pay_status.pay_status_id = " . $data['status_id'];
        }
        $query = "SELECT
                        expense.expense_code,
                        expense.expense_id,
                        expense.partner_id,
                        partner.partner_name,
                        expense.receiver_name,
                        expense.expense_date,
                        expense.description,
                        expense.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        pay_status.pay_status_name,
                        SUM(expense_line.money) AS total_price
                      FROM 
                        expense
                      LEFT JOIN
                        partner
                      ON 
                        expense.partner_id = partner.partner_id
                      LEFT JOIN
                        pay_status
                      ON
                        expense.expense_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        expense_line
                      ON
                        expense.expense_id = expense_line.expense_id
                      LEFT JOIN
                        bank_account
                      ON
                        expense.pay_type_id = bank_account.account_id " . $condition . "
                      GROUP BY
                        expense.expense_id,expense_line.expense_id
                      ORDER BY
                        expense.expense_id DESC
                      LIMIT
                        " . $page . "," . $data["per_page_number"];
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function getExpense($expenseId) {
       
        $query = "SELECT
                        expense.expense_id,
                        expense.expense_code,
                        expense.pay_type_id,
                        expense.expense_date,
                        expense.partner_id,
                        partner.partner_name,
                        partner.partner_address,
                        expense.receiver_name,
                        expense.description,
                        expense.expense_status_id,
                        pay_status.pay_status_name
                      FROM 
                        expense 
                      LEFT JOIN
                        pay_status
                      ON
                        expense.expense_status_id = pay_status.pay_status_id
                      LEFT JOIN
                        partner
                      ON 
                        expense.partner_id = partner.partner_id
                      WHERE
                        expense.expense_id = ".$expenseId."
                      AND
                        expense.status != 1";
                                      
//        $select = array(
//            "expense.expense_id",
//            "expense.expense_code",
//            "expense.pay_type_id",
//            "expense.expense_date",
//            "expense.receiver_name",
//            "expense.partner_id",
//            "partner.partner_name",
//            "partner.partner_address",
//            "expense.description"
//        );
//        $this->db->select($select);
//        $this->db->from("expense");
//        $this->db->join("partner","expense.partner_id = partner.partner_id", "LEFT");
//        $this->db->where("expense.expense_id", $expenseId);
//        $this->db->where("expense.status", '1', "!=");
        $this->db->query($query);
        $out = $this->db->getResult();
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $out;
        return $data;
    }

    public function addNew($dataUpdate) {
        $dataUpdate["create_time"] = time();
        $dataUpdate["expense_id"] = null;
        if(isset($dataUpdate['expense_status_id'])) {
        } else {
            $dataUpdate["expense_status_id"] = '1';
        }
        $dataUpdate["status"] = '0';
        $this->db->insert('expense', $dataUpdate);

//        $query = "INSERT INTO
//                    expense
//                  VALUES 
//                  (NULL, 
//                  '', 
//                  '" . $dataUpdate["partner_id"] . "', 
//                  '" . $dataUpdate["expense_date"] . "',
//                  '" . $dataUpdate["receiver_name"] . "',
//                  '" . $dataUpdate["pay_type_id"] . "',
//                  '0', 
//                  '" . $dataUpdate["description"] . "', 
//                  '" . $dataUpdate["create_time"] . "',
//                  ''
//                  '0')
//                 ";
////        echo $query;
//        $this->db->query($query);

        $query_2 = "SELECT 
                    expense_id
                  FROM
                    expense
                  ORDER BY
                    expense_id
                  DESC";
        $this->db->query($query_2);
        $expense_id = $this->db->getResult();
        $expense_code = "PC-" . $expense_id[0]["expense_id"];

//        $query_3 = "UPDATE 
//                        expense
//                    SET 
//                        expense_code = '" . $expense_code . "'
//                    WHERE
//                        expense_id = " . $expense_id[0]["expense_id"];
//        $this->db->query($query_3);

        $update["expense_code"] = $expense_code;
        $this->db->setItem($update);
        $this->db->where('expense_id', $expense_id[0]["expense_id"]);
        $this->db->update("expense");

        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "expense_id" => $expense_id[0]["expense_id"],
            "expense_code" => $expense_code
        );
        return $output;
    }

    public function update($expenseId, $dataUpdate) {
        $dataUpdate["last_update"] = time();
        $this->db->setItem($dataUpdate);
        $this->db->where('expense_id', $expenseId);
        $this->db->update("expense");

        $data["status"] = $this->db->update("expense");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function delete($expenseId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('expense_id', $expenseId);
        $this->db->update("expense");
        $data["status"] = $this->db->update("expense");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

    public function getLines($expenseId) {
        $query = "SELECT
                        expense_line.expense_line_id,
                        expense_line.partner_id,
                        partner.partner_name,
                        category.category_name,
                        expense_line.money,
                        expense_line.description
                      FROM 
                        expense_line
                      LEFT JOIN
                        partner
                      ON 
                        expense_line.partner_id = partner.partner_id
                      LEFT JOIN
                        partner_category 
                      ON
                        partner_category.partner_id = partner.partner_id
                      LEFT JOIN
                        category
                      ON
                        category.category_id = partner_category.category_id
                      WHERE
                        expense_line.expense_id = " . $expenseId."
                      AND
                        expense_line.status != 1";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function addNewLine($data) {
        $data["create_time"] = time();
        $data["status"] = "0";
        $output["status"] = 0;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "expense_line_id" => $this->db->insert("expense_line", $data)
        );
        return $output;
    }

    public function updateLine($lineId, $data) {
        $data["last_update"] = time();
        $this->db->setItem($data);
        $this->db->where('expense_line_id', $lineId);
        $output["status"] = $this->db->update("expense_line");
        $output["erros"] = NULL;
        $output["message"] = NULL;
        return $output;
    }

    public function deleteLine($lineId) {
        $this->db->setItem(array(
            "status" => "1"
        ));
        $this->db->where('expense_line_id', $lineId);
        $data["status"] = $this->db->update("expense_line");
        $data["erros"] = NULL;
        $data["message"] = NULL;
        return $data;
    }

}

/*end of file MReceipt.class.php*/
/*location: MReceipt.class.php*/