<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use					BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MShopManager extends Model {
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	public function getAllShopManagers() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_user_work_list" );
		$this->httpRequest->setData( array( "data_post" => array( "position_id" => 4 ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return ( $data == null ) ? null : $data->data->item_list;
	}
	public function newShopManager( $userID, $workplaceID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=new_user_work" );
		$this->httpRequest->setData( array( "data_post" => array(
			"user_id"			=>			$userID,
			"position_id"		=>			4,
			"workplace_id"		=> 			$workplaceID
		) ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		return $data->status;
	}
	public function getShopManager( $shopID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_user_work_list" );
		$this->httpRequest->setData( array( "data_post" => array( 
				"position_id" => 4,
				"workplace_id" => $shopID
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) return $data->data->item_list;
		return null;
	}
	public function updateShopManager( $recoreID, $userID, $workplaceID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=update_user_work" );
		$this->httpRequest->setData( array( "data_post" => array(
				"employee_work_id"	=> 			$recoreID,
				"user_id"			=>			$userID,
				"position_id"		=>			4,
				"workplace_id"		=> 			$workplaceID
		) ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		return $data->status;
	}
}
/*end of file MShopManager.class.php*/
/*location: MShopManager.class.php*/