<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
use 				BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MProduct extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
		
		$this->db->connect();
	}
	/**
	 * 
	 * @param string $dataSearch
	 */
	public function getProducts( $dataSearch = null ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product&action=get_products" );
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		//input: inventory_date
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}

	public function getAllProduct() {
		$this->db->select("product_id, product_code, product_name");
		$this->db->limit(10000);
		$products = $this->db->get("product");
		return json_decode(json_encode($products));
	}

	public function getAllProduct_old() {
		
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product&action=get_all_product" );
		
		$data = json_decode( $this->httpRequest->send() );
		print_r($data);
		return $data->data->item_list;
	}

	/**
	 * @param null $dataSearch
	 * @return mixed
	 */
	public function getListProducts( $dataSearch = null ) {
		
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=product&action=get_list_products" );
		
		
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		
		//input: inventory_date
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * Get product by product's id
	 * 
	 * @param unknown $productID
	 */
	public function getProduct( $productID ) {
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product&action=get_product" );
		$this->httpRequest->setData( array( "data_post" => array(
			"product_id" => $productID
		) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	/**
	 * 
	 * @param unknown $data
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product&action=new_product" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->new_product ) )
			return $data->data->new_product;
		return 0;
	}
	/**
	 * 
	 * @param unknown $productID
	 * @param unknown $data
	 */
	public function update( $productID, $data ) {
		$data[ 'product_id' ] = $productID;
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product&action=update_product" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function delete( $productID ) {
		$data = array();
		$data[ 'product_id' ] = $productID;
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product&action=delete_product" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Get product's price
	 * 
	 * @param unknown $productID
	 * @param string $priceID
	 */
	public function getProductPrice( $productID, $priceID = null ) {
		$this->httpRequest->setServer( 
				SERVER_API . "index.php?controller=product_price&action=get_product_prices" );
		$this->httpRequest->setData( 
			array( "data_post" => array( 
				"product_id" => $productID, 
				"price_id" => $priceID
			) ) 
		);
		
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data[0]->product_price ) )
			return $data->data[0]->product_price;
		return 0;
	}
	/**
	 * Hàm này dùng để tạo 1 record mới trong db. 
	 * Để xem sản phẩm ... thuộc chuyên mục nào...
	 */
	public function newProductCategory( $productID, $categoryID ) {
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product_category&action=new_product_category" );
		$data = array(
			"product_id" => $productID,
			"category_id" => $categoryID
		);
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 * @param unknown $productID
	 */
	public function getProductsCategory( $productID ) {
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product_category&action=get_product_categories" );
		$data = array(
				"product_id" => $productID,
		);
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data->item_list;
	}
	/**
	 * 
	 * @param unknown $productCateoryID
	 */
	public function deleteProductCategory( $productCateoryID ) {
		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=product_category&action=delete_product_category" );
		$data = array(
				"product_category_id" => $productCateoryID,
		);
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Get product price
	 * 
	 * API này làm sai phần giá sản phẩm. Phải code thêm để cho chuẩn.
	 * 
	 * @param unknown $productId
	 * @param unknown $warehouseId
	 * @return mixed
	 */
	public function getProductPriceInWarehouse($productId, $warehouseId) {
//		$this->httpRequest->setServer(  SERVER_API . "index.php?controller=input_lines&action=get_product_price_import" );
//		$data = array(
//				"warehouse_id" => $warehouseId,
//				"product_id" => $productId
//		);
//		$this->httpRequest->setData( array( "data_post" => $data ) );
//		$return = json_decode( $this->httpRequest->send() );
		
// 		print_r($return->data->item_list[0]);

		$return = array();

		//Tồn kho
		$this->db->where("product_id", $productId);
		$this->db->where("warehouse_id", $warehouseId);
		$this->db->order("inventory_id", "DESC");
		$this->db->select("inventory_amount");
		$data = $this->db->get("inventory");
		if(isset($data[0]['inventory_amount']))
			$openingStock = $data[0]['inventory_amount'];
		else
			$openingStock = 0;

		$this->db->select("product_price");
		$this->db->from("input_lines as line");
		$this->db->from("input_order");
		$this->db->where("product_id", $productId);
		$this->db->where("input_line_amount > buy_amount");
		$this->db->where("input_order.input_order_id = line.input_order_id");
		$this->db->limit(1);
		$this->db->order("input_order.last_update", null);
		$data = $this->db->get();
		if(isset($data[0]["product_price"])) {
			$productPrice = $data[0]['product_price'];
		} else {
			$productPrice = 0;
		}
		$return = array();
		$return['data']['item_list'][0] = array(
			"product_price" => $productPrice,
			"inventory_amount" => $openingStock
		);
		return json_decode(json_encode($return));
	}
}
/*end of file MProduct.class.php*/
/*location: MProduct.class.php*/