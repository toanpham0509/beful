<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MSalesPeople extends Model {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		//class HttpRequest was included constructor controller
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * 
	 * Get sale people
	 * 
	 * @return mixed
	 */
	public function getSalesPeople() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=employee_work&action=get_salespeople" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
}
/*end of file MSalesPeople.php*/
/*location: MSalesPeople.php*/