<?php
namespace 			App\Models;
use BKFW\Bootstraps\Model;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MSession extends Model {
	public function __construct() {
		parent::__construct();
	}
	public function isLogin() {
		
	}
	public function getUserID() {
		
	}
	public function getUserLevel() {
		
	}
}
/*end of file MSession.class.php*/
/*location: MSession.class.php*/