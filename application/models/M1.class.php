<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class M1 extends Model {
	public function __construct() {
		parent::__construct();
	}
	public function f1() {
		return "Hello. I am method f1";
	}
}
/*end of file M1.php*/
/*location: M1.php*/