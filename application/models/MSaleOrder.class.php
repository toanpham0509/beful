<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MSaleOrder extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		//class HttpRequest was included constructor controller
		//$this->loadLibrary( "HttpRequest" );
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );

		$this->loadModel("MRefund");

		$this->db->connect();
	}
	/**
	 * 
	 * @param unknown $dataSearch
	 * @return mixed
	 */
	public function getOrders( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=get_orders" );
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		return json_decode( $this->httpRequest->send() );
	}
	/**
	 * Get order
	 * 
	 * @param unknown $orderID
	 * @return mixed
	 */
	public function getOrder( $orderID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=get_order" );
		$this->httpRequest->setData( array( "data_post" => array( "order_id" => $orderID ) ) );
		$data = json_decode( $this->httpRequest->send() );
// 		print_r($data);
		return $data->data[0];
	}
	/**
	 * Update order 
	 * 
	 * @param unknown $orderID
	 * @param unknown $dataUpdate
	 */
	public function update( $orderID, $dataUpdate ) {
		$dataUpdate[ 'order_id' ] = $orderID;
		 
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=update_order" );
		$orderlines = $dataUpdate[ 'order_lines' ];
		
		unset( $dataUpdate[ 'order_lines' ] );
		
		$this->httpRequest->setData( array( "data_post" => $dataUpdate, "order_lines" => $orderlines ) );
		$data = $this->httpRequest->send();
		$data = json_decode( $data );
		
		if( isset( $data->status ) )
			return $data->status;
		else 
			return 0;
	}
	/**
	 * 
	 * @param unknown $data
	 * @return number
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=new_order" );
		$orderlines = $data[ 'order_lines' ];
		
		unset( $data[ 'order_lines' ] );
		
		$this->httpRequest->setData( array( "data_post" => $data, "order_lines" => $orderlines ) );
		$data = json_decode( $this->httpRequest->send() );
		
		if( $data->status == 1 ) return $data->data->order_id;
		return 0;
	}
	/**
	 * 
	 */
	public function reportSale() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=report_sale" );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}

	/**
	 * @return null
	 */
	public function reportSaleLine() {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=partner_category&action=report_sale_line" );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) return $data->data->item_list;
		return null;
	}

	/**
	 * Delete order 
	 * 
	 * @param unknown $orderID
	 * @return mixed
	 */
	public function delete( $orderID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=delete_order" );
		$this->httpRequest->setData( array( "data_post" => array( "order_id" => $orderID ) ) );
		$data = json_decode( $this->httpRequest->send() ); 
		return $data->status;
	}

	/**
	 * @param $orderLineID
	 * @return mixed
	 */
	public function deleteOrderLine( $orderLineID ){
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=output_lines&action=delete_line" );
		$this->httpRequest->setData( array( "data_post" => array( "output_line_id" => $orderLineID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}

	/**
	 * @param $orderId
	 * @return mixed
	 */
	public function inspectionOrder( $orderId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=order&action=inspection_order" );
		$this->httpRequest->setData( array( "data_post" => array( "order_id" => $orderId ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
/**
	 * Update order line
	 * 
	 * @param unknown $orderId
	 * @param unknown $key
	 * @param unknown $value
	 */
	public function updateOrderLine($orderLineId, $dataUpdate) {
		$this->db->where("output_line_id", $orderLineId);
		$this->db->setItem($dataUpdate);
		$this->db->update("output_lines");
		echo $this->db->getQueryString();
		return 1;
	}
	public function updateOrderLineInputLineId($orderLineId, $data) {
		$query = "UPDATE output_lines SET input_lines = \"" . mysql_escape_string(($data)) ."\" WHERE output_line_id = " . $orderLineId;
// 		echo $query;
		return $this->db->query($query);
	}
	/**
	 * Update order
	 * 
	 * @param unknown $orderId
	 * @param unknown $dataUpdate
	 */
	public function updateOrder($orderId, $dataUpdate) {
		$this->db->where("order_id", $orderId);
		$this->db->setItem($dataUpdate);
		return $this->db->update("output_order");
	}

	/**
	 * @param $orderId
	 * @param $productID
	 * @return mixed
	 */
	public function getOrderLineByProductOrderId($orderId, $productID) {
		$this->db->where("order_id", $orderId);
		$this->db->where("product_id", $productID);
		return $this->db->get("output_lines");
	}

	/**
	 * @param $orderId
	 */
	public function getOrderTotalSale($orderId) {
		$orderLines = array();

		//Kêt nối tới phần của a Huy, TỐc độ quá chậm
//		$orderLines = $this->getOrder($orderId);
//		if(isset($orderLines->order_lines))
//			$orderLines = $orderLines->order_lines;
//
//		print_r($orderLines);
		$this->db->select("output_line_id as order_line_id, product_discount, output_line_amount as product_amount, product_price");
		$this->db->where(array(
				"order_id" => $orderId,
				"status" => 0
		));
		$orderLines = json_decode(json_encode($this->db->get("output_lines")));

		if(empty($orderLines)) {
			return 0;
		} else {
			$totalPrice = 0;
			foreach( $orderLines as $orderLine ) {
//				print_r($orderLine);
				$refundAmount = $this->mRefund->getCountProductRefundByOutputLineId(
						$orderLine->order_line_id
				);
				$totalPrice += ((100 - $orderLine->product_discount)/100) * (
								$orderLine->product_amount - $refundAmount
						) * $orderLine->product_price;
			}
			return $totalPrice;
		}
	}

	/**
	 * @param $orderId
	 * @return mixed
	 */
	public function getOrderUnpaid($orderId) {
		$data["status"] = 1;
		$data["errors"] = NULL;
		$data["message"] = NULL;
		$data["data"] = null;

		$query = "SELECT
                    output_order.order_id,
                    output_order.order_code,
                    output_order.order_date,
                    output_order.order_price,
                    output_order.discount
                  FROM
                    output_order
                  WHERE
                    output_order.order_id = '" . $orderId . "'
                  AND
                    output_order.status = 3
                  AND
                    output_order.status != 1
                  GROUP BY
                    output_order.order_id";

		$this->db->query($query);
		$out = $this->db->getResult();

		if(empty($out)) {
			return $data;
		}

		$this->db->selectSum("amount_paid", "sum");
		$this->db->where(array(
				"order_id" => $orderId
		));
		$this->db->where("status", "1", "!=");
		$re = $this->db->get("pay_line");
		if(isset($re[0]['sum']))
			$re = $re[0]['sum'];
		else
			$re = 0;

		$out[0]["amount_paid"] = $re;
		$out[0]["order_price"] = $this->getOrderTotalSale($orderId);
		$out[0]["have_to_pay"] = ($out[0]["order_price"] - ($out[0]["order_price"] * $out[0]["discount"] / 100))
				- $out[0]["amount_paid"];

		$data['data'] = $out;

		return $data;
	}
}
/*end of file MSaleOrder.class.php*/
/*location: MSaleOrder.class.php*/