<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MBuyOrder extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
	}
	/**
	 * Get orders
	 * 
	 * @param unknown $dataSearch
	 */
	public function getOrders( $dataSearch = array() ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=get_buy_orders" );
		$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * Get order
	 * 
	 * @param unknown $orderID
	 */
	public function getOrder( $orderID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=get_buy_order" );
		$this->httpRequest->setData( array( "data_post" => array( "order_id" => $orderID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		
		if( empty( $data->data ) ) return null;
		return $data->data[ 0 ];
	}
	/**
	 * Update
	 * 
	 * @param unknown $orderID
	 * @param unknown $dataUpdate
	 */
	public function update( $orderID, $dataUpdate ) {
		$dataUpdate[ 'order_id' ] = $orderID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=update_buy_order" );
		$this->httpRequest->setData( array( "data_post" => $dataUpdate ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Add new
	 * 
	 * @param unknown $data
	 */
	public function addNew( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=new_buy_order" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		
		if( $data->status == 1 ) {
			return $data->data->order_id;
		} 
		return 0;
	}
	/**
	 * Delete
	 * 
	 * @param unknown $orderID
	 */
	public function delete( $orderID ) {
		$data[ 'order_id' ] = $orderID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=delete_buy_order" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Report sale
	 * 
	 */
	public function reportSale() {
		
	}
	/**
	 * Get order lines
	 * 
	 * @param unknown $orderID
	 * @return NULL
	 */
	public function getOrderLines( $orderID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=get_buy_lines" );
		$this->httpRequest->setData( array( "data_post" => array( "input_order_id" => $orderID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) ) return $data->data->item_list;
		return null;
	}
	/**
	 * Add new order line
	 * 
	 * @param unknown $orderID
	 * @param unknown $data
	 */
	public function addNewOrderLine( $orderID, $data ) {
		$data[ 'order_id' ] = $orderID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=new_buy_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Update order line
	 * 
	 * @param unknown $orderID
	 * @param unknown $data
	 */
	public function updateOrderLine( $orderID, $data ) {
		$data[ 'order_id' ] = $orderID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=update_buy_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * Delete order line
	 * 
	 * @param unknown $orderLineID
	 */
	public function deleteOrderLine( $orderLineID ) {
		$data[ 'input_line_id' ] = $orderLineID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=delete_buy_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	public function inspectionOrder( $orderId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=inspection_order" );
		$this->httpRequest->setData( array( "data_post" => array( "input_order_id" => $orderId ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
}
/*end of file MBuyOrder.class.php*/
/*location: MBuyOrder.class.php*/