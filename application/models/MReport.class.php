<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MReport extends Model
{

    public function __construct()
    {
        parent::__construct();
        $this->httpRequest = new HttpRequest();
        $this->httpRequest->setMethod("POST");

        $this->loadModel("MSaleOrder");
        $this->loadModel("MRefund");


        $this->mRefund = new MRefund();

        $this->mSaleOrder = new MSaleOrder();

        $this->db->connect();
    }

    public function statisticsSaleTopProduct($data)
    {
        $query = "SELECT
                    output_lines.product_id,
                    product.product_name
                FROM
                    output_lines
                LEFT JOIN
                    product
                ON
                    output_lines.product_id = product.product_id
                LEFT JOIN
                    output_order
                ON
                    output_lines.order_id = output_order.order_id
                WHERE
                    output_lines.status != '1'
                AND
                    output_order.status_id = '3'
                AND
                    output_lines.create_time >=" . $data["date_from"] . "
                AND
                    output_lines.create_time <=" . $data["date_to"] . "
                GROUP BY
                    output_lines.product_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        foreach ($out as $key1 => $product) {
            foreach ($product as $product_id => $value) {
                if ($product_id == "product_id") {
                    $query_2 = "SELECT
                                    output_lines.output_line_id,
                                    output_lines.output_line_amount AS amount,
                                    output_lines.product_price,
                                    output_order.discount
                                FROM
                                    output_lines
                                LEFT JOIN
                                    product
                                ON
                                    output_lines.product_id = product.product_id
                                LEFT JOIN
                                    output_order
                                ON
                                    output_lines.order_id = output_order.order_id
                                WHERE
                                    output_lines.status != '1'
                                AND
                                    output_lines.product_id = " . $value . "
                                AND
                                    output_order.status_id = '3'
                                AND
                                    output_lines.product_id = " . $product['product_id'] . "
                                AND
                                    output_lines.create_time >=" . $data["date_from"] . "
                                AND
                                    output_lines.create_time <=" . $data["date_to"];
                    $this->db->query($query_2);
                    $total_price = $this->db->getResult();
                    $total = 0;
                    $size_total_price = sizeof($total_price);
                    $total_price['total_price'] = 0;
                    for ($i = 0; $i < $size_total_price; $i++) {
                        $refund = $this->mRefund->getCountProductRefundByOutputLineId($total_price[$i]['output_line_id']);
                        $amount = $total_price[$i]['amount'] - $refund;
                        $total = $total + (($total_price[$i]['product_price'] * $amount) -
                                (($total_price[$i]['product_price'] * $amount) * ($total_price[$i]['discount'] / 100)));
                        $total_price['total_price'] = $total;
                    }
                    $out[$key1]['total_price'] = $total_price['total_price'];
                }
            }
        }

        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsSaleTopProductLines($data)
    {
        $query = "SELECT
                    product.product_id,
                    product.product_name,
                    SUM(output_lines.product_price) AS total_price
                FROM
                    output_lines
                LEFT JOIN
                    product
                ON
                    output_lines.product_id = product.product_id
                WHERE
                    output_lines.status !='1'
                AND
                    output_lines.create_time >=" . $data["date_from"] . "
                AND
                    output_lines.create_time <=" . $data["date_to"] . "
                GROUP BY
                    output_lines.product_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $result = "";
        $size = sizeof($out);
        $query_2 = array();
        for ($i = 0; $i < $size; $i++) {
            $query_2[] = "SELECT
                            DATE_FORMAT(FROM_UNIXTIME(output_lines.create_time), '%c') AS month,
                            DATE_FORMAT(FROM_UNIXTIME(output_lines.create_time), '%Y') AS year,
                            SUM(output_lines.product_price) AS total_price
                        FROM
                            output_lines
                        LEFT JOIN
                            product
                        ON
                            output_lines.product_id = product.product_id
                        WHERE
                            output_lines.status !='1'
                        AND
                            output_lines.product_id = " . $out[$i]["product_id"] . "
                        AND
                            output_lines.create_time >=" . $data["date_from"] . "
                        AND
                            output_lines.create_time <=" . $data["date_to"] . "
                        GROUP BY
                            DATE_FORMAT(FROM_UNIXTIME(output_lines.create_time), '%m'),
                            DATE_FORMAT(FROM_UNIXTIME(output_lines.create_time), '%Y')";
            $result[$i] = $out[$i];
        }
        $size_2 = sizeof($query_2);
        for ($i = 0; $i < $size_2; $i++) {
            $this->db->query($query_2[$i]);
            $q = $this->db->getResult();
            $s = sizeof($q);
            for ($j = 0; $j < $s; $j++) {
                $arr[$j] = array(
                    $q[$j]["month"] => $q[$j]["total_price"]
                );
            }
            $ds[$i] = $arr;
        }
        for ($i = 0; $i < $size; $i++) {
            for ($j = 1; $j < 13; $j++) {
                $result[$i]["doanhthu"][$j] = "";
            }
        }
        for ($i = 0; $i < $size; $i++) {
            $c = sizeof($ds[$i]);
            for ($j = 0; $j < $c; $j++) {
                if (isset($ds[$i][$j][1])) {
                    $result[$i]["doanhthu"][1] = $ds[$i][$j][1];
                }
                if (isset($ds[$i][$j][2])) {
                    $result[$i]["doanhthu"][2] = $ds[$i][$j][2];
                }
                if (isset($ds[$i][$j][3])) {
                    $result[$i]["doanhthu"][3] = $ds[$i][$j][3];
                }
                if (isset($ds[$i][$j][4])) {
                    $result[$i]["doanhthu"][4] = $ds[$i][$j][4];
                }
                if (isset($ds[$i][$j][5])) {
                    $result[$i]["doanhthu"][5] = $ds[$i][$j][5];
                }
                if (isset($ds[$i][$j][6])) {
                    $result[$i]["doanhthu"][6] = $ds[$i][$j][6];
                }
                if (isset($ds[$i][$j][7])) {
                    $result[$i]["doanhthu"][7] = $ds[$i][$j][7];
                }
                if (isset($ds[$i][$j][8])) {
                    $result[$i]["doanhthu"][8] = $ds[$i][$j][8];
                }
                if (isset($ds[$i][$j][9])) {
                    $result[$i]["doanhthu"][9] = $ds[$i][$j][9];
                }
                if (isset($ds[$i][$j][10])) {
                    $result[$i]["doanhthu"][10] = $ds[$i][$j][10];
                }
                if (isset($ds[$i][$j][11])) {
                    $result[$i]["doanhthu"][11] = $ds[$i][$j][11];
                }
                if (isset($ds[$i][$j][12])) {
                    $result[$i]["doanhthu"][12] = $ds[$i][$j][12];
                }
            }
        }
//         for ($i = 0; $i < $size; $i++) {
//             $result[$i]["doanhthu"] = json_encode($result[$i]["doanhthu"]);
//         }

        $count = sizeof($result);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $result
        );
        return $output;
    }

    public function statisticsSaleTopCustomer($data)
    {
        $query = "SELECT
                    output_order.partner_id,
                    partner.partner_name
                FROM
                    output_order
                LEFT JOIN
                    partner
                ON
                    output_order.partner_id = partner.partner_id
                WHERE
                    output_order.status ='3'
                AND
                    output_order.status_id = '3'
                AND
                    output_order.order_date >=" . $data["date_from"] . "
                AND
                    output_order.order_date <=" . $data["date_to"] . "
                GROUP BY
                    output_order.partner_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();

        foreach ($out as $key1 => $partner) {
            foreach ($partner as $partner_id => $value) {
                if ($partner_id == "partner_id") {
                    $query_2 = "SELECT
                                    output_order.order_id,
                                    output_order.discount,
                                    output_order.order_price
                                FROM
                                    output_order
                                WHERE
                                    output_order.status ='3'
                                AND
                                    output_order.partner_id = " . $value . "
                                AND
                                    output_order.status_id = '3'
                                AND
                                    output_order.order_date >=" . $data["date_from"] . "
                                AND
                                    output_order.order_date <=" . $data["date_to"];
//                    echo $query_2;
                    $this->db->query($query_2);
                    $total_price = $this->db->getResult();
                    $total = 0;
                    $size_total_price = sizeof($total_price);
                    $total_price['total_price'] = 0;
                    for ($i = 0; $i < $size_total_price; $i++) {
                        $refund_price = $this->mSaleOrder->getOrderTotalSale($total_price[$i]['order_id']);
                        $out[$key1]['refund'] = $refund_price;
                        if ($refund_price == 0) {
                            $total = $total + ($total_price[$i]['order_price'] - ($total_price[$i]['order_price'] * ($total_price[$i]['discount'] / 100)));
                        } else {
                            $total = $total + ($refund_price - ($refund_price * ($total_price[$i]['discount'] / 100)));
                        }
                        $total_price['total_price'] = $total;
                    }
                    $out[$key1]['total_price'] = $total_price['total_price'];
                }
            }
        }
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsSaleTopCustomerLines($data)
    {
        $query = "SELECT
                    output_order.partner_id,
                    partner.partner_name,
                    SUM(output_order.order_price) AS total_price
                FROM
                    output_order
                LEFT JOIN
                    partner
                ON
                    output_order.partner_id = partner.partner_id
                WHERE
                    output_order.status !='1'
                AND
                    output_order.partner_id != ''
                AND
                    output_order.order_date >=" . $data["date_from"] . "
                AND
                    output_order.order_date <=" . $data["date_to"] . "
                GROUP BY
                    output_order.partner_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            if ($out[$i]["partner_id"] != null) {
                $query_2[] = "SELECT
                            output_order.partner_id,
                            partner.partner_name,
                            DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%c') AS month,
                            DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%Y') AS year,
                            SUM(output_order.order_price) AS total_price
                        FROM
                            output_order
                        LEFT JOIN
                            partner
                        ON
                            output_order.partner_id = partner.partner_id
                        WHERE
                                output_order.status !='1'
                        AND
                            output_order.partner_id = " . $out[$i]["partner_id"] . "
                        AND
                            output_order.order_date >=" . $data["date_from"] . "
                        AND
                            output_order.order_date <=" . $data["date_to"] . "
                        GROUP BY
                            DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%m'),
                            DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%Y')";
            }
        }
//        echo $query_2;
        $size_2 = sizeof($query_2);
        for ($i = 0; $i < $size_2; $i++) {
            $this->db->query($query_2[$i]);
            $q = $this->db->getResult();
            $s = sizeof($q);
            for ($j = 0; $j < $s; $j++) {
                $arr[$j] = array(
                    $q[$j]["month"] => $q[$j]["total_price"]
                );
            }
            $ds[$i] = $arr;
        }
        for ($i = 0; $i < $size; $i++) {
            for ($j = 1; $j < 13; $j++) {
                $out[$i]["doanhthu"][$j] = "";
            }
        }
        for ($i = 0; $i < $size; $i++) {
            $c = sizeof($ds[$i]);
            for ($j = 0; $j < $c; $j++) {
                if (isset($ds[$i][$j][1])) {
                    $out[$i]["doanhthu"][1] = $ds[$i][$j][1];
                }
                if (isset($ds[$i][$j][2])) {
                    $out[$i]["doanhthu"][2] = $ds[$i][$j][2];
                }
                if (isset($ds[$i][$j][3])) {
                    $out[$i]["doanhthu"][3] = $ds[$i][$j][3];
                }
                if (isset($ds[$i][$j][4])) {
                    $out[$i]["doanhthu"][4] = $ds[$i][$j][4];
                }
                if (isset($ds[$i][$j][5])) {
                    $out[$i]["doanhthu"][5] = $ds[$i][$j][5];
                }
                if (isset($ds[$i][$j][6])) {
                    $out[$i]["doanhthu"][6] = $ds[$i][$j][6];
                }
                if (isset($ds[$i][$j][7])) {
                    $out[$i]["doanhthu"][7] = $ds[$i][$j][7];
                }
                if (isset($ds[$i][$j][8])) {
                    $out[$i]["doanhthu"][8] = $ds[$i][$j][8];
                }
                if (isset($ds[$i][$j][9])) {
                    $out[$i]["doanhthu"][9] = $ds[$i][$j][9];
                }
                if (isset($ds[$i][$j][10])) {
                    $out[$i]["doanhthu"][10] = $ds[$i][$j][10];
                }
                if (isset($ds[$i][$j][11])) {
                    $out[$i]["doanhthu"][11] = $ds[$i][$j][11];
                }
                if (isset($ds[$i][$j][12])) {
                    $out[$i]["doanhthu"][12] = $ds[$i][$j][12];
                }
            }
        }
//         for ($i = 0; $i < $size; $i++) {
//             $out[$i]["doanhthu"] = json_encode($out[$i]["doanhthu"]);
//         }

        $count = sizeof($out);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsTopSalePerson($data)
    {
        $query = "SELECT
                    output_order.salesperson_id,
                    user.user_name
                FROM
                    output_order
                LEFT JOIN
                    user
                ON
                    output_order.salesperson_id = user.user_id
                WHERE
                    output_order.status ='3'
                AND
                    output_order.status_id = '3'
                AND
                    output_order.order_date >=" . $data["date_from"] . "
                AND
                    output_order.order_date <=" . $data["date_to"] . "
                GROUP BY
                    output_order.salesperson_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        if ($count != 0) {
            foreach ($out as $key1 => $saleperson) {
                foreach ($saleperson as $saleperson_id => $value) {
                    if ($saleperson_id == "salesperson_id" && $value != '') {
                        $query_2 = "SELECT
                                    output_order.order_id,
                                    output_order.discount,
                                    output_order.order_price
                                FROM
                                    output_order
                                WHERE
                                    output_order.status ='3'
                                AND
                                    output_order.salesperson_id = " . $value . "
                                AND
                                    output_order.status_id = '3'
                                AND
                                    output_order.order_date >=" . $data["date_from"] . "
                                AND
                                    output_order.order_date <=" . $data["date_to"];
//                    echo $query_2;
                        $this->db->query($query_2);
                        $total_price = $this->db->getResult();
                        $total = 0;
                        $size_total_price = sizeof($total_price);
                        $total_price['total_price'] = 0;
                        for ($i = 0; $i < $size_total_price; $i++) {
                            $refund_price = $this->mSaleOrder->getOrderTotalSale($total_price[$i]['order_id']);
                            $out[$key1]['refund'] = $refund_price;
                            if ($refund_price == 0) {
                                $total = $total + ($total_price[$i]['order_price'] - ($total_price[$i]['order_price'] * ($total_price[$i]['discount'] / 100)));
                            } else {
                                $total = $total + ($refund_price - ($refund_price * ($total_price[$i]['discount'] / 100)));
                            }
                            $total_price['total_price'] = $total;
                        }
                        $out[$key1]['total_price'] = $total_price['total_price'];
                    }
                }
            }
        }
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsTopSalePersonLines($data)
    {
        $query = "SELECT
                    output_order.salesperson_id,
                    user.user_name,
                    SUM(output_order.order_price) AS total_price
                FROM
                    output_order
                LEFT JOIN
                    user
                ON
                    output_order.salesperson_id = user.user_id
                WHERE
                    output_order.status !='1'
                AND
                    output_order.salesperson_id !=''
                AND
                    output_order.order_date >=" . $data["date_from"] . "
                AND
                    output_order.order_date <=" . $data["date_to"] . "
                GROUP BY
                    output_order.salesperson_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $result = "";
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            if ($out[$i]["salesperson_id"] != "") {
                $query_2[] = "  SELECT
                                output_order.salesperson_id,
                                user.user_name,
                                DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%m') AS month,
                                DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%Y') AS year,
                                SUM(output_order.order_price) AS total_price
                            FROM
                                output_order
                            LEFT JOIN
                                user
                            ON
                                output_order.salesperson_id = user.user_id
                            WHERE
                                output_order.status !='1'
                            AND
                                output_order.salesperson_id = " . $out[$i]["salesperson_id"] . "
                            AND
                                output_order.order_date >=" . $data["date_from"] . "
                            AND
                                output_order.order_date <=" . $data["date_to"] . "
                            GROUP BY
                                DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%m'),
                                DATE_FORMAT(FROM_UNIXTIME(output_order.order_date), '%Y')";
            }
        }
        if (isset($query_2)) {
            $size_2 = sizeof($query_2);
        }else{
            $size_2 = 0;
        }
        for ($i = 0; $i < $size_2; $i++) {
            $this->db->query($query_2[$i]);
            $q = $this->db->getResult();
            $s = sizeof($q);
            for ($j = 0; $j < $s; $j++) {
                $arr[$j] = array(
                    $q[$j]["month"] => $q[$j]["total_price"]
                );
            }
            $ds[$i] = $arr;
        }
        for ($i = 0; $i < $size; $i++) {
            for ($j = 1; $j < 13; $j++) {
                $out[$i]["doanhthu"][$j] = "";
            }
        }
        for ($i = 0; $i < $size; $i++) {
            $c = sizeof($ds[$i]);
            for ($j = 0; $j < $c; $j++) {
                if (isset($ds[$i][$j][1])) {
                    $out[$i]["doanhthu"][1] = $ds[$i][$j][1];
                }
                if (isset($ds[$i][$j][2])) {
                    $out[$i]["doanhthu"][2] = $ds[$i][$j][2];
                }
                if (isset($ds[$i][$j][3])) {
                    $out[$i]["doanhthu"][3] = $ds[$i][$j][3];
                }
                if (isset($ds[$i][$j][4])) {
                    $out[$i]["doanhthu"][4] = $ds[$i][$j][4];
                }
                if (isset($ds[$i][$j][5])) {
                    $out[$i]["doanhthu"][5] = $ds[$i][$j][5];
                }
                if (isset($ds[$i][$j][6])) {
                    $out[$i]["doanhthu"][6] = $ds[$i][$j][6];
                }
                if (isset($ds[$i][$j][7])) {
                    $out[$i]["doanhthu"][7] = $ds[$i][$j][7];
                }
                if (isset($ds[$i][$j][8])) {
                    $out[$i]["doanhthu"][8] = $ds[$i][$j][8];
                }
                if (isset($ds[$i][$j][9])) {
                    $out[$i]["doanhthu"][9] = $ds[$i][$j][9];
                }
                if (isset($ds[$i][$j][10])) {
                    $out[$i]["doanhthu"][10] = $ds[$i][$j][10];
                }
                if (isset($ds[$i][$j][11])) {
                    $out[$i]["doanhthu"][11] = $ds[$i][$j][11];
                }
                if (isset($ds[$i][$j][12])) {
                    $out[$i]["doanhthu"][12] = $ds[$i][$j][12];
                }
            }
        }
//         for ($i = 0; $i < $size; $i++) {
//             $out[$i]["doanhthu"] = json_encode($out[$i]["doanhthu"]);
//         }

        $count = sizeof($out);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsTopImport($data)
    {
        $query = "SELECT
                    input_order.partner_id AS supplier_id,
                    partner.partner_name AS supplier_name,
                    SUM(input_order.total_sale) AS total_sale
                FROM
                    input_order
                LEFT JOIN
                    partner
                ON
                    input_order.partner_id = partner.partner_id
                WHERE
                    input_order.status !='1'
                AND
                    input_order.input_code LIKE '%PO%'
                AND
                    input_order.input_date >=" . $data["date_from"] . "
                AND
                    input_order.input_date <=" . $data["date_to"] . "
                GROUP BY
                    input_order.partner_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsTopImportLines($data)
    {
        $query = "SELECT
                    input_order.partner_id,
                    partner.partner_name,
                    SUM(input_order.total_sale) AS total_sale
                FROM
                    input_order
                LEFT JOIN
                    partner
                ON
                    input_order.partner_id = partner.partner_id
                WHERE
                    input_order.status !='1'
                AND
                    input_order.input_code LIKE '%PO%'
                AND
                    input_order.partner_id != ''
                AND
                    input_order.input_date >=" . $data["date_from"] . "
                AND
                    input_order.input_date <=" . $data["date_to"] . "
                GROUP BY
                    input_order.partner_id";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            if ($out[$i]["partner_id"] != "") {
                $query_2[] = "SELECT
                            input_order.partner_id,
                            partner.partner_name,
                            DATE_FORMAT(FROM_UNIXTIME(input_order.input_date), '%c') AS month,
                            DATE_FORMAT(FROM_UNIXTIME(input_order.input_date), '%Y') AS year,
                            SUM(input_order.total_sale) AS total_sales
                        FROM
                            input_order
                        LEFT JOIN
                            partner
                        ON
                            input_order.partner_id = partner.partner_id
                        WHERE
                            input_order.status !='1'
                        AND
                            input_order.input_code LIKE '%PO%'
                        AND
                            input_order.partner_id = " . $out[$i]["partner_id"] . "
                        AND
                            input_order.input_date >=" . $data["date_from"] . "
                        AND
                            input_order.input_date <=" . $data["date_to"] . "
                        GROUP BY
                            DATE_FORMAT(FROM_UNIXTIME(input_order.input_date), '%m'),
                            DATE_FORMAT(FROM_UNIXTIME(input_order.input_date), '%Y')";
            }
        }
        $size_2 = sizeof($query_2);
        for ($i = 0; $i < $size_2; $i++) {
            $this->db->query($query_2[$i]);
            $q = $this->db->getResult();
            $s = sizeof($q);
            for ($j = 0; $j < $s; $j++) {
                $arr[$j] = array(
                    $q[$j]["month"] => $q[$j]["total_sales"]
                );
            }
            $ds[$i] = $arr;
        }
        for ($i = 0; $i < $size; $i++) {
            for ($j = 1; $j < 13; $j++) {
                $out[$i]["doanhthu"][$j] = "";
            }
        }
        for ($i = 0; $i < $size; $i++) {
            $c = sizeof($ds[$i]);
            for ($j = 0; $j < $c; $j++) {
                if (isset($ds[$i][$j][1])) {
                    $out[$i]["doanhthu"][1] = $ds[$i][$j][1];
                }
                if (isset($ds[$i][$j][2])) {
                    $out[$i]["doanhthu"][2] = $ds[$i][$j][2];
                }
                if (isset($ds[$i][$j][3])) {
                    $out[$i]["doanhthu"][3] = $ds[$i][$j][3];
                }
                if (isset($ds[$i][$j][4])) {
                    $out[$i]["doanhthu"][4] = $ds[$i][$j][4];
                }
                if (isset($ds[$i][$j][5])) {
                    $out[$i]["doanhthu"][5] = $ds[$i][$j][5];
                }
                if (isset($ds[$i][$j][6])) {
                    $out[$i]["doanhthu"][6] = $ds[$i][$j][6];
                }
                if (isset($ds[$i][$j][7])) {
                    $out[$i]["doanhthu"][7] = $ds[$i][$j][7];
                }
                if (isset($ds[$i][$j][8])) {
                    $out[$i]["doanhthu"][8] = $ds[$i][$j][8];
                }
                if (isset($ds[$i][$j][9])) {
                    $out[$i]["doanhthu"][9] = $ds[$i][$j][9];
                }
                if (isset($ds[$i][$j][10])) {
                    $out[$i]["doanhthu"][10] = $ds[$i][$j][10];
                }
                if (isset($ds[$i][$j][11])) {
                    $out[$i]["doanhthu"][11] = $ds[$i][$j][11];
                }
                if (isset($ds[$i][$j][12])) {
                    $out[$i]["doanhthu"][12] = $ds[$i][$j][12];
                }
            }
        }
//         for ($i = 0; $i < $size; $i++) {
//             $out[$i]["doanhthu"] = json_encode($out[$i]["doanhthu"]);
//         }

        $count = sizeof($out);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsSales($data)
    {
        $condition = "WHERE
                        output_lines.status !='1'
                      AND
                        output_order. status = 3
                      AND
                        output_order.status_id = 3 ";
        if (isset($data["sale_person_id"]) && $data["sale_person_id"] != "") {
            $condition = $condition . "AND
                    output_order.salesperson_id = '" . $data["sale_person_id"] . "' ";
        }
        if (isset($data["customer_id"]) && $data["customer_id"] != "") {
            $condition = $condition . "AND
                    output_order.partner_id = '" . $data["customer_id"] . "' ";
        }
        if (isset($data["product_category_id"]) && $data["product_category_id"] != "") {
            $condition = $condition . "AND
                    product_category.product_category_id = '" . $data["product_category_id"] . "' ";
        }
        if (isset($data["product_id"]) && $data["product_id"] != "") {
            $condition = $condition . "AND
                    output_lines.product_id = '" . $data["product_id"] . "'% ";
        }
        if (isset($data["pay_type_id"]) && $data["pay_type_id"] != "") {
            $condition = $condition . "AND
                    bank_account.account_id = '" . $data["pay_type_id"] . "' ";
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition = $condition . "AND
                    output_lines.create_time >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $condition = $condition . "AND
                    output_lines.create_time <=" . $data["date_to"] . " ";
        }
        $query = "SELECT
                    output_lines.order_id,
                    output_order.order_code,
                    output_lines.product_id,
                    product.product_code,
                    product.product_name,
                    output_lines.output_line_id,
                    unit.unit_name AS product_unit_name,
                    product_category.product_category_id,
                    category.category_name AS product_category_name,
                    output_lines.output_line_amount AS total_amount,
                    output_lines.product_price AS sale_price,
                    output_lines.product_discount AS product_discount,
                    output_order.discount AS order_discount,
                    bank_account.account_id AS pay_type_id,
                    bank_account.account_name,
                    output_lines.input_lines
                FROM
                    output_lines
                LEFT JOIN
                    product
                ON
                    output_lines.product_id = product.product_id
                LEFT JOIN
                    unit
                ON
                    product.unit_id = unit.unit_id
                LEFT JOIN
                    product_category
                ON
                    output_lines.product_id = product_category.product_id
                LEFT JOIN
                    category
                ON
                    product_category.category_id = category.category_id
                LEFT JOIN
                    product_price
                ON
                    output_lines.product_id = product_price.product_id
                LEFT JOIN
                    pay_line
                ON
                    output_lines.order_id = pay_line.order_id
                LEFT JOIN
                    customer_pay
                ON
                    pay_line.pay_id = customer_pay.pay_id
                LEFT JOIN
                    bank_account
                ON
                    customer_pay.pay_type_id = bank_account.account_id
                LEFT JOIN
                    input_lines
                ON
                    output_lines.input_line_id = input_lines.input_line_id
                LEFT JOIN
                    output_order
                ON
                    output_lines.order_id = output_order.order_id
               " . $condition . "
                GROUP BY
                    output_lines.output_line_id
                    ";
//        echo $query;
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output_line_id = 0;
        for ($i = 0; $i < $count; $i++) {
            $input = json_decode($out[$i]["input_lines"]);
            $output_line_id = $out[$i]["output_line_id"];
            $out[$i]["refund"] = $this->mRefund->getCountProductRefundByOutputLineId($output_line_id);
            $out[$i]["amount"] = $out[$i]["total_amount"] - $out[$i]["refund"];
            $out[$i]["input_lines"] = $input;
            $out[$i]["discount"] = $out[$i]["product_discount"] + $out[$i]["order_discount"];
            $total = $out[$i]["amount"];
            for ($j = 0; $j < sizeof($input); $j++) {
                $query_1 = "SELECT
                                input_lines.input_order_id,
                                input_order.input_code,
                                input_lines.product_price
                            FROM
                                input_lines
                            LEFT JOIN
                                input_order
                            ON
                                input_lines.input_order_id = input_order.input_order_id
                            WHERE
                                input_line_id = " . $input[$j]->input_line_id;
                $this->db->query($query_1);
                $price = $this->db->getResult();
                $out[$i]["input_lines"][$j]->input_code = $price[0]["input_code"];
                $out[$i]["input_lines"][$j]->input_order_id = $price[0]["input_order_id"];
                $out[$i]["input_lines"][$j]->product_price = $price[0]["product_price"];
//                $out[$i]["input_lines"][$j]->root_price = $out[$i]["input_lines"][$j]->product_price * $out[$i]["input_lines"][$j]->amount;
//                $total = $total - $out[$i]["input_lines"][$j]->amount;
                if ($total >= $out[$i]["input_lines"][$j]->amount && $total != 0) {
                    $out[$i]["input_lines"][$j]->root_price = $out[$i]["input_lines"][$j]->product_price * $out[$i]["input_lines"][$j]->amount;
                    $total = $total - $out[$i]["input_lines"][$j]->amount;
                    $out[$i]["input_lines"][$j]->total = $total;
                } else {
                    $out[$i]["input_lines"][$j]->root_price = $out[$i]["input_lines"][$j]->product_price * $total;
                    $out[$i]["input_lines"][$j]->amount = $total;
                    $total = 0;
                }
            }
        }
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

//    public function statisticsExportWarehouse($data) {
//        $condition = "WHERE
//                        inventory.status !='1' ";
//        if (isset($data["product_id"])) {
//            $condition = $condition . "AND
//                    inventory.product_id LIKE '%" . $data["product_id"] . "'% ";
//        }
//        if (isset($data["partner_id"])) {
//            $condition = $condition . "AND
//                    output_order.partner_id LIKE '%" . $data["partner_id"] . "'% ";
//        }
//        if (isset($data["warehouse_id"])) {
//            $condition = $condition . "AND
//                    inventory.warehouse_id LIKE '%" . $data["warehouse_id"] . "'% ";
//        }
//        if (isset($data["date_from"])) {
//            $condition = $condition . "AND
//                    inventory.inventory_date >=" . $data["date_from"] . " ";
//        }
//        if (isset($data["date_to"])) {
//            $condition = $condition . "AND
//                    inventory.inventory_date <=" . $data["date_to"] . " ";
//        }
//        $day = "SELECT
//                inventory.inventory_date,
//                inventory.order_id,
//                inventory.product_id,
//                inventory.inventory_amount
//               FROM
//                inventory
//               LEFT JOIN
//                output_order
//               ON
//                inventory.order_id = output_order.order_id
//               LEFT JOIN
//                product
//               ON
//                inventory.product_id = product.product_id "
//                . $condition .
//                "GROUP BY
//                    inventory.order_id,
//                    inventory.product_id";
////        echo $day;
//        $this->db->query($day);
//        $out_1 = $this->db->getResult();
//        for ($i = 0; $i < sizeof($out_1); $i++) {
//            if ($out_1[$i]["inventory_amount"] < 0) {
//                $query[] = "SELECT
//                                DATE_FORMAT(FROM_UNIXTIME(inventory.inventory_date), '%d/%m/%Y') AS date,
//                                output_order.order_code AS code,
//                                inventory.product_id,
//                                product.product_name,
//                                product.product_code,
//                                product_category.product_category_id,
//                                category.category_name,
//                                unit.unit_name,
//                                inventory.inventory_amount AS product_amount
//                            FROM
//                                inventory
//                            LEFT JOIN
//                                output_order
//                            ON
//                                inventory.order_id = output_order.order_id
//                            LEFT JOIN
//                                product
//                            ON
//                                inventory.product_id = product.product_id
//                            LEFT JOIN
//                                unit
//                            ON
//                                product.unit_id = unit.unit_id
//                            LEFT JOIN
//                                product_category
//                            ON
//                                product.product_id = product_category.product_id
//                            LEFT JOIN
//                               category
//                            ON
//                                product_category.category_id = category.category_id
//                            WHERE
//                                inventory.create_time =
//                                (SELECT
//                                    MAX(inventory.create_time)
//                                FROM
//                                    inventory
//                                WHERE
//                                    inventory.inventory_date = " . $out_1[$i]["inventory_date"] . "
//                                AND
//                                    inventory.order_id = " . $out_1[$i]["order_id"] . "
//                                AND
//                                    inventory.product_id = " . $out_1[$i]["product_id"] . ")
//                            AND
//                                inventory.inventory_date = " . $out_1[$i]["inventory_date"] . "
//                            AND
//                                inventory.order_id = " . $out_1[$i]["order_id"] . "
//                            GROUP BY
//                                inventory.product_id";
//            }
//        }
//        for ($i = 0; $i < sizeof($query); $i++) {
//            $this->db->query($query[$i]);
////            echo $query[$i];
////            echo ("<br/>");
//            $result[] = $this->db->getResult();
//        }
//        for ($i = 0 ; $i < sizeof($result); $i++){
//            $out[$i] = $result[$i][0];
//        }
//        $count = sizeof($out);
//        $output["status"] = 1;
//        $output["erros"] = NULL;
//        $output["message"] = NULL;
//        $output["data"] = array(
//            "numer_row_total" => $count,
//            "number_row_show" => $count,
//            "item_list" => $out
//        );
//        return $output;
//    }


    public function statisticsExportWarehouse($data)
    {
        $condition = "WHERE
                        output_order.status != '1'
                      AND
                        output_order.status = 3
                      AND
                        output_order.status_id = 3";
        $condition_2 = "WHERE
                          inventory.amount < 0
                        AND
                          inventory.description = 'Điều chỉnh tồn kho' ";
        if (isset($data["product_id"]) && $data["product_id"] != "") {
            $condition = $condition . " AND
                    output_lines.product_id = " . $data["product_id"];
            $condition_2 = $condition_2 . "AND
                    inventory.inventory_id = " . $data["product_id"];
        }
        if (isset($data["partner_id"]) && $data["partner_id"] != "") {
            $condition = $condition . " AND
                    output_order.partner_id = " . $data["partner_id"];
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
            $condition = $condition . " AND
                    output_lines.warehouse_id = " . $data["warehouse_id"];
            $condition_2 = $condition_2 . "AND
                    inventory.warehouse_id = " . $data["warehouse_id"];
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition = $condition . " AND
                    output_order.order_date >=" . $data["date_from"] . " ";
            $condition_2 = $condition_2 . "AND
                    inventory.inventory_date >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $condition = $condition . " AND
                     output_order.order_date <=" . $data["date_to"] . " ";
            $condition_2 = $condition_2 . "AND
                     inventory.inventory_date <=" . $data["date_to"] . " ";
        }
        $q_input = "SELECT
                    inventory.inventory_date AS order_date,
                    adjust_inventory.adjust_inventory_name AS order_code
                  FROM
                    inventory
                  LEFT JOIN
                    adjust_inventory
                  ON
                    inventory.order_id = adjust_inventory.adjust_inventory_id
                    " . $condition_2 . "
                  AND
                    inventory.description = 'Điều chỉnh tồn kho' ";
        // echo $q_input;
        $this->db->query($q_input);
        $input = $this->db->getResult();
        $size_2 = sizeof($input);
        for ($i = 0; $i < $size_2; $i++) {
            $q_i = "SELECT
                      adjust_inventory_line.product_id,
                      product.product_name,
                      product.product_code,
                      product_category.category_id AS product_category_id,
                      category.category_name AS product_category_name,
                      unit.unit_name,
                      adjust_inventory_line.product_old_amount,
                      adjust_inventory_line.product_amount
                    FROM
                      adjust_inventory_line
                    LEFT JOIN
                      adjust_inventory
                    ON
                      adjust_inventory_line.adjust_inventory_id = adjust_inventory.adjust_inventory_id
                    LEFT JOIN
                      product
                    ON
                      adjust_inventory_line.product_id = product.product_id
                    LEFT JOIN
                      product_category
                    ON
                      adjust_inventory_line.product_id = product_category.product_id
                    LEFT JOIN
                      category
                    ON
                      product_category.category_id = category.category_id
                    LEFT JOIN
                      unit
                    ON
                      product.unit_id = unit.unit_id
                    WHERE
                      adjust_inventory.adjust_inventory_name = '" . $input[$i]["order_code"] . "'
                    AND
                      adjust_inventory_line.order_id != ''
                    AND
                      adjust_inventory_line.status != '1'";
            $this->db->query($q_i);
            $i_product = $this->db->getResult();
            $s_i = sizeof($i_product);
            for ($j = 0; $j < $s_i; $j++) {
                $i_product[$j]["product_amount"] = $i_product[$j]["product_old_amount"] - $i_product[$j]["product_amount"];
            }
            $input[$i]["product"] = $i_product;
        }
        $query = "SELECT
                    output_order.order_date,
                    output_order.order_code
                  FROM
                    output_order
                  LEFT JOIN
                    output_lines
                  ON
                    output_order.order_id = output_lines.order_id
                  LEFT JOIN
                    partner
                  ON
                    output_order.partner_id = partner.partner_id "
            . $condition . "
                  GROUP BY
                    output_order.order_id";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        // echo $query;
        for ($i = 0; $i < $size; $i++) {
            $q = "SELECT
                    output_lines.product_id,
                    product.product_name,
                    product.product_code,
                    product_category.category_id AS product_category_id,
                    category.category_name AS product_category_name,
                    unit.unit_name,
                    output_lines.output_line_amount AS product_amount
                  FROM
                    output_lines
                  LEFT JOIN
                    output_order
                  ON
                    output_lines.order_id = output_order.order_id
                  LEFT JOIN
                    product
                  ON
                    output_lines.product_id = product.product_id
                  LEFT JOIN
                    product_category
                  ON
                    output_lines.product_id = product_category.product_id
                  LEFT JOIN
                    category
                  ON
                    product_category.category_id = category.category_id
                  LEFT JOIN
                    unit
                  ON
                    product.unit_id = unit.unit_id
                  WHERE
                    output_order.order_code = '" . $out[$i]["order_code"] . "'
                  AND
                    output_lines.status != '1'";
            $this->db->query($q);
            $p = $this->db->getResult();
            $out[$i]["product"] = $p;
        }
        $result = array_merge($out, $input);
        $count = sizeof($result);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $result
        );
        return $output;
    }

//    public function statisticsInventory($data) {
//        $condition = "WHERE
//                        input_lines.status !='1'";
//        $condition_2 = "WHERE
//                        input_lines.status !='1'";
//        $query = "SELECT
//                    input_lines.product_id,
//                    product.product_code,
//                    product.product_name,
//                    product_category.product_category_id,
//                    category.category_name,
//                    unit.unit_name,
//                    input_order.input_order_id AS import_id,
//                    input_order.input_code AS import_code,
//                    input_lines.expire_date AS expired_date,
//                    (SELECT
//                        SUM(input_lines.input_line_amount)
//                     FROM
//                        input_lines
//                     " . $condition_2 . "
//                     GROUP BY
//                        input_lines.product_id)
//                  FROM
//                    input_lines
//                  LEFT JOIN
//                    input_order
//                  ON
//                    input_lines.input_order_id = input_order.input_order_id
//                  LEFT JOIN
//                    product
//                  ON
//                    input_lines.product_id = product.product_id
//                  LEFT JOIN
//                    unit
//                  ON
//                    product.unit_id = unit.unit_id
//                  LEFT JOIN
//                    product_category
//                  ON
//                    product.product_id = product_category.product_id
//                  LEFT JOIN
//                    category
//                  ON
//                    product_category.category_id = category.category_id "
//                . $condition;
//        echo $query;
//    }
//    public function statisticsImportWarehouse() {
//        $condition = "WHERE
//                        input_lines.status !='1'
//                      AND
//                        input_order.input_code LIKE '%PO%'";
//        $query = "SELECT
//                    input_order.input_date,
//                    input_order.input_code,
//                    input_lines.product_id,
//                    product.product_code,
//                    product.product_name,
//                    product_category.product_category_id,
//                    category.category_name AS product_category_name,
//                    unit.unit_name AS product_unit_name,
//                    input_lines.input_line_amount AS product_amount
//                  FROM
//                    input_lines
//                  LEFT JOIN
//                    input_order
//                  ON
//                    input_lines.input_order_id = input_order.input_order_id
//                  LEFT JOIN
//                    product
//                  ON
//                    input_lines.product_id = product.product_id
//                  LEFT JOIN
//                    unit
//                  ON
//                    product.unit_id = unit.unit_id
//                  LEFT JOIN
//                    product_category
//                  ON
//                    product.product_id = product_category.product_id
//                  LEFT JOIN
//                    category
//                  ON
//                    product_category.category_id = category.category_id
//                  LEFT JOIN
//                    partner
//                  ON
//                    input_order.partner_id = partner.partner_id
//                   " . $condition . "
//                  GROUP BY
//                    input_order.input_code";
//        echo $query;
//    }
    public function statisticsImportWarehouse($data)
    {
        $condition = "WHERE
                        input_order.status != '1'
                      AND
                        input_order.status = 3
                      AND
                        input_order.input_code LIKE '%PO-%' ";
        $condition_2 = "WHERE
                          inventory.amount > 0";
        if (isset($data["product_id"]) && $data["product_id"] != "") {
            $condition = $condition . " AND
                    input_lines.product_id = " . $data["product_id"];
            $condition_2 = $condition_2 . "AND
                              inventory.product_id = " . $data["product_id"];
        }
        if (isset($data["partner_id"]) && $data["partner_id"] != "") {
            $condition = $condition . " AND
                    input_order.partner_id = " . $data["partner_id"];
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
            $condition = $condition . " AND
                    input_lines.warehouse_id = " . $data["warehouse_id"];
            $condition_2 = $condition_2 . "
                    inventory.warehouse_id = " . $data["warehouse_id"];
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition = $condition . " AND
                    input_order.input_date >=" . $data["date_from"] . " ";
            $condition_2 = $condition_2 . " AND
                    inventory.create_time >= " . $data["date_from"] . " ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $condition = $condition . " AND
                     input_order.input_date <=" . $data["date_to"] . " ";
            $condition_2 = $condition_2 . "AND
                     inventory.create_time <=" . $data["date_to"] . " ";
        }
        $q_input = "SELECT
                    inventory.inventory_date AS input_date,
                    adjust_inventory.adjust_inventory_name AS input_code
                  FROM
                    inventory
                  LEFT JOIN
                    adjust_inventory
                  ON
                    inventory.order_id = adjust_inventory.adjust_inventory_id
                    " . $condition_2 . "
                  AND
                    inventory.description = 'Điều chỉnh tồn kho' ";
        // echo $q_input;
        $this->db->query($q_input);
        $input = $this->db->getResult();
        $size_2 = sizeof($input);
        for ($i = 0; $i < $size_2; $i++) {
            $q_i = "SELECT
                      adjust_inventory_line.product_id,
                      product.product_name,
                      product.product_code,
                      product_category.category_id AS product_category_id,
                      category.category_name AS product_category_name,
                      unit.unit_name,
                      adjust_inventory_line.product_old_amount,
                      adjust_inventory_line.product_amount
                    FROM
                      adjust_inventory_line
                    LEFT JOIN
                      adjust_inventory
                    ON
                      adjust_inventory_line.adjust_inventory_id = adjust_inventory.adjust_inventory_id
                    LEFT JOIN
                      product
                    ON
                      adjust_inventory_line.product_id = product.product_id
                    LEFT JOIN
                      product_category
                    ON
                      adjust_inventory_line.product_id = product_category.product_id
                    LEFT JOIN
                      category
                    ON
                      product_category.category_id = category.category_id
                    LEFT JOIN
                      unit
                    ON
                      product.unit_id = unit.unit_id
                    WHERE
                      adjust_inventory.adjust_inventory_name = '" . $input[$i]["input_code"] . "'
                    AND
                      adjust_inventory_line.input_order_id != ''
                    AND
                      adjust_inventory_line.status != '1'";
            $this->db->query($q_i);
            $i_product = $this->db->getResult();
            $s_i = sizeof($i_product);
            for ($j = 0; $j < $s_i; $j++) {
                $i_product[$j]["product_amount"] = $i_product[$j]["product_amount"] - $i_product[$j]["product_old_amount"];
            }
            $input[$i]["product"] = $i_product;
        }
        $query = "SELECT
                    input_order.input_date,
                    input_order.input_code
                  FROM
                    input_order
                  LEFT JOIN
                    input_lines
                  ON
                    input_order.input_order_id = input_lines.input_order_id
                  LEFT JOIN
                    partner
                  ON
                    input_order.partner_id = partner.partner_id "
            . $condition . "
                  GROUP BY
                    input_order.input_order_id";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            $q = "SELECT
                    input_lines.product_id,
                    product.product_name,
                    product.product_code,
                    product_category.category_id AS product_category_id,
                    category.category_name AS product_category_name,
                    unit.unit_name,
                    input_lines.input_line_amount AS product_amount
                  FROM
                    input_lines
                  LEFT JOIN
                    input_order
                  ON
                    input_lines.input_order_id = input_order.input_order_id
                  LEFT JOIN
                    product
                  ON
                    input_lines.product_id = product.product_id
                  LEFT JOIN
                    product_category
                  ON
                    input_lines.product_id = product_category.product_id
                  LEFT JOIN
                    category
                  ON
                    product_category.category_id = category.category_id
                  LEFT JOIN
                    unit
                  ON
                    product.unit_id = unit.unit_id
                  WHERE
                    input_order.input_code = '" . $out[$i]["input_code"] . "'
                  AND
                    input_lines.status != '1'";
            $this->db->query($q);
            $p = $this->db->getResult();
            $out[$i]["product"] = $p;
        }
        $result = array_merge($out, $input);
        $count = sizeof($result);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $result
        );
        return $output;
    }

    public function statisticsInventory($data)
    {
        $condition = "WHERE
                        inventory.status != '1'
                      AND
                        inventory.inventory_amount > '0'";
        $condition_total = " ";
        if (isset($data["partner_id"]) && $data["partner_id"] != "") {
            $condition = $condition . " AND
                    input_order.partner_id = " . $data["partner_id"];
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
            $condition = $condition . " AND
                    inventory.warehouse_id = " . $data["warehouse_id"];
            $condition_total = $condition_total . "AND
                    inventory.warehouse_id = " . $data["warehouse_id"];
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition = $condition . " AND
                    inventory.inventory_date >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $condition = $condition . " AND
                    inventory.inventory_date <=" . $data["date_to"] . " ";
        } else {
            $data["date_to"] = time();
            $condition = $condition . " AND
                    inventory.inventory_date <=" . $data["date_to"] . " ";
        }

        if (isset($data["product_id"]) && $data["product_id"] != "") {
            $condition = $condition . " AND
                    inventory.product_id = " . $data["product_id"];
        }
        $query = "SELECT
                    inventory.product_id,
                    inventory.order_id,
                    inventory.amount,
                    product.product_code,
                    product.product_name,
                    product_category.category_id AS product_category_id,
                    category.category_name AS product_category_name,
                    unit.unit_name AS product_unit_name
                  FROM
                    inventory
                  LEFT JOIN
                    product
                  ON
                    inventory.product_id = product.product_id
                  LEFT JOIN
                    product_category
                  ON
                    inventory.product_id = product_category.product_id
                  LEFT JOIN
                    category
                  ON
                    product_category.category_id = category.category_id
                  LEFT JOIN
                    unit
                  ON
                     product.unit_id = unit.unit_id
                  LEFT JOIN
                    input_order
                  ON
                    inventory.order_id = input_order.input_order_id  "
            . $condition . "
                  GROUP BY
                    inventory.product_id";
        //  echo $query;
//        echo "<br/>";
        $this->db->query($query);
        $out = $this->db->getResult();
        $size = sizeof($out);
        for ($i = 0; $i < $size; $i++) {
            $q2 = "SELECT
                        inventory.order_id,
                        input_order.input_order_id AS import_id,
			                  input_order.input_date AS input_date,
                        input_order.input_code AS import_code,
                        inventory.amount
                    FROM
                        inventory
                    LEFT JOIN
                        input_lines
                    ON
                        inventory.order_id = input_lines.input_order_id
                    LEFT JOIN
                        input_order
                    ON
                        inventory.order_id = input_order.input_order_id
                    WHERE
                        inventory.inventory_amount > 0
                    AND
                        input_order.status !=1
                    AND
                        input_order.status =3
                    AND
                        inventory.amount > 0
                    AND
                        inventory.product_id = " . $out[$i]["product_id"] . "
                    GROUP BY
                        inventory.order_id";
            $this->db->query($q2);
            $p = $this->db->getResult();
//            echo $q2;
            $out[$i]["order"] = $p;
            $s3 = sizeof($p);

            $q3 = "SELECT
                        SUM(inventory.amount) AS inventory_amount,
                        inventory.product_id
                    FROM
                        inventory
                    WHERE
                        inventory.status !='1'
                    AND
                        inventory.product_id = " . $out[$i]["product_id"] . "
                    AND
                        inventory.create_time <= " . $data["date_to"] . "
                    " . $condition_total . "
                    ORDER BY
                        inventory.create_time DESC";
            //  echo $q3;
//                echo ("<br/>");
            $this->db->query($q3);
//                $o = array();
            $total = $this->db->getResult();
            $out[$i]["product_inventory"] = $total[0]["inventory_amount"];


//            echo '<pre>';
//            print_r($total);
//            echo '</pre>';


            for ($j = 0; $j < $s3; $j++) {
                $q4 = "SELECT
                        input_lines.product_id,
                        input_lines.input_order_id,
                        input_lines.expire_date,
                        input_lines.warehouse_id
                       FROM
                        input_lines
                       WHERE
                        input_lines.product_id = " . $out[$i]["product_id"] . "
                       AND
                        input_lines.input_order_id = " . $p[$j]["order_id"] . " ";
                //  echo $q4;
                $this->db->query($q4);
                $ex_date = $this->db->getResult();
                $p[$j]["warehouse_id"] = $ex_date[0]["warehouse_id"];
                $p[$j]["expire_date"] = $ex_date[0]["expire_date"];
//                print_r($ex_date);
            }
            $out[$i]["order"] = $p;
        }
        $count = sizeof($out);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function statisticsWarehouseTag($data)
    {
        $condition_1 = "WHERE
                inventory.status != '1'
                AND
                inventory.amount > '0'
                AND
                inventory.product_id = " . $data["product_id"] . " ";

        $condition_2 = "WHERE
                inventory.status != '1'
                AND
                inventory.amount < '0'
                AND
                inventory.product_id = " . $data["product_id"] . " ";
        if (isset($data["partner_id"]) && $data["partner_id"] != "") {
            $condition_1 = $condition_1 . " AND
                input_order.partner_id = '" . $data["partner_id"] . "' ";
            $condition_2 = $condition_2 . " AND
                input_order.partner_id = '" . $data["partner_id"] . "' ";
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
            $condition_1 = $condition_1 . " AND
                inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
            $condition_2 = $condition_2 . " AND
                inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $condition_1 = $condition_1 . " AND
                inventory.inventory_date >= '" . $data["date_from"] . "' ";
            $condition_2 = $condition_2 . " AND
                inventory.inventory_date >= '" . $data["date_from"] . "' ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $condition_1 = $condition_1 . " AND
                inventory.inventory_date <='" . $data["date_to"] . "' ";
            $condition_2 = $condition_2 . " AND
                inventory.inventory_date <='" . $data["date_to"] . "' ";
        }
        $query_1 = "SELECT
                inventory.inventory_id,
                inventory.inventory_date AS date,
                inventory.order_id,
                inventory.product_id,
                inventory.create_time,
                inventory.description
                FROM
                inventory
                LEFT JOIN
                input_order
                ON
                inventory.order_id = input_order.input_order_id
                LEFT JOIN
                partner
                ON
                input_order.partner_id = partner.partner_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id "
            . $condition_1 . "
                AND
                inventory.description != 'Chuyển kho'
                GROUP BY
                inventory.inventory_id
                ORDER BY
                inventory.inventory_id ASC";
//        echo $query_1;
        $this->db->query($query_1);
        $t1 = $this->db->getResult();
        $s1 = sizeof($t1);
        for ($i = 0; $i < $s1; $i++) {
            if ($t1[$i]["description"] == "Nhập kho") { // Nhập kho
                $q1 = "SELECT
                inventory.amount,
                inventory.inventory_amount AS total,
                input_order.partner_id,
                partner.partner_name,
                input_order.input_code,
                inventory.order_id,
                inventory.product_id,
                unit.unit_name,
                input_lines.product_price,
                inventory.warehouse_id,
                warehouse.warehouse_name
                FROM
                inventory
                LEFT JOIN
                input_order
                ON
                inventory.order_id = input_order.input_order_id
                LEFT JOIN
                input_lines
                ON
                inventory.order_id = input_lines.input_order_id
                LEFT JOIN
                partner
                ON
                input_order.partner_id = partner.partner_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id
                LEFT JOIN
                warehouse
                ON
                inventory.warehouse_id = warehouse.warehouse_id
                WHERE
                inventory.amount > 0
                AND
                inventory.order_id = " . $t1[$i]["order_id"] . "
                AND
                inventory.product_id = " . $t1[$i]["product_id"] . "
                AND
                inventory.inventory_id = " . $t1[$i]["inventory_id"] . "
                GROUP BY
                inventory.inventory_amount";
                $this->db->query($q1);
//                echo $q1;
//            echo ("\n");
                $t2 = $this->db->getResult();
                $t1[$i]["code"] = $t2[0]["input_code"];
                $t1[$i]["partner_id"] = $t2[0]["partner_id"];
                $t1[$i]["partner_name"] = $t2[0]["partner_name"];
                $t1[$i]["import_amount"] = $t2[0]["amount"];
                $t1[$i]["product_unit_name"] = $t2[0]["unit_name"];
                $t1[$i]["export_amount"] = '';
                $t1[$i]["inventory"] = $t2[0]["total"];
                $t1[$i]["product_price"] = $t2[0]["product_price"];
                $t1[$i]["warehouse_name"] = $t2[0]["warehouse_name"];
            }
            if ($t1[$i]["description"] == "Điều chỉnh tồn kho") {// Điều chỉnh tồn kho tăng số lượng sản phầm
                $q5 = "SELECT
                inventory.amount,
                inventory.inventory_amount AS total,
                adjust_inventory.adjust_inventory_id,
                adjust_inventory.adjust_inventory_name,
                inventory.order_id,
                inventory.product_id,
                unit.unit_name,
                adjust_inventory_line.product_price,
                inventory.warehouse_id,
                warehouse.warehouse_name
                FROM
                inventory
                LEFT JOIN
                adjust_inventory
                ON
                inventory.order_id = adjust_inventory.adjust_inventory_id
                LEFT JOIN
                adjust_inventory_line
                ON
                inventory.order_id = adjust_inventory_line.adjust_inventory_line_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id
                LEFT JOIN
                warehouse
                ON
                inventory.warehouse_id = warehouse.warehouse_id
                WHERE
                inventory.amount > 0
                AND
                inventory.order_id = " . $t1[$i]["order_id"] . "
                AND
                inventory.product_id = " . $t1[$i]["product_id"] . "
                AND
                inventory.inventory_id = " . $t1[$i]["inventory_id"] . "
                GROUP BY
                inventory.inventory_amount";
                $this->db->query($q5);
//                echo $q5;
//            echo ("\n");
                $t5 = $this->db->getResult();
                $t1[$i]["code"] = $t5[0]["adjust_inventory_name"];
                $t1[$i]["partner_id"] = '';
                $t1[$i]["partner_name"] = '';
                $t1[$i]["import_amount"] = $t5[0]["amount"];
                $t1[$i]["product_unit_name"] = $t5[0]["unit_name"];
                $t1[$i]["export_amount"] = '';
                $t1[$i]["inventory"] = $t5[0]["total"];
                $t1[$i]["product_price"] = $t5[0]["product_price"];
                $t1[$i]["warehouse_name"] = $t5[0]["warehouse_name"];
            }
//            if ($t1[$i]["description"] == "Chuyển kho") {// Chuyển từ 1 kho khác đến 1 kho này, đây là kho được chuyển đến nên số lượng sản phẩm sẽ tăng lên
//                $q6 = "SELECT
//                inventory.amount,
//                inventory.inventory_amount AS total,
//                input_order.partner_id,
//                partner.partner_name,
//                input_order.input_code,
//                inventory.order_id,
//                inventory.product_id,
//                unit.unit_name,
//                input_lines.product_price,
//                inventory.warehouse_id,
//                warehouse.warehouse_name
//                FROM
//                inventory
//                LEFT JOIN
//                input_order
//                ON
//                inventory.order_id = input_order.input_order_id
//                LEFT JOIN
//                input_lines
//                ON
//                inventory.order_id = input_lines.input_order_id
//                LEFT JOIN
//                partner
//                ON
//                input_order.partner_id = partner.partner_id
//                LEFT JOIN
//                product
//                ON
//                inventory.product_id = product.product_id
//                LEFT JOIN
//                unit
//                ON
//                product.unit_id = unit.unit_id
//                LEFT JOIN
//                warehouse
//                ON
//                inventory.warehouse_id = warehouse.warehouse_id
//                WHERE
//                inventory.amount > 0
//                AND
//                inventory.order_id = " . $t1[$i]["order_id"] . "
//                AND
//                inventory.product_id = " . $t1[$i]["product_id"] . "
//                AND
//                inventory.inventory_id = " . $t1[$i]["inventory_id"] . "
//                GROUP BY
//                inventory.inventory_amount";
//                $this->db->query($q6);
////                echo $q1;
////            echo ("\n");
//                $t6 = $this->db->getResult();
//                $t1[$i]["code"] = $t6[0]["input_code"];
//                $t1[$i]["partner_id"] = $t6[0]["partner_id"];
//                $t1[$i]["partner_name"] = $t6[0]["partner_name"];
//                $t1[$i]["import_amount"] = $t6[0]["amount"];
//                $t1[$i]["product_unit_name"] = $t6[0]["unit_name"];
//                $t1[$i]["export_amount"] = '';
//                $t1[$i]["inventory"] = $t6[0]["total"];
//                $t1[$i]["product_price"] = $t6[0]["product_price"];
//                $t1[$i]["warehouse_name"] = $t6[0]["warehouse_name"];
//            }
            if ($t1[$i]["description"] == "Hàng trả về") {// Hàng trả về số lượng sản phẩm trong kho sẽ tăng lên
                $q7 = "SELECT
                inventory.amount,
                inventory.inventory_amount AS total,
                input_order.partner_id,
                partner.partner_name,
                input_order.input_code,
                inventory.order_id,
                inventory.product_id,
                unit.unit_name,
                input_lines.product_price,
                inventory.warehouse_id,
                warehouse.warehouse_name
                FROM
                inventory
                LEFT JOIN
                input_order
                ON
                inventory.order_id = input_order.input_order_id
                LEFT JOIN
                input_lines
                ON
                inventory.order_id = input_lines.input_order_id
                LEFT JOIN
                partner
                ON
                input_order.partner_id = partner.partner_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id
                LEFT JOIN
                warehouse
                ON
                inventory.warehouse_id = warehouse.warehouse_id
                WHERE
                inventory.amount > 0
                AND
                inventory.order_id = " . $t1[$i]["order_id"] . "
                AND
                inventory.product_id = " . $t1[$i]["product_id"] . "
                AND
                inventory.inventory_id = " . $t1[$i]["inventory_id"] . "
                GROUP BY
                inventory.inventory_amount";
                $this->db->query($q7);
//                echo $q1;
//            echo ("\n");
                $t7 = $this->db->getResult();
                $t1[$i]["code"] = $t7[0]["input_code"];
                $t1[$i]["partner_id"] = $t7[0]["partner_id"];
                $t1[$i]["partner_name"] = $t7[0]["partner_name"];
                $t1[$i]["import_amount"] = $t7[0]["amount"];
                $t1[$i]["product_unit_name"] = $t7[0]["unit_name"];
                $t1[$i]["export_amount"] = '';
                $t1[$i]["inventory"] = $t7[0]["total"];
                $t1[$i]["product_price"] = $t7[0]["product_price"];
                $t1[$i]["warehouse_name"] = $t7[0]["warehouse_name"];
            }
        }

        $query_2 = "SELECT
                inventory.inventory_id,
                inventory.inventory_date AS date,
                inventory.order_id,
                inventory.product_id,
                inventory.create_time,
                inventory.description
                FROM
                inventory LEFT JOIN
                output_order
                ON
                inventory.order_id = output_order.order_id
                LEFT JOIN
                partner
                ON
                output_order.partner_id = partner.partner_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id "
            . $condition_2 . "
                AND
                inventory.description != 'Chuyển kho'
                GROUP BY
                inventory.inventory_id";
        //  echo $query_2;
        $this->db->query($query_2);
        $t3 = $this->db->getResult();
        $s3 = sizeof($t3);
        for ($i = 0; $i < $s3; $i++) {
            //Xuất kho theo hình thức đơn hàng bán
            if ($t3[$i]['description'] == "Đơn hàng bán") {
                $q2 = "SELECT
                inventory.amount AS amount,
                inventory.inventory_amount AS total,
                output_order.partner_id,
                partner.partner_name,
                output_order.order_code,
                inventory.order_id,
                inventory.product_id,
                unit.unit_name,
                output_lines.product_price,
                inventory.warehouse_id,
                warehouse.warehouse_name
                FROM
                inventory
                LEFT JOIN
                output_order
                ON
                inventory.order_id = output_order.order_id
                LEFT JOIN
                output_lines
                ON
                inventory.order_id = output_lines.order_id
                LEFT JOIN
                partner
                ON
                output_order.partner_id = partner.partner_id
                LEFT JOIN
                product
                ON
                inventory.product_id = product.product_id
                LEFT JOIN
                unit
                ON
                product.unit_id = unit.unit_id
                LEFT JOIN
                warehouse
                ON
                inventory.warehouse_id = warehouse.warehouse_id
                WHERE
                inventory.amount < 0
                AND
                inventory.order_id = " . $t3[$i]["order_id"] . "
                AND
                inventory.product_id = " . $t3[$i]["product_id"] . "
                AND
                inventory.inventory_id = " . $t3[$i]["inventory_id"] . "
                GROUP BY
                inventory.inventory_amount";
//            echo $q2;
                $this->db->query($q2);
                $t4 = $this->db->getResult();
                $t3[$i]["code"] = $t4[0]["order_code"];
                $t3[$i]["partner_id"] = $t4[0]["partner_id"];
                $t3[$i]["partner_name"] = $t4[0]["partner_name"];
                $t3[$i]["import_amount"] = '';
                $t3[$i]["product_unit_name"] = $t4[0]["unit_name"];
                $t3[$i]["export_amount"] = $t4[0]["amount"];
                $t3[$i]["inventory"] = $t4[0]["total"];
                $t3[$i]["product_price"] = $t4[0]["product_price"];
                $t3[$i]["warehouse_name"] = $t4[0]["warehouse_name"];
            }
            //Xuất kho theo hình thức điều chỉnh tồn kho
            if ($t3[$i]['description'] == "Điều chỉnh tồn kho") {
                $q8 = "SELECT
                            inventory.amount,
                            inventory.inventory_amount AS total,
                            adjust_inventory.adjust_inventory_id,
                            adjust_inventory.adjust_inventory_name,
                            inventory.order_id,
                            inventory.product_id,
                            unit.unit_name,
                            adjust_inventory_line.product_price,
                            inventory.warehouse_id,
                            warehouse.warehouse_name
                        FROM
                            inventory
                        LEFT JOIN
                            adjust_inventory
                        ON
                            inventory.order_id = adjust_inventory.adjust_inventory_id
                        LEFT JOIN
                            adjust_inventory_line
                        ON
                            inventory.order_id = adjust_inventory_line.adjust_inventory_line_id
                        LEFT JOIN
                            product
                        ON
                            inventory.product_id = product.product_id
                        LEFT JOIN
                            unit
                        ON
                            inventory.product_id = unit.unit_id
                        LEFT JOIN
                            warehouse
                        ON
                            inventory.warehouse_id = warehouse.warehouse_id
                        WHERE
                            inventory.amount < 0
                        AND
                            inventory.order_id = " . $t3[$i]["order_id"] . "
                        AND
                            inventory.product_id = " . $t3[$i]["product_id"] . "
                        AND
                            inventory.inventory_id = " . $t3[$i]["inventory_id"] . "
                        GROUP BY
                            inventory.inventory_amount";
                $this->db->query($q8);
//                echo $q8;
//            echo ("\n");
                $t8 = $this->db->getResult();
                $t3[$i]["code"] = $t8[0]["adjust_inventory_name"];
                $t3[$i]["partner_id"] = '';
                $t3[$i]["partner_name"] = '';
                $t3[$i]["import_amount"] = '';
                $t3[$i]["product_unit_name"] = $t8[0]["unit_name"];
                $t3[$i]["export_amount"] = $t8[0]["amount"];
                $t3[$i]["inventory"] = $t8[0]["total"];
                $t3[$i]["product_price"] = $t8[0]["product_price"];
                $t3[$i]["warehouse_name"] = $t8[0]["warehouse_name"];
            }
            //Xuất kho theo hình thức chuyển từ kho này sang kho khác, sản phẩm kho này sẽ bị giảm đi
//            if ($t3[$i]['description'] == "Chuyển kho") {
//                $q9 = "SELECT
//                inventory.amount AS amount,
//                inventory.inventory_amount AS total,
//                output_order.partner_id,
//                partner.partner_name,
//                output_order.order_code,
//                inventory.order_id,
//                inventory.product_id,
//                unit.unit_name,
//                output_lines.product_price,
//                warehouse.warehouse_name
//                FROM
//                inventory
//                LEFT JOIN
//                output_order
//                ON
//                inventory.order_id = output_order.order_id
//                LEFT JOIN
//                output_lines
//                ON
//                inventory.order_id = output_lines.order_id
//                LEFT JOIN
//                partner
//                ON
//                output_order.partner_id = partner.partner_id
//                LEFT JOIN
//                product
//                ON
//                inventory.product_id = product.product_id
//                LEFT JOIN
//                unit
//                ON
//                product.unit_id = unit.unit_id
//                LEFT JOIN
//                warehouse
//                ON
//                inventory.warehouse_id = warehouse.warehouse_id
//                WHERE
//                inventory.amount < 0
//                AND
//                inventory.order_id = " . $t3[$i]["order_id"] . "
//                AND
//                inventory.product_id = " . $t3[$i]["product_id"] . "
//                AND
//                inventory.inventory_id = " . $t3[$i]["inventory_id"] . "
//                GROUP BY
//                inventory.inventory_amount";
////            echo $q2;
//                $this->db->query($q9);
//                $t9 = $this->db->getResult();
//                $t3[$i]["code"] = $t9[0]["order_code"];
//                $t3[$i]["partner_id"] = $t9[0]["partner_id"];
//                $t3[$i]["partner_name"] = $t9[0]["partner_name"];
//                $t3[$i]["import_amount"] = '';
//                $t3[$i]["product_unit_name"] = $t9[0]["unit_name"];
//                $t3[$i]["export_amount"] = $t9[0]["amount"];
//                $t3[$i]["inventory"] = $t9[0]["total"];
//                $t3[$i]["product_price"] = $t9[0]["product_price"];
//                $t3[$i]["warehouse_name"] = $t9[0]["warehouse_name"];
//            }
        }
//        for ($i = 0; $i < sizeof($t1); $i++) {
//            $q2 = "SELECT
//                    inventory.product_id
//                   FROM
//                    inventory
//                   WHERE
//                    inventory.inventory_date = " . $t1[$i]["date"] . "
//                   AND
//                    inventory.order_id = " . $t1[$i]["order_id"];
////            echo $q2;
//            $this->db->query($q2);
//            $t2 = $this->db->getResult();
//            $t1[$i]["product_id"] = $t2;
//        }
        $result = array_merge($t1, $t3);
        $count = sizeof($result);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $result
        );
        return $output;
    }

    /*
      public function statisticsImportExport($data) {
      $condition_1 = "WHERE
      inventory.status != '1'
      AND
      inventory.inventory_amount > '0'";
      $condition_2 = "WHERE
      inventory.status != '1'
      AND
      inventory.inventory_amount < '0'";
      if (isset($data["partner_id"]) && $data["partner_id"] != "") {
      $condition_1 = $condition_1 . " AND
      input_order.partner_id = '" . $data["partner_id"] . "' ";
      $condition_2 = $condition_2 . " AND
      input_order.partner_id = '" . $data["partner_id"] . "' ";
      }
      if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
      $condition_1 = $condition_1 . " AND
      inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
      $condition_2 = $condition_2 . " AND
      inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
      }
      if (isset($data["date_from"]) && $data["date_from"] != "") {
      $condition_1 = $condition_1 . " AND
      inventory.inventory_date >= '" . $data["date_from"] . "' ";
      $condition_2 = $condition_2 . " AND
      inventory.inventory_date >= '" . $data["date_from"] . "' ";
      }
      if (isset($data["date_to"]) && $data["date_to"] != "") {
      $condition_1 = $condition_1 . " AND
      inventory.inventory_date <='" . $data["date_to"] . "' ";
      $condition_2 = $condition_2 . " AND
      inventory.inventory_date <='" . $data["date_to"] . "' ";
      }
      $query_1 = "SELECT
      inventory.inventory_date AS date,
      inventory.order_id
      FROM
      inventory
      LEFT JOIN
      input_order
      ON
      inventory.order_id = input_order.input_order_id
      LEFT JOIN
      partner
      ON
      input_order.partner_id = partner.partner_id
      LEFT JOIN
      product
      ON
      inventory.product_id = product.product_id
      LEFT JOIN
      unit
      ON
      product.unit_id = unit.unit_id "
      . $condition_1 . "
      GROUP BY
      inventory.order_id
      ORDER BY
      inventory.order_id DESC";
      //        echo $query_1;
      $this->db->query($query_1);
      $t1 = $this->db->getResult();
      $s1 = sizeof($t1);
      if ($s1 != 0) {
      for ($i = 0; $i < $s1; $i++) {
      $q1 = "SELECT
      SUM(inventory.amount) AS amount,
      input_order.input_code,
      inventory.order_id,
      inventory.product_id
      FROM
      inventory
      LEFT JOIN
      input_order
      ON
      inventory.order_id = input_order.input_order_id
      WHERE
      inventory.order_id = " . $t1[$i]["order_id"] . "
      GROUP BY
      inventory.order_id";
      //            echo $q1;
      $this->db->query($q1);
      $t2 = $this->db->getResult();
      $t1[$i]["import_code"] = $t2[0]["input_code"];
      $t1[$i]["import_amount"] = $t2[0]["amount"];
      $t1[$i]["export_code"] = '';
      $t1[$i]["export_amount"] = '';

      $p1 = "SELECT
      inventory.product_id,
      product.product_name,
      product.product_code,
      product_category.category_id AS product_category_id,
      category.category_name AS product_category_name,
      unit.unit_name AS product_unit_name,
      inventory.amount AS product_amount
      FROM
      inventory
      LEFT JOIN
      product
      ON
      inventory.product_id = product.product_id
      LEFT JOIN
      product_category
      ON
      inventory.product_id = product_category.product_id
      LEFT JOIN
      category
      ON
      product_category.category_id = category.category_id
      LEFT JOIN
      unit
      ON
      product.unit_id = unit.unit_id
      WHERE
      inventory.status !='1'
      AND
      inventory.amount > 0
      AND
      inventory.order_id = " . $t1[$i]["order_id"] . "
      GROUP BY
      inventory.product_id
      ORDER BY
      inventory.product_id ASC";
      //            echo $p1;
      $this->db->query($p1);
      $rp1 = $this->db->getResult();
      $t1[$i]["product"] = $rp1;
      }
      }
      //        return $t1;

      $query_2 = "SELECT
      inventory.inventory_date AS date,
      inventory.order_id
      FROM
      inventory LEFT JOIN
      input_order
      ON
      inventory.order_id = input_order.input_order_id
      LEFT JOIN
      partner
      ON
      input_order.partner_id = partner.partner_id
      LEFT JOIN
      product
      ON
      inventory.product_id = product.product_id
      LEFT JOIN
      unit
      ON
      product.unit_id = unit.unit_id "
      . $condition_2 . "
      GROUP BY
      inventory.order_id
      ORDER  BY
      inventory.order_id DESC";
      $this->db->query($query_2);
      $t3 = $this->db->getResult();
      $s3 = sizeof($t3);
      if ($s3 != 0) {
      for ($i = 0; $i < $s3; $i++) {
      $q2 = "SELECT
      SUM(inventory.amount) AS amount,
      output_order.order_code,
      inventory.order_id,
      inventory.product_id
      FROM
      inventory
      LEFT JOIN
      output_order
      ON
      inventory.order_id = output_order.order_id
      WHERE
      inventory.order_id = " . $t3[$i]["order_id"] . "
      GROUP BY
      inventory.order_id";
      $this->db->query($q2);
      $t4 = $this->db->getResult();
      $t3[$i]["export_code"] = $t4[0]["order_code"];
      $t3[$i]["export_amount"] = $t4[0]["amount"];
      $t3[$i]["import_amount"] = '';
      $t3[$i]["import_code"] = '';

      $p2 = "SELECT
      inventory.product_id,
      product.product_name,
      product.product_code,
      product_category.category_id AS product_category_id,
      category.category_name AS product_category_name,
      unit.unit_name AS product_unit_name,
      inventory.amount AS product_amount
      FROM
      inventory
      LEFT JOIN
      product
      ON
      inventory.product_id = product.product_id
      LEFT JOIN
      product_category
      ON
      inventory.product_id = product_category.product_id
      LEFT JOIN
      category
      ON
      product_category.category_id = category.category_id
      LEFT JOIN
      unit
      ON
      product.unit_id = unit.unit_id
      WHERE
      inventory.status !='1'
      AND
      inventory.amount < 0
      AND
      inventory.order_id = " . $t3[$i]["order_id"] . "
      GROUP BY
      inventory.product_id
      ORDER BY
      inventory.product_id ASC";
      //            echo $p1;
      $this->db->query($p2);
      $rp2 = $this->db->getResult();
      $t3[$i]["product"] = $rp2;
      }
      }
      //        for ($i = 0; $i < sizeof($t1); $i++) {
      //            $q2 = "SELECT
      //                    inventory.product_id
      //                   FROM
      //                    inventory
      //                   WHERE
      //                    inventory.inventory_date = " . $t1[$i]["date"] . "
      //                   AND
      //                    inventory.order_id = " . $t1[$i]["order_id"];
      ////            echo $q2;
      //            $this->db->query($q2);
      //            $t2 = $this->db->getResult();
      //            $t1[$i]["product_id"] = $t2;
      //        }
      //        return $t3;
      //return $t1;
      $result = array_merge($t1, $t3);
      $count = sizeof($result);
      $output["status"] = 1;
      $output["erros"] = NULL;
      $output["message"] = NULL;
      $output["data"] = array(
      "numer_row_total" => $count,
      "number_row_show" => $count,
      "item_list" => $result
      );
      return $output; */

    public function statisticsImportExport($data)
    {
        $condition_1 = "WHERE
                inventory.status != '1' ";
        $warehouse = " ";
        if (isset($data["product_id"]) && $data["product_id"] != '') {
            $condition_1 = $condition_1 . "AND
                inventory.product_id = '" . $data["product_id"] . "' ";
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != '') {
            $condition_1 = $condition_1 . "AND
                inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
            $warehouse = $warehouse . "AND
                inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
        }
        if (isset($data["partner_id"]) && $data["partner_id"] != "") {
            $condition_1 = $condition_1 . " AND
					input_order.partner_id = '" . $data["partner_id"] . "' ";
        }
        if (isset($data["warehouse_id"]) && $data["warehouse_id"] != "") {
            $dataUp["warehouse_id"] = $data["warehouse_id"];
            $condition_1 = $condition_1 . " AND
					inventory.warehouse_id = '" . $data["warehouse_id"] . "' ";
        }
        if (isset($data["date_from"]) && $data["date_from"] != "") {
            $dataUp["time_start"] = $data["date_from"];
            $condition_1 = $condition_1 . " AND
					inventory.inventory_date >= '" . $data["date_from"] . "' ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != "") {
            $dataUp["time_end"] = $data["date_to"];
            $condition_1 = $condition_1 . " AND
					inventory.inventory_date <='" . $data["date_to"] . "' ";
        }
        $query2 = "SELECT
                    inventory.product_id,
                    product.product_code,
                    product.product_name,
                    unit.unit_name
                   FROM
                    inventory
                   LEFT JOIN
                    product_category
                   ON
                    inventory.product_id = product_category.product_id
                   LEFT JOIN
                    category
                   ON
                    product_category.category_id = category.category_id
                   LEFT JOIN
                    product
                   ON
                    inventory.product_id = product.product_id
                   LEFT JOIN
                    unit
                   ON
                    product.unit_id = unit.unit_id
                    " . $condition_1 . "
                   GROUP BY
                    inventory.product_id";
        $this->db->query($query2);
        $cate = $this->db->getResult();
//        echo $query2;
        // $out = array();
        // for ($i = 0; $i < sizeof($cate); $i++) {
        //     $dataUp["product_id"][$i] = $cate[$i]["product_id"];
        //     $this->httpRequest->setData(array("data_post" => array(
        //             "product_id" => $dataUp["product_id"][$i])));
        //     $out[$i] = json_decode($this->httpRequest->send());
        //     $out[$i] = $out[$i]->data->item_list[0];
        //     $out[$i]->product_code = $cate[$i]["product_code"];
        //     $out[$i]->product_name = $cate[$i]["product_name"];
        //     $out[$i]->unit_name = $cate[$i]["unit_name"];
        // }
        $size = sizeof($cate);
        $all_p = 0;
        for ($i = 0; $i < $size; $i++) {
            $query_3 = "SELECT
                        inventory.opening_stock,
                        inventory.amount,
                        inventory.inventory_amount,
                        inventory.create_time,
                        inventory.description,
                        warehouse.warehouse_name
                      FROM
                        inventory
                      LEFT JOIN
                        warehouse
                      ON
                        inventory.warehouse_id = warehouse.warehouse_id
                      WHERE
                        inventory.product_id = " . $cate[$i]['product_id'] . $warehouse . "
                      ORDER BY
                        inventory.inventory_id ASC";
            $this->db->query($query_3);
            $import = array();
            $import = $this->db->getResult();
            $si = sizeof($import);
            for ($j = 0; $j < $si; $j++) {
                if ($import[$j]['amount'] > 0) {
                    $cate[$i]['import'][$j]['opening_stock'] = $import[$j]['opening_stock'];
                    $cate[$i]['import'][$j]['input_amount'] = $import[$j]['amount'];
                    $cate[$i]['import'][$j]['output_amount'] = NULL;
                    $cate[$i]['import'][$j]['inventory_amount'] = $import[$j]['inventory_amount'];
                    $cate[$i]['import'][$j]['warehouse_name'] = $import[$j]['warehouse_name'];
                    $cate[$i]['import'][$j]['description'] = $import[$j]['description'];
                } else {
                    $cate[$i]['import'][$j]['opening_stock'] = $import[$j]['opening_stock'];
                    $cate[$i]['import'][$j]['input_amount'] = NULL;
                    $cate[$i]['import'][$j]['output_amount'] = $import[$j]['amount'];
                    $cate[$i]['import'][$j]['inventory_amount'] = $import[$j]['inventory_amount'];
                    $cate[$i]['import'][$j]['warehouse_name'] = $import[$j]['warehouse_name'];
                    $cate[$i]['import'][$j]['description'] = $import[$j]['description'];
                }
            }
            $query_4 = "SELECT
                            inventory.opening_stock,
                            inventory.amount,
                            inventory.inventory_amount,
                            inventory.create_time,
                            inventory.description,
                            warehouse.warehouse_name
                          FROM
                            inventory
                          LEFT JOIN
                            warehouse
                          ON
                            inventory.warehouse_id = warehouse.warehouse_id
                          WHERE
                            inventory.product_id = " . $cate[$i]['product_id'] . $warehouse . "
                          GROUP BY
                            inventory.warehouse_id
                          ORDER BY
                            inventory.inventory_id ASC";
            $this->db->query($query_4);
            $all = $this->db->getResult();
            $sa = sizeof($all);
            for ($j = 0; $j < $sa; $j++) {
                $all_p = $all_p + $all[$j]["opening_stock"];
            }
            $cate[$i]['all']['opening_stock'] = $all_p;
            $query_5 = "SELECT
                            inventory.inventory_id,
                            inventory.amount,
                            inventory.description
                          FROM
                            inventory
                          LEFT JOIN
                            warehouse
                          ON
                            inventory.warehouse_id = warehouse.warehouse_id
                          WHERE
                            inventory.product_id = " . $cate[$i]['product_id'] . $warehouse . "
                          AND
                            inventory.description != 'Chuyển kho'
                          ORDER BY
                            inventory.inventory_id ASC";
            $this->db->query($query_5);
            $inventory = $this->db->getResult();
            $size_in = sizeof($inventory);
            for ($j = 0; $j < $size_in; $j++) {
                if ($inventory[$j]['amount'] > 0) {
                    $cate[$i]['all']['order'][$j]['inventory_id'] = $inventory[$j]['inventory_id'];
                    $cate[$i]['all']['order'][$j]['input_amount'] = $inventory[$j]['amount'];
                    $cate[$i]['all']['order'][$j]['output_amount'] = NULL;
                    $cate[$i]['all']['order'][$j]['description'] = $inventory[$j]['description'];
                } else {
                    $cate[$i]['all']['order'][$j]['inventory_id'] = $inventory[$j]['inventory_id'];
                    $cate[$i]['all']['order'][$j]['input_amount'] = NULL;
                    $cate[$i]['all']['order'][$j]['output_amount'] = $inventory[$j]['amount'];
                    $cate[$i]['all']['order'][$j]['description'] = $inventory[$j]['description'];
                }
            }
            $query_6 = " SELECT
                            inventory.inventory_id,
                            inventory.amount,
                            inventory.description
                        FROM
                            inventory
                        LEFT JOIN
                            warehouse
                        ON
                            inventory.warehouse_id = warehouse.warehouse_id
                        WHERE
                            inventory.product_id = " . $cate[$i]['product_id'] . $warehouse . "
                        AND
                            inventory.amount > 0
                        AND
                            inventory.description = 'Chuyển kho'
                        GROUP BY
                            inventory.create_time";
            $this->db->query($query_6);
            $tranfer = $this->db->getResult();
            $size_tran = sizeof($tranfer);
            for ($j = 0; $j < $size_tran; $j++) {
                $tranfer['order'][$j]['inventory_id'] = $tranfer[$j]['inventory_id'];
                $tranfer['order'][$j]['input_amount'] = $tranfer[$j]['amount'];
                $tranfer['order'][$j]['output_amount'] = -$tranfer[$j]['amount'];
                $tranfer['order'][$j]['description'] = $tranfer[$j]['description'];
                array_push($cate[$i]['all']['order'], $tranfer['order'][$j]);
            }
            $size_all = sizeof($cate[$i]['all']['order']);
            for ($j = 0; $j < ($size_all - 1); $j++) {
                for ($k = $j + 1; $k < $size_all; $k++) {
                    if ($cate[$i]['all']['order'][$j]["inventory_id"] > $cate[$i]['all']['order'][$k]["inventory_id"]) {
                        $temp = $cate[$i]['all']['order'][$j];
                        $cate[$i]['all']['order'][$j] = $cate[$i]['all']['order'][$k];
                        $cate[$i]['all']['order'][$k] = $temp;
                    }
                }
            }
        }
        return $cate;
    }

    public function productInventoryAllWarehouse($data = array())
    {
        //Condition
        $condition = "WHERE
                        inventory.status !=1
                      AND
                        inventory.product_id =" . $data["product_id"] . " ";
        if (isset($data["date_from"]) && $data["date_from"] != '') {
            $condition = $condition . "AND
                        inventory.create_time >=" . $data["date_from"] . " ";
        } else {
            $condition = $condition . "AND
                        inventory.create_time >= '0' ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != '') {
            $condition = $condition . "AND
                        inventory.create_time <=" . $data["date_to"] . " ";
        } else {
            $data["date_to"] = time();
            $condition = $condition . "AND
                        inventory.create_time <=" . $data["date_to"] . " ";
        }

        //Query opening_stock(Tồn đầu kỳ)
        $query_opening_stock = "SELECT
                                inventory.product_id,
                                SUM(inventory.opening_stock) AS opening_stock
                               FROM
                                inventory
                               " . $condition . "
                               GROUP BY
                                inventory.create_time
                               ORDER BY
                                inventory.inventory_id ASC";
//        echo $query_opening_stock;
        $this->db->query($query_opening_stock);
        $opening_stock = $this->db->getResult();

        //Query amount_change (các biến động trong kỳ, tính tất cả xuất kho, nhập kho, chuyển kho, hàng trả về)
        $query_amount_change = "SELECT
                                    SUM(inventory.amount) AS amount
                                FROM
                                    inventory
                                " . $condition;
        $this->db->query($query_amount_change);
        $amount_change = $this->db->getResult();

        //Query inventory_amount(Tồn cuối kỳ của tất cả các kho)
        $query_inventory_amount = "SELECT
                                        inventory.product_id,
                                        SUM(inventory.inventory_amount) AS inventory_amount
                                    FROM
                                        inventory
                                    " . $condition . "
                                    GROUP BY
                                        inventory.create_time
                                    ORDER BY
                                        inventory.inventory_id DESC";
//        echo $query_inventory_amount;
        $this->db->query($query_inventory_amount);
        $inventory_amount = $this->db->getResult();

        //$out is result(kết quả trả về)
        $out['product_id'] = $data["product_id"];
        $out["opening_stock"] = $opening_stock[0]['opening_stock'];
        $out["amount_change"] = $amount_change[0]['amount'];
        $out["inventory_amount"] = $inventory_amount[0]['inventory_amount'];
        return $out;
    }

    public function productInventoryWarehouse($data = array())
    {
        //Condition
        $condition = "WHERE
                        inventory.status !=1
                      AND
                        inventory.product_id =" . $data["product_id"] . " ";
        if (isset($data["date_from"]) && $data["date_from"] != '') {
            $condition = $condition . "AND
                        inventory.create_time >=" . $data["date_from"] . " ";
        } else {
            $condition = $condition . "AND
                        inventory.create_time >= '0' ";
        }
        if (isset($data["date_to"]) && $data["date_to"] != '') {
            $condition = $condition . "AND
                        inventory.create_time <=" . $data["date_to"] . " ";
        } else {
            $data["date_to"] = time();
            $condition = $condition . "AND
                        inventory.create_time <=" . $data["date_to"] . " ";
        }

        //Query inventory_amount(Tồn cuối kỳ của tất cả các kho)
        $query_inventory_amount = "SELECT
                                        inventory.product_id,
                                        SUM(inventory.inventory_amount) AS inventory_amount
                                    FROM
                                        inventory
                                    " . $condition . "
                                    GROUP BY
                                        inventory.create_time
                                    ORDER BY
                                        inventory.inventory_id DESC";
        echo $query_inventory_amount;
        $this->db->query($query_inventory_amount);
        $inventory_amount = $this->db->getResult();

        //Query có bao nhiêu kho có sản phẩm đấy
        $query_warehouse = "SELECT DISTINCT
                                inventory.warehouse_id
                            FROM
                                inventory
                            " . $condition . "
                            AND
                                inventory.inventory_amount > 0";
//        echo $query_warehouse;
        $this->db->query($query_warehouse);
        $warehouse = $this->db->getResult();
        $size = sizeof($warehouse);

        //$out is result(kết quả trả về)
        $out['product_id'] = $data["product_id"];
        $out["inventory_amount"] = $inventory_amount[0]['inventory_amount'];

        //Query inventory_amount_warehouse(Tồn cuối kỳ của 1 kho)
        for ($i = 0; $i < $size; $i++) {
            $query_inventory_amount_warehouse = "SELECT
                                                    inventory.product_id,
                                                    inventory.inventory_amount AS inventory_amount_warehouse,
                                                    inventory.warehouse_id
                                                FROM
                                                    inventory
                                                " . $condition . "
                                                AND
                                                    inventory.warehouse_id = " . $warehouse[$i]['warehouse_id'] . "
                                                ORDER BY
                                                    inventory.inventory_id DESC";
//        echo $query_inventory_amount_warehouse;
            $this->db->query($query_inventory_amount_warehouse);
            $inventory_amount_warehouse = $this->db->getResult();
            $out['warehouse'][$i] = $inventory_amount_warehouse[0];
        }

        return $out;
    }

}
