<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MPayType extends Model {

    /**
     * 
     */
    public function __construct() {
        parent::__construct();
        $this->db->connect();
    }

    /**
     * 
     * @param unknown $data
     */
    public function getAllPayTypes() {
        $query_2 = "SELECT 
                    account_id AS pay_type_id,
                    account_name AS pay_type_name
                  FROM
                    bank_account
                  WHERE
                    account_status = 5
                  AND
                    status != 1";
        $this->db->query($query_2);
        $account= $this->db->getResult();
        $data["status"] = 1;
        $data["erros"] = NULL;
        $data["message"] = NULL;
        $data["data"] = $account;
        return $data;
    }

}
