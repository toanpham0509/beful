<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
use 			BKFW\Libraries\HttpRequest;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MImportWarehouse extends Model {
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->httpRequest = new HttpRequest();
		$this->httpRequest->setMethod( "POST" );
		
		$this->db->connect();
	}
	/**
	 * Get import warehouse
	 * 
	 * @param string $dataSearch
	 */
	public function getImportWarehouses( $dataSearch = null ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=get_imports" );
		if( $dataSearch != null ) 
			$this->httpRequest->setData( array( "data_post" => $dataSearch ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data;
	}
	/**
	 * Get import
	 * 
	 * @param unknown $importID
	 */
	public function getImportWarehouse( $importID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=get_import" );
		$this->httpRequest->setData( array( "data_post" => array( "import_id" => $importID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->data[ 0 ];
	}
	/**
	 * new import
	 * 
	 * @param unknown $dataInsert
	 */
	public function newImportWarehouse( $dataInsert ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=new_import" );
		$this->httpRequest->setData( array( "data_post" => $dataInsert ) );
		$data = json_decode( $this->httpRequest->send() );
		print_r($data);
		if( isset( $data->data->import_id ) )
			return $data->data->import_id;
		return 0;
	}
	/**
	 * 
	 * @param unknown $importID
	 * @param unknown $dataUpdate
	 */
	public function updateImportWarehouse( $importID, $dataUpdate ) {
		$dataUpdate[ 'import_id' ] = $importID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=update_import" );
		$this->httpRequest->setData( array( "data_post" => $dataUpdate ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 * @param unknown $importID
	 */
	public function deleteImportWarehouse( $importID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=delete_import" );
		$this->httpRequest->setData( array( "data_post" => array( "import_id" => $importID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * New import line
	 * 
	 * @param unknown $data
	 */
	public function newImportLine( $data ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=new_import_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 */
	public function getImportLines( $importID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=get_import_lines" );
		$this->httpRequest->setData( array( "data_post" => array( "input_order_id" => $importID ) ) );
		$data = json_decode( $this->httpRequest->send() );
		if( isset( $data->data->item_list ) )
			return $data->data->item_list;
		return null;
	}
	/**
	 * 
	 * @param unknown $importLineID
	 */
	public function deleteImportLine( $importLineID ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=delete_import_line" );
		$this->httpRequest->setData( array( "data_post" => array( "input_line_id" => $importLineID ) ) );
		$data = json_decode( $this->httpRequest->send() );

		return $data->status;
	}
	/**
	 * 
	 * @param unknown $importLineID
	 * @param unknown $data
	 */
	public function updateImportLine( $importLineID, $data ) {
		$data[ 'input_line_id' ] = $importLineID;
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_lines&action=update_import_line" );
		$this->httpRequest->setData( array( "data_post" => $data ) );
		$data = json_decode( $this->httpRequest->send() );
		return $data->status;
	}
	/**
	 * 
	 * @param unknown $importId
	 */
	public function inspectionImport( $importId ) {
		$this->httpRequest->setServer( SERVER_API . "index.php?controller=input_order&action=inspection_order" );
		$this->httpRequest->setData( array( "data_post" => array( "input_order_id" => $importId ) ) );
// 		$data = $this->httpRequest->send();
		
		$data = json_decode( $this->httpRequest->send() );
// 		print_r( $data );
		return $data->status;
	}
	/**
	 * Lấy danh sách các lần nhập kho còn tồn trong kho tương ứng với sản phẩm và kho ở input
	 * 
	 * @param unknown $productId
	 */
	public function getImportsInStockByProduct($productId, $warehouseId) {
		$this->db->from("input_order");
		$this->db->where("line.input_order_id = input_order.input_order_id");
		$this->db->where("line.input_line_amount > line.buy_amount");
		$this->db->where("input_order.status", 3);
		$this->db->where("input_order.status_id IN (14, 5, 4, 17)");
		$this->db->where("line.product_id", $productId);
		$this->db->where("line.warehouse_id", $warehouseId);
		$this->db->order("input_order.last_update");
		return $this->db->get("input_lines as line");
	}
	/**
	 * Update import line
	 * 
	 * @param unknown $importId
	 * @param unknown $key
	 * @param unknown $value
	 */
	public function updateImportLineCustom($importLineId, $dataUpdate) {
		$this->db->where("input_line_id", $importLineId);
		$this->db->setItem($dataUpdate);
		return $this->db->update("input_lines");
	}

	/**
	 * Get import line by import id
	 *
	 * @param $importLineId
	 * @return mixed
	 */
	public function getImportLine($importLineId) {
		$this->db->where("input_line_id", $importLineId);
		return $this->db->get("input_lines");
	}

	/**
	 * Insert new record
	 *
	 * @param $data
	 * @return mixed
	 */
	public function newImportLineCustom($data) {
		$this->db->setItem($data);
		return $this->db->insert("input_lines");
	}

	/**
	 * @param $importLineId
	 * @return mixed
	 */
	public function deleteImportLineCus($importLineId) {
		$this->db->where("input_line_id", $importLineId);
		return $this->db->delete("input_lines");
	}
}
/*end of file MImportWarehouse.class.php*/
/*location: MImportWarehouse.class.php*/