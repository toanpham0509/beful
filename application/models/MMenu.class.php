<?php
namespace 		App\Models;
use 			BKFW\Bootstraps\Model;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );
class MMenu extends Model {
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->db->connect();
	}
	/**
	 * 
	 * @param number $parentMenuID
	 */
	public function getMenus( $parentMenuID = 0 ) {
		$this->db->where( "parent_id", $parentMenuID );
		$menus = $this->db->get( "menu" );
		if( $parentMenuID > 0 ) :
			foreach( $menus as $menu ) {
				$menuItem = $this->getMenus( $menu[ 'id' ] );
				if( $menuItem != null ) {
					$menus[ 'item_' . $menu[ 'id' ] ] = $menuItem;
				}
			}
		endif;
		if( empty( $menus ) ) {
			return null; 
		}
		return $menus;
	}
	/**
	 * 
	 * @param unknown $menuID
	 * @return NULL|unknown
	 */
	public function getMenu( $menuID ) {
		$this->db->where( "id", $menuID );
		$result = $this->db->get( "menu" );
		if( empty( $result ) ) {
			return null; 
		}
		return $result[ 0 ];
	}
}
/*end of file MMenu.class.php*/
/*location: MMenu.class.php*/