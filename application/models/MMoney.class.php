<?php
namespace 			App\Models;
use 				BKFW\Bootstraps\Model;
/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if( !defined ( 'BOOTSTRAP_PATH' ) ) exit( "No direct script access allowed" );

/**
 * Class MMoney
 * @package App\Models
 */
class MMoney {
	public function __construct() {
	}
	public function addDotToMoney( $money ) {
		$money .= "";
		$stringReturn = "";
		$j = -1;
		$length = strlen( $money );
		for( $i = $length - 1; $i >= 0 ; $i-- ) {
			$j++;
			if( $j % 3 == 0 && $j > 0 ) {
				$stringReturn .= "." . $money[ $i ];
			} else {
				$stringReturn .= $money[ $i ];
			}
		}
		return strrev( $stringReturn );
	}
}
/*end of file MMoney.class.php*/
/*location: MMoney.class.php*/