<?php

namespace App\Models;

use BKFW\Bootstraps\Model;
use BKFW\Libraries\HttpRequest;

/**
 * BK-Framework
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 */
if (!defined('BOOTSTRAP_PATH'))
    exit("No direct script access allowed");

class MAccounting extends Model {

    public function __construct() {
        parent::__construct();
        $this->loadModel("MSaleOrder");
        $this->db->connect();
    }

    public function getCustomerPays($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];

        $this->db->select("order_id");
        $this->db->where(array(
            "status_id" => 3,
            "status" => 3,
            "pay_status_id" => 2
        ));
        $orders = array_reverse($this->db->get("output_order"));
        $size = sizeof($orders);
        $return = array();
        $i = 0;
        $last = $page + $data['per_page_number'];
        foreach($orders as $order) {
            if($i < $page ) {
                $i++;
                continue;
            }
            if($i > $last) {
                break;
            }
            $dataUnpaid = $this->mSaleOrder->getOrderUnpaid($order["order_id"]);
            if(isset($dataUnpaid["data"][0]["have_to_pay"])
                && $dataUnpaid["data"][0]["have_to_pay"] > 0
                && $i >= $page
                && $i < $page + $data["per_page_number"]
            ) {
                $return[$i] = $this->mSaleOrder->getOrder ($dataUnpaid["data"][0]["order_id"]);
                unset($return[$i]->order_lines);
                $return[$i]->orderId = $dataUnpaid["data"][0]["order_id"];
                $return[$i]->newPrice = (100 - $dataUnpaid["data"][0]["discount"]) * $dataUnpaid["data"][0]["order_price"] / 100;
                $return[$i]->have_to_pay = $dataUnpaid["data"][0]["have_to_pay"];
                $return[$i]->amount_paid = $dataUnpaid["data"][0]["amount_paid"];
            }
            $i++;
        }

        return array(
            "status" => 1,
            "errors" => array(),
            "message" => "",
            "data" => array(
                "numer_row_total" => $size,
                "number_row_show" => $data["per_page_number"],
                "item_list" => (array) $return
            )
        );

        /*
        $condition = "WHERE customer_pay.status !='1' AND customer_pay.pay_status_id !='1' AND customer_pay.pay_status_id !='0' ";
        if (isset($data["customer_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["customer_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            output_order.order_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            customer_pay.pay_type_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["pay_status_id"])) {
            $condition = $condition . "AND
            customer_pay.pay_status_id LIKE '%" . $data["pay_status_id"] . "%'";
        }
        if (isset($data["date_from"])) {
            $condition = $condition . "AND
            customer_pay.pay_date >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"])) {
            $condition = $condition . "AND
            customer_pay.pay_date <=" . $data["date_to"] . " ";
        }
        $query = "SELECT
                    customer_pay.pay_id,
                    customer_pay.pay_code,
                    customer_pay.pay_type_id,
                    customer_pay.pay_status_id,
                    pay_line.order_id,
                    output_order.order_code,
                    output_order.order_date,
                    customer_pay.customer_id,
                    partner.partner_name,
                    customer_pay.pay_money,
                    pay_line.amount_paid AS total_price,
                    bank_account.account_name AS pay_type_name,
                    pay_status.pay_status_name AS status
                  FROM
                    customer_pay
                  LEFT JOIN
                    pay_line
                  ON
                    customer_pay.pay_id = pay_line.pay_id
                  LEFT JOIN
                    output_order
                  ON 
                    pay_line.order_id = output_order.order_id
                  LEFT JOIN
                    partner
                  ON
                    customer_pay.customer_id = partner.partner_id
                  LEFT JOIN
                    bank_account
                  ON
                    customer_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN 
                    pay_status
                  ON
                    customer_pay.pay_status_id =  pay_status.pay_status_id
                  " . $condition . "
                  GROUP BY
                    pay_line.order_id
                  ORDER BY
                    customer_pay.pay_id DESC
                  LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
        */
    }

    public function getCustomerPays_old($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE customer_pay.status !='1' AND customer_pay.pay_status_id !='1' AND customer_pay.pay_status_id !='0' ";
        if (isset($data["customer_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["customer_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            output_order.order_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            customer_pay.pay_type_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["pay_status_id"])) {
            $condition = $condition . "AND
            customer_pay.pay_status_id LIKE '%" . $data["pay_status_id"] . "%'";
        }
        if (isset($data["date_from"])) {
            $condition = $condition . "AND
            customer_pay.pay_date >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"])) {
            $condition = $condition . "AND
            customer_pay.pay_date <=" . $data["date_to"] . " ";
        }
        $query = "SELECT
                    customer_pay.pay_id,
                    customer_pay.pay_code,
                    customer_pay.pay_type_id,
                    customer_pay.pay_status_id,
                    pay_line.order_id,
                    output_order.order_code,
                    output_order.order_date,
                    customer_pay.customer_id,
                    partner.partner_name,
                    customer_pay.pay_money,
                    pay_line.amount_paid AS total_price,
                    bank_account.account_name AS pay_type_name,
                    pay_status.pay_status_name AS status
                  FROM
                    customer_pay
                  LEFT JOIN
                    pay_line
                  ON
                    customer_pay.pay_id = pay_line.pay_id
                  LEFT JOIN
                    output_order
                  ON
                    pay_line.order_id = output_order.order_id
                  LEFT JOIN
                    partner
                  ON
                    customer_pay.customer_id = partner.partner_id
                  LEFT JOIN
                    bank_account
                  ON
                    customer_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN
                    pay_status
                  ON
                    customer_pay.pay_status_id =  pay_status.pay_status_id
                  " . $condition . "
                  GROUP BY
                    pay_line.order_id
                  ORDER BY
                    customer_pay.pay_id DESC
                  LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getSupplierPays($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];

        $condition = "WHERE supplier_pay.status != '1'
        AND supplier_pay.pay_status_id != '1'
        AND supplier_pay.pay_status_id != '0' ";
        if (isset($data["supplier_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["supplier_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            output_order.order_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            supplier_pay.pay_type_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["pay_status_id"])) {
            $condition = $condition . "AND
            supplier_pay.pay_status_id LIKE '%" . $data["pay_status_id"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        if (isset($data["date_from"])) {
            $condition = $condition . "AND
            supplier_pay.create_time >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"])) {
            $condition = $condition . "AND
            supplier_pay.create_time <=" . $data["date_to"] . " ";
        }
        $query = "SELECT
                    supplier_pay.pay_id,
                    supplier_pay.pay_code,
                    supplier_pay.pay_type_id,
                    supplier_pay.pay_status_id,
                    supplier_pay_line.order_id,
                    input_order.input_code,
                    input_order.input_date,
                    supplier_pay.supplier_id,
                    partner.partner_name,
                    supplier_pay.pay_date,
                    supplier_pay.pay_money ,
                    supplier_pay_line.amount_paid AS total_price,
                    bank_account.account_name AS pay_type_name,
                    pay_status.pay_status_name
                  FROM
                    supplier_pay
                  LEFT JOIN
                    supplier_pay_line
                  ON
                    supplier_pay.pay_id = supplier_pay_line.pay_id
                  LEFT JOIN
                    input_order
                  ON 
                    supplier_pay_line.order_id = input_order.input_order_id
                  LEFT JOIN
                    partner
                  ON
                    supplier_pay.supplier_id = partner.partner_id
                  LEFT JOIN
                    bank_account
                  ON
                    supplier_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN 
                    pay_status
                  ON
                    supplier_pay.pay_status_id =  pay_status.pay_status_id
                  " . $condition . "
                  GROUP BY
                    supplier_pay_line.order_id
                  ORDER BY
                    supplier_pay.pay_id DESC
                  LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function getSupplierPays_old($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition = "WHERE supplier_pay.status != '1' AND supplier_pay.pay_status_id != '1' AND supplier_pay.pay_status_id != '0' ";
        if (isset($data["supplier_name"])) {
            $condition = $condition . "AND
            partner.partner_name LIKE '%" . $data["supplier_name"] . "%'";
        }
        if (isset($data["order_code"])) {
            $condition = $condition . "AND
            output_order.order_code LIKE '%" . $data["order_code"] . "%'";
        }
        if (isset($data["pay_type_id"])) {
            $condition = $condition . "AND
            supplier_pay.pay_type_id LIKE '%" . $data["pay_type_id"] . "%'";
        }
        if (isset($data["pay_status_id"])) {
            $condition = $condition . "AND
            supplier_pay.pay_status_id LIKE '%" . $data["pay_status_id"] . "%'";
        }
        if (isset($data["status"])) {
            $condition = $condition . "AND
            pay_status.pay_status_name LIKE '%" . $data["status"] . "%'";
        }
        if (isset($data["date_from"])) {
            $condition = $condition . "AND
            supplier_pay.create_time >=" . $data["date_from"] . " ";
        }
        if (isset($data["date_to"])) {
            $condition = $condition . "AND
            supplier_pay.create_time <=" . $data["date_to"] . " ";
        }
        $query = "SELECT
                    supplier_pay.pay_id,
                    supplier_pay.pay_code,
                    supplier_pay.pay_type_id,
                    supplier_pay.pay_status_id,
                    supplier_pay_line.order_id,
                    input_order.input_code,
                    input_order.input_date,
                    supplier_pay.supplier_id,
                    partner.partner_name,
                    supplier_pay.pay_date,
                    supplier_pay.pay_money ,
                    supplier_pay_line.amount_paid AS total_price,
                    bank_account.account_name AS pay_type_name,
                    pay_status.pay_status_name
                  FROM
                    supplier_pay
                  LEFT JOIN
                    supplier_pay_line
                  ON
                    supplier_pay.pay_id = supplier_pay_line.pay_id
                  LEFT JOIN
                    input_order
                  ON
                    supplier_pay_line.order_id = input_order.input_order_id
                  LEFT JOIN
                    partner
                  ON
                    supplier_pay.supplier_id = partner.partner_id
                  LEFT JOIN
                    bank_account
                  ON
                    supplier_pay.pay_type_id = bank_account.account_id
                  LEFT JOIN
                    pay_status
                  ON
                    supplier_pay.pay_status_id =  pay_status.pay_status_id
                  " . $condition . "
                  GROUP BY
                    supplier_pay_line.order_id
                  ORDER BY
                    supplier_pay.pay_id DESC
                  LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $count = $this->db->countResult();
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $out
        );
        return $output;
    }

    public function account($data) {
        $page = ($data["limit_number_start"] - 1) * $data["per_page_number"];
        $condition_expense = "WHERE
                    expense.status != '1' ";
        $condition_receipt = "WHERE
                    receipt.status != '1' ";
        $condition_supplier = "WHERE
                    supplier_pay.status != '1' ";
        $condition_customer = "WHERE
                    customer_pay.status != '1' ";
//        $condition_customer = "WHERE
//                    customer_pay.status !='1' ";
//        $condition_supplier = "WHERE
//                    supplier_pay.status !='1' ";
        if (isset($data["date_from"])) {
            $condition_expense = $condition_expense . "AND
                expense.expense_date >= " . $data["date_from"] . " ";
            $condition_receipt = $condition_receipt . "AND
                receipt.receipt_date >= " . $data["date_from"] . " ";
            $condition_supplier = $condition_supplier . "AND
                supplier_pay.pay_date>= " . $data["date_from"] . " ";
            $condition_customer = $condition_customer . "AND
                customer_pay.pay_date>= " . $data["date_from"] . " ";
//            $condition_customer = $condition_customer . "AND
//                customer_pay.pay_date >= " . $data["date_from"] . " ";
//            $condition_supplier = $condition_supplier . "AND
//                supplier_pay.pay_date >= " . $data["date_from"] . " ";
        }
//        if (isset($data["money_paid"])) { // nếu lấy tài khoản tiền mặt thì trong mảng có key money_paid và ko có key pay_ty
//            $condition_expense = $condition_expense . "AND
//                expense.pay_type_id = 1 ";
//            $condition_receipt = $condition_receipt . "AND
//                receipt.pay_type_id = 1 ";
////             $condition_customer = $condition_customer . "AND
////                customer_pay.pay_type_id != '1' ";
////            $condition_supplier = $condition_supplier . "AND
////                supplier_pay.pay_type_id != '1' ";
//        }
        if (isset($data["date_to"])) {
            $condition_expense = $condition_expense . "AND
                expense.expense_date <= " . $data["date_to"] . " ";
            $condition_receipt = $condition_receipt . "AND
                receipt.receipt_date <= " . $data["date_to"] . " ";
            $condition_supplier = $condition_supplier . "AND
                supplier_pay.pay_date>= " . $data["date_to"] . " ";
            $condition_customer = $condition_customer . "AND
                customer_pay.pay_date>= " . $data["date_to"] . " ";
//            $condition_customer = $condition_customer . "AND
//                customer_pay.pay_date <= " . $data["date_to"] . " ";
//            $condition_supplier = $condition_supplier . "AND
//                supplier_pay.pay_date <= " . $data["date_to"] . " ";
        }
        if (isset($data["pay_type_id"])) {
            if ($data["pay_type_id"] != "-99") {
                $condition_expense = $condition_expense . "AND
                    expense.pay_type_id = " . $data["pay_type_id"] . "";
                $condition_receipt = $condition_receipt . "AND
                    receipt.pay_type_id = " . $data["pay_type_id"] . "";
                $condition_supplier = $condition_supplier . "AND
                    supplier_pay.pay_type_id = " . $data["pay_type_id"] . "";
                $condition_customer = $condition_customer . "AND
                    customer_pay.pay_type_id = " . $data["pay_type_id"] . "";
            } else {
                $condition_expense = $condition_expense . "AND
                    expense.pay_type_id != 1 AND
                    expense.pay_type_id != 0 ";
                $condition_receipt = $condition_receipt . "AND
                    receipt.pay_type_id != 1 AND
                    receipt.pay_type_id != 0 ";
                $condition_supplier = $condition_supplier . "AND
                    supplier_pay.pay_type_id != 1 AND
                    supplier_pay.pay_type_id != 0 ";
                $condition_customer = $condition_customer . "AND
                    customer_pay.pay_type_id != 1 AND
                    customer_pay.pay_type_id != 0 ";
            }

//            $condition_customer = $condition_customer . "AND
//                customer_pay.pay_type_id = '" . $data["pay_type_id"] . "' ";
//            $condition_supplier = $condition_supplier . "AND
//                supplier_pay.pay_type_id = '" . $data["pay_type_id"] . "' ";
        }
        if (isset($data["pay_status_id"])) {
            $condition_expense = $condition_expense . "AND
                expense.pay_status_id = '" . $data["pay_status_id"] . "' ";
            $condition_receipt = $condition_receipt . "AND
                receipt.pay_status_id = '" . $data["pay_status_id"] . "' ";
            $condition_supplier = $condition_supplier . "AND
                    supplier_pay.pay_status_id = '" . $data["pay_status_id"] . "' ";
            $condition_customer = $condition_customer . "AND
                    customer_pay.pay_status_id = '" . $data["pay_status_id"] . "' ";
//            $condition_customer = $condition_customer . "AND
//                customer_pay.pay_status_id = '" . $data["pay_status_id"] . "' ";
//            $condition_supplier = $condition_supplier . ""
        }
        $query = "SELECT
                        expense.expense_id AS pay_id,
                        expense.expense_code AS pay_code,
                        expense.expense_date AS pay_date,
                        expense.partner_id,
                        partner.partner_name,
                        expense.description,
                        expense.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        expense.expense_status_id AS pay_status_id,
                        pay_status.pay_status_name
                    FROM
                        expense
                    LEFT JOIN
                        expense_line
                    ON
                        expense.expense_id = expense_line.expense_id
                    LEFT JOIN
                        partner
                    ON
                        expense.partner_id = partner.partner_id
                    LEFT JOIN
                        bank_account
                    ON
                        expense.pay_type_id = bank_account.account_id
                    LEFT JOIN
                        pay_status
                    ON
                        expense.expense_status_id = pay_status.pay_status_id
                    " . $condition_expense . "
                    GROUP BY
                        expense.expense_id
                    LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query);
        $out = $this->db->getResult();
        $size_1 = sizeof($out);
        $q1 = array();
        $o = array();
        $q2 = array();
        $o_2 = array();
        for ($i = 0; $i < $size_1; $i++) {
            $q1[] = "SELECT
                        expense_line.expense_id,
                        SUM(expense_line.money) AS total_price
                    FROM
                        expense_line
                    WHERE
                        expense_line.expense_id= " . $out[$i]["pay_id"];
        }
        $size_2 = sizeof($q1);
        for ($i = 0; $i < $size_2; $i++) {
            $this->db->query($q1[$i]);
            $o[] = $this->db->getResult();
        }
        $size_3 = sizeof($o);
        for ($i = 0; $i < $size_3; $i++) {
            $total_price[$i] = $o[$i][0];
        }
        for ($i = 0; $i < $size_1; $i++) {
            $out[$i]["total_price"] = $total_price[$i]["total_price"];
            $out[$i]["type"] = 'expense';
        }
//        $expense = $this->db->getResult();
        $query_2 = "SELECT
                        receipt.receipt_id AS pay_id,
                        receipt.receipt_code AS pay_code,
                        receipt.receipt_date AS pay_date,
                        receipt.partner_id,
                        partner.partner_name,
                        receipt.description,
                        receipt.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        receipt.receipt_status_id AS pay_status_id,
                        pay_status.pay_status_name
                    FROM
                        receipt
                    LEFT JOIN
                        receipt_line
                    ON
                       receipt.receipt_id = receipt_line.receipt_id
                    LEFT JOIN
                        partner
                    ON
                        receipt.partner_id = partner.partner_id
                    LEFT JOIN
                        bank_account
                    ON
                        receipt.pay_type_id = bank_account.account_id
                    LEFT JOIN
                        pay_status
                    ON
                        receipt.receipt_status_id = pay_status.pay_status_id
                    " . $condition_receipt . "
                    GROUP BY
                        receipt.receipt_id
                    LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query_2);
        $out_1 = $this->db->getResult();
        $size_4 = sizeof($out_1);
        for ($i = 0; $i < $size_4; $i++) {
            $q2[] = "SELECT
                        receipt_line.receipt_id,
                        SUM(receipt_line.money) AS total_price
                    FROM
                        receipt_line
                    WHERE
                        receipt_line.receipt_id= " . $out_1[$i]["pay_id"];
        }
        $size_5 = sizeof($q2);
        for ($i = 0; $i < $size_5; $i++) {
            $this->db->query($q2[$i]);
            $o_2[] = $this->db->getResult();
        }
        $size_6 = sizeof($o_2);
        for ($i = 0; $i < $size_6; $i++) {
            $total_price_2[$i] = $o_2[$i][0];
        }
        for ($i = 0; $i < $size_4; $i++) {
            $out_1[$i]["total_price"] = $total_price_2[$i]["total_price"];
            $out_1[$i]["type"] = 'receipt';
        }
//        echo $query_2;
//        $out = array_merge($expense, $receipt);
        //Supplier_pay
        $query_3 = "SELECT
                        supplier_pay.pay_id AS pay_id,
                        supplier_pay.pay_code AS pay_code,
                        supplier_pay.pay_date AS pay_date,
                        supplier_pay.supplier_id AS partner_id,
                        supplier_pay.pay_money AS total_price,
                        partner.partner_name,
                        supplier_pay.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        supplier_pay.pay_status_id AS pay_status_id,
                        pay_status.pay_status_name
                    FROM
                        supplier_pay
                    LEFT JOIN
                        supplier_pay_line
                    ON
                       supplier_pay.pay_id = supplier_pay_line.pay_id
                    LEFT JOIN
                        partner
                    ON
                        supplier_pay.supplier_id = partner.partner_id
                    LEFT JOIN
                        bank_account
                    ON
                        supplier_pay.pay_type_id = bank_account.account_id
                    LEFT JOIN
                        pay_status
                    ON
                        supplier_pay.pay_status_id = pay_status.pay_status_id
                    " . $condition_supplier . "
                    GROUP BY
                        supplier_pay.pay_id
                    LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query_3);
        $out_2 = $this->db->getResult();
        $size_7 = sizeof($out_2);
//        for ($i = 0; $i < $size_7; $i++) {
//            $q2[] = "SELECT
//                        receipt_line.receipt_id,
//                        SUM(receipt_line.money) AS total_price
//                    FROM
//                        receipt_line
//                    WHERE
//                        receipt_line.receipt_id= " . $out_2[$i]["pay_id"];
//        }
//        $size_8 = sizeof($q2);
//        for ($i = 0; $i < $size_5; $i++) {
//            $this->db->query($q2[$i]);
//            $o_2[] = $this->db->getResult();
//        }
//        $size_9 = sizeof($o_2);
//        for ($i = 0; $i < $size_6; $i++) {
//            $total_price_2[$i] = $o_2[$i][0];
//        }
        for ($i = 0; $i < $size_7; $i++) {
            $out_2[$i]["description"] = '';
            $out_2[$i]["type"] = 'supplier_pay';
        }

        //Customer_pay
        $query_4 = "SELECT
                        customer_pay.pay_id AS pay_id,
                        customer_pay.pay_code AS pay_code,
                        customer_pay.pay_date AS pay_date,
                        customer_pay.customer_id AS partner_id,
                        customer_pay.pay_money AS total_price,
                        partner.partner_name,
                        customer_pay.pay_type_id,
                        bank_account.account_name AS pay_type_name,
                        customer_pay.pay_status_id AS pay_status_id,
                        pay_status.pay_status_name
                    FROM
                        customer_pay
                    LEFT JOIN
                        pay_line
                    ON
                       customer_pay.pay_id = pay_line.pay_id
                    LEFT JOIN
                        partner
                    ON
                        customer_pay.customer_id = partner.partner_id
                    LEFT JOIN
                        bank_account
                    ON
                        customer_pay.pay_type_id = bank_account.account_id
                    LEFT JOIN
                        pay_status
                    ON
                        customer_pay.pay_status_id = pay_status.pay_status_id
                    " . $condition_customer . "
                    GROUP BY
                        customer_pay.pay_id
                    LIMIT
                        " . $page . "," . $data["per_page_number"];
        $this->db->query($query_4);
        $out_3 = $this->db->getResult();
        $size_8 = sizeof($out_3);
//        for ($i = 0; $i < $size_7; $i++) {
//            $q2[] = "SELECT
//                        receipt_line.receipt_id,
//                        SUM(receipt_line.money) AS total_price
//                    FROM
//                        receipt_line
//                    WHERE
//                        receipt_line.receipt_id= " . $out_2[$i]["pay_id"];
//        }
//        $size_8 = sizeof($q2);
//        for ($i = 0; $i < $size_5; $i++) {
//            $this->db->query($q2[$i]);
//            $o_2[] = $this->db->getResult();
//        }
//        $size_9 = sizeof($o_2);
//        for ($i = 0; $i < $size_6; $i++) {
//            $total_price_2[$i] = $o_2[$i][0];
//        }
        for ($i = 0; $i < $size_8; $i++) {
            $out_3[$i]["description"] = '';
            $out_3[$i]["type"] = 'customer_pay';
        }


        $result = array_merge($out, $out_1, $out_2, $out_3);
        $count = sizeof($result);
        $output["status"] = 1;
        $output["erros"] = NULL;
        $output["message"] = NULL;
        $output["data"] = array(
            "numer_row_total" => $count,
            "number_row_show" => $count,
            "item_list" => $result
        );
        return $output;
    }
}
